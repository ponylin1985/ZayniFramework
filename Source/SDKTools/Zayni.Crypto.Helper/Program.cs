﻿using System;
using System.Linq;
using ZayniFramework.Common;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;
using System.IO;


namespace ZayniFramework.Crypto.Helper
{
    /// <summary>Zayni Framework Crypto Helper dotnet tool CLI application<para/>
    /// <para>* 以下為範例:</para>
    /// <para>  * `dotnet zch genkey -o /Users/pony/GitRepo/MyGitLab/ZayniFramework/Source/SDKTools/Zayni.Crypto.Helper/Crypto.key`</para>
    /// <para>  * `dotnet zch encrypt -f /Users/pony/GitRepo/MyGitLab/ZayniFramework/Source/SDKTools/Zayni.Crypto.Helper/Test.dll.config -k jXnKj2NdjGs7w`</para>
    /// <para>  * `dotnet zch encrypt-config -f /Users/pony/GitRepo/MyGitLab/ZayniFramework/Source/SDKTools/Zayni.Crypto.Helper/Test.dll.config -k jXnKj2NdjGs7w`</para>
    /// </summary>
    public class Program
    {
        /// <summary>主程式<para/>
        /// <para>* 產生金鑰檔案: `zch genkey -o {output_path}`</para>
        /// <para>* 執行對 .NET config 設定檔中的 db connection string 加密: `zch encrypt -f {config_path} -k {encryptKeyString}`</para>
        /// <para>* 執行對整份 .NET config 設定檔進行加密: `zch encrypt-config -f {config_path} -k {encryptKeyString}`</para>
        /// </summary>
        /// <param name="args">命令列參數</param>
        /// <returns>應用程式回傳碼</returns>
        public static int Main(params string[] args)
        {
            try
            {
                args = args.Where(h => h.IsNotNullOrEmpty()).ToArray();

                if (args.IsNullOrEmpty())
                {
                    Stdout($"Welcome to use Zayni Framework Crypto Helper dotnet tool CLI application.");
                    Stdout("dotnet tool version: 8.0.142.");
                    Stdout("Use the following commands to generate key file for ZayniFramework.Cryptography module or");
                    Stdout("Encrypt the db connection string for specific .NET config file.");
                    Command.Stdout("zch genkey -o {output_path}", ConsoleColor.Yellow, false);
                    Command.Stdout("zch encrypt -f {config_path} -k {encryptKeyString}", ConsoleColor.Yellow, false);
                    return 0;
                }

                var command = args.FirstOrDefault()?.Trim()?.ToLower();

                if (command != "genkey" && command != "encrypt" && command != "encrypt-config")
                {
                    StdErr($"Unknow zch command.");
                    StdErr("Use the following commands to generate key file for ZayniFramework.Cryptography module or");
                    StdErr("Encrypt the db connection string for specific .NET config file.");
                    Command.Stdout("zch genkey -o {output_path}", ConsoleColor.Yellow, false);
                    Command.Stdout("zch encrypt -f {config_path} -k {encryptKeyString}", ConsoleColor.Yellow, false);
                    Command.Stdout("zch encrypt-config -f {config_path} -k {encryptKeyString}", ConsoleColor.Yellow, false);
                    return -1;
                }

                if (command == "genkey")
                {
                    return GenerateKey();
                }

                if (command == "encrypt")
                {
                    return EncryptConnectionStringsSection();
                }

                if (command == "encrypt-config")
                {
                    return EncryptConfigFile();
                }

                Console.WriteLine();
                return 0;
            }
            catch (Exception ex)
            {
                StdErr($"Error!{Environment.NewLine}{ex}");
                return -1;
            }

            int GenerateKey()
            {
                if (args.Length != 3)
                {
                    StdErr($"Invalid zch genkey command.");
                    StdErr("Valid command --> zch genkey -o {output_path}");
                    return -1;
                }

                if (args.Where(g => g == "-o").Count() != 1)
                {
                    StdErr($"Invalid zch genkey command.");
                    StdErr("Valid command --> zch genkey -o {output_path}");
                    return -1;
                }

                var o = Array.FindIndex(args, z => z == "-o");
                var path = args[o + 1]?.Trim();

                if (!path.EndsWith(".key"))
                {
                    StdErr($"Invalid zch genkey command.");
                    StdErr($"Invalid output_path argument. The output path must ends with '.key' extension name.");
                    return -1;
                }

                var sb = new StringBuilder();

                For.Reset();
                For.Do(5, () =>
                {
                    var text = RandomTextHelper.Create(100);
                    sb.AppendLine(text);
                });

                var c = CreateKeyFile(path, sb.ToString());

                if (!c.Success)
                {
                    StdErr($"Create key file occur error. {Environment.NewLine}{c.Message}");
                    return -1;
                }

                Stdout($"Encryption key file created!");
                Stdout($"Key file: {path}");
                return 0;
            }

            int EncryptConnectionStringsSection()
            {
                if (args.Length != 5)
                {
                    StdErr($"Invalid zch encrypt command.");
                    StdErr("Valid command --> zch encrypt -f {config_path} -k {encryptKeyString}");
                    return -1;
                }

                if (args.Where(g => g == "-f").Count() != 1 || args.Where(g => g == "-k").Count() != 1)
                {
                    StdErr($"Invalid zch encrypt command.");
                    StdErr("Valid command --> zch encrypt -f {config_path} -k {encryptKeyString}");
                    return -1;
                }

                var f = Array.FindIndex(args, f => f == "-f");
                var configPath = args[f + 1]?.Trim();

                var k = Array.FindIndex(args, f => f == "-k");
                var cryptoKey = args[k + 1]?.Trim();

                if (configPath.IsNullOrEmpty())
                {
                    StdErr($"Invalid zch encrypt command.");
                    StdErr("Valid command --> zch encrypt -f {config_path} -k {encryptKeyString}");
                    return -1;
                }

                if (!configPath.EndsWith(".config"))
                {
                    StdErr($"Invalid zch encrypt command.");
                    StdErr($"Invalid config_path argument. The path of .NET config file must ends with '.config' extension name.");
                    return -1;
                }

                var fileConfigSource = new FileConfigSource(configPath);
                var connStringSection = fileConfigSource.GetConnectionStringsSection();

                if (connStringSection.IsNullOrEmpty())
                {
                    Command.Stdout($"No db connection string found in config file.", ConsoleColor.Yellow, false);
                    return 0;
                }

                foreach (ConnectionStringSettings connStringConfig in connStringSection)
                {
                    if (0 == string.Compare(connStringConfig.Name, "LocalSqlServer", true))
                    {
                        continue;
                    }

                    if (connStringConfig.ConnectionString.IsNullOrEmpty())
                    {
                        continue;
                    }

                    var encryptString = AesEncryptBase64(connStringConfig.ConnectionString, cryptoKey);
                    connStringConfig.ConnectionString = encryptString;
                }

                fileConfigSource.Save();
                Stdout($"Database connection string encrypt successfully!");
                return 0;
            }

            int EncryptConfigFile()
            {
                if (args.Length != 5)
                {
                    StdErr($"Invalid zch encrypt command.");
                    StdErr("Valid command --> zch encrypt-config -f {config_path} -k {encryptKeyString}");
                    return -1;
                }

                if (args.Where(g => g == "-f").Count() != 1 || args.Where(g => g == "-k").Count() != 1)
                {
                    StdErr($"Invalid zch encrypt command.");
                    StdErr("Valid command --> zch encrypt-config -f {config_path} -k {encryptKeyString}");
                    return -1;
                }

                var f = Array.FindIndex(args, f => f == "-f");
                var configPath = args[f + 1]?.Trim();

                var k = Array.FindIndex(args, f => f == "-k");
                var cryptoKey = args[k + 1]?.Trim();

                if (configPath.IsNullOrEmpty())
                {
                    StdErr($"Invalid zch encrypt command.");
                    StdErr("Valid command --> zch encrypt-config -f {config_path} -k {encryptKeyString}");
                    return -1;
                }

                var rawData = File.ReadAllText(configPath, Encoding.UTF8);
                var encryptData = AesEncryptBase64(rawData, cryptoKey);

                File.Delete(configPath);
                File.WriteAllText(configPath, encryptData, Encoding.UTF8);

                Stdout($"Config file encrypt successfully!");
                return 0;
            }
        }

        /// <summary>產生金鑰
        /// </summary>
        /// <param name="path">金鑰路徑</param>
        /// <param name="keyText">金鑰文字內容</param>
        /// <returns>產生金鑰是否成功</returns>
        private static Result CreateKeyFile(string path, string keyText)
        {
            var result = Result.Create();

            try
            {
                When.True(File.Exists(path), () => File.Delete(path));
                File.WriteAllText(path, keyText);
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                return result;
            }

            result.Success = true;
            return result;
        }

        /// <summary>AES 對稱式加密
        /// </summary>
        /// <param name="source">加密前字串</param>
        /// <param name="cryptoKey">加密金鑰</param>
        /// <returns>加密後字串</returns>
        public static string AesEncryptBase64(string source, string cryptoKey)
        {
            var encrypt = "";
            var keyString = $"rNduE{cryptoKey}NdjwZtcSk";

            try
            {
                var aes = Aes.Create();
                var md5 = MD5.Create();
                var sha256 = SHA256.Create();

                var key = sha256.ComputeHash(Encoding.UTF8.GetBytes(keyString));
                var iv = md5.ComputeHash(Encoding.UTF8.GetBytes(keyString));

                aes.Key = key;
                aes.IV = iv;

                var dataByteArray = Encoding.UTF8.GetBytes(source);

                using var ms = new MemoryStream();
                using var cs = new CryptoStream(ms, aes.CreateEncryptor(), CryptoStreamMode.Write);
                cs.Write(dataByteArray, 0, dataByteArray.Length);
                cs.FlushFinalBlock();
                encrypt = Convert.ToBase64String(ms.ToArray());
            }
            catch (Exception ex)
            {
                Command.StdoutErr(ex.ToString());
                throw;
            }

            return encrypt;
        }

        /// <summary>Console 主控台標準輸出
        /// </summary>
        /// <param name="message">訊息</param>
        private static void Stdout(string message) => Command.Stdout(message, ConsoleColor.Green, false);

        /// <summary>Console 主控台標準錯誤輸出
        /// </summary>
        /// <param name="message">訊息</param>
        private static void StdErr(string message) => Command.Stdout(message, ConsoleColor.Red, false);
    }
}
