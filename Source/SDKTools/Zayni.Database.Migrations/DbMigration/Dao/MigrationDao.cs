﻿using ZayniFramework.DataAccess;


namespace ZayniFramework.Database.Migrations
{
    /// <summary>資料庫當前 Migration 版本的資料存取類別
    /// </summary>
    internal class MigrationDao : BaseDataAccess
    {
        /// <summary>多載建構子
        /// </summary>
        /// <param name="dbName">資料庫連線設定名稱</param>
        public MigrationDao(string dbName) : base(dbName)
        {
        }
    }
}
