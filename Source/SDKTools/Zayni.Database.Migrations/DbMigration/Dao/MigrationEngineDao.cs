﻿using System.Data;
using System.Data.Common;
using ZayniFramework.Common;
using ZayniFramework.DataAccess;


namespace ZayniFramework.Database.Migrations
{
    /// <summary>Database 資料庫 Schema Migration 資料存取引擎
    /// </summary>
    internal class MigrationEngineDao : BaseDataAccess
    {
        #region 宣告建構子

        /// <summary>多載建構子
        /// </summary>
        /// <param name="dbName">資料庫連線名稱</param>
        public MigrationEngineDao(string dbName) : base(dbName)
        {
        }

        #endregion 宣告建構子


        #region 宣告公開的方法

        /// <summary>建立全新的資料庫連線
        /// </summary>
        /// <param name="dbName">資料庫連線字串名稱</param>
        /// <returns>新的資料庫連線</returns>
        public new DbConnection CreateConnection(string dbName = "") => base.CreateConnection(dbName);

        /// <summary>開啟資料庫交易
        /// </summary>
        /// <param name="connection">來源資料庫連線</param>
        /// <param name="isolationLevel">交易鎖定層級</param>
        /// <returns>資料庫交易</returns>
        public new DbTransaction BeginTransaction(DbConnection connection, IsolationLevel isolationLevel = IsolationLevel.ReadCommitted) =>
            base.BeginTransaction(connection, isolationLevel);

        /// <summary>確認目標資料庫交易
        /// </summary>
        /// <param name="transaction">目標資料庫交易</param>
        /// <returns>是否確認交易成功</returns>
        internal new bool Commit(DbTransaction transaction) => base.Commit(transaction);

        /// <summary>取消目標資料庫交易
        /// </summary>
        /// <param name="transaction">目標資料庫交易</param>
        /// <returns>是否取消交易成功</returns>
        internal new bool Rollback(DbTransaction transaction) => base.Rollback(transaction);

        /// <summary>執行資料庫 Migration 腳本
        /// </summary>
        /// <param name="sqlMigration">資料庫 Migration SQL 腳本</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <returns>執行結果</returns>
        public Result<int> Execute(string sqlMigration, DbConnection connection, DbTransaction transaction)
        {
            var result = new Result<int>()
            {
                Success = false,
                Data = -1
            };

            var cmd = GetSqlStringCommand(sqlMigration, connection, transaction);

            if (!base.ExecuteNonQuery(cmd))
            {
                result.Message = base.Message;
                return result;
            }

            result.Success = true;
            return result;
        }

        #endregion 宣告公開的方法
    }
}
