﻿using System;
using ZayniFramework.Common;
using ZayniFramework.DataAccess;
using ZayniFramework.DataAccess.Lightweight;
using ZayniFramework.Logging;


namespace ZayniFramework.Database.Migrations
{
    /// <summary>資料庫當前 Migration 版本的 DataContext 資料存取類別
    /// </summary>
    internal class MigrationDataContext : DataContext<MigrationModel, MigrationModel>
    {
        #region 宣告私有的欄位

        /// <summary>資料庫連線設定名稱
        /// </summary>
        private string _dbName;

        #endregion 宣告私有的欄位


        #region 宣告建構子

        /// <summary>多載建構子
        /// </summary>
        /// <param name="dbName">資料庫連線設定名稱</param>
        public MigrationDataContext(string dbName) : base(dbName)
        {
            _dbName = dbName;
        }

        /// <summary>解構子
        /// </summary>
        ~MigrationDataContext()
        {
            _dbName = null;
        }

        #endregion 宣告建構子


        #region 宣告公開的方法

        /// <summary>新增或更新 Migration 版本資料
        /// </summary>
        /// <param name="current">資料庫 Migration 版本資料模型</param>
        /// <param name="pervious">資料庫目前版本的 Migration 紀錄</param>
        /// <param name="transaction">資料庫交易</param>
        /// <returns>執行結果</returns>
        public Result InsertOrUpdate(MigrationModel current, MigrationModel pervious)
        {
            var result = Result.Create();

            Result<MigrationModel> r;

            if (pervious.IsNull())
            {
                r = base.Insert(current);
            }
            else
            {
                r = base.Update(current, wheres: [new DbParameterEntry() { ColumnName = "CURRENT_SCRIPT", Name = "ScriptName", DbType = DbColumnType.String, Value = pervious.ScriptFileName }]);
            }

            if (!r.Success)
            {
                result.Message = $"Insert or Update fs_db_migration data to database '{_dbName}' occur error. {Environment.NewLine}{r.Message}";
                Logger.Error(this, result.Message, Logger.GetTraceLogTitle(this, nameof(InsertOrUpdate)));
            }

            return r.GetResult();
        }

        #endregion 宣告公開的方法
    }
}
