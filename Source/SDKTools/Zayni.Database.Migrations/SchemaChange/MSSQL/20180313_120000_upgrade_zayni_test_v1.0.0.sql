CREATE TABLE [ZayniTest]
(
	[AccountId] NVARCHAR(50) NOT NULL PRIMARY KEY, 
    [Name] NVARCHAR(50) NOT NULL, 
    [Age] INT NOT NULL, 
    [Sex] INT NOT NULL, 
    [Birthday] DATETIME NULL, 
    [IsVIP] BIT NULL, 
    [IsGood] BIT NULL, 
    [DataFlag] TIMESTAMP NOT NULL
);