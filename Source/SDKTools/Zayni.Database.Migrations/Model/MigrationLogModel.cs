﻿using System;
using ZayniFramework.Common.ORM;


namespace ZayniFramework.Database.Migrations
{
    /// <summary>資料庫 Migration 執行日誌記錄資料模型
    /// </summary>
    [MappingTable(TableName = "FS_DB_MIGRATION_LOG")]
    internal class MigrationLogModel
    {
        /// <summary>日誌記錄流水號
        /// </summary>
        [TableColumn(ColumnName = "LOG_SRNO", IsPrimaryKey = true, OrderByDesc = true)]
        public int LogSrNo { get; set; }

        /// <summary>資料庫 migration 腳本檔案名稱
        /// </summary>
        [TableColumn(ColumnName = "SCRIPT")]
        public string Script { get; set; }

        /// <summary>資料庫 migration 腳本類型: upgrade、downgrade
        /// </summary>
        [TableColumn(ColumnName = "SCRIPT_TYPE")]
        public string ScriptType { get; set; }

        /// <summary>資料庫 migration 腳本的版本號
        /// </summary>
        [TableColumn(ColumnName = "SCRIPT_VERSION")]
        public string ScriptVersion { get; set; }

        /// <summary>資料庫 migration 腳本的時間戳記
        /// </summary>
        [TableColumn(ColumnName = "SCRIPT_TIMESTAMP")]
        public DateTime ScriptTimestamp { get; set; }

        /// <summary>資料庫 migration 腳本內容
        /// </summary>
        [TableColumn(ColumnName = "SCRIPT_CONTENT", IsAllowNull = true, DefaultValue = null)]
        public string ScriptContent { get; set; }

        /// <summary>執行是否成功
        /// </summary>
        [TableColumn(ColumnName = "IS_SUCCESS")]
        public bool IsSuccess { get; set; }

        /// <summary>執行結果訊息
        /// </summary>
        [TableColumn(ColumnName = "MESSAGE", IsAllowNull = true, DefaultValue = null)]
        public string Message { get; set; }

        /// <summary>執行日誌時間
        /// </summary>
        [TableColumn(ColumnName = "LOG_TIME")]
        public DateTime LogTime { get; set; }
    }
}
