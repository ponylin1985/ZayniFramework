﻿using System;
using ZayniFramework.Common.ORM;


namespace ZayniFramework.Database.Migrations
{
    /// <summary>資料庫當前 Migration 版本的資料模型
    /// </summary>
    [MappingTable(TableName = "FS_DB_MIGRATION")]
    internal class MigrationModel
    {
        /// <summary>資料庫 Migration 腳本的檔名<para/>
        /// 1. 基本格式: {yyyyMMdd}_{HHmmss}_{ScriptType}_{ProjectNameOrServiceName}_v{ScriptVersion}.sql<para/>
        /// 2. 全部以下底線當作分隔符號。<para/>
        /// 3. 相同的 ScriptVersion 版本的 ScriptTimestamp 必須完全一致，無論是 upgrade 或 downgrade 類型的腳本。<para/>
        /// 4. ProjectNameOrService 中間可以自行有其他的下底線，但是，ScriptVersion 絕對要放在最後一個段落。<para/>
        /// 5. ScriptTimestamp 基本上只是一個與版本號固定對應的時間戳記，可以自行給定，只要符合第三項原則即可。<para/>
        /// 6. ScriptType 注意有大小寫區別，整個檔案名稱實際上都有大小寫區別。<para/>
        /// 7. 副檔名限定必須是 .sql<para/>
        /// 範例如下:<para/>
        /// 20180313_120000_upgrade_zayni_test_v1.0.0.sql<para/>
        /// 20180315_140500_upgrade_zayni_test_v1.1.0.sql<para/>
        /// 20180315_140500_downgrade_zayni_test_v1.1.0.sql<para/>
        /// 20180315_143000_upgrade_zayni_test_v1.1.1.sql
        /// </summary>
        [TableColumn(ColumnName = "CURRENT_SCRIPT")]
        public string ScriptFileName { get; set; }

        /// <summary>腳本類型: upgrade 或 downgrade，有大小寫區別。
        /// </summary>
        public string ScriptType { get; set; }

        /// <summary>資料庫 migration 腳本的版本號
        /// </summary>
        [TableColumn(ColumnName = "CURRENT_SCRIPT_VERSION")]
        public string ScriptVersion { get; set; }

        /// <summary>資料庫版本的時間戳記
        /// </summary>
        [TableColumn(ColumnName = "CURRENT_SCRIPT_TIMESTAMP")]
        public DateTime ScriptTimestamp { get; set; }
    }
}
