﻿namespace ZayniFramework.Database.Migrations
{
    /// <summary>CLI 指令回傳代碼
    /// </summary>
    internal class CliReturnCode
    {
        /// <summary>不合法的 CLI 命令輸入
        /// </summary>
        internal const int INVALID_COMMAND = -100;

        /// <summary>組態設定錯誤
        /// </summary>
        internal const int CONFIG_ERROR = -101;

        /// <summary>執行發生錯誤或異常
        /// </summary>
        internal const int ERROR = -102;

        /// <summary>執行 Migration SQL 腳本失敗
        /// </summary>
        internal const int EXECUTE_MIGRATION_SCRIPT_FAIL = -200;

        /// <summary>執行成功
        /// </summary>
        internal const int SUCCESS = 0;
    }
}
