﻿using System;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.Database.Migrations
{
    /// <summary>Help CLI 命令<para/>
    /// zdm help
    /// </summary>
    internal class HelpCliCommand : CliCommand
    {
        /// <summary>CLI 命令的動作
        /// </summary>
        /// <returns>CLI 命令執行結果</returns>
        public override Result<int> Action()
        {
            var result = new Result<int>();

            Console.WriteLine();
            ConsoleLogger.WriteLine($"Zayni Framework Database Migrations support CLI commands:", ConsoleColor.Green);
            Console.WriteLine();
            ConsoleLogger.WriteLine("CLI Commands                       Descriptions", ConsoleColor.Green);
            ConsoleLogger.WriteLine("===============================================", ConsoleColor.Green);
            Console.WriteLine();
            ConsoleLogger.WriteLine("help                               Display all supported CLI commands.", ConsoleColor.Green);
            ConsoleLogger.WriteLine("configure {path}                   Configure the zayni framework config file path.", ConsoleColor.Green);
            ConsoleLogger.WriteLine("configs                            Display the current zayni framework config file path.", ConsoleColor.Green);
            ConsoleLogger.WriteLine("create -v {version} -n {db_name}   Create migration script file.", ConsoleColor.Green);
            ConsoleLogger.WriteLine("migration {version}                Execute database mirgation to specific version.", ConsoleColor.Green);
            Console.WriteLine();

            result.Data = CliReturnCode.SUCCESS;
            result.Success = true;
            return result;
        }
    }
}
