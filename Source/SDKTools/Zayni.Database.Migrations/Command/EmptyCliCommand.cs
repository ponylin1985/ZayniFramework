﻿using System;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.Database.Migrations
{
    /// <summary>空的 CLI 命令<para/>
    /// zdm
    /// </summary>
    internal class EmptyCliCommand : CliCommand
    {
        /// <summary>CLI 命令的動作
        /// </summary>
        /// <returns>CLI 命令執行結果</returns>
        public override Result<int> Action()
        {
            var result = new Result<int>()
            {
                Success = false,
                Data = -1
            };

            Console.WriteLine();
            ConsoleLogger.WriteLine($"Welcome to use Zayni Framework Database Migrations SDK Tool Command-line interface.", ConsoleColor.Green);
            ConsoleLogger.WriteLine($"Enter CLI command to process database schema mirgation or", ConsoleColor.Green);
            ConsoleLogger.WriteLine($"enter 'zdm help' to check out all supported CLI commands.", ConsoleColor.Green);
            Console.WriteLine();

            result.Data = 0;
            result.Success = true;
            return result;
        }
    }
}
