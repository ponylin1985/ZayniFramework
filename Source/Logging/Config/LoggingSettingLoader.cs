﻿using System;
using System.Collections.Generic;
using ZayniFramework.Common;


namespace ZayniFramework.Logging
{
    /// <summary>日誌功能設定 Config 讀取器
    /// </summary>
    internal static class LoggingSettingLoader
    {
        #region 宣告靜態的私有欄位

        /// <summary>The configuration of ZayniFramework.
        /// </summary>
        /// <returns></returns>
        private static readonly ZayniFrameworkSettings _config = ConfigManager.GetZayniFrameworkSettings();

        #endregion 宣告靜態的私有欄位


        #region 宣告內部的靜態方法

        /// <summary>取得 Config 設定檔中 LoggingSetting 區段的設定值
        /// </summary>
        /// <returns></returns>
        internal static LoggingSettings GetLoggingModuleConfigs() => _config.LoggingSettings;

        /// <summary>讀取 Config 檔中 LoggingSetting 區段的設定值
        /// </summary>
        /// <returns>LoggingSetting 區段的設定值</returns>
        internal static LogSettingsOption LoadLoggingSettings()
        {
            var result = new LogSettingsOption
            {
                IsEnable = false,
                IsEventLogEnable = false
            };

            if (_config.IsNull())
            {
                ConsoleLogger.LogError($"zayni.json is null. Can not load Zayni Framework config settings.");
                throw new Exception($"zayni.json is null. Can not load Zayni Framework config settings.");
            }

            try
            {
                result.IsEnable = _config.LoggingSettings.EnableLog;
                result.IsEventLogEnable = _config.LoggingSettings.EnableWindowsEventLog;
            }
            catch (Exception ex)
            {
                ConsoleLogger.LogError($"Load Zayni Framework Logging Setting occur exception. {ex}");
                throw;
            }

            return result;
        }

        /// <summary>讀取 Config 檔中預設文字日誌功能的設定值
        /// </summary>
        /// <returns>讀取設定值結果</returns>
        internal static Result<List<LoggerSetting>> GetLoggerSettings()
        {
            var result = Result.Create<List<LoggerSetting>>();

            if (_config.IsNull())
            {
                result.Message = $"{GetLogActionName(nameof(GetLoggerSettings))}, read DefaultTextLogger config section occur error due to the ZayniConfigSection is null.";
                ConsoleLogger.LogError(result.Message);
                return result;
            }

            List<LoggerSetting> configs = null;

            try
            {
                configs = _config.LoggingSettings.LoggerSettings;
            }
            catch (Exception ex)
            {
                result.Message = $"{GetLogActionName(nameof(GetLoggerSettings))}, read DefaultTextLogger config section occur exception. ZayniFramework config error. {ex}";
                ConsoleLogger.LogError(result.Message);
                return result;
            }

            result.Data = configs;
            result.Success = true;
            return result;
        }

        /// <summary>取得 Config 檔設定中指定的 EmailNotifyLogger 設定值
        /// </summary>
        /// <param name="loggerName">EmailNotifyLogger 組態設定名稱</param>
        /// <returns>取得結果</returns>
        internal static Result<EmailNotifyLoggerSetting> GetEmailNotifyLoggerSetting(string loggerName)
        {
            var result = Result.Create<EmailNotifyLoggerSetting>();

            if (loggerName.IsNullOrEmpty())
            {
                return result;
            }

            var configs = _config.LoggingSettings.EmailNotifyLoggerSettings;
            result.Data = configs.Find(m => m.Name == loggerName);
            return result;
        }

        #endregion 宣告內部的靜態方法


        #region 宣告私有的方法

        /// <summary>取得方法名稱的日誌訊息
        /// </summary>
        /// <param name="methodName">目標方法名稱</param>
        /// <returns>方法名稱的日誌訊息</returns>
        private static string GetLogActionName(string methodName) => $"{nameof(LoggingSettingLoader)}.{methodName}";

        #endregion 宣告私有的方法
    }
}
