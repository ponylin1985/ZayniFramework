﻿using System;
using System.Text;
using ZayniFramework.Common;


namespace ZayniFramework.Logging
{
    /// <summary>日誌訊息產生器
    /// </summary>
    public static class LogMessageMaker
    {
        /// <summary>產生程式異常事件的日誌訊息
        /// </summary>
        /// <param name="logMetadata">日誌記錄資訊</param>
        /// <returns>程式異常事件的日誌訊息</returns>
        public static string MakeExceptionEventLogMessage(LogOption logMetadata) =>
            MakeExceptionEventLogMessage(logMetadata.ExceptionObject, logMetadata.Message);

        /// <summary>產生程式異常事件的日誌訊息
        /// </summary>
        /// <param name="ex">程式異常物件</param>
        /// <param name="message">事件訊息</param>
        /// <returns>程式異常事件的日誌訊息</returns>
        public static string MakeExceptionEventLogMessage(Exception ex, string message)
        {
            if (ex.IsNull())
            {
                return message;
            }

            var sb = new StringBuilder();

            sb.Append(Environment.NewLine);
            sb.Append(ex.ToString());    // 20131206 Pony Says: .NET 的 Exception 物件的 ToString() 有特別處理過，會直接回傳 Message 和 CallStack

            if (ex.InnerException.IsNotNull())
            {
                sb.Append(Environment.NewLine);
                sb.Append($"InnerException:{Environment.NewLine}{ex.InnerException}");
                sb.Append(Environment.NewLine);
            }

            var result = $"{message} {sb}";
            return result;
        }
    }
}
