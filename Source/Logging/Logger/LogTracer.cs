using System;
using ZayniFramework.Common;


namespace ZayniFramework.Logging
{
    /// <summary>日誌訊息追蹤器
    /// </summary>
    public class LogTracer : IDisposable
    {
        /// <summary>多載建構子
        /// </summary>
        /// <param name="loggerName">日誌器名稱，預設值為 _DefaultLog，日誌將會與 ZayniFramework 框架內部的日誌一起輸出。。</param>
        internal LogTracer(string loggerName = "_DefaultLog")
        {
            LoggerName = loggerName;
            GenerateLogTraceId();
        }

        /// <summary>日誌器名稱
        /// </summary>
        /// <value></value>
        public string LoggerName { get; internal set; }

        /// <summary>日誌訊息的追蹤代碼
        /// </summary>
        /// <value></value>
        public string LogTraceId { get; internal set; }

        /// <summary>產生日誌訊息追蹤代碼
        /// </summary>
        /// <returns></returns>
        internal void GenerateLogTraceId() => LogTraceId = RandomTextHelper.Create(36);

        /// <summary>
        /// </summary>
        public void Dispose()
        {
            LogTraceId = null;
        }
    }
}