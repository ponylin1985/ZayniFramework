using System;
using System.Collections.Generic;
using NeoSmart.AsyncLock;
using ZayniFramework.Common;


namespace ZayniFramework.Logging
{
    /// <summary>日誌訊息追蹤器
    /// </summary>
    public static class LogTracerContainer
    {
        #region Private Static Fields

        /// <summary>非同步作業鎖定物件
        /// </summary>
        private static readonly AsyncLock _asyncLockInstance = new();

        /// <summary>
        /// </summary>
        /// <returns></returns>
        private static readonly Dictionary<string, LogTracer> _tracers = [];

        #endregion Private Static Fields


        #region Public Static Methods

        /// <summary>依照指定的日誌器名稱 (loggerName) 建立日誌訊息追蹤器，如果相同的日誌器已經有對應的日誌追蹤器 (LogTracer) 則不會重複建立，並且回傳建立成功。
        /// </summary>
        /// <param name="loggerName">日誌器名稱，預設值為 _DefaultLog，日誌將會與 ZayniFramework 框架內部的日誌一起輸出。。</param>
        /// <returns></returns>
        public static LogTracer CreateLogTracer(string loggerName = "_DefaultLog")
        {
            try
            {
                using (_asyncLockInstance.Lock())
                {
                    if (loggerName.IsNullOrEmpty())
                    {
                        var errorMsg = $"Cannot create a LogTracer instance due to the '{nameof(loggerName)}' argument is null or empty.";
                        ConsoleLogger.LogError(errorMsg);
                        throw new ArgumentNullException(errorMsg);
                    }
                }

                if (_tracers.ContainsKey(loggerName) && _tracers.TryGetValue(loggerName, out var tracer) && tracer.IsNotNull())
                {
                    return tracer;
                }

                var logTracer = new LogTracer(loggerName);
                _tracers.Add(loggerName, logTracer);
                return logTracer;
            }
            catch (Exception ex)
            {
                ConsoleLogger.LogError($"An exception occur while creating a new LogTracer instance and add into LogTracerContainer.{Environment.NewLine}{ex}");
                throw;
            }
        }

        #endregion Public Static Methods


        #region Internal Static Methods

        /// <summary>嘗試從容器中取得被建立好並且控管的 LogTracer 追蹤器
        /// </summary>
        /// <param name="loggerName">日誌名稱</param>
        /// <param name="tracer">日誌追蹤器</param>
        internal static bool TryGetLogTracer(string loggerName, out LogTracer tracer)
        {
            tracer = null;

            try
            {
                using (_asyncLockInstance.Lock())
                {
                    if (loggerName.IsNullOrEmpty())
                    {
                        ConsoleLogger.LogError($"An error ocurr while trying retive a LogTracer instance from LogTracerContainer due to the '{nameof(loggerName)}' is null or empty.");
                        return false;
                    }
                }

                if (!_tracers.TryGetValue(loggerName, out tracer) && tracer.IsNotNull())
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                ConsoleLogger.LogError($"An error ocurr while trying retive a LogTracer instance from LogTracerContainer.{Environment.NewLine}{ex}");
                return false;
            }
        }

        #endregion Internal Static Methods

    }
}