﻿using NeoSmart.AsyncLock;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using ZayniFramework.Common;


namespace ZayniFramework.Logging
{
    /// <summary>日誌記錄元件類別
    /// </summary>
    public sealed class Logger
    {
        #region Private Members

        /// <summary>非同步作業鎖定物件
        /// </summary>
        private static readonly AsyncLock _asyncLock = new();

        /// <summary>Logging 日誌功能設定值
        /// </summary>
        private static LogSettingsOption _logInfo = null;

        /// <summary>文字檔 Logger 常數池
        /// </summary>
        private static readonly Dictionary<string, LogOption> _loggers = [];

        /// <summary>寫入日誌的委派
        /// </summary>
        /// <param name="sender">觸發來源物件</param>
        /// <param name="eventMessage">日誌記錄訊息</param>
        /// <param name="eventTitle">日誌記錄標題</param>
        /// <param name="loggerName">日誌檔名稱設定值</param>
        private delegate void WriteLogHandler(object sender, string eventMessage, string eventTitle, string loggerName = null);

        #endregion Private Members


        #region Static Constructor

        /// <summary>靜態建構子
        /// </summary>
        static Logger() => StaticInitialize();

        #endregion Static Constructor


        #region Private Static Methods

        /// <summary>靜態初始化
        /// </summary>
        private static void StaticInitialize()
        {
            LoadLoggingSettings();
            LoadTextLoggingSettings();
        }

        /// <summary>讀取 Zayni.Logging 模組的全域性設定值
        /// </summary>
        private static void LoadLoggingSettings()
        {
            _logInfo = LoggingSettingLoader.LoadLoggingSettings();
        }

        /// <summary>取得文字日誌功能Config設定值，並且加入到文字檔Log常數池內
        /// </summary>
        private static void LoadTextLoggingSettings()
        {
            #region 讀取日誌 Config 檔設定值

            var g = LoggingSettingLoader.GetLoggerSettings();

            if (!g.Success)
            {
                ConsoleLogger.LogError($"Load Zayni Framework Logging setting occur. {g.Message}");
                return;
            }

            #endregion 讀取日誌 Config 檔設定值

            var configs = g.Data;

            foreach (var cfg in configs)
            {
                #region 初始化文字日誌記錄資訊

                var textLogSetting = new LogOption();

                try
                {
                    textLogSetting.Name = cfg.Name;
                    textLogSetting.IsEnable = cfg.EnableLogger;
                    textLogSetting.LogPath = cfg.FileLogSetting?.Path;
                    textLogSetting.LogFileMaxSize = cfg.MaxFileSize;
                    textLogSetting.EnableConsoleOutput = cfg.EnableConsoleOutput;
                    textLogSetting.ConsoleOutpotColor = cfg.ConsoleColor;
                    textLogSetting.EmailNotifyLoggerName = cfg.EmailNotifyLoggerName;
                    textLogSetting.DbLoggerName = cfg.DbLoggerName;
                    textLogSetting.DbLoggerType = cfg.DbLoggerType;
                    textLogSetting.ESLoggerName = cfg.ElasticStackLoggerName;
                    textLogSetting.Message = string.Empty;
                    textLogSetting.Title = string.Empty;
                }
                catch (Exception ex)
                {
                    ConsoleLogger.LogError(ex.ToString());
                    return;
                }

                #endregion 初始化文字日誌記錄資訊

                #region 檢查日誌記錄層級篩選器

                var strCategories = "";

                try
                {
                    if (cfg.Filter.IsNotNull())
                    {
                        strCategories = cfg.Filter.Category;
                    }

                    if (strCategories.IsNotNullOrEmpty())
                    {
                        var categories = strCategories.SplitString(",", true);
                        categories.WhenNotNullOrEmpty(s =>
                        {
                            categories.When(
                                e => categories.Contains(EventLogEntryType.Information.ToString()),
                                f => textLogSetting.LogEventFilters.Add(EventLogEntryType.Information.ToString())
                            );

                            categories.When(
                                k => categories.Contains(EventLogEntryType.Warning.ToString()),
                                z => textLogSetting.LogEventFilters.Add(EventLogEntryType.Warning.ToString())
                            );
                        });
                    }
                }
                catch (Exception ex)
                {
                    ConsoleLogger.LogError(ex.ToString());
                    return;
                }

                #endregion 檢查日誌記錄層級篩選器

                #region 加入文字日誌記錄資訊到常數池中

                if (_loggers.ContainsKey(cfg.Name))
                {
                    continue;
                }

                try
                {
                    _loggers.Add(cfg.Name, textLogSetting);
                }
                catch (Exception ex)
                {
                    ConsoleLogger.LogError(ex.ToString());
                    continue;
                }

                #endregion 加入文字日誌記錄資訊到常數池中
            }
        }

        #endregion Private Static Methods


        #region Private Constructor

        /// <summary>私有建構子
        /// </summary>
        private Logger()
        {
        }

        #endregion Private Constructor


        #region Public Static Singleton Property

        /// <summary>Logger 物件實體
        /// </summary>
        private static Logger _instance;

        /// <summary>取得 Logger 獨體的物件
        /// </summary>
        public static Logger Instance
        {
            get
            {
                using (_asyncLock.Lock())
                {
                    if (null == _instance)
                    {
                        _instance = new Logger();
                    }

                    return _instance;
                }
            }
        }

        #endregion Public Static Singleton Property


        #region Public Event Delegates

        /// <summary>紀錄程式異常的委派
        /// </summary>
        /// <param name="ex">程式例外物件</param>
        /// <param name="errorMsg">自訂的錯誤訊息</param>
        /// <param name="sender">傳送此事件的物件</param>
        /// <param name="loggerName">日誌物件設定名稱</param>
        public delegate void ExceptionLogEventHandler(object sender, Exception ex, string errorMsg, string loggerName = null);

        /// <summary>紀錄事件資訊的委派
        /// </summary>
        /// <param name="eventMsg">事件訊息</param>
        /// <param name="eventTitle">事件標題</param>
        /// <param name="sender">傳送此事件的物件</param>
        /// <param name="loggerName">日誌物件設定名稱</param>
        public delegate void InformationLogEventHandler(object sender, string eventMsg, string eventTitle, string loggerName = null);

        /// <summary>紀錄事件警告的委派
        /// </summary>
        /// <param name="eventMsg">事件訊息</param>
        /// <param name="eventTitle">事件標題</param>
        /// <param name="sender">傳送此事件的物件</param>
        /// <param name="loggerName">日誌物件設定名稱</param>
        public delegate void WarningLogEventHandler(object sender, string eventMsg, string eventTitle, string loggerName = null);

        /// <summary>紀錄事件錯誤的委派
        /// </summary>
        /// <param name="errorMsg">事件訊息</param>
        /// <param name="errorTitle">事件標題</param>
        /// <param name="sender">傳送此事件的物件</param>
        /// <param name="loggerName">日誌物件設定名稱</param>
        public delegate void ErrorLogEventHandler(object sender, string errorMsg, string errorTitle, string loggerName = null);

        #endregion Public Event Delegates


        #region Public Evens

        /// <summary>紀錄程式異常的事件
        /// </summary>
        public event ExceptionLogEventHandler WriteExceptionLogEvent;

        /// <summary>紀錄程式資訊的事件
        /// </summary>
        public event InformationLogEventHandler WriteInfomationLogEvent;

        /// <summary>紀錄程式警告的事件
        /// </summary>
        public event WarningLogEventHandler WriteWarningLogEvent;

        /// <summary>紀錄程式錯誤的事件
        /// </summary>
        public event ErrorLogEventHandler WriteErrorLogEvent;

        #endregion Public Evens


        #region Private Event Methods

        /// <summary>觸發紀錄程式異常日誌紀錄的方法
        /// </summary>
        /// <param name="ex">程式異常物件</param>
        /// <param name="message">自訂的錯誤訊息</param>
        /// <param name="sender">觸發此事件的物件</param>
        /// <param name="loggerName">日誌物件設定名稱</param>
        private static void OnExceptionLogEvent(object sender, Exception ex, string message, string loggerName = null)
        {
            using (_asyncLock.Lock())
            {
                if (new object[] { ex, Instance.WriteExceptionLogEvent }.HasNullElements())
                {
                    return;
                }
            }

            Instance.WriteExceptionLogEvent?.Invoke(sender, ex, message, loggerName);
        }

        /// <summary>觸發紀錄訊息日誌紀錄的方法
        /// </summary>
        /// <param name="eventMsg">日誌訊息</param>
        /// <param name="eventTitle">日誌標題</param>
        /// <param name="sender">觸發此事件的物件</param>
        /// <param name="loggerName">日誌物件設定名稱</param>
        private static void OnInformationLogEvent(object sender, string eventMsg, string eventTitle, string loggerName = null)
        {
            using (_asyncLock.Lock())
            {
                if (new string[] { eventMsg, eventTitle }.HasNullOrEmptyElemants())
                {
                    return;
                }
            }

            Instance.WriteInfomationLogEvent?.Invoke(sender, eventMsg, eventTitle, loggerName);
        }

        /// <summary>觸發紀錄警告日誌紀錄的方法
        /// </summary>
        /// <param name="eventMsg">警告訊息</param>
        /// <param name="eventTitle">警告標題</param>
        /// <param name="sender">觸發此事件的物件</param>
        /// <param name="loggerName">日誌物件設定名稱</param>
        private static void OnWarningLogEvent(object sender, string eventMsg, string eventTitle, string loggerName = null)
        {
            using (_asyncLock.Lock())
            {
                if (new string[] { eventMsg, eventTitle }.HasNullOrEmptyElemants())
                {
                    return;
                }
            }

            Instance.WriteWarningLogEvent?.Invoke(sender, eventMsg, eventTitle, loggerName);
        }

        /// <summary>觸發紀錄錯誤日誌紀錄的方法
        /// </summary>
        /// <param name="eventMsg">錯誤訊息</param>
        /// <param name="eventTitle">錯誤標題</param>
        /// <param name="sender">觸發此事件的物件</param>
        /// <param name="loggerName">日誌物件設定名稱</param>
        private static void OnErrorLogEvent(object sender, string eventMsg, string eventTitle, string loggerName = null)
        {
            using (_asyncLock.Lock())
            {
                if (new string[] { eventMsg, eventTitle }.HasNullOrEmptyElemants())
                {
                    return;
                }
            }

            Instance.WriteErrorLogEvent?.Invoke(sender, eventMsg, eventTitle, loggerName);
        }

        #endregion Private Event Methods


        #region Public Static Methods

        /// <summary>取得日誌動作的標題名稱，格式為 $"{className}.{methodName}"
        /// </summary>
        /// <param name="className">類別名稱</param>
        /// <param name="methodName">方法名稱</param>
        /// <returns>日誌動作的標題名稱</returns>
        public static string GetTraceLogTitle(string className, string methodName) => $"{className}.{methodName}";

        /// <summary>取得日誌動作的標題名稱，格式為 $"{className}.{methodName}"
        /// </summary>
        /// <param name="type">類別的型別</param>
        /// <param name="methodName">方法名稱</param>
        /// <returns>日誌動作的標題名稱</returns>
        public static string GetTraceLogTitle(Type type, string methodName) => $"{type.Name}.{methodName}";

        /// <summary>取得日誌動作的標題名稱，格式為 $"{className}.{methodName}"
        /// </summary>
        /// <param name="sender">觸發寫入日誌的物件</param>
        /// <param name="methodName">方法名稱</param>
        /// <returns>日誌動作的標題名稱</returns>
        public static string GetTraceLogTitle(object sender, string methodName) => $"{sender?.GetType().Name}.{methodName}";

        /// <summary>紀錄程式異常到日誌檔
        /// </summary>
        /// <param name="sender">觸發此事件的物件</param>
        /// <param name="ex">程式例外物件</param>
        /// <param name="message">自訂的錯誤訊息</param>
        /// <param name="loggerName">日誌物件設定名稱</param>
        public static void Exception(object sender, Exception ex, string message, string loggerName = null)
        {
            if (_logInfo.IsEnable)
            {
                OnExceptionLogEvent(sender, ex, message);
            }

            WindowsEventLogger.WriteLog(message, ex.Message, EventLogEntryType.Error, _logInfo.IsEventLogEnable, ex);
            WriteLog(loggerName, message, ex.Message, EventLogEntryType.Error, ex);
        }

        /// <summary>紀錄程式異常到日誌檔
        /// </summary>
        /// <param name="sender">觸發此事件的物件</param>
        /// <param name="ex">程式例外物件</param>
        /// <param name="eventTitle">日誌標題</param>
        /// <param name="message">日誌訊息</param>
        /// <param name="loggerName">日誌物件設定名稱</param>
        public static void Exception(object sender, Exception ex, string eventTitle, string message, string loggerName = null)
        {
            if (_logInfo.IsEnable)
            {
                OnExceptionLogEvent(sender, ex, message);
            }

            WindowsEventLogger.WriteLog(eventTitle, message, EventLogEntryType.Error, _logInfo.IsEventLogEnable, ex);
            WriteLog(loggerName, eventTitle, message, EventLogEntryType.Error, ex);
        }

        /// <summary>紀錄資訊到日誌檔
        /// </summary>
        /// <param name="sender">觸發此事件的物件</param>
        /// <param name="eventMsg">日誌訊息</param>
        /// <param name="eventTitle">日誌標題</param>
        /// <param name="loggerName">日誌物件設定名稱</param>
        public static void Info(object sender, string eventMsg, string eventTitle, string loggerName = null) =>
            WriteLog(OnInformationLogEvent, sender, eventMsg, eventTitle, loggerName: loggerName);

        /// <summary>紀錄警告到日誌檔
        /// </summary>
        /// <param name="sender">觸發此事件的物件</param>
        /// <param name="eventMsg">日誌訊息</param>
        /// <param name="eventTitle">日誌標題</param>
        /// <param name="loggerName">日誌物件設定名稱</param>
        public static void Warn(object sender, string eventMsg, string eventTitle, string loggerName = null) =>
            WriteLog(OnWarningLogEvent, sender, eventMsg, eventTitle, EventLogEntryType.Warning, loggerName);

        /// <summary>紀錄錯誤到日誌檔
        /// </summary>
        /// <param name="sender">觸發此事件的物件</param>
        /// <param name="eventMsg">日誌訊息</param>
        /// <param name="eventTitle">日誌標題</param>
        /// <param name="loggerName">日誌物件設定名稱</param>
        public static void Error(object sender, string eventMsg, string eventTitle, string loggerName = null) =>
            WriteLog(OnErrorLogEvent, sender, eventMsg, eventTitle, EventLogEntryType.Error, loggerName);

        #endregion Public Static Methods


        #region Private Static Methods

        /// <summary>執行日誌記錄
        /// </summary>
        /// <param name="handler">寫入日誌記錄委派</param>
        /// <param name="sender">觸發來源物件</param>
        /// <param name="eventMessage">日誌記錄訊息</param>
        /// <param name="eventTitle">日誌記錄標題</param>
        /// <param name="eventType">日誌事件層級種類</param>
        /// <param name="loggerName">日誌器設定名稱</param>
        private static void WriteLog(WriteLogHandler handler, object sender, string eventMessage, string eventTitle, EventLogEntryType eventType = EventLogEntryType.Information, string loggerName = null)
        {
            if (_logInfo.IsEnable)
            {
                try
                {
                    ExecuteLogHandler(handler, sender, eventMessage, eventTitle, loggerName);
                }
                catch (Exception ex)
                {
                    var msg = new StringBuilder();
                    msg.AppendLine($"Zayni Framework Logger try to write log occur runtime exception: {ex}");
                    msg.AppendLine($"Original message:");
                    msg.AppendLine($"Event Title: {eventTitle}");
                    msg.AppendLine($"Event Message: {eventMessage}");
                    WindowsEventLogger.WriteLog($"ZayniFramework {nameof(Logger)}", msg.ToString(), EventLogEntryType.Error, true, ex);
                }
            }

            WindowsEventLogger.WriteLog(eventTitle, eventMessage, eventType, _logInfo.IsEventLogEnable, null);
            WriteLog(loggerName, eventTitle, eventMessage, eventType);
        }

        /// <summary>執行寫入日誌的委派
        /// </summary>
        /// <param name="handler">寫入日誌記錄委派</param>
        /// <param name="sender">觸發來源物件</param>
        /// <param name="eventMessage">日誌記錄訊息</param>
        /// <param name="eventTitle">日誌記錄標題</param>
        /// <param name="loggerName">日誌器設定名稱</param>
        private static void ExecuteLogHandler(WriteLogHandler handler, object sender, string eventMessage, string eventTitle, string loggerName = null) =>
            handler?.Invoke(sender, eventMessage, eventTitle, loggerName);

        /// <summary>寫入日誌訊息
        /// </summary>
        /// <param name="loggerName">日誌器設定名稱</param>
        /// <param name="eventTitle">事件標題</param>
        /// <param name="eventMessage">事件日誌訊息</param>
        /// <param name="eventType">事件紀錄層級</param>
        /// <param name="ex">程式異常物件</param>
        private static void WriteLog(string loggerName, string eventTitle, string eventMessage, EventLogEntryType eventType, Exception ex = null)
        {
            loggerName = loggerName.IsNullOrEmptyString("_DefaultLog");

            // 20151022 Edited by Pony: 只要沒有在 Config 檔中正確設定 LoggingSettings/DefaultTextLogger 的區段，基本上，就不會去執行文字檔的 Log 日誌記錄。
            // 包含連 Zayni Framework 框架內部的 Tracing Log 也一樣。
            if (!_loggers.TryGetValue(loggerName, out var value))
            {
                return;
            }

            var loggerCfg = value;
            var logOption = new LogOption
            {
                Name = loggerCfg.Name,
                IsEnable = loggerCfg.IsEnable,
                LogEventFilters = loggerCfg.LogEventFilters,
                LogPath = loggerCfg.LogPath,
                LogFileMaxSize = loggerCfg.LogFileMaxSize,
                DbLoggerName = loggerCfg.DbLoggerName,
                DbLoggerType = loggerCfg.DbLoggerType,
                ESLoggerName = loggerCfg.ESLoggerName,
                EnableConsoleOutput = loggerCfg.EnableConsoleOutput,
                ConsoleOutpotColor = loggerCfg.ConsoleOutpotColor,
                EmailNotifyLoggerName = loggerCfg.EmailNotifyLoggerName,
                Title = eventTitle,
                Message = eventMessage,
                EventType = eventType,
                ExceptionObject = ex
            };

            if (LogTracerContainer.TryGetLogTracer(loggerName, out var tracer) && tracer.IsNotNull())
            {
                if (tracer.LogTraceId.IsNullOrEmpty())
                {
                    tracer.GenerateLogTraceId();
                }

                logOption.LogtraceId = tracer.LogTraceId;
            }

            EventMessageLogger.Instance.WriteLog(logOption);
        }

        #endregion Private Static Methods
    }
}
