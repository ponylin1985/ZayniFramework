﻿using NeoSmart.AsyncLock;
using System;
using System.Globalization;
using System.Threading.Tasks;
using ZayniFramework.Common;


namespace ZayniFramework.Logging
{
    /// <summary>命令列主控台 Console 日誌器<para/>
    /// * 目前在 docker container 實際進行測試，目前此版本的實作，可以在 docker container 中正常輸出 console log 到 terminal。
    /// </summary>
    public static class ConsoleLogger
    {
        #region Private Fields

        /// <summary>非同步作業的鎖定物件
        /// </summary>
        /// <returns></returns>
        private static readonly AsyncLock _asyncLock = new();

        #endregion Private Fields


        #region Public Methods

        /// <summary>主控台訊息輸出，均為對 Console.WriteLine 採用 AsyncWorker 進行非同步的標準輸出。<para/>
        /// 1. 預設輸出訊息會換行。<para/>
        /// 2. 預設字體顏色為灰色，可自行指定訊息的字體顏色。<para/>
        /// </summary>
        /// <param name="message">輸出訊息</param>
        /// <param name="color">訊息字體顏色</param>
        public static void WriteLine(string message, ConsoleColor color = ConsoleColor.Gray)
        {
            try
            {
                if (message.IsNullOrEmpty())
                {
                    return;
                }

                using (_asyncLock.Lock())
                {
                    var currentColor = Console.ForegroundColor;
                    Console.ForegroundColor = color;
                    Console.WriteLine(message);
                    Console.ForegroundColor = currentColor;
                }
            }
            catch (Exception)
            {
                Console.WriteLine(message);
                return;
            }
        }

        /// <summary>主控台訊息輸出，均為對 Console.WriteLine 採用 AsyncWorker 進行非同步的標準輸出。<para/>
        /// 1. 預設輸出訊息會換行。<para/>
        /// 2. 預設字體顏色為灰色，可自行指定訊息的字體顏色。<para/>
        /// </summary>
        /// <param name="message">輸出訊息</param>
        /// <param name="color">訊息字體顏色</param>
        public static async Task WriteLineAsync(string message, ConsoleColor color = ConsoleColor.Gray)
        {
            try
            {
                if (message.IsNullOrEmpty())
                {
                    return;
                }

                using (await _asyncLock.LockAsync())
                {
                    var currentColor = Console.ForegroundColor;
                    Console.ForegroundColor = color;
                    await Console.Out.WriteLineAsync(message);
                    Console.ForegroundColor = currentColor;
                }
            }
            catch
            {
                await Console.Out.WriteLineAsync(message);
                return;
            }
        }

        /// <summary>主控台日誌訊息輸出。<para/>
        /// 1. 預設輸出訊息會換行。<para/>
        /// 2. 預設字體顏色為灰色，可自行指定訊息的字體顏色。<para/>
        /// 3. 強制輸出訊息時，統一格式，包括輸出的時間，格式為 yyyy-MM-dd HH:mm:ss.fff。<para/>
        /// </summary>
        /// <param name="message">日誌訊息</param>
        /// <param name="color">訊息字體顏色</param>
        public static void Log(string message, ConsoleColor color = ConsoleColor.Gray) => WriteLog(message, color);

        /// <summary>主控台日誌訊息輸出。<para/>
        /// 1. 預設輸出訊息會換行。<para/>
        /// 2. 預設字體顏色為灰色，可自行指定訊息的字體顏色。<para/>
        /// 3. 強制輸出訊息時，統一格式，包括輸出的時間，格式為 yyyy-MM-dd HH:mm:ss.fff。<para/>
        /// </summary>
        /// <param name="message">日誌訊息</param>
        /// <param name="color">訊息字體顏色</param>
        public static async Task LogAsync(string message, ConsoleColor color = ConsoleColor.Gray) => await WriteLogAsync(message, color);

        /// <summary>主控台錯誤日誌訊息輸出，強制使用「紅色」(ConsoleColor.Red) 字體輸出。
        /// </summary>
        /// <param name="message">錯誤日誌訊息</param>
        public static void LogError(string message) => Log(message, ConsoleColor.Red);

        /// <summary>主控台錯誤日誌訊息輸出，強制使用「紅色」(ConsoleColor.Red) 字體輸出。
        /// </summary>
        /// <param name="message">錯誤日誌訊息</param>
        public static async Task LogErrorAsync(string message) => await LogAsync(message, ConsoleColor.Red);

        #endregion Public Methods


        #region Private Methods

        /// <summary>主控台日誌訊息輸出。<para/>
        /// 1. 預設輸出訊息會換行。<para/>
        /// 2. 預設字體顏色為灰色，可自行指定訊息的字體顏色。<para/>
        /// 3. 強制輸出訊息時，統一格式，包括輸出的時間，格式為 yyyy-MM-dd HH:mm:ss.fff。<para/>
        /// </summary>
        /// <param name="message">輸出訊息</param>
        /// <param name="color">訊息字體顏色</param>
        private static void WriteLog(string message, ConsoleColor color = ConsoleColor.Gray) =>
            WriteLine($"[{DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture)}] {message}", color);

        /// <summary>主控台日誌訊息輸出。<para/>
        /// 1. 預設輸出訊息會換行。<para/>
        /// 2. 預設字體顏色為灰色，可自行指定訊息的字體顏色。<para/>
        /// 3. 強制輸出訊息時，統一格式，包括輸出的時間，格式為 yyyy-MM-dd HH:mm:ss.fff。<para/>
        /// </summary>
        /// <param name="message">輸出訊息</param>
        /// <param name="color">訊息字體顏色</param>
        private static async Task WriteLogAsync(string message, ConsoleColor color = ConsoleColor.Gray) =>
            await WriteLineAsync($"[{DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture)}] {message}", color);

        #endregion Private Methods
    }
}
