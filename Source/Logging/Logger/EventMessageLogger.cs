﻿using NeoSmart.AsyncLock;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Common.Tasks;


namespace ZayniFramework.Logging
{
    /// <summary>預設的訊息日誌記錄器
    /// </summary>
    public sealed class EventMessageLogger
    {
        #region Private Fields

        /// <summary>私有 DefaultLogger 實體
        /// </summary>
        private static EventMessageLogger _instance;

        /// <summary>非同步作業鎖定物件
        /// </summary>
        private static readonly AsyncLock _asyncLockInstance = new();

        /// <summary>非同步作業鎖定物件: 鎖定 _logPaths 字典
        /// </summary>
        /// <returns></returns>
        private static readonly AsyncLock _asyncLock = new();

        /// <summary>日誌檔案路徑集合<para/>
        /// * Key 值: 格式為 {日誌物件設定名稱}###FilePath###<para/>
        /// * Value 值: LogFilePathInfo 物件<para/>
        /// </summary>
        private readonly Dictionary<string, LogFilePathOption> _logPaths = [];

        /// <summary>預設的文字日誌路徑，預設路徑為:<para/>
        /// * ./zayni/zayni.log
        /// </summary>
        private const string DEFAULT_PATH = @"./zayni/zayni.log";

        /// <summary>非同步日誌工作佇列處理器
        /// </summary>
        /// <returns></returns>
        private static readonly AsyncWorker _loggerWorker = AsyncWorkerContainer.Resolve("ZayniFrameworkLoggerAsyncWorker");

        #endregion Private Fields


        #region Constructors

        /// <summary>靜態建構子
        /// </summary>
        static EventMessageLogger() => _loggerWorker.Start();

        /// <summary>私有建構子
        /// </summary>
        private EventMessageLogger() { }

        /// <summary>解構子
        /// </summary>
        ~EventMessageLogger() => _logPaths.Clear();

        #endregion Constructors


        #region Public Properties

        /// <summary>公開的獨體物件
        /// </summary>
        public static EventMessageLogger Instance
        {
            get
            {
                using (_asyncLockInstance.Lock())
                {
                    if (_instance.IsNull())
                    {
                        _instance = new EventMessageLogger();
                        return _instance;
                    }

                    return _instance;
                }
            }
        }

        #endregion Public Properties


        #region Private Methods

        /// <summary>檢查是否可以執行寫入日誌
        /// </summary>
        /// <param name="enable">文字日誌記錄資訊</param>
        /// <param name="logInfo">文字日誌記錄資訊</param>
        /// <returns>是否可以執行寫入日誌</returns>
        private static bool CanLog(bool enable, LogOption logInfo)
        {
            if (!enable)
            {
                return false;
            }

            // 20140201 Pony Says: 檢查目前要寫入的日誌事件層級是否有在 zayni.json 設定中被過濾掉，沒有過濾掉的事件層級，才真正執行日誌寫入
            if (logInfo.LogEventFilters.IsNotNullOrEmpty() && logInfo.LogEventFilters.Contains(logInfo.EventType.ToString()))
            {
                return false;
            }

            return true;
        }

        /// <summary>取得目前日誌檔案的完整路徑
        /// </summary>
        /// <returns>目前日誌檔案的完整路徑</returns>
        private string GetCurrentPath(string loggerName)
        {
            try
            {
                var lastestTime = _logPaths.Values.Max(k => k.CreateTime);
                var key = _logPaths
                    .Where(m => m.Value.LoggerName == loggerName)
                    .Where(q => q.Value.CreateTime == lastestTime)
                    .Select(y => y.Key)
                    .FirstOrDefault();

                return _logPaths[key].FullPath;
            }
            catch (Exception)
            {
                try
                {
                    return _logPaths[$"{loggerName}###FilePath###"].FullPath;
                }
                catch (Exception)
                {
                    return DEFAULT_PATH;
                }
            }
        }

        /// <summary>檢查是否需要將 Log File 寫入到自動產生亂數檔名機制
        /// </summary>
        /// <param name="logFilePath">日誌檔案的完整路徑</param>
        /// <param name="logFileSize">目前 Log 檔案的大小 (單位為 MB)</param>
        /// <param name="maxSize">Log 檔案大小的上限值 (單位為 MB)</param>
        /// <returns>是否需要將 Log File 寫入到自動產生亂數檔名機制</returns>
        private static bool IsNeedWriteToOtherFile(string logFilePath, decimal logFileSize, decimal maxSize)
        {
            return logFileSize >= maxSize || FileHelper.IsFileLocked(logFilePath);
        }

        /// <summary>加入收件人電子郵件地址
        /// </summary>
        /// <param name="targetMail">目標 MailMessage 物件</param>
        /// <param name="toAddresses">電子郵件地址字串</param>
        private static void AddMailAddress(MailMessage targetMail, string toAddresses)
        {
            var addresses = toAddresses.SplitString(";", true);

            foreach (var address in addresses)
            {
                if (address.IsNullOrEmpty())
                {
                    continue;
                }

                targetMail.To.Add(address);
            }
        }

        #endregion Private Methods


        #region Public Methods

        /// <summary>寫入日誌訊息紀錄
        /// </summary>
        /// <param name="logOption">日誌記錄資訊</param>
        public void WriteLog(LogOption logOption)
        {
            #region Pipe to file log

            When.True(CanLog(logOption.IsEnable, logOption), () => _loggerWorker.Enqueue(async () =>
            {
                using (await _asyncLock.LockAsync())
                {
                    try
                    {
                        var logPath = logOption.LogPath;
                        var dir = logPath.IsNullOrEmpty() ? DEFAULT_PATH : Path.GetDirectoryName(logPath);
                        var fileName = new FileInfo(logPath).Name;
                        var fPath = $"{logOption.Name}###FilePath###";

                        if (!_logPaths.ContainsKey(fPath))
                        {
                            _logPaths.Add(fPath, new LogFilePathOption() { CreateTime = DateTime.UtcNow, FullPath = logPath, LoggerName = logOption.Name });
                        }

                        StreamWriter streamWriter = null;

                        var now = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff");
                        var newLine = Environment.NewLine;
                        var separator = "==============================";
                        var time = $"Event Time: {now}{newLine}";
                        var level = $"Event Level: {logOption.EventType}{newLine}";
                        var title = $"Event Title: {logOption.Title}{newLine}";
                        var message = $"Event Message: {LogMessageMaker.MakeExceptionEventLogMessage(logOption)}{newLine}";
                        var messageContent = $"{time}{level}{title}{message}{newLine}{separator}";

                        logPath = GetCurrentPath(logOption.Name);

                        if (File.Exists(logPath))
                        {
                            var f = new FileInfo(logPath);

                            var logFileMBSize = f.Length / 1024.0M / 1024.0M;
                            var maxMBSize = logOption.LogFileMaxSize;

                            // 20150828 Edited by Pony: 如果檢查到目前要寫入 log 的檔案，已經被其他的 Process 或 Thread 的 IO 串流咬住，一樣也要自動可以寫到另外產生的亂數檔名 Log File
                            if (IsNeedWriteToOtherFile(logPath, logFileMBSize, maxMBSize))
                            {
                                var randomId = RandomTextHelper.Create(6);
                                var nowTime = DateTime.UtcNow;
                                logPath = $@"{dir}{Path.DirectorySeparatorChar}{DateTime.UtcNow:yyyy-MM-dd HH-mm-ss-fffffff}-{fileName}";
                                _logPaths.Add(randomId, new LogFilePathOption() { CreateTime = nowTime, FullPath = logPath, LoggerName = logOption.Name });
                            }

                            streamWriter = File.AppendText(logPath);
                        }
                        else
                        {
                            Directory.Exists(dir).WhenFalse(() => Directory.CreateDirectory(dir));
                            streamWriter = new StreamWriter(logPath);
                        }

                        await streamWriter.WriteLineAsync(messageContent);
                        await streamWriter.WriteLineAsync();
                        await streamWriter.FlushAsync();
                        streamWriter.Close();
                        streamWriter.Dispose();
                    }
                    catch (Exception ex)
                    {
                        await ConsoleLogger.LogErrorAsync($"Write file log occur exception. {ex}");
                        await ConsoleLogger.LogAsync($"FileLogger Original Log:{Environment.NewLine}{JsonConvert.SerializeObject(logOption, Formatting.Indented)}");
                        return;
                    }
                }
            }));

            #endregion Pipe to file log

            #region Pipe to console log

            When.True(CanLog(logOption.EnableConsoleOutput, logOption), async () =>
            {
                try
                {
                    var msgSeparator = "==============================";
                    var newLine = Environment.NewLine;
                    var eventLevel = $"Event Level: {logOption.EventType}{newLine}";
                    var eventTitle = $"Event Title: {logOption.Title}{newLine}";
                    var eventMsg = $"Event Message: {LogMessageMaker.MakeExceptionEventLogMessage(logOption)}{newLine}";
                    var msgContent = $"{newLine}{eventLevel}{eventTitle}{eventMsg}{newLine}{msgSeparator}{newLine}";

                    var outputCallback = logOption.EventType == EventLogEntryType.Error ?
                        (Func<Task>)(async () => await ConsoleLogger.LogErrorAsync(msgContent)) :
                        (Func<Task>)(async () => await ConsoleLogger.LogAsync(msgContent, color: logOption.ConsoleOutpotColor.ParseEnum<ConsoleColor>()));

                    await outputCallback();
                }
                catch
                {
                    // 如果日誌訊息輸出發生異常，還是要讓後續的 Logging 機制可以正常運行，因此不會再此 return 掉。
                }
            });

            #endregion Pipe to console log

            #region Pipe to relational database

            var dbLogEnable = logOption.DbLoggerName.IsNotNullOrEmpty() && logOption.DbLoggerType.IsNotNullOrEmpty();

            When.True(CanLog(dbLogEnable, logOption), () =>
            {
                var dao = EventLogDaoFactory.Create(logOption.DbLoggerName, logOption.DbLoggerType);

                _ = _loggerWorker.Enqueue(async () =>
                {
                    _ = await Proxy.InvokeAsync(
                        async () => await dao?.InsertLogAsync(logOption),
                        retryOption: new RetryOption { EnableRetry = true, RetryFrequencies = [TimeSpan.FromMilliseconds(500), TimeSpan.FromMilliseconds(500),] },
                        retryWhen: y => !y
                    );
                });
            });

            #endregion Pipe to relational database

            #region Pipe to Elasticsearch service

            var esLogEnable = logOption.ESLoggerName.IsNotNullOrEmpty();

            When.True(CanLog(esLogEnable, logOption), async () =>
            {
                var esEntry = new ElasticsearchLogEntry()
                {
                    LogTraceId = logOption.LogtraceId,
                    LoggerName = logOption.Name,
                    EventLevel = logOption.EventType.ToString(),
                    EventTitle = logOption.Title,
                    EventMessage = LogMessageMaker.MakeExceptionEventLogMessage(logOption),
                    Host = await GetHostIpAddressAsync(),
                    LogTime = DateTime.UtcNow
                };

                var esLogger = ElasticsearchLoggerContainer.Get(logOption.ESLoggerName);
                _loggerWorker.Enqueue(async () => await esLogger?.InsertLogAsync(esEntry));
            });

            #endregion Pipe to Elasticsearch service

            #region Pipe to SMTP service for email notification

            When.True(CanLog(logOption.EmailNotifyLoggerName.IsNotNullOrEmpty(), logOption), () => _loggerWorker.Enqueue(async () =>
            {
                try
                {
                    #region 取得 EmailNotifyLogger 日誌設定

                    var r = LoggingSettingLoader.GetEmailNotifyLoggerSetting(logOption.EmailNotifyLoggerName);

                    if (!r.Success)
                    {
                        return;
                    }

                    var emailLoggerCfg = r.Data;

                    if (emailLoggerCfg.IsNull())
                    {
                        return;
                    }

                    var smtpAccount = emailLoggerCfg.SmtpAccount;
                    var smtpPassword = emailLoggerCfg.SmtpPassword;

                    #endregion 取得 EmailNotifyLogger 日誌設定

                    #region 日誌訊息格式

                    var now = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff");
                    var newLine = Environment.NewLine;
                    var time = $"Event Time: {now}{newLine}";
                    var level = $"Event Level: {logOption.EventType}{newLine}";
                    var title = $"Event Title: {logOption.Title}{newLine}";
                    var message = $"Event Message: {LogMessageMaker.MakeExceptionEventLogMessage(logOption)}{newLine}";
                    var messageContent = $"{logOption.Name}{newLine}{newLine}{time}{level}{title}{message}{newLine}";

                    #endregion 日誌訊息格式

                    #region 初始化 SmtpClient

                    var smtp = new SmtpClient
                    {
                        Port = emailLoggerCfg.SmtpPort,
                        Host = emailLoggerCfg.SmtpHost,
                        EnableSsl = emailLoggerCfg.EnableSsl
                    };

                    if (smtpAccount.IsNotNullOrEmpty() && smtpPassword.IsNotNullOrEmpty())
                    {
                        smtp.Credentials = new NetworkCredential()
                        {
                            Domain = emailLoggerCfg.SmtpDomain.IsNotNullOrEmpty() ? emailLoggerCfg.SmtpDomain : null,
                            UserName = smtpAccount,
                            Password = smtpPassword
                        };
                    }
                    else
                    {
                        smtp.UseDefaultCredentials = true;
                    }

                    #endregion 初始化 SmtpClient

                    #region 初始化 MailMessage 訊息

                    var mailMessage = new MailMessage()
                    {
                        From = emailLoggerCfg.FromDisplayName.IsNullOrEmpty() ? new MailAddress(emailLoggerCfg.FromEmailAddress) : new MailAddress(emailLoggerCfg.FromEmailAddress, emailLoggerCfg.FromDisplayName, Encoding.UTF8),
                        Subject = logOption.Title,
                        Body = messageContent
                    };

                    AddMailAddress(mailMessage, emailLoggerCfg.ToEmailAddress);

                    #endregion 初始化 MailMessage 訊息

                    #region 寄送日誌訊息至 Smtp Server 服務

                    try
                    {
                        await Proxy.InvokeAsync(
                            async () => await smtp.SendMailAsync(mailMessage),
                            retryOption: new RetryOption { EnableRetry = true, RetryFrequencies = [TimeSpan.FromSeconds(5), TimeSpan.FromSeconds(5),] }
                        );
                    }
                    catch (SmtpException smtpEx)
                    {
                        await ConsoleLogger.LogErrorAsync($"Send email message log to SMTP server occur {nameof(SmtpException)}. {smtpEx}");
                        await ConsoleLogger.LogAsync($"SMTP service notification. Original Log:{Environment.NewLine}{JsonConvert.SerializeObject(logOption, Formatting.Indented)}");
                        return;
                    }
                    catch (Exception ex)
                    {
                        await ConsoleLogger.LogErrorAsync($"Send email message log to SMTP server occur exception. {ex}");
                        await ConsoleLogger.LogAsync($"SMTP service notification. Original Log:{Environment.NewLine}{JsonConvert.SerializeObject(logOption, Formatting.Indented)}");
                        return;
                    }

                    #endregion 寄送日誌訊息至 Smtp Server 服務
                }
                catch (Exception ex)
                {
                    ConsoleLogger.LogError($"Process email notify log message occur exception. {ex.Message}");
                    ConsoleLogger.Log($"SMTP service notification. Original Log:{Environment.NewLine}{JsonConvert.SerializeObject(logOption, Formatting.Indented)}");
                    return;
                }
            }));

            #endregion Pipe to SMTP service for email notification
        }

        #endregion Public Methods


        #region Pirvate Methods

        /// <summary>嘗試取得主機的 IP 位址<para/>
        /// * 如果可以正常取得到內部 IP 位址，則優先回傳內部網路的 IP 位址。<para/>
        /// * 如果內部網路的 IP 位址無法取得到，則嘗試取得到公開網際網路上的 IP 位址<para/>
        /// * 假設以上的 IP 位址都無法正常取得，則會回傳 127.0.0.1。
        /// </summary>
        /// <returns></returns>
        private static async Task<string> GetHostIpAddressAsync()
        {
            var address = IPAddressHelper.GetLocalIPAddress();

            if (address.IsNotNullOrEmpty() && address != "127.0.0.1" && address != "localhost")
            {
                return address;
            }

            address = await IPAddressHelper.GetExternalIPAddressAsync();

            if (address.IsNotNullOrEmpty() && address != "127.0.0.1" && address != "localhost")
            {
                return address;
            }

            return "127.0.0.1";
        }

        #endregion Pirvate Methods
    }
}
