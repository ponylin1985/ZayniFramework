using Newtonsoft.Json;
using System;


namespace ZayniFramework.Logging
{
    /// <summary>ElasticSearch 日誌訊息載體
    /// </summary>

    public class ElasticsearchLogEntry
    {
        /// <summary>日誌追蹤器的流水編號
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "log_trace_id")]
        public string LogTraceId { get; set; }

        /// <summary>日誌流水號
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "event_id")]
        public string EventId { get; set; }

        /// <summary>日誌器名稱
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "logger_name")]
        public string LoggerName { get; set; }

        /// <summary>事件層級
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "event_level")]
        public string EventLevel { get; set; }

        /// <summary>事件標題
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "event_title")]
        public string EventTitle { get; set; }

        /// <summary>事件訊息內容
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "event_message")]
        public string EventMessage { get; set; }

        /// <summary>事件觸發的主機位址
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "host")]
        public string Host { get; set; }

        /// <summary>日治事件名稱
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "log_time")]
        public DateTime LogTime { get; set; } = DateTime.UtcNow;
    }

    /// <summary>FS_EVENT_LOG 事件日誌
    /// </summary>

    internal class fs_event_log : ElasticsearchLogEntry
    {
    }
}