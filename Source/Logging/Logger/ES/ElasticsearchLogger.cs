using Elasticsearch.Net;
using Nest;
using Nest.JsonNetSerializer;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using ZayniFramework.Common;


namespace ZayniFramework.Logging
{
    /// <summary>Elasticsearch 日誌器
    /// </summary>
    public class ElasticsearchLogger
    {
        #region Static Members

        /// <summary>所有 ElasticsearchLogger 客戶端 Config 區段設定集合
        /// </summary>
        private static readonly List<ElasticStackLoggerSetting> _configCollection;

        /// <summary>靜態建構子
        /// </summary>
        /// <returns></returns>
        static ElasticsearchLogger()
        {
            _configCollection =
                LoggingSettingLoader
                    .GetLoggingModuleConfigs()?
                    .ElasticStackLoggerSettings;
        }


        #endregion Static Members


        #region Private Fields

        /// <summary>日誌訊息寫入重試選項
        /// </summary>
        private readonly IRetryOption _retryOption;

        /// <summary>ElasticsearchLogger 客戶端實體 Config 設定元素
        /// </summary>
        private readonly ElasticStackLoggerSetting _esLoggerConfig;

        /// <summary>Elasticsearch 服務的 Uri 位址
        /// </summary>
        private readonly Uri _esConnString;

        /// <summary>Elasticsearch 服務預設的 Index 名稱
        /// </summary>
        private readonly string _esDefaultIndex;

        /// <summary>Elasticsearch 客戶端連線設定物件
        /// </summary>
        private readonly ConnectionSettings _esConnSettings;

        /// <summary>Elasticsearch 客戶端實體
        /// </summary>
        private readonly ElasticClient _esClient;

        #endregion Private Fields


        #region Constructors

        /// <summary>預設建構子
        /// </summary>
        public ElasticsearchLogger()
        {
            foreach (var config in _configCollection)
            {
                if (config.IsDefault)
                {
                    _esLoggerConfig = config;
                    _esConnString = new Uri(config.HostUrl);
                    _esDefaultIndex = config.DefaultIndex;
                    _esConnSettings = new ConnectionSettings(new SingleNodeConnectionPool(_esConnString), sourceSerializer: JsonNetSerializer.Default).DefaultIndex(_esDefaultIndex);
                    _esClient = new ElasticClient(_esConnSettings);
                    break;
                }
            }

            if (_esLoggerConfig.IsNull())
            {
                throw new ConfigurationErrorsException($"No default ElasticsearchLogger found in zayni.json file's ElasticsearchLogger config section.");
            }

            var retryFrequencies = new List<TimeSpan>();
            For.Do(_esLoggerConfig.RetryTimes, () => retryFrequencies.Add(TimeSpan.FromSeconds(1)));

            _retryOption = new RetryOption
            {
                EnableRetry = _esLoggerConfig.EnableRetry,
                RetryFrequencies = retryFrequencies.ToArray(),
            };
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="name">日誌器設定名稱</param>
        public ElasticsearchLogger(string name)
        {
            foreach (var config in _configCollection)
            {
                if (config.Name == name)
                {
                    _esLoggerConfig = config;
                    _esConnString = new Uri(config.HostUrl);
                    _esDefaultIndex = config.DefaultIndex;
                    _esConnSettings = new ConnectionSettings(new SingleNodeConnectionPool(_esConnString), sourceSerializer: JsonNetSerializer.Default).DefaultIndex(_esDefaultIndex);
                    _esClient = new ElasticClient(_esConnSettings);
                    break;
                }
            }

            if (_esLoggerConfig.IsNull())
            {
                throw new ConfigurationErrorsException($"No ElasticsearchLogger found in zayni.json file's ElasticsearchLogger config section. Name: {name}.");
            }

            var retryFrequencies = new List<TimeSpan>();
            For.Do(_esLoggerConfig.RetryTimes, () => retryFrequencies.Add(TimeSpan.FromSeconds(1)));

            _retryOption = new RetryOption
            {
                EnableRetry = _esLoggerConfig.EnableRetry,
                RetryFrequencies = retryFrequencies.ToArray(),
            };
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="hostUri">Elasticsearch 服務的 URL 位址</param>
        /// <param name="defaultIndex">Elasticsearch 服務預設的 Index 名稱</param>
        public ElasticsearchLogger(string hostUri, string defaultIndex = "zayni")
        {
            _esConnString = new Uri(hostUri);
            _esDefaultIndex = defaultIndex;
            _esConnSettings = new ConnectionSettings(new SingleNodeConnectionPool(_esConnString), sourceSerializer: JsonNetSerializer.Default).DefaultIndex(_esDefaultIndex);
            _esClient = new ElasticClient(_esConnSettings);
        }

        #endregion Constructors


        #region Public Methods

        /// <summary>寫入日誌訊息到 Elasticsearch 服務中
        /// </summary>
        /// <param name="logEntry">Elasticsearch 日誌訊息載體</param>
        public virtual void InsertLog(ElasticsearchLogEntry logEntry)
        {
            try
            {
                if (logEntry.IsNull())
                {
                    return;
                }

                var model = new fs_event_log()
                {
                    EventId = RandomTextHelper.Create(25),
                    LoggerName = logEntry.LoggerName,
                    EventLevel = logEntry.EventLevel,
                    EventTitle = logEntry.EventTitle,
                    EventMessage = logEntry.EventMessage,
                    Host = logEntry.Host,
                    LogTime = logEntry.LogTime
                };

                Proxy
                    .Invoke(func: () => Insert(), retryOption: _retryOption, retryWhen: q => !q)
                    .When(r => !r, s => ConsoleLogger.Log($"{nameof(ElasticsearchLogger)} Original Log:{Environment.NewLine}{JsonConvert.SerializeObject(logEntry, Formatting.Indented)}"));

                bool Insert()
                {
                    if (!_esClient.Indices.Exists(_esDefaultIndex).Exists)
                    {
                        var i = _esClient.Indices.Create(
                            _esDefaultIndex,
                            j => j
                                .Map<fs_event_log>(m => m.AutoMap())
                        );

                        if (!i.IsValid)
                        {
                            return false;
                        }
                    }

                    var r = _esClient.Index(model, i => i
                            .Index(_esDefaultIndex)
                            .Id(RandomTextHelper.Create(15))
                            .Refresh(Refresh.WaitFor)
                    );

                    return r.IsValid;
                }
            }
            catch
            {
                ConsoleLogger.Log($"{nameof(ElasticsearchLogger)} Original Log:{Environment.NewLine}{JsonConvert.SerializeObject(logEntry, Formatting.Indented)}");
            }
        }

        /// <summary>寫入日誌訊息到 Elasticsearch 服務中
        /// </summary>
        /// <param name="logEntry">Elasticsearch 日誌訊息載體</param>
        public virtual async Task InsertLogAsync(ElasticsearchLogEntry logEntry)
        {
            try
            {
                if (logEntry.IsNull())
                {
                    return;
                }

                var model = new fs_event_log()
                {
                    LogTraceId = logEntry.LogTraceId,
                    EventId = RandomTextHelper.Create(25),
                    LoggerName = logEntry.LoggerName,
                    EventLevel = logEntry.EventLevel,
                    EventTitle = logEntry.EventTitle,
                    EventMessage = logEntry.EventMessage,
                    Host = logEntry.Host,
                    LogTime = logEntry.LogTime
                };

                var z = await Proxy.InvokeAsync(asyncFunc: InsertAsync, retryOption: _retryOption, retryWhen: q => !q);
                await (z.When(r => !r, async s => await ConsoleLogger.LogAsync($"{nameof(ElasticsearchLogger)} Original Log:{Environment.NewLine}{JsonConvert.SerializeObject(logEntry, Formatting.Indented)}")));

                async Task<bool> InsertAsync()
                {
                    if (!(await _esClient.Indices.ExistsAsync(_esDefaultIndex)).Exists)
                    {
                        var i = await _esClient.Indices.CreateAsync(
                            _esDefaultIndex,
                            j => j.Map<fs_event_log>(m => m.AutoMap())
                        );

                        if (!i.IsValid)
                        {
                            return false;
                        }
                    }

                    var r = await _esClient.IndexAsync(model, i => i
                            .Index(_esDefaultIndex)
                            .Id(RandomTextHelper.Create(15))
                            .Refresh(Refresh.WaitFor)
                    );

                    return r.IsValid;
                }
            }
            catch
            {
                await ConsoleLogger.LogAsync($"{nameof(ElasticsearchLogger)} Original Log:{Environment.NewLine}{JsonConvert.SerializeObject(logEntry, Formatting.Indented)}");
            }
        }

        /// <summary>寫入資料到 Elasticsearch 服務中，在 zayni.json 設定的 DefaultIndex 中。<para/>
        /// * 傳入的 model 文件資料模型，Property 必須宣告為 public string 型別的 Property。<para/>
        /// * 傳入的 model 文件資料模型，Property 允許標記 Nest 套件支援的 [Text]、[Number]、[Date] 或 [Nested] 屬性標籤。<para/>
        /// * 傳入的 model 文件資料模型，Property 允許標記 Newtonsoft.Json 套件的 [JsonProperty] 屬性，可使用 System.Text.Json 提供的 [JsonPropertyName]，但對 ES 的操作會以 Newtonsoft.Json 進行 JSON 序列化操作。<para/>
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <param name="id">資料 id 值</param>
        /// <typeparam name="TModel">資料模型的泛型</typeparam>
        public virtual void InsertLog<TModel>(TModel model, string id)
            where TModel : class
        {
            try
            {
                if (model.IsNull())
                {
                    return;
                }

                Proxy
                    .Invoke(func: () => Insert(), retryOption: _retryOption, retryWhen: q => !q)
                    .When(r => !r, s => ConsoleLogger.Log($"{nameof(ElasticsearchLogger)} Original Log:{Environment.NewLine}{JsonConvert.SerializeObject(model, Formatting.Indented)}"));

                bool Insert()
                {
                    if (!_esClient.Indices.Exists(_esDefaultIndex).Exists)
                    {
                        var i = _esClient.Indices.Create(
                            _esDefaultIndex,
                            j => j.Map<TModel>(m => m.AutoMap())
                        );

                        if (!i.IsValid)
                        {
                            return false;
                        }
                    }

                    var r = _esClient.Index(model, i => i
                        .Index(_esDefaultIndex)
                        .Id(id)
                        .Refresh(Refresh.WaitFor)
                    );

                    return r.IsValid;
                }
            }
            catch
            {
                ConsoleLogger.Log($"{nameof(ElasticsearchLogger)} Original Log:{Environment.NewLine}{JsonConvert.SerializeObject(model, Formatting.Indented)}");
            }
        }

        /// <summary>寫入資料到 Elasticsearch 服務中，在 zayni.json 設定的 DefaultIndex 中。<para/>
        /// * 傳入的 model 文件資料模型，Property 必須宣告為 public string 型別的 Property。<para/>
        /// * 傳入的 model 文件資料模型，Property 允許標記 Nest 套件支援的 [Text]、[Number]、[Date] 或 [Nested] 屬性標籤。<para/>
        /// * 傳入的 model 文件資料模型，Property 允許標記 Newtonsoft.Json 套件的 [JsonProperty] 屬性，可使用 System.Text.Json 提供的 [JsonPropertyName]，但對 ES 的操作會以 Newtonsoft.Json 進行 JSON 序列化操作。<para/>
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <param name="id">資料 id 值</param>
        /// <typeparam name="TModel">資料模型的泛型</typeparam>
        public virtual async Task InsertLogAsync<TModel>(TModel model, string id)
            where TModel : class
        {
            try
            {
                if (model.IsNull())
                {
                    return;
                }

                var z = await Proxy.InvokeAsync(asyncFunc: InsertAsync, retryOption: _retryOption, retryWhen: q => !q);
                await (z.When(r => !r, async s => await ConsoleLogger.LogAsync($"{nameof(ElasticsearchLogger)} Original Log:{Environment.NewLine}{JsonConvert.SerializeObject(model, Formatting.Indented)}")));

                async Task<bool> InsertAsync()
                {
                    if (!(await _esClient.Indices.ExistsAsync(_esDefaultIndex)).Exists)
                    {
                        var i = await _esClient.Indices.CreateAsync(
                            _esDefaultIndex,
                            j => j.Map<TModel>(m => m.AutoMap())
                        );

                        if (!i.IsValid)
                        {
                            return false;
                        }
                    }

                    var r = await _esClient.IndexAsync(model, i => i
                        .Index(_esDefaultIndex)
                        .Id(id)
                        .Refresh(Refresh.WaitFor)
                    );

                    return r.IsValid;
                }
            }
            catch
            {
                await ConsoleLogger.LogAsync($"{nameof(ElasticsearchLogger)} Original Log:{Environment.NewLine}{JsonConvert.SerializeObject(model, Formatting.Indented)}");
            }
        }

        /// <summary>清除掉預設 Elasticsearch 服務 index 中所有的 log 日誌
        /// </summary>
        public virtual void ClearLog()
        {
            try
            {
                if (_esDefaultIndex.IsNullOrEmpty() || _esClient.IsNull())
                {
                    return;
                }

                if (!_esClient.Indices.Exists(_esDefaultIndex).Exists)
                {
                    return;
                }

                _esClient.Indices.Delete(_esDefaultIndex);
            }
            catch (Exception ex)
            {
                ConsoleLogger.LogError($"Clear log messages from elasticsearch service occur error. {ex}");
            }
        }

        /// <summary>清除掉預設 Elasticsearch 服務 index 中所有的 log 日誌
        /// </summary>
        public async Task ClearLogAsync_old()
        {
            try
            {
                if (_esDefaultIndex.IsNullOrEmpty() || _esClient.IsNull())
                {
                    return;
                }

                if (!(await _esClient.Indices.ExistsAsync(_esDefaultIndex)).Exists)
                {
                    return;
                }

                await _esClient.Indices.DeleteAsync(_esDefaultIndex);
            }
            catch (Exception ex)
            {
                await ConsoleLogger.LogErrorAsync($"Clear log messages from elasticsearch service occur error. {ex}");
            }
        }

        /// <summary>清除掉預設 Elasticsearch 服務 index 中指定日期之前的 log 日誌。
        /// </summary>
        /// <param name="date">指定的日期 (UTC 時間)</param>
        /// <returns></returns>
        public virtual async Task ClearLogAsync(DateTime date)
        {
            try
            {
                var searchRequest = new SearchRequest<ElasticsearchLogEntry>
                {
                    Query = new DateRangeQuery
                    {
                        Field = "log_time",
                        LessThan = date,
                    },
                    Size = 200,
                };

                while (true)
                {
                    var searchResponse = await _esClient.SearchAsync<ElasticsearchLogEntry>(searchRequest);
                    var documentIds = searchResponse.Hits.Select(hit => hit.Id).ToList();

                    if (documentIds.IsNullOrEmpty())
                    {
                        break;
                    }

                    var deleteRequest = new BulkRequest();

                    foreach (var documentId in documentIds)
                    {
                        var deleteResponse = await _esClient.DeleteAsync<ElasticsearchLogEntry>(documentId);

                        if (!deleteResponse.IsValid)
                        {
                            await ConsoleLogger.LogAsync($"An error occured while try to delete the log document. Index: {_esDefaultIndex}, _id: {documentId}.", ConsoleColor.Magenta);
                            continue;
                        }

                        // 等待非常重要! 原因是因為 elasticsearch 是全文檢索用的 NoSQL，當 delete 請求回應合法時，只是接受了這個請求，實際上 elasticsearch 真的刪除這筆資料會需要一點時間。
                        // 如果完全沒有等待，很可能會被下一輪的 SearchAsync 查詢出來，進行重複刪除的操作，導致之後重複操作 delete 的動作被 elasticsearch 拒絕!
                        await Task.Delay(TimeSpan.FromMilliseconds(1000 * 1.5));
                    }
                }
            }
            catch (Exception ex)
            {
                await ConsoleLogger.LogErrorAsync($"An error occur while clearing log messages from elasticsearch service.{Environment.NewLine}{ex}");
                throw;
            }
        }

        #endregion Public Methods
    }

    /// <summary>ElasticsearchLogger 空類別
    /// </summary>
    public class NullElasticSearchLogger : ElasticsearchLogger
    {
        #region Constructors

        /// <summary>預設建構子
        /// </summary>
        public NullElasticSearchLogger()
        {
        }

        #endregion Constructors


        #region Override Methods

        /// <summary>寫入日誌訊息到 Elasticsearch 服務中
        /// </summary>
        /// <param name="logEntry">Elasticsearch 日誌訊息載體</param>
        public override void InsertLog(ElasticsearchLogEntry logEntry) { }

        /// <summary>寫入日誌訊息到 Elasticsearch 服務中
        /// </summary>
        /// <param name="logEntry">Elasticsearch 日誌訊息載體</param>
        public override Task InsertLogAsync(ElasticsearchLogEntry logEntry) => Task.FromResult(0);

        /// <summary>寫入資料到 Elasticsearch 服務中，在 zayni.json 設定的 DefaultIndex 中。
        /// * 傳入的 model 文件資料模型，Property 必須宣告為 public string 型別的 Property。
        /// * 傳入的 model 文件資料模型，Property 允許標記 Nest 套件支援的 [Text]、[Number]、[Date] 或 [Nested] 屬性標籤。
        /// * 傳入的 model 文件資料模型，Property 允許標記 Newtonsoft.Json 套件的 [JsonProperty] 屬性，可使用 System.Text.Json 提供的 [JsonPropertyName]，但對 ES 的操作會以 Newtonsoft.Json 進行 JSON 序列化操作。
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <param name="id">資料 id 值</param>
        /// <typeparam name="TModel">資料模型的泛型</typeparam>
        public override void InsertLog<TModel>(TModel model, string id) { }

        /// <summary>寫入資料到 Elasticsearch 服務中，在 zayni.json 設定的 DefaultIndex 中。
        /// * 傳入的 model 文件資料模型，Property 必須宣告為 public string 型別的 Property。
        /// * 傳入的 model 文件資料模型，Property 允許標記 Nest 套件支援的 [Text]、[Number]、[Date] 或 [Nested] 屬性標籤。
        /// * 傳入的 model 文件資料模型，Property 允許標記 Newtonsoft.Json 套件的 [JsonProperty] 屬性，可使用 System.Text.Json 提供的 [JsonPropertyName]，但對 ES 的操作會以 Newtonsoft.Json 進行 JSON 序列化操作。
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <param name="id">資料 id 值</param>
        /// <typeparam name="TModel">資料模型的泛型</typeparam>
        public override Task InsertLogAsync<TModel>(TModel model, string id) => Task.FromResult(0);

        /// <summary>清除掉預設 Elasticsearch 服務 index 中所有的 log 日誌
        /// </summary>
        public override void ClearLog() { }

        /// <summary>清除掉預設 Elasticsearch 服務 index 中所有的 log 日誌
        /// </summary>
        public override Task ClearLogAsync(DateTime date) => Task.FromResult(0);

        #endregion Override Methods
    }
}