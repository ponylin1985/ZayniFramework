using Elasticsearch.Net;
using Nest;
using Nest.JsonNetSerializer;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ZayniFramework.Common;


namespace ZayniFramework.Logging
{
    /// <summary>Elasticsearch 服務存取客戶端
    /// </summary>
    public class ElasticsearchBroker
    {
        #region Private Fields

        /// <summary>Elasticsearch 服務的 Uri 位址
        /// </summary>
        private readonly Uri _esConnString;

        /// <summary>Elasticsearch 服務預設的 Index 名稱
        /// </summary>
        private readonly string _esDefaultIndex;

        /// <summary>Elasticsearch 客戶端連線設定物件
        /// </summary>
        private readonly ConnectionSettings _esConnSettings;

        /// <summary>Elasticsearch 客戶端實體
        /// </summary>
        private readonly ElasticClient _esClient;

        #endregion Private Fields


        #region Constructors

        /// <summary>多載建構子
        /// </summary>
        /// <param name="esHostUrl">Elasticsearch 服務的 URL 位址</param>
        /// <param name="defaultIndex">Elasticsearch 服務預設的 Index 名稱，預設為 default_data</param>
        /// <param name="enableActionLog">是否啟用 Elasticsearch NEST 的 action log 日誌紀錄，loggerName 為 ElasticsearchBrokerActionLog，預設為 false。</param>
        public ElasticsearchBroker(string esHostUrl, string defaultIndex = "default_data", bool enableActionLog = false)
        {
            _esConnString = new Uri(esHostUrl);
            _esDefaultIndex = defaultIndex;
            _esConnSettings = new ConnectionSettings(new SingleNodeConnectionPool(_esConnString), sourceSerializer: JsonNetSerializer.Default).DefaultIndex(_esDefaultIndex);

            if (enableActionLog)
            {
                _esConnSettings.DisableDirectStreaming(true);
                _esConnSettings.OnRequestCompleted(ElasticsearchActionLog);
            }

            _esClient = new ElasticClient(_esConnSettings);
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="connectionSettings">NEST Elasticsearch 的連線設定</param>
        /// <param name="defaultIndex">Elasticsearch 服務預設的 Index 名稱，預設為 default_data</param>
        public ElasticsearchBroker(ConnectionSettings connectionSettings, string defaultIndex = "default_data")
        {
            _esConnSettings = connectionSettings;
            _esClient = new ElasticClient(_esConnSettings);
        }

        #endregion Constructors


        #region Public Properties

        /// <summary>Elasticsearch 服務的 Uri 位址
        /// </summary>
        public Uri ESHostUrl => _esConnString;

        /// <summary>Elasticsearch 客戶端連線設定物件
        /// </summary>
        public ConnectionSettings ConnectionSetting => _esConnSettings;

        /// <summary>Elasticsearch 服務預設的 Index 名稱
        /// </summary>
        public string DefaultIndex => _esDefaultIndex;

        /// <summary>Elasticsearch 客戶端實體
        /// </summary>
        public ElasticClient ElasticClient => _esClient;

        #endregion Public Properties


        #region Public Methods

        /// <summary>寫入文件資料模型<para/>
        /// * 傳入的 model 文件資料模型，Property 必須宣告為 public string 型別的 Property。<para/>
        /// * 傳入的 model 文件資料模型，Property 允許標記 Nest 套件支援的 [Text]、[Number]、[Date] 或 [Nested] 屬性標籤。<para/>
        /// * 傳入的 model 文件資料模型，Property 允許標記 Newtonsoft.Json 套件的 [JsonProperty] 屬性，可使用 System.Text.Json 提供的 [JsonPropertyName]，但對 ES 的操作會以 Newtonsoft.Json 進行 JSON 序列化操作。<para/>
        /// </summary>
        /// <param name="id">文件 id 值</param>
        /// <param name="model">文件資料模型</param>
        /// <typeparam name="TModel">資料模型的泛型</typeparam>
        /// <returns>執行結果</returns>
        public IResult Upsert<TModel>(string id, TModel model)
            where TModel : class
        {
            var result = Common.Result.Create();

            if (id.IsNullOrEmpty())
            {
                result.Message = $"The '{nameof(id)}' can not be null or empty string.";
                return result;
            }

            if (model.IsNull())
            {
                result.Message = $"The '{nameof(model)}' can not be null..";
                return result;
            }

            if (!_esClient.Indices.Exists(_esDefaultIndex).Exists)
            {
                var i = _esClient.Indices.Create(_esDefaultIndex);

                if (!i.IsValid)
                {
                    result.Message = $"Upsert document data to Elasticsearch service occur error due to create index to Elasticsearch service occur error. Index Name: {_esDefaultIndex}. {Environment.NewLine}{i.DebugInformation}";
                    return result;
                }
            }

            var r = _esClient.Index(model, i => i
                    .Index(_esDefaultIndex)
                    .Id(id)
                    .Refresh(Refresh.True)
                );

            if (!r.IsValid)
            {
                result.Message = $"Upsert document data to Elasticsearch service occur error. Index Name: {_esDefaultIndex}, Id: {id}. {Environment.NewLine}{r.DebugInformation}";
                return result;
            }

            result.Success = true;
            return result;
        }

        /// <summary>寫入文件資料模型<para/>
        /// * 傳入的 model 文件資料模型，Property 必須宣告為 public string 型別的 Property。<para/>
        /// * 傳入的 model 文件資料模型，Property 允許標記 Nest 套件支援的 [Text]、[Number]、[Date] 或 [Nested] 屬性標籤。<para/>
        /// * 傳入的 model 文件資料模型，Property 允許標記 Newtonsoft.Json 套件的 [JsonProperty] 屬性，可使用 System.Text.Json 提供的 [JsonPropertyName]，但對 ES 的操作會以 Newtonsoft.Json 進行 JSON 序列化操作。<para/>
        /// </summary>
        /// <param name="id">文件 id 值</param>
        /// <param name="model">文件資料模型</param>
        /// <typeparam name="TModel">資料模型的泛型</typeparam>
        /// <returns>執行結果</returns>
        public async Task<IResult> UpsertAsync<TModel>(string id, TModel model)
            where TModel : class
        {
            var result = Common.Result.Create();

            if (id.IsNullOrEmpty())
            {
                result.Message = $"The '{nameof(id)}' can not be null or empty string.";
                return result;
            }

            if (model.IsNull())
            {
                result.Message = $"The '{nameof(model)}' can not be null..";
                return result;
            }

            if (!(await _esClient.Indices.ExistsAsync(_esDefaultIndex)).Exists)
            {
                var i = await _esClient.Indices.CreateAsync(_esDefaultIndex);

                if (!i.IsValid)
                {
                    result.Message = $"Upsert document data to Elasticsearch service occur error due to create index to Elasticsearch service occur error. Index Name: {_esDefaultIndex}. {Environment.NewLine}{i.DebugInformation}";
                    return result;
                }
            }

            var r = await _esClient.IndexAsync(model, i => i
                    .Index(_esDefaultIndex)
                    .Id(id)
                    .Refresh(Refresh.True)
                );

            if (!r.IsValid)
            {
                result.Message = $"Upsert document data to Elasticsearch service occur error. Index Name: {_esDefaultIndex}, Id: {id}. {Environment.NewLine}{r.DebugInformation}";
                return result;
            }

            result.Success = true;
            return result;
        }

        /// <summary>寫入文件資料模型<para/>
        /// * 傳入的 model 文件資料模型，Property 必須宣告為 public string 型別的 Property。<para/>
        /// * 傳入的 model 文件資料模型，Property 允許標記 Nest 套件支援的 [Text]、[Number]、[Date] 或 [Nested] 屬性標籤。<para/>
        /// * 傳入的 model 文件資料模型，Property 允許標記 Newtonsoft.Json 套件的 [JsonProperty] 屬性，可使用 System.Text.Json 提供的 [JsonPropertyName]，但對 ES 的操作會以 Newtonsoft.Json 進行 JSON 序列化操作。<para/>
        /// </summary>
        /// <param name="indexName">Elasticsearch 文件庫 index 名稱</param>
        /// <param name="id">文件 id 值</param>
        /// <param name="model">文件資料模型</param>
        /// <typeparam name="TModel">資料模型的泛型</typeparam>
        /// <returns>執行結果</returns>
        public IResult Upsert<TModel>(string indexName, string id, TModel model)
            where TModel : class
        {
            var result = Common.Result.Create();

            if (id.IsNullOrEmpty())
            {
                result.Message = $"The '{nameof(id)}' can not be null or empty string.";
                return result;
            }

            if (model.IsNull())
            {
                result.Message = $"The '{nameof(model)}' can not be null..";
                return result;
            }

            if (!_esClient.Indices.Exists(indexName).Exists)
            {
                var i = _esClient.Indices.Create(indexName, j => j.Map<TModel>(m => m.AutoMap()));

                if (!i.IsValid)
                {
                    result.Message = $"Upsert document data to Elasticsearch service occur error due to create index to Elasticsearch service occur error. Index Name: {indexName}. {Environment.NewLine}{i.DebugInformation}";
                    return result;
                }
            }

            var r = _esClient.Index(model, i => i
                    .Index(indexName)
                    .Id(id)
                    .Refresh(Refresh.True)
                );

            if (!r.IsValid)
            {
                result.Message = $"Upsert document data to Elasticsearch service occur error. Index Name: {indexName}, Id: {id}. {Environment.NewLine}{r.DebugInformation}";
                return result;
            }

            result.Success = true;
            return result;
        }

        /// <summary>寫入文件資料模型<para/>
        /// * 傳入的 model 文件資料模型，Property 必須宣告為 public string 型別的 Property。<para/>
        /// * 傳入的 model 文件資料模型，Property 允許標記 Nest 套件支援的 [Text]、[Number]、[Date] 或 [Nested] 屬性標籤。<para/>
        /// * 傳入的 model 文件資料模型，Property 允許標記 Newtonsoft.Json 套件的 [JsonProperty] 屬性，可使用 System.Text.Json 提供的 [JsonPropertyName]，但對 ES 的操作會以 Newtonsoft.Json 進行 JSON 序列化操作。<para/>
        /// </summary>
        /// <param name="indexName">Elasticsearch 文件庫 index 名稱</param>
        /// <param name="id">文件 id 值</param>
        /// <param name="model">文件資料模型</param>
        /// <typeparam name="TModel">資料模型的泛型</typeparam>
        /// <returns>執行結果</returns>
        public async Task<IResult> UpsertAsync<TModel>(string indexName, string id, TModel model)
            where TModel : class
        {
            var result = Common.Result.Create();

            if (id.IsNullOrEmpty())
            {
                result.Message = $"The '{nameof(id)}' can not be null or empty string.";
                return result;
            }

            if (model.IsNull())
            {
                result.Message = $"The '{nameof(model)}' can not be null.";
                return result;
            }

            if (!(await _esClient.Indices.ExistsAsync(indexName)).Exists)
            {
                var i = await _esClient.Indices.CreateAsync(indexName, j => j.Map<TModel>(m => m.AutoMap()));

                if (!i.IsValid)
                {
                    result.Message = $"Upsert document data to Elasticsearch service occur error due to create index to Elasticsearch service occur error. Index Name: {indexName}. {Environment.NewLine}{i.DebugInformation}";
                    return result;
                }
            }

            var r = await _esClient.IndexAsync(model, i => i
                    .Index(indexName)
                    .Id(id)
                    .Refresh(Refresh.True)
                );

            if (!r.IsValid)
            {
                result.Message = $"Upsert document data to Elasticsearch service occur error. Index Name: {indexName}, Id: {id}. {Environment.NewLine}{r.DebugInformation}";
                return result;
            }

            result.Success = true;
            return result;
        }

        /// <summary>批次寫入文件資料模型<para/>
        /// </summary>
        /// <param name="indexName">Elasticsearch 文件庫 index 名稱</param>
        /// <param name="models">文件資料模型集合</param>
        /// <typeparam name="TModel">資料模型的泛型</typeparam>
        /// <returns>執行結果</returns>
        public IResult BulkUpsert<TModel>(string indexName, IEnumerable<TModel> models)
            where TModel : class
        {
            var result = Common.Result.Create();

            if (models.IsNullOrEmpty())
            {
                result.Message = $"The '{nameof(models)}' can not be null or empty collection.";
                return result;
            }

            if (models.HasNullElements())
            {
                result.Message = $"The '{nameof(models)}' can not contains any null object.";
                return result;
            }

            var r = _esClient.Bulk(s => s.Index(indexName).IndexMany(models));

            if (r.Errors)
            {
                result.Message = $"Bulk upsert document data to Elasticsearch occur error. Index Name: {indexName}.{Environment.NewLine}{r.DebugInformation}";
                return result;
            }

            result.Success = true;
            return result;
        }

        /// <summary>批次寫入文件資料模型
        /// </summary>
        /// <param name="indexName">Elasticsearch 文件庫 index 名稱</param>
        /// <param name="models">文件資料模型集合</param>
        /// <typeparam name="TModel">資料模型的泛型</typeparam>
        /// <returns>執行結果</returns>
        public async Task<IResult> BulkUpsertAsync<TModel>(string indexName, IEnumerable<TModel> models)
            where TModel : class
        {
            var result = Common.Result.Create();

            if (models.IsNullOrEmpty())
            {
                result.Message = $"The '{nameof(models)}' can not be null or empty collection.";
                return result;
            }

            if (models.HasNullElements())
            {
                result.Message = $"The '{nameof(models)}' can not contains any null object.";
                return result;
            }

            var r = await _esClient.BulkAsync(s => s.Index(indexName).IndexMany(models));

            if (r.Errors)
            {
                result.Message = $"Bulk upsert document data to Elasticsearch occur error. Index Name: {indexName}.{Environment.NewLine}{r.DebugInformation}";
                return result;
            }

            result.Success = true;
            return result;
        }

        /// <summary>批次寫入文件資料模型
        /// </summary>
        /// <param name="indexName">Elasticsearch 文件庫 index 名稱</param>
        /// <param name="idPropertyName">Elasticsearch 文件的 _id 對應 Property 屬性名稱</param>
        /// <param name="models">文件資料模型集合</param>
        /// <typeparam name="TModel">資料模型的泛型</typeparam>
        /// <returns>執行結果</returns>
        public IResult BulkUpsert<TModel>(string indexName, string idPropertyName, IEnumerable<TModel> models)
            where TModel : class
        {
            var result = Common.Result.Create();

            if (models.IsNullOrEmpty())
            {
                result.Message = $"The '{nameof(models)}' can not be null or empty collection.";
                return result;
            }

            if (models.HasNullElements())
            {
                result.Message = $"The '{nameof(models)}' can not contains any null object.";
                return result;
            }

            var r = _esClient.Bulk(s => s.Index(indexName).IndexMany(models, (d, m) => d.Id(m.GetPropertyValue(idPropertyName) + "")));

            if (r.Errors)
            {
                result.Message = $"Bulk upsert document data to Elasticsearch occur error. Index Name: {indexName}.{Environment.NewLine}{r.DebugInformation}";
                return result;
            }

            result.Success = true;
            return result;
        }

        /// <summary>批次寫入文件資料模型
        /// </summary>
        /// <param name="indexName">Elasticsearch 文件庫 index 名稱</param>
        /// <param name="idPropertyName">Elasticsearch 文件的 _id 對應 Property 屬性名稱</param>
        /// <param name="models">文件資料模型集合</param>
        /// <typeparam name="TModel">資料模型的泛型</typeparam>
        /// <returns>執行結果</returns>
        public async Task<IResult> BulkUpsertAsync<TModel>(string indexName, string idPropertyName, IEnumerable<TModel> models)
            where TModel : class
        {
            var result = Common.Result.Create();

            if (models.IsNullOrEmpty())
            {
                result.Message = $"The '{nameof(models)}' can not be null or empty collection.";
                return result;
            }

            if (models.HasNullElements())
            {
                result.Message = $"The '{nameof(models)}' can not contains any null object.";
                return result;
            }

            var r = await _esClient.BulkAsync(s => s.Index(indexName).IndexMany(models, (d, m) => d.Id(m.GetPropertyValue(idPropertyName) + "")));

            if (r.Errors)
            {
                result.Message = $"Bulk upsert document data to Elasticsearch occur error. Index Name: {indexName}.{Environment.NewLine}{r.DebugInformation}";
                return result;
            }

            result.Success = true;
            return result;
        }

        #endregion Public Methods


        #region Private Methods

        /// <summary>ElasticsearchBroker 對 Elasticsearch 服務的請求回應動作日誌記錄
        /// </summary>
        /// <param name="requestDetails">請求回應動作明細</param>
        private void ElasticsearchActionLog(IApiCallDetails requestDetails)
        {
            try
            {
                var requestId = RandomTextHelper.Create(15);
                var reqUrl = $"{requestDetails.HttpMethod} {requestDetails.Uri}";
                var reqJson = JToken.Parse(requestDetails.RequestBodyInBytes.GetStringFromUtf8Bytes()).ToString(Formatting.Indented);
                var resJson = JToken.Parse(requestDetails.ResponseBodyInBytes.GetStringFromUtf8Bytes()).ToString(Formatting.Indented);
                Logger.Info(this, $"ElasticsearchBroker request log: {Environment.NewLine}RequestId: {requestId}{Environment.NewLine}{reqUrl}{Environment.NewLine}{reqJson}", "ElasticsearchBroker request action log", "ElasticsearchBrokerActionLog");
                Logger.Info(this, $"ElasticsearchBroker response log: {Environment.NewLine}RequestId: {requestId}{Environment.NewLine}{reqUrl}{Environment.NewLine}[Response http status code]: {requestDetails.HttpStatusCode}{Environment.NewLine}{resJson}", "ElasticsearchBroker response action log", "ElasticsearchBrokerActionLog");
            }
            catch
            {
            }
        }

        #endregion Private Methods
    }
}