using System;
using System.Collections.Generic;
using System.Timers;
using ZayniFramework.Common;


namespace ZayniFramework.Logging
{
    /// <summary>Elasticsearch 日誌器管理容器
    /// </summary>
    public static class ElasticsearchLoggerContainer
    {
        #region Private Fields

        /// <summary>ElasticSearchLogger 空物件
        /// </summary>
        /// <returns></returns>
        private static readonly NullElasticSearchLogger _nullObj = new();

        /// <summary>Elasticsearch 日誌器容器字典
        /// </summary>
        /// <returns></returns>
        private static readonly Dictionary<string, ElasticsearchLogger> _container = [];

        /// <summary>日誌清除定時器
        /// </summary>
        private static readonly Timer _logResetTimer;

        #endregion Private Fields


        #region Constructors

        /// <summary>靜態建構子
        /// </summary>
        static ElasticsearchLoggerContainer()
        {
            var config = LoggingSettingLoader.GetLoggingModuleConfigs();

            if (config.EsLogResetDays.IsNull() || config.EsLogResetDays <= 0)
            {
                return;
            }

            if (_container.IsNull())
            {
                return;
            }

            var resetDays = Convert.ToDouble(config.EsLogResetDays);
            var resetBeginDate = DateTime.UtcNow.AddDays(resetDays * -1);

            // 由於 elasticsearch 刪除文件的操作較慢，因此，每 6 小時才執行一次 house keeping 的工作
            _logResetTimer = new Timer
            {
                Interval = TimeSpan.FromHours(6).TotalMilliseconds
            };

            _logResetTimer.Elapsed += async (sender, e) =>
            {
                try
                {
                    if (_container.IsNullOrEmpty())
                    {
                        return;
                    }

                    foreach (var logPair in _container)
                    {
                        if (logPair.IsNull() || logPair.Value.IsNull())
                        {
                            continue;
                        }

                        var esLogger = logPair.Value;
                        await esLogger.ClearLogAsync(resetBeginDate);
                    }
                }
                catch (Exception ex)
                {
                    await ConsoleLogger.LogErrorAsync($"An error occur while doing house keeping for elasticsearch log message.{Environment.NewLine}{ex}");
                }
            };

            _logResetTimer.Enabled = true;
        }

        #endregion Constructors


        #region Public Methods

        /// <summary>取得指定設定名稱的 Elasticsearch 日誌器實體
        /// </summary>
        /// <param name="name">Elasticsearch 日誌器設定名稱</param>
        /// <returns>Elasticsearch 日誌器實體</returns>
        public static ElasticsearchLogger Get(string name)
        {
            if (name.IsNullOrEmpty())
            {
                return _nullObj;
            }

            if (_container.TryGetValue(name, out var esLogger) && esLogger.IsNotNull())
            {
                return esLogger;
            }

            esLogger = new ElasticsearchLogger(name);
            _container[name] = esLogger;
            return esLogger;
        }

        #endregion Public Methods
    }
}