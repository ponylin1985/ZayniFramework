﻿using Newtonsoft.Json;
using System;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Threading.Tasks;
using System.Timers;
using ZayniFramework.Common;


namespace ZayniFramework.Logging
{
    /// <summary>MSSQL 資料庫的日誌訊息資料存取類別
    /// </summary>
    internal sealed class MSSqlEventLogDao : IEventLogDao
    {
        #region 宣告私有的欄位

        /// <summary>日誌清除定時器
        /// </summary>
        private static readonly Timer _logResetTimer;

        /// <summary>資料庫連線字串
        /// </summary>
        private string _dbConnectionString;

        #endregion 宣告私有的欄位


        #region 宣告靜態建構子

        /// <summary>靜態建構子
        /// </summary>
        static MSSqlEventLogDao()
        {
            var config = LoggingSettingLoader.GetLoggingModuleConfigs();

            if (config.DbLogResetDays.IsNull() || config.DbLogResetDays <= 0)
            {
                return;
            }

            string connString = null;

            try
            {
                var dbSetting = ConfigManager.GetZayniFrameworkSettings().DataAccessSettings.Find(c => c.Name == "Zayni");

                if (dbSetting.IsNull())
                {
                    ConsoleLogger.LogError($"{nameof(MSSqlEventLogDao)} initialize error. Can not find Zayni database connection string.");
                    return;
                }

                connString = dbSetting.ConnectionString;
            }
            catch
            {
                return;
            }

            if (connString.IsNullOrEmpty())
            {
                return;
            }

            var interval = Convert.ToDouble(config.DbLogResetDays);
            _logResetTimer = new Timer();

#if DEBUG
            _logResetTimer.Interval = TimeSpan.FromMinutes(1).TotalMilliseconds;
#else
            _logResetTimer.Interval = TimeSpan.FromDays( interval ).TotalMilliseconds;
#endif

            _logResetTimer.Elapsed += (sender, e) => ClearLog(connString);
            _logResetTimer.Enabled = true;
        }

        #endregion 宣告靜態建構子


        #region 宣告建構子

        /// <summary>多載建構子
        /// </summary>
        /// <param name="dbName">資料庫連線設定名稱</param>
        internal MSSqlEventLogDao(string dbName)
        {
            var dbSetting = ConfigManager.GetZayniFrameworkSettings().DataAccessSettings.Find(c => c.Name == "Zayni");

            if (dbSetting.IsNull())
            {
                ConsoleLogger.LogError($"{nameof(MSSqlEventLogDao)} initialize error. Can not find Zayni database connection string.");
                return;
            }

            _dbConnectionString = dbSetting.ConnectionString;
        }

        /// <summary>解構子
        /// </summary>
        ~MSSqlEventLogDao()
        {
            _dbConnectionString = null;
        }

        #endregion 宣告建構子


        #region 實作介面

        /// <summary>寫入日誌事件訊息
        /// </summary>
        /// <param name="model">文字日誌記錄資訊</param>
        /// <returns>寫入日誌是否成功</returns>
        public async Task<bool> InsertLogAsync(LogOption model)
        {
            try
            {
                using var connection = new SqlConnection(_dbConnectionString);
                await connection.OpenAsync();

                var sql = @"
                        -- Insert FS_EVENT_LOG
                        INSERT INTO FS_EVENT_LOG (
                              TEXT_LOGGER
                            , EVENT_LEVEL
                            , EVENT_TITLE
                            , EVENT_MESSAGE
                            , HOST

                            , LOG_TIME
                        ) VALUES (
                              @TextLogger
                            , @EventLevel
                            , @EventTitle
                            , @EventMessage
                            , @Host

                            , @LogTime
                        ); ";

                using (var command = new SqlCommand(sql, connection))
                {
                    command.CommandTimeout = 10;

                    AddInParameter(command, "@TextLogger", SqlDbType.NVarChar, model.Name);
                    AddInParameter(command, "@EventLevel", SqlDbType.NVarChar, model.EventType.ToString());
                    AddInParameter(command, "@EventTitle", SqlDbType.NVarChar, model.Title);
                    AddInParameter(command, "@EventMessage", SqlDbType.NVarChar, model.Message);
                    AddInParameter(command, "@Host", SqlDbType.NVarChar, IPAddressHelper.GetLocalIPAddress());

                    AddInParameter(command, "@LogTime", SqlDbType.DateTime, DateTime.UtcNow);
                    await command.ExecuteNonQueryAsync();
                }

                await connection.CloseAsync();
            }
            catch
            {
                await ConsoleLogger.LogAsync($"{nameof(IEventLogDao)} Original Log:{Environment.NewLine}{JsonConvert.SerializeObject(model, Formatting.Indented)}");
                return false;
            }

            return true;
        }

        #endregion 實作介面


        #region 宣告私有的方法

        /// <summary>加入 SQL 敘述的參數
        /// </summary>
        /// <param name="cmd">目標 SQL 指令</param>
        /// <param name="parameterName">參數名稱</param>
        /// <param name="dbType">參數資料庫型別</param>
        /// <param name="value">參數值</param>
        private static void AddInParameter(SqlCommand cmd, string parameterName, SqlDbType dbType, object value)
        {
            cmd.Parameters.Add(parameterName, dbType);
            cmd.Parameters[parameterName].Value = value ?? DBNull.Value;
            cmd.Parameters[parameterName].Direction = ParameterDirection.Input;
        }

        /// <summary>清除在資料庫中所有的日誌。<para/>
        /// <para>* 以下的 log table 中的資料會被直接清除。</para>
        /// <para>  * FS_SQL_LOG</para>
        /// <para>  * FS_EVENT_LOG</para>
        /// <para>  * FS_SERVICE_HOST_ACTION_LOG</para>
        /// <para>  * FS_SERVICE_CLIENT_ACTION_LOG</para>
        /// <para>  * FS_CACHING_REDIS_ACTION_LOG</para>
        /// </summary>
        /// <param name="connString"></param>
        private static void ClearLog(string connString)
        {
            try
            {
                using var connection = new SqlConnection(connString);
                connection.Open();

                var sql = @"
                        DELETE FROM FS_SQL_LOG;
                        DELETE FROM FS_EVENT_LOG;
                        DELETE FROM FS_SERVICE_HOST_ACTION_LOG;
                        DELETE FROM FS_SERVICE_CLIENT_ACTION_LOG;
                        DELETE FROM FS_CACHING_REDIS_ACTION_LOG; ";

                using (var command = new SqlCommand(sql, connection))
                {
                    command.CommandTimeout = 60;
                    command.ExecuteNonQuery();
                }

                connection.Close();
            }
            catch (Exception)
            {
                return;
            }
        }

        #endregion 宣告私有的方法
    }
}
