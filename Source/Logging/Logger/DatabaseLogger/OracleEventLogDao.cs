using Newtonsoft.Json;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Threading.Tasks;
using System.Timers;
using ZayniFramework.Common;


namespace ZayniFramework.Logging
{
    /// <summary>Oracle 資料庫的日誌訊息資料存取類別
    /// </summary>
    internal sealed class OracleEventLogDao : IEventLogDao
    {
        #region 宣告私有的欄位

        /// <summary>日誌清除定時器
        /// </summary>
        private static readonly Timer _logResetTimer;

        /// <summary>資料庫連線字串
        /// </summary>
        private string _dbConnectionString;

        #endregion 宣告私有的欄位


        #region 宣告靜態建構子

        /// <summary>靜態建構子
        /// </summary>
        static OracleEventLogDao()
        {
            var config = LoggingSettingLoader.GetLoggingModuleConfigs();

            if (config.DbLogResetDays.IsNull() || config.DbLogResetDays <= 0)
            {
                return;
            }

            string connString = null;

            try
            {
                var dbSetting = ConfigManager.GetZayniFrameworkSettings().DataAccessSettings.Find(c => c.Name == "Zayni");

                if (dbSetting.IsNull())
                {
                    ConsoleLogger.LogError($"{nameof(MSSqlEventLogDao)} initialize error. Can not find Zayni database connection string.");
                    return;
                }

                connString = dbSetting.ConnectionString;
            }
            catch
            {
                return;
            }

            if (connString.IsNullOrEmpty())
            {
                return;
            }

            var interval = Convert.ToDouble(config.DbLogResetDays);
            _logResetTimer = new Timer();

#if DEBUG
            _logResetTimer.Interval = TimeSpan.FromMinutes(1).TotalMilliseconds;
#else
            _logResetTimer.Interval = TimeSpan.FromDays( interval ).TotalMilliseconds;
#endif

            _logResetTimer.Elapsed += (sender, e) => ClearLog(connString);
            _logResetTimer.Enabled = true;
        }

        #endregion 宣告靜態建構子


        #region 宣告建構子

        /// <summary>多載建構子
        /// </summary>
        /// <param name="dbName">資料庫連線設定名稱</param>
        internal OracleEventLogDao(string dbName)
        {
            var dbSetting = ConfigManager.GetZayniFrameworkSettings().DataAccessSettings.Find(c => c.Name == "Zayni");

            if (dbSetting.IsNull())
            {
                ConsoleLogger.LogError($"{nameof(MSSqlEventLogDao)} initialize error. Can not find Zayni database connection string.");
                return;
            }

            _dbConnectionString = dbSetting.ConnectionString;
        }

        /// <summary>解構子
        /// </summary>
        ~OracleEventLogDao()
        {
            _dbConnectionString = null;
        }

        #endregion 宣告建構子


        #region 實作介面

        /// <summary>寫入日誌事件訊息
        /// </summary>
        /// <param name="model">文字日誌記錄資訊</param>
        /// <returns>寫入日誌是否成功</returns>
        public async Task<bool> InsertLogAsync(LogOption model)
        {
            try
            {
                using var connection = new OracleConnection(_dbConnectionString);
                await connection.OpenAsync();

                var sql = @"
                        -- Insert FS_EVENT_LOG
                        INSERT INTO FS_EVENT_LOG (
                              TEXT_LOGGER
                            , EVENT_LEVEL
                            , EVENT_TITLE
                            , EVENT_MESSAGE
                            , HOST

                            , LOG_TIME
                        ) VALUES (
                              :TextLogger
                            , :EventLevel
                            , :EventTitle
                            , :EventMessage
                            , :Host

                            , :LogTime
                        ); ";

                using (var command = new OracleCommand(sql, connection))
                {
                    command.BindByName = true;
                    command.CommandTimeout = 10;

                    AddInParameter(command, "TextLogger", OracleDbType.Varchar2, model.Name);
                    AddInParameter(command, "EventLevel", OracleDbType.Varchar2, model.EventType.ToString());
                    AddInParameter(command, "EventTitle", OracleDbType.Varchar2, model.Title);
                    AddInParameter(command, "EventMessage", OracleDbType.Varchar2, model.Message);
                    AddInParameter(command, "Host", OracleDbType.Varchar2, IPAddressHelper.GetLocalIPAddress());

                    AddInParameter(command, "LogTime", OracleDbType.TimeStamp, DateTime.UtcNow);
                    await command.ExecuteNonQueryAsync();
                }

                await connection.CloseAsync();
            }
            catch
            {
                await ConsoleLogger.LogAsync($"{nameof(IEventLogDao)} Original Log:{Environment.NewLine}{JsonConvert.SerializeObject(model, Formatting.Indented)}");
                return false;
            }

            return true;
        }

        #endregion 實作介面


        #region 宣告私有的方法

        /// <summary>加入 SQL 敘述的參數
        /// </summary>
        /// <param name="cmd">目標 SQL 指令</param>
        /// <param name="parameterName">參數名稱</param>
        /// <param name="dbType">參數資料庫型別</param>
        /// <param name="value">參數值</param>
        private static void AddInParameter(OracleCommand cmd, string parameterName, OracleDbType dbType, object value)
        {
            var parameter = new OracleParameter(parameterName, dbType, value, ParameterDirection.Input);
            cmd.Parameters.Add(parameter);
        }

        /// <summary>清除在資料庫中所有的日誌。<para/>
        /// <para>* 以下的 log table 中的資料會被直接清除。</para>
        /// <para>  * FS_SQL_LOG</para>
        /// <para>  * FS_EVENT_LOG</para>
        /// <para>  * FS_SERVICE_HOST_ACTION_LOG</para>
        /// <para>  * FS_SERVICE_CLIENT_ACTION_LOG</para>
        /// <para>  * FS_CACHING_REDIS_ACTION_LOG</para>
        /// </summary>
        /// <param name="connString"></param>
        private static void ClearLog(string connString)
        {
            try
            {
                using var connection = new OracleConnection(connString);
                connection.Open();

                var sqlDelete1 = "DELETE FROM FS_SQL_LOG";
                var sqlDelete2 = "DELETE FROM FS_EVENT_LOG";
                var sqlDelete3 = "DELETE FROM FS_SERVICE_HOST_ACTION_LOG";
                var sqlDelete4 = "DELETE FROM FS_SERVICE_CLIENT_ACTION_LOG";
                var sqlDelete5 = "DELETE FROM FS_CACHING_REDIS_ACTION_LOG";

                using (var command1 = new OracleCommand(sqlDelete1, connection))
                {
                    command1.CommandTimeout = 120;
                    command1.ExecuteNonQuery();
                }

                using (var command2 = new OracleCommand(sqlDelete2, connection))
                {
                    command2.CommandTimeout = 120;
                    command2.ExecuteNonQuery();
                }

                using (var command3 = new OracleCommand(sqlDelete3, connection))
                {
                    command3.CommandTimeout = 120;
                    command3.ExecuteNonQuery();
                }

                using (var command4 = new OracleCommand(sqlDelete4, connection))
                {
                    command4.CommandTimeout = 120;
                    command4.ExecuteNonQuery();
                }

                using (var command5 = new OracleCommand(sqlDelete5, connection))
                {
                    command5.CommandTimeout = 120;
                    command5.ExecuteNonQuery();
                }

                connection.Close();
            }
            catch
            {
                return;
            }
        }

        #endregion 宣告私有的方法
    }
}
