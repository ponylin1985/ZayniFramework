﻿using NeoSmart.AsyncLock;
using Newtonsoft.Json;
using System;
using System.Diagnostics;
using ZayniFramework.Common;


namespace ZayniFramework.Logging
{
    /// <summary>Windows 事件檢視器日誌記錄元件。<para/>
    /// * 只有在 Windows 作業系統下才可以使用。<para/>
    /// * 寫入 Windows Event 需要具有系統權限。
    /// </summary>
    public static class WindowsEventLogger
    {
        /// <summary>非同步作業鎖定物件
        /// </summary>
        private static readonly AsyncLock _lockThiz = new();

        /// <summary>將事件寫入事件檢視器
        /// </summary>
        /// <param name="eventTitle">事件標題</param>
        /// <param name="eventMessage">事件內容訊息</param>
        /// <param name="eventType">事件層級種類</param>
        /// <param name="isEventLogEnable">是否啟用事件檢視器紀錄功能</param>
        /// <param name="ex">程式例外物件</param>
        public static void WriteLog(string eventTitle, string eventMessage, EventLogEntryType eventType, bool isEventLogEnable, Exception ex = null)
        {
            if (!RuntimeEnvironmentHelper.IsWindowsOS())
            {
                return;
            }

            if (!isEventLogEnable)
            {
                return;
            }

            if (new string[] { eventTitle, eventMessage }.HasNullOrEmptyElemants())
            {
                return;
            }

            var message = ex.IsNotNull() ? LogMessageMaker.MakeExceptionEventLogMessage(ex, eventMessage) : eventMessage;

            try
            {
                using (_lockThiz.Lock())
                {
                    EventLog.WriteEntry(eventTitle, message, eventType);
                }
            }
            catch (Exception e)
            {
                ConsoleLogger.LogError($"Write windows event log occur exception. {e}");

                var msg = JsonConvert.SerializeObject(new
                {
                    eventTitle,
                    eventType,
                    eventMessage = message
                }, Formatting.Indented);

                ConsoleLogger.Log($"${nameof(WindowsEventLogger)} Original Log:{Environment.NewLine}{msg}");
            }
        }
    }
}
