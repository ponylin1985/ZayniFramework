﻿using ZayniFramework.Common;


namespace ZayniFramework.Compression
{
    /// <summary>DataSet壓縮器工廠
    /// </summary>
    public class DataSetCompressorFactory
    {
        /// <summary>建立DataSet壓縮器
        /// </summary>
        /// <param name="compressionType">壓縮格式(目前只支援GZIP)</param>
        /// <returns>字串壓縮器</returns>
        public static IDataSetCompressor Create(string compressionType = "GZIP")
        {
            IDataSetCompressor result = compressionType.ToUpper() switch
            {
                "GZIP" => new GZipCompressor(),
                _ => new GZipCompressor(),
            };

            return result;
        }
    }
}
