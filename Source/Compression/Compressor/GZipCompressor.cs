﻿using System;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Text;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.Compression
{
    /// <summary>GZip 格式字串壓縮器
    /// </summary>
    public class GZipCompressor : ITextCompressor, IDataSetCompressor
    {
        #region 實作ITextCompressor介面方法

        /// <summary>壓縮目標字串
        /// </summary>
        /// <param name="target">目標字串</param>
        /// <returns>壓縮過後的字串</returns>
        public string Compress(string target)
        {
            if (target.IsNullOrEmpty())
            {
                Logger.Error(this, "目標字串為Null或是空字串，無法進行字串壓縮。", "GZipCompressor.Compress");
                return string.Empty;
            }

            string result;

            try
            {
                result = target.GZipCompress();
            }
            catch (Exception ex)
            {
                Logger.Exception(this, ex, "GZipCompressor.Compress");
                return string.Empty;
            }

            return result;
        }

        /// <summary>解壓縮目標字串
        /// </summary>
        /// <param name="target">有壓縮過的目標字串</param>
        /// <returns>解壓縮過的字串</returns>
        public string Decompress(string target)
        {
            if (target.IsNullOrEmpty())
            {
                Logger.Error(this, "目標字串為Null或是空字串，無法進行字串解壓縮。", "GZipCompressor.Decompress");
                return string.Empty;
            }

            string result;

            try
            {
                result = target.GZipDecompress();
            }
            catch (Exception ex)
            {
                Logger.Exception(this, ex, "GZipCompressor.Decompress");
                return string.Empty;
            }

            return result;
        }

        #endregion 實作ITextCompressor介面方法


        #region 實作IDataSetCompressor介面方法

        /// <summary>壓縮目標的DataSet物件
        /// </summary>
        /// <param name="target">目標DataSet</param>
        /// <returns>壓縮過的二進位資料</returns>
        public byte[] Compress(DataSet target)
        {
            byte[] result = null;

            using (var stream = new MemoryStream())
            using (var gzip = new GZipStream(stream, CompressionMode.Compress))
            {
                try
                {
                    target.WriteXml(gzip, XmlWriteMode.WriteSchema);
                    gzip.Close();

                    result = stream.ToArray();
                    stream.Close();
                }
                catch (Exception ex)
                {
                    Logger.Exception($"GZipCompressor", ex, "將目標DataSet物件壓縮成GZip二進位資料發生異常: {ex}");
                    return null;
                }
            }

            return result;
        }

        /// <summary>解壓縮目標二進位資料
        /// </summary>
        /// <param name="target">目標二進位資料</param>
        /// <returns>解壓縮過後的DataSet物件</returns>
        public DataSet Decompress(byte[] target)
        {
            if (target.IsNull())
            {
                Logger.Error(this, "目標二進位資料為Null值，無法進行二進位解壓縮。", "GZipCompressor.Decompress");
                return null;
            }

            var result = new DataSet();

            using (var stream = new MemoryStream(target))
            using (var gzip = new GZipStream(stream, CompressionMode.Decompress))
            {
                try
                {
                    result.ReadXml(gzip, XmlReadMode.ReadSchema);
                    gzip.Close();
                    stream.Close();
                }
                catch (Exception ex)
                {
                    Logger.Exception($"GZipCompressor", ex, "將二進位資料解壓縮成DataSet物件發生異常: {ex}");
                    return null;
                }
            }

            return result;
        }

        #endregion 實作IDataSetCompressor介面方法


        #region 宣告公開的靜態字串壓縮/解壓縮方法

        /// <summary>將字串壓縮成GZip壓縮格式Base64編碼的字串
        /// </summary>
        /// <param name="target">要進行GZip壓縮的目標字串</param>
        /// <returns>GZip壓縮格式的二進位元陣列</returns>
        public static byte[] GZipCompress(string target)
        {
            try
            {
                var bytes = Encoding.UTF8.GetBytes(target);

                using var msi = new MemoryStream(bytes);
                using var mso = new MemoryStream();
                using (var gs = new GZipStream(mso, CompressionMode.Compress))
                {
                    msi.CopyTo(gs);
                }

                var result = mso.ToArray();
                return result;
            }
            catch (Exception ex)
            {
                Logger.Exception($"GZipCompressor", ex, "將目標字串壓縮成GZip字串發生異常: {ex}");
                return null;
            }
        }

        /// <summary>將GZip壓縮格式的二位元陣列解壓縮成一般字串
        /// </summary>
        /// <param name="uncompressed">壓縮過的二位元陣列</param>
        /// <returns>一般字串</returns>
        public static string GZipDecompress(byte[] uncompressed)
        {
            try
            {
                using var msi = new MemoryStream(uncompressed);
                using var mso = new MemoryStream();
                using (var gs = new GZipStream(msi, CompressionMode.Decompress))
                {
                    gs.CopyTo(mso);
                }

                var result = Encoding.UTF8.GetString(mso.ToArray());
                return result;
            }
            catch (Exception ex)
            {
                Logger.Exception($"GZipCompressor", ex, "將目標GZip資料解壓縮成一般字串發生異常: {ex}");
                return null;
            }
        }

        #endregion 宣告公開的靜態字串壓縮/解壓縮方法
    }
}
