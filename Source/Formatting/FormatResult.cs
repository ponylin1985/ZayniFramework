﻿using ZayniFramework.Common;


namespace ZayniFramework.Formatting
{
    /// <summary>格式化結果
    /// </summary>
    public class FormatResult : BaseResult
    {
        /// <summary>格式化結果字串
        /// </summary>
        public string Result { get; set; }
    }
}
