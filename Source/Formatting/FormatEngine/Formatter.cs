﻿using System;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.Formatting
{
    /// <summary>格式化元件
    /// </summary>
    public class Formatter
    {
        #region 宣告私有的實體欄位

        /// <summary>錯誤訊息標題
        /// </summary>
        private readonly string _msgTitle = "資料格式化失敗";

        /// <summary>錯誤訊息內容
        /// </summary>
        private readonly string _message = "資料格式化錯誤: {0}";

        #endregion 宣告私有的實體欄位


        #region 宣告私有的建構子

        /// <summary>私有建構子
        /// </summary>
        private Formatter()
        {
            // pass
        }

        #endregion 宣告私有的建構子


        #region 宣告公開靜態的獨體屬性

        /// <summary>執行緒鎖定物件
        /// </summary>
        private static readonly object _lockThis = new();

        /// <summary>獨體參考
        /// </summary>
        private static Formatter _instance;

        /// <summary>Formatter獨體
        /// </summary>
        public static Formatter Instance
        {
            get
            {
                lock (_lockThis)
                {
                    if (_instance.IsNull())
                    {
                        _instance = new Formatter();
                        return _instance;
                    }
                }

                return _instance;
            }
        }

        #endregion 宣告公開靜態的獨體屬性


        #region 宣告公開的實體方法

        /// <summary>根據傳入的格式化資訊，對目標物件進行格式化的動作
        /// </summary>
        /// <param name="target">待格式化的目標物件</param>
        /// <param name="formatInfo">格式化資訊</param>
        /// <param name="result">格式化結果字串</param>
        /// <param name="message">錯誤訊息</param>
        /// <returns>格式化動作是否成功</returns>
        public bool TryFormat(object target, FormatInfo formatInfo, out string result, out string message)
        {
            #region 初始化輸出參數

            result = "";

            #endregion 初始化輸出參數

            #region 檢查輸入的引數

            if (target.IsNull())
            {
                message = string.Format(_message, "傳入的目標物件為Null值，無法進行資料格式化。");
                Logger.Error(this, message, _msgTitle);
                return false;
            }

            var isValid = CheckInputFormatInfo(formatInfo, out message);

            if (!isValid)
            {
                Logger.Error(this, message, _msgTitle);
                return false;
            }

            #endregion 檢查輸入的引數

            #region 取得Formatter物件，開始執行格式化工作

            var formatter = FormatterFactory.CreateFormatter(formatInfo);

            bool isSuccess;

            try
            {
                isSuccess = formatter.TryFormat(target, formatInfo, out result, out message);
            }
            catch (Exception ex)
            {
                message = string.Format(_message, "格式化動作發生程式異常，" + ex.Message);
                Logger.Exception(this, ex, "格式化動作發生程式異常");
                return false;
            }

            #endregion 取得Formatter物件，開始執行格式化工作

            #region 判斷格式化動作是否成功

            if (!isSuccess)
            {
                message = string.Format(_message, message);
                Logger.Error(this, message, _msgTitle);
                return false;
            }

            #endregion 判斷格式化動作是否成功

            return true;
        }

        #endregion 宣告公開的實體方法


        #region 宣告私有的實體方法

        /// <summary>檢查外界傳入格式化資訊是否合法
        /// </summary>
        /// <param name="target">受檢的目標FormatInfo物件</param>
        /// <param name="message">錯誤訊息</param>
        /// <returns>外界傳入格式化資訊是否合法</returns>
        private bool CheckInputFormatInfo(FormatInfo target, out string message)
        {
            message = "";

            if (target.IsNull())
            {
                message = string.Format(_message, "傳入的FormatInfo參數為Null値，無法進行資料格式化。");
                return false;
            }

            var type = target.GetType();
            var properties = type.GetProperties();

            foreach (var m in properties)
            {
                var name = m.Name;
                var value = "";

                // 因為CultureInfo屬性可以為空字串，或是傳入為Null值
                if (name == "CultureInfo")
                {
                    continue;
                }

                try
                {
                    value = m.GetValue(target, null) + "";
                }
                catch (Exception ex)
                {
                    message = string.Format(_message, "以反射機制取得FormatInfo的屬性值發生異常。");
                    Logger.Exception(this, ex, "以反射機制取得FormatInfo的屬性值發生異常");
                    return false;
                }

                if (value.IsNullOrEmpty())
                {
                    message = string.Format("FormatInfo屬性值{0}為空字串或Null不合法", name);
                    return false;
                }
            }

            return true;
        }

        #endregion 宣告私有的實體方法
    }
}
