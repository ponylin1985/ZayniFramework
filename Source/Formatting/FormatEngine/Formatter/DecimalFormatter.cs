﻿using System;
using ZayniFramework.Logging;


namespace ZayniFramework.Formatting
{
    /// <summary>Decimal的格式化元件
    /// </summary>
    public class DecimalFormatter : IFormatter
    {
        #region 宣告私有的欄位

        /// <summary>錯誤訊息
        /// </summary>
        private readonly string _message = "DecimalFormatter格式化錯誤: {0}";

        #endregion 宣告私有的欄位


        #region 實作IFormatter介面

        /// <summary>根據傳入的格式化資訊，對嘗試目標物件進行適當的格式化
        /// </summary>
        /// <param name="target">待格式化的目標物件</param>
        /// <param name="formatInfo">格式化資訊</param>
        /// <param name="result">格式化結果字串</param>
        /// <param name="message">錯誤訊息</param>
        /// <returns>嘗試格式化是否成功</returns>
        public bool TryFormat(object target, FormatInfo formatInfo, out string result, out string message)
        {
            result = "";
            message = "";

            if (!decimal.TryParse(target + "", out var targetDecimal))
            {
                message = string.Format(_message, "The target object to be formatted cannot be cast to decimal and cannot be formatted.");
                Logger.Error(this, message, "DecimalFormatter.TryFormat");
                return false;
            }

            try
            {
                result = string.Format(formatInfo.FormatString, targetDecimal);
            }
            catch (Exception ex)
            {
                message = $"Decimal formatting failed. {ex}";
                Logger.Exception(this, ex, "An exception occurred during the formatting operation");
                return false;
            }

            return true;
        }

        #endregion 實作IFormatter介面
    }
}
