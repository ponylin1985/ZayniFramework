﻿using System;
using System.Collections;
using System.Linq;
using ZayniFramework.Common;
using ZayniFramework.Common.Dynamic;
using ZayniFramework.Logging;


namespace ZayniFramework.Formatting
{
    /// <summary>資料模型格式化元件
    /// </summary>
    public class DataFormatter
    {
        #region 宣告公開的實體方法

        /// <summary>基本型態 Model 的格式化處理，格式化處理資料以字典集合存取，不需要ViewModel物件。<para/>
        /// 1. 如果DateTime的屬性格式化需要支援多國語系，則必須要傳入language語系代碼參數。<para/>
        /// 2. 格式化過程如果需要檢查預設值，則checkDefaultValue參數必須設定為true。<para/>
        /// </summary>
        /// <param name="model">目標Model物件，目標Model必須要實作IBasicModel介面</param>
        /// <param name="checkDefaultValue">是否檢查系統預設值 (預設不檢查)</param>
        /// <param name="language">語系代碼 (預設為繁體中文語系)</param>
        /// <returns>格式化成功的目標Model物件</returns>
        public object DoModelSimpleFormat(object model, bool checkDefaultValue = false, string language = "zh-TW")
        {
            #region 檢查輸入的引數是否合法

            if (model.IsNull())
            {
                return null;
            }

            #endregion 檢查輸入的引數是否合法

            #region 迭代目標資料模型所有的公開屬性，對公開屬性值進行格式化處理

            try
            {
                var modelType = model.GetType();
                var properties = modelType.GetProperties();

                foreach (var property in properties)
                {
                    #region 屬性名稱為 ViewModel 的不需要格式化

                    var propertyName = property.Name;

                    if ("ViewModel" == propertyName)
                    {
                        continue;
                    }

                    #endregion 屬性名稱為 ViewModel 的不需要格式化

                    #region 沒有標記 FormatAttribute 的屬性不需要格式化

                    var attrFormat = Reflector.GetCustomAttribute<FormatAttribute>(property);

                    if (attrFormat.IsNull())
                    {
                        continue;
                    }

                    #endregion 沒有標記 FormatAttribute 的屬性不需要格式化

                    #region 對目標屬性值開始進行格式化處理程序

                    var attrType = attrFormat.GetType();
                    var attrFormatTypeName = attrType.Name;

                    if ("CollectionFormatAttribute" == attrFormatTypeName)
                    {
                        return model;
                    }

                    var propertyValue = property.GetValue(model, null);
                    var formatResult = DoFormat(model, propertyValue, propertyName, attrFormat, attrFormatTypeName, checkDefaultValue, language);

                    PutFormatResultToViewData(model, formatResult, propertyName);

                    #endregion 對目標屬性值開始進行格式化處理程序
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(this, ex, $"ModelFormatter.DoModelSimpleFormat 對目標的資料模型進行基本的格式化處理發生異常: {ex}");
                return model;
            }

            #endregion 迭代目標資料模型所有的公開屬性，對公開屬性值進行格式化處理

            return model;
        }

        /// <summary>基本型態Model的格式化處理，格式化處理資料以ViewModel及物件字典集合兩種方式存取。
        /// 1. 如果DateTime的屬性格式化需要支援多國語系，則必須要傳入language語系代碼參數。
        /// 2. 格式化過程如果需要檢查預設值，則checkDefaultValue參數必須設定為true。
        /// </summary>
        /// <param name="model">目標Model物件，目標Model必須要實作IBasicModel介面</param>
        /// <param name="viewModelType">目標Model物件的ViewModel型別</param>
        /// <param name="checkDefaultValue">是否檢查系統預設值 (預設不檢查)</param>
        /// <param name="language">語系代碼 (預設為繁體中文語系)</param>
        /// <returns>格式化成功的目標Model物件</returns>
        public object DoModelSimpleFormat(object model, Type viewModelType, bool checkDefaultValue = false, string language = "zh-TW")
        {
            #region 檢查輸入的引數是否合法

            var isValid = CheckInputArguments(model, viewModelType);

            if (!isValid)
            {
                return model;
            }

            #endregion 檢查輸入的引數是否合法

            #region 取得資料模型型別、並且建立ViewModel畫面資料模型

            var modelType = model.GetType();
            var vModel = Activator.CreateInstance(viewModelType);

            #endregion 取得資料模型型別、並且建立ViewModel畫面資料模型

            #region 迭代目標資料模型所有的公開屬性，對公開屬性值進行格式化處理

            try
            {
                var properties = modelType.GetProperties();

                foreach (var property in properties)
                {
                    #region 屬性名稱為ViewModel的不需要格式化

                    var propertyName = property.Name;

                    if ("ViewModel" == propertyName)
                    {
                        continue;
                    }

                    #endregion 屬性名稱為ViewModel的不需要格式化

                    #region 沒有標記FormatAttribute的屬性不需要格式化

                    var attrFormat = Reflector.GetCustomAttribute<FormatAttribute>(property);

                    if (attrFormat.IsNull())
                    {
                        continue;
                    }

                    #endregion 沒有標記FormatAttribute的屬性不需要格式化

                    #region 對目標屬性值開始進行格式化處理程序

                    var attrType = attrFormat.GetType();
                    var attrFormatTypeName = attrType.Name;

                    if ("CollectionFormatAttribute" == attrFormatTypeName)
                    {
                        return model;
                    }

                    var propertyValue = property.GetValue(model, null);
                    var formatResult = DoFormat(model, propertyValue, propertyName, attrFormat, attrFormatTypeName, checkDefaultValue, language);

                    PutFormatResultToViewData(model, formatResult, propertyName);
                    PutFormatResultToViewModel(vModel, viewModelType, formatResult, propertyName);

                    #endregion 對目標屬性值開始進行格式化處理程序
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(this, ex, $"ModelFormatter.DoModelSimpleFormat 對目標的資料模型進行基本的格式化處理發生異常: {ex}");
                return model;
            }

            #endregion 迭代目標資料模型所有的公開屬性，對公開屬性值進行格式化處理

            #region 重出初始化ViewModel，並且設定格式化成功資料模型的ViewModel屬性

            try
            {
                InitializeViewModel(vModel, viewModelType);
                var prop = modelType.GetProperty("ViewModel");
                prop.SetValue(model, vModel, null);
            }
            catch (Exception ex)
            {
                Logger.Exception(this, ex, $"ModelFormatter.DoModelSimpleFormat 對目標的資料模型進行基本的格式化處理發生異常: {ex}");
                return model;
            }

            #endregion 重出初始化ViewModel，並且設定格式化成功資料模型的ViewModel屬性

            return model;
        }

        /// <summary>對目標Model物件進行深度的屬性的格式化處理，目標Model必須要實作IBasicModel介面。
        /// 1. 如果DateTime的屬性格式化需要支援多國語系，則必須要傳入language語系代碼參數。
        /// 2. 格式化過程如果需要檢查預設值，則checkDefaultValue參數必須設定為true。
        /// </summary>
        /// <param name="model">目標Model物件，目標Model必須要實作IBasicModel介面</param>
        /// <param name="checkDefaultValue">是否檢查系統預設值 (預設不檢查)</param>
        /// <param name="language">語系代碼 (預設為繁體中文語系)</param>
        /// <returns></returns>
        public object DoModelDeepFormat(object model, bool checkDefaultValue = false, string language = "zh-TW")
        {
            #region 檢查傳入的資料模型是否合法

            if (model.IsNull())
            {
                return null;
            }

            #endregion 檢查傳入的資料模型是否合法

            #region 迭代目標資料模型所有的公開屬性，對公開屬性值進行格式化處理

            try
            {
                var modelType = model.GetType();

                foreach (var p in modelType.GetProperties())
                {
                    #region 屬性名稱為ViewModel的不需要格式化

                    var propertyName = p.Name;

                    if ("ViewModel" == propertyName)
                    {
                        continue;
                    }

                    #endregion 屬性名稱為ViewModel的不需要格式化

                    #region 沒有標記FormatAttribute的不需要格式化

                    var attrFormat = Reflector.GetCustomAttribute<FormatAttribute>(p);

                    if (attrFormat.IsNull())
                    {
                        continue;
                    }

                    #endregion 沒有標記FormatAttribute的不需要格式化

                    #region 判斷是否可以直接對目標屬性進行格式化處理，或是需要在遞迴該目標屬性之後再格式化處理

                    var propertyValue = p.GetValue(model, null);


                    var canFormat = CanDirectFormat(attrFormat, out var attrFormatTypeName);

                    if (canFormat)
                    {
                        var formatResult = DoFormat(model, propertyValue, propertyName, attrFormat, attrFormatTypeName, checkDefaultValue, language);
                        PutFormatResultToViewData(model, formatResult, propertyName);
                    }
                    else
                    {
                        ViewDataCollectionPropertyFormat(propertyValue, checkDefaultValue, language);
                    }

                    #endregion 判斷是否可以直接對目標屬性進行格式化處理，或是需要在遞迴該目標屬性之後再格式化處理
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(this, ex, $"ModelFormatter.DoModelDeepFormat 對目標的資料模型進行深度的格式化處理發生異常: {ex}");
                return model;
            }

            #endregion 迭代目標資料模型所有的公開屬性，對公開屬性值進行格式化處理

            return model;
        }

        /// <summary>對目標Model物件進行深度的屬性的格式化處理，目標Model必須要實作IBasicModel介面。
        /// 1. 如果DateTime的屬性格式化需要支援多國語系，則必須要傳入language語系代碼參數。
        /// 2. 格式化過程如果需要檢查預設值，則checkDefaultValue參數必須設定為true。
        /// </summary>
        /// <param name="model">目標Model物件，目標Model必須要實作IBasicModel介面</param>
        /// <param name="viewModelType">目標Model物件的ViewModel型別</param>
        /// <param name="checkDefaultValue">是否檢查系統預設值 (預設不檢查)</param>
        /// <param name="language">語系代碼 (預設為繁體中文語系)</param>
        /// <returns>格式化成功的目標Model物件</returns>
        public object DoModelDeepFormat(object model, Type viewModelType, bool checkDefaultValue = false, string language = "zh-TW")
        {
            #region 檢查輸入的引數是否合法

            var isValid = CheckInputArguments(model, viewModelType);

            if (!isValid)
            {
                return model;
            }

            #endregion 檢查輸入的引數是否合法

            #region 取得資料模型型別、並且建立ViewModel畫面資料模型

            var modelType = model.GetType();
            var vModel = Activator.CreateInstance(viewModelType);

            #endregion 取得資料模型型別、並且建立ViewModel畫面資料模型

            #region 迭代目標資料模型所有的公開屬性，對公開屬性值進行格式化處理

            try
            {
                foreach (var p in modelType.GetProperties())
                {
                    #region 屬性名稱為ViewModel的不需要格式化

                    var propertyName = p.Name;

                    if ("ViewModel" == propertyName)
                    {
                        continue;
                    }

                    #endregion 屬性名稱為ViewModel的不需要格式化

                    #region 沒有標記FormatAttribute的不需要格式化

                    var attrFormat = Reflector.GetCustomAttribute<FormatAttribute>(p);

                    if (attrFormat.IsNull())
                    {
                        continue;
                    }

                    #endregion 沒有標記FormatAttribute的不需要格式化

                    #region 判斷是否可以直接對目標屬性進行格式化處理，或是需要在遞迴該目標屬性之後再格式化處理

                    var propertyValue = p.GetValue(model, null);


                    var canFormat = CanDirectFormat(attrFormat, out var attrFormatTypeName);

                    if (canFormat)
                    {
                        var formatResult = DoFormat(model, propertyValue, propertyName, attrFormat, attrFormatTypeName, checkDefaultValue, language);
                        PutFormatResultToViewData(model, formatResult, propertyName);
                        PutFormatResultToViewModel(vModel, viewModelType, formatResult, propertyName);
                    }
                    else
                    {
                        CollectionPropertyFormat(propertyValue, checkDefaultValue, language);
                    }

                    #endregion 判斷是否可以直接對目標屬性進行格式化處理，或是需要在遞迴該目標屬性之後再格式化處理
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(this, ex, $"ModelFormatter.DoModelDeepFormat 對目標的資料模型進行深度的格式化處理發生異常: {ex}");
                return model;
            }

            #endregion 迭代目標資料模型所有的公開屬性，對公開屬性值進行格式化處理

            #region 重出初始化ViewModel，並且設定格式化成功資料模型的ViewModel屬性

            try
            {
                InitializeViewModel(vModel, viewModelType);
                var prop = modelType.GetProperty("ViewModel");
                prop.SetValue(model, vModel, null);
            }
            catch (Exception ex)
            {
                Logger.Exception(this, ex, $"ModelFormatter.DoModelDeepFormat 對目標的資料模型進行深度的格式化處理發生異常: {ex}");
                return model;
            }

            #endregion 重出初始化ViewModel，並且設定格式化成功資料模型的ViewModel屬性

            return model;
        }

        /// <summary>對目標DynamicModel的資料模型進行深度的格式化處理，目標資料模型必須要繼承DynamicModel父型別。
        /// 1. 如果DateTime的屬性格式化需要支援多國語系，則必須要傳入language語系代碼參數。
        /// 2. 格式化過程如果需要檢查預設值，則checkDefaultValue參數必須設定為true。
        /// </summary>
        /// <param name="model">目標資料模型，必須要繼承DynamicModel父型別</param>
        /// <param name="checkDefaultValue">是否檢查系統預設值</param>
        /// <param name="language">語系代碼 (預設值為繁體中文語系)</param>
        /// <returns>格式化成功的資料模型物件</returns>
        public object FormatDynamicModel(object model, bool checkDefaultValue = false, string language = "zh-TW")
        {
            #region 檢查傳入的資料模型是否合法

            var isValid = CheckDynamicModel(model);

            if (!isValid)
            {
                return model;
            }

            #endregion 檢查傳入的資料模型是否合法

            #region 取得資料模型型別、並且建立ViewModel畫面資料模型

            var modelType = model.GetType();
            dynamic viewModel = DynamicHelper.CreateDynamicObject();

            #endregion 取得資料模型型別、並且建立ViewModel畫面資料模型

            #region 迭代目標資料模型所有的公開屬性，對公開屬性值進行格式化處理

            try
            {
                foreach (var p in modelType.GetProperties())
                {
                    #region 屬性名稱為ViewModel的不需要格式化

                    var propertyName = p.Name;

                    if ("ViewModel" == propertyName)
                    {
                        continue;
                    }

                    #endregion 屬性名稱為ViewModel的不需要格式化

                    #region 沒有標記FormatAttribute的屬性不需要格式化

                    var formatAttribute = Reflector.GetCustomAttribute<FormatAttribute>(p);

                    if (formatAttribute.IsNull())
                    {
                        continue;
                    }

                    #endregion 沒有標記FormatAttribute的屬性不需要格式化

                    #region 判斷是否可以直接對目標屬性進行格式化處理，或是需要在遞迴該目標屬性之後再格式化處理

                    var propertyValue = p.GetValue(model, null);


                    var canFormat = CanDirectFormat(formatAttribute, out var formatAttributeTypeName);

                    if (canFormat)
                    {
                        var formatResult = DoFormat(model, propertyValue, propertyName, formatAttribute, formatAttributeTypeName, checkDefaultValue, language);
                        PutFormatResultToViewData(model, formatResult, propertyName);
                        PutFormatResultToViewModel(viewModel, formatResult, propertyName);
                    }
                    else
                    {
                        ComplexTypePropertyFormat(propertyValue, checkDefaultValue, language);
                    }

                    #endregion 判斷是否可以直接對目標屬性進行格式化處理，或是需要在遞迴該目標屬性之後再格式化處理
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(this, ex, $"ModelFormatter.FormatDynamicModel 對目標的資料模型進行深度的格式化處理發生異常: {ex}");
                return model;
            }

            #endregion 迭代目標資料模型所有的公開屬性，對公開屬性值進行格式化處理

            #region 重出初始化ViewModel，並且設定格式化成功資料模型的ViewModel屬性

            try
            {
                InitializeViewModel(viewModel);
                var viewModelProperty = modelType.GetProperty("ViewModel");
                viewModelProperty.SetValue(model, viewModel, null);
            }
            catch (Exception ex)
            {
                Logger.Exception(this, ex, $"ModelFormatter.FormatDynamicModel 對目標的資料模型進行深度的格式化處理發生異常: {ex}");
                return model;
            }

            #endregion 重出初始化ViewModel，並且設定格式化成功資料模型的ViewModel屬性

            return model;
        }

        #endregion 宣告公開的實體方法


        #region 宣告私有的實體方法

        /// <summary>檢查輸入的引數是否合法
        /// </summary>
        /// <param name="model">目標Model</param>
        /// <param name="viewModelType">目標Model的ViewModel型別</param>
        /// <returns>輸入的引數是否合法</returns>
        private static bool CheckInputArguments(object model, Type viewModelType)
        {
            if (new object[] { model, viewModelType }.Any(m => m.IsNull()))
            {
                return false;
            }

            var type = model.GetType();

            if ("BasicModel`1" != type.BaseType.Name)
            {
                return false;
            }

            if ("BasicViewModel" != viewModelType.BaseType.Name)
            {
                return false;
            }

            return true;
        }

        /// <summary>檢查輸入的資料模型是否為合法的DynamicModel資料模型
        /// </summary>
        /// <param name="model">目標資料模型</param>
        /// <returns>是否為合法的DynamicModel資料模型</returns>
        private static bool CheckDynamicModel(object model)
        {
            if (model.IsNull())
            {
                return false;
            }

            return model is BasicDynamicModel;
        }

        /// <summary>使用字典集合存放格式化後資料之Model的集合屬性的格式化處理方法
        /// </summary>
        /// <param name="checkDefaultValue">是否檢查預設值</param>
        /// <param name="language">語系代碼</param>
        /// <param name="target">目標格式化屬性</param>
        private void ViewDataCollectionPropertyFormat(dynamic target, bool checkDefaultValue = false, string language = "zh-TW")
        {
            if (null == target)
            {
                return;
            }

            if (target is IList collection)
            {
                if (0 == collection.Count)
                {
                    return;
                }

                foreach (var item in collection)
                {
                    DoModelDeepFormat(item, checkDefaultValue, language);
                }
            }
            else
            {
                DoModelDeepFormat(target, checkDefaultValue, language);
            }
        }

        /// <summary>使用ViewModel存放格式化後資料之Model的集合屬性的格式化處理方法
        /// </summary>
        /// <param name="target">目標格式化屬性</param>
        /// <param name="checkDefaultValue">是否檢查系統預設值</param>
        /// <param name="language">語系代碼</param>
        private void CollectionPropertyFormat(dynamic target, bool checkDefaultValue = false, string language = "zh-TW")
        {
            if (null == target)
            {
                return;
            }

            Type vModelType;

            if (target is IList collection)
            {
                if (0 == collection.Count)
                {
                    return;
                }

                var obj = collection[0];
                vModelType = ReflectTargatViewModelType(obj);

                foreach (var item in collection)
                {
                    DoModelDeepFormat(item, vModelType, checkDefaultValue, language);
                }
            }
            else
            {
                vModelType = ReflectTargatViewModelType(target);
                DoModelDeepFormat(target, vModelType, checkDefaultValue, language);
            }
        }

        /// <summary>對複雜型態的屬性進行深度格式化的遞迴，資料模型為DynamicModel
        /// </summary>
        /// <param name="target">目標屬性</param>
        /// <param name="checkDefaultValue">是否檢查系統預設值</param>
        /// <param name="language">語系代碼</param>
        public void ComplexTypePropertyFormat(dynamic target, bool checkDefaultValue = false, string language = "zh-TW")
        {
            if (null == target)
            {
                return;
            }

            if (target is IList collection)
            {
                if (0 == collection.Count)
                {
                    return;
                }

                foreach (var item in collection)
                {
                    FormatDynamicModel(item, checkDefaultValue, language);
                }
            }
            else
            {
                FormatDynamicModel(target, checkDefaultValue, language);
            }
        }

        /// <summary>檢查是否可以直接進行格式化
        /// </summary>
        /// <param name="attribute">格式化中介資料</param>
        /// <param name="typeName">型別的名稱</param>
        /// <returns>是否可以直接進行格式化</returns>
        private static bool CanDirectFormat(FormatAttribute attribute, out string typeName)
        {
            var type = attribute.GetType();
            typeName = type.Name;

            switch (typeName)
            {
                case "NumberFormatAttribute":
                case "DateTimeFormatAttribute":
                case "DateFormatAttribute":
                case "MaskFormatAttribute":
                    return true;

                case "FormatAttribute":
                case "CollectionFormatAttribute":
                    return false;

                default:
                    return false;
            }
        }

        /// <summary>反射取得目標物件的ViewModel屬性的型別
        /// </summary>
        /// <param name="target">目標物件</param>
        /// <returns>ViewModel的型別</returns>
        private static Type ReflectTargatViewModelType(object target)
        {
            return target.GetType().GetProperty("ViewModel").PropertyType;
        }

        // 20150731 Edtied by Pony: DateTime 日期時間的資料格式化需要可以支援多國語系的設定
        /// <summary>對傳入的目標值進行格式化。
        /// 1. 若進行格式化時，DateTime型別的屬性值需要支援多國語系設定進行格式化，則必須要傳入language語系代碼參數。
        /// 2. 預設不會對目標值檢查系統預設值。
        /// </summary>
        /// <param name="model">包含該目標值的資料Model</param>
        /// <param name="targetValue">待被格式化的目標物件</param>
        /// <param name="name">目標值的屬性名稱字串</param>
        /// <param name="formatAttribute">目標值所屬的FormatAttribute中介資料特性</param>
        /// <param name="formatTypeName">FormatAttribute型別的字串名稱</param>
        /// <param name="checkDefaultValue">是否檢查系統預設值 (預設不檢查)</param>
        /// <param name="language">語系代碼 (預設為繁體中文語系)</param>
        /// <returns>格式化後的字串</returns>
        private string DoFormat(object model, object targetValue, string name, FormatAttribute formatAttribute, string formatTypeName,
            bool checkDefaultValue = false, string language = "zh-TW")
        {
            var strTargetValue = targetValue + "";
            FormatResult formatResult = null;

            #region 讀多國語系的日期格式化字串設定

            var settings = FormatSettingLoader.LoadDateFormatStyle();
            var format = "";

            try
            {
                format = settings.Find(f => f.Language == language).Format.IsNullOrEmptyString("yyyy/MM/dd");
            }
            catch
            {
            }

            if (format.IsNullOrEmpty())
            {
                return string.Empty;
            }

            #endregion 讀多國語系的日期格式化字串設定

            #region 根據不同的資料型別進行格式化處理

            switch (formatTypeName)
            {
                case "NumberFormatAttribute":
                    formatResult = DoNumberStringFormat(model, strTargetValue, formatAttribute, checkDefaultValue);
                    break;

                case "DateFormatAttribute":
                case "DateTimeFormatAttribute":
                    formatResult = DoDateTimeStringFormat(strTargetValue, format, checkDefaultValue);
                    break;

                case "MaskFormatAttribute":
                    // 20180607 Pony Bugfix: 假若要進行隱碼格式化的實際資料為 DateTime 型別，則應該要先進行日期格式化後，再進行資料隱碼格式化。
                    if (targetValue is DateTime)
                    {
                        var fr = DoDateTimeStringFormat(strTargetValue, format, checkDefaultValue);
                        strTargetValue = fr.Success ? fr.Result : strTargetValue;
                    }

                    formatResult = DoMaskFormat(model, strTargetValue, formatAttribute);
                    break;
            }

            #endregion 根據不同的資料型別進行格式化處理

            var result = formatResult.Success ? formatResult.Result : "";
            return result;
        }

        // 20150731 Edited by Pony: 日期時間隔式化需要可以支援多國語系設定
        /// <summary>日期時間字串格式化處理
        /// </summary>
        /// <param name="targetValue">目標日期時間字串</param>
        /// <param name="formatString">日期格式化字串</param>
        /// <param name="checkDefaultValue">是否檢查系統預設值</param>
        /// <returns>格式化結果</returns>
        private static FormatResult DoDateTimeStringFormat(string targetValue, string formatString, bool checkDefaultValue = false)
        {
            var result = new FormatResult()
            {
                Success = false,
                Result = null
            };

            var isSuccess = DateTime.TryParse(targetValue, out var target);

            if (!isSuccess)
            {
                result.Message = "目標字串不是有效的日期格式字串。";
                return result;
            }

            if (checkDefaultValue)
            {
                if (default == target)
                {
                    result.Message = "";
                    return result;
                }
            }

            result = FormatManager.ExecuteDateTimeFormat(target, formatString);
            return result;
        }

        /// <summary>數字字串格式化處理
        /// </summary>
        /// <param name="model">資料來源目標Model</param>
        /// <param name="targetValue">目標數字字串</param>
        /// <param name="formatAttribute">格式化中界資料Attribute</param>
        /// <param name="checkDefaultValue">是否檢查系統預設值</param>
        /// <returns>格式化結果</returns>
        private static FormatResult DoNumberStringFormat(object model, string targetValue, FormatAttribute formatAttribute, bool checkDefaultValue = false)
        {
            var result = new FormatResult()
            {
                Success = false,
                Result = null
            };

            var numberformatAttr = (NumberFormatAttribute)formatAttribute;
            var decLength = numberformatAttr.LengthPropertyName.IsNotNullOrEmpty() ?
                                            ReflectDecLength(model, numberformatAttr.LengthPropertyName) :
                                            numberformatAttr.Length;


            var isSuccess = decimal.TryParse(targetValue, out var value);

            if (!isSuccess)
            {
                result.Message = "目標字串不是有效的數字格式。";
                return result;
            }

            if (checkDefaultValue)
            {
                if (default == value)
                {
                    result.Message = "";
                    return result;
                }
            }

            result = FormatManager.ToCurrencyString(value, decLength);
            return result;
        }

        /// <summary>字串隱碼處理
        /// </summary>
        /// <param name="model">資料來源目標Model</param>
        /// <param name="targetValue">目標隱碼字串</param>
        /// <param name="formatAttribute">格式化中界資料Attribute</param>
        /// <returns>隱碼過後的字串</returns>
        private static FormatResult DoMaskFormat(object model, string targetValue, FormatAttribute formatAttribute)
        {
            var result = new FormatResult()
            {
                Success = false,
                Result = null
            };

            var maskformatAttr = (MaskFormatAttribute)formatAttribute;

            var startIndex = maskformatAttr.StartIndexPropertyName.IsNotNullOrEmpty() ?
                            (int)ReflectDecLength(model, maskformatAttr.StartIndexPropertyName) :
                            maskformatAttr.StartIndex;

            var length = maskformatAttr.LengthPropertyName.IsNotNullOrEmpty() ?
                       (int)ReflectDecLength(model, maskformatAttr.LengthPropertyName) :
                       maskformatAttr.Length;

            if (targetValue.IsNullOrEmpty())
            {
                result.Message = "目標字串值為Null值或空字串，無法進行隱碼格式化。";
                return result;
            }

            result.Result = targetValue.ReplaceChar(maskformatAttr.ReplaceChar, startIndex, length);
            result.Success = true;
            result.Message = "";
            return result;
        }

        /// <summary>將格式化後的結果字串設定到字典集合中
        /// </summary>
        /// <param name="target">目標Model物件</param>
        /// <param name="formatValue">格式化結果字串</param>
        /// <param name="propertyName">ViewModel中的屬性名稱</param>
        private static void PutFormatResultToViewData(object target, string formatValue, string propertyName)
        {
            var model = target as BasicModel;

            if (model.IsNull())
            {
                return;
            }

            var propertyValue = (formatValue + "").IsNullOrEmpty() ? string.Empty : formatValue;   // 如果格式化失敗，預設在ViewModel中填入空字串
            model[propertyName] = propertyValue;
        }

        /// <summary>將格式化後的結果字串設定到指定型別的ViewModel的屬性中
        /// </summary>
        /// <param name="vModel">目標ViewModel物件</param>
        /// <param name="viewModelType">目標ViewModel型別物件</param>
        /// <param name="formatValue">格式化結果字串</param>
        /// <param name="propertyName">ViewModel中的屬性名稱</param>
        private void PutFormatResultToViewModel(object vModel, Type viewModelType, string formatValue, string propertyName)
        {
            var vProperty = viewModelType.GetProperty(propertyName);

            if (vProperty.IsNull())
            {
                return;
            }

            var propertyValue = (formatValue + "").IsNullOrEmpty() ? string.Empty : formatValue;   // 如果格式化失敗，預設在ViewModel中填入空字串

            try
            {
                vProperty.SetValue(vModel, propertyValue, null);
            }
            catch (Exception ex)
            {
                Logger.Exception(this, ex, $"將格式化後的結果字串設定到指定型別的ViewModel的屬性發生異常: {ex}");
            }
        }

        /// <summary>將格式化後的結果字串動態繫結到ViewModel動態物件上
        /// </summary>
        /// <param name="viewModel">ViewModel動態物件</param>
        /// <param name="formatValue">格式化結果字串</param>
        /// <param name="propertyName">ViewModel中的屬性名稱</param>
        private void PutFormatResultToViewModel(dynamic viewModel, string formatValue, string propertyName)
        {
            var propertyValue = (formatValue + "").IsNullOrEmpty() ? string.Empty : formatValue;

            bool isSuccess = DynamicHelper.BindProperty(viewModel, propertyName, propertyValue);

            if (!isSuccess)
            {
                Logger.Error(this, "將格式化後的結果字串動態繫結到ViewModel動態物件上失敗", "ModelFormatter.PutFormatResultToViewModel");
            }
        }

        /// <summary>初始化所有的目標ViewModel中的屬性，如果ViewModel中的屬性值有Null值，會強制把屬性值設定為空字串
        /// </summary>
        /// <param name="vModel">目標ViewModel</param>
        /// <param name="viewModelType">目標ViewModel的型別物件</param>
        private static void InitializeViewModel(object vModel, Type viewModelType = null)
        {
            if (new object[] { vModel, viewModelType }.Any(q => q.IsNull()))
            {
                return;
            }

            foreach (var p in viewModelType.GetProperties())
            {
                var pValue = p.GetValue(vModel, null);

                if (pValue.IsNull())
                {
                    p.SetValue(vModel, string.Empty, null);
                }
            }
        }

        /// <summary>反射取得傳入的Model中的小數位數長度
        /// </summary>
        /// <param name="model">資料來源Model</param>
        /// <param name="lengthPropName">存放小數位數的屬性名稱</param>
        /// <returns>小數位數長度</returns>
        private static decimal ReflectDecLength(object model, string lengthPropName)
        {
            var type = model.GetType();
            var property = type.GetProperty(lengthPropName);

            if (property.IsNull())
            {
                return 0M;
            }

            var strLength = property.GetValue(model, null) + "";

            if (!decimal.TryParse(strLength, out var result))
            {
                return 0M;
            }

            return result;
        }

        #endregion 宣告私有的實體方法
    }
}
