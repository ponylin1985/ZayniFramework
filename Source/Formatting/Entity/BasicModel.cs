﻿using System;
using ZayniFramework.Common;
using ZayniFramework.Serialization;


namespace ZayniFramework.Formatting
{
    /// <summary>基礎資料模型
    /// </summary>
    public class BasicModel : IBasicModel
    {
        #region 宣告私有的欄位

        //  20131218 Edited by Pony: Service會在ServiceSide對資料模型內的資料進行序列化動作，而序列化動作，只會針對public成員進行序列化，所以暫時先把_viewData欄位改成公開的欄位 (JSON)
        //  20140103 Edited by Pony: Dictionary XML序列化會有有多問題! 所以型別改成可以讓XML序列化的Dictionary，但是序列化之後會有一些html特殊字元，導致在XML反序列化時會有問題，所以還是先改為private
        /// <summary>格式化後字典資料集合
        /// </summary>
        private readonly XmlSerializableDictionary<string, string> _viewData = [];

        #endregion 宣告私有的欄位


        #region 宣告建構子

        /// <summary>預設於參數的建構子
        /// </summary>
        public BasicModel()
        {
            Initialize();
        }

        #endregion 宣告建構子


        #region 宣告私有的初始化相關方法

        /// <summary>初始化屬性的方法
        /// </summary>
        private void Initialize()
        {
            var type = GetType();

            foreach (var p in type.GetProperties())
            {
                if ("Item" == p.Name)
                {
                    continue;
                }

                var attrFormat = Reflector.GetCustomAttribute<FormatAttribute>(p);

                if (attrFormat.IsNull())
                {
                    continue;
                }

                var name = attrFormat.GetType().Name;

                if ("CollectionFormatAttribute" != name)
                {
                    continue;
                }

                var value = Activator.CreateInstance(p.PropertyType);
                p.SetValue(this, value, null);
            }

        }

        #endregion 宣告私有的初始化相關方法


        #region 宣告公開的索引子

        /// <summary>畫面資料集合索引子
        /// </summary>
        /// <param name="key">集合索引</param>
        /// <returns>字串資料值</returns>
        public string this[string key]
        {
            get
            {
                if (_viewData.TryGetValue(key, out var value))
                {
                    return value;
                }

                return string.Empty;
            }
            set
            {
                if (value.IsNullOrEmpty())
                {
                    _viewData[key] = string.Empty;
                    return;
                }

                if (_viewData.ContainsKey(value))
                {
                    _viewData[key] = string.Empty;
                    return;
                }

                if (value.IsNullOrEmpty())
                {
                    _viewData[key] = string.Empty;
                    return;
                }

                _viewData[key] = value;
            }
        }

        #endregion 宣告公開的索引子
    }

    /// <summary>基礎資料模型
    /// </summary>
    /// <typeparam name="TViewModel">ViewModel畫面資料模型泛型</typeparam>
    public class BasicModel<TViewModel> : BasicModel, IBasicModel<TViewModel>
        where TViewModel : class
    {
        /// <summary>畫面資料模型
        /// </summary>
        public TViewModel ViewModel
        {
            get;
            set;
        }
    }
}
