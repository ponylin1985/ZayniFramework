﻿namespace ZayniFramework.Formatting
{
    /// <summary>基礎畫面資料模型
    /// </summary>
    public abstract class BasicViewModel
    {
        /// <summary>預設無參數建構子
        /// </summary>
        public BasicViewModel()
        {
            ViewModelInitializer.Initailize(this);
        }
    }
}
