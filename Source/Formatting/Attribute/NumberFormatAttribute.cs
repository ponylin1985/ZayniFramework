﻿using System;


namespace ZayniFramework.Formatting
{
    /// <summary>數字格式化處理Attribute
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class NumberFormatAttribute : FormatAttribute
    {
        #region 宣告私有的實體欄位

        private string _lengthPropertyName;
        private int _length;

        #endregion 宣告私有的實體欄位


        #region 宣告公開的屬性

        /// <summary>小數位數來源屬性名稱
        /// </summary>
        public string LengthPropertyName
        {
            get { return _lengthPropertyName; }
            set { _lengthPropertyName = value; }
        }

        /// <summary>小數位數長度
        /// </summary>
        public int Length
        {
            get { return _length; }
            set { _length = value; }
        }

        #endregion 宣告公開的屬性


        #region 宣告建構子

        /// <summary>多載建構子
        /// </summary>
        /// <param name="lengthPropertyName">小數位數來源屬性名稱</param>
        /// <param name="length">小數位數長度</param>
        /// <param name="formatString">格式化字串</param>
        public NumberFormatAttribute(string lengthPropertyName, int length, string formatString)
            : base(formatString)
        {
            _lengthPropertyName = lengthPropertyName;
            _length = length;
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="lengthPropertyName"></param>
        /// <param name="length"></param>
        public NumberFormatAttribute(string lengthPropertyName, int length)
            : this(lengthPropertyName, length, "")
        {
            // pass
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="lengthPropertyName">小數位數來源屬性名稱</param>
        public NumberFormatAttribute(string lengthPropertyName)
            : this(lengthPropertyName, 0, "")
        {
            // pass
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="length">小數位數長度</param>
        public NumberFormatAttribute(int length)
            : this("", length, "")
        {
            // pass
        }

        /// <summary>預設無參數的建構子
        /// </summary>
        public NumberFormatAttribute()
            : this("", 0, "")
        {
            // pass
        }

        /// <summary>解構子
        /// </summary>
        ~NumberFormatAttribute()
        {
            _lengthPropertyName = null;
        }

        #endregion 宣告建構子
    }
}
