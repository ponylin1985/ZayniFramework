﻿using System;


namespace ZayniFramework.Formatting
{
    /// <summary>字串隱碼處理Attribute
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class MaskFormatAttribute : FormatAttribute
    {
        #region 宣告建構子

        /// <summary>多載建構子
        /// </summary>
        /// <param name="replaceChar">要被置換的字元</param>
        /// <param name="startIndex">從哪個起始位置開始置換(Zero-base)</param>
        /// <param name="length">置換多少個長度的字元</param>
        /// <param name="startIndexPropertyName">置換多少個長度的字元</param>
        /// <param name="lengthPropertyName">置換多少個長度的字元</param>
        public MaskFormatAttribute(char replaceChar = 'X', int startIndex = 0, int length = -1, string startIndexPropertyName = "", string lengthPropertyName = "")
        {
            ReplaceChar = replaceChar;
            StartIndex = startIndex;
            Length = length;
            StartIndexPropertyName = startIndexPropertyName;
            LengthPropertyName = lengthPropertyName;
        }

        #endregion 建構子


        #region 宣告公開的屬性

        /// <summary>要被置換的字元
        /// </summary>
        public char ReplaceChar { get; set; }

        /// <summary>從哪個起始位置開始置換(Zero-base)
        /// </summary>
        public int StartIndex { get; set; }

        /// <summary>置換多少個長度的字元
        /// </summary>
        public int Length { get; set; }

        /// <summary>隱碼起始位置來源屬性名稱
        /// </summary>
        public string StartIndexPropertyName { get; set; }

        /// <summary>隱碼長度來源屬性名稱
        /// </summary>
        public string LengthPropertyName { get; set; }

        #endregion 宣告公開的屬性
    }
}
