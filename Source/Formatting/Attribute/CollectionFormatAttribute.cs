﻿using System;


namespace ZayniFramework.Formatting
{
    /// <summary>集合格式化處理Attribute
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class CollectionFormatAttribute : FormatAttribute
    {
    }
}
