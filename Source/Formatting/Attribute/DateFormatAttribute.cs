﻿using System;


namespace ZayniFramework.Formatting
{
    /// <summary>日期格式化處理Attribute
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class DateFormatAttribute : FormatAttribute
    {
    }
}
