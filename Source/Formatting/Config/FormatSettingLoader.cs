﻿using System;
using System.Collections.Generic;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.Formatting
{
    /// <summary>格式化設定Config讀取器
    /// </summary>
    public static class FormatSettingLoader
    {
        /// <summary>讀取Config檔中日期時間的格式化設定集合
        /// </summary>
        /// <returns>日期的格式化樣式設定值</returns>
        public static List<DateTimeFormatStyleSetting> LoadDateFormatStyle()
        {
            try
            {
                var result =
                    ConfigManager
                        .GetZayniFrameworkSettings()
                        .FormattingSettings
                        .DateTimeFormatStyleSettings;

                return result;
            }
            catch (Exception ex)
            {
                Logger.Exception(nameof(FormatSettingLoader), ex, "An exception occured while loading date format style settings.");
                return null;
            }
        }
    }
}
