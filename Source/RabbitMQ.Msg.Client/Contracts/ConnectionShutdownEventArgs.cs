﻿namespace RabbitMQ.Message.Client
{
    /// <summary>
    /// 斷線事件回覆資訊
    /// </summary>
    public class ConnectionShutdownEventArgs
    {
        /// <summary>
        /// 建構子
        /// </summary>
        public ConnectionShutdownEventArgs(string code, string msg)
        {
            ErrorCode = code;
            ErrorMessage = msg;
        }

        /// <summary>
        /// 該服務回傳的錯誤代碼
        /// </summary>
        public string ErrorCode { get; set; }

        /// <summary>
        /// 該服務回傳的錯誤訊息
        /// </summary>
        public string ErrorMessage { get; set; }
    }
}
