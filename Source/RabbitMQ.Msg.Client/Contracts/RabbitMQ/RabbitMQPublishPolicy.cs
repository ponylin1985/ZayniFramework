﻿namespace RabbitMQ.Message.Client
{
    /// <summary>
    /// Publish 的規則
    /// </summary>
    public class RabbitMQPublishPolicy
    {
        /// <summary>
        /// 是否確保Message推送至Broker，若false的話，不保證訊息一定送至server，有丟包的風險
        /// </summary>
        public bool IsConfirm { get; set; } = true;

        /// <summary>
        /// 設定publish是否confirm
        /// </summary>
        public void SetPublishConfirm(bool isConfirm)
        {
            IsConfirm = isConfirm;
        }
    }
}
