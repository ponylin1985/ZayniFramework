﻿namespace RabbitMQ.Message.Client
{
    /// <summary>
    /// Listener結束的錯誤代碼
    /// </summary>
    public enum ListenerCloseCode
    {
        /// <summary>
        /// 監聽匿名函式回傳true, 監聽事件結束
        /// </summary>
        Finish,

        /// <summary>
        /// Listener執行發生例外
        /// </summary>
        Exception,

        /// <summary>
        /// Channel關閉，相關定義看各Library
        /// </summary>
        Close
    }
}
