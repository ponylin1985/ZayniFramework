﻿using Microsoft.Win32.SafeHandles;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ZayniFramework.Common;


namespace RabbitMQ.Message.Client
{
    /// <summary>
    /// RabbitMQ Service Event, Instance exist, the connection exist.
    /// Note: 
    /// If connection recovery when disconnect, act from channel maybe fail.
    /// once the channel recovery context,
    /// the listener you register would work again and refetch the same item from queue on server.
    /// (because you act before channel recovery would not work, and the item would back to queue.)
    /// please consider the above.
    /// </summary>
    public class RabbitMQServiceClient : IServiceEvent
    {
        #region 私有欄位

        /// <summary>
        /// 主要資訊RabbitMQModule
        /// </summary>
        private readonly RabbitMQModule _module;

        /// <summary>
        /// MQ連線的建立工廠
        /// </summary>
        private readonly ConnectionFactory _factory;

        /// <summary>
        /// MQ連線資訊
        /// </summary>
        private readonly IConnection _conn;

        /// <summary>
        /// Dispose相關設定
        /// </summary>
        private bool disposed = false;

        /// <summary>
        /// Dispose相關設定
        /// </summary>
        private readonly SafeHandle _handle = new SafeFileHandle(IntPtr.Zero, true);

        /// <summary>
        /// corrId 相對應的等待 message 的Queue
        /// </summary>
        private static readonly Dictionary<string, BlockingCollection<IEventMessage>> _responseQueue = [];

        /// <summary>監聽的 Queue 名稱
        /// </summary>
        private string _listenQueueName;

        #endregion


        #region 建、解構子與Dispose

        /// <summary>
        /// 建構子，同時建立一條MQ連線
        /// </summary>
        /// <param name="module">主要Module資訊</param>
        public RabbitMQServiceClient(RabbitMQModule module)
        {
            //主要資訊RabbitMQModule存在欄位
            _module = module ?? throw new Exception("module should not be null.");

            //建立MQ連線，並決定是否自動重新連線
            var hostInfo = _module.ConnectionPolicy;
            _factory = new ConnectionFactory
            {
                HostName = hostInfo.HostName,
                Port = hostInfo.Port,
                UserName = hostInfo.UserName,
                Password = hostInfo.Password,
                VirtualHost = hostInfo.VirtualHost,
                RequestedHeartbeat = TimeSpan.FromSeconds(_module.RequestHeartBeat),
                AutomaticRecoveryEnabled = _module.ConnectionPolicy.AutoReConnect
            };

            _conn = _factory.CreateConnection();
            _conn.ConnectionShutdown += _conn_ConnectionShutdown;

            _ = Task.Run(() =>
            {
                if (!string.IsNullOrEmpty(_module.DefaultRPCReplyQueueName))
                {
                    Listen(_module.DefaultRPCReplyQueueName, eventHandleRPCQueue);
                }

                //聽有註冊的mapper
                var listenList = new List<string>();
                foreach (var reply in _module.RPCReplyQueueMapper)
                {
                    if (string.IsNullOrEmpty(reply.Value))
                    {
                        continue;
                    }

                    if (reply.Value == _module.DefaultRPCReplyQueueName)
                    {
                        continue;
                    }

                    if (listenList.Contains(reply.Value))
                    {
                        continue;
                    }

                    Listen(reply.Value, eventHandleRPCQueue);
                    listenList.Add(reply.Value);
                }
            });
        }

        /// <summary>
        /// 解構子，記憶體釋放的同時，如果連線存在，也斷開連線
        /// </summary>
        ~RabbitMQServiceClient()
        {
            if (_conn != null && _conn.IsOpen)
            {
                _conn.Close();
            }

            Console.WriteLine("RabbitMQ.Message.Client.RabbitMQServiceClient Dispose");
        }

        /// <summary>
        /// 實作IDisposable 方法，並且連線存在時，關閉連線
        /// </summary>
        public void Dispose()
        {
            if (_conn != null && _conn.IsOpen)
            {
                _conn.Close();
            }

            Dispose(true);
        }

        /// <summary>
        /// 執行Dispose
        /// </summary>
        private void Dispose(bool disposing)
        {
            if (disposed)
            {
                return;
            }

            if (disposing)
            {
                _handle.Dispose();
            }

            disposed = true;
        }

        #endregion


        #region 公開的實作方法

        /// <summary>
        /// 監聽ServiceEvent訊息, 若事件發生例外, 則關閉Channel停止該Queue的監聽事件
        /// </summary>
        /// <param name="eventName">監聽的ServiceEvent事件名稱</param>
        /// <param name="listenFunc">監聽的委派事件。必須包含Attribute RabbitMQListenPolicyAttribute。若Return True則停止監控，若Return False則繼續監聽</param>
        /// <param name="closeNotifyAction">監聽事件結束的委派事件。</param>
        public void Listen(string eventName, Func<IEventMessage, bool> listenFunc, Action<ListenShutdownEventArgs> closeNotifyAction = null)
        {
            if (listenFunc == null)
            {
                throw new Exception("func should not null.");
            }

            //取得委派的RabbitMQListenPolicyAttribute
            RabbitMQListenPolicyAttribute policyAttr = null;
            var attrs = listenFunc.Method.GetCustomAttributes(typeof(RabbitMQListenPolicyAttribute), true);

            foreach (var attr in attrs)
            {
                if (attr != null)
                {
                    policyAttr = attr as RabbitMQListenPolicyAttribute;
                    break;
                }
            }

            if (policyAttr == null)
            {
                throw new Exception("'RabbitMQListenPolicyAttribute' not fund in Func");
            }

            //利用_conn連線建立一個channel
            var channel = _conn.CreateModel();

            //設定Channel取得message的大小和數量
            channel.BasicQos(policyAttr.PrefetchSize, policyAttr.PrefetchCount, false);

            //在channel註冊一個consumer物件，定義回到message的委派事件
            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (ch, ea) =>
            {
                //Channel若已經關閉，則多收進來的unack message不處理
                if (channel.IsClosed)
                {
                    return;
                }

                var consu = ch as EventingBasicConsumer;

                //執行委派事件
                try
                {
                    var funcRtn = listenFunc(CreateEventMessage(ch, ea, policyAttr.ReQueue, policyAttr.AutoAct));
                    if (funcRtn)
                    {
                        channel.BasicCancel(ea.ConsumerTag);
                        channel.Close(9090, "Listener stop.");
                    }
                }
                //例外處理: Channel關閉
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);

                    if (channel.IsOpen)
                    {
                        channel.BasicCancel(ea.ConsumerTag);
                        channel.Close(9999, ex.Message);
                    }
                }
            };
            channel.ModelShutdown += (object sender, ShutdownEventArgs e) =>
            {
                //派斷是否須往前通知
                if (closeNotifyAction == null)
                {
                    return;
                }

                var code = ListenerCloseCode.Finish;

                switch (e.ReplyCode)
                {
                    case 9090:
                        code = ListenerCloseCode.Finish;
                        break;
                    case 9999:
                        code = ListenerCloseCode.Exception;
                        break;
                    default:
                        code = ListenerCloseCode.Close;
                        break;
                }

                //執行匿名委派
                closeNotifyAction(new ListenShutdownEventArgs(code, e.ReplyText));
            };

            channel.BasicConsume(eventName, policyAttr.AutoAct, consumer);
        }

        /// <summary>
        /// 監聽ServiceEvent訊息, 若事件發生例外, 則關閉Channel停止該Queue的監聽事件
        /// </summary>
        /// <param name="queueNamePrefix">監聽的 Queue 名稱前綴字</param>
        /// <param name="exchangeName">監聽的 Exchange 名稱</param>
        /// <param name="routingKey">監聽的 RoutingKey 名稱</param>
        /// <param name="listenFunc">監聽的委派事件。必須包含Attribute RabbitMQListenPolicyAttribute。若Return True則停止監控，若Return False則繼續監聽</param>
        /// <param name="closeNotifyAction">監聽事件結束的委派事件。</param>
        public void ListenExchange(string queueNamePrefix, string exchangeName, string routingKey, Func<IEventMessage, bool> listenFunc, Action<ListenShutdownEventArgs> closeNotifyAction = null)
        {
            //判斷func
            if (listenFunc == null)
            {
                throw new Exception("func should not null.");
            }

            //取得委派的RabbitMQListenPolicyAttribute
            RabbitMQListenPolicyAttribute policyAttr = null;
            var attrs = listenFunc.Method.GetCustomAttributes(typeof(RabbitMQListenPolicyAttribute), true);

            foreach (var attr in attrs)
            {
                if (attr != null)
                {
                    policyAttr = attr as RabbitMQListenPolicyAttribute;
                    break;
                }
            }

            if (policyAttr == null)
            {
                throw new Exception("'RabbitMQListenPolicyAttribute' not fund in Func");
            }

            //利用_conn連線建立一個channel
            var channel = _conn.CreateModel();

            #region 20191128 Edited by Pony: 動態新增 RabbitMQ 的 Queue，並且對此動態產生出的 Queue，對指定的 Exchange 和 RoutingKey 進行 Binding

            _listenQueueName = $"{queueNamePrefix}{RandomTextHelper.Create(10)}";
            channel.QueueDeclare(_listenQueueName, false, false, true, null);
            channel.QueueBind(_listenQueueName, exchangeName, routingKey, null);

            #endregion 20191128 Edited by Pony

            //設定Channel取得message的大小和數量
            channel.BasicQos(policyAttr.PrefetchSize, policyAttr.PrefetchCount, false);

            //在channel註冊一個consumer物件，定義回到message的委派事件
            var consumer = new EventingBasicConsumer(channel);

            consumer.Received += (ch, ea) =>
            {
                //Channel若已經關閉，則多收進來的unack message不處理
                if (channel.IsClosed)
                {
                    return;
                }

                var consu = ch as EventingBasicConsumer;

                try
                {
                    var funcRtn = listenFunc(CreateEventMessage(ch, ea, policyAttr.ReQueue, policyAttr.AutoAct));
                    if (funcRtn)
                    {
                        channel.BasicCancel(ea.ConsumerTag);
                        channel.Close(9090, "Listener stop.");
                    }
                }
                //例外處理: Channel關閉
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);

                    if (channel.IsOpen)
                    {
                        channel.BasicCancel(ea.ConsumerTag);
                        channel.Close(9999, ex.Message);
                    }
                }
            };

            channel.ModelShutdown += (object sender, ShutdownEventArgs e) =>
            {
                //派斷是否須往前通知
                if (closeNotifyAction == null)
                {
                    return;
                }

                var code = ListenerCloseCode.Finish;

                switch (e.ReplyCode)
                {
                    case 9090:
                        code = ListenerCloseCode.Finish;
                        break;
                    case 9999:
                        code = ListenerCloseCode.Exception;
                        break;
                    default:
                        code = ListenerCloseCode.Close;
                        break;
                }

                //執行匿名委派
                closeNotifyAction(new ListenShutdownEventArgs(code, e.ReplyText));
            };

            channel.BasicConsume(_listenQueueName, policyAttr.AutoAct, consumer);
            // channel.BasicConsume(exchangeName, policyAttr.AutoAct, consumer);
        }

        /// <summary>
        /// 發送訊息
        /// </summary>
        /// <param name="eventName">ServiceEvent事件名稱</param>
        /// <param name="contentType">內容型式</param>
        /// <param name="body">主要訊息</param>
        /// <param name="messageID">訊息識別碼</param>
        /// <param name="timestamp">時間戳紀(毫秒)</param>
        public void Emit(string eventName, string contentType, string body, string messageID, long timestamp)
        {
            Publish(eventName, null, contentType, body, messageID, timestamp, null);
        }

        /// <summary>
        /// 發送RPC訊息
        /// </summary>
        /// <param name="eventName">ServiceEvent事件名稱</param>
        /// <param name="contentType">內容型式</param>
        /// <param name="body">主要訊息</param>
        /// <param name="messageID">訊息識別碼</param>
        /// <param name="timestamp">時間戳紀(毫秒)</param>
        /// <param name="timeout">等待時間(毫秒)</param>
        /// <param name="onlyWaitOne">有一個Message回覆，則立即返回</param>
        /// <returns>IEventMessage</returns>
        public List<IEventMessage> RPC(string eventName, string contentType, string body, string messageID, long timestamp, ushort timeout, bool onlyWaitOne)
        {
            //為ReplyQueue的consumer，執行監聽回覆
            var answers = new List<IEventMessage>();

            //取得RPCReplyQueueName
            var replyQueueName = GetRPCReplyQueueName(eventName);
            if (string.IsNullOrEmpty(replyQueueName))
            {
                //利用_conn連線建立一個channel
                using var channel = _conn.CreateModel();

                //若空白則，宣告一個隨機命名的Queue，專門接受該次RPC的回應
                replyQueueName = channel.QueueDeclare().QueueName;

                #region Marked by Pony

                // 20200128 Marked by Pony: 改用 EventingBasicConsumer，因為新版本的 C# RabbitMQ.Client 官方建議不再使用 QueueingBasicConsumer 當作 RPC 的 ReplyQueue 的 consumer，可能是因為效能問題。
                // https://stackoverflow.com/questions/38746900/queueingbasicconsumer-is-deprecated-which-consumer-is-better-to-implement-rabbi
                //請求前先在ReplyQueue建立一個consumer
                // var consumer = new QueueingBasicConsumer(channel);

                #endregion Marked by Pony

                var consumer = new EventingBasicConsumer(channel);
                channel.BasicConsume(queue: replyQueueName, autoAck: true, consumer: consumer);

                var replyEvent = new AutoResetEvent(false);

                //送出RPC訊息
                var corrId = Guid.NewGuid().ToString();
                Publish(eventName, corrId, contentType, body, messageID, timestamp, replyQueueName, timeout);

                consumer.Received += (model, ea) =>
                {
                    if (null == ea)
                    {
                        replyEvent.Set();
                        return;
                    }

                    if (ea.BasicProperties.CorrelationId != corrId)
                    {
                        replyEvent.Set();
                        return;
                    }

                    //將訊息存入answers
                    answers.Add(new RabbitMQEventMessage(ea));

                    //若設定為只收一個，則返回
                    if (onlyWaitOne)
                    {
                        replyEvent.Set();
                        return;
                    }

                    replyEvent.Set();
                };

                // 檢查是否 timeout 逾時
                if (replyEvent.WaitOne((int)timeout))
                {
                    return answers;
                }

                #region Marked by Pony 20200128: 改用 EventingBacisConsumer 當作 consumer 之後的改寫

                // while (true)
                // {
                //     BasicDeliverEventArgs ea;
                //     //倒出Queue的Message

                //     var deResult = consumer.Queue.Dequeue(timeout, out ea);
                //     if (ea == null)
                //         break;

                //     //確認CorrelationId
                //     if (ea.BasicProperties.CorrelationId == corrId)
                //     {
                //         //將訊息存入answers
                //         answers.Add(new RabbitMQEventMessage(ea));

                //         //若設定為只收一個，則返回
                //         if (onlyWaitOne)
                //             break;
                //     }
                // }

                #endregion Marked by Pony 20200128
            }
            else
            {
                //建立corrid
                var corrId = Guid.NewGuid().ToString();

                //註冊一個Queue收corrid的訊息
                var collection = new BlockingCollection<IEventMessage>();

                lock (_responseQueue)
                {
                    _responseQueue.Add(corrId, collection);
                }

                Publish(eventName, corrId, contentType, body, messageID, timestamp, replyQueueName, timeout);

                while (true)
                {
                    var takeResult = collection.TryTake(out var msg, timeout);

                    if (!takeResult)
                    {
                        break;
                    }

                    answers.Add(msg);

                    if (onlyWaitOne)
                    {
                        break;
                    }
                }

                lock (_responseQueue)
                {
                    _responseQueue.Remove(corrId);
                }
            }

            return answers;
        }

        /// <summary>
        /// 回覆RPC訊息
        /// </summary>
        /// <param name="eventMessage">收到來自於RPC的請求Message</param>
        /// <param name="contentType">回覆的內容格式</param>
        /// <param name="body">回覆的主要訊息</param>
        /// <param name="messageID">回覆的訊息識別碼</param>
        /// <param name="timestamp">回覆時間戳紀(毫秒)</param>
        public void ResponseRPC(IEventMessage eventMessage, string contentType, string body, string messageID, long timestamp)
        {
            PublishRPC(eventMessage.GetReplyTo(), eventMessage.GetCorrelationID(), contentType, body, messageID, timestamp, null);
        }

        #endregion

        #region 事件委派

        /// <summary>
        /// 斷事通知的註冊事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _conn_ConnectionShutdown(object sender, ShutdownEventArgs e)
        {
            //派斷是否須往前通知
            if (_module.ConnShutdownEvent == null)
            {
                Console.WriteLine(e);
                return;
            }

            //執行匿名委派
            _module.ConnShutdownEvent(new ConnectionShutdownEventArgs(e.ReplyCode.ToString(), e.Cause + ""));
        }

        #endregion

        #region 私有方法

        /// <summary>
        /// 發送信息
        /// </summary>
        /// <param name="routingKey">事件名稱</param>
        /// <param name="correlationID">關聯識別碼</param>
        /// <param name="contentType">內容格式</param>
        /// <param name="body">主要信息</param>
        /// <param name="messageID">信息識別碼</param>
        /// <param name="timestamp">時間戳紀(秒)</param>
        /// <param name="replyto">信息需回覆的QueueName</param>
        /// <param name="expireTime">信息有效期限(毫秒)</param>
        private void PublishRPC(string routingKey, string correlationID, string contentType, string body, string messageID, long timestamp, string replyto, ushort expireTime = 0)
        {
            //event Name 不可null, 至少空字串
            if (routingKey == null)
            {
                throw new Exception("routingKey null");
            }

            //利用_conn連線建立一個channel
            using var channel = CreateChannel();

            var exchange = "";
            var byteBody = Encoding.UTF8.GetBytes(body + "");
            var props = channel.CreateBasicProperties();
            if (messageID != null)
            {
                props.MessageId = messageID;
            }

            props.Timestamp = new AmqpTimestamp(timestamp);
            if (contentType != null)
            {
                props.ContentType = contentType;
            }

            if (correlationID != null)
            {
                props.CorrelationId = correlationID;
            }

            if (replyto != null)
            {
                props.ReplyTo = replyto;
            }

            if (expireTime > 0)
            {
                props.Expiration = expireTime.ToString();
            }

            //推送訊息
            channel.BasicPublish(
                exchange: exchange,
                routingKey: routingKey,
                basicProperties: props,
                body: byteBody);

            if (_module.PublishPolicy.IsConfirm)
            {
                channel.WaitForConfirms();
            }
        }

        /// <summary>
        /// 發送信息
        /// </summary>
        /// <param name="routingKey">事件名稱</param>
        /// <param name="correlationID">關聯識別碼</param>
        /// <param name="contentType">內容格式</param>
        /// <param name="body">主要信息</param>
        /// <param name="messageID">信息識別碼</param>
        /// <param name="timestamp">時間戳紀(秒)</param>
        /// <param name="replyto">信息需回覆的QueueName</param>
        /// <param name="expireTime">信息有效期限(毫秒)</param>
        private void Publish(string routingKey, string correlationID, string contentType, string body, string messageID, long timestamp, string replyto, ushort expireTime = 0)
        {
            //event Name 不可null, 至少空字串
            if (routingKey == null)
            {
                throw new Exception("routingKey null");
            }

            //利用_conn連線建立一個channel
            using var channel = CreateChannel();

            var exchange = GetExchange(routingKey);
            var byteBody = Encoding.UTF8.GetBytes(body + "");
            var props = channel.CreateBasicProperties();
            if (messageID != null)
            {
                props.MessageId = messageID;
            }

            props.Timestamp = new AmqpTimestamp(timestamp);
            if (contentType != null)
            {
                props.ContentType = contentType;
            }

            if (correlationID != null)
            {
                props.CorrelationId = correlationID;
            }

            if (replyto != null)
            {
                props.ReplyTo = replyto;
            }

            if (expireTime > 0)
            {
                props.Expiration = expireTime.ToString();
            }

            //推送訊息
            channel.BasicPublish(
                exchange: exchange,
                routingKey: routingKey,
                basicProperties: props,
                body: byteBody);

            if (_module.PublishPolicy.IsConfirm)
            {
                channel.WaitForConfirms();
            }
        }

        /// <summary>
        /// 利用IConnection建立Channel
        /// </summary>
        /// <returns></returns>
        private IModel CreateChannel()
        {
            var channel = _conn.CreateModel();

            if (_module.PublishPolicy.IsConfirm)
            {
                channel.ConfirmSelect();
            }

            return channel;
        }

        /// <summary>
        /// 建立RabbitMQEventMessage物件
        /// </summary>
        private static RabbitMQEventMessage CreateEventMessage(object ch, BasicDeliverEventArgs ea, bool reqeueu, bool autoAct)
        {
            return new RabbitMQEventMessage(ch, ea, reqeueu, autoAct);
        }

        /// <summary>
        /// 從Module中取得mapper到的exchange Name
        /// </summary>
        private string GetExchange(string routingKey)
        {
            var exchange = _module.DefaultRoutingExchange + "";

            if (_module.RoutingExchangeMapper.TryGetValue(routingKey, out var value) && !string.IsNullOrEmpty(value))
            {
                exchange = value;
            }
            else
            {
                foreach (var item in _module.RoutingExchangeMapper)
                {
                    if (!routingKey.StartsWith(item.Key))
                    {
                        continue;
                    }

                    if (string.IsNullOrEmpty(item.Value))
                    {
                        continue;
                    }

                    exchange = item.Value;
                }
            }

            return exchange;
        }

        /// <summary>
        /// 從Module中取得RPCReplyQueueMapper，取得對應的QueueName
        /// </summary>
        private string GetRPCReplyQueueName(string routingKey)
        {
            var queueName = _module.DefaultRPCReplyQueueName + "";

            if (_module.RPCReplyQueueMapper.TryGetValue(routingKey, out var value))
            {
                queueName = value + "";
            }

            return queueName;
        }

        /// <summary>
        /// Listen方法-統一處理指定RPCReplyQueue的訊息
        /// </summary>
        [RabbitMQListenPolicy(AutoAct = true)]
        private bool eventHandleRPCQueue(IEventMessage msg)
        {
            var corrId = msg.GetCorrelationID();

            if (!_responseQueue.TryGetValue(corrId, out var value))
            {
                return false;
            }

            try
            {
                value.Add(msg);
            }
            catch (Exception ex)
            {
                Console.WriteLine("[RabbitMQ.Message.Client]:Handle RPC reply queue exception.");
                Console.WriteLine(ex.Message);
            }

            return false;
        }

        #endregion
    }
}
