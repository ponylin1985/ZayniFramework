namespace ZayniFramework.Serialization
{
    /// <summary>序列化引擎種類
    /// </summary>
    public enum SerializerType
    {
        /// <summary>使用 System.Text.Json 的 JSON 序列化處理引擎，回傳 JsonSerialize
        /// </summary>
        SystemTextJson,

        /// <summary>使用 Newtonsoft.Json 的 JSON 序列化處理引擎，回傳 JsonNetSerializer
        /// </summary>
        NewtonsoftJson,

        /// <summary>XML 序列化處理引擎，回傳 XmlSerializer
        /// </summary>
        Xml,

        /// <summary>使用 YamlDotNet 的 YAML 序列化處理引擎，回傳 YamlSerializer
        /// </summary>
        YamlDotNet,
    }
}