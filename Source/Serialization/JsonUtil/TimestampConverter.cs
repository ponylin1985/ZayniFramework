﻿using Newtonsoft.Json;
using System;
using ZayniFramework.Common;


namespace ZayniFramework.Serialization
{
    /// <summary>DateTime 與 UNIX Timestamp 的 JSON 轉換器
    /// </summary>
    public class TimestampConverter : JsonConverter
    {
        #region 宣告建構子

        /// <summary>預設建構子
        /// </summary>
        public TimestampConverter()
        {
            WithMilliseconds = false;
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="withMilliseconds">時間戳記最小單位是否包含毫秒數</param>
        public TimestampConverter(bool withMilliseconds)
        {
            WithMilliseconds = withMilliseconds;
        }

        #endregion 宣告建構子


        #region 宣告公開的屬性

        /// <summary>時間戳記最小單位是否包含毫秒數，預設不包含
        /// </summary>
        public bool WithMilliseconds { get; set; }

        #endregion 宣告公開的屬性


        #region 覆寫方法

        /// <summary>檢查是否可以轉換
        /// </summary>
        /// <param name="objectType">欄位的型別</param>
        /// <returns>是否可以轉換</returns>
        public override bool CanConvert(Type objectType) => objectType == typeof(DateTime);

        /// <summary>讀取 JSON 格式字串: 讀取 Unix Timestamp 時間戳記，反序列化後成 DateTime 資料。
        /// </summary>
        /// <param name="reader">JSON 字串讀取器</param>
        /// <param name="objectType">資料的型別</param>
        /// <param name="existingValue"></param>
        /// <param name="serializer">JSON 序列化物件</param>
        /// <returns>反序列化後的資料值</returns>
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, Newtonsoft.Json.JsonSerializer serializer)
        {
            if (reader.Value is not long timestamp)
            {
                return reader.Value;
            }

            return timestamp.ToDateTimeFromUnixTimestamp(WithMilliseconds);
        }

        /// <summary>輸出 JSON 格式字串: DateTime 序列化後轉換成 Unix Timestamp 時間戳記。
        /// </summary>
        /// <param name="writer">JSON 字串輸出器</param>
        /// <param name="value">原始資料值</param>
        /// <param name="serializer">JSON 序列化物件</param>
        public override void WriteJson(JsonWriter writer, object value, Newtonsoft.Json.JsonSerializer serializer)
        {
            if (value is not DateTime dt)
            {
                return;
            }

            var timestamp = dt.ToUnixUtcTimestamp(WithMilliseconds);
            writer.WriteValue(timestamp);
        }

        #endregion 覆寫方法
    }
}
