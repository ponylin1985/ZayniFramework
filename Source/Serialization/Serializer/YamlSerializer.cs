using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;


namespace ZayniFramework.Serialization
{
    /// <summary>YAML 序列化處理器<para/>
    /// * 使用 YamlDotNet 套件的 YAML 序列化處理器。<para/>
    /// * 預設 YamlDoNet 的 ISerializer 物件設置如下: <para/>
    /// * Serializer 物件: WithNamingConvention( CamelCaseNamingConvention.Instance )<para/>
    /// * Deserializer 物件: WithNamingConvention( CamelCaseNamingConvention.Instance )
    /// </summary>
    public sealed class YamlSerializer : ZayniFramework.Common.ISerializer
    {
        /// <summary>YAML 序列化處理器
        /// </summary>
        private static readonly YamlDotNet.Serialization.ISerializer _serializer;

        /// <summary>YAML 反序列化處理器
        /// </summary>
        private static readonly YamlDotNet.Serialization.IDeserializer _deserializer;

        /// <summary>靜態建構子
        /// </summary>
        static YamlSerializer()
        {
            _serializer =
                new SerializerBuilder()
                    .WithNamingConvention(CamelCaseNamingConvention.Instance)
                    .Build();

            _deserializer =
                new DeserializerBuilder()
                    .WithNamingConvention(CamelCaseNamingConvention.Instance)
                    .Build();
        }

        /// <summary>序列化物件，回傳值為 YAML 字串<para/>
        /// * 預設 YamlDoNet 的 ISerializer 物件設置為: WithNamingConvention( CamelCaseNamingConvention.Instance )<para/>
        /// </summary>
        /// <param name="obj">目標序列化物件</param>
        /// <returns>序列化結果 YAML 字串</returns>
        public object Serialize(object obj) => SerializeObject(obj);

        /// <summary>將物件序列化成 YAML 格式字串。<para/>
        /// * 預設 YamlDoNet 的 ISerializer 物件設置為: WithNamingConvention( CamelCaseNamingConvention.Instance )<para/>
        /// </summary>
        /// <param name="obj">目標序列化物件</param>
        /// <returns>序列化結果 YAML 字串</returns>
        public static string SerializeObject(object obj) => _serializer.Serialize(obj);

        /// <summary>反序列化 YAML 字串<para/>
        /// * 預設 YamlDoNet 的 ISerializer 物件設置為: WithNamingConvention( CamelCaseNamingConvention.Instance )<para/>
        /// </summary>
        /// <param name="obj">來源 YAML 字串</param>
        /// <typeparam name="TResult">反序列化的物件泛型</typeparam>
        /// <returns>反序列化後的物件</returns>
        public TResult Deserialize<TResult>(object obj) => DeserializeObject<TResult>(obj + "");

        /// <summary>將來源的 YAML 格式字串反序列化成物件<para/>
        /// * 預設 YamlDoNet 的 ISerializer 物件設置為: WithNamingConvention( CamelCaseNamingConvention.Instance )<para/>
        /// </summary>
        /// <param name="yaml">來源 YAML 字串</param>
        /// <typeparam name="TResult">反序列化的物件泛型</typeparam>
        /// <returns>反序列化後的物件</returns>
        public static TResult DeserializeObject<TResult>(string yaml) => _deserializer.Deserialize<TResult>(yaml);
    }
}