using System.Text.Json;
using System.Text.Json.Serialization;
using ZayniFramework.Common;


namespace ZayniFramework.Serialization;


/// <summary>System.Text.Json 的 JsonSerializer 序列化處理器。<para/>
/// <para>  * 在 .NET Core 3.0 與 .NET Core 3.1 之後提供的高效能 JSON 序列化元件。</para>
/// <para>  * 對於 JSON 序列化機制有最高的效能表現，佔用的記憶體內存資源也最為節省。</para>
/// </summary>
public sealed class JsonSerialize : ISerializer
{
    /// <summary>預設的 JSON 序列化設定
    /// </summary>
    /// <value></value>
    private static readonly JsonSerializerOptions _serializeOptions = new()
    {
        WriteIndented = true,
        AllowTrailingCommas = true,
        DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull,
    };

    /// <summary>序列化物件，回傳值為 JSON 字串
    /// </summary>
    /// <param name="obj">目標序列化物件</param>
    /// <returns>序列化結果 JSON 字串</returns>
    public object Serialize(object obj)
    {
        return JsonSerializer.Serialize(obj);
    }

    /// <summary>序列化物件，回傳值為 JSON 字串。<para/>
    /// <para>  * 以下為預設的 JsonSerializerOptions 設定值。</para>
    /// <para>  * WriteIndented = true</para>
    /// <para>  * IgnoreNullValues = true</para>
    /// <para>  * AllowTrailingCommas = true</para>
    /// </summary>
    /// <param name="obj">目標序列化物件</param>
    /// <returns>序列化結果 JSON 字串</returns>
    public static string SerializeObject(object obj)
    {
        return JsonSerializer.Serialize(obj, _serializeOptions);
    }

    /// <summary>反序列化 JSON 字串
    /// </summary>
    /// <param name="obj">JSON 字串</param>
    /// <typeparam name="TResult">反序列化的物件泛型</typeparam>
    /// <returns>反序列化後的物件</returns>
    public TResult Deserialize<TResult>(object obj)
    {
        return JsonSerializer.Deserialize<TResult>(obj + "");
    }

    /// <summary>反序列化 JSON 字串<para/>
    /// <para>  * 以下為預設的 JsonSerializerOptions 設定值。</para>
    /// <para>  * ReadCommentHandling = JsonCommentHandling.Allow</para>
    /// </summary>
    /// <param name="json">JSON 字串</param>
    /// <returns>反序列化後的物件</returns>
    public static object DeserializeObject(string json)
    {
        return JsonSerializer.Deserialize(json, typeof(object));
    }

    /// <summary>反序列化 JSON 字串<para/>
    /// <para>  * 以下為預設的 JsonSerializerOptions 設定值。</para>
    /// <para>  * ReadCommentHandling = JsonCommentHandling.Allow</para>
    /// </summary>
    /// <param name="json">JSON 字串</param>
    /// <typeparam name="TResult">反序列化的物件泛型</typeparam>
    /// <returns>反序列化後的物件</returns>
    public static TResult DeserializeObject<TResult>(string json)
    {
        return JsonSerializer.Deserialize<TResult>(json, _serializeOptions);
    }
}
