using Newtonsoft.Json;
using ZayniFramework.Common;


namespace ZayniFramework.Serialization
{
    /// <summary>Newtonsoft Json.NET 序列化處理器
    /// </summary>
    public sealed class JsonNetSerializer : ISerializer
    {
        /// <summary>序列化物件，回傳值為 JSON 字串
        /// </summary>
        /// <param name="obj">目標序列化物件</param>
        /// <returns>序列化結果 JSON 字串</returns>
        public object Serialize(object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }

        /// <summary>序列化物件，回傳值為 JSON 字串
        /// </summary>
        /// <param name="obj">目標序列化物件</param>
        /// <returns>序列化結果 JSON 字串</returns>
        public static string SerializeObject(object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }

        /// <summary>反序列化 JSON 字串
        /// </summary>
        /// <param name="obj">JSON 字串</param>
        /// <typeparam name="TResult">反序列化的物件泛型</typeparam>
        /// <returns>反序列化後的物件</returns>
        public TResult Deserialize<TResult>(object obj)
        {
            return JsonConvert.DeserializeObject<TResult>(obj + "");
        }

        /// <summary>反序列化 JSON 字串
        /// </summary>
        /// <param name="json">JSON 字串</param>
        /// <typeparam name="TResult">反序列化的物件泛型</typeparam>
        /// <returns>反序列化後的物件</returns>
        public static TResult DeserializeObject<TResult>(string json)
        {
            return JsonConvert.DeserializeObject<TResult>(json);
        }
    }
}