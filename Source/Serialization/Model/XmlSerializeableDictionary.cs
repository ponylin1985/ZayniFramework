﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using ZayniFramework.Common;


namespace ZayniFramework.Serialization
{
    /// <summary>可進行 XML 序列化的字典資料集合
    /// </summary>
    /// <typeparam name="TKey">索引泛型</typeparam>
    /// <typeparam name="TValue">資料質泛型</typeparam>
    public sealed class XmlSerializableDictionary<TKey, TValue> : Dictionary<TKey, TValue>, IXmlSerializable
    {
        #region 實作IXmlSerializable介面方法

        /// <summary>讀取 XML 綱要 (此方法尚未實作)
        /// </summary>
        /// <returns>XML 綱要</returns>
        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        /// <summary>將 XML 字串反序列化為字典物件
        /// </summary>
        /// <param name="reader">XML 讀取器</param>
        public void ReadXml(XmlReader reader)
        {
            var key = default(TKey);
            var value = default(TValue);

            if (reader.IsNull() || reader.IsEmptyElement)
            {
                return;
            }


            while (reader.Read())
            {
                if (XmlNodeType.Element != reader.NodeType)
                {
                    continue;
                }

                if ("Item" == reader.LocalName)
                {
                    var keyXml = reader.GetAttribute("Key");
                    var valueXml = reader.GetAttribute("Value");

                    if (new string[] { keyXml, valueXml }.Any(y => y.IsNullOrEmpty()))
                    {
                        continue;
                    }

                    key = (TKey)DeserializeXml(keyXml, typeof(TKey));
                    value = (TValue)DeserializeXml(valueXml, typeof(TValue));

                    if (!base.ContainsKey(key))
                    {
                        base.Add(key, value);
                    }
                }
            }
        }

        /// <summary>將字典物件序列化成 XML 字串
        /// </summary>
        /// <param name="writer">XML 寫入器</param>
        public void WriteXml(XmlWriter writer)
        {
            foreach (var key in Keys)
            {
                writer.WriteStartElement("Item");

                var obj = this[key];

                if (obj.IsNull())
                {
                    continue;
                }

                var keyXml = SerializeObject(key);
                var valueXml = SerializeObject(obj);

                writer.WriteAttributeString("Key", keyXml);
                writer.WriteAttributeString("Value", valueXml);
                writer.WriteEndElement();
            }
        }

        #endregion 實作IXmlSerializable介面方法


        #region 宣告私有的序列化方法

        /// <summary>將目標物件序列化成 XML 字串
        /// </summary>
        /// <param name="target">目標物件</param>
        /// <returns>XML 字串</returns>
        private static string SerializeObject(object target)
        {
            var serializer = new XmlSerializerComponent();
            var sResult = serializer.Serialize(target, target.GetType());

            if (!sResult.Success)
            {
                return string.Empty;
            }

            var result = sResult.SerializedString;
            return result;
        }

        /// <summary>將目標 XML 字串反序列化成物件
        /// </summary>
        /// <param name="target">目標 XML 字串</param>
        /// <param name="type">物件型別</param>
        /// <returns>物件實體</returns>
        private static object DeserializeXml(string target, Type type)
        {
            var serializer = new XmlSerializerComponent();
            var dResult = serializer.Deserialize(target, type);

            if (!dResult.Success)
            {
                return null;
            }

            var result = dResult.Datas;
            return result;
        }

        #endregion 宣告私有的序列化方法
    }
}
