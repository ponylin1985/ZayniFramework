﻿using System.Collections.Generic;
using ZayniFramework.Common;


namespace ZayniFramework.DataAccess
{
    /// <summary>資料存取模組設定值
    /// </summary>
    internal static class DataAccessSettingsOption
    {
        /// <summary>資料存取模組 Config 設定值
        /// </summary>
        internal static List<DataAccessSetting> Settings { get; set; }
    }
}
