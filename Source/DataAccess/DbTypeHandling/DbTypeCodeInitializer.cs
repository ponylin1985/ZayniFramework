﻿using MySql.Data.MySqlClient;
using NpgsqlTypes;
using Oracle.ManagedDataAccess.Client;
using System.Data;


namespace ZayniFramework.DataAccess
{
    /// <summary>資料庫型別代碼初始化元件
    /// </summary>
    internal static class DbColumnTypeInitializer
    {
        #region 宣告內部的方法

        /// <summary>設定 MSSQL 資料庫的資料庫型別代碼
        /// </summary>
        /// <param name="codeCreater">資料庫型別代碼建立者</param>
        internal static void SetSqlDbColumnType(IDbColumnTypeCreater codeCreater)
        {
            SetSqlDbTypeByDefault();

            // DbColumnTypeMappingElement DbColumnTypeMapping = DataAccessSettingsOption.Settings.DbColumnTypeMapping;

            // if (DbColumnTypeMapping.IsNull())
            // {
            //     SetSqlDbTypeByDefault();
            // }

            // if (!SetDbColumnTypeFromConfig<SqlDbTypeConfigCollection>(DbColumnTypeMapping, _sqlDbColumnTypes, "SqlDbTypeManpping", "MSSQL", codeCreater))
            // {
            //     SetSqlDbTypeByDefault();
            // }
        }

        /// <summary>設定 Oracle 資料庫的資料庫型別代碼
        /// </summary>
        /// <param name="codeCreater">資料庫型別代碼建立者</param>
        internal static void SetOracleDbColumnType(IDbColumnTypeCreater codeCreater)
        {
            SetOracleDbTypeByDefault();

            // DbColumnTypeMappingElement DbColumnTypeMapping = DataAccessSettingsOption.Settings.DbColumnTypeMapping;

            // if (DbColumnTypeMapping.IsNull())
            // {
            //     SetOracleDbTypeByDefault();
            // }

            // if (!SetDbColumnTypeFromConfig<OracleTypeConfigCollection>(DbColumnTypeMapping, _orlaceDbTypes, "OracleDbTypeMapping", "Oracle", codeCreater))
            // {
            //     SetOracleDbTypeByDefault();
            // }
        }

        /// <summary>設定 MySQL 資料庫的資料庫型別代碼
        /// </summary>
        /// <param name="codeCreater">資料庫型別代碼建立者</param>
        internal static void SetMySqlDbColumnType(IDbColumnTypeCreater codeCreater)
        {
            SetMySqlDbTypeByDefault();

            // DbColumnTypeMappingElement DbColumnTypeMapping = DataAccessSettingsOption.Settings.DbColumnTypeMapping;

            // if (DbColumnTypeMapping.IsNull())
            // {
            //     SetMySqlDbTypeByDefault();
            // }

            // if (!SetDbColumnTypeFromConfig<MySqlDbTypeConfigCollection>(DbColumnTypeMapping, _mySqlDbTypes, "MySqlDbTypeMapping", "MySQL", codeCreater))
            // {
            //     SetMySqlDbTypeByDefault();
            // }
        }

        /// <summary>設定 PostgreSQL 資料庫的資料庫型別代碼
        /// </summary>
        /// <param name="codeCreater">資料庫型別代碼建立者</param>
        internal static void SetPgSqlDbColumnType(IDbColumnTypeCreater codeCreater)
        {
            SetPgSqlDbTypeByDefault();

            // DbColumnTypeMappingElement DbColumnTypeMapping = DataAccessSettingsOption.Settings.DbColumnTypeMapping;

            // if (DbColumnTypeMapping.IsNull())
            // {
            //     SetPgSqlDbTypeByDefault();
            // }

            // if (!SetDbColumnTypeFromConfig<MySqlDbTypeConfigCollection>(DbColumnTypeMapping, _pgSqlDbTypes, "PgSqlDbTypeMapping", "PostgreSQL", codeCreater))
            // {
            //     SetPgSqlDbTypeByDefault();
            // }
        }

        #endregion 宣告內部的方法


        #region 宣告私有的方法

        // /// <summary>使用 Config 組態值進行設定 DbColumnType 代碼
        // /// </summary>
        // /// <typeparam name="TCollection">資料庫型別對應 Config 區段泛型</typeparam>
        // /// <param name="dbTypeMapping">資料庫型別對應 Config 元素</param>
        // /// <param name="sectionName">MSSQL、MySQL 或 PostgreSQL 資料庫 Config 設定區段</param>
        // /// <param name="dbTypeStrings">資料庫的型別代碼字串</param>
        // /// <param name="dbProviderName">MSSQL、MySQL 或 PostgreSQL 資料庫供應商名稱</param>
        // /// <param name="codeCreater">資料庫型別代碼建立者</param>
        // private static bool SetDbColumnTypeFromConfig<TCollection>(DbColumnTypeMappingElement dbTypeMapping, string[] dbTypeStrings, string sectionName,
        //         string dbProviderName, IDbColumnTypeCreater codeCreater)
        //     where TCollection : ConfigurationElementCollection
        // {
        //     #region 反射取得 Config 設定區段集合

        //     TCollection mappings = default(TCollection);

        //     try
        //     {
        //         mappings = dbTypeMapping.GetType().GetProperty(sectionName).GetValue(dbTypeMapping) as TCollection;
        //     }
        //     catch (Exception ex)
        //     {
        //         Logger.Exception("DbColumnTypeInitializer", ex, $"ZayniFramework.DataAccess set DbColumnType from config occur exception. {ex}", "DbColumnTypeInitializer.SetDbColumnTypeFromConfig");
        //         return false;
        //     }

        //     // 20141212 Edited by Pony: 這邊應該還要檢查Config區段的數量，如果是0，代表應該要回傳false，讓DbColumnType使用框架預設值
        //     if (mappings.IsNull() || 0 == mappings.Count)
        //     {
        //         return false;
        //     }

        //     #endregion 反射取得 Config 設定區段集合

        //     #region 設定 DbColumnType 資料庫型別代碼

        //     foreach (var s in mappings)
        //     {
        //         #region 檢查 Config 的型別設定值是否存在

        //         DbTypeMappingElement mapping = (DbTypeMappingElement)s;

        //         string name = mapping.Name + "";
        //         string value = mapping.Value + "";

        //         if (value.IsNullOrEmpty())
        //         {
        //             continue;
        //         }

        //         if (!dbTypeStrings.Contains(value))
        //         {
        //             continue;
        //         }

        //         #endregion 檢查 Config 的型別設定值是否存在

        //         #region 設定 DbColumnType 代碼

        //         try
        //         {
        //             var typeCode = codeCreater.Create(value);
        //             var field = typeof(DbColumnType).GetFields().Where(f => f.Name == name).ToList().FirstOrDefault();
        //             field.SetValue(null, typeCode);
        //         }
        //         catch (Exception ex)
        //         {
        //             Logger.Exception("DbColumnTypeInitializer", ex, $"ZayniFramework.DataAccess set DbColumnType from config occur exception. {ex}", "DbColumnTypeInitializer.SetDbColumnTypeFromConfig");
        //             return false;
        //         }

        //         #endregion 設定 DbColumnType 代碼
        //     }

        //     #endregion 設定 DbColumnType 資料庫型別代碼

        //     return true;
        // }

        /// <summary>使用程式預設值進行設定 MSSQL 資料庫的 DbColumnType 代碼
        /// </summary>
        private static void SetSqlDbTypeByDefault()
        {
            DbColumnType.Binary = (int)SqlDbType.Binary;
            DbColumnType.Byte = (int)SqlDbType.TinyInt;
            DbColumnType.Boolean = (int)SqlDbType.Bit;
            DbColumnType.Date = (int)SqlDbType.Date;
            DbColumnType.DateTime = (int)SqlDbType.DateTime;

            DbColumnType.DateTime2 = (int)SqlDbType.DateTime2;
            DbColumnType.DateTimeOffset = (int)SqlDbType.DateTimeOffset;
            DbColumnType.Double = (int)SqlDbType.Float;
            DbColumnType.Decimal = (int)SqlDbType.Decimal;
            DbColumnType.Int16 = (int)SqlDbType.SmallInt;

            DbColumnType.Int32 = (int)SqlDbType.Int;
            DbColumnType.Int64 = (int)SqlDbType.BigInt;
            DbColumnType.Guid = (int)SqlDbType.UniqueIdentifier;
            DbColumnType.Object = (int)SqlDbType.Variant;
            DbColumnType.String = (int)SqlDbType.NVarChar;

            DbColumnType.Time = (int)SqlDbType.Time;
            DbColumnType.Xml = (int)SqlDbType.Xml;
        }

        /// <summary>使用程式預設值進行設定 Oracle 資料庫的 DbColumnType 代碼
        /// </summary>
        private static void SetOracleDbTypeByDefault()
        {
            DbColumnType.Binary = (int)OracleDbType.Blob;
            DbColumnType.Byte = (int)OracleDbType.Byte;
            DbColumnType.Boolean = (int)OracleDbType.Int32;
            DbColumnType.Date = (int)OracleDbType.Date;
            DbColumnType.DateTime = (int)OracleDbType.Date;

            DbColumnType.Double = (int)OracleDbType.Double;
            DbColumnType.Decimal = (int)OracleDbType.Decimal;
            DbColumnType.Int16 = (int)OracleDbType.Int16;
            DbColumnType.Int32 = (int)OracleDbType.Int32;
            DbColumnType.Int64 = (int)OracleDbType.Int64;

            DbColumnType.Guid = (int)OracleDbType.Varchar2;
            DbColumnType.Object = (int)OracleDbType.Blob;
            DbColumnType.String = (int)OracleDbType.Varchar2;
            DbColumnType.Time = (int)OracleDbType.TimeStamp;
            DbColumnType.Xml = (int)OracleDbType.Varchar2;
        }

        /// <summary>使用程式預設值進行設定 MySQL 資料庫的 DbColumnType 代碼
        /// </summary>
        private static void SetMySqlDbTypeByDefault()
        {
            DbColumnType.Binary = (int)MySqlDbType.Binary;
            DbColumnType.Byte = (int)MySqlDbType.Byte;
            DbColumnType.Boolean = (int)MySqlDbType.Int32;
            DbColumnType.Date = (int)MySqlDbType.Date;
            DbColumnType.DateTime = (int)MySqlDbType.DateTime;

            DbColumnType.DateTimeOffset = (int)MySqlDbType.DateTime;
            DbColumnType.Double = (int)MySqlDbType.Float;
            DbColumnType.Decimal = (int)MySqlDbType.Decimal;
            DbColumnType.Int16 = (int)MySqlDbType.Int16;
            DbColumnType.Int32 = (int)MySqlDbType.Int32;

            DbColumnType.Int64 = (int)MySqlDbType.Int64;
            DbColumnType.Guid = (int)MySqlDbType.Guid;
            DbColumnType.Object = (int)MySqlDbType.Blob;
            DbColumnType.String = (int)MySqlDbType.VarChar;
            DbColumnType.Time = (int)MySqlDbType.Time;

            DbColumnType.Xml = (int)MySqlDbType.VarChar;
        }

        /// <summary>使用程式預設值進行設定 PostgreSQL 資料庫的 DbColumnType 代碼
        /// </summary>
        private static void SetPgSqlDbTypeByDefault()
        {
            DbColumnType.Binary = (int)NpgsqlDbType.Bytea;
            DbColumnType.Byte = (int)NpgsqlDbType.Smallint;
            DbColumnType.Boolean = (int)NpgsqlDbType.Boolean;
            DbColumnType.Date = (int)NpgsqlDbType.TimestampTz;
            DbColumnType.DateTime = (int)NpgsqlDbType.TimestampTz;

            DbColumnType.DateTimeOffset = (int)NpgsqlDbType.TimestampTz;
            DbColumnType.Double = (int)NpgsqlDbType.Numeric;
            DbColumnType.Decimal = (int)NpgsqlDbType.Numeric;
            DbColumnType.Int16 = (int)NpgsqlDbType.Smallint;
            DbColumnType.Int32 = (int)NpgsqlDbType.Integer;

            DbColumnType.Int64 = (int)NpgsqlDbType.Bigint;
            DbColumnType.Guid = (int)NpgsqlDbType.Uuid;
            DbColumnType.String = (int)NpgsqlDbType.Varchar;
            DbColumnType.Time = (int)NpgsqlDbType.Time;
            DbColumnType.Xml = (int)NpgsqlDbType.Xml;
        }

        #endregion 宣告私有的方法
    }
}
