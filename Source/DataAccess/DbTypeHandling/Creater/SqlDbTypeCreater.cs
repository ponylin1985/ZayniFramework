﻿using System;
using System.Data;


namespace ZayniFramework.DataAccess
{
    /// <summary>MSSQL資料型別代碼建立者
    /// </summary>
    internal sealed class SqlDbColumnTypeCreater : IDbColumnTypeCreater
    {
        /// <summary>建立資料庫行別代碼
        /// </summary>
        /// <param name="source">資料庫行別代碼字串</param>
        /// <returns>數字資料庫行別代碼</returns>
        public int Create(string source)
        {
            var result = (int)CreateCode(source);
            return result;
        }

        /// <summary>建立MSSQL資料庫行別代碼
        /// </summary>
        /// <param name="source">MSSQL資料庫行別代碼字串</param>
        /// <returns>MSSQL資料庫行別代碼</returns>
        private static SqlDbType CreateCode(string source)
        {
            return source switch
            {
                "BigInt" => SqlDbType.BigInt,
                "Binary" => SqlDbType.Binary,
                "Bit" => SqlDbType.Bit,
                "Char" => SqlDbType.Char,
                "DateTime" => SqlDbType.DateTime,
                "Decimal" => SqlDbType.Decimal,
                "Float" => SqlDbType.Float,
                "Image" => SqlDbType.Image,
                "Int" => SqlDbType.Int,
                "Money" => SqlDbType.Money,
                "NChar" => SqlDbType.NChar,
                "NText" => SqlDbType.NText,
                "NVarChar" => SqlDbType.NVarChar,
                "Real" => SqlDbType.Real,
                "UniqueIdentifier" => SqlDbType.UniqueIdentifier,
                "SmallDateTime" => SqlDbType.SmallDateTime,
                "SmallInt" => SqlDbType.SmallInt,
                "SmallMoney" => SqlDbType.SmallMoney,
                "Text" => SqlDbType.Text,
                "Timestamp" => SqlDbType.Timestamp,
                "TinyInt" => SqlDbType.TinyInt,
                "VarBinary" => SqlDbType.VarBinary,
                "VarChar" => SqlDbType.VarChar,
                "Variant" => SqlDbType.Variant,
                "Xml" => SqlDbType.Xml,
                "Udt" => SqlDbType.Udt,
                "Structured" => SqlDbType.Structured,
                "Date" => SqlDbType.Date,
                "Time" => SqlDbType.Time,
                "DateTime2" => SqlDbType.DateTime2,
                "DateTimeOffset" => SqlDbType.DateTimeOffset,
                _ => throw new ApplicationException($"建立 MSSQL 資料庫行別代碼對應失敗，請確認 MSSQL 列舉值中是否確實有 {source} 的項目"),
            };
        }
    }
}
