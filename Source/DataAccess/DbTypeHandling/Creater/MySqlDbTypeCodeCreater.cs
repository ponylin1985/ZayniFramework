﻿using MySql.Data.MySqlClient;
using System;


namespace ZayniFramework.DataAccess
{
    /// <summary>MySQL 資料型別代碼建立者
    /// </summary>
    internal sealed class MySqlDbColumnTypeCreater : IDbColumnTypeCreater
    {
        /// <summary>建立資料庫型別代碼
        /// </summary>
        /// <param name="source">資料庫型別代碼字串</param>
        /// <returns>數字資料庫型別代碼</returns>
        public int Create(string source)
        {
            var result = (int)CreateCode(source);
            return result;
        }

        /// <summary>建立 MySQL 資料庫型別代碼
        /// </summary>
        /// <param name="source"> MySQL 資料庫型別代碼字串</param>
        /// <returns> MySQL 資料庫型別代碼</returns>
        private static MySqlDbType CreateCode(string source)
        {
            return source switch
            {
                "Binary" => MySqlDbType.Binary,
                "Bit" => MySqlDbType.Bit,
                "Decimal" => MySqlDbType.Decimal,
                "Byte" => MySqlDbType.Byte,
                "Int16" => MySqlDbType.Int16,
                "Int24" => MySqlDbType.Int24,
                "Int32" => MySqlDbType.Int32,
                "Int64" => MySqlDbType.Int64,
                "Float" => MySqlDbType.Float,
                "Double" => MySqlDbType.Double,
                "Timestamp" => MySqlDbType.Timestamp,
                "Date" => MySqlDbType.Date,
                "Time" => MySqlDbType.Time,
                "DateTime" => MySqlDbType.DateTime,
                "Year" => MySqlDbType.Year,
                "Newdate" => MySqlDbType.Newdate,
                "VarString" => MySqlDbType.VarString,
                "JSON" => MySqlDbType.JSON,
                "NewDecimal" => MySqlDbType.NewDecimal,
                "Enum" => MySqlDbType.Enum,
                "Set" => MySqlDbType.Set,
                "TinyBlob" => MySqlDbType.TinyBlob,
                "MediumBlob" => MySqlDbType.MediumBlob,
                "LongBlob" => MySqlDbType.LongBlob,
                "Blob" => MySqlDbType.Blob,
                "VarChar" => MySqlDbType.VarChar,
                "String" => MySqlDbType.String,
                "UByte" => MySqlDbType.UByte,
                "UInt16" => MySqlDbType.UInt16,
                "UInt32" => MySqlDbType.UInt32,
                "UInt64" => MySqlDbType.UInt64,
                "UInt24" => MySqlDbType.UInt24,
                "VarBinary" => MySqlDbType.VarBinary,
                "TinyText" => MySqlDbType.TinyText,
                "MediumText" => MySqlDbType.MediumText,
                "LongText" => MySqlDbType.LongText,
                "Text" => MySqlDbType.Text,
                "Guid" => MySqlDbType.Guid,
                _ => throw new ArgumentException($"Create MySQL DbColumnType occur error. Source: {source}."),
            };
        }
    }
}
