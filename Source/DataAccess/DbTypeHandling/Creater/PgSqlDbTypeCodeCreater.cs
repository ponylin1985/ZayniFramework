using NpgsqlTypes;
using System;


namespace ZayniFramework.DataAccess
{
    /// <summary>PostgreSQL 資料型別代碼建立者
    /// </summary>
    public class PgSqlDbColumnTypeCreater : IDbColumnTypeCreater
    {
        /// <summary>建立資料庫型別代碼
        /// </summary>
        /// <param name="source">資料庫型別代碼字串</param>
        /// <returns>數字資料庫型別代碼</returns>
        public int Create(string source) => (int)CreateCode(source);

        /// <summary>建立 PostgreSQL 資料庫型別代碼
        /// </summary>
        /// <param name="source"> PostgreSQL 資料庫型別代碼字串</param>
        /// <returns> PostgreSQL 資料庫型別代碼</returns>
        private static NpgsqlDbType CreateCode(string source)
        {
            switch (source)
            {
                case "Array":
                    return NpgsqlDbType.Array;

                case "Bigint":
                    return NpgsqlDbType.Bigint;

                case "Boolean":
                    return NpgsqlDbType.Boolean;

                case "Box":
                    return NpgsqlDbType.Box;

                case "Bytea":
                    return NpgsqlDbType.Bytea;

                case "Circle":
                    return NpgsqlDbType.Circle;

                case "Char":
                    return NpgsqlDbType.Char;

                case "Date":
                    return NpgsqlDbType.Date;

                case "Double":
                    return NpgsqlDbType.Double;

                case "Integer":
                    return NpgsqlDbType.Integer;

                case "Line":
                    return NpgsqlDbType.Line;

                case "LSeg":
                    return NpgsqlDbType.LSeg;

                case "Money":
                    return NpgsqlDbType.Money;

                case "Numeric":
                    return NpgsqlDbType.Numeric;

                case "Path":
                    return NpgsqlDbType.Path;

                case "Point":
                    return NpgsqlDbType.Point;

                case "Polygon":
                    return NpgsqlDbType.Polygon;

                case "Real":
                    return NpgsqlDbType.Polygon;

                case "Smallint":
                    return NpgsqlDbType.Smallint;

                case "Text":
                    return NpgsqlDbType.Text;

                case "Time":
                    return NpgsqlDbType.Time;

                case "Timestamp":
                    return NpgsqlDbType.Timestamp;

                case "Varchar":
                    return NpgsqlDbType.Varchar;

                case "Refcursor":
                    return NpgsqlDbType.Refcursor;

                case "Inet":
                    return NpgsqlDbType.Inet;

                case "Bit":
                    return NpgsqlDbType.Bit;

                // case "TimestampTZ":
                //     return NpgsqlDbType.TimestampTZ;

                case "TimestampTz":
                    return NpgsqlDbType.TimestampTz;

                case "Uuid":
                    return NpgsqlDbType.Uuid;

                case "Xml":
                    return NpgsqlDbType.Xml;

                case "Oidvector":
                    return NpgsqlDbType.Oidvector;

                case "Interval":
                    return NpgsqlDbType.Interval;

                // case "TimeTZ":
                //     return NpgsqlDbType.TimeTZ;

                case "TimeTz":
                    return NpgsqlDbType.TimeTz;

                case "Name":
                    return NpgsqlDbType.Name;

                // case "Abstime":
                //     return NpgsqlDbType.Abstime;

                case "MacAddr":
                    return NpgsqlDbType.MacAddr;

                case "Json":
                    return NpgsqlDbType.Json;

                case "Jsonb":
                    return NpgsqlDbType.Jsonb;

                case "Hstore":
                    return NpgsqlDbType.Hstore;

                case "InternalChar":
                    return NpgsqlDbType.InternalChar;

                case "Varbit":
                    return NpgsqlDbType.Varbit;

                case "Unknown":
                    return NpgsqlDbType.Unknown;

                case "Oid":
                    return NpgsqlDbType.Oid;

                case "Xid":
                    return NpgsqlDbType.Xid;

                case "Cid":
                    return NpgsqlDbType.Cid;

                case "Cidr":
                    return NpgsqlDbType.Cidr;

                case "TsVector":
                    return NpgsqlDbType.TsVector;

                case "TsQuery":
                    return NpgsqlDbType.TsQuery;

                case "Regtype":
                    return NpgsqlDbType.Regtype;

                case "Geometry":
                    return NpgsqlDbType.Geometry;

                case "Citext":
                    return NpgsqlDbType.Citext;

                case "Int2Vector":
                    return NpgsqlDbType.Int2Vector;

                case "Tid":
                    return NpgsqlDbType.Tid;

                case "MacAddr8":
                    return NpgsqlDbType.MacAddr8;

                case "Geography":
                    return NpgsqlDbType.Geography;

                case "Regconfig":
                    return NpgsqlDbType.Regconfig;

                case "Range":
                    return NpgsqlDbType.Range;

                default:
                    throw new ArgumentException($"Create PostgreSQL DbColumnType occur error. Source: {source}.");
            }
        }
    }
}