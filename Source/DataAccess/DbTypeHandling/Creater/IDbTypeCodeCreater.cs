﻿namespace ZayniFramework.DataAccess
{
    /// <summary>資料庫型別代碼建立者合約
    /// </summary>
    internal interface IDbColumnTypeCreater
    {
        /// <summary>建立資料庫行別代碼
        /// </summary>
        /// <param name="source">資料庫行別代碼字串</param>
        /// <returns>數字資料庫行別代碼</returns>
        int Create(string source);
    }
}
