﻿using Oracle.ManagedDataAccess.Client;
using System;


namespace ZayniFramework.DataAccess
{
    /// <summary>Oracle 資料型別代碼建立者
    /// </summary>
    internal sealed class OracleDbColumnTypeCreater : IDbColumnTypeCreater
    {
        /// <summary>建立資料庫型別代碼
        /// </summary>
        /// <param name="source">資料庫型別代碼字串</param>
        /// <returns>數字資料庫型別代碼</returns>
        public int Create(string source) => (int)CreateCode(source);

        /// <summary>建立 PostgreSQL 資料庫型別代碼
        /// </summary>
        /// <param name="source"> PostgreSQL 資料庫型別代碼字串</param>
        /// <returns> PostgreSQL 資料庫型別代碼</returns>
        private static OracleDbType CreateCode(string source)
        {
            switch (source)
            {
                // case "Array":
                //     return OracleDbType.Array;

                case "Bigint":
                    return OracleDbType.Int64;

                case "Boolean":
                    return OracleDbType.Boolean;

                // case "Box":
                //     return OracleDbType.Box;

                // case "Bytea":
                //     return OracleDbType.Bytea;

                // case "Circle":
                //     return OracleDbType.Circle;

                case "Char":
                    return OracleDbType.Char;

                case "Date":
                    return OracleDbType.Date;

                case "Double":
                    return OracleDbType.Double;

                case "Integer":
                    return OracleDbType.Int32;

                // case "Line":
                //     return OracleDbType.Line;

                // case "LSeg":
                //     return OracleDbType.LSeg;

                // case "Money":
                //     return OracleDbType.Money;

                case "Numeric":
                    return OracleDbType.Decimal;

                // case "Path":
                //     return OracleDbType.Path;

                // case "Point":
                //     return OracleDbType.Point;

                // case "Polygon":
                //     return OracleDbType.Polygon;

                // case "Real":
                //     return OracleDbType.Polygon;

                case "Smallint":
                    return OracleDbType.Int16;

                case "Text":
                    return OracleDbType.Varchar2;

                // case "Time":
                //     return OracleDbType.Time;

                case "Timestamp":
                    return OracleDbType.TimeStamp;

                case "Varchar":
                    return OracleDbType.Varchar2;

                // case "Refcursor":
                //     return OracleDbType.Refcursor;

                // case "Inet":
                //     return OracleDbType.Inet;

                case "Bit":
                    return OracleDbType.Boolean;

                // case "TimestampTZ":
                //     return OracleDbType.TimestampTZ;

                // case "TimestampTz":
                //     return OracleDbType.TimestampTz;

                // case "Uuid":
                //     return OracleDbType.Uuid;

                // case "Xml":
                //     return OracleDbType.Xml;

                // case "Oidvector":
                //     return OracleDbType.Oidvector;

                // case "Interval":
                //     return OracleDbType.Interval;

                // case "TimeTZ":
                //     return OracleDbType.TimeTZ;

                // case "TimeTz":
                //     return OracleDbType.TimeTz;

                // case "Name":
                //     return OracleDbType.Name;

                // case "Abstime":
                //     return OracleDbType.Abstime;

                // case "MacAddr":
                //     return OracleDbType.MacAddr;

                // case "Json":
                //     return OracleDbType.Json;

                // case "Jsonb":
                //     return OracleDbType.Jsonb;

                // case "Hstore":
                //     return OracleDbType.Hstore;

                // case "InternalChar":
                //     return OracleDbType.InternalChar;

                // case "Varbit":
                //     return OracleDbType.Varbit;

                // case "Unknown":
                //     return OracleDbType.Unknown;

                // case "Oid":
                //     return OracleDbType.Oid;

                // case "Xid":
                //     return OracleDbType.Xid;

                // case "Cid":
                //     return OracleDbType.Cid;

                // case "Cidr":
                //     return OracleDbType.Cidr;

                // case "TsVector":
                //     return OracleDbType.TsVector;

                // case "TsQuery":
                //     return OracleDbType.TsQuery;

                // case "Regtype":
                //     return OracleDbType.Regtype;

                // case "Geometry":
                //     return OracleDbType.Geometry;

                // case "Citext":
                //     return OracleDbType.Citext;

                // case "Int2Vector":
                //     return OracleDbType.Int2Vector;

                // case "Tid":
                //     return OracleDbType.Tid;

                // case "MacAddr8":
                //     return OracleDbType.MacAddr8;

                // case "Geography":
                //     return OracleDbType.Geography;

                // case "Regconfig":
                //     return OracleDbType.Regconfig;

                // case "Range":
                //     return OracleDbType.Range;

                default:
                    throw new ArgumentException($"Create Oracle DbColumnType occur error. Source: {source}.");
            }
        }
    }
}
