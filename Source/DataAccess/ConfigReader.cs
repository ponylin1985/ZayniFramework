﻿using System;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.DataAccess
{
    /// <summary>ZayniFramework DataAccess 模組的 Config 設定值讀取器
    /// </summary>
    internal static class ConfigReader
    {
        #region Private Fields

        /// <summary>The configuration of ZayniFramework.
        /// </summary>
        /// <returns></returns>
        private static readonly ZayniFrameworkSettings _zayniSettings = ConfigManager.GetZayniFrameworkSettings();

        #endregion Private Fields


        #region Public Methods

        /// <summary>讀取資料庫連線字串
        /// </summary>
        /// <param name="dbName">資料庫連線字串名稱</param>
        /// <returns>讀取結果</returns>
        public static Result<string> GetDbConnectionString(string dbName)
        {
            var result = Result.Create<string>();
            var logTitle = GetLogTitle(nameof(GetDbConnectionString));

            var connectionString = "";

            try
            {
                var dbSetting = _zayniSettings.DataAccessSettings.Find(c => c.Name == dbName);
                connectionString = dbSetting.ConnectionString;

                if (string.IsNullOrEmpty(connectionString))
                {
                    result.Message = $"The connection string of {dbName} is empty.";
                    Logger.Error(nameof(ConfigReader), $"{result.Message}", logTitle);
                    ConsoleLogger.LogError($"{logTitle} {result.Message}");
                    return result;
                }

                result.Data = connectionString;
                result.Success = true;
                return result;
            }
            catch (Exception ex)
            {
                result.Message = $"Get db connection string in config file occur exception. dbName: {dbName}. {ex}";
                Logger.Error(nameof(ConfigReader), $"{result.Message}", logTitle);
                ConsoleLogger.LogError($"{logTitle} {result.Message}");
                return result;
            }
        }

        #endregion Public Methods


        #region Internal Methods

        /// <summary>讀取 DataAccess 資料存取模組的 Config 設定值
        /// </summary>
        internal static void LoadSettings()
        {
            try
            {
                var zayniConfig = _zayniSettings;
                DataAccessSettingsOption.Settings = zayniConfig.DataAccessSettings;
            }
            catch (Exception ex)
            {
                var errorMsg = $"Load DataAccessSettings config section in config file occur exception. {ex}";
                Logger.Exception(nameof(LoadSettings), ex, eventTitle: GetLogTitle(nameof(LoadSettings)), message: errorMsg);
                ConsoleLogger.LogError($"{GetLogTitle(nameof(GetDbConnectionString))} {errorMsg}");
                throw;
            }
        }

        /// <summary>讀取資料存取連線 Config 設定
        /// </summary>
        /// <param name="dbName">資料庫連線設定名稱</param>
        /// <returns>資料存取連線 Config 設定</returns>
        internal static DataAccessSetting GetDbProviderConfig(string dbName)
        {
            try
            {
                var dbSetting = _zayniSettings.DataAccessSettings.Find(d => d.Name == dbName);
                return dbSetting;
            }
            catch (Exception ex)
            {
                var errorMsg = $"Load DataAccessSettings/DatabaseProviders config in config file occur exception. DbName: {dbName}. {ex}";
                Logger.Exception(nameof(GetDbProviderConfig), ex, eventTitle: GetLogTitle(nameof(GetDbProviderConfig)), message: errorMsg);
                ConsoleLogger.LogError($"{GetLogTitle(nameof(GetDbProviderConfig))} {errorMsg}");
                throw;
            }
        }

        #endregion Internal Methods


        #region Private Methods

        /// <summary>取得日誌的標題
        /// </summary>
        /// <param name="methodName">方法名稱</param>
        /// <returns>日誌的標題</returns>
        private static string GetLogTitle(string methodName) => $"{nameof(ConfigReader)}.{methodName}";

        #endregion Private Methods
    }
}
