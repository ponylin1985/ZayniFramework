﻿using System;
using System.Configuration;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.DataAccess
{
    /// <summary>資料存取元件工廠
    /// </summary>
    internal sealed class DaoFactory
    {
        /// <summary>建構資料存取 Dao 物件
        /// </summary>
        /// <param name="dbName">連線字串名稱</param>
        /// <returns>資料存取物件</returns>
        internal IDataAccess Create(string dbName) => Create(dbName, null);

        /// <summary>建構資料存取 Dao 物件
        /// </summary>
        /// <param name="dbName">連線字串名稱</param>
        /// <param name="connectionString">資料庫連線字串</param>
        /// <param name="zayniConnectionString">ZayniFramework 框架的資料庫連線字串 (若傳入為 Null 或空字串，SQL Profile Log 功能將停用)</param>
        /// <returns>資料存取物件</returns>
        internal IDataAccess Create(string dbName, string connectionString, string zayniConnectionString = null)
        {
            IDataAccess result = null;

            var message = "";
            var provider = "MSSQL";

            try
            {
                var settings = ConfigManager.GetZayniFrameworkSettings().DataAccessSettings.Find(d => d.Name == dbName);

                if (settings.IsNotNull())
                {
                    provider = settings.DatabaseProvider + "";
                }

                if (provider.IsNullOrEmpty())
                {
                    message = $"ZayniFramework/DataAccessSettings/DatabaseProviders config error. Database provider name: {dbName}. The 'dataBaseProvider' can not be null or empty string.";
                    Logger.Error(this, message, "DaoFactory.Create");
                    throw new ConfigurationErrorsException(message);
                }
            }
            catch (Exception ex)
            {
                Logger.Warn(this, message + $"{Environment.NewLine}{ex}", "DaoFactory.Create");
            }

            IDbColumnTypeCreater codeCreater = null;

            switch (provider.ToLower())
            {
                case "mssql":
                    result = connectionString.IsNullOrEmpty() ? new BaseMSSqlDao(dbName) : new BaseMSSqlDao(dbName, connectionString, zayniConnectionString);
                    codeCreater = new SqlDbColumnTypeCreater();
                    DbColumnTypeInitializer.SetSqlDbColumnType(codeCreater);
                    break;

                case "mysql":
                    result = connectionString.IsNullOrEmpty() ? new BaseMySqlDao(dbName) : new BaseMySqlDao(dbName, connectionString, zayniConnectionString);
                    codeCreater = new MySqlDbColumnTypeCreater();
                    DbColumnTypeInitializer.SetMySqlDbColumnType(codeCreater);
                    break;

                case "postgresql":
                    result = connectionString.IsNullOrEmpty() ? new BasePgSqlDao(dbName) : new BasePgSqlDao(dbName, connectionString, zayniConnectionString);
                    codeCreater = new PgSqlDbColumnTypeCreater();
                    DbColumnTypeInitializer.SetPgSqlDbColumnType(codeCreater);
                    break;

                case "oracle":
                    result = connectionString.IsNullOrEmpty() ? new BaseOracleDao(dbName) : new BaseOracleDao(dbName, connectionString, zayniConnectionString);
                    codeCreater = new OracleDbColumnTypeCreater();
                    DbColumnTypeInitializer.SetOracleDbColumnType(codeCreater);
                    break;

                default:
                    message = $"ZayniFramework/DataAccessSettings/DatabaseProviders config error. The 'dataBaseProvider' is only support the MSSQL, MySQL, PostgreSQL or Oracle. Database provider name: {dbName}.";
                    Logger.Error(this, message, "DaoFactory.Create");
                    throw new ConfigurationErrorsException(message);
            }

            return result;
        }


    }
}
