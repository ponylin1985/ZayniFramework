﻿using System;
using ZayniFramework.Logging;


namespace ZayniFramework.DataAccess
{
    /// <summary>資料庫供應商工廠
    /// </summary>
    internal sealed class DbProviderFactory
    {
        /// <summary>建立資料庫供應商
        /// </summary>
        /// <param name="providerType">資料庫廠牌種類</param>
        /// <returns>資料庫供應商</returns>
        internal static DbProvider Create(DbProviderOption providerType)
        {
            DbProvider result = null;

            switch (providerType)
            {
                case DbProviderOption.MSSql:
                    result = new MSSqlProvider();
                    break;

                case DbProviderOption.MySql:
                    result = new MySqlProvider();
                    break;

                case DbProviderOption.Postgres:
                    result = new PgSqlProvider();
                    break;

                case DbProviderOption.Oracle:
                    result = new OracleProvider();
                    break;

                default:
                    var errorMsg = $"Unknow or unsupport {nameof(DbProviderOption)}: {providerType}";
                    Logger.Error(nameof(DbProviderFactory), errorMsg, Logger.GetTraceLogTitle(nameof(DbProviderFactory), nameof(Create)));
                    throw new ArgumentException(errorMsg);
            }

            return result;
        }
    }
}
