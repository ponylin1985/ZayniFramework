﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using ZayniFramework.Common;
using ZayniFramework.Common.ORM;


namespace ZayniFramework.DataAccess
{
    /// <summary>ORM 資料轉換對應處理器
    /// </summary>
    public static class OrmTransfer
    {
        #region Public Methods

        /// <summary>對來源的 DataSet 進行 ORM 資料轉換
        /// </summary>
        /// <param name="source">資料來源的 DataSet 資料集合</param>
        /// <param name="tableName">資料表名稱</param>
        /// <param name="targetType">目標 ORM 的型別</param>
        /// <returns>轉換後的 ORM 對應資料模型集合</returns>
        public static IEnumerable<object> Map(DataSet source, string tableName, Type targetType)
        {
            var table = source.Tables[tableName];
            return Map(targetType, table);
        }

        #endregion Public Methods


        #region Private Methods

        /// <summary>將傳入的 DataTable 轉換指定型別的資料模型物件
        /// </summary>
        /// <param name="type">目標資料模型的型別</param>
        /// <param name="source">資料來源 DataTable 物件</param>
        /// <returns>轉換後的資料模型集合</returns>
        private static IEnumerable<object> Map(Type type, DataTable source)
        {
            var properties = type.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

            if (properties.IsNotNullOrEmpty())
            {
                var columns = source.Columns;

                foreach (DataRow row in source.Rows)
                {
                    var model = Activator.CreateInstance(type);

                    foreach (var propertyInfo in properties)
                    {
                        #region 設定資料模型屬性對應的欄位名稱

                        var columnName = propertyInfo.Name;
                        var ormAttriibute = Reflector.GetCustomAttribute<TableColumnAttribute>(propertyInfo);
                        ormAttriibute.IsNotNull(m => columnName = ormAttriibute.ColumnName);

                        // 如果找不到可以對應的 Property、Attribute 名稱與資料表欄位名稱，就先忽略
                        if (!columns.Contains(columnName))
                        {
                            continue;
                        }

                        var columnType = row[columnName].GetType();

                        #endregion 設定資料模型屬性對應的欄位名稱

                        #region 為了防止資料庫中的欄位真的有 DBNull 的情況，造成轉換失敗的處理

                        // 如果 DataTable 中的欄位值確實為 DBNull
                        if (ormAttriibute.IsNotNull() && IsDBNull(columnType.Name))
                        {
                            HandleDBNullValue(ormAttriibute.IsAllowNull, ormAttriibute.DefaultValue, ormAttriibute.IsUtcTime, propertyInfo, model);
                            continue;
                        }

                        #endregion 為了防止資料庫中的欄位真的有 DBNull 的情況，造成轉換失敗的處理

                        #region 資料繫結

                        var isUtcTime = null != ormAttriibute && ormAttriibute.IsUtcTime;
                        var isTimestamp = null != ormAttriibute && ormAttriibute.IsTimeStamp;
                        PropertyValueBinding(row, columnType, columnName, model, propertyInfo, isUtcTime, isTimestamp);

                        #endregion 資料繫結
                    }

                    yield return model;
                }
            }
        }

        private static readonly string[] _sourceArray = ["Int64", "Int32", "Int16"];

        /// <summary>進行資料模型屬性值繫結
        /// </summary>
        /// <param name="row">DataRow 資料列物件</param>
        /// <param name="columnType">資料表欄位型別</param>
        /// <param name="columnName">資料表欄位名稱</param>
        /// <param name="model">目標資料模型物件</param>
        /// <param name="propertyInfo">目標資料模型得屬性資訊</param>
        /// <param name="isUtcTime">是否為 UTC 時區時間</param>
        /// <param name="isTimestamp">是否為資料異動時間戳記 (IsTimestamp) 欄位</param>
        /// <returns>資料繫結過的資料模型</returns>
        private static object PropertyValueBinding(DataRow row, Type columnType, string columnName, object model, PropertyInfo propertyInfo, bool isUtcTime, bool isTimestamp)
        {
            #region 資料異動時間戳記 (Timestamp) 欄位處理

            if (isTimestamp)
            {
                var dbTimestamp = row[columnName];
                var timestamp = 0L;

                switch (dbTimestamp)
                {
                    case DateTime dt:
                        timestamp = dt.Ticks;
                        break;

                    case byte[] bytes:
                        timestamp = BitConverter.ToInt64(bytes, 0);
                        break;
                }

                propertyInfo.SetValue(model, timestamp);
                return model;
            }

            #endregion 資料異動時間戳記 (Timestamp) 欄位處理

            #region 逐一欄位進行資料繫結處理

            var value = row[columnName];
            var strValue = value + "";

            var columnTypeName = columnType.Name;
            var propertyTypeName = propertyInfo.PropertyType.Name;

            if ("DBNull" == columnTypeName)
            {
                return model;
            }

            if ("Nullable`1" == propertyTypeName)
            {
                propertyTypeName = Reflector.GetNullableType(propertyInfo.PropertyType).Name;
            }

            if (columnTypeName == propertyTypeName)
            {
                #region UTC Time 時區處理

                if ("DateTime" == propertyTypeName && isUtcTime)
                {
                    var utcTime = Convert.ToDateTime(value).ToUtcKind();
                    propertyInfo.SetValue(model, utcTime, null);
                    return model;
                }

                if ("DateTimeOffset" == propertyTypeName && isUtcTime)
                {
                    var utcDateTimeOffset = new DateTimeOffset(Convert.ToDateTime(value).ToUtcKind(), new TimeSpan(0, 0, 0));
                    propertyInfo.SetValue(model, utcDateTimeOffset, null);
                    return model;
                }

                #endregion UTC Time 時區處理

                propertyInfo.SetValue(model, value, null);
                return model;
            }

            #endregion 逐一欄位進行資料繫結處理

            #region 數值型別處理

            if (IsNumeric(columnType.Name))
            {
                #region 預設在資料庫數字型別的欄位可以繫結到字串型別的屬性上

                // 預設數字可轉為 String
                if ("String" == propertyTypeName)
                {
                    propertyInfo.SetValue(model, strValue, null);
                    return model;
                }

                #endregion 預設在資料庫數字型別的欄位可以繫結到字串型別的屬性上

                #region 20150824 Added by Pony: 布林值型別的屬性的轉換處理

                if ("Boolean" == propertyTypeName)
                {
                    if (!int.TryParse(strValue, out var numValue))
                    {
                        return model;
                    }

                    if (0 == numValue)
                    {
                        propertyInfo.SetValue(model, false, null);
                        return model;
                    }

                    propertyInfo.SetValue(model, true, null);
                    return model;
                }

                #endregion 20150824 Added by Pony: 布林值型別的屬性的轉換處理

                #region 20150824 Edited by Pony: 針對 Oracle 資料庫的 NUMBER 型別的欄位進行轉換與處理

                // 20140307 Pony Says: 以下是一種 Special Case，因為在 Oracle 資料庫中，所有的數字型別對應到 .NET 中的型別都會是 Decimal
                // 20140307 Pony Says: 所以針對資料模型中的 Int32 型別的屬性有這樣的特殊處理
                // 20150827 Pony Says: 我不得不在這邊寫死，把所有可能在 C# 中的數字型別拿出來逐一檢查比對，這樣子做針對 Oracle 資料庫的數字型別才不會有問題。
                if (_sourceArray.Contains(columnTypeName))
                {
                    object val = null;

                    switch (propertyTypeName)
                    {
                        case "Int64":
                            val = long.TryParse(strValue, out var int64Value) ? int64Value : 0;
                            break;

                        case "Int32":
                            val = int.TryParse(strValue, out var int32Value) ? int32Value : 0;
                            break;

                        case "Int16":
                            val = short.TryParse(strValue, out var int16Value) ? int16Value : 0;
                            break;
                    }

                    propertyInfo.SetValue(model, val, null);
                    return model;
                }

                if ("Decimal" == columnTypeName && "Byte" == propertyTypeName)
                {
                    var byteValue = byte.Parse(strValue);
                    propertyInfo.SetValue(model, byteValue, null);
                    return model;
                }

                if ("Decimal" == columnTypeName && "Int16" == propertyTypeName)
                {
                    short shortValue = byte.Parse(strValue);
                    propertyInfo.SetValue(model, shortValue, null);
                    return model;
                }

                if ("Decimal" == columnTypeName && "Int32" == propertyTypeName)
                {
                    var decimalValue = decimal.Parse(strValue);

                    if (!decimalValue.IsRealDecimal())
                    {
                        propertyInfo.SetValue(model, int.Parse(strValue), null);
                        return model;
                    }
                }

                if ("Decimal" == columnTypeName && "Int64" == propertyTypeName)
                {
                    var longValue = long.Parse(strValue);
                    propertyInfo.SetValue(model, longValue, null);
                    return model;
                }

                if ("Decimal" == columnTypeName && "Double" == propertyTypeName)
                {
                    double doubleValue = long.Parse(strValue);
                    propertyInfo.SetValue(model, doubleValue, null);
                    return model;
                }

                if ("Decimal" == columnTypeName && "Float" == propertyTypeName)
                {
                    float floatValue = long.Parse(strValue);
                    propertyInfo.SetValue(model, floatValue, null);
                    return model;
                }

                #endregion 20150824 Edited by Pony: 針對 Oracle 資料庫的 NUMBER 型別的欄位進行轉換與處理
            }

            #endregion 數值型別處理

            throw new OrmTransferingException($"ORM transfering occur exception. Property value binding occur exception. DataColumn: '{columnName}', DataColumn Type: {columnType.Name}, Property Name: {propertyInfo.Name} , Property Type: {propertyInfo.PropertyType.Name}.");
        }

        /// <summary>判斷型別是否為數字
        /// </summary>
        /// <param name="typeName">型別的名稱</param>
        /// <returns>是否為一種數字的型別</returns>
        private static bool IsNumeric(string typeName)
        {
            return typeName switch
            {
                "Byte" or "SByte" or "Int16" or "UInt16" or "Int32" or "UInt32" or "Int64" or "UInt64" or "Decimal" or "Double" or "Float" => true,
                _ => false,
            };
        }

        /// <summary>處理 DBNull 的值
        /// </summary>
        /// <typeparam name="TConvert"></typeparam>
        /// <param name="isAllowNull">是否允許 Null 值</param>
        /// <param name="defaultValue">程式預設值字串</param>
        /// <param name="isUtcTime">是否為 UTC 時區時間</param>
        /// <param name="propertyInfo">屬性中介資料</param>
        /// <param name="model">資料模型</param>
        private static void HandleDBNullValue<TConvert>(bool isAllowNull, string defaultValue, bool isUtcTime, PropertyInfo propertyInfo, TConvert model)
        {
            // 如果該屬性標記為不可以為 Null 值，直接判定轉換失敗!
            if (!isAllowNull)
            {
                throw new Exception($"ORM convert fail due to Property '{propertyInfo.Name}' is not allowed DBNull value.");
            }

            var propertyTypeName = propertyInfo.PropertyType.Name;
            // 20171028 Pony Says: 我決定當 PropertyType 為 Struct Nullable 型別時，就不支援 ORM Metadata DefaultValue 的預設值給定了...
            //Check.IsTrue( Reflector.IsNullableType( propertyInfo.PropertyType ), () => propertyTypeName = Reflector.GetNullableType( propertyInfo.PropertyType ).Name );

            // 如果該屬性標記為可以為 Null 值，則給定預設值
            switch (propertyTypeName)
            {
                case "String":
                    var stringValue = defaultValue.IsNullOrEmptyString(string.Empty);
                    propertyInfo.SetValue(model, stringValue, null);
                    break;

                case "Decimal":
                case "Double":
                case "Float":
                    if (defaultValue.IsNullOrEmpty() || !decimal.TryParse(defaultValue, out var decimalValue))
                    {
                        propertyInfo.SetValue(model, default(decimal), null);
                        break;
                    }

                    propertyInfo.SetValue(model, decimalValue, null);
                    break;

                case "Byte":
                    if (defaultValue.IsNullOrEmpty() || !byte.TryParse(defaultValue, out var byteValue))
                    {
                        propertyInfo.SetValue(model, default(byte), null);
                        break;
                    }

                    propertyInfo.SetValue(model, byteValue, null);
                    break;

                case "Int16":
                    if (defaultValue.IsNullOrEmpty() || !short.TryParse(defaultValue, out var int16Value))
                    {
                        propertyInfo.SetValue(model, default(short), null);
                        break;
                    }

                    propertyInfo.SetValue(model, int16Value, null);
                    break;

                case "Int32":
                    if (defaultValue.IsNullOrEmpty() || !int.TryParse(defaultValue, out var int32Value))
                    {
                        propertyInfo.SetValue(model, default(int), null);
                        break;
                    }

                    propertyInfo.SetValue(model, int32Value, null);
                    break;

                case "Int64":
                    if (defaultValue.IsNullOrEmpty() || !long.TryParse(defaultValue, out var int64Value))
                    {
                        propertyInfo.SetValue(model, default(long), null);
                        break;
                    }

                    propertyInfo.SetValue(model, int64Value, null);
                    break;

                case "DateTime":
                    if (defaultValue.IsNullOrEmpty() || !DateTime.TryParse(defaultValue, out var dateTimeValue))
                    {
                        var defaultDateTime = isUtcTime ? default(DateTime).ToUtcKind() : default;
                        propertyInfo.SetValue(model, defaultDateTime, null);
                        break;
                    }

                    var valueOfDateTime = isUtcTime ? dateTimeValue.ToUtcKind() : dateTimeValue;
                    propertyInfo.SetValue(model, valueOfDateTime, null);
                    break;

                case "DateTimeOffset":
                    if (defaultValue.IsNullOrEmpty() || !DateTimeOffset.TryParse(defaultValue, out var dateTimeOffsetValue))
                    {
                        var defaultDateTimeOffset = isUtcTime ? new DateTimeOffset(default(DateTimeOffset).DateTime, new TimeSpan(0, 0, 0)) : default;
                        propertyInfo.SetValue(model, defaultDateTimeOffset, null);
                        break;
                    }

                    var valueOfDateTimeOffset = isUtcTime ? new DateTimeOffset(dateTimeOffsetValue.DateTime, new TimeSpan(0, 0, 0)) : dateTimeOffsetValue;
                    propertyInfo.SetValue(model, valueOfDateTimeOffset, null);
                    break;

                case "Boolean":
                    if (defaultValue.IsNullOrEmpty() || !bool.TryParse(defaultValue, out var booleanValue))
                    {
                        propertyInfo.SetValue(model, default(bool), null);
                        break;
                    }

                    propertyInfo.SetValue(model, booleanValue, null);
                    break;
            }
        }

        /// <summary>判斷是否為 DBNull 型別
        /// </summary>
        /// <param name="typeName">型別的名稱</param>
        /// <returns>是否為 DBNull 型別</returns>
        private static bool IsDBNull(string typeName) => "DBNull".Equals(typeName);

        #endregion Private Methods
    }
}
