﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZayniFramework.Common;
using ZayniFramework.Common.Dynamic;
using ZayniFramework.Common.ORM;
using ZayniFramework.Logging;


namespace ZayniFramework.DataAccess
{
    /// <summary>ORM 資料繫結處理器
    /// </summary>
    public class OrmDataBinder
    {
        /// <summary>對資料來源的 DataSet 進行指定泛型型別的 ORM 轉換處理。<para/>
        /// * 僅處理單子型別的 ORM 資料繫結。
        /// </summary>
        /// <param name="ds">資料來源的 DataSet 的資料集合</param>
        /// <typeparam name="TModel">ORM 轉換指定得泛型</typeparam>
        /// <returns>ORM 資料繫結處理結果</returns>
        public static IEnumerable<TModel> LoadDataToModel<TModel>(DataSet ds)
            where TModel : new()
        {
            var ormModels = OrmTransfer.Map(ds, ds.Tables[0].TableName + "", typeof(TModel));
            return ormModels.Select(m => (TModel)m);
        }

        /// <summary>對資料來源進行動態的 ORM 資料繫結處理
        /// </summary>
        /// <param name="reader">資料讀取器</param>
        /// <returns>ORM 資料繫結處理結果</returns>
        public IEnumerable<dynamic> LoadDataToDynamicModel(IDataReader reader)
        {
            var logTitle = Logger.GetTraceLogTitle(this, nameof(LoadDataToDynamicModel));
            var ormModels = new List<dynamic>();

            while (reader.Read())
            {
                dynamic model = DynamicHelper.CreateDynamicObject();

                for (var i = 0; i < reader.FieldCount; i++)
                {
                    var name = reader.GetName(i);
                    dynamic value = reader[i];

                    if (!DynamicHelper.BindProperty(model, name, value))
                    {
                        reader.Close();
                        var message = $"Dynamic object property binding occur error. PropertyName: {name}.";
                        Logger.Error(this, message, logTitle);
                        throw new OrmTransferingException(message);
                    }
                }

                ormModels.Add(model);
            }

            reader.Close();
            return ormModels;
        }

        /// <summary>對資料來源的 DataSet 進行指定泛型型別的 ORM 轉換處理。
        /// * 可以對所有指定的 Type 型別，依序處理 ORM 資料繫結。
        /// </summary>
        /// <param name="ds">資料來源的 DataSet 的資料集合</param>
        /// <param name="modelTypes">資料繫結的型別集合</param>
        /// <returns>ORM 資料繫結處理結果</returns>
        public static IEnumerable<object[]> LoadDataToModels(DataSet ds, Type[] modelTypes)
        {
            for (int i = 0, length = ds.Tables.Count; i < length; i++)
            {
                var table = ds.Tables[i].TableName + "";
                var type = modelTypes[i];
                var ormModels = OrmTransfer.Map(ds, table, type);
                var models = ormModels.ToArray();
                yield return models;
            }
        }

        /// <summary>將傳入的 DataSet 資料動態繫結到動態資料模型字典集合中 (Dynamic ORM機制)
        /// </summary>
        /// <param name="ds">資料來源 DataSet 資料集合</param>
        /// <param name="result">動態資料模型字典集合</param>
        /// <param name="message">錯誤訊息</param>
        /// <returns>是否成功</returns>
        public bool LoadDataToDynamicModelCollection(DataSet ds, out Dictionary<string, IEnumerable<dynamic>> result, out string message)
        {
            var logTitle = Logger.GetTraceLogTitle(this, nameof(LoadDataToDynamicModelCollection));

            result = [];
            message = null;

            try
            {
                for (int i = 0, length = ds.Tables.Count; i < length; i++)
                {
                    var table = ds.Tables[i];
                    var tableName = table.TableName + "";

                    var models = new List<dynamic>();

                    foreach (DataRow row in table.Rows)
                    {
                        dynamic model = DynamicHelper.CreateDynamicObject();

                        foreach (DataColumn colume in table.Columns)
                        {
                            var name = colume.ColumnName;
                            dynamic value = row[name];

                            if (!DynamicHelper.BindProperty(model, name, value))
                            {
                                result = null;
                                message = $"Dynamic object property binding occur error. PropertyName: {name}.";
                                Logger.Error(this, message, logTitle);
                                return false;
                            }
                        }

                        models.Add(model);
                    }

                    if (!result.ContainsKey(tableName))
                    {
                        result.Add(tableName, models);
                    }
                }
            }
            catch (Exception ex)
            {
                result = null;
                message = $"Dynamic object property binding occur error. {ex}";
                Logger.Exception(this, ex, logTitle, message);
                return false;
            }

            return true;
        }

        /// <summary>根據傳入的資料模型集合，依照 ORM 機制正確的載入到目標 ModelSet 中，ModelSet 中的屬性需要正確的標記 OrmData 中介資料
        /// </summary>
        /// <param name="modelTypes">目標資料模型型別陣列</param>
        /// <param name="modelSet">資料模型集合</param>
        /// <param name="datas">資料來源Model</param>
        /// <param name="message">錯誤訊息</param>
        /// <returns>是否成功</returns>
        public bool LoadDataToModelSetByIndex(Type[] modelTypes, List<object[]> datas, ModelSet modelSet, out string message)
        {
            message = null;

            foreach (var property in modelSet.GetType().GetProperties())
            {
                var orm = Reflector.GetCustomAttribute<OrmDataAttribute>(property);

                if (orm.IsNull())
                {
                    continue;
                }

                try
                {
                    var value = ListReflector.CreateGenericList(modelTypes[orm.Index]);

                    if (value.IsNull())
                    {
                        message = $"Create generic list occur exception.";
                        modelSet.Success = false;
                        return false;
                    }

                    var models = datas[orm.Index];

                    for (int i = 0, len = models.Length; i < len; i++)
                    {
                        if (!ListReflector.AddItemToGenericList(value, models[i]))
                        {
                            message = $"Add data into generic list occur error.";
                            modelSet.Success = false;
                            return false;
                        }
                    }

                    property.SetValue(modelSet, value);
                }
                catch (Exception ex)
                {
                    message = ex.ToString();
                    modelSet.Success = false;
                    Logger.Exception(this, ex, Logger.GetTraceLogTitle(this, nameof(LoadDataToModelSetByIndex)), message);
                    return false;
                }
            }

            modelSet.Success = true;
            modelSet.Message = null;
            return true;
        }

        /// <summary>根據傳入的資料模型集合，依照 ORM 機制正確的載入到目標 ModelSet 中，ModelSet 中的屬性需要正確的標記 OrmData 中介資料
        /// </summary>
        /// <param name="modelTypes">目標資料模型型別陣列</param>
        /// <param name="modelSet">資料模型集合</param>
        /// <param name="datas">資料來源Model</param>
        /// <param name="message">錯誤訊息</param>
        /// <returns>是否成功</returns>
        public bool LoadDataToModelSet(Type[] modelTypes, IEnumerable<object[]> datas, ModelSet modelSet, out string message)
        {
            message = null;

            foreach (var property in modelSet.GetType().GetProperties())
            {
                var orm = Reflector.GetCustomAttribute<OrmDataAttribute>(property);

                if (orm.IsNull())
                {
                    continue;
                }

                try
                {
                    var type = ListReflector.GetListFirstGenericType(property.PropertyType);
                    var mType = modelTypes.Where(m => m.FullName == type.FullName).FirstOrDefault();

                    var value = ListReflector.CreateGenericList(mType);

                    if (value.IsNull())
                    {
                        message = $"Create generic list occur exception.";
                        modelSet.Success = false;
                        return false;
                    }

                    var models = datas.Where(m => m[0].GetType().FullName == type.FullName).FirstOrDefault();

                    for (int i = 0, len = models.Length; i < len; i++)
                    {
                        var isOk = ListReflector.AddItemToGenericList(value, models[i]);

                        if (!isOk)
                        {
                            message = $"Add data into generic list occur error.";
                            modelSet.Success = false;
                            return false;
                        }
                    }

                    property.SetValue(modelSet, value);
                }
                catch (Exception ex)
                {
                    message = ex.ToString();
                    modelSet.Success = false;
                    Logger.Exception(this, ex, Logger.GetTraceLogTitle(this, nameof(LoadDataToModelSet)), message);
                    return false;
                }
            }

            modelSet.Success = true;
            modelSet.Message = null;
            return true;
        }
    }
}
