﻿using System;
using System.Data.Common;
using ZayniFramework.Common;
using ZayniFramework.Common.ORM;
using ZayniFramework.Logging;


namespace ZayniFramework.DataAccess
{
    /// <summary>資料庫預存程序資料模型
    /// </summary>
    public abstract class StoredProcedureModel
    {
        #region 宣告內部類別

        /// <summary>內部資料存取Dao元件
        /// </summary>
        private class DataAccess : BaseDataAccess
        {
            /// <summary>資料庫名稱
            /// </summary>
            /// <param name="dbName">資料庫連線字串名稱</param>
            public DataAccess(string dbName) : base(dbName)
            {
            }

            /// <summary>錯誤訊息
            /// </summary>
            public new string Message => base.Message;

            /// <summary>執行 DbCommand 資料庫指令。
            /// </summary>
            /// <param name="command">資料庫指令</param>
            /// <returns>是否執行成功</returns>
            public new bool ExecuteNonQuery(DbCommand command) => base.ExecuteNonQuery(command);

            /// <summary>執行 DbCommand 查詢指令，輸出自定義的 ModelSet 資料集合中。
            /// </summary>
            /// <param name="command">資料庫指令</param>
            /// <param name="modelTypes">資料模型型別陣列</param>
            /// <param name="modelSet">查詢結果資料集合</param>
            /// <returns>是否執行查詢成功</returns>
            public new bool LoadModelSet(DbCommand command, ModelSet modelSet, params Type[] modelTypes) =>
                base.LoadModelSet(command, modelSet, modelTypes);

            /// <summary>取得資料庫預存程序的資料庫指令
            /// </summary>
            /// <param name="storedProcName">資料庫預存程序名稱</param>
            /// <param name="connection">資料庫連線</param>
            /// <param name="transaction">資料庫交易</param>
            /// <returns>資料庫指令</returns>
            public new DbCommand GetStoredProcCommand(string storedProcName, DbConnection connection = null, DbTransaction transaction = null) =>
                base.GetStoredProcCommand(storedProcName, connection, transaction);

            /// <summary>新增資料庫指令參數
            /// </summary>
            /// <param name="command">資料庫指令</param>
            /// <param name="parameterName">資料庫指令的參數名稱</param>
            /// <param name="dbType">參數型態 (可以使用ZayniFramework 框架的 DbColumnType 靜態常數)</param>
            /// <param name="value">參數值</param>
            public new DbCommand AddInParameter(DbCommand command, string parameterName, int dbType, object value) =>
                base.AddInParameter(command, parameterName, dbType, value);

            /// <summary>新增資料庫指令的輸出參數
            /// </summary>
            /// <param name="command">資料庫指令</param>
            /// <param name="parameterName">資料庫指令的參數名稱</param>
            /// <param name="dbType">參數型態 (可以使用ZayniFramework 框架的 DbColumnType 靜態常數)</param>
            /// <param name="maxSize">變數的最大長度</param>
            public new DbCommand AddOutParameter(DbCommand command, string parameterName, int dbType, int maxSize) =>
                base.AddOutParameter(command, parameterName, dbType, maxSize);
        }

        #endregion 宣告內部類別


        #region 宣告唯一公開的虛擬方法

        /// <summary>執行資料庫預存程序
        /// </summary>
        /// <returns>執行資料庫預存程序結果</returns>
        public virtual Result Execute()
        {
            #region 初始化回傳值

            var result = new Result()
            {
                Success = false
            };

            #endregion 初始化回傳值

            #region 取得對應的預存程序的名稱，並且建立預存程序SQL命令

            var type = GetType();

            var procedureAttr = Reflector.GetCustomAttribute<StoredProcedureAttribute>(type);

            if (procedureAttr.IsNull())
            {
                result.Message = "沒有在資料模型上正確標記StoredProcedure中介資料，無法執行資料庫預存程序。";
                Logger.Error(this, result.Message, "StoredProcedureModel.Execute");
                return result;
            }

            var dbName = procedureAttr.DbName;
            var spName = procedureAttr.ProcedureName;

            if (new string[] { dbName, spName }.HasNullOrEmptyElemants())
            {
                result.Message = "StoredProcedure中介資料的DbName或ProcedureName屬性為Null或空字串，無法執行資料庫預存程序。";
                Logger.Error(this, result.Message, "StoredProcedureModel.Execute");
                return result;
            }

            var dao = new DataAccess(dbName);
            var spCommand = dao.GetStoredProcCommand(spName);

            #endregion 取得對應的預存程序的名稱，並且建立預存程序SQL命令

            #region 取得所有的屬性並且加入所有預存程序的參數

            var properties = type.GetProperties();

            foreach (var property in properties)
            {
                var parameterAttr = Reflector.GetCustomAttribute<ProcedureParameterAttribute>(property);

                if (parameterAttr.IsNull())
                {
                    continue;
                }

                var parameterName = parameterAttr.Name;
                var parameterType = parameterAttr.DbType;
                var parameterValue = property.GetValue(this);

                if (parameterName.IsNullOrEmpty())
                {
                    result.Message = $"屬性 {property.Name} 標記的 ProcedureParameter 中介資料 Name 屬性為 Null 或是空字串，無法執行資料庫預存程序。";
                    Logger.Error(this, result.Message, "StoredProcedureModel.Execute");
                    return result;
                }

                if (parameterAttr.IsOutput)
                {
                    dao.AddOutParameter(spCommand, parameterName, parameterType, parameterAttr.MaxSize);
                }
                else
                {
                    dao.AddInParameter(spCommand, parameterName, parameterType, parameterValue);
                }
            }

            #endregion 取得所有的屬性並且加入所有預存程序的參數
            #region 執行資料庫預存程序

            var modelSetProp = type.GetProperty("ResultSet");

            if (modelSetProp.IsNull())
            {
                _ = dao.ExecuteNonQuery(spCommand);
            }
            else
            {
                Type[] types;
                ModelSet modelSet;

                try
                {
                    types = (Type[])type.GetProperty("ModelSetTypes").GetValue(this);
                    var modelSetType = (Type)type.GetProperty("ResultSetType").GetValue(this);
                    modelSet = (ModelSet)Activator.CreateInstance(modelSetType);
                }
                catch (Exception ex)
                {
                    result.HasException = true;
                    result.ExceptionObject = ex;
                    result.Message = $"StoredProcedureModel執行資料庫預存程序發生異常: {ex}";
                    Logger.Exception(this, ex, result.Message);
                    return result;
                }

                var isSuccess = dao.LoadModelSet(spCommand, modelSet, types);

                if (!isSuccess)
                {
                    result.Message = $"StoredProcedureModel執行資料庫預存程序失敗: {dao.Message}";
                    Logger.Error(this, result.Message, "StoredProcedureModel.Execute");
                    return result;
                }

                try
                {
                    modelSetProp.SetValue(this, modelSet);
                }
                catch (Exception ex)
                {
                    result.Message = $"StoredProcedureModel執行資料庫預存程序發生異常: {ex}";
                    Logger.Exception(this, ex, result.Message);
                    return result;
                }
            }

            #endregion 執行資料庫預存程序

            #region 設定回傳值屬性

            result.Message = "";
            result.Success = true;

            #endregion 設定回傳值屬性

            return result;
        }

        #endregion 宣告唯一公開的虛擬方法
    }
}
