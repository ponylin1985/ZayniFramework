using Npgsql;
using System.Data;
using System.Data.Common;


namespace ZayniFramework.DataAccess
{
    /// <summary>PostgreSQL 資料庫供應商
    /// </summary>
    internal sealed class PgSqlProvider : DbProvider
    {
        #region 實作 DbProvider 基底的抽象

        /// <summary>建立資料庫連線
        /// </summary>
        /// <param name="connectionString">資料庫連線字串</param>
        /// <returns>資料庫連線</returns>
        internal override DbConnection CreateDbConnection(string connectionString) =>
            new NpgsqlConnection(connectionString);

        /// <summary>開啟資料庫交易
        /// </summary>
        /// <param name="connection">資料庫連線</param>
        /// <returns>資料庫交易</returns>
        internal override DbTransaction BeginDbTransaction(DbConnection connection) =>
            ((NpgsqlConnection)connection).BeginTransaction();

        /// <summary>開啟資料庫交易
        /// </summary>
        /// <param name="connection">資料庫連線</param>
        /// <param name="isolationLevel">交易格離層級</param>
        /// <returns>資料庫交易</returns>
        internal override DbTransaction BeginDbTransaction(DbConnection connection, IsolationLevel isolationLevel = IsolationLevel.ReadCommitted) =>
            ((NpgsqlConnection)connection).BeginTransaction(isolationLevel);

        /// <summary>取得資料轉接器
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <returns>資料轉接器</returns>
        internal override DbDataAdapter GetDataAdapter(DbCommand command) =>
            new NpgsqlDataAdapter((NpgsqlCommand)command);

        /// <summary>取得 SQL 敘述的資料庫指令
        /// </summary>
        /// <param name="sqlCommandText">SQL敘述字串 或 資料庫預存程序名稱</param>
        /// <param name="commandType">資料庫指令型態</param>
        /// <returns>SQL敘述的資料庫指令</returns>
        internal override DbCommand GetDbCommand(string sqlCommandText, CommandType commandType = CommandType.Text) =>
            new NpgsqlCommand()
            {
                CommandText = sqlCommandText,
                CommandType = commandType
            };

        #endregion 實作 DbProvider 基底的抽象
    }
}