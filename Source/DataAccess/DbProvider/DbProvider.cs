﻿using System.Data;
using System.Data.Common;


namespace ZayniFramework.DataAccess
{
    /// <summary>資料庫供應商基底
    /// </summary>
    internal abstract class DbProvider
    {
        /// <summary>建立資料庫連線
        /// </summary>
        /// <param name="connectionString">資料庫連線字串</param>
        /// <returns>資料庫連線</returns>
        internal abstract DbConnection CreateDbConnection(string connectionString);

        /// <summary>開啟資料庫交易
        /// </summary>
        /// <param name="connection">資料庫連線</param>
        /// <returns>資料庫交易</returns>
        internal abstract DbTransaction BeginDbTransaction(DbConnection connection);

        /// <summary>開啟資料庫交易
        /// </summary>
        /// <param name="connection">資料庫連線</param>
        /// <param name="isolationLevel">交易格離層級</param>
        /// <returns>資料庫交易</returns>
        internal abstract DbTransaction BeginDbTransaction(DbConnection connection, IsolationLevel isolationLevel = IsolationLevel.ReadCommitted);

        /// <summary>取得資料轉接器
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <returns>資料轉接器</returns>
        internal abstract DbDataAdapter GetDataAdapter(DbCommand command);

        /// <summary>取得SQL敘述的資料庫指令
        /// </summary>
        /// <param name="sqlCommandText">SQL敘述字串 或 資料庫預存程序名稱</param>
        /// <param name="commandType">資料庫指令型態</param>
        /// <returns>SQL敘述的資料庫指令</returns>
        internal abstract DbCommand GetDbCommand(string sqlCommandText, CommandType commandType = CommandType.Text);
    }
}
