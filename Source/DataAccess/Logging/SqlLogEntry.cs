﻿using System;
using Newtonsoft.Json;

namespace ZayniFramework.DataAccess
{
    /// <summary>SQL 日誌記錄資訊
    /// </summary>
    internal sealed class SqlLogEntry
    {
        /// <summary>資料存取物件的類別名稱
        /// </summary>
        internal string DaoName { get; set; }

        /// <summary>SQL 指令的識別代碼
        /// </summary>
        internal string CommandId { get; set; }

        /// <summary>SQL指令的執行方向<para/>
        /// 1: 請求指令<para/>
        /// 2: 執行指令的結果。
        /// </summary>
        internal int CommandDirection { get; set; }

        /// <summary>SQL 指令種類<para/>
        /// 1: CommandText<para/>
        /// 2: Stored Procedure
        /// </summary>
        internal int CommandType { get; set; }

        /// <summary>SQL 指令的敘述
        /// </summary>
        internal string CommandText { get; set; }

        /// <summary>SQL 指令執行的結果
        /// </summary>
        internal string CommandResult { get; set; }

        /// <summary>發送 SQL 指令的電腦名稱或 IP 位址
        /// </summary>
        internal string HostName { get; set; }

        /// <summary>日誌記錄時間
        /// </summary>
        internal DateTime LogTime { get; set; }
    }

    /// <summary>SQL 日誌記錄資訊 (ElasticSearch 日誌資料載體)
    /// </summary>
    internal sealed class sql_log
    {
        /// <summary>日誌流水號
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "log_srno")]
        public string LogSrNo { get; set; }

        /// <summary>資料存取物件的類別名稱
        /// </summary>
        [JsonProperty(PropertyName = "dao_name")]
        public string DaoName { get; set; }

        /// <summary>SQL 指令的識別代碼
        /// </summary>
        [JsonProperty(PropertyName = "command_id")]
        public string CommandId { get; set; }

        /// <summary>SQL指令的執行方向<para/>
        /// 1: 請求指令<para/>
        /// 2: 執行指令的結果。
        /// </summary>
        [JsonProperty(PropertyName = "command_direction")]
        public int CommandDirection { get; set; }

        /// <summary>SQL 指令種類<para/>
        /// 1: CommandText<para/>
        /// 2: Stored Procedure
        /// </summary>
        [JsonProperty(PropertyName = "command_type")]
        public int CommandType { get; set; }

        /// <summary>SQL 指令的敘述
        /// </summary>
        [JsonProperty(PropertyName = "command_text")]
        public string CommandText { get; set; }

        /// <summary>SQL 指令執行的結果
        /// </summary>
        [JsonProperty(PropertyName = "command_result")]
        public string CommandResult { get; set; }

        /// <summary>發送 SQL 指令的電腦名稱或 IP 位址
        /// </summary>
        [JsonProperty(PropertyName = "host_name")]
        public string HostName { get; set; }

        /// <summary>日誌記錄時間
        /// </summary>
        [JsonProperty(PropertyName = "log_time")]
        public DateTime LogTime { get; set; }
    }
}
