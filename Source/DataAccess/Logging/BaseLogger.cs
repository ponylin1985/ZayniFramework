﻿using System;
using System.Data;
using System.Data.Common;
using System.Net;
using ZayniFramework.Common;
using ZayniFramework.Common.Tasks;
using ZayniFramework.Logging;
using ZayniFramework.Serialization;


namespace ZayniFramework.DataAccess
{
    /// <summary>SQL Profile 日誌記錄員基底
    /// </summary>
    internal abstract class BaseLogger
    {
        #region 宣告私有的欄位

        /// <summary>資料庫名稱
        /// </summary>
        private readonly string _dbName;

        /// <summary>資料庫連線字串
        /// </summary>
        private readonly string _connectionString;

        /// <summary>SQL日誌記錄資料存取物件
        /// </summary>
        private LoggerDao _dao;

        /// <summary>SQL Profile Log 非同步工作佇列處理器
        /// </summary>
        /// <returns></returns>
        private static readonly AsyncWorker _worker = AsyncWorkerContainer.Resolve("ZayniFrameworkLoggerAsyncWorker");

        #endregion 宣告私有的欄位


        #region 宣告建構子

        /// <summary>靜態建構子
        /// </summary>
        /// <returns></returns>
        static BaseLogger() => _worker.Start();

        /// <summary>預設建構子
        /// </summary>
        /// <param name="dbName">資料庫名稱</param>
        public BaseLogger(string dbName)
        {
            _dbName = dbName;
            _dao = LoggerDaoFactory.Create(out _connectionString);
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="dbName">資料庫名稱</param>
        /// <param name="zayniConnectionString">ZayniFramework 框架的資料庫連線字串</param>
        public BaseLogger(string dbName, string zayniConnectionString)
        {
            _connectionString = zayniConnectionString;
            _dbName = dbName;
            _dao = LoggerDaoFactory.Create(_connectionString);
        }

        /// <summary>解構子
        /// </summary>
        ~BaseLogger()
        {
            _dao = null;
        }

        #endregion 宣告建構子


        #region 宣告公開的抽象

        /// <summary>從來源的資料庫指令中擷取要進行SQL紀錄的語法
        /// </summary>
        /// <param name="command">來源資料庫指令</param>
        /// <returns>SQL語法</returns>
        public abstract string FetchLogCommandText(DbCommand command);

        #endregion 宣告公開的抽象


        #region 宣告公開的方法

        /// <summary>寫入 SQL 指令請求的執行日誌
        /// </summary>
        /// <param name="commandId">SQL 指令識別碼</param>
        /// <param name="command">資料庫 SQL 指令</param>
        /// <param name="daoName">資料存取物件的類別名稱</param>
        public void WriteDbCommandRequestLog(string commandId, DbCommand command, string daoName = null)
        {
            WriteESCommandRequestLog(commandId, command, daoName);

            if (!CanLog())
            {
                return;
            }

            _worker.Enqueue(async () =>
            {
                var model = new SqlLogEntry();

                switch (command.CommandType)
                {
                    case CommandType.Text:
                        model.CommandType = 1;
                        break;

                    case CommandType.StoredProcedure:
                        model.CommandType = 2;
                        break;
                }

                try
                {
                    model.DaoName = daoName;
                    model.CommandId = commandId;
                    model.CommandDirection = 1;
                    model.CommandText = FetchLogCommandText(command);
                    model.HostName = Dns.GetHostName();
                    model.LogTime = DateTime.UtcNow;

                    await _dao.WriteLogAsync(model);
                }
                catch (Exception ex)
                {
                    Logger.Exception(this, ex, $"{nameof(BaseLogger)} insert SQL profile log occur exception.");
                }
            });
        }

        /// <summary>寫入 SQL 指令請求的執行日誌到 ES 服務
        /// </summary>
        /// <param name="commandId">SQL 指令識別碼</param>
        /// <param name="command">資料庫 SQL 指令</param>
        /// <param name="daoName">資料存取物件的類別名稱</param>
        public void WriteESCommandRequestLog(string commandId, DbCommand command, string daoName = null)
        {
            var config = ConfigReader.GetDbProviderConfig(_dbName);

            if (config.IsNull() || config.ElasticStackLoggerName.IsNullOrEmpty())
            {
                return;
            }

            _worker.Enqueue(async () =>
            {
                var model = new sql_log();

                switch (command.CommandType)
                {
                    case CommandType.Text:
                        model.CommandType = 1;
                        break;

                    case CommandType.StoredProcedure:
                        model.CommandType = 2;
                        break;
                }

                try
                {
                    model.LogSrNo = RandomTextHelper.CreateInt64String(20);
                    model.DaoName = daoName;
                    model.CommandId = commandId;
                    model.CommandDirection = 1;
                    model.CommandText = FetchLogCommandText(command);
                    model.HostName = Dns.GetHostName();
                    model.LogTime = DateTime.UtcNow;

                    await ElasticsearchLoggerContainer.Get(config.ElasticStackLoggerName)?.InsertLogAsync<sql_log>(model, model.LogSrNo);
                }
                catch (Exception ex)
                {
                    Logger.Exception(this, ex, $"{nameof(BaseLogger)} insert SQL profile log to ElasticSearch occur exception.");
                }
            });
        }

        /// <summary>寫入 SQL 指令執行結果的日誌
        /// </summary>
        /// <param name="commandId">SQL 指令識別碼</param>
        /// <param name="command">資料庫 SQL 指令</param>
        /// <param name="commandResult">執行結果</param>
        /// <param name="daoName">資料存取物件的類別名稱</param>
        public void WriteDbCommandResultLog(string commandId, DbCommand command, object commandResult, string daoName = null)
        {
            WriteESCommandResultLog(commandId, command, commandResult, daoName);

            if (!CanLog())
            {
                return;
            }

            _worker.Enqueue(async () =>
            {
                var model = new SqlLogEntry();

                switch (command.CommandType)
                {
                    case CommandType.Text:
                        model.CommandType = 1;
                        break;

                    case CommandType.StoredProcedure:
                        model.CommandType = 2;
                        break;
                }

                try
                {
                    model.DaoName = daoName;
                    model.CommandId = commandId;
                    model.CommandDirection = 2;
                    model.CommandResult = NewtonsoftJsonConvert.Serialize(commandResult);
                    model.HostName = Dns.GetHostName();
                    model.LogTime = DateTime.UtcNow;

                    await _dao.WriteLogAsync(model);
                }
                catch (Exception ex)
                {
                    Logger.Exception(this, ex, $"{nameof(BaseLogger)} insert SQL profile log occur exception.");
                }
            });
        }

        /// <summary>寫入 SQL 指令執行結果的日誌到 ES 服務
        /// </summary>
        /// <param name="commandId">SQL 指令識別碼</param>
        /// <param name="command">資料庫 SQL 指令</param>
        /// <param name="commandResult">執行結果</param>
        /// <param name="daoName">資料存取物件的類別名稱</param>
        public void WriteESCommandResultLog(string commandId, DbCommand command, object commandResult, string daoName = null)
        {
            var config = ConfigReader.GetDbProviderConfig(_dbName);

            if (config.IsNull() || config.ElasticStackLoggerName.IsNullOrEmpty())
            {
                return;
            }

            _worker.Enqueue(async () =>
            {
                var model = new sql_log();

                switch (command.CommandType)
                {
                    case CommandType.Text:
                        model.CommandType = 1;
                        break;

                    case CommandType.StoredProcedure:
                        model.CommandType = 2;
                        break;
                }

                try
                {
                    model.LogSrNo = RandomTextHelper.CreateInt64String(20);
                    model.DaoName = daoName;
                    model.CommandId = commandId;
                    model.CommandDirection = 2;
                    model.CommandResult = NewtonsoftJsonConvert.Serialize(commandResult);
                    model.HostName = Dns.GetHostName();
                    model.LogTime = DateTime.UtcNow;

                    await ElasticsearchLoggerContainer.Get(config.ElasticStackLoggerName)?.InsertLogAsync<sql_log>(model, model.LogSrNo);
                }
                catch (Exception ex)
                {
                    Logger.Exception(this, ex, $"{nameof(BaseLogger)} insert SQL profile log to ElasticSearch occur exception.");
                }
            });
        }

        /// <summary>紀錄 SQL 執行語法
        /// </summary>
        /// <remarks>
        /// 1. 目前只應該剩下 BaseOracleDao 還有使用此方法紀錄 SQL Log。<para/>
        /// 2. 另外就是 BaseSqlDao 和 BaseMySqlDao 中在執行 DoExecuteReader() 方法中才會呼叫到這個方法來紀錄 SQL Log。
        /// </remarks>
        /// <param name="command">資料庫指令</param>
        /// <param name="daoName">資料存取物件的類別名稱</param>
        public void WriteLog(DbCommand command, string daoName = null)
        {
            WriteESLog(command, daoName);

            if (!CanLog())
            {
                return;
            }

            var model = new SqlLogEntry();

            switch (command.CommandType)
            {
                case CommandType.Text:
                    model.CommandType = 1;
                    break;

                case CommandType.StoredProcedure:
                    model.CommandType = 2;
                    break;
            }

            try
            {
                model.DaoName = daoName;
                model.CommandText = FetchLogCommandText(command);
                model.HostName = Dns.GetHostName();
                model.LogTime = DateTime.UtcNow;

                _worker.Enqueue(async () => await _dao.WriteLogAsync(model));
            }
            catch (Exception ex)
            {
                Logger.Exception(this, ex, $"{nameof(BaseLogger)} insert SQL log occur exception.");
            }
        }

        /// <summary>紀錄 SQL 執行語法到 ES 服務
        /// </summary>
        /// <remarks>
        /// 1. 目前只應該剩下 BaseOracleDao 還有使用此方法紀錄 SQL Log。<para/>
        /// 2. 另外就是 BaseSqlDao 和 BaseMySqlDao 中在執行 DoExecuteReader() 方法中才會呼叫到這個方法來紀錄 SQL Log。
        /// </remarks>
        /// <param name="command">資料庫指令</param>
        /// <param name="daoName">資料存取物件的類別名稱</param>
        public void WriteESLog(DbCommand command, string daoName = null)
        {
            var config = ConfigReader.GetDbProviderConfig(_dbName);

            if (config.IsNull() || config.ElasticStackLoggerName.IsNullOrEmpty())
            {
                return;
            }

            var model = new sql_log();

            switch (command.CommandType)
            {
                case CommandType.Text:
                    model.CommandType = 1;
                    break;

                case CommandType.StoredProcedure:
                    model.CommandType = 2;
                    break;
            }

            try
            {
                model.LogSrNo = RandomTextHelper.CreateInt64String(20);
                model.DaoName = daoName;
                model.CommandText = FetchLogCommandText(command);
                model.HostName = Dns.GetHostName();
                model.LogTime = DateTime.UtcNow;

                _worker.Enqueue(async () => await ElasticsearchLoggerContainer.Get(config.ElasticStackLoggerName)?.InsertLogAsync<sql_log>(model, model.LogSrNo));
            }
            catch (Exception ex)
            {
                Logger.Exception(this, ex, $"{nameof(BaseLogger)} insert SQL log to ElastisSearch occur exception.");
            }
        }

        #endregion 宣告公開的方法


        #region 宣告私有的方法

        /// <summary>判斷是否可以執行 SQL Log 日誌記錄
        /// </summary>
        /// <returns>是否可以執行 SQL Log 日誌記錄</returns>
        private bool CanLog()
        {
            try
            {
                return DataAccessSettingsOption.Settings.Find(o => o.Name == _dbName).EnableSqlLogProfile && _connectionString.IsNotNullOrEmpty();
            }
            catch (Exception ex)
            {
                Logger.Warn(this, $"ZayniFramework configuration error. No config value for /ZayniFramework/DataAccessSettings/DatabaseProviders sqlLogEnable.{Environment.NewLine}{ex}", $"{nameof(BaseLogger)}.{nameof(BaseLogger.CanLog)}");
                return false;
            }
        }

        #endregion 宣告私有的方法
    }
}
