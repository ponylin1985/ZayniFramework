﻿using Microsoft.Data.SqlClient;
using System.Data;
using System.Data.Common;
using System.Text;


namespace ZayniFramework.DataAccess
{
    /// <summary>MSSQL的SQL日誌記錄員
    /// </summary>
    internal sealed class MSSqlLogger : BaseLogger
    {
        #region 宣告建構子

        /// <summary>預設建構子
        /// </summary>
        /// <param name="dbName">資料庫名稱</param>
        public MSSqlLogger(string dbName) : base(dbName)
        {
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="dbName">資料庫名稱</param>
        /// <param name="zayniConnectionString">ZayniFramework 框架資料庫連線字串</param>
        public MSSqlLogger(string dbName, string zayniConnectionString) : base(dbName, zayniConnectionString)
        {
        }

        #endregion 宣告建構子


        #region 實作公開的抽象

        /// <summary>從來源的資料庫指令中擷取要進行 SQL 紀錄的語法
        /// </summary>
        /// <param name="command">來源資料庫指令</param>
        /// <returns>SQL 語法</returns>
        public override string FetchLogCommandText(DbCommand command)
        {
            var sbCommandText = new StringBuilder(command.CommandText);

            if (CommandType.StoredProcedure == command.CommandType)
            {
                sbCommandText.Insert(0, $" Exec dbo.{sbCommandText} ");
            }

            foreach (DbParameter p in command.Parameters)
            {
                var parameter = (SqlParameter)p;

                if ("@LogID_Update" == parameter.ParameterName)
                {
                    continue;
                }

                if ("@LogID_Approve" == parameter.ParameterName)
                {
                    continue;
                }

                if (
                        (parameter.SqlDbType != SqlDbType.SmallInt) &&
                        (parameter.SqlDbType != SqlDbType.Int) &&
                        (parameter.SqlDbType != SqlDbType.BigInt) &&
                        (parameter.SqlDbType != SqlDbType.Decimal)
                   )
                {
                    if (CommandType.StoredProcedure == command.CommandType)
                    {
                        sbCommandText.AppendFormat(" {0} = '{1}', ", parameter.ParameterName, parameter.Value.ToString());
                    }
                    else
                    {
                        sbCommandText = sbCommandText.Replace(parameter.ParameterName, $"'{parameter.Value}'");
                    }
                }
                else
                {
                    if (CommandType.StoredProcedure == command.CommandType)
                    {
                        sbCommandText.AppendFormat(" {0} = {1}, ", parameter.ParameterName, parameter.Value.ToString());
                    }
                    else
                    {
                        sbCommandText = sbCommandText.Replace(parameter.ParameterName, parameter.Value + "");
                    }
                }
            }

            var commandText = sbCommandText.ToString();

            if (CommandType.StoredProcedure == command.CommandType)
            {
                commandText = commandText[..(sbCommandText.Length - 1)];
            }

            return commandText;
        }

        #endregion 實作公開的抽象
    }
}
