﻿using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Threading.Tasks;
using ZayniFramework.Logging;
using ZayniFramework.Serialization;


namespace ZayniFramework.DataAccess
{
    /// <summary>MySQL 的 SQL Profile 日誌記錄資料存取類別
    /// </summary>
    internal class MySqlLoggerDao : LoggerDao
    {
        #region 宣告建構子

        /// <summary>多載建構子
        /// </summary>
        /// <param name="connectionString">ZayniFramework 框架的資料庫連線字串</param>
        public MySqlLoggerDao(string connectionString) : base(connectionString)
        {
        }

        #endregion 宣告建構子


        #region 實作公開的抽象

        /// <summary>寫入 SQL Profile 日誌記錄到資料庫中
        /// </summary>
        /// <param name="model">SQL Statement 日誌記錄資訊</param>
        public override async Task WriteLogAsync(SqlLogEntry model)
        {
            MySqlConnection conn = null;

            try
            {
                var sql = @"
                    --  Insert FS_SQL_LOG
                    INSERT INTO FS_SQL_LOG (
                        DAO_NAME,
                        COMMAND_ID,
                        COMMAND_DIRECTION,
                        COMMAND_TYPE,
                        COMMAND_TEXT,

                        COMMAND_RESULT,
                        HOST_NAME,
                        LOG_TIME
                    ) VALUES (
                        ?DaoName,
                        ?CommandId,
                        ?CommandDirection,
                        ?CommandType,
                        ?CommandTxt,

                        ?CommandResult,
                        ?HostName,
                        ?LogTime
                    ); ";

                using (conn = new MySqlConnection(ConnectionString))
                {
                    await conn.OpenAsync();

                    var cmd = new MySqlCommand(sql, conn)
                    {
                        CommandTimeout = 30
                    };

                    AddInParameter(cmd, "?DaoName", MySqlDbType.VarChar, model.DaoName);
                    AddInParameter(cmd, "?CommandId", MySqlDbType.VarChar, model.CommandId);
                    AddInParameter(cmd, "?CommandDirection", MySqlDbType.Int32, model.CommandDirection);
                    AddInParameter(cmd, "?CommandType", MySqlDbType.Int32, model.CommandType);
                    AddInParameter(cmd, "?CommandTxt", MySqlDbType.LongText, model.CommandText);

                    AddInParameter(cmd, "?CommandResult", MySqlDbType.LongText, model.CommandResult);
                    AddInParameter(cmd, "?HostName", MySqlDbType.VarChar, model.HostName);
                    AddInParameter(cmd, "?LogTime", MySqlDbType.DateTime, model.LogTime);

                    await cmd.ExecuteNonQueryAsync();
                    await conn.CloseAsync();
                }
            }
            catch
            {
                await ConsoleLogger.LogAsync($"{nameof(ILoggerDao)} Origin Log:{Environment.NewLine}{NewtonsoftJsonConvert.Serialize(model)}");
            }
        }

        #endregion 實作公開的抽象


        #region 宣告私有的方法

        /// <summary>加入 SQL 敘述的參數
        /// </summary>
        /// <param name="cmd">目標SQL指令</param>
        /// <param name="parameterName">參數名稱</param>
        /// <param name="dbType">參數資料庫型別</param>
        /// <param name="value">參數值</param>
        private static void AddInParameter(MySqlCommand cmd, string parameterName, MySqlDbType dbType, object value)
        {
            cmd.Parameters.Add(parameterName, dbType);
            cmd.Parameters[parameterName].Value = value ?? DBNull.Value;
            cmd.Parameters[parameterName].Direction = ParameterDirection.Input;
        }

        #endregion 宣告私有的方法
    }
}
