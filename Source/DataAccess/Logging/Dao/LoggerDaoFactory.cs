﻿using System;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.DataAccess
{
    /// <summary>SQL 日誌記錄資料存取類別工廠
    /// </summary>
    internal class LoggerDaoFactory
    {
        #region 宣告私有的靜態欄位

        /// <summary>預設資料庫連線字串名稱
        /// </summary>
        private static readonly string _connectionStringName = "Zayni";

        /// <summary>資料庫連線字串
        /// </summary>
        private static string _connectionString;

        #endregion 宣告私有的靜態欄位


        #region 宣告靜態建構子

        /// <summary>靜態建構子
        /// </summary>
        static LoggerDaoFactory()
        {
            try
            {
                var g = ConfigReader.GetDbConnectionString(_connectionStringName);

                if (g.Success)
                {
                    _connectionString = g.Data;
                }
            }
            catch
            {
            }
        }

        #endregion 宣告靜態建構子


        #region 宣告公開的靜態方法

        /// <summary>建立 SQL 日誌記錄資料存取物件
        /// </summary>
        /// <returns>SQL 日誌記錄資料存取物件</returns>
        public static LoggerDao Create(out string zayniConnectionString)
        {
            zayniConnectionString = _connectionString;
            var zayniProvider = "mssql";

            try
            {
                zayniProvider =
                    ConfigManager
                        .GetZayniFrameworkSettings()
                        .DataAccessSettings
                        .Find(d => d.Name == _connectionStringName)
                        .DatabaseProvider;
            }
            catch (Exception ex)
            {
                Logger.Warn("LoggerDaoFactory", $"ZayniFramework/DataAccessSettings/DatabaseProviders config error. {ex}", "LoggerDaoFactory.Create");
            }

            return zayniProvider.ToLower() switch
            {
                "mssql" => new MSSqlLoggerDao(_connectionString),
                "mysql" => new MySqlLoggerDao(_connectionString),
                "postgresql" => new PgSqlLoggerDao(_connectionString),
                "oracle" => new OracleLoggerDao(_connectionString),
                _ => null,
            };
        }

        /// <summary>建立 SQL 日誌記錄資料存取物件
        /// </summary>
        /// <param name="zayniConnectionString">ZayniFramework 框架的資料庫連線字串</param>
        /// <returns>SQL 日誌記錄資料存取物件</returns>
        public static LoggerDao Create(string zayniConnectionString = null)
        {
            zayniConnectionString.IsNotNullOrEmpty(s => _connectionString = zayniConnectionString);
            var zayniProvider =
                    ConfigManager
                        .GetZayniFrameworkSettings()
                        .DataAccessSettings
                        .Find(d => d.Name == _connectionStringName)
                        .DatabaseProvider;

            return zayniProvider.ToLower() switch
            {
                "mssql" => new MSSqlLoggerDao(_connectionString),
                "mysql" => new MySqlLoggerDao(_connectionString),
                "postgresql" => new PgSqlLoggerDao(_connectionString),
                "oracle" => new OracleLoggerDao(_connectionString),
                _ => null,
            };
        }

        #endregion 宣告公開的靜態方法
    }
}
