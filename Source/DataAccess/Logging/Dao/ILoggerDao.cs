﻿using System.Threading.Tasks;


namespace ZayniFramework.DataAccess
{
    /// <summary>SQL 日誌記錄資料存取介面
    /// </summary>
    internal interface ILoggerDao
    {
        /// <summary>寫入 SQL 日誌記錄到資料庫中
        /// </summary>
        /// <param name="model">SQL 日誌記錄資訊</param>
        Task WriteLogAsync(SqlLogEntry model);
    }
}
