﻿using System.Threading.Tasks;


namespace ZayniFramework.DataAccess
{
    /// <summary>SQL 日誌記錄資料存取類別基底
    /// </summary>
    internal abstract class LoggerDao : ILoggerDao
    {
        #region 宣告建構子

        /// <summary>多載建構子
        /// </summary>
        /// <param name="connectionString">ZayniFramework 框架的資料庫連線字串</param>
        public LoggerDao(string connectionString)
        {
            ConnectionString = connectionString;
        }

        /// <summary>解構子
        /// </summary>
        ~LoggerDao()
        {
            ConnectionString = null;
        }

        #endregion 宣告建構子


        #region 宣告保護的屬性

        /// <summary>ZayniFramework 框架的資料庫連線字串
        /// </summary>
        protected string ConnectionString { get; set; }

        #endregion 宣告保護的屬性


        #region 宣告公開的抽象

        /// <summary>寫入SQL日誌記錄到資料庫中
        /// </summary>
        /// <param name="model">SQL日誌記錄資訊</param>
        public abstract Task WriteLogAsync(SqlLogEntry model);

        #endregion 宣告公開的抽象
    }
}
