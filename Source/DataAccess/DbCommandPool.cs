﻿using NeoSmart.AsyncLock;
using System;
using System.Collections.Generic;
using System.Data.Common;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.DataAccess
{
    /// <summary>資料庫 SQL 指令池
    /// </summary>
    internal sealed class DbCommandPool
    {
        #region 宣告私有的欄位

        /// <summary>非同步作業鎖定物件
        /// </summary>
        private readonly AsyncLock _asyncLock = new();

        /// <summary>資料庫 SQL 指令池，暫存目前所有對資料庫的 DbCommand 指令<para/>
        /// Key 值: 指令識別代碼 (CommandId)<para/>
        /// Value 值: 資料庫 SQL 指令物件
        /// </summary>
        private readonly Dictionary<string, DbCommand> _dbCommandPool = [];

        #endregion 宣告私有的欄位


        #region 宣告內部的方法

        /// <summary>新增資料庫 SQL 指令
        /// </summary>
        /// <param name="command">資料庫 SQL 指令</param>
        /// <param name="commandId">指令識別代碼</param>
        /// <returns>新增是否成功</returns>
        internal bool Add(DbCommand command, out string commandId)
        {
            commandId = null;

            try
            {
                using (_asyncLock.Lock())
                {
                    commandId = CreateCommandId();
                    _dbCommandPool.Add(commandId, command);
                }

                return true;
            }
            catch (Exception ex)
            {
                Logger.Exception(this, ex, $"{GetLogTraceName(nameof(Add))}, add DbCommand to pool occur exception.");
                return false;
            }
        }

        /// <summary>移除資料庫 SQL 指令
        /// </summary>
        /// <param name="commandId">指令識別代碼</param>
        /// <returns>移除是否成功</returns>
        internal bool Remove(string commandId)
        {
            try
            {
                using (_asyncLock.Lock())
                {
                    return _dbCommandPool.Remove(commandId);
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(this, ex, $"{GetLogTraceName(nameof(Remove))}, remove DbCommand from pool occur exception.");
                return false;
            }
        }

        /// <summary>取得資料庫 SQL 指令
        /// </summary>
        /// <param name="commandId">指令識別代碼</param>
        /// <param name="command">資料庫 SQL 指令</param>
        /// <returns>是否取得成功</returns>
        internal bool Get(string commandId, out DbCommand command)
        {
            command = null;

            try
            {
                var result = _dbCommandPool.TryGetValue(commandId, out command);

                if (!result)
                {
                    Logger.Error(this, $"{GetLogTraceName(nameof(Get))}, get DbCommand from pool fail. CommandId: {commandId}.", GetLogTraceName(nameof(Get)));
                    return false;
                }

                return result;
            }
            catch (Exception ex)
            {
                Logger.Exception(this, ex, $"{GetLogTraceName(nameof(Get))}, get DbCommand from pool occur exception. CommandId: {commandId}.");
                return false;
            }
        }

        #endregion 宣告內部的方法


        #region 宣告私有的方法

        /// <summary>建立資料庫 SQL 指令識別代碼，長度為 20 碼。
        /// </summary>
        /// <returns>資料庫 SQL 指令識別代碼</returns>
        private string CreateCommandId()
        {
            var requestId = RandomTextHelper.Create(20);

            if (_dbCommandPool.ContainsKey(requestId))
            {
                requestId = CreateCommandId();
            }

            return requestId;
        }

        /// <summary>取得日誌的動作名稱
        /// </summary>
        /// <param name="methodName">方法名稱</param>
        /// <returns>日誌的動作名稱</returns>
        private string GetLogTraceName(string methodName) => $"{nameof(DbCommandPool)}.{methodName}";

        #endregion 宣告私有的方法
    }
}
