﻿using System;
using System.Data;


namespace ZayniFramework.DataAccess
{
    /// <summary>ADO.NET DataRow 擴充類別
    /// </summary>
    public static class DataRowExtension
    {
        /// <summary>取得 Byte 整數值
        /// </summary>
        /// <param name="row">ADO.NET DataRow 資料列物件</param>
        /// <param name="columnName">欄位名稱</param>
        /// <returns>Byte 整數值</returns>
        public static int GetByte(this DataRow row, string columnName) => DataRowConverter.GetByte(row, columnName);

        /// <summary>取得 Byte Nullable 整數值
        /// </summary>
        /// <param name="row">ADO.NET DataRow 資料列物件</param>
        /// <param name="columnName">欄位名稱</param>
        /// <returns>Byte Nullable 整數值</returns>
        public static byte? GetByteNullable(this DataRow row, string columnName) => DataRowConverter.GetByteNullable(row, columnName);

        /// <summary>取得 Int32 整數值
        /// </summary>
        /// <param name="row">ADO.NET DataRow 資料列物件</param>
        /// <param name="columnName">欄位名稱</param>
        /// <returns>Int32 整數值</returns>
        public static int GetInt32(this DataRow row, string columnName) => DataRowConverter.GetInt32(row, columnName);

        /// <summary>取得 Int32 Nullable 整數值
        /// </summary>
        /// <param name="row">ADO.NET DataRow 資料列物件</param>
        /// <param name="columnName">欄位名稱</param>
        /// <returns>Int32 Nullable 整數值</returns>
        public static int? GetInt32Nullable(this DataRow row, string columnName) => DataRowConverter.GetInt32Nullable(row, columnName);

        /// <summary>取得 Int64 整數值
        /// </summary>
        /// <param name="row">ADO.NET DataRow 資料列物件</param>
        /// <param name="columnName">欄位名稱</param>
        /// <returns>Int64 整數值</returns>
        public static long GetInt64(this DataRow row, string columnName) => DataRowConverter.GetInt64(row, columnName);

        /// <summary>取得 Int64 Nullable 整數值
        /// </summary>
        /// <param name="row">ADO.NET DataRow 資料列物件</param>
        /// <param name="columnName">欄位名稱</param>
        /// <returns>Int64 Nullable 整數值</returns>
        public static long? GetInt64Nullable(this DataRow row, string columnName) => DataRowConverter.GetInt64Nullable(row, columnName);

        /// <summary>取得 Double 浮點數值
        /// </summary>
        /// <param name="row">ADO.NET DataRow 資料列物件</param>
        /// <param name="columnName">欄位名稱</param>
        /// <returns>Double 浮點數值</returns>
        public static double GetDouble(this DataRow row, string columnName) => DataRowConverter.GetDouble(row, columnName);

        /// <summary>取得 Double Nullable 整數值
        /// </summary>
        /// <param name="row">ADO.NET DataRow 資料列物件</param>
        /// <param name="columnName">欄位名稱</param>
        /// <returns>Double Nullable 整數值</returns>
        public static double? GetDoubleNullable(this DataRow row, string columnName) => DataRowConverter.GetDoubleNullable(row, columnName);

        /// <summary>取得 Decimal 數值
        /// </summary>
        /// <param name="row">ADO.NET DataRow 資料列物件</param>
        /// <param name="columnName">欄位名稱</param>
        /// <returns>Decimal 數值</returns>
        public static decimal GetDecimal(this DataRow row, string columnName) => DataRowConverter.GetDecimal(row, columnName);

        /// <summary>取得 Decimal Nullable 整數值
        /// </summary>
        /// <param name="row">ADO.NET DataRow 資料列物件</param>
        /// <param name="columnName">欄位名稱</param>
        /// <returns>Decimal Nullable 整數值</returns>
        public static decimal? GetDecimalNullable(this DataRow row, string columnName) => DataRowConverter.GetDecimalNullable(row, columnName);

        /// <summary>取得 DateTime 日期時間，包含資料庫中時間的毫秒數。
        /// </summary>
        /// <param name="row">ADO.NET DataRow資料列物件 </param>
        /// <param name="columnName">欄位名稱</param>
        /// <returns>DateTime 日期時間，包含資料庫中時間的毫秒數。</returns>
        public static DateTime GetDateTime(this DataRow row, string columnName) => DataRowConverter.GetDateTime(row, columnName);

        /// <summary>取得 DateTime? Nullable 日期時間
        /// </summary>
        /// <param name="row">ADO.NET DataRow資料列物件</param>
        /// <param name="columnName">欄位名稱</param>
        /// <returns>DateTime? Nullable 日期時間</returns>
        public static DateTime? GetDateTimeNullable(this DataRow row, string columnName) => DataRowConverter.GetDateTimeNullable(row, columnName);

        /// <summary>取得字串值
        /// </summary>
        /// <param name="row">ADO.NET DataRow 資料列物件</param>
        /// <param name="columnName">欄位名稱</param>
        /// <returns>字串值</returns>
        public static string GetString(this DataRow row, string columnName) => DataRowConverter.GetString(row, columnName);

        /// <summary>取得布林值
        /// </summary>
        /// <param name="row">ADO.NET DataRow 資料列物件</param>
        /// <param name="columnName">欄位名稱</param>
        /// <returns>布林值</returns>
        public static bool GetBoolean(this DataRow row, string columnName) => DataRowConverter.GetBoolean(row, columnName);

        /// <summary>取得 bool? 布林值
        /// </summary>
        /// <param name="row">ADO.NET DataRow 資料列物件</param>
        /// <param name="columnName">欄位名稱</param>
        /// <returns>bool? 布林值</returns>
        public static bool? GetBooleanNullable(this DataRow row, string columnName) => DataRowConverter.GetBooleanNullable(row, columnName);

        /// <summary>取得二進位陣列 (byte[]) 資料
        /// </summary>
        /// <param name="row">ADO.NET DataRow 資料列物件</param>
        /// <param name="columnName">欄位名稱</param>
        /// <returns>二進位資料</returns>
        public static byte[] GetBinary(this DataRow row, string columnName) => DataRowConverter.GetBinary(row, columnName);
    }
}
