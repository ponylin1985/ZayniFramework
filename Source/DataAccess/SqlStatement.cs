﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using ZayniFramework.Common;


namespace ZayniFramework.DataAccess
{
    /// <summary>SQL 敘述
    /// </summary>
    internal sealed class SqlStatement
    {
        #region 宣告私有的欄位

        /// <summary>SQL指令字串
        /// </summary>
        private string _sql;

        #endregion 宣告私有的欄位


        #region 宣告建構子

        /// <summary>建構子
        /// </summary>
        /// <param name="sqlCommand">SQL指令字串</param>
        internal SqlStatement(string sqlCommand)
        {
            if (sqlCommand.IsNullOrEmpty())
            {
                throw new ArgumentException("ZayniFramework.DataAccess.SqlStatement建構子的參數不可以為空字串或Null值。");
            }

            _sql = sqlCommand;
        }

        /// <summary>解構子
        /// </summary>
        ~SqlStatement()
        {
            _sql = null;
        }

        #endregion 宣告建構子


        #region 宣告內部的方法

        /// <summary>將指定的字串用空字串取代掉
        /// </summary>
        /// <param name="str">指定的目標字串</param>
        /// <returns>SQL敘述</returns>
        internal SqlStatement Remove(string str)
        {
            return Replace(str, "");
        }

        /// <summary>以新的字串取代舊有的字串
        /// </summary>
        /// <param name="oldString">舊有的字串</param>
        /// <param name="newString">新的字串</param>
        /// <returns>SQL敘述</returns>
        internal SqlStatement Replace(string oldString, string newString)
        {
            _sql = _sql.Replace(oldString, newString);
            return this;
        }

        /// <summary>去除掉所有SQL敘述中的單行註解
        /// </summary>
        /// <returns>SQL敘述</returns>
        internal SqlStatement TrimSingleComment()
        {
            var strAry = Regex.Split(_sql, @"[\r\n]");
            var sb = new StringBuilder();

            for (var i = 0; i < strAry.Length; i++)
            {
                foreach (var t in Regex.Matches(strAry[i], @"-{2,}.*"))
                {
                    strAry[i] = strAry[i].Replace(t.ToString(), "");
                }

                sb.Append(strAry[i]);
            }

            _sql = sb.ToString();

            return this;
        }

        /// <summary>印出SQL續數字串
        /// </summary>
        /// <returns>SQL敘述字串</returns>
        public override string ToString() => _sql + "";

        #endregion 宣告內部的方法
    }
}
