﻿using NeoSmart.AsyncLock;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.DataAccess
{
    /// <summary>基礎資料存取元件Dao
    /// </summary>
    /// <remarks>多載建構子
    /// </remarks>
    /// <param name="providerType">資料庫廠牌種類</param>
    public abstract class BaseDao(DbProviderOption providerType)
    {
        #region 宣告靜態建構子

        /// <summary>靜態建構子
        /// </summary>
        static BaseDao() => ConfigReader.LoadSettings();

        #endregion 宣告靜態建構子


        #region 宣告私有的欄位

        /// <summary>非同步作業鎖定物件，鎖定 _dataCount 操作。
        /// </summary>
        private readonly AsyncLock _asyncLockCount = new();

        /// <summary>非同步作業鎖定物件，鎖定 _message 操作。
        /// </summary>
        /// <returns></returns>
        private readonly AsyncLock _asyncLockMessage = new();

        /// <summary>資料存取處理專員
        /// </summary>
        private DbProviderAgent _agent = new(providerType);

        /// <summary>執行結果影響資料列數
        /// </summary>
        private int _dataCount;

        /// <summary>執行結果訊息、執行結果錯誤訊息
        /// </summary>
        private string _message;

        #endregion 宣告私有的欄位
        #region 宣告建構子

        /// <summary>解構子
        /// </summary>
        ~BaseDao()
        {
            _agent = null;
        }

        #endregion 宣告建構子


        #region 宣告保護或公開的屬性

        /// <summary>資料庫 SQL 指令池
        /// </summary>
        internal DbCommandPool DbCommands { get; private set; } = new DbCommandPool();

        /// <summary>資料庫連線名稱 (資料庫連線字串名稱)
        /// </summary>
        protected string DbName { get; set; }

        /// <summary>資料庫連線字串
        /// </summary>
        public string ConnectionString { get; protected set; }

        /// <summary>ORM資料繫結器
        /// </summary>
        protected OrmDataBinder OrmBinder { get; set; }

        /// <summary>執行結果訊息、執行結果錯誤訊息
        /// </summary>
        public string Message
        {
            get
            {
                using (_asyncLockMessage.Lock())
                {
                    return _message;
                }
            }
            set
            {
                using (_asyncLockMessage.Lock())
                {
                    _message = value;
                }
            }
        }

        /// <summary>執行結果影響資料列數 (保護層級的唯獨屬性)
        /// </summary>
        public int DataCount
        {
            get
            {
                using (_asyncLockCount.Lock())
                {
                    return _dataCount;
                }
            }
            protected set
            {
                using (_asyncLockCount.Lock())
                {
                    _dataCount = value;
                }
            }
        }

        /// <summary>資料庫異常物件
        /// </summary>
        public Exception ExceptionObject { get; protected set; }

        #endregion 宣告保護或公開的屬性


        #region 宣告保護的方法

        /// <summary>讀取資料庫連線字串
        /// </summary>
        protected void LoadConnectionString()
        {
            var logTitle = Logger.GetTraceLogTitle(this, nameof(LoadConnectionString));

            if (DbName.IsNullOrEmpty())
            {
                Message = $"DbName can not be null or empty string.";
                Logger.Error(this, Message, logTitle);
                ConsoleLogger.LogError($"{logTitle} {Message}");
                throw new ArgumentException(Message);
            }

            var g = ConfigReader.GetDbConnectionString(DbName);

            if (!g.Success)
            {
                Message = $"Load database connection string fail. DbName: {DbName} {Environment.NewLine}{g.Message}";
                Logger.Error(this, Message, logTitle);
                ConsoleLogger.LogError($"{logTitle} {Message}");
                throw new ArgumentException(Message);
            }

            ConnectionString = g.Data;

            if (ConnectionString.IsNullOrEmpty())
            {
                Message = $"Database connection string can not be null or empty string. DbName: {DbName}.";
                Logger.Error(this, Message, logTitle);
                ConsoleLogger.LogError($"{logTitle} {Message}");
                throw new ArgumentException(Message);
            }
        }

        #endregion 宣告保護的方法


        #region 宣告公開的抽象

        /// <summary>取得資料庫的空字串值
        /// </summary>
        /// <returns>資料庫的空字串值</returns>
        public abstract string GetDBEmptyString();

        #endregion 宣告公開的抽象


        #region 宣告公開的連線、交易與資源釋放方法

        /// <summary>建立並且開啟資料庫連線<para/>
        /// * 假若無法正常開啟資料庫連線，則直接拋出 exception 異常物件。
        /// </summary>
        /// <param name="dbName">資料庫連線字串名稱</param>
        /// <returns>新的資料庫連線</returns>
        public async Task<DbConnection> CreateConnectionAsync(string dbName = "")
        {
            var logTitle = Logger.GetTraceLogTitle(this, nameof(CreateConnectionAsync));
            DbConnection connection = null;

            if (ConnectionString.IsNullOrEmpty())
            {
                dbName = dbName.IsNullOrEmptyString(DbName, true);
                var r = ConfigReader.GetDbConnectionString(dbName);

                if (!r.Success)
                {
                    Message = $"Load db connection string fail. {r.Message}";
                    Logger.Error(this, Message, logTitle);
                    ConsoleLogger.LogError($"{logTitle} {Message}");
                    throw new ApplicationException(Message);
                }

                ConnectionString = r.Data;
            }

            if (ConnectionString.IsNullOrEmpty())
            {
                Message = $"Connection string null or empty string. DbName: {DbName}.";
                Logger.Error(this, Message, logTitle);
                ConsoleLogger.LogError($"{logTitle} {Message}");
                throw new ApplicationException(Message);
            }

            try
            {
                connection = _agent.CreateDbConnection(ConnectionString);
                await connection.OpenAsync();
            }
            catch (Exception ex)
            {
                Message = $"Create new db connection occur exception. DbName: {DbName}, ConnectionString: {ConnectionStringMask.Mask(ConnectionString)}.";
                Logger.Error(this, $"{Message}{Environment.NewLine}{ex}", logTitle);
                ConsoleLogger.LogError($"{logTitle} {Message}{Environment.NewLine}{ex}");
                throw;
            }

            return connection;
        }

        /// <summary>建立並且開啟資料庫連線<para/>
        /// * 假若無法正常開啟資料庫連線，則直接拋出 exception 異常物件。
        /// </summary>
        /// <param name="dbName">資料庫連線字串名稱</param>
        /// <returns>新的資料庫連線</returns>
        public DbConnection CreateConnection(string dbName = "")
        {
            var logTitle = Logger.GetTraceLogTitle(this, nameof(CreateConnection));
            DbConnection connection = null;

            if (ConnectionString.IsNullOrEmpty())
            {
                dbName = dbName.IsNullOrEmptyString(DbName, true);
                var r = ConfigReader.GetDbConnectionString(dbName);

                if (!r.Success)
                {
                    Message = $"Load db connection string fail. {r.Message}";
                    Logger.Error(this, Message, logTitle);
                    ConsoleLogger.LogError($"{logTitle} {Message}");
                    throw new ApplicationException(Message);
                }

                ConnectionString = r.Data;
            }

            if (ConnectionString.IsNullOrEmpty())
            {
                Message = $"Connection string null or empty string. DbName: {DbName}.";
                Logger.Error(this, Message, logTitle);
                ConsoleLogger.LogError($"{logTitle} {Message}");
                throw new ApplicationException(Message);
            }

            try
            {
                connection = _agent.CreateDbConnection(ConnectionString);
                connection.Open();
            }
            catch (Exception ex)
            {
                Message = $"Create new db connection occur exception. DbName: {DbName}, ConnectionString: {ConnectionStringMask.Mask(ConnectionString)}.";
                Logger.Error(this, $"{Message}{Environment.NewLine}{ex}", logTitle);
                ConsoleLogger.LogError($"{logTitle} {Message}{Environment.NewLine}{ex}");
                throw;
            }

            return connection;
        }

        /// <summary>開啟資料庫交易<para/>
        /// * 假若無法正常開啟資料庫交易，則直接拋出 exception 異常物件。
        /// </summary>
        /// <param name="connection">來源資料庫連線</param>
        /// <param name="isolationLevel">交易鎖定層級</param>
        /// <returns>資料庫交易</returns>
        public DbTransaction BeginTransaction(DbConnection connection, IsolationLevel isolationLevel = IsolationLevel.ReadCommitted)
        {
            var logTitle = Logger.GetTraceLogTitle(this, nameof(BeginTransaction));

            try
            {
                if (connection.IsNull())
                {
                    Message = $"Can not begin db transaction due to the connection argument is null.";
                    Logger.Error(this, $"{Message}", logTitle);
                    throw new ArgumentNullException(Message);
                }

                return connection.BeginTransaction(isolationLevel);
            }
            catch (Exception ex)
            {
                Message = $"Begin db transaction occur exception.{Environment.NewLine}{ex}";
                Logger.Error(this, $"{Message}", logTitle);
                throw;
            }
        }

        /// <summary>開啟資料庫交易<para/>
        /// * 假若無法正常開啟資料庫交易，則直接拋出 exception 異常物件。
        /// </summary>
        /// <param name="connection">來源資料庫連線</param>
        /// <param name="isolationLevel">交易鎖定層級</param>
        /// <returns>資料庫交易</returns>
        public async Task<DbTransaction> BeginTransactionAsync(DbConnection connection, IsolationLevel isolationLevel = IsolationLevel.ReadCommitted)
        {
            var logTitle = Logger.GetTraceLogTitle(this, nameof(BeginTransaction));

            try
            {
                if (connection.IsNull())
                {
                    Message = $"Can not begin db transaction due to the connection argument is null.";
                    Logger.Error(this, $"{Message}", logTitle);
                    throw new ArgumentNullException(Message);
                }

                return await connection.BeginTransactionAsync(isolationLevel);
            }
            catch (Exception ex)
            {
                Message = $"Begin db transaction occur exception.{Environment.NewLine}{ex}";
                Logger.Error(this, $"{Message}", logTitle);
                throw;
            }
        }

        /// <summary>確認目標資料庫交易
        /// </summary>
        /// <param name="transaction">目標資料庫交易</param>
        /// <returns>是否確認交易成功</returns>
        public bool Commit(DbTransaction transaction)
        {
            var logTitle = Logger.GetTraceLogTitle(this, nameof(Commit));

            if (transaction.IsNull())
            {
                Message = $"Can not commit DbTransaction due to the transaction object is null.";
                Logger.Error(this, Message, logTitle);
                return false;
            }

            try
            {
                transaction.Commit();
            }
            catch (Exception ex)
            {
                Message = $"Commit the DbTransaction occur exception. {Environment.NewLine}{ex}";
                ExceptionObject = ex;
                Logger.Error(this, $"{Message}{Environment.NewLine}{ex}", logTitle);
                return false;
            }

            return true;
        }

        /// <summary>確認目標資料庫交易
        /// </summary>
        /// <param name="transaction">目標資料庫交易</param>
        /// <returns>是否確認交易成功</returns>
        public async Task<bool> CommitAsync(DbTransaction transaction)
        {
            var logTitle = Logger.GetTraceLogTitle(this, nameof(Commit));

            if (transaction.IsNull())
            {
                Message = $"Can not commit DbTransaction due to the transaction object is null.";
                Logger.Error(this, Message, logTitle);
                return false;
            }

            try
            {
                await transaction.CommitAsync();
            }
            catch (Exception ex)
            {
                Message = $"Commit the DbTransaction occur exception. {Environment.NewLine}{ex}";
                ExceptionObject = ex;
                Logger.Error(this, $"{Message}{Environment.NewLine}{ex}", logTitle);
                return false;
            }

            return true;
        }

        /// <summary>取消目標資料庫交易
        /// </summary>
        /// <param name="transaction">目標資料庫交易</param>
        /// <returns>是否取消交易成功</returns>
        public bool Rollback(DbTransaction transaction)
        {
            var logTitle = Logger.GetTraceLogTitle(this, nameof(Rollback));

            if (transaction.IsNull())
            {
                Message = $"Can not rollback DbTransaction due to the transaction object is null.";
                Logger.Error(this, Message, logTitle);
                return false;
            }

            try
            {
                transaction.Rollback();
            }
            catch (Exception ex)
            {
                Message = $"Rollback the DbTransaction occur exception. {Environment.NewLine}{ex}";
                ExceptionObject = ex;
                Logger.Error(this, $"{Message}{Environment.NewLine}{ex}", logTitle);
                return false;
            }

            return true;
        }

        /// <summary>取消目標資料庫交易
        /// </summary>
        /// <param name="transaction">目標資料庫交易</param>
        /// <returns>是否取消交易成功</returns>
        public async Task<bool> RollbackAsync(DbTransaction transaction)
        {
            var logTitle = Logger.GetTraceLogTitle(this, nameof(Rollback));

            if (transaction.IsNull())
            {
                Message = $"Can not rollback DbTransaction due to the transaction object is null.";
                Logger.Error(this, Message, logTitle);
                return false;
            }

            try
            {
                await transaction.RollbackAsync();
            }
            catch (Exception ex)
            {
                Message = $"Rollback the DbTransaction occur exception. {Environment.NewLine}{ex}";
                ExceptionObject = ex;
                Logger.Error(this, $"{Message}{Environment.NewLine}{ex}", logTitle);
                return false;
            }

            return true;
        }

        #endregion 宣告公開的連線、交易與資源釋放方法


        #region 宣告保護的執行資料庫 SQL 指令的樣板方法

        /// <summary>執行目標 SQL 敘述命令
        /// </summary>
        /// <param name="command">目標 SQL 敘述命令</param>
        /// <param name="handler">執行 ADO.NET 資料庫非查詢的資料庫指令的委派</param>
        /// <returns>執行目標SQL命令是否成功</returns>
        protected async Task<bool> DoExecuteNonQueryAsync(DbCommand command, ExecuteNonQueryAsyncHandler handler)
        {
            var logTitle = Logger.GetTraceLogTitle(this, nameof(DoExecuteNonQueryAsync));

            try
            {
                DataCount = await handler();
            }
            catch (Exception ex)
            {
                Message = $"Execute SQL DbCommand occur exception. SQL statement:{Environment.NewLine}{command.CommandText}. {Environment.NewLine}DbCommandParameters:{Environment.NewLine}{command.Parameters.GetParametersLog()}.";
                DataCount = 0;
                ExceptionObject = ex;
                Logger.Error(this, $"{Message}{Environment.NewLine}{ex}", logTitle);
                return false;
            }

            return true;
        }

        /// <summary>執行目標 SQL 敘述命令
        /// </summary>
        /// <param name="command">目標 SQL 敘述命令</param>
        /// <param name="handler">執行 ADO.NET 資料庫非查詢的資料庫指令的委派</param>
        /// <returns>執行目標SQL命令是否成功</returns>
        protected bool DoExecuteNonQuery(DbCommand command, ExecuteNonQueryHandler handler)
        {
            var logTitle = Logger.GetTraceLogTitle(this, nameof(DoExecuteNonQuery));

            try
            {
                DataCount = handler();
            }
            catch (Exception ex)
            {
                Message = $"Execute SQL DbCommand occur exception. SQL statement:{Environment.NewLine}{command.CommandText}. {Environment.NewLine}DbCommandParameters:{Environment.NewLine}{command.Parameters.GetParametersLog()}.";
                DataCount = 0;
                ExceptionObject = ex;
                Logger.Error(this, $"{Message}{Environment.NewLine}{ex}", logTitle);
                return false;
            }

            return true;
        }

        /// <summary>執行目標 SQL 敘述，回傳 Name Value Tuple 物件。
        /// <para>* 回傳值:</para>
        /// <para>  * success: 執行 SQL 敘述是否成功</para>
        /// <para>  * reader: DataReader 資料讀取器物件</para>
        /// </summary>
        /// <param name="command">目標 SQL 敘述命令</param>
        /// <param name="handler">執行 ADO.NET 資料庫查詢的資料庫指令的委派</param>
        /// <returns>
        /// <para>* success: 執行 SQL 敘述是否成功</para>
        /// <para>* reader: DataReader 資料讀取器物件</para>
        /// </returns>
        protected async Task<Result<IDataReader>> DoExecuteReaderAsync(DbCommand command, ExecuteReaderAsyncHandler handler)
        {
            var logTitle = Logger.GetTraceLogTitle(this, nameof(DoExecuteReaderAsync));
            var result = Result.Create<IDataReader>(false, data: null, message: null);

            IDataReader reader = null;

            try
            {
                reader = await handler();

                // 20180912 Marked by Pony: 在 MySQL 的情況下，好像也不能先把 DbCommand Dispose 掉。
                //command.Dispose();

                // 20140206 Pony Says: 不可以自行關閉資料庫連線，或釋放連線，因為會造成 DataReader 自動被 Close 掉，造成外界無法使用 DataReader 取得資料
                //if ( trans.IsNull() || InTransaction )
                //{
                //    cmd.Connection.Close();
                //    cmd.Connection.Dispose();
                //}
            }
            catch (Exception ex)
            {
                Message = $"Execute SQL DbCommand occur exception. SQL statement:{Environment.NewLine}{command.CommandText}. {Environment.NewLine}DbCommandParameters:{Environment.NewLine}{command.Parameters.GetParametersLog()}.";
                ExceptionObject = ex;
                result.Message = Message;
                result.ExceptionObject = ex;
                Logger.Error(this, $"{Message}{Environment.NewLine}{ex}", logTitle);
                return result;
            }

            if (reader.IsNull())
            {
                Message = $"ExecuteDataReader occur error due to the IDataReader is null. SQL statement:{Environment.NewLine}{command.CommandText}. {Environment.NewLine}DbCommandParameters:{Environment.NewLine}{command.Parameters.GetParametersLog()}.";
                result.Message = Message;
                Logger.Error(this, Message, logTitle);
                return result;
            }

            result.Data = reader;
            result.Success = true;
            return result;
        }

        /// <summary>執行目標 SQL 敘述，並且取得資料讀取器
        /// </summary>
        /// <param name="command">目標 SQL 敘述命令</param>
        /// <param name="handler">執行 ADO.NET 資料庫查詢的資料庫指令的委派</param>
        /// <param name="reader">資料讀取器</param>
        /// <returns>執行目標SQL命令是否成功</returns>
        protected bool DoExecuteReader(DbCommand command, ExecuteReaerHandler handler, out IDataReader reader)
        {
            var logTitle = Logger.GetTraceLogTitle(this, nameof(DoExecuteReader));
            reader = null;

            try
            {
                reader = handler();

                // 20180912 Marked by Pony: 在 MySQL 的情況下，好像也不能先把 DbCommand Dispose 掉。
                //command.Dispose();

                // 20140206 Pony Says: 不可以自行關閉資料庫連線，或釋放連線，因為會造成 DataReader 自動被 Close 掉，造成外界無法使用 DataReader 取得資料
                //if ( trans.IsNull() || InTransaction )
                //{
                //    cmd.Connection.Close();
                //    cmd.Connection.Dispose();
                //}
            }
            catch (Exception ex)
            {
                Message = $"Execute SQL DbCommand occur exception. SQL statement:{Environment.NewLine}{command.CommandText}. {Environment.NewLine}DbCommandParameters:{Environment.NewLine}{command.Parameters.GetParametersLog()}.";
                ExceptionObject = ex;
                Logger.Error(this, $"{Message}{Environment.NewLine}{ex}", logTitle);
                return false;
            }

            if (reader.IsNull())
            {
                Message = $"ExecuteDataReader occur error due to the IDataReader is null. SQL statement:{Environment.NewLine}{command.CommandText}. {Environment.NewLine}DbCommandParameters:{Environment.NewLine}{command.Parameters.GetParametersLog()}.";
                Logger.Error(this, Message, logTitle);
                return false;
            }

            return true;
        }

        /// <summary>執行目標 SQL 敘述，並且查詢單一純量值
        /// </summary>
        /// <param name="command">目標SQL敘述命令</param>
        /// <param name="handler">執行 ADO.NET 資料庫單一純量查詢指令的委派</param>
        /// <returns>執行目標 SQL 命令是否成功</returns>
        protected async Task<Result<object>> DoExecuteScalarAsync(DbCommand command, ExecuteScalarAsyncHandler handler)
        {
            var logTitle = Logger.GetTraceLogTitle(this, nameof(DoExecuteScalarAsync));
            var result = Result.Create<object>();

            try
            {
                var obj = await handler();
                return Result.Create<object>(true, data: obj);
            }
            catch (Exception ex)
            {
                Message = $"Execute SQL DbCommand occur exception. SQL statement:{Environment.NewLine}{command.CommandText}. {Environment.NewLine}DbCommandParameters:{Environment.NewLine}{command.Parameters.GetParametersLog()}.";
                ExceptionObject = ex;
                Logger.Error(this, $"{Message}{Environment.NewLine}{ex}", logTitle);
                return Result.Create<object>(false, data: null, message: Message);
            }
        }

        /// <summary>執行目標 SQL 敘述，並且查詢單一純量值
        /// </summary>
        /// <param name="command">目標SQL敘述命令</param>
        /// <param name="handler">執行 ADO.NET 資料庫單一純量查詢指令的委派</param>
        /// <param name="obj">查詢結果(單一純量值)</param>
        /// <returns>執行目標SQL命令是否成功</returns>
        protected bool DoExecuteScalar(DbCommand command, ExecuteScalarHandler handler, out object obj)
        {
            var logTitle = Logger.GetTraceLogTitle(this, nameof(DoExecuteScalar));
            obj = null;

            try
            {
                obj = handler();
            }
            catch (Exception ex)
            {
                Message = $"Execute SQL DbCommand occur exception. SQL statement:{Environment.NewLine}{command.CommandText}. {Environment.NewLine}DbCommandParameters:{Environment.NewLine}{command.Parameters.GetParametersLog()}.";
                ExceptionObject = ex;
                Logger.Error(this, $"{Message}{Environment.NewLine}{ex}", logTitle);
                return false;
            }

            return true;
        }

        /// <summary>執行 SQL 查詢指令，回傳執行使否成功
        /// </summary>
        /// <param name="command">要執行的指令</param>
        /// <param name="ds">要載入查詢資料的 DataSet 資料集合</param>
        /// <param name="tableNames">資料表名稱</param>
        /// <returns>是否執行查詢成功</returns>
        protected async Task<bool> DoLoadDataSetAsync(DbCommand command, DataSet ds, params string[] tableNames)
        {
            var logTitle = Logger.GetTraceLogTitle(this, nameof(DoLoadDataSetAsync));

            if (tableNames.HasNullOrEmptyElemants())
            {
                Message = $"Execute SQL occur due to the 'tableNames' can not contain null or empty string element.";
                Logger.Error(this, Message, logTitle);
                return false;
            }

            try
            {
                using (var adapter = _agent.GetDataAdapter(command))
                {
                    var rootName = "Table";

                    for (var i = 0; i < tableNames.Length; i++)
                    {
                        var tableName = i == 0 ? rootName : rootName + i;
                        adapter.TableMappings.Add(tableName, tableNames[i]);
                    }

                    try
                    {
                        adapter.Fill(ds);
                    }
                    catch (Exception ex)
                    {
                        Message = $"Execute DataAdapter fill data to DataSet async task occur exception. SQL statement: {Environment.NewLine}{command.CommandText}. {Environment.NewLine}DbCommandParameters:{Environment.NewLine}{command.Parameters.GetParametersLog()}.";
                        Logger.Error(this, $"{Message}{Environment.NewLine}{ex}", logTitle);
                    }
                }

                command.Dispose();
            }
            catch (Exception ex)
            {
                Message = $"Execute SQL DbCommand occur exception. SQL statement: {Environment.NewLine}{command.CommandText}. {Environment.NewLine}DbCommandParameters:{Environment.NewLine}{command.Parameters.GetParametersLog()}.";
                ExceptionObject = ex;
                Logger.Error(this, $"{Message}{Environment.NewLine}{ex}", logTitle);
                return false;
            }

            return await Task.FromResult(true);
        }

        /// <summary>執行 SQL 查詢指令，回傳執行使否成功
        /// </summary>
        /// <param name="command">要執行的指令</param>
        /// <param name="ds">要載入查詢資料的 DataSet 資料集合</param>
        /// <param name="tableNames">資料表名稱</param>
        /// <returns>是否執行查詢成功</returns>
        protected bool DoLoadDataSet(DbCommand command, DataSet ds, params string[] tableNames)
        {
            var logTitle = Logger.GetTraceLogTitle(this, nameof(DoLoadDataSet));

            if (tableNames.HasNullOrEmptyElemants())
            {
                Message = $"Execute SQL occur due to the 'tableNames' can not contain null or empty string element.";
                Logger.Error(this, Message, logTitle);
                return false;
            }

            try
            {
                using (var adapter = _agent.GetDataAdapter(command))
                {
                    var rootName = "Table";

                    for (var i = 0; i < tableNames.Length; i++)
                    {
                        var tableName = i == 0 ? rootName : rootName + i;
                        adapter.TableMappings.Add(tableName, tableNames[i]);
                    }

                    adapter.Fill(ds);
                }

                command.Dispose();
            }
            catch (Exception ex)
            {
                Message = $"Execute SQL DbCommand occur exception. SQL statement: {Environment.NewLine}{command.CommandText}. {Environment.NewLine}DbCommandParameters:{Environment.NewLine}{command.Parameters.GetParametersLog()}.";
                ExceptionObject = ex;
                Logger.Error(this, $"{Message}{Environment.NewLine}{ex}", logTitle);
                return false;
            }

            return true;
        }

        #endregion 宣告保護的執行資料庫 SQL 指令的樣板方法


        #region 宣告公開的 ORM 機制查詢方法

        /// <summary>執行 SQL 查詢指令，輸出目標型別的資料模型串列，回傳執行使否成功
        /// </summary>
        /// <typeparam name="TModel">資料模型泛型</typeparam>
        /// <param name="command">要執行的指令</param>
        /// <param name="handler">執行載入DataSet查詢的處理委派</param>
        /// <returns>是否執行查詢成功</returns>
        public async Task<Result<IEnumerable<TModel>>> DoLoadDataToModelAsync<TModel>(DbCommand command, ExecuteDataSetAsyncHandler handler)
            where TModel : new()
        {
            var logTitle = Logger.GetTraceLogTitle(this, nameof(DoLoadDataToModelAsync));
            var result = Result.Create<IEnumerable<TModel>>();

            #region 執行 SQL 資料查詢

            var ds = new DataSet();

            if (!await handler(ds))
            {
                result.Message = Message;
                result.Data = Enumerable.Empty<TModel>();
                return result;
            }

            #endregion 執行 SQL 資料查詢

            #region 執行 ORM 資料繫結

            var models = Enumerable.Empty<TModel>();

            try
            {
                models = OrmDataBinder.LoadDataToModel<TModel>(ds);
            }
            catch (Exception ex)
            {
                result.Message = $"ORM data binding occur exception. {ex}";
                Logger.Error(this, result.Message, logTitle);
                return result;
            }

            #endregion 執行 ORM 資料繫結

            result.Data = models;
            result.Success = true;
            return result;
        }

        /// <summary>執行 SQL 查詢指令，輸出目標型別的資料模型串列，回傳執行使否成功
        /// </summary>
        /// <typeparam name="TModel">資料模型泛型</typeparam>
        /// <param name="command">要執行的指令</param>
        /// <param name="handler">執行載入DataSet查詢的處理委派</param>
        /// <param name="models">轉換後的Model/Entity串列</param>
        /// <returns>是否執行查詢成功</returns>
        public bool DoLoadDataToModel<TModel>(DbCommand command, ExecuteDataSetHandler handler, out IEnumerable<TModel> models)
            where TModel : new()
        {
            var logTitle = Logger.GetTraceLogTitle(this, nameof(DoLoadDataToModel));
            models = Enumerable.Empty<TModel>();

            #region 執行 SQL 資料查詢

            var ds = new DataSet();

            if (!handler(ds))
            {
                return false;
            }

            #endregion 執行 SQL 資料查詢

            #region 執行 ORM 資料繫結

            try
            {
                models = OrmDataBinder.LoadDataToModel<TModel>(ds);
            }
            catch (Exception ex)
            {
                Message = $"ORM data binding occur exception. {ex}";
                Logger.Error(this, Message, logTitle);
                return false;
            }

            #endregion 執行 ORM 資料繫結

            return true;
        }

        /// <summary>執行 SQL 查詢指令，進行動態的Dynamic ORM資料繫結機制，輸出動態資料模型(Dynamic Model)串列 (ORM機制: 單一資料模型ORM對應，應用在SQL查詢語法中只有一個SELECT語法，回傳單一Table)
        /// </summary>
        /// <remarks>ORM機制: 單一資料模型ORM對應，應用在SQL查詢語法中只有一個SELECT語法，回傳單一Table
        /// </remarks>
        /// <param name="command">要執行的指令</param>
        /// <param name="handler">執行IDataReader查詢的處理委派</param>
        /// <returns>是否執行查詢成功</returns>
        public async Task<Result<IEnumerable<dynamic>>> DoLoadDataToDynamicModelAsync(DbCommand command, ExecReaderAsyncHandler handler)
        {
            var logTitle = Logger.GetTraceLogTitle(this, nameof(DoLoadDataToDynamicModelAsync));
            var result = Result.Create<IEnumerable<dynamic>>();

            #region 執行 SQL 資料查詢

            var r = await handler();

            if (!r.Success || r.Data.IsNull())
            {
                result.Data = Enumerable.Empty<dynamic>();
                return result;
            }

            var reader = r.Data;

            #endregion 執行 SQL 資料查詢

            #region 進行 Dynamic ORM 動態資料繫結

            var models = Enumerable.Empty<dynamic>();

            try
            {
                models = OrmBinder.LoadDataToDynamicModel(reader);
            }
            catch (Exception ex)
            {
                Message = $"Dynamic ORM data binding occur error. {ex}";
                result.Message = Message;
                Logger.Error(this, Message, logTitle);
                return result;
            }

            #endregion 進行 Dynamic ORM 動態資料繫結

            result.Data = models;
            result.Success = true;
            return result;
        }

        /// <summary>執行 SQL 查詢指令，進行動態的Dynamic ORM資料繫結機制，輸出動態資料模型(Dynamic Model)串列 (ORM機制: 單一資料模型ORM對應，應用在SQL查詢語法中只有一個SELECT語法，回傳單一Table)
        /// </summary>
        /// <remarks>ORM機制: 單一資料模型ORM對應，應用在SQL查詢語法中只有一個SELECT語法，回傳單一Table
        /// </remarks>
        /// <param name="command">要執行的指令</param>
        /// <param name="handler">執行IDataReader查詢的處理委派</param>
        /// <param name="models">動態資料模型串列集合</param>
        /// <returns>是否執行查詢成功</returns>
        public bool DoLoadDataToDynamicModel(DbCommand command, ExecReaderHandler handler, out IEnumerable<dynamic> models)
        {
            var logTitle = Logger.GetTraceLogTitle(this, nameof(DoLoadDataToDynamicModel));
            models = Enumerable.Empty<dynamic>();

            #region 執行 SQL 資料查詢

            var success = handler(out var reader);

            if (!success)
            {
                return false;
            }

            #endregion 執行 SQL 資料查詢

            #region 進行 Dynamic ORM 動態資料繫結

            try
            {
                models = OrmBinder.LoadDataToDynamicModel(reader);
            }
            catch (Exception ex)
            {
                Message = $"Dynamic ORM data binding occur error. {ex}";
                Logger.Error(this, Message, logTitle);
                return false;
            }

            #endregion 進行Dynamic ORM動態資料繫結

            return true;
        }

        /// <summary>執行 SQL 查詢指令，輸出資料模型的 List 資料集合，回傳執行使否成功 (自動開啟 ZayniFramework 內部的資料庫連線)
        /// </summary>
        /// <param name="command">要執行的指令</param>
        /// <param name="modelTypes">資料模型型別陣列</param>
        /// <param name="handler">執行載入 DataSet 查詢的處理委派</param>
        /// <returns>是否執行查詢成功</returns>
        public async Task<Result<IEnumerable<object[]>>> DoLoadDataToModelsAsync(DbCommand command, Type[] modelTypes, ExecuteDataSetAsyncHandler handler)
        {
            var logTitle = Logger.GetTraceLogTitle(this, nameof(DoLoadDataToModelsAsync));
            var result = Result.Create<IEnumerable<object[]>>();

            #region 執行 SQL 資料查詢

            var data = new DataSet();

            if (!await handler(data))
            {
                result.Data = Enumerable.Empty<object[]>();
                return result;
            }

            #endregion 執行 SQL 資料查詢

            #region 執行 ORM 資料繫結

            var models = Enumerable.Empty<object[]>();

            try
            {
                models = OrmDataBinder.LoadDataToModels(data, modelTypes);
            }
            catch (Exception ex)
            {
                var message = $"ORM data binding occur error. {ex}";
                result.Message = message;
                Logger.Error(this, result.Message, logTitle);
                return result;
            }

            #endregion 執行 ORM 資料繫結

            result.Data = models;
            result.Success = true;
            return result;
        }

        /// <summary>執行 SQL 查詢指令，輸出資料模型的 List 資料集合，回傳執行使否成功 (自動開啟 ZayniFramework 內部的資料庫連線)
        /// </summary>
        /// <param name="command">要執行的指令</param>
        /// <param name="modelTypes">資料模型型別陣列</param>
        /// <param name="handler">執行載入 DataSet 查詢的處理委派</param>
        /// <param name="result">資料模型的List</param>
        /// <returns>是否執行查詢成功</returns>
        public bool DoLoadDataToModels(DbCommand command, Type[] modelTypes, ExecuteDataSetHandler handler, out IEnumerable<object[]> result)
        {
            var logTitle = Logger.GetTraceLogTitle(this, nameof(DoLoadDataToModels));
            result = Enumerable.Empty<object[]>();

            #region 執行 SQL 資料查詢

            var ds = new DataSet();

            if (!handler(ds))
            {
                return false;
            }

            #endregion 執行 SQL 資料查詢

            #region 執行 ORM 資料繫結

            try
            {
                result = OrmDataBinder.LoadDataToModels(ds, modelTypes);
            }
            catch (Exception ex)
            {
                var message = $"ORM data binding occur error. {ex}";
                Logger.Error(this, message, logTitle);
                return false;
            }

            #endregion 執行 ORM 資料繫結

            return true;
        }

        /// <summary>執行 SQL 查詢指令，進行動態的Dynamic ORM資料繫結機制，輸出動態資料模型 (Dynamic Model) 字典集合<para/>
        /// (ORM 機制: 多種資料模型 ORM 對應，應用在 SQL 查詢語法中有多個 SELECT 語法，回傳多個 Table 查詢結果的資料)
        /// </summary>
        /// <remarks>ORM 機制: 多種資料模型ORM對應，應用在SQL查詢語法中有多個 SELECT 語法，回傳多個 Table 查詢結果的資料</remarks>
        /// <param name="command">要執行的指令</param>
        /// <param name="handler">執行載入 DataSet 查詢的處理委派</param>
        /// <param name="tableName">資料表名稱</param>
        /// <returns>是否執行查詢成功</returns>
        public async Task<Result<Dictionary<string, IEnumerable<dynamic>>>> DoLoadDataToDynamicCollectionAsync(DbCommand command, ExecuteDataSetAsyncHandler handler, params string[] tableName)
        {
            var logTitle = Logger.GetTraceLogTitle(this, nameof(DoLoadDataToDynamicCollectionAsync));
            var result = Result.Create<Dictionary<string, IEnumerable<dynamic>>>();

            #region 執行 SQL 資料查詢

            var data = new DataSet();

            if (!await handler(data))
            {
                return result;
            }

            #endregion 執行 SQL 資料查詢

            #region 進行 Dynamic ORM 動態資料繫結

            if (!OrmBinder.LoadDataToDynamicModelCollection(data, out var collections, out var message))
            {
                var errorMessage = $"Dynamic ORM data binding occur error. {message}";
                Logger.Error(this, errorMessage, logTitle);
                return result;
            }

            #endregion 進行 Dynamic ORM 動態資料繫結

            result.Data = collections;
            result.Success = true;
            return result;
        }

        /// <summary>執行 SQL 查詢指令，進行動態的Dynamic ORM資料繫結機制，輸出動態資料模型 (Dynamic Model) 字典集合<para/>
        /// (ORM 機制: 多種資料模型 ORM 對應，應用在 SQL 查詢語法中有多個 SELECT 語法，回傳多個 Table 查詢結果的資料)
        /// </summary>
        /// <remarks>ORM 機制: 多種資料模型ORM對應，應用在SQL查詢語法中有多個 SELECT 語法，回傳多個 Table 查詢結果的資料</remarks>
        /// <param name="command">要執行的指令</param>
        /// <param name="handler">執行載入 DataSet 查詢的處理委派</param>
        /// <param name="modelCollection">動態資料模型(Dynamic Model)字典集合</param>
        /// <param name="tableName">資料表名稱</param>
        /// <returns>是否執行查詢成功</returns>
        public bool DoLoadDataToDynamicCollection(DbCommand command, ExecuteDataSetHandler handler, out Dictionary<string, IEnumerable<dynamic>> modelCollection, params string[] tableName)
        {
            var logTitle = Logger.GetTraceLogTitle(this, nameof(DoLoadDataToDynamicCollection));
            modelCollection = [];

            #region 執行 SQL 資料查詢

            var data = new DataSet();

            var isLoadOk = handler(data);

            if (!isLoadOk)
            {
                return false;
            }

            #endregion 執行 SQL 資料查詢

            #region 進行 Dynamic ORM 動態資料繫結

            var isSuccess = OrmBinder.LoadDataToDynamicModelCollection(data, out modelCollection, out var message);

            if (!isSuccess)
            {
                var errorMessage = $"Dynamic ORM data binding occur error. {message}";
                Logger.Error(this, errorMessage, logTitle);
                return false;
            }

            #endregion 進行 Dynamic ORM 動態資料繫結

            return true;
        }

        /// <summary>執行 SQL 查詢指令，將查詢結果的資料模型都自動載入到指定的 ModelSet 資料模型集合中
        /// </summary>
        /// <param name="command">要執行的指令</param>
        /// <param name="modelTypes">資料模型型別陣列</param>
        /// <param name="modelSet">查詢結果資料集合</param>
        /// <param name="handler">執行載入 DataSet 查詢的處理委派</param>
        /// <returns>是否執行查詢成功</returns>
        public async Task<bool> DoLoadModelSetAsync(DbCommand command, ModelSet modelSet, ExecuteModelsAsyncHandler handler, params Type[] modelTypes)
        {
            var logTitle = Logger.GetTraceLogTitle(this, nameof(DoLoadModelSetAsync));

            #region 檢查輸入參數

            if (modelSet.IsNull())
            {
                var errorMessage = $"The ModelSet object is null. Can not execute LoadModelSet.";
                modelSet.Success = false;
                Logger.Error(this, errorMessage, logTitle);
                return false;
            }

            #endregion 檢查輸入參數

            #region 將查詢資料繫結到串列集合

            var r = await handler();

            if (!r.Success || r.Data.IsNull())
            {
                modelSet.Success = false;
                Logger.Error(this, Message, logTitle);
                return false;
            }

            var data = r.Data;

            #endregion 將查詢資料繫結到串列集合

            #region 將串列資料集合繫結到 ModelSet 中

            if (!OrmBinder.LoadDataToModelSet(modelTypes, data, modelSet, out var message))
            {
                var errorMessage = $"ModelSet data binding occur error. {message}";
                modelSet.Success = false;
                Logger.Error(this, errorMessage, logTitle);
                return false;
            }

            #endregion 將串列資料集合繫結到 ModelSet 中

            return true;
        }

        /// <summary>執行 SQL 查詢指令，將查詢結果的資料模型都自動載入到指定的 ModelSet 資料模型集合中
        /// </summary>
        /// <param name="command">要執行的指令</param>
        /// <param name="modelTypes">資料模型型別陣列</param>
        /// <param name="modelSet">查詢結果資料集合</param>
        /// <param name="handler">執行載入 DataSet 查詢的處理委派</param>
        /// <returns>是否執行查詢成功</returns>
        public bool DoLoadModelSet(DbCommand command, ModelSet modelSet, ExecuteModelsHandler handler, params Type[] modelTypes)
        {
            var logTitle = Logger.GetTraceLogTitle(this, nameof(DoLoadModelSet));

            #region 檢查輸入參數


            if (modelSet.IsNull())
            {
                var errorMessage = $"The ModelSet object is null. Can not execute LoadModelSet.";
                modelSet.Success = false;
                Logger.Error(this, errorMessage, logTitle);
                return false;
            }

            #endregion 檢查輸入參數

            #region 將查詢資料繫結到串列集合

            var isSuccess = handler(out var datas);

            if (!isSuccess)
            {
                modelSet.Success = false;
                Logger.Error(this, Message, logTitle);
                return false;
            }

            #endregion 將查詢資料繫結到串列集合

            #region 將串列資料集合繫結到 ModelSet 中

            isSuccess = OrmBinder.LoadDataToModelSet(modelTypes, datas, modelSet, out var message);

            if (!isSuccess)
            {
                var errorMessage = $"ModelSet data binding occur error. {message}";
                modelSet.Success = false;
                Logger.Error(this, errorMessage, logTitle);
                return false;
            }

            #endregion 將串列資料集合繫結到 ModelSet 中

            return true;
        }

        #endregion 宣告公開的 ORM 機制查詢方法


        #region 宣告公開的資料庫指令 DbCommand 的處理方法

        /// <summary>取得 SQL 字串的資料庫指令 DbCommand 物件
        /// </summary>
        /// <param name="commandTextOrSpName">SQL命令內容 或 資料庫預存程序名稱</param>
        /// <param name="commandType">資料庫指令型態</param>
        /// <returns>資料庫指令DbCommand物件</returns>
        public DbCommand GetDbCommand(string commandTextOrSpName, CommandType commandType = CommandType.Text)
        {
            var logTitle = Logger.GetTraceLogTitle(this, nameof(GetDbCommand));
            var result = default(DbCommand);

            if (commandTextOrSpName.IsNullOrEmpty())
            {
                var message = $"Invalid argument '{nameof(commandTextOrSpName)}'. Can not be null or empty string.";
                Logger.Error(this, message, logTitle);
                return null;
            }

            try
            {
                result = _agent.GetDbCommand(commandTextOrSpName, commandType);
            }
            catch (Exception ex)
            {
                var message = $"Get DbCommand occur exception. CommandType: {commandType}, SQL statement: {commandTextOrSpName}";
                ExceptionObject = ex;
                Logger.Error(this, $"{message}{Environment.NewLine}{ex}", logTitle);
                return null;
            }

            return result;
        }

        /// <summary>清除資料庫指令的所有參數
        /// </summary>
        /// <param name="command">目標資料庫命令</param>
        /// <returns>是否清除成功</returns>
        public bool ClearAllParameters(DbCommand command)
        {
            var logTitle = Logger.GetTraceLogTitle(this, nameof(ClearAllParameters));

            try
            {
                command.Parameters.Clear();
            }
            catch (Exception ex)
            {
                var message = $"Clear all DbParameters in DbCommand occur exception. SQL statement: {command.CommandText}";
                ExceptionObject = ex;
                Logger.Error(this, $"{message}{Environment.NewLine}{ex}", logTitle);
                return false;
            }

            return true;
        }

        #endregion 宣告公開的資料庫指令 DbCommand 的處理方法
    }
}
