﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.DataAccess
{
    /// <summary>Oracle 資料庫存取基底
    /// </summary>
    public class BaseOracleDao : BaseDao, IDataAccess
    {
        #region Private Fields

        /// <summary>Oracle 資料庫的 SQL 日誌記錄員
        /// </summary>
        private OracleLogger _logger;

        /// <summary>ZayniFramework 框架資料庫連線字串
        /// </summary>
        private readonly string _zayniConnectionString;

        #endregion Private Fields


        #region Constructors

        /// <summary>多載建構子
        /// </summary>
        /// <param name="dbName">資料庫連線字串名稱</param>
        public BaseOracleDao(string dbName) : base(DbProviderOption.Oracle)
        {
            _logger = new OracleLogger(dbName);
            base.DbName = dbName;
            base.OrmBinder = new OrmDataBinder();
            base.LoadConnectionString();
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="dbName">資料庫連線字串名稱</param>
        /// <param name="connectionString">資料庫連線字串</param>
        /// <param name="zayniConnectionString">ZayniFramework 框架的資料庫連線字串 (若傳入為Null或空字串，SQL Log功能將停用)</param>
        public BaseOracleDao(string dbName, string connectionString, string zayniConnectionString = null) : base(DbProviderOption.Oracle)
        {
            _zayniConnectionString = zayniConnectionString;
            _logger = new OracleLogger(dbName, zayniConnectionString);
            base.ConnectionString = connectionString;
            base.OrmBinder = new OrmDataBinder();
        }

        /// <summary>解構子
        /// </summary>
        ~BaseOracleDao()
        {
            _logger = null;
            base.DbName = null;
            base.OrmBinder = null;
            base.ConnectionString = null;
        }

        #endregion Constructors


        #region Public Methods

        /// <summary>取得 Oracle 資料庫的空字串值
        /// </summary>
        /// <returns>Oracle 資料庫的空字串值</returns>
        public override string GetDBEmptyString() => " ";

        #endregion Public Methods


        #region Private Execute Methods

        /// <summary>執行目標 SQL 敘述命令
        /// </summary>
        /// <param name="command">目標 SQL 敘述命令</param>
        /// <returns>執行目標 SQL 命令是否成功</returns>
        private async Task<bool> DoExecuteNonQueryAsync(OracleCommand command)
        {
            _logger.IsNull(() => _logger = _zayniConnectionString.IsNullOrEmpty() ? new OracleLogger(base.DbName) : new OracleLogger(base.DbName, _zayniConnectionString));

            if (DbCommands.Add(command, out var commandId))
            {
                _logger?.WriteDbCommandRequestLog(commandId, command, GetType().Name);
            }

            command.CommandText = command.CommandText.Replace(";", string.Empty);
            var success = await base.DoExecuteNonQueryAsync(command, command.ExecuteNonQueryAsync);

            if (commandId.IsNotNullOrEmpty())
            {
                _logger?.WriteDbCommandResultLog(commandId, command, DataCount, GetType().Name);
                DbCommands.Remove(commandId);
            }

            return success;
        }

        /// <summary>執行目標 SQL 敘述命令
        /// </summary>
        /// <param name="command">目標 SQL 敘述命令</param>
        /// <returns>執行目標 SQL 命令是否成功</returns>
        private bool DoExecuteNonQuery(OracleCommand command)
        {
            _logger.IsNull(() => _logger = _zayniConnectionString.IsNullOrEmpty() ? new OracleLogger(base.DbName) : new OracleLogger(base.DbName, _zayniConnectionString));

            if (DbCommands.Add(command, out var commandId))
            {
                _logger?.WriteDbCommandRequestLog(commandId, command, GetType().Name);
            }

            command.CommandText = command.CommandText.Replace(";", string.Empty);
            var isSuccess = base.DoExecuteNonQuery(command, command.ExecuteNonQuery);

            if (commandId.IsNotNullOrEmpty())
            {
                _logger?.WriteDbCommandResultLog(commandId, command, DataCount, GetType().Name);
                DbCommands.Remove(commandId);
            }

            return isSuccess;
        }

        /// <summary>執行目標 SQL 敘述，並且取得資料讀取器
        /// </summary>
        /// <param name="command">目標 SQL 敘述命令</param>
        /// <returns>執行目標 SQL 命令是否成功</returns>
        private async Task<Result<IDataReader>> DoExecuteReaderAsync(OracleCommand command)
        {
            command.CommandText = command.CommandText.Replace(";", string.Empty);
            var r = await base.DoExecuteReaderAsync(command, async () => (IDataReader)await command.ExecuteReaderAsync());

            if (r.Success)
            {
                _logger.IsNull(() => _logger = _zayniConnectionString.IsNullOrEmpty() ? new OracleLogger(base.DbName) : new OracleLogger(base.DbName, _zayniConnectionString));
                _logger?.WriteLog(command, GetType().Name);
            }

            var reader = r.Data;
            return Result.Create<IDataReader>(true, data: reader);
        }

        /// <summary>執行目標 SQL 敘述，並且取得資料讀取器
        /// </summary>
        /// <param name="command">目標 SQL 敘述命令</param>
        /// <param name="reader">資料讀取器</param>
        /// <returns>執行目標 SQL 命令是否成功</returns>
        private bool DoExecuteReader(OracleCommand command, out IDataReader reader)
        {
            command.CommandText = command.CommandText.Replace(";", string.Empty);
            var isSuccess = base.DoExecuteReader(command, command.ExecuteReader, out reader);

            if (isSuccess)
            {
                _logger.IsNull(() => _logger = _zayniConnectionString.IsNullOrEmpty() ? new OracleLogger(base.DbName) : new OracleLogger(base.DbName, _zayniConnectionString));
                _logger?.WriteLog(command, GetType().Name);
            }

            return isSuccess;
        }

        /// <summary>執行目標 SQL 敘述，並且查詢單一純量值
        /// </summary>
        /// <param name="command">目標 SQL 敘述命令</param>
        /// <returns>執行目標 SQL 命令是否成功</returns>
        private async Task<Result<object>> DoExecuteScalarAsync(OracleCommand command)
        {
            _logger.IsNull(() => _logger = _zayniConnectionString.IsNullOrEmpty() ? new OracleLogger(base.DbName) : new OracleLogger(base.DbName, _zayniConnectionString));

            if (DbCommands.Add(command, out var commandId))
            {
                _logger?.WriteDbCommandRequestLog(commandId, command, GetType().Name);
            }

            command.CommandText = command.CommandText.Replace(";", string.Empty);
            var result = await base.DoExecuteScalarAsync(command, command.ExecuteScalarAsync);

            if (commandId.IsNotNullOrEmpty())
            {
                _logger?.WriteDbCommandResultLog(commandId, command, result.Data, GetType().Name);
                DbCommands.Remove(commandId);
            }

            return result;
        }

        /// <summary>執行目標 SQL 敘述，並且查詢單一純量值
        /// </summary>
        /// <param name="command">目標 SQL 敘述命令</param>
        /// <param name="obj">查詢結果 (單一純量值)</param>
        /// <returns>執行目標 SQL 命令是否成功</returns>
        private bool DoExecuteScalar(OracleCommand command, out object obj)
        {
            _logger.IsNull(() => _logger = _zayniConnectionString.IsNullOrEmpty() ? new OracleLogger(base.DbName) : new OracleLogger(base.DbName, _zayniConnectionString));

            if (DbCommands.Add(command, out var commandId))
            {
                _logger?.WriteDbCommandRequestLog(commandId, command, GetType().Name);
            }

            command.CommandText = command.CommandText.Replace(";", string.Empty);
            var isSuccess = base.DoExecuteScalar(command, command.ExecuteScalar, out obj);

            if (commandId.IsNotNullOrEmpty())
            {
                _logger?.WriteDbCommandResultLog(commandId, command, obj, GetType().Name);
                DbCommands.Remove(commandId);
            }

            return isSuccess;
        }

        /// <summary>執行 SQL 查詢指令，回傳執行使否成功
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="ds">資料集合</param>
        /// <param name="trimSemicolon">是否將 SQL 指令中的半形分號去除掉，育社會置換成空字串。</param>
        /// <param name="tableNames">資料表名稱</param>
        /// <returns>是否執行查詢成功</returns>
        private async Task<bool> DoLoadDataSetAsync(OracleCommand command, DataSet ds, bool trimSemicolon = true, params string[] tableNames)
        {
            _logger.IsNull(() => _logger = _zayniConnectionString.IsNullOrEmpty() ? new OracleLogger(base.DbName) : new OracleLogger(base.DbName, _zayniConnectionString));

            if (DbCommands.Add(command, out var commandId))
            {
                _logger?.WriteDbCommandRequestLog(commandId, command, GetType().Name);
            }

            When.True(trimSemicolon, () => command.CommandText = command.CommandText.Replace(";", string.Empty));
            var success = await base.DoLoadDataSetAsync(command, ds, tableNames);

            if (commandId.IsNotNullOrEmpty())
            {
                _logger?.WriteDbCommandResultLog(commandId, command, ds, GetType().Name);
                DbCommands.Remove(commandId);
            }

            return success;
        }

        /// <summary>執行 SQL 查詢指令，回傳執行使否成功
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="ds">資料集合</param>
        /// <param name="trimSemicolon">是否將 SQL 指令中的半形分號去除掉，育社會置換成空字串。</param>
        /// <param name="tableNames">資料表名稱</param>
        /// <returns>是否執行查詢成功</returns>
        private bool DoLoadDataSet(OracleCommand command, DataSet ds, bool trimSemicolon = true, params string[] tableNames)
        {
            _logger.IsNull(() => _logger = _zayniConnectionString.IsNullOrEmpty() ? new OracleLogger(base.DbName) : new OracleLogger(base.DbName, _zayniConnectionString));

            if (DbCommands.Add(command, out var commandId))
            {
                _logger?.WriteDbCommandRequestLog(commandId, command, GetType().Name);
            }

            When.True(trimSemicolon, () => command.CommandText = command.CommandText.Replace(";", string.Empty));
            var isSuccess = base.DoLoadDataSet(command, ds, tableNames);

            if (commandId.IsNotNullOrEmpty())
            {
                _logger?.WriteDbCommandResultLog(commandId, command, ds, GetType().Name);
                DbCommands.Remove(commandId);
            }

            return isSuccess;
        }

        /// <summary>執行 SQL 查詢指令，回傳執行使否成功
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="ds">資料集合</param>
        /// <param name="tableNames">資料表名稱</param>
        /// <returns>是否執行查詢成功</returns>
        private async Task<bool> ExecuteLoadDataSetAsync(OracleCommand command, DataSet ds, params string[] tableNames)
        {
            var logTitle = Logger.GetTraceLogTitle(this, nameof(ExecuteLoadDataSetAsync));

            if (1 == tableNames.Length)
            {
                return await DoLoadDataSetAsync(command, ds, true, tableNames);
            }

            var sbDeclare = new StringBuilder();
            var sbSelects = new StringBuilder();
            var sbOutParas = new StringBuilder();

            try
            {
                var selectStrings = command.CommandText.FetchSubstring("SELECT", ";", true, true).ToArray();

                for (int i = 0, len = selectStrings.Length; i < len; i++)
                {
                    var strCursorVar = $"cur{i}";
                    var sbCursorVar = new StringBuilder($"{strCursorVar} SYS_REFCURSOR; ");
                    sbDeclare.Append(sbCursorVar);

                    var sbCursorSelect = new StringBuilder($" OPEN {strCursorVar} FOR {selectStrings[i]} ");
                    sbSelects.Append(sbCursorSelect);

                    var sbParameter = new StringBuilder($" :paraCur{i} := {strCursorVar}; ");
                    sbOutParas.Append(sbParameter);
                }

                var sbDeclareCur = new StringBuilder(" DECLARE ").Append(sbDeclare);

                var sql = $" {sbSelects} {sbOutParas} ";
                var sbSql = new StringBuilder($" BEGIN {sql} END; ");

                command.CommandText = (sbDeclareCur.Append(sbSql).ToString());

                for (int j = 0, len = selectStrings.Length; j < len; j++)
                {
                    AddOutParameter(command, $"paraCur{j}", OracleDbType.RefCursor, int.MaxValue);
                }
            }
            catch (Exception ex)
            {
                base.Message = $"Process Oracle Multi-select SQL statement occur exception. CommandText:{Environment.NewLine}{command.CommandText}";
                Logger.Error(this, $"{base.Message}{Environment.NewLine}{ex}", logTitle);
                return false;
            }

            return await DoLoadDataSetAsync(command, ds, false, tableNames);
        }

        /// <summary>執行 SQL 查詢指令，回傳執行使否成功
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="ds">資料集合</param>
        /// <param name="tableNames">資料表名稱</param>
        /// <returns>是否執行查詢成功</returns>
        private bool ExecuteLoadDataSet(OracleCommand command, DataSet ds, params string[] tableNames)
        {
            var logTitle = Logger.GetTraceLogTitle(this, nameof(ExecuteLoadDataSet));

            if (1 == tableNames.Length)
            {
                return DoLoadDataSet(command, ds, true, tableNames);
            }

            var sbDeclare = new StringBuilder();
            var sbSelects = new StringBuilder();
            var sbOutParas = new StringBuilder();

            try
            {
                var selectStrings = command.CommandText.FetchSubstring("SELECT", ";", true, true).ToArray();

                for (int i = 0, len = selectStrings.Length; i < len; i++)
                {
                    var strCursorVar = $"cur{i}";
                    var sbCursorVar = new StringBuilder($"{strCursorVar} SYS_REFCURSOR; ");
                    sbDeclare.Append(sbCursorVar);

                    var sbCursorSelect = new StringBuilder($" OPEN {strCursorVar} FOR {selectStrings[i]} ");
                    sbSelects.Append(sbCursorSelect);

                    var sbParameter = new StringBuilder($" :paraCur{i} := {strCursorVar}; ");
                    sbOutParas.Append(sbParameter);
                }

                var sbDeclareCur = new StringBuilder(" DECLARE ").Append(sbDeclare);

                var sql = $" {sbSelects} {sbOutParas} ";
                var sbSql = new StringBuilder($" BEGIN {sql} END; ");

                command.CommandText = (sbDeclareCur.Append(sbSql).ToString());

                for (int j = 0, len = selectStrings.Length; j < len; j++)
                {
                    AddOutParameter(command, $"paraCur{j}", OracleDbType.RefCursor, int.MaxValue);
                }
            }
            catch (Exception ex)
            {
                base.Message = $"Process Oracle Multi-select SQL statement occur exception. CommandText:{Environment.NewLine}{command.CommandText}";
                Logger.Error(this, $"{base.Message}{Environment.NewLine}{ex}", logTitle);
                return false;
            }

            return DoLoadDataSet(command, ds, false, tableNames);
        }

        #endregion Private Execute Methods


        #region ExecuteNonQuery Methods

        /// <summary>執行 DbCommand 資料庫指令。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <returns>是否執行成功</returns>
        public async Task<bool> ExecuteNonQueryAsync(DbCommand command)
        {
            var cmd = command as OracleCommand;

            if (cmd.IsNull())
            {
                Message = $"Invalid argument. The DbCommand is not a OracleCommand.";
                return false;
            }

            return await DoExecuteNonQueryAsync(cmd);
        }

        /// <summary>執行 DbCommand 資料庫指令。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <returns>是否執行成功</returns>
        public bool ExecuteNonQuery(DbCommand command)
        {
            var cmd = command as OracleCommand;

            if (cmd.IsNull())
            {
                Message = $"Invalid argument. The DbCommand is not a OracleCommand.";
                return false;
            }

            return DoExecuteNonQuery(cmd);
        }

        #endregion ExecuteNonQuery Methods


        #region ExecuteReader Methods

        /// <summary>執行資料庫 DbCommand 查詢指令。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <returns>執行查詢 SQL 資料讀取器是否成功</returns>
        public async Task<Result<IDataReader>> ExecuteReaderAsync(DbCommand command)
        {
            var cmd = command as OracleCommand;

            if (cmd.IsNull())
            {
                Message = $"Invalid argument. The DbCommand is not a OracleCommand.";
                return Result.Create<IDataReader>(false, message: Message);
            }

            return await DoExecuteReaderAsync(cmd);
        }

        /// <summary>執行資料庫 DbCommand 查詢指令。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="reader">資料讀取器</param>
        /// <returns>執行查詢 SQL 資料讀取器是否成功</returns>
        public bool ExecuteReader(DbCommand command, out IDataReader reader)
        {
            reader = null;

            var cmd = command as OracleCommand;

            if (cmd.IsNull())
            {
                Message = $"Invalid argument. The DbCommand is not a OracleCommand.";
                return false;
            }

            return DoExecuteReader(cmd, out reader);
        }

        #endregion ExecuteReader Methods


        #region ExecuteScalar Methods

        /// <summary>執行 DbCommand 查詢指令，並且輸出單一純量值。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <returns>執行是否成功</returns>
        public async Task<Result<object>> ExecuteScalarAsync(DbCommand command)
        {
            var cmd = command as OracleCommand;

            if (cmd.IsNull())
            {
                Message = $"Invalid argument. The DbCommand is not a OracleCommand.";
                return Result.Create<object>(false, message: Message);
            }

            return await DoExecuteScalarAsync(cmd);
        }

        /// <summary>執行 DbCommand 查詢指令，並且輸出單一純量值。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="result">查詢單一純量值結果</param>
        /// <returns>執行是否成功</returns>
        public bool ExecuteScalar(DbCommand command, out object result)
        {
            result = null;

            var cmd = command as OracleCommand;

            if (cmd.IsNull())
            {
                Message = $"Invalid argument. The DbCommand is not a OracleCommand.";
                return false;
            }

            return DoExecuteScalar(cmd, out result);
        }

        #endregion ExecuteScalar Methods


        #region LoadDataSet Methods

        /// <summary>執行 DbCommand 查詢指令，回傳執行使否成功。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="ds">資料集合</param>
        /// <param name="tableNames">資料表名稱</param>
        /// <returns>是否執行查詢成功</returns>
        public async Task<bool> LoadDataSetAsync(DbCommand command, DataSet ds, params string[] tableNames)
        {
            var cmd = command as OracleCommand;

            if (cmd.IsNull())
            {
                Message = $"Invalid argument. The DbCommand is not a OracleCommand.";
                return false;
            }

            return await ExecuteLoadDataSetAsync(cmd, ds, tableNames);
        }

        /// <summary>執行 DbCommand 查詢指令，回傳執行使否成功。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="ds">資料集合</param>
        /// <param name="tableNames">資料表名稱</param>
        /// <returns>是否執行查詢成功</returns>
        public bool LoadDataSet(DbCommand command, DataSet ds, params string[] tableNames)
        {
            var cmd = command as OracleCommand;

            if (cmd.IsNull())
            {
                Message = $"Invalid argument. The DbCommand is not a OracleCommand.";
                return false;
            }

            return ExecuteLoadDataSet(cmd, ds, tableNames);
        }

        #endregion LoadDataSet Methods


        #region LoadDataToModel Methods

        /// <summary>執行 DbCommand 查詢指令，輸出查詢結果資料模型集合
        /// </summary>
        /// <typeparam name="TModel">資料模型泛型</typeparam>
        /// <param name="command">資料庫指令</param>
        /// <returns>是否執行查詢成功</returns>
        public async Task<Result<IEnumerable<TModel>>> LoadDataToModelAsync<TModel>(DbCommand command) where TModel : new() =>
            await base.DoLoadDataToModelAsync<TModel>(command, async ds => await LoadDataSetAsync(command, ds, typeof(TModel).Name));

        /// <summary>執行 DbCommand 查詢指令，輸出查詢結果資料模型集合
        /// </summary>
        /// <typeparam name="TModel">資料模型泛型</typeparam>
        /// <param name="command">資料庫指令</param>
        /// <param name="models">資料模型集合</param>
        /// <returns>是否執行查詢成功</returns>
        public bool LoadDataToModel<TModel>(DbCommand command, out IEnumerable<TModel> models) where TModel : new() =>
            base.DoLoadDataToModel<TModel>(command, ds => { return LoadDataSet(command, ds, typeof(TModel).Name); }, out models);

        #endregion LoadDataToModel Methods


        #region LoadDataToDynamicModel Methods

        /// <summary>執行 DbCommand 查詢指令，輸出動態物件資料集合。
        /// * ORM 資料綁定機制: 單一資料模型 ORM 對應，應用在 SQL 查詢語法中只有一個 SELECT 查詢語法，回傳單一資料表結果集合。
        /// * 對查詢單一結果資料表，進行動態 ORM 資料綁定。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <returns>是否執行查詢成功</returns>
        public async Task<Result<IEnumerable<dynamic>>> LoadDataToDynamicModelAsync(DbCommand command) =>
            await base.DoLoadDataToDynamicModelAsync(command, async () => await ExecuteReaderAsync(command));

        /// <summary>執行 DbCommand 查詢指令，輸出動態物件資料集合。
        /// * ORM 資料綁定機制: 單一資料模型 ORM 對應，應用在 SQL 查詢語法中只有一個 SELECT 查詢語法，回傳單一資料表結果集合。
        /// * 對查詢單一結果資料表，進行動態 ORM 資料綁定。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="models">動態資料模型串列集合</param>
        /// <returns>是否執行查詢成功</returns>
        public bool LoadDataToDynamicModel(DbCommand command, out IEnumerable<dynamic> models) =>
            base.DoLoadDataToDynamicModel(command, (out IDataReader reader) => { return ExecuteReader(command, out reader); }, out models);

        #endregion LoadDataToDynamicModel Methods


        #region LoadDataToModels Methods

        /// <summary>執行 DbCommand 查詢指令，依照指定的 Type 陣列回傳多組 ORM 綁定的資料模型集合。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="modelTypes">資料模型型別陣列</param>
        /// <returns>是否執行查詢成功</returns>
        public async Task<Result<IEnumerable<object[]>>> LoadDataToModelsAsync(DbCommand command, Type[] modelTypes)
        {
            var tableNames = new string[modelTypes.Length];

            for (int i = 0, len = modelTypes.Length; i < len; i++)
            {
                tableNames[i] = $"{modelTypes[i].Name}{i}";
            }

            return await base.DoLoadDataToModelsAsync(command, modelTypes, async ds => await LoadDataSetAsync(command, ds, tableNames));
        }


        /// <summary>執行 DbCommand 查詢指令，依照指定的 Type 陣列回傳多組 ORM 綁定的資料模型集合。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="modelTypes">資料模型型別陣列</param>
        /// <param name="result">查詢結果資料模型集合</param>
        /// <returns>是否執行查詢成功</returns>
        public bool LoadDataToModels(DbCommand command, Type[] modelTypes, out IEnumerable<object[]> result)
        {
            var tableNames = new string[modelTypes.Length];

            for (int i = 0, len = modelTypes.Length; i < len; i++)
            {
                tableNames[i] = $"{modelTypes[i].Name}{i}";
            }

            return base.DoLoadDataToModels(command, modelTypes, ds => LoadDataSet(command, ds, tableNames), out result);
        }

        #endregion LoadDataToModels Methods


        #region LoadDataToDynamicCollection Methods

        /// <summary>執行 DbCommand 查詢指令，DbCommand 的 SQL 查詢語法可以支援多組 SELECT 查詢，並且輸出多組動態 ORM 資料綁定的查詢結果集合。
        /// </summary>
        /// <param name="command">資料庫指令
        /// * Key 值: 依照順序傳入的 tableName 參數名稱。
        /// * Value 值: 動態物件資料模型串列。
        /// </param>
        /// <param name="tableName">資料表名稱</param>
        /// <returns>是否執行查詢成功</returns>
        public async Task<Result<Dictionary<string, IEnumerable<dynamic>>>> LoadDataToDynamicCollectionAsync(DbCommand command, params string[] tableName) =>
            await base.DoLoadDataToDynamicCollectionAsync(command, async ds => await LoadDataSetAsync(command, ds, tableName), tableName);

        /// <summary>執行 DbCommand 查詢指令，DbCommand 的 SQL 查詢語法可以支援多組 SELECT 查詢，並且輸出多組動態 ORM 資料綁定的查詢結果集合。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="modelCollection">查詢結果的資料集合，動態物件字典集合。<para/>
        /// * Key 值: 依照順序傳入的 tableName 參數名稱。
        /// * Value 值: 動態物件資料模型串列。
        /// </param>
        /// <param name="tableName">資料表名稱</param>
        /// <returns>是否執行查詢成功</returns>
        public bool LoadDataToDynamicCollection(DbCommand command, out Dictionary<string, IEnumerable<dynamic>> modelCollection, params string[] tableName) =>
            base.DoLoadDataToDynamicCollection(command, ds => { return LoadDataSet(command, ds, tableName); }, out modelCollection, tableName);

        #endregion LoadDataToDynamicCollection Methods


        #region LoadModelSet Methods

        /// <summary>執行 DbCommand 查詢指令，輸出自定義的 ModelSet 資料集合中。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="modelTypes">資料模型型別陣列</param>
        /// <param name="modelSet">查詢結果資料集合</param>
        /// <returns>是否執行查詢成功</returns>
        public async Task<bool> LoadModelSetAsync(DbCommand command, ModelSet modelSet, params Type[] modelTypes) =>
            await base.DoLoadModelSetAsync(command, modelSet, async () => await LoadDataToModelsAsync(command, modelTypes), modelTypes);

        /// <summary>執行 DbCommand 查詢指令，輸出自定義的 ModelSet 資料集合中。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="modelTypes">資料模型型別陣列</param>
        /// <param name="modelSet">查詢結果資料集合</param>
        /// <returns>是否執行查詢成功</returns>
        public bool LoadModelSet(DbCommand command, ModelSet modelSet, params Type[] modelTypes) =>
            base.DoLoadModelSet(command, modelSet, (out IEnumerable<object[]> m) => { return LoadDataToModels(command, modelTypes, out m); }, modelTypes);

        #endregion LoadModelSet Methods


        #region DbCommand Methods

        /// <summary>取得資料庫指令物件
        /// * 透過 GetSqlStringCommand 方法取得的 OracleCommand 的 BindByName 會自動初始化為 true，可以依照參數名稱綁定至 OracleCommand 中。
        /// </summary>
        /// <param name="commandText">SQL 敘述字串</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <returns>Oracle 資料庫指令</returns>
        public DbCommand GetSqlStringCommand(string commandText, DbConnection connection = null, DbTransaction transaction = null)
        {
            var sql = ConvertToOracleSqlString(commandText);
            var command = base.GetDbCommand(sql, CommandType.Text);
            command.Connection = connection;
            command.Transaction = transaction;
            return command;
        }

        /// <summary>取得資料庫預存程序的資料庫指令
        /// * 透過 GetSqlStringCommand 方法取得的 OracleCommand 的 BindByName 會自動初始化為 true，可以依照參數名稱綁定至 OracleCommand 中。
        /// </summary>
        /// <param name="storedProcName">資料庫預存程序名稱</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <returns>Oracle 資料庫指令</returns>
        public DbCommand GetStoredProcCommand(string storedProcName, DbConnection connection = null, DbTransaction transaction = null)
        {
            var command = base.GetDbCommand(storedProcName, CommandType.StoredProcedure);
            command.Connection = connection;
            command.Transaction = transaction;
            return command;
        }

        /// <summary>加入 Oralce 輸入參數到指定的OracleCommand中，參數名稱不可以為Null或空字串
        /// * 透過 GetSqlStringCommand 方法取得的 OracleCommand 的 BindByName 會自動初始化為 true，可以依照參數名稱綁定至 OracleCommand 中。
        /// </summary>
        /// <remarks>參數名稱不可以為Null或空字串</remarks>
        /// <param name="command">要加入的命令</param>
        /// <param name="parameterName">參數名稱 (不可以為Null或空字串)</param>
        /// <param name="dbType">參數型態 (可以使用ZayniFramework 框架的DbColumnType靜態常數)</param>
        /// <param name="value">參數內容</param>
        /// <returns>Oracle 資料庫指令</returns>
        public DbCommand AddInParameter(DbCommand command, string parameterName, int dbType, object value)
        {
            var logTitle = Logger.GetTraceLogTitle(this, nameof(AddInParameter));

            var cmd = command as OracleCommand;

            if (new string[] { command + "", parameterName }.HasNullOrEmptyElemants(true))
            {
                Message = $"Invalid argument. The '{nameof(cmd)}' or '{nameof(parameterName)}' can not be null or empty string.";
                return null;
            }

            try
            {
                if (value.IsNotNull() && bool.TryParse(value + "", out var boolValue))
                {
                    value = boolValue ? 1 : 0;
                }

                var paramName = parameterName.TrimChars('@');
                var oracleDbType = OracleDbTypeHelper.GetOracleDbType(dbType);
                var paramValue = value ?? DBNull.Value;
                var parameter = new OracleParameter(paramName, oracleDbType, value, ParameterDirection.Input);
                command.Parameters.Add(parameter);
            }
            catch (Exception ex)
            {
                Message = $"Add DbCommand parameter occur exception. ParameterName: {parameterName}, DbType: {dbType}, Value: {value}. {ex}";
                ExceptionObject = ex;
                Logger.Error(this, $"{Message}{Environment.NewLine}{ex}", logTitle);
                return null;
            }

            return cmd;
        }

        /// <summary>新增資料庫指令參數
        /// </summary>
        /// <param name="cmd">資料庫指令</param>
        /// <param name="parameterName">資料庫指令的參數名稱</param>
        /// <param name="dbType">參數型別代碼</param>
        /// <param name="value">參數值</param>
        /// <returns>Oracle 資料庫指令</returns>
        public DbCommand AddInParameter(DbCommand cmd, string parameterName, OracleDbType dbType, object value) =>
            AddInParameter(cmd, parameterName, (int)dbType, value);

        /// <summary>新增資料庫指令的輸出參數
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="parameterName">資料庫指令的參數名稱</param>
        /// <param name="dbType">參數型態 (可以使用ZayniFramework 框架的 DbColumnType 靜態常數)</param>
        /// <param name="maxSize">變數的最大長度</param>
        /// <returns>Oracle 資料庫指令</returns>
        public DbCommand AddOutParameter(DbCommand command, string parameterName, int dbType, int maxSize)
        {
            var logTitle = Logger.GetTraceLogTitle(this, nameof(AddOutParameter));

            var cmd = command as OracleCommand;

            if (new string[] { command + "", parameterName }.HasNullOrEmptyElemants(true))
            {
                Message = $"Invalid argument. The '{nameof(command)}' or '{nameof(parameterName)}' can not be null or empty string.";
                return null;
            }

            try
            {
                parameterName = parameterName.TrimChars('@');
                var parameter = cmd.Parameters.Add(parameterName, (OracleDbType)dbType, maxSize);
                parameter.Direction = ParameterDirection.Output;
            }
            catch (Exception ex)
            {
                Message = $"Add out DbCommand parameter occur exception. ParameterName: {parameterName}, DbType: {dbType}, MaxSize: {maxSize}. {ex}";
                ExceptionObject = ex;
                Logger.Error(this, $"{Message}{Environment.NewLine}{ex}", logTitle);
                return null;
            }

            return cmd;
        }

        /// <summary>新增資料庫指令的輸出參數
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="parameterName">資料庫指令的參數名稱</param>
        /// <param name="dbType">參數型態</param>
        /// <param name="maxSize">變數的最大長度</param>
        /// <returns>Oracle 資料庫指令</returns>
        public DbCommand AddOutParameter(DbCommand command, string parameterName, OracleDbType dbType, int maxSize) =>
            AddOutParameter(command, parameterName, (int)dbType, maxSize);

        /// <summary>檢查是否包含指定的資料庫參數
        /// </summary>
        /// <param name="command">來源資料庫命令</param>
        /// <param name="parameterName">資料庫參數名稱</param>
        /// <returns>是否包含目標參數</returns>
        public bool ContainsParameter(DbCommand command, string parameterName)
        {
            if (new string[] { command + "", parameterName }.HasNullOrEmptyElemants())
            {
                return false;
            }

            if (!command.Parameters.Contains(parameterName))
            {
                Message = $"DbCommand does not contain '{parameterName}' parameter name.";
                return false;
            }

            return true;
        }

        /// <summary>取得資料庫指令參數值
        /// </summary>
        /// <param name="command">目標資料庫命令</param>
        /// <param name="parameterName">資料庫參數名稱</param>
        /// <returns>資料庫參數值</returns>
        public object GetParameterValue(DbCommand command, string parameterName)
        {
            var logTitle = Logger.GetTraceLogTitle(this, nameof(GetParameterValue));

            object result = null;

            if (parameterName.IsNullOrEmpty())
            {
                Message = $"Invalid argument. The '{nameof(parameterName)}' can not be null or empty string.";
                return null;
            }

            if (new object[] { command, command.Parameters }.HasNullElements())
            {
                Message = $"Invalid argument. The '{nameof(command)}' or '{nameof(parameterName)}' can not be null or empty string.";
                return null;
            }

            if (!ContainsParameter(command, parameterName))
            {
                return null;
            }

            try
            {
                result = command.Parameters[parameterName].Value;
            }
            catch (Exception ex)
            {
                Message = $"Get DbCommand parameter value occur exception. ParameterName: {parameterName}. {ex}";
                ExceptionObject = ex;
                Logger.Error(this, $"{Message}{Environment.NewLine}{ex}", logTitle);
                return null;
            }

            return result;
        }

        /// <summary>設定資料庫指令的參數值
        /// </summary>
        /// <param name="command">目標資料庫命令</param>
        /// <param name="parameterName">資料庫參數名稱</param>
        /// <param name="value">參數值</param>
        /// <returns>設定參數值是否成功</returns>
        public bool SetParameterValue(DbCommand command, string parameterName, object value)
        {
            var logTitle = Logger.GetTraceLogTitle(this, nameof(SetParameterValue));

            if (new string[] { command + "", parameterName }.HasNullOrEmptyElemants())
            {
                return false;
            }

            if (!ContainsParameter(command, parameterName))
            {
                return false;
            }

            try
            {
                command.Parameters[parameterName].Value = value ?? DBNull.Value;
            }
            catch (Exception ex)
            {
                Message = $"Set DbCommand parameter value occur exception. ParameterName: {parameterName}, ParameterValue: {value}. {ex}";
                ExceptionObject = ex;
                Logger.Error(this, $"{Message}{Environment.NewLine}{ex}", logTitle);
                return false;
            }

            return true;
        }

        /// <summary>將Sql字串轉型成Oracle字串
        /// </summary>
        /// <param name="commandText">OracleCommand字串</param>
        /// <returns>OracleCommand字串</returns>
        private string ConvertToOracleSqlString(string commandText)
        {
            var logTitle = Logger.GetTraceLogTitle(this, nameof(ConvertToOracleSqlString));

            try
            {
                // 20140611 Added by Pony: 發現在某些 PL/SQL 語法中，如果有 Windows 作業系統內建的換行符號在 Oracle 資料庫中執行會發生異常，因此在這邊要把 \r 字元置換成空字串
                // 20140620 Edited by Pony: 必須要先把 PL/SQL 語法中的單行註解 Trim 掉，然後才可以進行換行符號 Replace 的動作，否則最後的 SQL 字串會有問題
                var stmt = new SqlStatement(commandText);
                var sql = stmt.TrimSingleComment().Remove("\r").Remove("\n").ToString();
            }
            catch (Exception ex)
            {
                Logger.Error(this, $"Replace command text \\r \\n occur exception: {ex}", logTitle);
                return null;
            }

            var input = commandText;
            var pattern = @"\B[@]";
            var replacepattern = ":";
            var result = "";

            var regex = new Regex(pattern, RegexOptions.IgnoreCase);

            try
            {
                result = Regex.IsMatch(input, pattern) ? Regex.Replace(input, pattern, replacepattern) : input;
                result = result.Replace("|", "\"");           // 處理 Oracle Select 出來的欄位 AS 的別名，將 | 字元置換成半形雙引號
                result = result.Replace("`", string.Empty);   // 處理跨資料庫相容性 SQL 語法問題，把 ` 字元置換成空字串
            }
            catch (Exception ex)
            {
                Message = $"Convert the SQL statement into Oracle SQL statement occur exception. Original SQL statement: {commandText}.{Environment.NewLine}{ex}";
                ExceptionObject = ex;
                Logger.Error(this, $"{Message}{Environment.NewLine}{ex}", logTitle);
                return null;
            }

            return result;
        }

        #endregion DbCommand Methods
    }
}
