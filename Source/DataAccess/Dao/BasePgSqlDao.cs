using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Logging;

namespace ZayniFramework.DataAccess
{
    /// <summary>PostgreSQL 資料庫存取基底
    /// </summary>
    public class BasePgSqlDao : BaseDao, IDataAccess
    {
        #region Private Fields

        /// <summary>PostgreSQL 的 SQL Profile 日誌記錄員
        /// </summary>
        private PgSqlLogger _logger;

        /// <summary>ZayniFramework 框架資料庫連線字串
        /// </summary>
        private readonly string _zayniConnectionString;

        #endregion Private Fields


        #region Constructors

        /// <summary>靜態建構子
        /// </summary>
        static BasePgSqlDao()
        {
            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="dbName">資料庫連線字串名稱</param>
        public BasePgSqlDao(string dbName) : base(DbProviderOption.Postgres)
        {
            _logger = new PgSqlLogger(dbName);
            base.DbName = dbName;
            base.OrmBinder = new OrmDataBinder();
            base.LoadConnectionString();
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="dbName">資料庫連線字串名稱</param>
        /// <param name="connectionString">資料庫連線字串</param>
        /// <param name="zayniConnectionString">ZayniFramework 框架的資料庫連線字串 (若傳入為 Null 或空字串，SQL Profile Log 功能將停用)</param>
        public BasePgSqlDao(string dbName, string connectionString, string zayniConnectionString = null) : base(DbProviderOption.Postgres)
        {
            _zayniConnectionString = zayniConnectionString;
            _logger = new PgSqlLogger(dbName, zayniConnectionString);
            base.ConnectionString = connectionString;
            base.OrmBinder = new OrmDataBinder();
        }

        /// <summary>解構子
        /// </summary>
        ~BasePgSqlDao()
        {
            _logger = null;
            base.DbName = null;
            base.OrmBinder = null;
            base.ConnectionString = null;
        }

        #endregion Constructors


        #region Public Methods

        /// <summary>取得 PostgreSQL 資料庫的空字串值
        /// </summary>
        /// <returns>PostgreSQL 資料庫的空字串值</returns>
        public override string GetDBEmptyString() => string.Empty;

        #endregion Public Methods


        #region Private Execute Methods

        /// <summary>執行目標 SQL 敘述命令
        /// </summary>
        /// <param name="command">目標 SQL 敘述命令</param>
        /// <returns>執行目標 SQL 命令是否成功</returns>
        private async Task<bool> DoExecuteNonQueryAsync(NpgsqlCommand command)
        {
            _logger.IsNull(() => _logger = _zayniConnectionString.IsNullOrEmpty() ? new PgSqlLogger(base.DbName) : new PgSqlLogger(base.DbName, _zayniConnectionString));

            if (DbCommands.Add(command, out var commandId))
            {
                _logger?.WriteDbCommandRequestLog(commandId, command, GetType().Name);
            }

            var success = await base.DoExecuteNonQueryAsync(command, command.ExecuteNonQueryAsync);

            if (commandId.IsNotNullOrEmpty())
            {
                _logger?.WriteDbCommandResultLog(commandId, command, DataCount, GetType().Name);
                DbCommands.Remove(commandId);
            }

            return success;
        }

        /// <summary>執行目標 SQL 敘述命令
        /// </summary>
        /// <param name="command">目標 SQL 敘述命令</param>
        /// <returns>執行目標 SQL 命令是否成功</returns>
        private bool DoExecuteNonQuery(NpgsqlCommand command)
        {
            _logger.IsNull(() => _logger = _zayniConnectionString.IsNullOrEmpty() ? new PgSqlLogger(base.DbName) : new PgSqlLogger(base.DbName, _zayniConnectionString));

            if (DbCommands.Add(command, out var commandId))
            {
                _logger?.WriteDbCommandRequestLog(commandId, command, GetType().Name);
            }

            var isSuccess = base.DoExecuteNonQuery(command, command.ExecuteNonQuery);

            if (commandId.IsNotNullOrEmpty())
            {
                _logger?.WriteDbCommandResultLog(commandId, command, DataCount, GetType().Name);
                DbCommands.Remove(commandId);
            }

            return isSuccess;
        }

        /// <summary>執行目標 SQL 敘述，並且取得資料讀取器
        /// </summary>
        /// <param name="command">目標 SQL 敘述命令</param>
        /// <returns>執行目標 SQL 命令是否成功</returns>
        private async Task<Result<IDataReader>> DoExecuteReaderAsync(NpgsqlCommand command)
        {
            var r = await base.DoExecuteReaderAsync(command, async () => (IDataReader)await command.ExecuteReaderAsync());

            if (r.Success)
            {
                _logger.IsNull(() => _logger = _zayniConnectionString.IsNullOrEmpty() ? new PgSqlLogger(base.DbName) : new PgSqlLogger(base.DbName, _zayniConnectionString));
                _logger?.WriteLog(command, GetType().Name);
            }

            var reader = r.Data;
            return Result.Create<IDataReader>(true, data: reader);
        }

        /// <summary>執行目標 SQL 敘述，並且取得資料讀取器
        /// </summary>
        /// <param name="command">目標 SQL 敘述命令</param>
        /// <param name="reader">資料讀取器</param>
        /// <returns>執行目標 SQL 命令是否成功</returns>
        private bool DoExecuteReader(NpgsqlCommand command, out IDataReader reader)
        {
            var isSuccess = base.DoExecuteReader(command, command.ExecuteReader, out reader);

            if (isSuccess)
            {
                _logger.IsNull(() => _logger = _zayniConnectionString.IsNullOrEmpty() ? new PgSqlLogger(base.DbName) : new PgSqlLogger(base.DbName, _zayniConnectionString));
                _logger?.WriteLog(command, GetType().Name);
            }

            return isSuccess;
        }

        /// <summary>執行目標 SQL 敘述，並且查詢單一純量值
        /// </summary>
        /// <param name="command">目標 SQL 敘述命令</param>
        /// <returns>執行目標 SQL 命令是否成功</returns>
        private async Task<Result<object>> DoExecuteScalarAsync(NpgsqlCommand command)
        {
            _logger.IsNull(() => _logger = _zayniConnectionString.IsNullOrEmpty() ? new PgSqlLogger(base.DbName) : new PgSqlLogger(base.DbName, _zayniConnectionString));

            if (DbCommands.Add(command, out var commandId))
            {
                _logger?.WriteDbCommandRequestLog(commandId, command, GetType().Name);
            }

            var result = await base.DoExecuteScalarAsync(command, command.ExecuteScalarAsync);

            if (commandId.IsNotNullOrEmpty())
            {
                _logger?.WriteDbCommandResultLog(commandId, command, result.Data, GetType().Name);
                DbCommands.Remove(commandId);
            }

            return result;
        }

        /// <summary>執行目標 SQL 敘述，並且查詢單一純量值
        /// </summary>
        /// <param name="command">目標 SQL 敘述命令</param>
        /// <param name="obj">查詢結果 (單一純量值)</param>
        /// <returns>執行目標 SQL 命令是否成功</returns>
        private bool DoExecuteScalar(NpgsqlCommand command, out object obj)
        {
            _logger.IsNull(() => _logger = _zayniConnectionString.IsNullOrEmpty() ? new PgSqlLogger(base.DbName) : new PgSqlLogger(base.DbName, _zayniConnectionString));

            if (DbCommands.Add(command, out var commandId))
            {
                _logger?.WriteDbCommandRequestLog(commandId, command, GetType().Name);
            }

            var isSuccess = base.DoExecuteScalar(command, command.ExecuteScalar, out obj);

            if (commandId.IsNotNullOrEmpty())
            {
                _logger?.WriteDbCommandResultLog(commandId, command, obj, GetType().Name);
                DbCommands.Remove(commandId);
            }

            return isSuccess;
        }

        /// <summary>執行 SQL 查詢指令，回傳執行使否成功
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="ds">資料集合</param>
        /// <param name="tableNames">資料表名稱</param>
        /// <returns>是否執行查詢成功</returns>
        private async Task<bool> DoLoadDataSetAsync(NpgsqlCommand command, DataSet ds, params string[] tableNames)
        {
            _logger.IsNull(() => _logger = _zayniConnectionString.IsNullOrEmpty() ? new PgSqlLogger(base.DbName) : new PgSqlLogger(base.DbName, _zayniConnectionString));

            if (DbCommands.Add(command, out var commandId))
            {
                _logger?.WriteDbCommandRequestLog(commandId, command, GetType().Name);
            }

            var success = await base.DoLoadDataSetAsync(command, ds, tableNames);

            if (commandId.IsNotNullOrEmpty())
            {
                _logger?.WriteDbCommandResultLog(commandId, command, ds, GetType().Name);
                DbCommands.Remove(commandId);
            }

            return success;
        }

        /// <summary>執行 SQL 查詢指令，回傳執行使否成功
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="ds">資料集合</param>
        /// <param name="tableNames">資料表名稱</param>
        /// <returns>是否執行查詢成功</returns>
        private bool DoLoadDataSet(NpgsqlCommand command, DataSet ds, params string[] tableNames)
        {
            _logger.IsNull(() => _logger = _zayniConnectionString.IsNullOrEmpty() ? new PgSqlLogger(base.DbName) : new PgSqlLogger(base.DbName, _zayniConnectionString));

            if (DbCommands.Add(command, out var commandId))
            {
                _logger?.WriteDbCommandRequestLog(commandId, command, GetType().Name);
            }

            var isSuccess = base.DoLoadDataSet(command, ds, tableNames);

            if (commandId.IsNotNullOrEmpty())
            {
                _logger?.WriteDbCommandResultLog(commandId, command, ds, GetType().Name);
                DbCommands.Remove(commandId);
            }

            return isSuccess;
        }

        #endregion Private Execute Methods


        #region ExecuteNonQuery Methods

        /// <summary>執行 DbCommand 資料庫指令。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <returns>是否執行成功</returns>
        public async Task<bool> ExecuteNonQueryAsync(DbCommand command)
        {
            var cmd = command as NpgsqlCommand;

            if (cmd.IsNull())
            {
                Message = $"Invalid argument. The DbCommand is not a NpgsqlCommand.";
                return false;
            }

            return await DoExecuteNonQueryAsync(cmd);
        }

        /// <summary>執行 DbCommand 資料庫指令。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <returns>是否執行成功</returns>
        public bool ExecuteNonQuery(DbCommand command)
        {
            var cmd = command as NpgsqlCommand;

            if (cmd.IsNull())
            {
                Message = $"Invalid argument. The DbCommand is not a NpgsqlCommand.";
                return false;
            }

            return DoExecuteNonQuery(cmd);
        }

        #endregion ExecuteNonQuery Methods


        #region ExecuteReader Methods

        /// <summary>執行資料庫 DbCommand 查詢指令。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <returns>執行查詢 SQL 資料讀取器是否成功</returns>
        public async Task<Result<IDataReader>> ExecuteReaderAsync(DbCommand command)
        {
            var cmd = command as NpgsqlCommand;

            if (cmd.IsNull())
            {
                Message = $"Invalid argument. The DbCommand is not a NpgsqlCommand.";
                return Result.Create<IDataReader>(false, message: Message);
            }

            return await DoExecuteReaderAsync(cmd);
        }

        /// <summary>執行資料庫 DbCommand 查詢指令。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="reader">資料讀取器</param>
        /// <returns>執行查詢 SQL 資料讀取器是否成功</returns>
        public bool ExecuteReader(DbCommand command, out IDataReader reader)
        {
            reader = null;

            var cmd = command as NpgsqlCommand;

            if (cmd.IsNull())
            {
                Message = $"Invalid argument. The DbCommand is not a NpgsqlCommand.";
                return false;
            }

            return DoExecuteReader(cmd, out reader);
        }

        #endregion ExecuteReader Methods


        #region ExecuteScalar Methods

        /// <summary>執行 DbCommand 查詢指令，並且輸出單一純量值。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <returns>執行是否成功</returns>
        public async Task<Result<object>> ExecuteScalarAsync(DbCommand command)
        {
            var cmd = command as NpgsqlCommand;

            if (cmd.IsNull())
            {
                Message = $"Invalid argument. The DbCommand is not a NpgsqlCommand.";
                return Result.Create<object>(false, message: Message);
            }

            return await DoExecuteScalarAsync(cmd);
        }

        /// <summary>執行 DbCommand 查詢指令，並且輸出單一純量值。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="result">查詢單一純量值結果</param>
        /// <returns>執行是否成功</returns>
        public bool ExecuteScalar(DbCommand command, out object result)
        {
            result = null;

            var cmd = command as NpgsqlCommand;

            if (cmd.IsNull())
            {
                Message = $"Invalid argument. The DbCommand is not a NpgsqlCommand.";
                return false;
            }

            return DoExecuteScalar(cmd, out result);
        }

        #endregion ExecuteScalar Methods


        #region LoadDataSet Methods

        /// <summary>執行 DbCommand 查詢指令，回傳執行使否成功。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="ds">資料集合</param>
        /// <param name="tableNames">資料表名稱</param>
        /// <returns>是否執行查詢成功</returns>
        public async Task<bool> LoadDataSetAsync(DbCommand command, DataSet ds, params string[] tableNames)
        {
            var cmd = command as NpgsqlCommand;

            if (cmd.IsNull())
            {
                Message = $"Invalid argument. The DbCommand is not a NpgsqlCommand.";
                return false;
            }

            return await DoLoadDataSetAsync(cmd, ds, tableNames);
        }

        /// <summary>執行 DbCommand 查詢指令，回傳執行使否成功。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="ds">資料集合</param>
        /// <param name="tableNames">資料表名稱</param>
        /// <returns>是否執行查詢成功</returns>
        public bool LoadDataSet(DbCommand command, DataSet ds, params string[] tableNames)
        {
            var cmd = command as NpgsqlCommand;

            if (cmd.IsNull())
            {
                Message = $"Invalid argument. The DbCommand is not a NpgsqlCommand.";
                return false;
            }

            return DoLoadDataSet(cmd, ds, tableNames);
        }

        #endregion LoadDataSet Methods


        #region LoadDataToModel

        /// <summary>執行 DbCommand 查詢指令，輸出查詢結果資料模型集合
        /// </summary>
        /// <typeparam name="TModel">資料模型泛型</typeparam>
        /// <param name="command">資料庫指令</param>
        /// <returns>是否執行查詢成功</returns>
        public async Task<Result<IEnumerable<TModel>>> LoadDataToModelAsync<TModel>(DbCommand command) where TModel : new() =>
            await base.DoLoadDataToModelAsync<TModel>(command, async ds => await LoadDataSetAsync(command, ds));

        /// <summary>執行 DbCommand 查詢指令，輸出查詢結果資料模型集合
        /// </summary>
        /// <typeparam name="TModel">資料模型泛型</typeparam>
        /// <param name="command">資料庫指令</param>
        /// <param name="models">資料模型集合</param>
        /// <returns>是否執行查詢成功</returns>
        public bool LoadDataToModel<TModel>(DbCommand command, out IEnumerable<TModel> models) where TModel : new() =>
            base.DoLoadDataToModel<TModel>(command, ds => { return LoadDataSet(command, ds); }, out models);

        #endregion LoadDataToModel


        #region LoadDataToDynamicModel

        /// <summary>執行 DbCommand 查詢指令，輸出動態物件資料集合。
        /// * ORM 資料綁定機制: 單一資料模型 ORM 對應，應用在 SQL 查詢語法中只有一個 SELECT 查詢語法，回傳單一資料表結果集合。
        /// * 對查詢單一結果資料表，進行動態 ORM 資料綁定。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <returns>是否執行查詢成功</returns>
        public async Task<Result<IEnumerable<dynamic>>> LoadDataToDynamicModelAsync(DbCommand command) =>
            await base.DoLoadDataToDynamicModelAsync(command, async () => await ExecuteReaderAsync(command));

        /// <summary>執行 DbCommand 查詢指令，輸出動態物件資料集合。
        /// * ORM 資料綁定機制: 單一資料模型 ORM 對應，應用在 SQL 查詢語法中只有一個 SELECT 查詢語法，回傳單一資料表結果集合。
        /// * 對查詢單一結果資料表，進行動態 ORM 資料綁定。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="models">動態資料模型串列集合</param>
        /// <returns>是否執行查詢成功</returns>
        public bool LoadDataToDynamicModel(DbCommand command, out IEnumerable<dynamic> models) =>
            base.DoLoadDataToDynamicModel(command, (out IDataReader reader) => { return ExecuteReader(command, out reader); }, out models);

        #endregion LoadDataToDynamicModel


        #region LoadDataToModels

        /// <summary>執行 DbCommand 查詢指令，依照指定的 Type 陣列回傳多組 ORM 綁定的資料模型集合。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="modelTypes">資料模型型別陣列</param>
        /// <returns>是否執行查詢成功</returns>
        public async Task<Result<IEnumerable<object[]>>> LoadDataToModelsAsync(DbCommand command, Type[] modelTypes) =>
            await base.DoLoadDataToModelsAsync(command, modelTypes, async ds => await LoadDataSetAsync(command, ds));

        /// <summary>執行 DbCommand 查詢指令，依照指定的 Type 陣列回傳多組 ORM 綁定的資料模型集合。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="modelTypes">資料模型型別陣列</param>
        /// <param name="result">查詢結果資料模型集合</param>
        /// <returns>是否執行查詢成功</returns>
        public bool LoadDataToModels(DbCommand command, Type[] modelTypes, out IEnumerable<object[]> result) =>
            base.DoLoadDataToModels(command, modelTypes, ds => { return LoadDataSet(command, ds); }, out result);

        #endregion LoadDataToModels


        #region LoadDataToDynamicCollection

        /// <summary>執行 DbCommand 查詢指令，DbCommand 的 SQL 查詢語法可以支援多組 SELECT 查詢，並且輸出多組動態 ORM 資料綁定的查詢結果集合。
        /// </summary>
        /// <param name="command">資料庫指令
        /// * Key 值: 依照順序傳入的 tableName 參數名稱。
        /// * Value 值: 動態物件資料模型串列。
        /// </param>
        /// <param name="tableName">資料表名稱</param>
        /// <returns>是否執行查詢成功</returns>
        public async Task<Result<Dictionary<string, IEnumerable<dynamic>>>> LoadDataToDynamicCollectionAsync(DbCommand command, params string[] tableName) =>
            await base.DoLoadDataToDynamicCollectionAsync(command, async ds => await LoadDataSetAsync(command, ds, tableName), tableName);

        /// <summary>執行 DbCommand 查詢指令，DbCommand 的 SQL 查詢語法可以支援多組 SELECT 查詢，並且輸出多組動態 ORM 資料綁定的查詢結果集合。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="modelCollection">查詢結果的資料集合，動態物件字典集合。<para/>
        /// * Key 值: 依照順序傳入的 tableName 參數名稱。
        /// * Value 值: 動態物件資料模型串列。
        /// </param>
        /// <param name="tableName">資料表名稱</param>
        /// <returns>是否執行查詢成功</returns>
        public bool LoadDataToDynamicCollection(DbCommand command, out Dictionary<string, IEnumerable<dynamic>> modelCollection, params string[] tableName) =>
            base.DoLoadDataToDynamicCollection(command, ds => { return LoadDataSet(command, ds, tableName); }, out modelCollection, tableName);

        #endregion LoadDataToDynamicCollection


        #region LoadModelSet

        /// <summary>執行 DbCommand 查詢指令，輸出自定義的 ModelSet 資料集合中。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="modelTypes">資料模型型別陣列</param>
        /// <param name="modelSet">查詢結果資料集合</param>
        /// <returns>是否執行查詢成功</returns>
        public async Task<bool> LoadModelSetAsync(DbCommand command, ModelSet modelSet, params Type[] modelTypes) =>
            await base.DoLoadModelSetAsync(command, modelSet, async () => await LoadDataToModelsAsync(command, modelTypes), modelTypes);

        /// <summary>執行 DbCommand 查詢指令，輸出自定義的 ModelSet 資料集合中。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="modelTypes">資料模型型別陣列</param>
        /// <param name="modelSet">查詢結果資料集合</param>
        /// <returns>是否執行查詢成功</returns>
        public bool LoadModelSet(DbCommand command, ModelSet modelSet, params Type[] modelTypes) =>
            base.DoLoadModelSet(command, modelSet, (out IEnumerable<object[]> m) => { return LoadDataToModels(command, modelTypes, out m); }, modelTypes);

        #endregion LoadModelSet


        #region DbCommand Methods

        /// <summary>取得資料庫指令物件
        /// * 如果在 PostgreSQL 資料庫中的 table name 或 column name 之類的 object name 是以大寫 snake case 命名，則傳入的 SQL statement 則需要以 ` 字元包覆住 object name。
        /// * PostgreSQL 資料庫預設習慣以小寫 snake case 進行 table、column、function、procedure、trigger 進行命名。
        /// </summary>
        /// <param name="commandText">SQL 敘述字串</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <returns>PostgreSQL 資料庫指令</returns>
        public DbCommand GetSqlStringCommand(string commandText, DbConnection connection = null, DbTransaction transaction = null)
        {
            commandText = ConvertToPgSqlString(commandText);
            var command = base.GetDbCommand(commandText, CommandType.Text);
            command.Connection = connection;
            command.Transaction = transaction;
            return command;
        }

        /// <summary>取得資料庫預存程序的資料庫指令
        /// </summary>
        /// <param name="storedProcName">資料庫預存程序名稱</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <returns>MSSQL 資料庫指令</returns>
        public DbCommand GetStoredProcCommand(string storedProcName, DbConnection connection = null, DbTransaction transaction = null)
        {
            var command = base.GetDbCommand(storedProcName, CommandType.StoredProcedure);
            command.Connection = connection;
            command.Transaction = transaction;
            return command;
        }

        /// <summary>新增資料庫指令參數
        /// </summary>
        /// <param name="cmd">資料庫指令</param>
        /// <param name="parameterName">資料庫指令的參數名稱</param>
        /// <param name="dbType">參數型態 (可以使用ZayniFramework 框架的 DbColumnType 靜態常數)</param>
        /// <param name="value">參數值</param>
        public DbCommand AddInParameter(DbCommand cmd, string parameterName, int dbType, object value)
        {
            var logTitle = Logger.GetTraceLogTitle(this, nameof(AddInParameter));

            var command = cmd as NpgsqlCommand;

            if (new string[] { command + "", parameterName }.HasNullOrEmptyElemants(true))
            {
                Message = $"Invalid argument. The '{nameof(cmd)}' or '{nameof(parameterName)}' can not be null or empty string.";
                return null;
            }

            try
            {
                if (parameterName.StartsWith('@'))
                {
                    parameterName = parameterName.RemoveFirstAppeared("@");
                }

                command.Parameters.Add(parameterName, (NpgsqlDbType)dbType);
                command.Parameters[parameterName].Value = value ?? DBNull.Value;
                command.Parameters[parameterName].Direction = ParameterDirection.Input;
            }
            catch (Exception ex)
            {
                Message = $"Add DbCommand parameter occur exception. ParameterName: {parameterName}, DbType: {dbType}, Value: {value}. {ex}";
                ExceptionObject = ex;
                Logger.Error(this, $"{Message}{Environment.NewLine}{ex}", logTitle);
                return null;
            }

            return command;
        }

        /// <summary>新增資料庫指令參數
        /// </summary>
        /// <param name="cmd">資料庫指令</param>
        /// <param name="parameterName">資料庫指令的參數名稱</param>
        /// <param name="dbType">參數型別代碼</param>
        /// <param name="value">參數值</param>
        public DbCommand AddInParameter(DbCommand cmd, string parameterName, NpgsqlDbType dbType, object value) =>
            AddInParameter(cmd, parameterName, (int)dbType, value);

        /// <summary>新增資料庫指令的輸出參數
        /// </summary>
        /// <param name="cmd">資料庫指令</param>
        /// <param name="parameterName">資料庫指令的參數名稱</param>
        /// <param name="dbType">參數型態 (可以使用ZayniFramework 框架的 DbColumnType 靜態常數)</param>
        /// <param name="maxSize">變數的最大長度</param>
        public DbCommand AddOutParameter(DbCommand cmd, string parameterName, int dbType, int maxSize)
        {
            var logTitle = Logger.GetTraceLogTitle(this, nameof(AddOutParameter));

            var command = cmd as NpgsqlCommand;

            if (new string[] { command + "", parameterName }.HasNullOrEmptyElemants(true))
            {
                Message = $"Invalid argument. The '{nameof(cmd)}' or '{nameof(parameterName)}' can not be null or empty string.";
                return null;
            }

            try
            {
                parameterName = parameterName.TrimChars('@');
                parameterName = "?" + parameterName;

                var param = command.Parameters.Add(parameterName, (NpgsqlDbType)dbType, maxSize);
                param.Direction = ParameterDirection.Output;
            }
            catch (Exception ex)
            {
                Message = $"Add out DbCommand parameter occur exception. ParameterName: {parameterName}, DbType: {dbType}, MaxSize: {maxSize}. {ex}";
                ExceptionObject = ex;
                Logger.Error(this, $"{Message}{Environment.NewLine}{ex}", logTitle);
                return null;
            }

            return command;
        }

        /// <summary>新增資料庫指令的輸出參數
        /// </summary>
        /// <param name="cmd">資料庫指令</param>
        /// <param name="parameterName">資料庫指令的參數名稱</param>
        /// <param name="dbType">參數型態</param>
        /// <param name="maxSize">變數的最大長度</param>
        public DbCommand AddOutParameter(DbCommand cmd, string parameterName, NpgsqlDbType dbType, int maxSize)
        {
            return AddOutParameter(cmd, parameterName, (int)dbType, maxSize);
        }

        /// <summary>檢查是否包含指定的資料庫參數
        /// </summary>
        /// <param name="command">來源資料庫命令</param>
        /// <param name="parameterName">資料庫參數名稱</param>
        /// <returns>是否包含目標參數</returns>
        public bool ContainsParameter(DbCommand command, string parameterName)
        {
            if (new string[] { command + "", parameterName }.HasNullOrEmptyElemants())
            {
                return false;
            }

            if (!command.Parameters.Contains(parameterName))
            {
                Message = $"DbCommand does not contain '{parameterName}' parameter name.";
                return false;
            }

            return true;
        }

        /// <summary>取得資料庫指令參數值
        /// </summary>
        /// <param name="command">目標資料庫命令</param>
        /// <param name="parameterName">資料庫參數名稱</param>
        /// <returns>資料庫參數值</returns>
        public object GetParameterValue(DbCommand command, string parameterName)
        {
            var logTitle = Logger.GetTraceLogTitle(this, nameof(GetParameterValue));

            object result = null;

            if (parameterName.IsNullOrEmpty())
            {
                Message = $"Invalid argument. The '{nameof(parameterName)}' can not be null or empty string.";
                return null;
            }

            if (new object[] { command, command.Parameters }.HasNullElements())
            {
                Message = $"Invalid argument. The '{nameof(command)}' or '{nameof(parameterName)}' can not be null or empty string.";
                return null;
            }

            if (!ContainsParameter(command, parameterName))
            {
                return null;
            }

            try
            {
                result = command.Parameters[parameterName].Value;
            }
            catch (Exception ex)
            {
                Message = $"Get DbCommand parameter value occur exception. ParameterName: {parameterName}. {ex}";
                ExceptionObject = ex;
                Logger.Error(this, $"{Message}{Environment.NewLine}{ex}", logTitle);
                return null;
            }

            return result;
        }

        /// <summary>設定資料庫指令的參數值
        /// </summary>
        /// <param name="command">目標資料庫命令</param>
        /// <param name="parameterName">資料庫參數名稱</param>
        /// <param name="value">參數值</param>
        /// <returns>設定參數值是否成功</returns>
        public bool SetParameterValue(DbCommand command, string parameterName, object value)
        {
            var logTitle = Logger.GetTraceLogTitle(this, nameof(SetParameterValue));

            if (new string[] { command + "", parameterName }.HasNullOrEmptyElemants())
            {
                return false;
            }

            if (!ContainsParameter(command, parameterName))
            {
                return false;
            }

            try
            {
                command.Parameters[parameterName].Value = value ?? DBNull.Value;
            }
            catch (Exception ex)
            {
                Message = $"Set DbCommand parameter value occur exception. ParameterName: {parameterName}, ParameterValue: {value}. {ex}";
                ExceptionObject = ex;
                Logger.Error(this, $"{Message}{Environment.NewLine}{ex}", logTitle);
                return false;
            }

            return true;
        }

        /// <summary>將 SQL 字串轉型成 PostgreSQL 字串
        /// * 如果在 PostgreSQL 資料庫中的 table name 或 column name 之類的 object name 是以大寫 snake case 命名，則傳入的 SQL statement 則需要以 ` 字元包覆住 object name。
        /// * 在此方法中會自動將 ` 字元置換成 " 雙引號字元。
        /// </summary>
        /// <param name="commandText">NpgsqlCommand 字串</param>
        /// <returns>NpgsqlCommand 字串</returns>
        private string ConvertToPgSqlString(string commandText)
        {
            var logTitle = Logger.GetTraceLogTitle(this, nameof(ConvertToPgSqlString));
            var result = string.Empty;

            try
            {
                commandText = commandText?.Replace("`", "\"");    // 處理 Postgres 的資料表名稱或欄位名稱為大寫英文字元時，需要將 ` 字元置換成半形雙引號
                commandText = commandText?.Replace("|", "\"");    // 處理 Postgres Select 出來的欄位 AS 的別名，將半形單引號置換成半形雙引號
                return commandText;
            }
            catch (Exception ex)
            {
                Message = $"Convert the SQL statement into PostgreSQL SQL statement occur exception. Original SQL statement: {commandText}.{Environment.NewLine}{ex}";
                ExceptionObject = ex;
                Logger.Error(this, $"{Message}{Environment.NewLine}{ex}", logTitle);
                return null;
            }
        }

        #endregion DbCommand Methods
    }
}