﻿using System.Collections.Generic;


namespace ZayniFramework.Validation
{
    /// <summary>資料驗證請求
    /// </summary>
    public sealed class ValidationEntry
    {
        #region 宣告建構子

        /// <summary>解構子
        /// </summary>
        ~ValidationEntry()
        {
            Language = null;
            IgnoreProperies = null;
            IgnoreValitions = null;
        }

        #endregion 宣告建構子


        #region 宣告公開的屬性

        /// <summary>目標受檢資料模型
        /// </summary>
        public object TargetModel { get; set; }

        /// <summary>語系代碼 (預設為繁體中文)<para/>
        /// 1. 控制當資料驗證失敗時，Validation 模組回傳的「驗證失敗訊息」是根據哪一種語系。<para/>
        /// 2. 如果有設定傳入語系代碼屬性，必須要在Entity組件中包含「驗證失敗訊息」多國語系的Resource File。<para/>
        /// ex: Messages.resx、Messages.en-US.resx、Messages.zh-CN.resx等等的資源檔案。<para/>
        /// </summary>
        public string Language { get; set; } = "zh-TW";

        /// <summary>要忽略資料驗證的屬性
        /// </summary>
        public List<string> IgnoreProperies { get; set; } = [];

        /// <summary>要忽略資料驗證的驗證規則，Key為屬性名稱，Value為要忽略的驗證規則
        /// </summary>
        public Dictionary<string, List<string>> IgnoreValitions { get; set; } = [];

        #endregion 宣告公開的屬性
    }
}
