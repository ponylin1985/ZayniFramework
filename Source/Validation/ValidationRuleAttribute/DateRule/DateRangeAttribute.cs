﻿using System;
using ZayniFramework.Common;


namespace ZayniFramework.Validation
{
    /// <summary>時間區間檢查
    /// </summary>
    public class DateRangeAttribute : ValidationAttribute
    {
        #region 公開屬性

        /// <summary>開始日期
        /// </summary>
        public string BeginDate
        {
            get;
            set;
        }

        /// <summary>結束日期
        /// </summary>
        public string EndDate
        {
            get;
            set;
        }

        /// <summary>是否包含開始日期，預設為true有包含開始日期
        /// </summary>
        public bool IncludeBeginDate
        {
            get;
            set;
        }


        /// <summary>是否包含結束日期，預設為true有包含結束日期
        /// </summary>
        public bool IncludeEndDate
        {
            get;
            set;
        }

        #endregion 公開屬性


        #region 建構子

        /// <summary>多載建構子
        /// </summary>
        public DateRangeAttribute() : base()
        {
            IncludeBeginDate = true;
            IncludeEndDate = true;
        }

        /// <summary>多載建構子
        /// </summary>
        public DateRangeAttribute(string message) : base(message)
        {
            IncludeBeginDate = true;
            IncludeEndDate = true;
        }

        #endregion 建構子


        #region 公開方法

        /// <summary>實作驗證機制
        /// </summary>
        /// <param name="field">需驗證物件</param>
        /// <returns>回傳驗證結果</returns>
        public override ValidationResult DoValidate(object field)
        {
            var strValue = field + "";
            var result = new ValidationResult();

            if (!DateTime.TryParse(strValue, out var target))
            {
                result.IsValid = false;
                result.Message = string.IsNullOrWhiteSpace(Message) ? "DateTime轉型失敗" : Message; ;
                return result;
            }

            var beginExpression = true;
            var endExpression = true;

            if (DateTime.TryParse(BeginDate, out var beginDate))
            {
                beginExpression = beginDate.IsSmaller(target, IncludeBeginDate);
            }

            if (DateTime.TryParse(EndDate, out var endDate))
            {
                endExpression = endDate.IsGreater(target, IncludeEndDate);
            }

            if (beginExpression && endExpression)
            {
                result.IsValid = true;
                result.Message = "";
                return result;
            }

            result.IsValid = false;
            result.Message = string.IsNullOrWhiteSpace(Message) ? "日期不在設定的範圍內" : Message; ;
            return result;
        }

        #endregion 公開方法
    }
}
