﻿using System;
using System.Collections;
using ZayniFramework.Common;


namespace ZayniFramework.Validation
{
    /// <summary>檢查是否為NULL或者是空字串
    /// </summary>
    public class NotNullOrEmptyAttribute : ValidationAttribute
    {
        #region 建構子

        /// <summary>多載建構子
        /// </summary>
        public NotNullOrEmptyAttribute()
        {
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="message">外部訊息</param>
        public NotNullOrEmptyAttribute(string message) : base(message)
        {
        }

        #endregion 建構子


        #region 宣告公開方法

        /// <summary>實作驗證機制
        /// </summary>
        /// <param name="target">需驗證物件</param>
        /// <returns>回傳驗證結果</returns>
        public override ValidationResult DoValidate(object target)
        {
            var result = new ValidationResult()
            {
                IsValid = false
            };

            var value = default(IEnumerable);

            try
            {
                value = (IEnumerable)target;
            }
            catch (Exception ex)
            {
                result.Message = $"{nameof(NotNullOrEmptyAttribute)} must be apply on an {nameof(IEnumerable)} property type. {Environment.NewLine}{ex}";
                return result;
            }

            if (value.IsNullOrEmpty())
            {
                result.Message = Message.IsNullOrEmpty() ? $"Data can not be null or empty collection of {nameof(IEnumerable)}." : Message;
                return result;
            }

            result.IsValid = true;
            return result;
        }

        #endregion 宣告公開方法
    }
}
