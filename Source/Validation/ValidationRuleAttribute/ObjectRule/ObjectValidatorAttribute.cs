﻿using ZayniFramework.Common;


namespace ZayniFramework.Validation
{
    /// <summary>複雜型別屬性的資料驗證
    /// </summary>
    public class ObjectValidatorAttribute : ObjectPropertyValidatorAttribute
    {
        /// <summary>進行資料驗證
        /// </summary>
        /// <param name="target">目標資料</param>
        /// <returns>資料驗證結果</returns>
        public override ValidationResult DoValidate(object target)
        {
            var result = new ValidationResult()
            {
                IsValid = false
            };

            if (target.IsNull())
            {
                result.Message = Message.IsNullOrEmptyString("資料不可以為Null值。", true);
                return result;
            }

            return ValidatorFactory.Create().Validate(new ValidationEntry()
            {
                TargetModel = target,
                Language = base.Language
            });
        }
    }
}
