﻿using ZayniFramework.Common;


namespace ZayniFramework.Validation
{
    /// <summary>檢查目標資料是否為Null值
    /// </summary>
    public class NotNullAttribute : ValidationAttribute
    {
        /// <summary>進行資料驗證
        /// </summary>
        /// <param name="target">目標資料</param>
        /// <returns>資料驗證結果</returns>
        public override ValidationResult DoValidate(object target)
        {
            var result = new ValidationResult()
            {
                IsValid = false
            };

            if (null == target)
            {
                result.Message = Message.IsNullOrEmptyString("The target object cannot be null.", true);
                return result;
            }

            result.IsValid = true;
            return result;
        }
    }
}
