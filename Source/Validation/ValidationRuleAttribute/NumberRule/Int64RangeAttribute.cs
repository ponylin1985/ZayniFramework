using ZayniFramework.Common;


namespace ZayniFramework.Validation
{
    /// <summary>Int64 整數範圍檢查
    /// </summary>
    public class Int64RangeAttribute : NumberRangeAttribute
    {
        /// <summary>實作驗證機制
        /// </summary>
        /// <param name="field">需驗證物件</param>
        /// <returns>回傳驗證結果</returns>
        public override ValidationResult DoValidate(object field)
        {
            var strValue = field + "";
            var result = base.CheckTargetType<long>(strValue, out var target, "Invalid Int64 value.", long.TryParse);

            if (!result.IsValid)
            {
                return result;
            }

            result = base.Validate(strValue, "Int64 value out of range.");
            return result;
        }
    }
}
