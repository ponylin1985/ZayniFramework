﻿using System.Linq;
using ZayniFramework.Common;


namespace ZayniFramework.Validation
{
    /// <summary>數值大小範圍檢查
    /// </summary>
    public abstract class NumberRangeAttribute : ValidationAttribute
    {
        #region 宣告公開屬性

        /// <summary>最大值
        /// </summary>
        public string Max { get; set; }

        /// <summary>最小值
        /// </summary>
        public string Min { get; set; }

        /// <summary>是否有包含最大值，預設值為true有包含最大值
        /// </summary>
        public bool IncludeMax { get; set; }

        /// <summary>是否有包含最小值，預設值為true有包含最小值。
        /// </summary>
        public bool IncludeMin { get; set; }

        #endregion 宣告公開屬性


        #region 宣告建構子

        /// <summary>預設建構子
        /// </summary>
        protected NumberRangeAttribute()
        {
            IncludeMax = true;
            IncludeMin = true;
        }

        /// <summary>多載建構子
        /// </summary>
        protected NumberRangeAttribute(string message) : base(message)
        {
            IncludeMax = true;
            IncludeMin = true;
        }

        #endregion 宣告建構子


        #region 宣告受保護的方法

        /// <summary>檢查轉型是否成功，回傳驗證結果
        /// </summary>
        /// <typeparam name="TResult">需驗證的泛型</typeparam>
        /// <param name="targetString">來源字串物件</param>
        /// <param name="target">轉型後輸出的物件</param>
        /// <param name="message">錯誤訊息</param>
        /// <param name="handler">處理器</param>
        /// <returns>回傳驗證結果</returns>
        protected ValidationResult CheckTargetType<TResult>(string targetString, out TResult target,
            string message, TryParseHandler<TResult> handler)
        {
            var result = new ValidationResult();

            if (!handler(targetString, out target))
            {
                result.IsValid = false;
                result.Message = Message.IsNullOrEmpty() ? message : Message;
                return result;
            }

            result.IsValid = true;
            result.Message = "";
            return result;
        }

        /// <summary>驗證機制
        /// </summary>
        /// <param name="targetString">來源字串物件</param>
        /// <param name="defaultMessage">預設錯誤訊息</param>
        /// <returns>回傳驗證結果</returns>
        protected ValidationResult Validate(string targetString, string defaultMessage)
        {
            var result = new ValidationResult();
            var target = decimal.Parse(targetString);

            if (new string[] { Min, Max }.All(m => m.IsNullOrEmpty()))
            {
                result.IsValid = false;
                result.Message = $"The {nameof(NumberRangeAttribute)} property value is incorrect.";
                return result;
            }

            var minExpression = true;
            var maxExpression = true;

            if (Min.IsNotNullOrEmpty())
            {
                if (!decimal.TryParse(Min, out var min))
                {
                    result.IsValid = false;
                    result.Message = $"The {nameof(NumberRangeAttribute)} property value is incorrect. The value of Min property must be numerical value.";
                    return result;
                }

                minExpression = IncludeMin ? min <= target : min <= target;
            }

            if (Max.IsNotNullOrEmpty())
            {
                if (!decimal.TryParse(Max, out var max))
                {
                    result.IsValid = false;
                    result.Message = $"The {nameof(NumberRangeAttribute)} property value is incorrect. The value of Max property must be numerical value.";
                    return result;
                }

                maxExpression = IncludeMax ? target <= max : target < max;
            }

            if (minExpression && maxExpression)
            {
                result.IsValid = true;
                result.Message = "";
                return result;
            }

            result.IsValid = false;
            result.Message = Message.IsNullOrEmpty() ? defaultMessage : Message;
            return result;
        }

        #endregion 宣告受保護的方法
    }
}
