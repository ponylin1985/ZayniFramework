﻿using System;
using ZayniFramework.Common;


namespace ZayniFramework.Validation
{
    /// <summary>檢查是否為合法的身分證字號
    /// </summary>
    public class IsIdentityNoStringAttribute : ValidationAttribute
    {
        #region 實作資料驗證方法

        /// <summary>檢查身分證是否合法
        /// </summary>
        /// <param name="target">需要驗證身分證</param>
        /// <returns>回傳驗證結果</returns>
        public override ValidationResult DoValidate(object target)
        {
            var result = CheckIdentity(target + "");
            return result;
        }

        #endregion 實作資料驗證方法


        #region 宣告私有的驗證方法

        /// <summary>檢查身分證是否合法
        /// </summary>
        /// <param name="identityId">身分證字號</param>
        /// <returns>是否為合法的身分證字號</returns>
        private ValidationResult CheckIdentity(string identityId)
        {
            var result = new ValidationResult();
            var uid = new int[10];

            if (identityId.IsNullOrEmpty())
            {
                base.PrepareFailure(result, "身分證字號不可為空的");
                return result;
            }

            if (!CheckLength(identityId))
            {
                base.PrepareFailure(result, "身分證字號長度不對");
                return result;
            }

            for (var i = 1; i < identityId.Length; i++)
            {
                uid[i] = Convert.ToInt32(identityId.Substring(i, 1));
            }


            if (!CheckSex(uid))
            {
                base.PrepareFailure(result, "身分證字號第2位錯誤");
                return result;
            }

            //身分證字母轉成大寫
            identityId = identityId.ToUpper();

            if (!CheckIdentity(uid, identityId))
            {
                base.PrepareFailure(result, "身分證字號格式錯誤");
                return result;
            }

            result.IsValid = true;
            result.Message = string.IsNullOrWhiteSpace(Message) ? "身分證字號格式正確" : Message;
            return result;

        }

        /// <summary>檢查長度
        /// </summary>
        /// <param name="identityId">身分證字號</param>
        /// <returns>身分證長度是否合法</returns>
        private static bool CheckLength(string identityId)
        {
            return 10 == identityId.Length;
        }

        /// <summary>檢查第一個數值是否為1.2(判斷性別)
        /// </summary>
        /// <param name="uid">身分證字號</param>
        /// <returns>身分證第2個數字是否合法</returns>
        private static bool CheckSex(int[] uid)
        {
            return 1 == uid[1] || 2 == uid[1];
        }

        /// <summary>檢查身分證是否有效
        /// </summary>
        /// <param name="uid">身分證字號的陣列</param>
        /// <param name="identityId">身分證字號</param>
        /// <returns>是否為有效的身分證字號</returns>
        private static bool CheckIdentity(int[] uid, string identityId)
        {
            int chkTotal;

            //將開頭字母轉換為對應的數值
            switch (identityId.Substring(0, 1).ToUpper())
            {
                case "A": uid[0] = 10; break;
                case "B": uid[0] = 11; break;
                case "C": uid[0] = 12; break;
                case "D": uid[0] = 13; break;
                case "E": uid[0] = 14; break;

                case "F": uid[0] = 15; break;
                case "G": uid[0] = 16; break;
                case "H": uid[0] = 17; break;
                case "I": uid[0] = 34; break;
                case "J": uid[0] = 18; break;

                case "K": uid[0] = 19; break;
                case "L": uid[0] = 20; break;
                case "M": uid[0] = 21; break;
                case "N": uid[0] = 22; break;
                case "O": uid[0] = 35; break;

                case "P": uid[0] = 23; break;
                case "Q": uid[0] = 24; break;
                case "R": uid[0] = 25; break;
                case "S": uid[0] = 26; break;
                case "T": uid[0] = 27; break;

                case "U": uid[0] = 28; break;
                case "V": uid[0] = 29; break;
                case "W": uid[0] = 32; break;
                case "X": uid[0] = 30; break;
                case "Y": uid[0] = 31; break;

                case "Z": uid[0] = 33; break;
            }

            chkTotal = (uid[0] / 10 * 1) + (uid[0] % 10 * 9);
            var k = 8;

            for (var j = 1; j < 9; j++)
            {
                chkTotal += uid[j] * k;
                k--;
            }

            chkTotal += uid[9];

            if (0 != chkTotal % 10)
            {
                return false;
            }

            return true;
        }

        #endregion 宣告私有的驗證方法
    }
}
