﻿using System;
using ZayniFramework.Common;


namespace ZayniFramework.Validation
{
    /// <summary>檢查是否為日期格式的字串
    /// </summary>
    public class StringIsDateAttribute : ValidationAttribute
    {
        /// <summary>實作驗證機制
        /// </summary>
        /// <param name="field">需驗證物件</param>
        /// <returns>回傳驗證結果</returns>
        public override ValidationResult DoValidate(object field)
        {
            var result = new ValidationResult();

            var value = field + "";
            if (!DateTime.TryParse(value, out _))
            {
                result.IsValid = false;
                result.Message = string.IsNullOrWhiteSpace(Message) ? "日期格式不對" : Message;
                return result;
            }

            result.IsValid = true;
            return result;
        }
    }
}
