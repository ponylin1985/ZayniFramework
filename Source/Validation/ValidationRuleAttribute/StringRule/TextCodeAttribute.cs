﻿using System.Linq;
using ZayniFramework.Common;


namespace ZayniFramework.Validation
{
    /// <summary>字串代碼規則 (資料值必須是傳入ValidCodes中定義的字串，才是合法值)
    /// </summary>
    public class TextCodeValidationAttribute : ValidationAttribute
    {
        #region 宣告公開的屬性

        /// <summary>合法的字串代碼
        /// </summary>
        public string[] ValidCodes { get; set; }

        /// <summary>代碼檢查是否可以忽略大小寫
        /// </summary>
        public bool IsIgnoreCase { get; set; }

        #endregion 宣告公開的屬性


        #region 實作驗證方法

        /// <summary>資料驗證
        /// </summary>
        /// <param name="target">受檢資料模型</param>
        /// <returns>資料驗證結果</returns>
        public override ValidationResult DoValidate(object target)
        {
            var result = new ValidationResult()
            {
                IsValid = false,
                Message = null
            };

            if (target.IsNull())
            {
                result.Message = Message.IsNullOrEmptyString("資料不可以為 Null 值。");
                return result;
            }

            var t = target + "";

            // 20140829 Pony Says: 讓此規則可以接受空字串，如果不能接受空字串，可以搭配NotNullOrEmptyAttribute，或是不要在ValidCodes中設定空字串
            //if ( t.IsNullOrEmpty() )
            //{
            //    result.Message = Message.IsNullOrEmptyString( "資料不可以為Null值或空字串。" );
            //    return result;
            //}

            var checks = ValidCodes.ToList();

            if (IsIgnoreCase)
            {
                checks.Clear();
                ValidCodes.ToList().ForEach(m => checks.Add(m.ToUpper()));
                t = t.ToUpper();
            }

            result.IsValid = checks.Contains(t);

            if (!result.IsValid)
            {
                result.Message = Message.IsNullOrEmptyString("資料必須要包含 ValidCodes 中定義的字串。");
            }

            return result;
        }

        #endregion 實作驗證方法
    }
}
