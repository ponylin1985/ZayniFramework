﻿using NeoSmart.AsyncLock;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Resources;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.Validation
{
    /// <summary>資料驗證器
    /// </summary>
    public sealed class Validator
    {
        #region 宣告私有的常數

        /// <summary>非同步作業鎖定物件
        /// </summary>
        private static readonly AsyncLock _asyncLock = new();

        /// <summary>錯誤訊息的標題
        /// </summary>
        private static readonly string _logTitle = nameof(Validator);

        /// <summary>作業系統的換行符號
        /// </summary>
        private static readonly string _newline = Environment.NewLine;

        #endregion 宣告私有的常數


        #region 宣告靜態成員

        /// <summary>資料驗證器實體參考
        /// </summary>
        private static Validator _instance;

        /// <summary>資料驗證器獨體
        /// </summary>
        public static Validator Instance
        {
            get
            {
                using (_asyncLock.Lock())
                {
                    if (_instance.IsNull())
                    {
                        _instance = new Validator();
                        return _instance;
                    }
                }

                return _instance;
            }
        }

        #endregion 宣告靜態成員


        #region 宣告私有的建構子

        /// <summary>私有的建構子
        /// </summary>
        private Validator()
        {
            // pass
        }

        #endregion 宣告私有的建構子


        #region 宣告公開的屬性

        /// <summary>驗證失敗的回呼委派
        /// </summary>
        public FailureHandler FailCallback { get; set; }

        /// <summary>程式異常的回呼委派
        /// </summary>
        public ExceptionHandler ExceptionCallback { get; set; }

        #endregion 宣告公開的屬性


        #region 宣告公開方法

        /// <summary>執行資料模型驗證
        /// </summary>
        /// <param name="entry">資料驗證請求</param>
        /// <returns>資料驗證結果</returns>
        public ValidationResult Validate(ValidationEntry entry)
        {
            var result = new ValidationResult()
            {
                IsValid = false
            };

            string[] ignoreProperies = null;

            if (entry.IgnoreProperies.IsNotNull())
            {
                ignoreProperies = entry.IgnoreProperies.ToArray();
            }

            var isValid = Validate(entry.TargetModel, entry.Language, entry.IgnoreValitions, out var invalidMessages, out var message, ignoreProperies);

            result.IsValid = isValid;
            result.ValidationMessages = invalidMessages;
            result.Message = message;
            return result;
        }

        #endregion 宣告公開方法


        #region 宣告私有方法

        /// <summary>執行驗證並回傳是否成功
        /// </summary>
        /// <param name="model">目標資料模型</param>
        /// <param name="language">語系代碼</param>
        /// <param name="ignoreValitions">此次資料驗證要忽略的驗證規則</param>
        /// <param name="invalidMessages">驗證失敗的資訊集合</param>
        /// <param name="message">錯誤訊息</param>
        /// <param name="ignoreProperies">此次資料驗證可以忽略檢查的屬性名稱，傳入後將不會對這些屬性進行資料驗證</param>
        /// <returns>目標資料模型是否通過驗證</returns>
        private bool Validate(object model, string language, Dictionary<string, List<string>> ignoreValitions, out List<Dictionary<string, List<string>>> invalidMessages, out string message, params string[] ignoreProperies)
        {
            message = string.Empty;
            invalidMessages = [];

            if (model.IsNull())
            {
                message = "Target model object is null.";
                Logger.Error(this, message, _logTitle);
                HandlerExecuter.DoFailureHandler(FailCallback);
                return false;
            }

            var type = model.GetType();

            var properties = type.GetProperties(BindingFlags.Public |
                                                            BindingFlags.NonPublic |
                                                            BindingFlags.Instance);

            if (properties.IsNullOrEmpty())
            {
                return true;
            }

            var result = true;
            var logMessage = string.Empty;

            ValidationResult validateResult = null;

            foreach (var propertyInfo in properties)
            {
                // 20140813 Added by Pony: 外界可以決定每次的資料模行驗證忽略掉哪些屬性不進行檢查
                if (ignoreProperies.IsNotNullOrEmpty() && ignoreProperies.Contains(propertyInfo.Name))
                {
                    continue;
                }

                List<string> ignoreValidationRules = null;

                try
                {
                    if (null != ignoreValitions && 0 != ignoreValitions.Count)
                    {
                        ignoreValidationRules = ignoreValitions.Where(m => m.Key == propertyInfo.Name).FirstOrDefault().Value;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Exception(this, ex, _logTitle);
                    ignoreValidationRules = null;
                }

                var obj = propertyInfo.GetValue(model);
                validateResult = DoValidate(propertyInfo, language, obj, ignoreValidationRules);

                if (!validateResult.IsValid)
                {
                    result = false;
                    var line = message.IsNullOrEmpty() ? string.Empty : _newline;
                    message += $"{line}{validateResult.Message}";
                    logMessage += string.Format("{0}{1}Property : {2}{1}Message : {3}", line, _newline, propertyInfo.Name, validateResult.Message);
                    invalidMessages.Add(validateResult.InvalidMessages);
                }
            }

            if (!result)
            {
                Logger.Error(this, logMessage, _logTitle);
            }

            return result;
        }

        /// <summary>對所有有標記資料驗證 ValidationAttribute 的 Property 進形資料驗證。<para/>
        /// 反射 Property 屬性，有標示 BaseAttribute 就做資料驗證
        /// </summary>
        /// <param name="property">屬性資訊</param>
        /// <param name="language">語系代碼</param>
        /// <param name="obj">需驗證資料</param>
        /// <param name="ignoreValitionRules">此次資料驗證要忽略掉的驗證規則</param>
        /// <returns>回傳驗證結果</returns>
        private ValidationResult DoValidate(PropertyInfo property, string language, object obj, List<string> ignoreValitionRules = null)
        {
            var result = new ValidationResult()
            {
                IsValid = true,
                Message = null
            };

            var map = new Dictionary<string, List<string>>();

            foreach (var attribute in Attribute.GetCustomAttributes(property))
            {
                var validationAttribute = attribute as ValidationAttribute;

                if (validationAttribute.IsNull())
                {
                    continue;
                }

                var validationRuleName = GetValidationAttributeName(validationAttribute.GetType().Name);

                // 20140821 Added by Pony: 讓外界可以決定每一次資料驗證時，可以決定該屬性哪些驗證規則可以忽略!
                if (ignoreValitionRules.IsNotNullOrEmpty() && ignoreValitionRules.Contains(validationRuleName))
                {
                    continue;
                }

                var type = attribute.GetType();
                var constructor = type.GetConstructor(Type.EmptyTypes);

                if (constructor.IsNull())
                {
                    result.IsValid = false;
                    result.Message = $"Validation fail due to '{type.Name}' does not contain default no args constructor.";
                    Logger.Error(this, result.Message, _logTitle);
                    continue;
                }

                object classObject;

                try
                {
                    classObject = constructor.Invoke([]);
                }
                catch (Exception ex)
                {
                    result.IsValid = false;
                    result.Message = $"{type.Name} : 呼叫預設建構子出現Exception{_newline} Exception : {ex}";
                    Logger.Error(this, result.Message, _logTitle);
                    HandlerExecuter.DoExceptionHandler(ExceptionCallback, ex);
                    continue;
                }

                var properties = type.GetProperties(BindingFlags.Public |
                                                                BindingFlags.NonPublic |
                                                                BindingFlags.Instance);

                if (properties.IsNullOrEmpty())
                {
                    continue;
                }

                foreach (var propertyInfo in properties)
                {
                    if ("TypeId" == propertyInfo.Name)
                    {
                        continue;
                    }

                    var prop = propertyInfo.GetValue(attribute);
                    classObject.GetType().GetProperty(propertyInfo.Name).SetValue(classObject, prop);
                }

                ValidationResult validationResult = null;

                try
                {
                    var methodInfo = type.GetMethod("DoValidate");
                    validationResult = methodInfo.Invoke(classObject, [obj]) as ValidationResult;
                }
                catch (Exception ex)
                {
                    result.IsValid = false;
                    result.Message = $"{type.Name} : DoValidate方法出現 Exception{_newline} Exception : {ex}";
                    Logger.Error(this, result.Message, _logTitle);
                    HandlerExecuter.DoExceptionHandler(ExceptionCallback, ex);
                    continue;
                }

                if (validationResult.IsNull())
                {
                    result.IsValid = false;
                    result.Message = $"{type.Name} : DoValidate 方法沒有正確回傳 ValidationResult 型別物件，資料驗證失敗。";
                    Logger.Error(this, result.Message, _logTitle);
                    HandlerExecuter.DoFailureHandler(FailCallback);
                    continue;
                }

                if (!validationResult.IsValid)
                {
                    result.IsValid = false;

                    if (validationAttribute.ResourceType.IsNotNull() && validationAttribute.ResourceKey.IsNotNullOrEmpty())
                    {
                        var rm = new ResourceManager(validationAttribute.ResourceType);
                        validationResult.Message = rm.GetString(validationAttribute.ResourceKey, CultureInfo.CreateSpecificCulture(language));
                    }

                    if (validationResult.Message.IsNotNullOrEmpty())
                    {
                        var line = result.Message.IsNullOrEmpty() ? string.Empty : _newline;
                        result.Message += $"{line}{validationResult.Message}";
                    }

                    if (!result.InvalidMessages.TryGetValue(property.Name, out var value))
                    {
                        var info = new List<string>
                        {
                            validationResult.Message
                        };

                        result.InvalidMessages.Add(property.Name, info);
                    }
                    else
                    {
                        var info = value;
                        info.Add(validationResult.Message);
                    }
                }
            }

            return result;
        }

        /// <summary>取得資料驗證規則 Attribute 的名稱 (去除掉後綴字Attribute)
        /// </summary>
        /// <param name="attributeName">Attribute的類別名稱</param>
        /// <returns>資料驗證規則Attribute的名稱 (去除掉後綴字Attribute)</returns>
        private string GetValidationAttributeName(string attributeName)
        {
            try
            {
                return attributeName.Remove(attributeName.IndexOf("Attribute"));
            }
            catch (Exception ex)
            {
                Logger.Exception(this, ex, _logTitle);
                return attributeName;
            }
        }

        #endregion 宣告私有方法
    }
}
