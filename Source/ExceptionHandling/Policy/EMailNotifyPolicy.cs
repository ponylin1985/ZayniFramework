﻿using ZayniFramework.Common;


namespace ZayniFramework.ExceptionHandling
{
    /// <summary>ZayniFramework 框架的EMail通知例外處理政策
    /// </summary>
    public class EMailNotifyPolicy : ExceptionPolicy
    {
        /// <summary>預設文字檔Log路徑
        /// </summary>
        private const string DEFAULT_LOG_PATH = @"C:\ZayniFramework\Log\Exception.log";

        /// <summary>預設建構子
        /// </summary>
        public EMailNotifyPolicy()
        {
            Initialize();
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="policyName">政策名稱</param>
        public EMailNotifyPolicy(string policyName)
        {
            Initialize(policyName);
        }

        /// <summary>初始化
        /// </summary>
        /// <param name="policyName">政策名稱</param>
        public void Initialize(string policyName = "")
        {
            ConfigReader.LoadEMailNotifyPolicySettings();

            base.PolicyName = policyName.IsNullOrEmpty() ? "EMailNotifyPolicy" : policyName;
            base.IsEnable = EMailNotifyPolicySettings.IsEMailNotifyEnable;
            base.NeedRethrow = EMailNotifyPolicySettings.NeedRethrow;
            base.IsCustomPolicy = false;

            base.Handler = (exception, sender, message) =>
            {
                var path = DefaultPolicySettings.DefaultPolicyLogPath.IsNullOrEmptyString(DEFAULT_LOG_PATH, true);

                IExceptionHandler handler = null;

                #region 執行文字日誌記錄處理

                handler = ExceptionHandlerFactory.Create(ExceptionHandlerType.TextLogging);

                handler.HandleException(new
                {
                    LogPath = path,
                    LogTitle = message,
                    LogMessage = exception.Message,
                    Exception = exception,
                    IsEnable = EMailNotifyPolicySettings.IsTextLogEnable
                });

                #endregion 執行文字日誌記錄處理

                #region 執行事件檢視器紀錄處理

                handler = ExceptionHandlerFactory.Create(ExceptionHandlerType.EventLogging);

                handler.HandleException(new
                {
                    LogTitle = message,
                    LogMessage = exception.Message,
                    Exception = exception,
                    IsEnable = EMailNotifyPolicySettings.IsEventLogEnable
                });

                #endregion 執行事件檢視器紀錄處理

                #region 執行異常EMail通知信處理

                handler = ExceptionHandlerFactory.Create(ExceptionHandlerType.EMailNotify);

                handler.HandleException(new
                {
                    LogTitle = message,
                    LogMessage = exception.Message,
                    Exception = exception,
                    IsEnable = EMailNotifyPolicySettings.IsEMailNotifyEnable,
                    EMailNotifyPolicySettings.SmtpPort,
                    EMailNotifyPolicySettings.SmtpHost,
                    EnableSsl = EMailNotifyPolicySettings.IsEnableSsl,
                    EMailNotifyPolicySettings.SmtpDomain,
                    EMailNotifyPolicySettings.SmtpAccount,
                    EMailNotifyPolicySettings.SmtpPassword,
                    EMailNotifyPolicySettings.FromEmailAddress,
                    EMailNotifyPolicySettings.FromDisplayName,
                    EMailNotifyPolicySettings.ToEmailAddress,
                    EMailNotifyPolicySettings.MailSubject
                });

                #endregion 執行異常EMail通知信處理
            };
        }
    }
}
