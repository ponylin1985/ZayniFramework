﻿using ZayniFramework.Common;


namespace ZayniFramework.ExceptionHandling
{
    /// <summary>ZayniFramework 框架的程式異常處理政策
    /// </summary>
    public class ExceptionPolicy
    {
        /// <summary>異常處理政策名稱
        /// </summary>
        public string PolicyName
        {
            get;
            set;
        }

        /// <summary>是否啟用此政策
        /// </summary>
        public bool IsEnable
        {
            get;
            set;
        }

        /// <summary>是否需要將程式異常重新拋出 (由Config檔中設定)
        /// </summary>
        public bool NeedRethrow
        {
            get;
            set;
        }

        /// <summary>程式異常的完整型別名稱
        /// </summary>
        public string ExceptionTypeFullName
        {
            get;
            set;
        }

        /// <summary>程式異常型別的組件路徑
        /// </summary>
        public string ExceptionAssemblyPath
        {
            get;
            set;
        }

        /// <summary>例外處理器名稱
        /// </summary>
        public string ExceptionHandlerName
        {
            get;
            set;
        }

        /// <summary>是否為客製化政策
        /// </summary>
        public bool IsCustomPolicy
        {
            get;
            set;
        }

        /// <summary>ZayniFramework 框架的例外處理委派
        /// </summary>
        public ExceptionHandler Handler
        {
            get;
            set;
        }
    }
}
