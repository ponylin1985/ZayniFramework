﻿using System;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.ExceptionHandling
{
    /// <summary>ExceptionHandling 模組的 Config 設定讀取器
    /// </summary>
    internal static class ConfigReader
    {
        /// <summary>Zayni框架設定區段Config集合
        /// </summary>
        private static readonly ZayniFrameworkSettings _zayniConfig;

        /// <summary>靜態建構子
        /// </summary>
        static ConfigReader()
        {
            _zayniConfig = ConfigManager.GetZayniFrameworkSettings();

            if (_zayniConfig.IsNull())
            {
                var errorMsg = $"Load ZayniConfigSection from config file is null. Configuration error.";
                Logger.Error(nameof(ConfigReader), errorMsg, GetLogTitle($"{nameof(ConfigReader)} static ctor"));
                ConsoleLogger.LogError($"{nameof(ConfigReader)} static ctor {errorMsg}");
                throw new Exception(errorMsg);
            }
        }

        /// <summary>讀取 ZayniFramework 框架預設的例外處理政策設定值
        /// </summary>
        public static void LoadDefaultPolicySettings()
        {
            var logTitle = GetLogTitle(nameof(LoadDefaultPolicySettings));

            try
            {
                var config = _zayniConfig.ExceptionHandlingSettings.FileLoggingPolicySetting;
                DefaultPolicySettings.IsEnable = config.EnableDefaultPolicy;
                DefaultPolicySettings.IsEnableDefaultPolicyEventLog = config.EnableDefaultPolicyEventLog;
                DefaultPolicySettings.DefaultPolicyLogPath = config.DefaultPolicyLogFilePath;
                DefaultPolicySettings.NeedRethrow = config.EnableRethrow;
            }
            catch (Exception ex)
            {
                var errorMsg = $"Load ExceptionHandling/DefaultTextLoggingPolicy from config file occur exception. {ex}";
                Logger.Error(nameof(ConfigReader), errorMsg, logTitle);
                ConsoleLogger.LogError($"{logTitle} {errorMsg}");
                throw;
            }
        }

        /// <summary>讀取ZayniFramework 框架電子郵件通知例外處理政策設定值
        /// </summary>
        public static void LoadEMailNotifyPolicySettings()
        {
            var logTitle = GetLogTitle(nameof(LoadEMailNotifyPolicySettings));

            try
            {
                var config = _zayniConfig.ExceptionHandlingSettings.EmailNotifyPolicySetting;
                EMailNotifyPolicySettings.IsTextLogEnable = config.EnableTextLog;
                EMailNotifyPolicySettings.TextLogPath = config.TextLogPath;
                EMailNotifyPolicySettings.IsEventLogEnable = config.EnableEventLog;
                EMailNotifyPolicySettings.IsEMailNotifyEnable = config.EnableEmailNotify;
                EMailNotifyPolicySettings.SmtpPort = config.SmtpPort;
                EMailNotifyPolicySettings.SmtpHost = config.SmtpHost;
                EMailNotifyPolicySettings.IsEnableSsl = config.EnableSsl;
                EMailNotifyPolicySettings.SmtpDomain = config.SmtpDomain;
                EMailNotifyPolicySettings.SmtpAccount = config.SmtpAccount;
                EMailNotifyPolicySettings.SmtpPassword = config.SmtpPassword;
                EMailNotifyPolicySettings.FromEmailAddress = config.FromEmailAddress;
                EMailNotifyPolicySettings.FromDisplayName = config.FromDisplayName;
                EMailNotifyPolicySettings.ToEmailAddress = config.ToEmailAddress;
                EMailNotifyPolicySettings.MailSubject = config.MailSubject;
                EMailNotifyPolicySettings.NeedRethrow = config.EnableSsl;
            }
            catch (Exception ex)
            {
                var errorMsg = $"Load ExceptionHandling/EMailNotifyPolicy from config file occur exception. {ex}";
                Logger.Error(nameof(ConfigReader), errorMsg, logTitle);
                ConsoleLogger.LogError($"{logTitle} {errorMsg}");
                throw;
            }
        }

        /// <summary>取得日誌的標題
        /// </summary>
        /// <param name="methodName">方法名稱</param>
        /// <returns>日誌的標題</returns>
        private static string GetLogTitle(string methodName) => $"{nameof(ConfigReader)}.{methodName}";
    }
}
