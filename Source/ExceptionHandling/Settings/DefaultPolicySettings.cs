﻿namespace ZayniFramework.ExceptionHandling
{
    /// <summary>ZayniFramework 框架預設的例外處理政策設定值
    /// </summary>
    public static class DefaultPolicySettings
    {
        /// <summary>是否啟用 ZayniFramework 框架預設的例外處理政策的事件檢視器記錄功能
        /// </summary>
        public static bool IsEnableDefaultPolicyEventLog { get; internal set; }

        /// <summary>ZayniFramework 框架預設例外處理政策的文字Log檔路徑
        /// </summary>
        public static string DefaultPolicyLogPath { get; internal set; }

        /// <summary>是否啟用 ZayniFramework 框架預設例外處理政策
        /// </summary>
        public static bool IsEnable { get; internal set; }

        /// <summary>是否需要將程式異常重新拋出 (由Config檔中設定)
        /// </summary>
        public static bool NeedRethrow { get; internal set; }
    }
}
