﻿namespace ZayniFramework.ExceptionHandling
{
    /// <summary>例外處理政策工廠
    /// </summary>
    public static class ExceptionPolicyFactory
    {
        /// <summary>建立例外處理政策工廠
        /// </summary>
        /// <param name="policyType">處理政策代碼</param>
        /// <returns>程式例外處理政策</returns>
        public static ExceptionPolicy Create(string policyType = "")
        {
            var result = policyType.ToLower() switch
            {
                "defaulttextlog" => new DefaultTextLoggingPolicy(),
                "emailnotify" => new EMailNotifyPolicy(),
                _ => new ExceptionPolicy(),
            };
            return result;
        }
    }
}
