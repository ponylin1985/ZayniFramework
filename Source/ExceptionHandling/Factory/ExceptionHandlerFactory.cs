﻿namespace ZayniFramework.ExceptionHandling
{
    /// <summary>ZayniFramework 框架的例外處理器工廠
    /// </summary>
    public static class ExceptionHandlerFactory
    {
        /// <summary>建立程式例外處理器
        /// </summary>
        /// <param name="type">例外處理器種類</param>
        /// <returns>程式例外處理器</returns>
        public static IExceptionHandler Create(ExceptionHandlerType type)
        {
            IExceptionHandler result = type switch
            {
                ExceptionHandlerType.EventLogging => new EventLoggingHandler(),
                ExceptionHandlerType.TextLogging => new TextLoggingHandler(),
                ExceptionHandlerType.EMailNotify => new EMailNotifyHandler(),
                _ => new TextLoggingHandler(),
            };
            return result;
        }
    }
}
