﻿using System;
using System.Net;
using System.Net.Mail;
using System.Text;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.ExceptionHandling
{
    /// <summary>ZayniFramework 框架的電子郵件通知例外處理器
    /// </summary>
    public class EMailNotifyHandler : IExceptionHandler
    {
        #region 實作IExceptionHandler介面方法

        /// <summary>進行程式例外處理
        /// </summary>
        /// <param name="model">資料集合</param>
        public void HandleException(dynamic model)
        {
            #region 取得處理的參數

            bool isEnable = model.IsEnable;

            if (!isEnable)
            {
                return;
            }

            string title = model.LogTitle;
            string message = model.LogMessage;
            Exception exception = model.Exception;

            int smtpPort = model.SmtpPort;
            string smtpHost = model.SmtpHost;
            bool enableSsl = model.EnableSsl;
            string smtpDomain = model.SmtpDomain;
            string account = model.SmtpAccount;
            string password = model.SmtpPassword;

            string fromAddress = model.FromEmailAddress;
            string fromDisplay = model.FromDisplayName;
            string toAddress = model.ToEmailAddress;
            string mailSubject = model.MailSubject;

            #endregion 取得處理的參數

            #region 初始化SmtpClient

            var smtp = new SmtpClient
            {
                Port = smtpPort,
                Host = smtpHost,
                EnableSsl = enableSsl
            };

            if (account.IsNotNullOrEmpty() && password.IsNotNullOrEmpty())
            {
                smtp.Credentials = new NetworkCredential()
                {
                    Domain = smtpDomain.IsNotNullOrEmpty() ? smtpDomain : null,
                    UserName = account,
                    Password = password
                };
            }
            else
            {
                smtp.UseDefaultCredentials = true;
            }

            #endregion 初始化SmtpClient

            #region 初始化MailMessage

            var mail = new MailMessage()
            {
                From = fromDisplay.IsNullOrEmpty() ? new MailAddress(fromAddress) : new MailAddress(fromAddress, fromDisplay, Encoding.UTF8),
                Subject = mailSubject,
                Body = MakeExceptionMailMessageBody(exception, title, message)
            };

            AddMailAddress(mail, toAddress);

            #endregion 初始化MailMessage

            #region 寄送Exception Mail通知信

            try
            {
                smtp.Send(mail);
            }
            catch (SmtpException ex)
            {
                Logger.Exception(this, ex, $"ZayniFramework 框架寄送 Exception 通知信件發生異常: {ex}");
            }
            catch (Exception ex)
            {
                Logger.Exception(this, ex, $"ZayniFramework 框架寄送 Exception 通知信件發生異常: {ex}");
            }

            #endregion 寄送Exception Mail通知信
        }

        #endregion 實作IExceptionHandler介面方法


        #region 宣告私有的方法

        /// <summary>加入收件人電子郵件地址
        /// </summary>
        /// <param name="targetMail">目標MailMessage物件</param>
        /// <param name="toAddresses">電子郵件地址字串</param>
        private static void AddMailAddress(MailMessage targetMail, string toAddresses)
        {
            var addresses = toAddresses.SplitString(";", true);

            foreach (var address in addresses)
            {
                if (address.IsNullOrEmpty())
                {
                    continue;
                }

                targetMail.To.Add(address);
            }
        }

        /// <summary>產生程式異常電子郵件信件內容本文
        /// </summary>
        /// <param name="exception">異常物件</param>
        /// <param name="logTitle">日誌標題</param>
        /// <param name="logMessage">日誌訊息</param>
        /// <returns>程式異常電子郵件信件內容本文</returns>
        private static string MakeExceptionMailMessageBody(Exception exception, string logTitle, string logMessage)
        {
            var now = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff");
            var newLine = Environment.NewLine;
            var separator = "=============================================================================================================================";

            var time = $"Event Time: {now}";
            var title = $"Event Title: {logTitle}";
            var message = $"Event Message: {LogMessageMaker.MakeExceptionEventLogMessage(exception, logMessage)}";

            var messageContent = string.Format("{4}{0}{1}{0}{2}{0}{3}{0}{4}", newLine, time, title, message, separator);
            return messageContent;
        }

        #endregion 宣告私有的方法
    }
}
