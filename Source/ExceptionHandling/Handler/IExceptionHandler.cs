﻿namespace ZayniFramework.ExceptionHandling
{
    /// <summary>ZayniFramework 框架的例外處理介面
    /// </summary>
    public interface IExceptionHandler
    {
        /// <summary>進行程式例外處理
        /// </summary>
        /// <param name="model"></param>
        void HandleException(dynamic model);
    }
}
