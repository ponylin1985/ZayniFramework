﻿namespace ZayniFramework.ExceptionHandling
{
    /// <summary>程式例外處理種類
    /// </summary>
    public enum ExceptionHandlerType : int
    {
        /// <summary>文字日誌記錄例外處理
        /// </summary>
        TextLogging,

        /// <summary>事件檢視器例外處理
        /// </summary>
        EventLogging,

        /// <summary>EMail電子郵件例外處理
        /// </summary>
        EMailNotify
    }
}
