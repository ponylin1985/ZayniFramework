using Newtonsoft.Json;


namespace ZayniFramework.Middle.Service.HttpCore
{
    /// <summary>ASP.NET Core Self-Hosted 通訊組態設定
    /// </summary>
    public class HttpCoreProtocolConfig : ProtocolConfig
    {
        /// <summary>ASP.NET Core Http API 的 Base URL
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "hostBaseUrl")]
        public string HostBaseUrl { get; set; }

        /// <summary>ASP.NET Core Kestrel 的 Http 連線存活期間
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "keepAliveTimeout")]
        public string KeepAliveTimeout { get; set; }
    }
}