using Microsoft.AspNetCore.Mvc;
using System;
using System.Text.Json;
using System.Threading.Tasks;
using ZayniFramework.Logging;
using ZayniFramework.Middle.Service.Entity;
using ZayniFramework.Middle.Service.HttpCore.Entity;
using ZayniFramework.Serialization;


namespace ZayniFramework.Middle.Service.HttpCore
{
    /// <summary>動作控制器
    /// </summary>
    [Route("action")]
    [ApiController()]
    public sealed class ActionController
    {
        /// <summary>執行 Http RPC 非同步遠端程序呼叫
        /// </summary>
        /// <param name="reqPackage">請求資料包裹</param>
        /// <returns>回應資料包裹</returns>
        [HttpPost("async")]
        public async Task<HttpResponsePackage> PostAsync(HttpCoreRequestPackage reqPackage)
        {
            try
            {
                var dataContent = (JsonElement)reqPackage.Data;
                var payload = dataContent.ToString();

                var requestPackage = new RequestPackage()
                {
                    PackageType = reqPackage.PackageType,
                    ServiceName = reqPackage.ServiceName,
                    RequestId = reqPackage.RequestId,
                    ActionName = reqPackage.ActionName,
                    Data = payload,
                    RequestTime = reqPackage.RequestTime
                };

                // 第一種寫法:
                var remoteService = RemoteServiceContainer.Get(requestPackage.ServiceName);
                var responsePackage = await remoteService?.ExecuteAsync(requestPackage);

                var result = new HttpResponsePackage()
                {
                    PackageType = responsePackage.PackageType,
                    RequestId = responsePackage.RequestId,
                    ActionName = responsePackage.ActionName,
                    Success = responsePackage.Success,
                    Code = responsePackage.Code,
                    Message = responsePackage.Message
                };

                result.Data = JsonDocument.Parse(responsePackage.Data).RootElement;
                return result;
            }
            catch (Exception ex)
            {
                var errResponsePackage = new HttpResponsePackage()
                {
                    PackageType = PackageType.RPCResponse,
                    RequestId = reqPackage?.RequestId,
                    ActionName = reqPackage?.ActionName,
                    Code = StatusCode.INTERNAL_ERROR,
                    Message = $"ASP.NET Core HttpCore remote host occur internal exception. RequestId: {reqPackage.RequestId}, ActionName: {reqPackage.ActionName}, Request ServiceName: {reqPackage.ServiceName}."
                };

                Logger.Exception(this, ex, Logger.GetTraceLogTitle(this, nameof(PostAsync)), $"{errResponsePackage.Message}{Environment.NewLine}Http Request:{Environment.NewLine}{NewtonsoftJsonConvert.Serialize(reqPackage)}", "ServiceHostTrace");
                return errResponsePackage;
            }
        }

        /// <summary>執行 Http RPC 遠端程序呼叫
        /// </summary>
        /// <param name="reqPackage">請求資料包裹</param>
        /// <returns>回應資料包裹</returns>
        [HttpPost()]
        public HttpResponsePackage Post(HttpCoreRequestPackage reqPackage)
        {
            try
            {
                var dataContent = (JsonElement)reqPackage.Data;
                var payload = dataContent.ToString();

                var requestPackage = new RequestPackage()
                {
                    PackageType = reqPackage.PackageType,
                    ServiceName = reqPackage.ServiceName,
                    RequestId = reqPackage.RequestId,
                    ActionName = reqPackage.ActionName,
                    Data = payload,
                    RequestTime = reqPackage.RequestTime
                };

                // 第一種寫法:
                var remoteService = RemoteServiceContainer.Get(requestPackage.ServiceName);
                var responsePackage = remoteService?.Execute(requestPackage);

                var result = new HttpResponsePackage
                {
                    PackageType = responsePackage.PackageType,
                    RequestId = responsePackage.RequestId,
                    ActionName = responsePackage.ActionName,
                    Success = responsePackage.Success,
                    Code = responsePackage.Code,
                    Message = responsePackage.Message
                };

                result.Data = JsonDocument.Parse(responsePackage.Data).RootElement;
                return result;

                // 第二種寫法:
                // IRemoteHost remoteHost = new HttpCoreRemoteHost() { ServiceName = requestPackage.ServiceName };
                // return remoteHost.Execute( requestPackage );
            }
            catch (Exception ex)
            {
                var errResponsePackage = new HttpResponsePackage()
                {
                    PackageType = PackageType.RPCResponse,
                    RequestId = reqPackage?.RequestId,
                    ActionName = reqPackage?.ActionName,
                    Code = StatusCode.INTERNAL_ERROR,
                    Message = $"ASP.NET Core HttpCore remote host occur internal exception. RequestId: {reqPackage.RequestId}, ActionName: {reqPackage.ActionName}, Request ServiceName: {reqPackage.ServiceName}."
                };

                Logger.Exception(this, ex, Logger.GetTraceLogTitle(this, nameof(Post)), $"{errResponsePackage.Message}{Environment.NewLine}Http Request:{Environment.NewLine}{NewtonsoftJsonConvert.Serialize(reqPackage)}", "ServiceHostTrace");
                return errResponsePackage;
            }
        }
    }
}