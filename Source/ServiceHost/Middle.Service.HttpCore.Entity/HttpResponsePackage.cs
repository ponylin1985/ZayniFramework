using Newtonsoft.Json;
using System.Text.Json.Serialization;


namespace ZayniFramework.Middle.Service.HttpCore.Entity
{
    /// <summary>HttpCore extension module 的回應資料包裹
    /// </summary>
    public class HttpResponsePackage
    {
        /// <summary>包裹種類
        /// </summary>
        [JsonPropertyName("packageType")]
        [JsonProperty(PropertyName = "packageType")]
        public virtual string PackageType { get; set; }

        /// <summary>原始請求代碼
        /// </summary>
        [JsonPropertyName("reqId")]
        [JsonProperty(PropertyName = "reqId")]
        public virtual string RequestId { get; set; }

        /// <summary>動作名稱
        /// </summary>
        [JsonPropertyName("action")]
        [JsonProperty(PropertyName = "action")]
        public virtual string ActionName { get; set; }

        /// <summary>動作執行是否成功
        /// </summary>
        [JsonPropertyName("success")]
        [JsonProperty(PropertyName = "success")]
        public virtual bool Success { get; set; }

        /// <summary>資料集合
        /// </summary>
        [JsonPropertyName("data")]
        [JsonProperty(PropertyName = "data")]
        public virtual object Data { get; set; }

        /// <summary>動作執行結果代碼
        /// </summary>
        [JsonPropertyName("code")]
        [JsonProperty(PropertyName = "code")]
        public virtual string Code { get; set; }

        /// <summary>執行結果訊息
        /// </summary>
        [JsonPropertyName("message")]
        [JsonProperty(PropertyName = "message")]
        public virtual string Message { get; set; }
    }
}