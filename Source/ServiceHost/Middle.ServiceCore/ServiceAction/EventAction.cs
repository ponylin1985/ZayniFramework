﻿using ZayniFramework.Common;


namespace ZayniFramework.Middle.Service
{
    /// <summary>服務事件動作基底
    /// </summary>
    /// <typeparam name="TMessage">事件訊息載體的泛型</typeparam>
    public abstract class EventAction<TMessage> : ServiceAction<TMessage, Result>
    {
        #region 宣告建構子

        /// <summary>多載建構子
        /// </summary>
        /// <param name="actionName">動作名稱</param>
        public EventAction(string actionName) : base(actionName)
        {
            // pass
        }

        #endregion 宣告建構子
    }
}
