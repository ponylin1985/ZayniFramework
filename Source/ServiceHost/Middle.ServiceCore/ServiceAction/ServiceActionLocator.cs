using Microsoft.Extensions.DependencyInjection;
using System;
using ZayniFramework.Common;



namespace ZayniFramework.Middle.Service
{
    /// <summary>服務動作定位器
    /// </summary>
    public static class ServiceActionLocator
    {
        #region Internal Properties

        /// <summary>服務動作集合
        /// </summary>
        /// <value></value>
        internal static IServiceCollection ServiceCollection { get; private set; } = new ServiceCollection();

        /// <summary>服務容器供應者
        /// </summary>
        /// <value></value>
        internal static IServiceProvider ServiceProvider { get; private set; }

        #endregion Internal Properties


        #region Internal Methods

        /// <summary>註冊 ServiceAction 服務動作
        /// </summary>
        /// <param name="serviceActionType">服務動作的型別</param>
        internal static void AddServiceAction(Type serviceActionType) =>
            ServiceCollection.AddTransient(serviceActionType);

        /// <summary>取得 ServiceAction 服務動作實作
        /// </summary>
        /// <param name="serviceActionType">服務動作的型別</param>
        /// <returns>ServiceAction 服務動作實作</returns>
        internal static object ResolveServiceAction(Type serviceActionType)
        {
            ServiceProvider.IsNull(() => ServiceProvider = ServiceCollection.BuildServiceProvider());
            return ServiceProvider.GetService(serviceActionType);
        }

        #endregion Internal Methods


        #region Public Methods

        /// <summary>取得 ServiceAction 服務動作實作
        /// </summary>
        /// <typeparam name="TServiceAction">服務動作的泛型</typeparam>
        /// <returns>ServiceAction 服務動作實作</returns>
        public static TServiceAction ResolveServiceAction<TServiceAction>()
        {
            ServiceProvider.IsNull(() => ServiceProvider = ServiceCollection.BuildServiceProvider());
            return ServiceProvider.GetService<TServiceAction>();
        }

        #endregion Public Methods
    }
}