﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NeoSmart.AsyncLock;
using ZayniFramework.Common;
using ZayniFramework.Logging;
using ZayniFramework.Middle.Service.Entity;


namespace ZayniFramework.Middle.Service
{
    /// <summary>服務動作容器管理員
    /// </summary>
    public static class ServiceActionContainerManager
    {
        #region 宣告私有的欄位

        /// <summary>非同步作業鎖定物件
        /// </summary>
        /// <returns></returns>
        private static readonly AsyncLock _asyncLock = new();

        /// <summary>服務的設定名稱集合
        /// </summary>
        private static readonly string[] _serviceNames;

        /// <summary>
        /// Key值: 服務的設定名稱<para/>
        /// Value值: ServiceActionContainer 服務動作的管理容器個體
        /// </summary>
        private static readonly Dictionary<string, ServiceActionContainer> _containers = [];

        #endregion 宣告私有的欄位


        #region 宣告靜態建構子

        /// <summary>靜態建構子
        /// </summary>
        static ServiceActionContainerManager()
        {
            using (_asyncLock.Lock())
            {
                try
                {
                    _serviceNames = GlobalContext.Config.ServiceHosts?.Select(s => s.ServiceName)?.ToArray();
                }
                catch (Exception ex)
                {
                    Logger.Exception(nameof(ServiceActionContainerManager), ex, "Get all service name from serviceHostConfig.json occur exception.", "ServiceHostTrace");
                    return;
                }

                if (_serviceNames.IsNullOrEmpty())
                {
                    _serviceNames = [];
                    return;
                }

                foreach (var serviceName in _serviceNames)
                {
                    if (serviceName.IsNullOrEmpty())
                    {
                        continue;
                    }

                    var container = new ServiceActionContainer(serviceName);

                    if (_containers.ContainsKey(serviceName))
                    {
                        continue;
                    }

                    try
                    {
                        _containers.Add(serviceName, container);
                    }
                    catch (Exception ex)
                    {
                        Logger.Exception(nameof(ServiceActionContainerManager), ex, $"Add '{serviceName}' container to pool occur exception.", "ServiceHostTrace");
                        continue;
                    }
                }
            }
        }

        #endregion 宣告靜態建構子


        #region 宣告公開的方法

        /// <summary>取得服務動作容器的狀態資訊
        /// </summary>
        /// <param name="serviceName">服務的設定名稱</param>
        /// <returns>取得結果</returns>
        public static Result<dynamic> GetServiceActionsInfo(string serviceName)
        {
            var result = Result.Create<dynamic>();

            if (serviceName.IsNullOrEmpty())
            {
                result.Message = $"Invalid argument '{nameof(serviceName)}'. Can not be null or empty string.";
                result.Code = StatusCode.INVALID_REQUEST;
                Logger.Error(nameof(ServiceActionContainerManager), result.Message, nameof(GetServiceActionsInfo), "ServiceHostTrace");
                return result;
            }

            if (!_serviceNames.Contains(serviceName))
            {
                result.Message = $"Invalid argument '{nameof(serviceName)}'. ServiceName is not config. ServiceName: {serviceName}.";
                result.Code = StatusCode.INVALID_REQUEST;
                Logger.Error(nameof(ServiceActionContainerManager), result.Message, nameof(GetServiceActionsInfo), "ServiceHostTrace");
                return result;
            }

            if (!GetContainer(serviceName, out var container))
            {
                result.Message = $"No service action container from container pool. ServiceName: {serviceName}.";
                result.Code = StatusCode.INVALID_REQUEST;
                Logger.Error(nameof(ServiceActionContainerManager), result.Message, nameof(GetServiceActionsInfo), "ServiceHostTrace");
                return result;
            }

            if (container.IsNull())
            {
                result.Message = $"Retirve service action container object is null. ServiceName: {serviceName}.";
                result.Code = StatusCode.INVALID_REQUEST;
                Logger.Error(nameof(ServiceActionContainerManager), result.Message, nameof(GetServiceActionsInfo), "ServiceHostTrace");
                return result;
            }

            return container.GetServiceActionsInfo();
        }

        /// <summary>對 serviceHostConfig.json 組態中 autoStart = true 的服務，註冊服務動作
        /// </summary>
        /// <param name="actionName">服務動作名稱</param>
        /// <param name="typeOfAction">服務動作的型別</param>
        /// <param name="configPath">serviceHostConfig.json 設定的的路徑</param>
        /// <returns>註冊結果</returns>
        public static IResult RegisterServiceAction(string actionName, Type typeOfAction, string configPath = "./serviceHostConfig.json")
        {
            var result = Result.Create();

            if (GlobalContext.Config.IsNull())
            {
                dynamic cr = ConfigReader.GetConfig(configPath);

                if (null == cr || !cr.Success)
                {
                    result.Code = StatusCode.INTERNAL_ERROR;
                    result.Message = $"Load serviceHostConfig.json occur error. {cr.Message}";
                    Logger.Error(nameof(ServiceActionContainerManager), result.Message, nameof(RegisterServiceAction), "ServiceHostTrace");
                    return result;
                }

                GlobalContext.Config = cr.Data;
                GlobalContext.JsonConfig = cr.JConfig;
            }

            var serviceHostCfgs = GlobalContext.Config?.ServiceHosts?.Where(h => h.AutoStart);

            if (serviceHostCfgs.IsNullOrEmpty())
            {
                result.Code = StatusCode.INTERNAL_ERROR;
                result.Message = $"";
                Logger.Error(nameof(ServiceActionContainerManager), result.Message, nameof(RegisterServiceAction), "ServiceHostTrace");
                return result;
            }

            foreach (var serviceHostCfg in serviceHostCfgs)
            {
                var r = Add(serviceHostCfg.ServiceName, actionName, typeOfAction);

                if (!r.Success)
                {
                    return r;
                }
            }

            result.Success = true;
            return result;
        }

        /// <summary>新增服務動作
        /// </summary>
        /// <param name="serviceName">服務的設定名稱</param>
        /// <param name="actionName">服務動作名稱</param>
        /// <param name="typeOfAction">服務動作的型別</param>
        /// <returns>新增動作結果</returns>
        public static Result Add(string serviceName, string actionName, Type typeOfAction)
        {
            var result = Result.Create();

            try
            {
                using (_asyncLock.Lock())
                {
                    if (serviceName.IsNullOrEmpty())
                    {
                        result.Message = $"Invalid argument '{nameof(serviceName)}'. Can not be null or empty string.";
                        result.Code = StatusCode.INVALID_REQUEST;
                        Logger.Error(nameof(ServiceActionContainerManager), result.Message, nameof(Add), "ServiceHostTrace");
                        return result;
                    }

                    if (!_serviceNames.Contains(serviceName))
                    {
                        result.Message = $"Invalid argument '{nameof(serviceName)}'. ServiceName is not config. ServiceName: {serviceName}.";
                        result.Code = StatusCode.INVALID_REQUEST;
                        Logger.Error(nameof(ServiceActionContainerManager), result.Message, nameof(Add), "ServiceHostTrace");
                        return result;
                    }

                    if (!GetContainer(serviceName, out var container))
                    {
                        result.Message = $"No service action container from container pool. ServiceName: {serviceName}.";
                        result.Code = StatusCode.INVALID_REQUEST;
                        Logger.Error(nameof(ServiceActionContainerManager), result.Message, nameof(Add), "ServiceHostTrace");
                        return result;
                    }

                    if (container.IsNull())
                    {
                        result.Message = $"Retirve service action container object is null. ServiceName: {serviceName}.";
                        result.Code = StatusCode.INVALID_REQUEST;
                        Logger.Error(nameof(ServiceActionContainerManager), result.Message, nameof(Add), "ServiceHostTrace");
                        return result;
                    }

                    var r = container.Add(actionName, typeOfAction);

                    if (!r.Success)
                    {
                        result.Code = r.Code;
                        result.Message = r.Message;
                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(nameof(ServiceActionContainerManager), ex, "Add service action to container occur exception.", "ServiceHostTrace");
                return result;
            }

            result.Success = true;
            return result;
        }

        /// <summary>新增服務動作
        /// </summary>
        /// <param name="serviceName">服務的設定名稱</param>
        /// <param name="actionName">服務動作名稱</param>
        /// <param name="typeOfAction">服務動作的型別</param>
        /// <returns>新增動作結果</returns>
        public static async Task<Result> AddAsync(string serviceName, string actionName, Type typeOfAction)
        {
            var result = Result.Create();

            try
            {
                using (await _asyncLock.LockAsync())
                {
                    if (serviceName.IsNullOrEmpty())
                    {
                        result.Message = $"Invalid argument '{nameof(serviceName)}'. Can not be null or empty string.";
                        result.Code = StatusCode.INVALID_REQUEST;
                        Logger.Error(nameof(ServiceActionContainerManager), result.Message, nameof(AddAsync), "ServiceHostTrace");
                        return result;
                    }

                    if (!_serviceNames.Contains(serviceName))
                    {
                        result.Message = $"Invalid argument '{nameof(serviceName)}'. ServiceName is not config. ServiceName: {serviceName}.";
                        result.Code = StatusCode.INVALID_REQUEST;
                        Logger.Error(nameof(ServiceActionContainerManager), result.Message, nameof(AddAsync), "ServiceHostTrace");
                        return result;
                    }

                    if (!GetContainer(serviceName, out var container))
                    {
                        result.Message = $"No service action container from container pool. ServiceName: {serviceName}.";
                        result.Code = StatusCode.INVALID_REQUEST;
                        Logger.Error(nameof(ServiceActionContainerManager), result.Message, nameof(AddAsync), "ServiceHostTrace");
                        return result;
                    }

                    if (container.IsNull())
                    {
                        result.Message = $"Retirve service action container object is null. ServiceName: {serviceName}.";
                        result.Code = StatusCode.INVALID_REQUEST;
                        Logger.Error(nameof(ServiceActionContainerManager), result.Message, nameof(AddAsync), "ServiceHostTrace");
                        return result;
                    }

                    var r = await container.AddAsync(actionName, typeOfAction);

                    if (!r.Success)
                    {
                        result.Code = r.Code;
                        result.Message = r.Message;
                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(nameof(ServiceActionContainerManager), ex, "Add service action to container occur exception.", "ServiceHostTrace");
                return result;
            }

            result.Success = true;
            return result;
        }

        #endregion 宣告公開的方法


        #region 宣告內部的方法

        /// <summary>取得服務動作的容器
        /// </summary>
        /// <param name="serviceName">服務的設定名稱</param>
        /// <param name="container">服務動作的容器</param>
        /// <returns>是否成功取得服務動作的容器</returns>
        internal static bool GetContainer(string serviceName, out ServiceActionContainer container) => _containers.TryGetValue(serviceName, out container);

        /// <summary>取得服務動作
        /// </summary>
        /// <param name="serviceName">服務的設定名稱</param>
        /// <param name="actionName">動作名稱</param>
        /// <returns>服務動作</returns>
        internal static ServiceAction GetServiceAction(string serviceName, string actionName)
        {
            if (serviceName.IsNullOrEmpty())
            {
                Logger.Error(nameof(ServiceActionContainerManager), $"Invalid argument '{nameof(serviceName)}'. Can not be null or empty string.", nameof(GetServiceAction), "ServiceHostTrace");
                return null;
            }

            if (!_serviceNames.Contains(serviceName))
            {
                Logger.Error(nameof(ServiceActionContainerManager), $"Invalid argument '{nameof(serviceName)}'. ServiceName is not config. ServiceName: {serviceName}.", nameof(GetServiceAction), "ServiceHostTrace");
                return null;
            }

            if (!GetContainer(serviceName, out var container))
            {
                Logger.Error(nameof(ServiceActionContainerManager), $"No service action container from container pool. ServiceName: {serviceName}.", nameof(GetServiceAction), "ServiceHostTrace");
                return null;
            }

            if (container.IsNull())
            {
                Logger.Error(nameof(ServiceActionContainerManager), $"Retirve service action container object is null. ServiceName: {serviceName}.", nameof(GetServiceAction), "ServiceHostTrace");
                return null;
            }

            return container.Get(actionName);
        }

        /// <summary>取得服務動作
        /// </summary>
        /// <param name="serviceName">服務的設定名稱</param>
        /// <param name="actionName">動作名稱</param>
        /// <returns>服務動作</returns>
        internal static async Task<ServiceActionAsync> GetServiceActionAsync(string serviceName, string actionName)
        {
            if (serviceName.IsNullOrEmpty())
            {
                Logger.Error(nameof(ServiceActionContainerManager), $"Invalid argument '{nameof(serviceName)}'. Can not be null or empty string.", nameof(GetServiceActionAsync), "ServiceHostTrace");
                return null;
            }

            if (!_serviceNames.Contains(serviceName))
            {
                Logger.Error(nameof(ServiceActionContainerManager), $"Invalid argument '{nameof(serviceName)}'. ServiceName is not config. ServiceName: {serviceName}.", nameof(GetServiceActionAsync), "ServiceHostTrace");
                return null;
            }

            if (!GetContainer(serviceName, out var container))
            {
                Logger.Error(nameof(ServiceActionContainerManager), $"No async service action container from container pool. ServiceName: {serviceName}.", nameof(GetServiceActionAsync), "ServiceHostTrace");
                return null;
            }

            if (container.IsNull())
            {
                Logger.Error(nameof(ServiceActionContainerManager), $"Retirve async service action container object is null. ServiceName: {serviceName}.", nameof(GetServiceActionAsync), "ServiceHostTrace");
                return null;
            }

            return await container.GetAsync(actionName);
        }

        #endregion 宣告內部的方法
    }
}
