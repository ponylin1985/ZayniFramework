﻿using System;


namespace ZayniFramework.Middle.Service
{
    /// <summary>服務動作基底
    /// </summary>
    public abstract class ServiceAction
    {
        #region Constructors

        /// <summary>多載建構子
        /// </summary>
        /// <param name="actionName">動作名稱</param>
        /// <param name="requestType">請求資料的型別</param>
        public ServiceAction(string actionName, Type requestType)
        {
            Name = actionName;
            RequestType = requestType;
        }

        /// <summary>解構子
        /// </summary>
        ~ServiceAction()
        {
            Name = null;
            RequestType = null;
            RequestData = null;
            ResponseData = null;
        }

        #endregion Constructors


        #region 宣告公開的屬性

        /// <summary>動作名稱 or 事件名稱
        /// </summary>
        public string Name { get; private set; }

        #endregion 宣告公開的屬性


        #region 宣告內部的屬性

        /// <summary>請求資料的型別
        /// </summary>
        internal Type RequestType { get; private set; }

        /// <summary>請求的資料集合
        /// </summary>
        internal object RequestData { get; set; }

        /// <summary>回應的資料集合
        /// </summary>
        internal object ResponseData { get; set; }

        /// <summary>是否需要自動對請求參數集合進行資料驗證，程式預設為 true。
        /// </summary>
        internal bool RequestValidation { get; set; } = true;

        #endregion 宣告內部的屬性


        #region 宣告抽象

        /// <summary>執行動作
        /// </summary>
        public abstract void Execute();

        #endregion 宣告抽象
    }

    /// <summary>服務動作基底
    /// </summary>
    /// <typeparam name="TRequest">請求資料的泛型</typeparam>
    /// <typeparam name="TResponse">回應資料的泛型</typeparam>
    public abstract class ServiceAction<TRequest, TResponse> : ServiceAction
    {
        #region 宣告建構子

        /// <summary>多載建構子
        /// </summary>
        /// <param name="actionName">動作名稱</param>
        public ServiceAction(string actionName) : base(actionName, typeof(TRequest))
        {
        }

        #endregion 宣告建構子


        #region 宣告內部的屬性

        /// <summary>請求的資料集合
        /// </summary>
        protected TRequest Request => (TRequest)base.RequestData;

        /// <summary>回應的資料集合
        /// </summary>
        protected TResponse Response
        {
            get => (TResponse)base.ResponseData;
            set => base.ResponseData = value;
        }

        #endregion 宣告內部的屬性
    }
}
