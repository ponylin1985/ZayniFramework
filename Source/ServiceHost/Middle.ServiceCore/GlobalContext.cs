﻿namespace ZayniFramework.Middle.Service
{
    /// <summary>全域環境設定與變數
    /// </summary>
    public static class GlobalContext
    {
        /// <summary>組態設定值集合
        /// </summary>
        public static Config Config { get; set; }

        /// <summary>JSON 組態設定值集合
        /// </summary>
        /// <value></value>
        public static dynamic JsonConfig { get; set; }
    }
}
