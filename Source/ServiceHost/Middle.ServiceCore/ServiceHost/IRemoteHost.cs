﻿using ZayniFramework.Common;
using ZayniFramework.Middle.Service.Entity;


namespace ZayniFramework.Middle.Service
{
    /// <summary>服務託管基礎介面
    /// </summary>
    public interface IRemoteHost : IRemoteService
    {
        /// <summary>服務設定名稱
        /// </summary>
        string ServiceName { get; }

        /// <summary>服務託管 Host 的設定名稱
        /// </summary>
        string HostName { get; }

        /// <summary>遠端服務是否註冊與啟動完成
        /// </summary>
        bool IsHostReady { get; }

        /// <summary>初始化
        /// </summary>
        /// <param name="serviceName">服務設定名稱</param>
        void Initialize(string serviceName = null);

        /// <summary>啟動服務託管
        /// </summary>
        /// <returns>啟動結果</returns>
        Result StartHost();

        /// <summary>關閉服務託管
        /// </summary>
        /// <returns>關閉結果</returns>
        Result StopHost();
    }
}
