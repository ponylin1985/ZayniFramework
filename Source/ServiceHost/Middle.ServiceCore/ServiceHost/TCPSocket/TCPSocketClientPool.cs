﻿using NeoSmart.AsyncLock;
using System;
using System.Collections.Generic;
using System.Linq;
using ZayniFramework.Common;


namespace ZayniFramework.Middle.Service
{
    /// <summary>TCP Socket 連線客戶端連線池
    /// </summary>
    internal class TCPSocketClientPool
    {
        #region 宣告私有的欄位

        /// <summary>非同步作業鎖定物件
        /// </summary>
        private readonly AsyncLock _asyncLock = new();

        /// <summary>非同步作業鎖定物件
        /// </summary>
        private readonly AsyncLock _asyncLockMsg = new();

        /// <summary>訊息
        /// </summary>
        private string _message;

        /// <summary>TCP Socket 連線客戶端端點集合。<para/>
        /// Key值: ConnectionId TCP 連線代碼<para/>
        /// Value值: TCPSocketClientEndpoint 物件。
        /// </summary>
        private readonly Dictionary<string, TCPSocketClientEndpoint> _clients = [];

        #endregion 宣告私有的欄位


        #region 宣告內部的屬性

        /// <summary>訊息
        /// </summary>
        internal string Message
        {
            get
            {
                using (_asyncLockMsg.Lock())
                {
                    return _message;
                }
            }
            private set
            {
                using (_asyncLockMsg.Lock())
                {
                    _message = value;
                }
            }
        }

        /// <summary>TCP Socket 連線數量
        /// </summary>
        internal int Count
        {
            get
            {
                using (_asyncLock.Lock())
                {
                    return _clients.Count;
                }
            }
        }

        #endregion 宣告內部的屬性


        #region 宣告內部的方法

        /// <summary>加入 TCP Socket 客戶端連線
        /// </summary>
        /// <param name="connectionId">TCP Socket 的連線代碼</param>
        /// <param name="clientEndpoint">TCP Socket 客戶端連線端點</param>
        /// <returns>是否成功加入連線池</returns>
        internal bool Add(string connectionId, TCPSocketClientEndpoint clientEndpoint)
        {
            using (_asyncLock.Lock())
            {
                try
                {
                    if (_clients.ContainsKey(connectionId))
                    {
                        Message = $"ConnectionId already exists in TCP socket connection pool. ConnectionId: {connectionId}.";
                        return false;
                    }

                    if (clientEndpoint.IsNull())
                    {
                        Message = $"{nameof(TCPSocketClientEndpoint)} object is null. Not allowed add to TCP client connection pool.";
                        return false;
                    }

                    if (new object[] { clientEndpoint.TcpClient, clientEndpoint.NetworkStream }.HasNullElements())
                    {
                        Message = $"Property of {nameof(TCPSocketClientEndpoint)} object {nameof(TCPSocketClientEndpoint)}.{nameof(TCPSocketClientEndpoint.TcpClient)} or {nameof(TCPSocketClientEndpoint.NetworkStream)} is null. Not allowed add to TCP client connection pool.";
                        return false;
                    }

                    _clients.Add(connectionId, clientEndpoint);
                }
                catch (Exception ex)
                {
                    Message = $"Add TCP client connection to TCP socket connection pool occur exception. {Environment.NewLine}{ex}";
                    return false;
                }

                return true;
            }
        }

        /// <summary>移除 TCP Socket 客戶端連線
        /// </summary>
        /// <param name="connectionId">TCP 客戶端連線代碼</param>
        /// <returns>是否移除成功</returns>
        internal bool Remove(string connectionId)
        {
            using (_asyncLock.Lock())
            {
                try
                {
                    return _clients.Remove(connectionId);
                }
                catch (Exception ex)
                {
                    Message = $"Remove TCP client connection occur exception. ConnectionId: {connectionId}. {Environment.NewLine}{ex}";
                    return false;
                }
            }
        }

        /// <summary>清空所有 TCP 客戶端連線
        /// </summary>
        /// <returns>清空是否成功</returns>
        internal bool Clear()
        {
            using (_asyncLock.Lock())
            {
                try
                {
                    _clients.Clear();
                }
                catch (Exception ex)
                {
                    Message = $"Clear all TCP client connection occur exception. {Environment.NewLine}{ex}";
                    return false;
                }

                return true;
            }
        }

        /// <summary>對所有 TCP 客戶端連線進行迭代
        /// </summary>
        /// <param name="action">迭代動作的委派</param>
        internal bool Foreach(Action<string, TCPSocketClientEndpoint> action)
        {
            using (_asyncLock.Lock())
            {
                if (_clients.IsNullOrEmpty())
                {
                    return false;
                }

                foreach (var client in _clients)
                {
                    var connectionId = client.Key;
                    var tcpClientEndpoint = client.Value;

                    try
                    {
                        action(connectionId, tcpClientEndpoint);
                    }
                    catch (Exception ex)
                    {
                        Message = $"{nameof(Foreach)} TCP socket connection pool occur exception. {Environment.NewLine}{ex}";
                        return false;
                    }
                }

                return true;
            }
        }

        /// <summary>查詢 TCP 客戶端連線端點
        /// </summary>
        /// <param name="predicate">查詢條件委派</param>
        /// <returns>查詢結果</returns>
        internal IEnumerable<KeyValuePair<string, TCPSocketClientEndpoint>> Where(Func<KeyValuePair<string, TCPSocketClientEndpoint>, bool> predicate)
        {
            using (_asyncLock.Lock())
            {
                return _clients.Where(predicate);
            }
        }

        #endregion 宣告內部的方法
    }
}
