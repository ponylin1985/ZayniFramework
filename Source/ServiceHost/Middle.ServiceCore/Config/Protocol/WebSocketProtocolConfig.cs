﻿using Newtonsoft.Json;
using System.Collections.Generic;


namespace ZayniFramework.Middle.Service
{
    /// <summary>WebSocket 連線通訊組態設定
    /// </summary>

    public sealed class WebSocketProtocolConfig : ProtocolConfig
    {
        /// <summary>WebSocket 服務託管的連線 Base URL
        /// </summary>
        [JsonProperty(PropertyName = "hostBaseUrl")]
        public string HostBaseUrl { get; internal set; }

        /// <summary>WebSocket 客戶端的 IP 白名單 (如過設定為空陣列或 Null 則代表不限制任何 IP 都可以請求)
        /// </summary>
        [JsonProperty(PropertyName = "whitelist")]
        public List<string> Whitelist { get; internal set; }
    }
}
