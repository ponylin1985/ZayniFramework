﻿using Newtonsoft.Json;


namespace ZayniFramework.Middle.Service
{
    /// <summary>>Service Host 自我裝載服務的組態設定
    /// </summary>
    public sealed class ServiceHostConfig
    {
        /// <summary>服務設定名稱
        /// </summary>
        [JsonProperty(PropertyName = "serviceName")]
        public string ServiceName { get; internal set; }

        /// <summary>服務自我託管的模式<para/>
        /// .NET Remoting: 採用 .NET Framework 的 Remoting 服務。<para/>
        /// RabbitMQ: 採用 RabbitMQ AMQP 的事件監聽。<para/>
        /// TCPSocket: 採用 TCP Socket 通訊協定的網路串流的服務。<para/>
        /// WebSocekt: 採用 WebSocket 通訊協定的網路串流的服務。<para/>
        /// Http: 採用 Http 通訊協定的網路串流的服務。<para/>
        /// </summary>
        [JsonProperty(PropertyName = "remoteHostType")]
        public string RemoteHostType { get; internal set; }

        /// <summary>採用的 Host 自我裝載服務方式設定名稱
        /// </summary>
        [JsonProperty(PropertyName = "hostName")]
        public string HostName { get; set; }

        /// <summary>是否啟用主控台追蹤日誌
        /// </summary>
        [JsonProperty(PropertyName = "enableConsoleTracingLog")]
        public bool EnableConsoleTracingLog { get; internal set; }

        /// <summary>是否紀錄服務動作日誌於資料庫中
        /// </summary>
        [JsonProperty(PropertyName = "enableActionLogToDB")]
        public bool EnableActionLogToDB { get; internal set; }

        /// <summary>是否紀錄服務動作日誌於文字檔中
        /// </summary>
        [JsonProperty(PropertyName = "enableActionLogToFile")]
        public bool EnableActionLogToFile { get; internal set; }

        /// <summary>動作日誌紀錄在 ElasticSearch 服務的 ESLogger 設定名稱
        /// * 當設定值不為 Null 或空字串的情況下，會自動啟動指定的 ESLogger 日誌器進行紀錄。
        /// </summary>
        [JsonProperty(PropertyName = "esLoggerName")]
        public string ESLoggerName { get; internal set; }

        /// <summary>是否由 Zayni Framework Middle.Service module 框架內部自動啟動自我裝載服務<para/>
        /// 1. 預設值為 true。<para/>
        /// 2. 仍需要使用 MiddlewareService.StartServiceHosts( string path = null ) 方法，啟動 ServiceHost 服務。<para/>
        /// </summary>
        [JsonProperty(PropertyName = "autoStart")]
        public bool AutoStart { get; set; } = true;
    }
}
