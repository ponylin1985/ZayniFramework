﻿using Newtonsoft.Json;
using System.Collections.Generic;


namespace ZayniFramework.Middle.Service
{
    /// <summary>服務的組態設定值
    /// </summary>
    public sealed class Config
    {
        /// <summary>是否啟用主控台追蹤日誌
        /// </summary>
        [JsonProperty(PropertyName = "enableConsoleTracingLog")]
        public bool EnableConsoleTracingLog { get; internal set; }

        /// <summary>Service Host 服務託管設定集合
        /// </summary>
        [JsonProperty(PropertyName = "serviceHosts")]
        public List<ServiceHostConfig> ServiceHosts { get; internal set; }

        /// <summary>TCP Socket Host 服務組態設定
        /// </summary>
        [JsonProperty(PropertyName = "tcpSocketHostSettings")]
        public TCPSocketHostSettingConfig TCPSocketSettings { get; internal set; }

        /// <summary>WebSocket Host 服務組態設定
        /// </summary>
        [JsonProperty(PropertyName = "webSocketHostSettings")]
        public WebSocketHostSettingConfig WebSocketSettings { get; internal set; }

        /// <summary>RabbitMQ AMQP 服務組態設定
        /// </summary>
        [JsonProperty(PropertyName = "rabbitMQSettings")]
        public RabbitMQHostSettingConfig RabbitMQSettings { get; internal set; }
    }
}
