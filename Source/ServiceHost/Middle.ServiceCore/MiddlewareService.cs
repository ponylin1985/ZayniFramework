﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.Json;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Logging;
using ZayniFramework.Middle.Service.Entity;


namespace ZayniFramework.Middle.Service
{
    /// <summary>通訊中介層服務
    /// </summary>
    public static class MiddlewareService
    {
        #region Private Fields

        /// <summary>應用程式執行目錄的相依組件
        /// </summary>
        private static IEnumerable<Assembly> _assemblies;

        /// <summary>服務動作集合
        /// </summary>
        private static readonly List<ServiceAction> _actions = [];

        /// <summary>非同步服務動作集合
        /// </summary>
        /// <returns></returns>
        private static readonly List<ServiceActionAsync> _asyncActions = [];

        #endregion Private Fields


        #region Public Properties

        /// <summary>初始化服務動作容器的委派
        /// </summary>
        public static Func<IResult> InitializeServiceActionHandler { get; set; }

        #endregion Public Properties


        #region Public Methods

        /// <summary>註冊 IRemoteHost 型別到容器中
        /// </summary>
        /// <param name="remoteHostTypeName">自我裝載服務的種類名稱</param>
        /// <typeparam name="TRemoteHost">IRemoteHost 自我裝載服務的泛型</typeparam>
        /// <returns>註冊結果</returns>
        public static IResult RegisterRemoteHostType<TRemoteHost>(string remoteHostTypeName) where TRemoteHost : IRemoteHost =>
            RemoteHostFactory.RegisterRemoteHostType<TRemoteHost>(remoteHostTypeName);

        /// <summary>啟動 serviceHostConfig.json 組態設定中 autoStart = true 的 ServiceHost 自我裝載服務。<para/>
        /// 1. 執行啟動通訊服務之前，必須要明確指定 InitializeServiceActionHandler 委派屬性。<para/>
        /// 2. 並且於 InitializeServiceActionHandler 回呼中正確調用 ServiceActionContainer 初始化服務動作容器。<para/>
        /// 3. 相關通訊協定的連線設定，也必須於呼叫 StartServiceHosts() 方法呼叫前，正確的於 serviceHostConfig.json 組態。<para/>
        /// 4. 假若 serviceHostConfig.json 設定檔的位置不在 Entry Assembly 組件的相同目錄下，則需要自行明確傳入正確的 serviceHostConfig.json 在 runtime 環境下的完整路徑。
        /// </summary>
        /// <param name="serviceHostConfigPath">serviceHostConfig.json 設定檔的路徑</param>
        /// <param name="execPath">應用程式執行路徑</param>
        /// <returns>啟動結果</returns>
        public static IResult StartServiceHosts(string serviceHostConfigPath = "./serviceHostConfig.json", string execPath = null)
        {
            #region 讀取 Config 組態設定

            var result = Result.Create();

            if (GlobalContext.Config.IsNull())
            {
                dynamic cr = ConfigReader.GetConfig(serviceHostConfigPath);

                if (null == cr || !cr.Success)
                {
                    result.Code = StatusCode.INTERNAL_ERROR;
                    result.Message = $"Initialize process fail. Config error.";
                    Logger.Error(nameof(StartServiceHosts), result.Message, nameof(StartServiceHosts), "ServiceHostTrace");
                    ConsoleLogger.LogError(result.Message);
                    return result;
                }

                GlobalContext.Config = cr.Data;
                GlobalContext.JsonConfig = cr.JConfig;
            }

            var serviceHostCfgs = GlobalContext.Config?.ServiceHosts?.Where(h => h.AutoStart);

            if (serviceHostCfgs.IsNullOrEmpty())
            {
                result.Code = StatusCode.INTERNAL_ERROR;
                result.Message = $"No service host is config in autoStart = true in serviceHostConfig.json. No service host is started.";
                Logger.Error(nameof(StartServiceHosts), result.Message, nameof(StartServiceHosts), "ServiceHostTrace");
                ConsoleLogger.LogError(result.Message);
                return result;
            }

            #endregion 讀取 Config 組態設定

            #region 依序啟動 Config 組態中的 ServiceHost 自我裝載服務

            foreach (var serviceHostCfg in serviceHostCfgs)
            {
                var r = StartService(serviceHostCfg.ServiceName, serviceHostConfigPath, execPath);

                if (!r.Success)
                {
                    return r;
                }
            }

            #endregion 依序啟動 Config 組態中的 ServiceHost 自我裝載服務

            result.Success = true;
            return result;
        }

        /// <summary>啟動 serviceHostConfig.json 組態設定中 autoStart = true 的 ServiceHost 自我裝載服務。<para/>
        /// 1. 執行啟動通訊服務之前，必須要明確指定 InitializeServiceActionHandler 委派屬性。<para/>
        /// 2. 並且於 InitializeServiceActionHandler 回呼中正確調用 ServiceActionContainer 初始化服務動作容器。<para/>
        /// 3. 相關通訊協定的連線設定，也必須於呼叫 StartServiceHosts() 方法呼叫前，正確的於 serviceHostConfig.json 組態。<para/>
        /// 4. 假若 serviceHostConfig.json 設定檔的位置不在 Entry Assembly 組件的相同目錄下，則需要自行明確傳入正確的 serviceHostConfig.json 在 runtime 環境下的完整路徑。
        /// </summary>
        /// <param name="serviceHostConfigPath">serviceHostConfig.json 設定檔的路徑</param>
        /// <param name="execPath">應用程式執行路徑</param>
        /// <returns>啟動結果</returns>
        public static async Task<IResult> StartServiceHostsAsync(string serviceHostConfigPath = "./serviceHostConfig.json", string execPath = null)
        {
            #region 讀取 Config 組態設定

            var result = Result.Create();

            if (GlobalContext.Config.IsNull())
            {
                dynamic cr = ConfigReader.GetConfig(serviceHostConfigPath);

                if (null == cr || !cr.Success)
                {
                    result.Code = StatusCode.INTERNAL_ERROR;
                    result.Message = $"Initialize process fail. Config error.";
                    Logger.Error(nameof(StartServiceHostsAsync), result.Message, nameof(StartServiceHostsAsync), "ServiceHostTrace");
                    ConsoleLogger.LogError(result.Message);
                    return result;
                }

                GlobalContext.Config = cr.Data;
                GlobalContext.JsonConfig = cr.JConfig;
            }

            var serviceHostCfgs = GlobalContext.Config?.ServiceHosts?.Where(h => h.AutoStart);

            if (serviceHostCfgs.IsNullOrEmpty())
            {
                result.Code = StatusCode.INTERNAL_ERROR;
                result.Message = $"No service host is config in autoStart = true in serviceHostConfig.json. No service host is started.";
                Logger.Error(nameof(StartServiceHostsAsync), result.Message, nameof(StartServiceHostsAsync), "ServiceHostTrace");
                ConsoleLogger.LogError(result.Message);
                return result;
            }

            #endregion 讀取 Config 組態設定

            #region 依序啟動 Config 組態中的 ServiceHost 自我裝載服務

            foreach (var serviceHostCfg in serviceHostCfgs)
            {
                var r = await StartServiceAsync(serviceHostCfg.ServiceName, serviceHostConfigPath, execPath);

                if (!r.Success)
                {
                    return r;
                }
            }

            #endregion 依序啟動 Config 組態中的 ServiceHost 自我裝載服務

            result.Success = true;
            return result;
        }

        /// <summary>指定啟動 serviceHostConfig.json 組態設定的 ServiceHost 自我裝載服務。<para/>
        /// 1. 執行啟動通訊服務之前，必須要明確指定 InitializeServiceActionHandler 委派屬性。<para/>
        /// 2. 並且於 InitializeServiceActionHandler 回呼中正確調用 ServiceActionContainer 初始化服務動作容器。<para/>
        /// 3. 相關通訊協定的連線設定，也必須於呼叫 StartService() 方法呼叫前，正確的於 serviceHostConfig.json 組態。<para/>
        /// 4. 假若 serviceHostConfig.json 設定檔的位置不在 Entry Assembly 組件的相同目錄下，則需要自行明確傳入正確的 serviceHostConfig.json 在 runtime 環境下的完整路徑。
        /// </summary>
        /// <param name="serviceName">服務設定名稱</param>
        /// <param name="serviceHostConfigPath">serviceHostConfig.json 設定檔的路徑</param>
        /// <param name="execPath">應用程式執行路徑</param>
        /// <returns>啟動結果</returns>
        public static IResult StartService(string serviceName = "", string serviceHostConfigPath = "./serviceHostConfig.json", string execPath = null)
        {
            var result = Result.Create();

            #region 讀取 Config 組態設定

            if (GlobalContext.Config.IsNull())
            {
                dynamic cr = ConfigReader.GetConfig(serviceHostConfigPath);

                if (null == cr || !cr.Success)
                {
                    result.Code = StatusCode.INTERNAL_ERROR;
                    result.Message = $"Initialize process fail. Config error.";
                    Logger.Error(nameof(StartService), result.Message, nameof(StartService), "ServiceHostTrace");
                    ConsoleLogger.LogError(result.Message);
                    return result;
                }

                GlobalContext.Config = cr.Data;
                GlobalContext.JsonConfig = cr.JConfig;
            }

            #endregion 讀取 Config 組態設定

            #region 初始化服務動作容器

            // 當外界的程式沒有指定 InitializeServiceActionHandler 委派，則自行載入 assembly 動態的註冊 ServiceAction 服務動作
            if (InitializeServiceActionHandler.IsNull())
            {
                #region 動態載入 assembly 註冊 ServiceAction 動作

                var serviceActionType = typeof(ServiceAction);

                if (_actions.IsNotNullOrEmpty())
                {
                    foreach (var action in _actions)
                    {
                        var d = ServiceActionContainerManager.Add(serviceName, action.Name, action.GetType());

                        if (!d.Success)
                        {
                            result.Message = $"Add the '{action.Name}' ServiceAction instance into ServieActionConatiner occur error. {d.Message}";
                            Logger.Error(nameof(StartService), result.Message, nameof(StartService), "ServiceHostTrace");
                            ConsoleLogger.LogError(result.Message);
                            return result;
                        }
                    }
                }
                else
                {
                    if (_assemblies.IsNullOrEmpty())
                    {
                        if (execPath.IsNullOrEmpty())
                        {
                            // 20190417 Pony Bugfix: 這邊必須要改成是 .NET Core 或是 .NET Framework 應用程式 Main 方法進入點的組件所在路徑
                            // 原因是因為: 當其他的 .NET Project 使用 Package Reference 的時候，並且使用 dotnet run 的方式執行應用程式的時候，
                            // 如果是取得 GetExecutingAssembly() 的路徑，會誤判成在 /.nuget 的安裝路徑...
                            // 而因為這邊的測試專案，都是採用 Project Reference 的方式，所以沒有測出 Package Reference + dotnet run 情境下取得路徑誤判的 bug...
                            // 對不起，我真的錯了，有關於「路徑」的問題，我真的吃了很多虧...
                            execPath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
                        }

                        var gz = Reflector.LoadAssemblies(execPath);

                        if (!gz.Success)
                        {
                            result.Message = $"Load assemblies from {execPath} occur exception. {gz.Message}";
                            Logger.Error(nameof(StartService), result.Message, nameof(StartService), "ServiceHostTrace");
                            ConsoleLogger.LogError(result.Message);
                            return result;
                        }

                        _assemblies = gz.Data?
                            .Where(b => !b.FullName.StartsWith("System."))?
                            .Where(b => !b.FullName.StartsWith("Microsoft."))?
                            .Where(b => !b.FullName.StartsWith("MySql.Data."))?
                            .Where(b => !b.FullName.StartsWith("RabbitMQ.Client."))?
                            .Where(b => !b.FullName.StartsWith("Newtonsoft.Json."))?
                            .Where(b => !b.FullName.StartsWith("Nest."))?
                            .Where(b => !b.FullName.StartsWith("Elasticsearch.Net."))?
                            .Where(b => !b.FullName.StartsWith("K4os."))?
                            .Where(b => !b.FullName.StartsWith("NeoSmart.AsyncLock."))?
                            .Where(b => !b.FullName.StartsWith("Oracle."))?
                            .Where(b => !b.FullName.StartsWith("Renci."))?
                            .Where(b => !b.FullName.StartsWith("Zstandard.Net."))?
                            .Where(b => !b.FullName.StartsWith("Ubiety.Dns."))?
                            .Where(b => !b.FullName.StartsWith("Pipelines.Sockets.Unofficial."))?
                            .Where(b => !b.FullName.StartsWith("BouncyCastle.Crypto."))?
                            .Where(b => !b.FullName.StartsWith("StackExchange.Redis."))?
                            .Where(b => !b.FullName.StartsWith("SshNet.Security.Cryptography."))?
                            .Where(b => !b.FullName.StartsWith("websocket-sharp-core."))?
                            .Where(b => b.FullName != "Npgsql.dll")?
                            .Where(b => !b.FullName.StartsWith("ZayniFramework.Caching.RedisClient"))?
                            .Where(b => !b.FullName.Contains("ZayniFramework.Common"))?
                            .Where(b => !b.FullName.Contains("ZayniFramework.Commpression"))?
                            .Where(b => !b.FullName.Contains("ZayniFramework.Cryptography"))?
                            .Where(b => !b.FullName.Contains("ZayniFramework.DataAccess"))?
                            .Where(b => !b.FullName.Contains("ZayniFramework.ExceptionHandling"))?
                            .Where(b => !b.FullName.Contains("ZayniFramework.Formatting"))?
                            .Where(b => !b.FullName.Contains("ZayniFramework.Logging"))?
                            .Where(b => !b.FullName.Contains("ZayniFramework.RabbitMQ.Msg.Client"))?
                            .Where(b => !b.FullName.Contains("ZayniFramework.Serialization"))?
                            .Where(b => !b.FullName.Contains("ZayniFramework.Validation"))?
                            .Where(b => !b.FullName.Contains("ZayniFramework.Middle") || !b.FullName.StartsWith("ZayniFramework.Middle"))?
                            .Where(b => !b.FullName.Contains("Microsoft.") && !b.FullName.StartsWith("System."))?
                            .Where(b => !b.FullName.Contains("Google.") && !b.FullName.StartsWith("Google."))?
                            .Where(b => !b.FullName.Contains("Grpc.") && !b.FullName.StartsWith("Grpc."))?
                            .Where(b => !b.FullName.Contains("aspnetcore", StringComparison.CurrentCultureIgnoreCase))?
                            .Where(b => !b.FullName.Contains("aspnet", StringComparison.CurrentCultureIgnoreCase) && !b.FullName.Contains("asp.net", StringComparison.CurrentCultureIgnoreCase))?
                            .Where(b => b.GetTypes().Where(z => z != serviceActionType && serviceActionType.IsAssignableFrom(z)).IsNotNullOrEmpty());
                    }

                    if (_assemblies.IsNullOrEmpty())
                    {
                        result.Message = $"No assemblies found in application executing path: {execPath}.";
                        Logger.Error(nameof(StartService), result.Message, nameof(StartService), "ServiceHostTrace");
                        ConsoleLogger.LogError(result.Message);
                        return result;
                    }

                    foreach (var assembly in _assemblies)
                    {
                        var type2 = assembly.GetTypes();
                        var types = assembly.GetTypes().Where(z => z != serviceActionType && serviceActionType.IsAssignableFrom(z));

                        if (types.IsNullOrEmpty())
                        {
                            continue;
                        }

                        foreach (var type in types)
                        {
                            var c = CreateServiceActionInstance(serviceName, type);

                            if (!c.Success)
                            {
                                result.Message = c.Message;
                                return result;
                            }

                            if (c.Data.IsNull())
                            {
                                continue;
                            }

                            var action = c.Data;
                            _actions.Add(action);
                        }
                    }
                }

                #endregion 動態載入 assembly 註冊 ServiceAction 動作
            }
            else
            {
                #region 執行註冊 ServiceAction 的委派

                var g = InitializeServiceActionHandler();

                if (!g.Success)
                {
                    result.Code = StatusCode.INTERNAL_ERROR;
                    result.Message = $"Initialize process fail. Initialize the service action container fail. {g.Message}";
                    Logger.Error(nameof(StartService), result.Message, nameof(StartService), "ServiceHostTrace");
                    ConsoleLogger.LogError(result.Message);
                    return result;
                }

                #endregion 執行註冊 ServiceAction 的委派
            }

            #endregion 初始化服務動作容器

            #region 建立服務託管

            var r = RemoteHostContainer.Get(serviceName);

            if (!r.Success)
            {
                result.Code = StatusCode.INTERNAL_ERROR;
                result.Message = $"Initialize process fail. Create remote service host fail. {r.Message}";
                Logger.Error(nameof(StartService), result.Message, nameof(StartService), "ServiceHostTrace");
                ConsoleLogger.LogError(result.Message);
                return result;
            }

            var remoteHost = r.Data;

            #endregion 建立服務託管

            #region 啟動 Host 服務

            var s = remoteHost.StartHost();

            if (!s.Success)
            {
                result.Code = StatusCode.INTERNAL_ERROR;
                result.Message = $"Initialize process fail. Start remote service host fail. {s.Message}";
                Logger.Error(nameof(StartService), result.Message, nameof(StartService), "ServiceHostTrace");
                ConsoleLogger.LogError(result.Message);
                return result;
            }

            #endregion 啟動 Host 服務

            result.Success = true;
            return result;
        }

        /// <summary>指定啟動 serviceHostConfig.json 組態設定的 ServiceHost 自我裝載服務。<para/>
        /// 1. 執行啟動通訊服務之前，必須要明確指定 InitializeServiceActionHandler 委派屬性。<para/>
        /// 2. 並且於 InitializeServiceActionHandler 回呼中正確調用 ServiceActionContainer 初始化服務動作容器。<para/>
        /// 3. 相關通訊協定的連線設定，也必須於呼叫 StartService() 方法呼叫前，正確的於 serviceHostConfig.json 組態。<para/>
        /// 4. 假若 serviceHostConfig.json 設定檔的位置不在 Entry Assembly 組件的相同目錄下，則需要自行明確傳入正確的 serviceHostConfig.json 在 runtime 環境下的完整路徑。
        /// </summary>
        /// <param name="serviceName">服務設定名稱</param>
        /// <param name="serviceHostConfigPath">serviceHostConfig.json 設定檔的路徑</param>
        /// <param name="execPath">應用程式執行路徑</param>
        /// <returns>啟動結果</returns>
        public static async Task<IResult> StartServiceAsync(string serviceName = "", string serviceHostConfigPath = "./serviceHostConfig.json", string execPath = null)
        {
            var result = Result.Create();

            #region 讀取 Config 組態設定

            if (GlobalContext.Config.IsNull())
            {
                dynamic cr = ConfigReader.GetConfig(serviceHostConfigPath);

                if (null == cr || !cr.Success)
                {
                    result.Code = StatusCode.INTERNAL_ERROR;
                    result.Message = $"Initialize process fail. Config error.";
                    Logger.Error(nameof(StartServiceAsync), result.Message, nameof(StartServiceAsync), "ServiceHostTrace");
                    ConsoleLogger.LogError(result.Message);
                    return result;
                }

                GlobalContext.Config = cr.Data;
                GlobalContext.JsonConfig = cr.JConfig;
            }

            #endregion 讀取 Config 組態設定

            #region 初始化服務動作容器

            // 當外界的程式沒有指定 InitializeServiceActionHandler 委派，則自行載入 assembly 動態的註冊 ServiceAction 服務動作
            if (InitializeServiceActionHandler.IsNull())
            {
                #region 動態載入 assembly 註冊 ServiceAction 動作

                var serviceActionType = typeof(ServiceActionAsync);

                if (_asyncActions.IsNotNullOrEmpty())
                {
                    foreach (var action in _asyncActions)
                    {
                        var d = await ServiceActionContainerManager.AddAsync(serviceName, action.Name, action.GetType());

                        if (!d.Success)
                        {
                            result.Message = $"Add the '{action.Name}' ServiceAction instance into ServieActionConatiner occur error. {d.Message}";
                            Logger.Error(nameof(StartServiceAsync), result.Message, nameof(StartServiceAsync), "ServiceHostTrace");
                            ConsoleLogger.LogError(result.Message);
                            return result;
                        }
                    }
                }
                else
                {
                    if (_assemblies.IsNullOrEmpty())
                    {
                        if (execPath.IsNullOrEmpty())
                        {
                            // 20190417 Pony Bugfix: 這邊必須要改成是 .NET Core 或是 .NET Framework 應用程式 Main 方法進入點的組件所在路徑
                            // 原因是因為: 當其他的 .NET Project 使用 Package Reference 的時候，並且使用 dotnet run 的方式執行應用程式的時候，
                            // 如果是取得 GetExecutingAssembly() 的路徑，會誤判成在 /.nuget 的安裝路徑...
                            // 而因為這邊的測試專案，都是採用 Project Reference 的方式，所以沒有測出 Package Reference + dotnet run 情境下取得路徑誤判的 bug...
                            // 對不起，我真的錯了，有關於「路徑」的問題，我真的吃了很多虧...
                            execPath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
                        }

                        var gz = Reflector.LoadAssemblies(execPath);

                        if (!gz.Success)
                        {
                            result.Message = $"Load assemblies from {execPath} occur exception. {gz.Message}";
                            Logger.Error(nameof(StartServiceAsync), result.Message, nameof(StartServiceAsync), "ServiceHostTrace");
                            ConsoleLogger.LogError(result.Message);
                            return result;
                        }

                        _assemblies = gz.Data?
                            .Where(b => !b.FullName.StartsWith("System."))?
                            .Where(b => !b.FullName.StartsWith("Microsoft."))?
                            .Where(b => !b.FullName.StartsWith("MySql.Data."))?
                            .Where(b => !b.FullName.StartsWith("RabbitMQ.Client."))?
                            .Where(b => !b.FullName.StartsWith("Newtonsoft.Json."))?
                            .Where(b => !b.FullName.StartsWith("Nest."))?
                            .Where(b => !b.FullName.StartsWith("Elasticsearch.Net."))?
                            .Where(b => !b.FullName.StartsWith("K4os."))?
                            .Where(b => !b.FullName.StartsWith("NeoSmart.AsyncLock."))?
                            .Where(b => !b.FullName.StartsWith("Oracle."))?
                            .Where(b => !b.FullName.StartsWith("Renci."))?
                            .Where(b => !b.FullName.StartsWith("Zstandard.Net."))?
                            .Where(b => !b.FullName.StartsWith("Ubiety.Dns."))?
                            .Where(b => !b.FullName.StartsWith("Pipelines.Sockets.Unofficial."))?
                            .Where(b => !b.FullName.StartsWith("BouncyCastle.Crypto."))?
                            .Where(b => !b.FullName.StartsWith("StackExchange.Redis."))?
                            .Where(b => !b.FullName.StartsWith("SshNet.Security.Cryptography."))?
                            .Where(b => !b.FullName.StartsWith("websocket-sharp-core."))?
                            .Where(b => b.FullName != "Npgsql.dll")?
                            .Where(b => !b.FullName.StartsWith("ZayniFramework.Caching.RedisClient"))?
                            .Where(b => !b.FullName.Contains("ZayniFramework.Common"))?
                            .Where(b => !b.FullName.Contains("ZayniFramework.Commpression"))?
                            .Where(b => !b.FullName.Contains("ZayniFramework.Cryptography"))?
                            .Where(b => !b.FullName.Contains("ZayniFramework.DataAccess"))?
                            .Where(b => !b.FullName.Contains("ZayniFramework.ExceptionHandling"))?
                            .Where(b => !b.FullName.Contains("ZayniFramework.Formatting"))?
                            .Where(b => !b.FullName.Contains("ZayniFramework.Logging"))?
                            .Where(b => !b.FullName.Contains("ZayniFramework.RabbitMQ.Msg.Client"))?
                            .Where(b => !b.FullName.Contains("ZayniFramework.Serialization"))?
                            .Where(b => !b.FullName.Contains("ZayniFramework.Validation"))?
                            .Where(b => !b.FullName.Contains("ZayniFramework.Middle") || !b.FullName.StartsWith("ZayniFramework.Middle"))?
                            .Where(b => !b.FullName.Contains("Microsoft.") && !b.FullName.StartsWith("System."))?
                            .Where(b => !b.FullName.Contains("Google.") && !b.FullName.StartsWith("Google."))?
                            .Where(b => !b.FullName.Contains("Grpc.") && !b.FullName.StartsWith("Grpc."))?
                            .Where(b => !b.FullName.ToLower().Contains("aspnetcore"))?
                            .Where(b => !b.FullName.ToLower().Contains("aspnet") && !b.FullName.ToLower().Contains("asp.net"))?
                            .Where(b => b.GetTypes().Where(z => z != serviceActionType && serviceActionType.IsAssignableFrom(z)).IsNotNullOrEmpty());
                    }

                    if (_assemblies.IsNullOrEmpty())
                    {
                        result.Message = $"No assemblies found in application executing path: {execPath}.";
                        Logger.Error(nameof(StartServiceAsync), result.Message, nameof(StartServiceAsync), "ServiceHostTrace");
                        ConsoleLogger.LogError(result.Message);
                        return result;
                    }

                    foreach (var assembly in _assemblies)
                    {
                        var types = assembly.GetTypes().Where(z => z != serviceActionType && serviceActionType.IsAssignableFrom(z));

                        if (types.IsNullOrEmpty())
                        {
                            continue;
                        }

                        foreach (var type in types)
                        {
                            var c = await CreateServiceActionAsyncInstance(serviceName, type);

                            if (!c.Success)
                            {
                                result.Message = c.Message;
                                return result;
                            }

                            if (c.Data.IsNull())
                            {
                                continue;
                            }

                            var serviceActionAsync = c.Data;
                            _asyncActions.Add(serviceActionAsync);
                        }
                    }
                }

                #endregion 動態載入 assembly 註冊 ServiceAction 動作
            }
            else
            {
                #region 執行註冊 ServiceAction 的委派

                var g = InitializeServiceActionHandler();

                if (!g.Success)
                {
                    result.Code = StatusCode.INTERNAL_ERROR;
                    result.Message = $"Initialize process fail. Initialize the service action container fail. {g.Message}";
                    Logger.Error(nameof(StartServiceAsync), result.Message, nameof(StartServiceAsync), "ServiceHostTrace");
                    ConsoleLogger.LogError(result.Message);
                    return result;
                }

                #endregion 執行註冊 ServiceAction 的委派
            }

            #endregion 初始化服務動作容器

            #region 建立服務託管

            var r = RemoteHostContainer.Get(serviceName);

            if (!r.Success)
            {
                result.Code = StatusCode.INTERNAL_ERROR;
                result.Message = $"Initialize process fail. Create remote service host fail. {r.Message}";
                Logger.Error(nameof(StartServiceAsync), result.Message, nameof(StartServiceAsync), "ServiceHostTrace");
                ConsoleLogger.LogError(result.Message);
                return result;
            }

            var remoteHost = r.Data;

            #endregion 建立服務託管

            #region 啟動 Host 服務

            var s = remoteHost.StartHost();

            if (!s.Success)
            {
                result.Code = StatusCode.INTERNAL_ERROR;
                result.Message = $"Initialize process fail. Start remote service host fail. {s.Message}";
                Logger.Error(nameof(StartServiceAsync), result.Message, nameof(StartServiceAsync), "ServiceHostTrace");
                ConsoleLogger.LogError(result.Message);
                return result;
            }

            #endregion 啟動 Host 服務

            result.Success = true;
            return result;
        }


        /// <summary>發佈事件訊息
        /// </summary>
        /// <param name="serviceName">指定的服務託管設定名稱</param>
        /// <param name="actionName">觸發的動作名稱</param>
        /// <param name="messageContent">訊息資料內容</param>
        /// <returns>發佈結果</returns>
        public static IResult Publish(string serviceName, string actionName, object messageContent)
        {
            var result = Result.Create();

            var messagePackage = new MessagePackage()
            {
                PackageType = PackageType.Publish,
                ServiceName = serviceName,
                MessageId = RandomTextHelper.Create(15),
                ActionName = actionName,
                PublishTime = DateTime.UtcNow
            };

            if (messageContent.IsNotNull())
            {
                string json;

                try
                {
                    json = JsonSerializer.Serialize(messageContent);
                }
                catch (Exception ex)
                {
                    result.Code = StatusCode.INTERNAL_ERROR;
                    result.Message = $"Serialize data content of message package occur exception. {Environment.NewLine}{ex}";
                    Logger.Error(nameof(MiddlewareService), result.Message, nameof(Publish), "ServiceHostTrace");
                    ConsoleLogger.LogError(result.Message);
                    return result;
                }

                messagePackage.Data = json;
            }

            var r = RemoteHostContainer.Get(serviceName);

            if (!r.Success)
            {
                result.Code = StatusCode.INTERNAL_ERROR;
                result.Message = $"Invalid service name. ServiceName: {serviceName}.";
                Logger.Error(nameof(MiddlewareService), result.Message, nameof(Publish), "ServiceHostTrace");
                ConsoleLogger.LogError(result.Message);
                return result;
            }

            var remoteHost = r.Data;
            result = remoteHost.Publish(messagePackage);
            When.True(result.Success, () => new ServiceActionLogger(serviceName).Log(4, messagePackage));
            return result;
        }

        #endregion Public Methods


        #region Private Methods

        /// <summary>建立 ServiceAction 服務動作物件，並且註冊至 ServiceActionContainerManager 中>
        /// </summary>
        /// <param name="serviceName">服務設定名稱</param>
        /// <param name="type">ServiceAction 動作的 Type 型別</param>
        /// <returns>執行結果</returns>
        private static IResult<ServiceAction> CreateServiceActionInstance(string serviceName, Type type)
        {
            var result = Result.Create<ServiceAction>();
            ServiceAction action;

            try
            {
                var constructor = type.GetConstructor(Type.EmptyTypes);

                if (constructor.IsNull())
                {
                    result.Success = true;
                    return result;
                }

                action = (ServiceAction)constructor.Invoke([]);
            }
            catch (Exception ex)
            {
                result.ExceptionObject = ex;
                result.Message = $"Create the ServiceAction instance occur exception. ServiceName: {serviceName}, ServiceActionType: {type.FullName}. {ex}";
                ConsoleLogger.LogError(result.Message);
                return result;
            }

            var d = ServiceActionContainerManager.Add(serviceName, action.Name, type);

            if (!d.Success)
            {
                result.Message = $"Add the '{action.Name}' ServiceAction instance into ServieActionConatiner occur error. {d.Message}";
                ConsoleLogger.LogError(result.Message);
                return result;
            }

            result.Data = action;
            result.Success = true;
            return result;
        }

        /// <summary>建立 ServiceAction 服務動作物件，並且註冊至 ServiceActionContainerManager 中>
        /// </summary>
        /// <param name="serviceName">服務設定名稱</param>
        /// <param name="type">ServiceAction 動作的 Type 型別</param>
        /// <returns>執行結果</returns>
        private static async Task<IResult<ServiceActionAsync>> CreateServiceActionAsyncInstance(string serviceName, Type type)
        {
            var result = Result.Create<ServiceActionAsync>();
            ServiceActionAsync action;

            try
            {
                var constructor = type.GetConstructor(Type.EmptyTypes);

                if (constructor.IsNull())
                {
                    result.Success = true;
                    return result;
                }

                action = (ServiceActionAsync)constructor.Invoke([]);
            }
            catch (Exception ex)
            {
                result.ExceptionObject = ex;
                result.Message = $"Create the ServiceActionAsync instance occur exception. ServiceName: {serviceName}, ServiceActionType: {type.FullName}. {ex}";
                ConsoleLogger.LogError(result.Message);
                return result;
            }

            var d = await ServiceActionContainerManager.AddAsync(serviceName, action.Name, type);

            if (!d.Success)
            {
                result.Message = $"Add the '{action.Name}' ServiceActionAsync instance into ServieActionConatiner occur error. {d.Message}";
                ConsoleLogger.LogError(result.Message);
                return result;
            }

            result.Data = action;
            result.Success = true;
            return result;
        }

        #endregion Private Methods
    }
}
