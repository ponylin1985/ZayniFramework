﻿using NeoSmart.AsyncLock;
using System;
using System.Globalization;
using System.Threading.Tasks;


namespace ZayniFramework.Middle.Service
{
    /// <summary>命令列主控台 Console 日誌器
    /// </summary>
    public sealed class ConsoleOutputLogger
    {
        #region 宣告私有的欄位

        /// <summary>非同步作業鎖定物件
        /// </summary>
        private static readonly AsyncLock _asyncLock = new();

        #endregion 宣告私有的欄位


        #region 宣告內部的屬性

        /// <summary>是否啟用主控台追蹤日誌紀錄
        /// </summary>
        public static bool Enable { get; internal set; } = GlobalContext.Config.EnableConsoleTracingLog;

        #endregion 宣告內部的屬性


        #region 宣告內部的方法

        /// <summary>主控台日誌訊息輸出。<para/>
        /// 1. 預設輸出訊息會換行。<para/>
        /// 2. 預設字體顏色為灰色，可自行指定訊息的字體顏色。<para/>
        /// 3. 強制輸出訊息時，統一格式，包括輸出的時間，格式為 yyyy-MM-dd HH:mm:ss.fff。<para/>
        /// 4. 由於 Console 輸出時採用 Task 非同步的作業進行，因此，無法保證於 Console 畫面上輸出的順序。<para/>
        /// 5. 假若期望在 Console 輸出時，要依序的輸出，則必須要傳入參數 wait = true; 等待非同步作業完成之後，才可以繼續執行下一個非同步作業。
        /// </summary>
        /// <param name="message">訊息</param>
        /// <param name="color">訊息字體顏色</param>
        /// <param name="wait">是否依序輸出 (預設為 false)</param>
        /// <param name="forceLog">是否強制輸出</param>
        public static void Log(string message, ConsoleColor color = ConsoleColor.Gray, bool wait = false, bool forceLog = false)
        {
            if (forceLog)
            {
                Log(message, color, wait);
                return;
            }

            if (!Enable)
            {
                return;
            }

            Log(message, color, wait);
        }

        /// <summary>主控台日誌訊息輸出。<para/>
        /// 1. 預設輸出訊息會換行。<para/>
        /// 2. 預設字體顏色為灰色，可自行指定訊息的字體顏色。<para/>
        /// 3. 強制輸出訊息時，統一格式，包括輸出的時間，格式為 yyyy-MM-dd HH:mm:ss.fff。<para/>
        /// 4. 由於 Console 輸出時採用 Task 非同步的作業進行，因此，無法保證於 Console 畫面上輸出的順序。<para/>
        /// 5. 假若期望在 Console 輸出時，要依序的輸出，則必須要傳入參數 wait = true; 等待非同步作業完成之後，才可以繼續執行下一個非同步作業。
        /// </summary>
        /// <param name="message">訊息</param>
        /// <param name="color">訊息字體顏色</param>
        /// <param name="wait">是否依序輸出 (預設為 false)</param>
        /// <param name="forceLog">是否強制輸出</param>
        /// <returns>非同步作業任務</returns>
        public static async Task LogAsync(string message, ConsoleColor color = ConsoleColor.Gray, bool wait = false, bool forceLog = false)
        {
            if (forceLog)
            {
                await LogAsync(message, color, wait);
                return;
            }

            if (!Enable)
            {
                return;
            }

            await LogAsync(message, color, wait);
        }

        #endregion 宣告內部的方法


        #region 宣告私有的方法

        /// <summary>主控台日誌訊息輸出。<para/>
        /// 1. 預設輸出訊息會換行。<para/>
        /// 2. 預設字體顏色為灰色，可自行指定訊息的字體顏色。<para/>
        /// 3. 強制輸出訊息時，統一格式，包括輸出的時間，格式為 yyyy-MM-dd HH:mm:ss.fff。<para/>
        /// 4. 由於 Console 輸出時採用 Task 非同步的作業進行，因此，無法保證於 Console 畫面上輸出的順序。<para/>
        /// 5. 假若期望在 Console 輸出時，要依序的輸出，則必須要傳入參數 wait = true; 等待非同步作業完成之後，才可以繼續執行下一個非同步作業。
        /// </summary>
        /// <param name="message">訊息</param>
        /// <param name="color">訊息字體顏色</param>
        /// <param name="wait">是否依序輸出 (預設為 false)</param>
        private static void Log(string message, ConsoleColor color = ConsoleColor.Gray, bool wait = false)
        {
            using (_asyncLock.Lock())
            {
                var currentColor = Console.ForegroundColor;
                Console.ForegroundColor = color;
                Console.WriteLine($"[{DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture)}] {message}");
                Console.ForegroundColor = currentColor;
            }
        }

        /// <summary>主控台日誌訊息輸出。<para/>
        /// 1. 預設輸出訊息會換行。<para/>
        /// 2. 預設字體顏色為灰色，可自行指定訊息的字體顏色。<para/>
        /// 3. 強制輸出訊息時，統一格式，包括輸出的時間，格式為 yyyy-MM-dd HH:mm:ss.fff。<para/>
        /// 4. 由於 Console 輸出時採用 Task 非同步的作業進行，因此，無法保證於 Console 畫面上輸出的順序。<para/>
        /// 5. 假若期望在 Console 輸出時，要依序的輸出，則必須要傳入參數 wait = true; 等待非同步作業完成之後，才可以繼續執行下一個非同步作業。
        /// </summary>
        /// <param name="message">訊息</param>
        /// <param name="color">訊息字體顏色</param>
        /// <param name="wait">是否依序輸出 (預設為 false)</param>
        /// <returns>非同步作業任務</returns>
        private static async Task LogAsync(string message, ConsoleColor color = ConsoleColor.Gray, bool wait = false)
        {
            using (await _asyncLock.LockAsync())
            {
                var currentColor = Console.ForegroundColor;
                Console.ForegroundColor = color;
                await Console.Out.WriteLineAsync($"[{DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture)}] {message}");
                Console.ForegroundColor = currentColor;
            }
        }

        #endregion 宣告私有的方法
    }
}
