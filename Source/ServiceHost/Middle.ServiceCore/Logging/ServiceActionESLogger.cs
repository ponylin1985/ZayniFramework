using NeoSmart.AsyncLock;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Logging;
using ZayniFramework.Middle.Service.Entity;


namespace ZayniFramework.Middle.Service
{
    /// <summary>服務動作 ES 日誌紀錄器
    /// </summary>
    internal sealed class ServiceActionESLogger
    {
        #region Private Fields

        /// <summary>非同步作業的鎖定物件
        /// </summary>
        /// <returns></returns>
        private static readonly AsyncLock _asyncLock = new();

        /// <summary>Elasticsearch 日誌器
        /// </summary>
        /// <returns></returns>
        private readonly ElasticsearchLogger _esLogger;

        #endregion Private Fields


        #region Constructors

        /// <summary>無參數預設建構子
        /// </summary>
        internal ServiceActionESLogger(string name)
        {
            try
            {
                _esLogger = ElasticsearchLoggerContainer.Get(name);
            }
            catch
            {
                // pass
            }
        }

        #endregion Constructors


        #region Internal Methods

        /// <summary>紀錄服務動作日誌
        /// </summary>
        /// <param name="requestPackage">服務請求包裹</param>
        internal async Task LogAsync(RequestPackage requestPackage)
        {
            using (await _asyncLock.LockAsync())
            {
                try
                {
                    var model = ConvertTo(requestPackage);
                    await _esLogger?.InsertLogAsync<service_host_action_log>(model, model.LogSrNo);
                }
                catch (Exception ex)
                {
                    Logger.Exception(nameof(ServiceActionLogRepo), ex, "Write request FS_SERVICE_HOST_ACTION_LOG to Elasticsearch occur exception.", "ServiceHostTrace");
                }
            }
        }

        /// <summary>紀錄服務動作日誌
        /// </summary>
        /// <param name="responsePackage">服務回應包裹</param>
        internal async Task LogAsync(ResponsePackageEntry responsePackage)
        {
            using (await _asyncLock.LockAsync())
            {
                try
                {
                    var model = ConvertTo(responsePackage);
                    await _esLogger?.InsertLogAsync<service_host_action_log>(model, model.LogSrNo);
                }
                catch (Exception ex)
                {
                    Logger.Exception(nameof(ServiceActionLogRepo), ex, "Write response FS_SERVICE_HOST_ACTION_LOG to Elasticsearch occur exception.", "ServiceHostTrace");
                }
            }
        }

        /// <summary>紀錄服務動作日誌
        /// </summary>
        /// <param name="direction">通訊的方向類型，3: ReceiveMessage，4: PublishMessage。</param>
        /// <param name="messagePackage">訊息資料包裹</param>
        internal async Task LogAsync(int direction, MessagePackage messagePackage)
        {
            using (await _asyncLock.LockAsync())
            {
                try
                {
                    var model = ConvertTo(direction, messagePackage);
                    await _esLogger?.InsertLogAsync(model, model.LogSrNo);
                }
                catch (Exception ex)
                {
                    Logger.Exception(nameof(ServiceActionLogRepo), ex, "Write response FS_SERVICE_HOST_ACTION_LOG to Elasticsearch occur exception.", "ServiceHostTrace");
                }
            }
        }

        #endregion Internal Methods


        #region Private Methods

        /// <summary>轉換服務動作日誌紀錄的資料模型
        /// </summary>
        /// <param name="requestPackage">服務請求包裹</param>
        /// <returns>服務動作日誌紀錄的資料模型</returns>
        private static service_host_action_log ConvertTo(RequestPackage requestPackage)
        {
            var model = new service_host_action_log()
            {
                LogSrNo = RandomTextHelper.CreateInt64String(20),
                RequestId = requestPackage.RequestId,
                ServiceName = requestPackage.ServiceName,
                ServiceHost = IPAddressHelper.GetLocalIPAddress(),
                Direction = 1,
                ActionName = requestPackage.ActionName,

                RequestTime = requestPackage.RequestTime,
                LogTime = DateTime.UtcNow
            };

            requestPackage.Data.IsNotNullOrEmpty(d => model.DataContent = JsonConvert.DeserializeObject(requestPackage.Data));
            return model;
        }

        /// <summary>轉換服務動作日誌紀錄的資料模型
        /// </summary>
        /// <param name="responsePackage">服務回應包裹</param>
        /// <returns>服務動作日誌紀錄的資料模型</returns>
        private static service_host_action_log ConvertTo(ResponsePackageEntry responsePackage)
        {
            var nowTime = DateTime.UtcNow;

            var model = new service_host_action_log()
            {
                LogSrNo = RandomTextHelper.CreateInt64String(20),
                RequestId = responsePackage.RequestId,
                ServiceName = responsePackage.ServiceName,
                ServiceHost = IPAddressHelper.GetLocalIPAddress(),
                Direction = 2,
                ActionName = responsePackage.ActionName,

                IsSuccess = responsePackage.Success,
                Code = responsePackage.Code,
                Message = responsePackage.Message,
                ResponseTime = nowTime,
                LogTime = nowTime
            };

            responsePackage.Data.IsNotNullOrEmpty(d => model.DataContent = JsonConvert.DeserializeObject(responsePackage.Data));
            return model;
        }

        /// <summary>轉換服務動作日誌紀錄的資料模型
        /// </summary>
        /// <param name="direction">通訊的方向類型，3: ReceiveMessage，4: PublishMessage。</param>
        /// <param name="messagePackage"></param>
        /// <returns>服務動作日誌紀錄的資料模型</returns>
        private static service_host_action_log ConvertTo(int direction, MessagePackage messagePackage)
        {
            var model = new service_host_action_log()
            {
                LogSrNo = RandomTextHelper.CreateInt64String(20),
                RequestId = messagePackage.MessageId,
                ServiceName = messagePackage.ServiceName,
                ServiceHost = IPAddressHelper.GetLocalIPAddress(),
                Direction = direction,
                ActionName = messagePackage.ActionName,
                LogTime = DateTime.UtcNow
            };

            switch (direction)
            {
                case 3:
                    model.RequestTime = messagePackage.PublishTime;
                    break;

                case 4:
                    model.ResponseTime = messagePackage.PublishTime;
                    break;
            }

            messagePackage.Data.IsNotNullOrEmpty(d => model.DataContent = JsonConvert.DeserializeObject(messagePackage.Data));
            return model;
        }

        #endregion Private Methods
    }
}
