﻿using System;
using System.Text.Json;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Logging;
using ZayniFramework.Middle.Service.Entity;


namespace ZayniFramework.Middle.Service
{
    /// <summary>非同步的遠端服務類別
    /// </summary>
    public sealed partial class RemoteService : IRemoteServiceAsync
    {
        #region 實作 IRemoteServiceAsync 介面

        /// <summary>執行遠端服務動作
        /// </summary>
        /// <param name="request">遠端服務請求包裹</param>
        /// <returns>遠端服務回應包裹</returns>
        public async Task<ResponsePackage> ExecuteAsync(RequestPackage request)
        {
            #region 初始化回應包裹

            var result = new ResponsePackage()
            {
                RequestId = request?.RequestId,
                ActionName = request?.ActionName,
                Success = false,
                Data = null
            };

            #endregion 初始化回應包裹

            #region 驗證請求包裹

            var v = Validator.Validate(request);

            if (!v.Success)
            {
                request.RequestId = request?.RequestId?.IsNullOrEmptyString("_InvalidReqId");
                request.ActionName = request?.ActionName.IsNullOrEmptyString("_InvalidActionName");
                result.RequestId = request?.RequestId.IsNullOrEmptyString("_InvalidReqId");
                result.ActionName = request?.ActionName.IsNullOrEmptyString("_InvalidActionName");
                result.Code = StatusCode.INVALID_REQUEST;
                result.Message = $"Invalid remote request. {v.Message}";
                _actionLog.Log(request);
                _actionLog.Log(result);
                return result;
            }

            #endregion 驗證請求包裹

            #region 取得服務動作

            var action = await ServiceActionContainerManager.GetServiceActionAsync(ServiceName, request.ActionName);

            if (action.IsNull())
            {
                result.Code = StatusCode.INVALID_REQUEST;
                result.Message = $"Invalid remote request. Service name: {ServiceName}. Unknow action: {request.ActionName}.";
                _actionLog.Log(request);
                _actionLog.Log(result);
                Logger.Error(this, result.Message, nameof(ExecuteAsync), loggerName: "ServiceHostTrace");
                return result;
            }

            #endregion 取得服務動作

            #region 設定服務動作的請求參數

            try
            {
                var requestData = request.Data;
                requestData.IsNotNullOrEmpty(d => action.RequestData = JsonSerializer.Deserialize(request.Data, action.RequestType));
            }
            catch (Exception ex)
            {
                result.Code = StatusCode.INTERNAL_ERROR;
                result.Message = $"Internal zayni framework service error. {ex}";
                Logger.Exception(this, ex, "Set request data to service action occur exception.", "ServiceHostTrace");
                _actionLog.Log(request);
                _actionLog.Log(result);
                return result;
            }

            #endregion 設定服務動作的請求參數

            #region 執行請求參數的資料驗證

            if (action.RequestData.IsNotNull() && action.RequestValidation)
            {
                var d = Validator.Validate(action.RequestData);

                if (!d.Success)
                {
                    result.Code = StatusCode.INVALID_REQUEST;
                    result.Message = $"Invalid remote request. {d.Message}";
                    _actionLog.Log(request);
                    _actionLog.Log(result);
                    Logger.Error(this, result.Message, nameof(ExecuteAsync), loggerName: "ServiceHostTrace");
                    return result;
                }
            }

            #endregion 執行請求參數的資料驗證

            #region 執行服務動作

            _actionLog.Log(request);

            try
            {
                await action.ExecuteAsync();
            }
            catch (Exception ex)
            {
                result.Code = StatusCode.INTERNAL_ERROR;
                result.Message = $"Internal zayni framework service error. {ex}";
                Logger.Exception(this, ex, $"Execute service action occur exception. ActionName: {request.ActionName}, RequestId: {request.RequestId}.", "ServiceHostTrace");
                _actionLog.Log(result);
                return result;
            }

            #endregion 執行服務動作

            #region 設定動作執行後的回傳值

            string json = null;

            try
            {
                var response = action.ResponseData;
                response.IsNotNull(r => json = JsonSerializer.Serialize(response));
            }
            catch (Exception ex)
            {
                result.Code = StatusCode.INTERNAL_ERROR;
                result.Message = $"Internal zayni framework service error. {ex}";
                Logger.Exception(this, ex, $"Set response data of service action occur exception.", "ServiceHostTrace");
                _actionLog.Log(result);
                return result;
            }

            #endregion 設定動作執行後的回傳值

            #region 設定回應包裹

            result.Data = json;
            result.Code = StatusCode.ACTION_SUCCESS;
            result.Success = true;

            _actionLog.Log(result);

            #endregion 設定回應包裹

            return result;
        }

        #endregion 實作 IRemoteServiceAsync 介面
    }
}
