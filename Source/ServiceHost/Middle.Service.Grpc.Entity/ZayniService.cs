// <auto-generated>
//     Generated by the protocol buffer compiler.  DO NOT EDIT!
//     source: zayni.service.proto
// </auto-generated>
#pragma warning disable 1591, 0612, 3021
#region Designer generated code

using pb = global::Google.Protobuf;
using pbc = global::Google.Protobuf.Collections;
using pbr = global::Google.Protobuf.Reflection;
using scg = global::System.Collections.Generic;
namespace ZayniFramework.Middle.Service.Grpc.Entity
{

    /// <summary>Holder for reflection information generated from zayni.service.proto</summary>
    public static partial class ZayniServiceReflection
    {

        #region Descriptor
        /// <summary>File descriptor for zayni.service.proto</summary>
        public static pbr::FileDescriptor Descriptor
        {
            get { return descriptor; }
        }
        private static pbr::FileDescriptor descriptor;

        static ZayniServiceReflection()
        {
            byte[] descriptorData = global::System.Convert.FromBase64String(
                string.Concat(
                  "ChN6YXluaS5zZXJ2aWNlLnByb3RvEilaYXluaUZyYW1ld29yay5NaWRkbGUu",
                  "U2VydmljZS5HcnBjLkVudGl0eSKEAQoOUmVxdWVzdFBhY2thZ2USEwoLcGFj",
                  "a2FnZVR5cGUYASABKAkSEwoLc2VydmljZU5hbWUYAiABKAkSEQoJcmVxdWVz",
                  "dElkGAMgASgJEhIKCmFjdGlvbk5hbWUYBCABKAkSDAoEZGF0YRgFIAEoCRIT",
                  "CgtyZXF1ZXN0VGltZRgGIAEoCSKNAQoPUmVzcG9uc2VQYWNrYWdlEhMKC3Bh",
                  "Y2thZ2VUeXBlGAEgASgJEhEKCXJlcXVlc3RJZBgCIAEoCRISCgphY3Rpb25O",
                  "YW1lGAMgASgJEhEKCWlzU3VjY2VzcxgEIAEoCRIMCgRkYXRhGAUgASgJEgwK",
                  "BGNvZGUYBiABKAkSDwoHbWVzc2FnZRgHIAEoCTKWAQoPR1JQQ1NlcnZpY2VI",
                  "b3N0EoIBCgdFeGVjdXRlEjkuWmF5bmlGcmFtZXdvcmsuTWlkZGxlLlNlcnZp",
                  "Y2UuR3JwYy5FbnRpdHkuUmVxdWVzdFBhY2thZ2UaOi5aYXluaUZyYW1ld29y",
                  "ay5NaWRkbGUuU2VydmljZS5HcnBjLkVudGl0eS5SZXNwb25zZVBhY2thZ2Ui",
                  "AGIGcHJvdG8z"));
            descriptor = pbr::FileDescriptor.FromGeneratedCode(descriptorData,
                new pbr::FileDescriptor[] { },
                new pbr::GeneratedClrTypeInfo(null, new pbr::GeneratedClrTypeInfo[] {
            new pbr::GeneratedClrTypeInfo(typeof(global::ZayniFramework.Middle.Service.Grpc.Entity.RequestPackage), global::ZayniFramework.Middle.Service.Grpc.Entity.RequestPackage.Parser, new[]{ "PackageType", "ServiceName", "RequestId", "ActionName", "Data", "RequestTime" }, null, null, null),
            new pbr::GeneratedClrTypeInfo(typeof(global::ZayniFramework.Middle.Service.Grpc.Entity.ResponsePackage), global::ZayniFramework.Middle.Service.Grpc.Entity.ResponsePackage.Parser, new[]{ "PackageType", "RequestId", "ActionName", "IsSuccess", "Data", "Code", "Message" }, null, null, null)
                }));
        }
        #endregion

    }
    #region Messages
    public sealed partial class RequestPackage : pb::IMessage<RequestPackage>
    {
        private static readonly pb::MessageParser<RequestPackage> _parser = new pb::MessageParser<RequestPackage>(() => new RequestPackage());
        private pb::UnknownFieldSet _unknownFields;
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public static pb::MessageParser<RequestPackage> Parser { get { return _parser; } }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public static pbr::MessageDescriptor Descriptor
        {
            get { return global::ZayniFramework.Middle.Service.Grpc.Entity.ZayniServiceReflection.Descriptor.MessageTypes[0]; }
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        pbr::MessageDescriptor pb::IMessage.Descriptor
        {
            get { return Descriptor; }
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public RequestPackage()
        {
            OnConstruction();
        }

        partial void OnConstruction();

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public RequestPackage(RequestPackage other) : this()
        {
            packageType_ = other.packageType_;
            serviceName_ = other.serviceName_;
            requestId_ = other.requestId_;
            actionName_ = other.actionName_;
            data_ = other.data_;
            requestTime_ = other.requestTime_;
            _unknownFields = pb::UnknownFieldSet.Clone(other._unknownFields);
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public RequestPackage Clone()
        {
            return new RequestPackage(this);
        }

        /// <summary>Field number for the "packageType" field.</summary>
        public const int PackageTypeFieldNumber = 1;
        private string packageType_ = "";
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public string PackageType
        {
            get { return packageType_; }
            set
            {
                packageType_ = pb::ProtoPreconditions.CheckNotNull(value, "value");
            }
        }

        /// <summary>Field number for the "serviceName" field.</summary>
        public const int ServiceNameFieldNumber = 2;
        private string serviceName_ = "";
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public string ServiceName
        {
            get { return serviceName_; }
            set
            {
                serviceName_ = pb::ProtoPreconditions.CheckNotNull(value, "value");
            }
        }

        /// <summary>Field number for the "requestId" field.</summary>
        public const int RequestIdFieldNumber = 3;
        private string requestId_ = "";
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public string RequestId
        {
            get { return requestId_; }
            set
            {
                requestId_ = pb::ProtoPreconditions.CheckNotNull(value, "value");
            }
        }

        /// <summary>Field number for the "actionName" field.</summary>
        public const int ActionNameFieldNumber = 4;
        private string actionName_ = "";
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public string ActionName
        {
            get { return actionName_; }
            set
            {
                actionName_ = pb::ProtoPreconditions.CheckNotNull(value, "value");
            }
        }

        /// <summary>Field number for the "data" field.</summary>
        public const int DataFieldNumber = 5;
        private string data_ = "";
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public string Data
        {
            get { return data_; }
            set
            {
                data_ = pb::ProtoPreconditions.CheckNotNull(value, "value");
            }
        }

        /// <summary>Field number for the "requestTime" field.</summary>
        public const int RequestTimeFieldNumber = 6;
        private string requestTime_ = "";
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public string RequestTime
        {
            get { return requestTime_; }
            set
            {
                requestTime_ = pb::ProtoPreconditions.CheckNotNull(value, "value");
            }
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public override bool Equals(object other)
        {
            return Equals(other as RequestPackage);
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public bool Equals(RequestPackage other)
        {
            if (ReferenceEquals(other, null))
            {
                return false;
            }
            if (ReferenceEquals(other, this))
            {
                return true;
            }
            if (PackageType != other.PackageType) return false;
            if (ServiceName != other.ServiceName) return false;
            if (RequestId != other.RequestId) return false;
            if (ActionName != other.ActionName) return false;
            if (Data != other.Data) return false;
            if (RequestTime != other.RequestTime) return false;
            return Equals(_unknownFields, other._unknownFields);
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public override int GetHashCode()
        {
            int hash = 1;
            if (PackageType.Length != 0) hash ^= PackageType.GetHashCode();
            if (ServiceName.Length != 0) hash ^= ServiceName.GetHashCode();
            if (RequestId.Length != 0) hash ^= RequestId.GetHashCode();
            if (ActionName.Length != 0) hash ^= ActionName.GetHashCode();
            if (Data.Length != 0) hash ^= Data.GetHashCode();
            if (RequestTime.Length != 0) hash ^= RequestTime.GetHashCode();
            if (_unknownFields != null)
            {
                hash ^= _unknownFields.GetHashCode();
            }
            return hash;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public override string ToString()
        {
            return pb::JsonFormatter.ToDiagnosticString(this);
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public void WriteTo(pb::CodedOutputStream output)
        {
            if (PackageType.Length != 0)
            {
                output.WriteRawTag(10);
                output.WriteString(PackageType);
            }
            if (ServiceName.Length != 0)
            {
                output.WriteRawTag(18);
                output.WriteString(ServiceName);
            }
            if (RequestId.Length != 0)
            {
                output.WriteRawTag(26);
                output.WriteString(RequestId);
            }
            if (ActionName.Length != 0)
            {
                output.WriteRawTag(34);
                output.WriteString(ActionName);
            }
            if (Data.Length != 0)
            {
                output.WriteRawTag(42);
                output.WriteString(Data);
            }
            if (RequestTime.Length != 0)
            {
                output.WriteRawTag(50);
                output.WriteString(RequestTime);
            }
            if (_unknownFields != null)
            {
                _unknownFields.WriteTo(output);
            }
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public int CalculateSize()
        {
            int size = 0;
            if (PackageType.Length != 0)
            {
                size += 1 + pb::CodedOutputStream.ComputeStringSize(PackageType);
            }
            if (ServiceName.Length != 0)
            {
                size += 1 + pb::CodedOutputStream.ComputeStringSize(ServiceName);
            }
            if (RequestId.Length != 0)
            {
                size += 1 + pb::CodedOutputStream.ComputeStringSize(RequestId);
            }
            if (ActionName.Length != 0)
            {
                size += 1 + pb::CodedOutputStream.ComputeStringSize(ActionName);
            }
            if (Data.Length != 0)
            {
                size += 1 + pb::CodedOutputStream.ComputeStringSize(Data);
            }
            if (RequestTime.Length != 0)
            {
                size += 1 + pb::CodedOutputStream.ComputeStringSize(RequestTime);
            }
            if (_unknownFields != null)
            {
                size += _unknownFields.CalculateSize();
            }
            return size;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public void MergeFrom(RequestPackage other)
        {
            if (other == null)
            {
                return;
            }
            if (other.PackageType.Length != 0)
            {
                PackageType = other.PackageType;
            }
            if (other.ServiceName.Length != 0)
            {
                ServiceName = other.ServiceName;
            }
            if (other.RequestId.Length != 0)
            {
                RequestId = other.RequestId;
            }
            if (other.ActionName.Length != 0)
            {
                ActionName = other.ActionName;
            }
            if (other.Data.Length != 0)
            {
                Data = other.Data;
            }
            if (other.RequestTime.Length != 0)
            {
                RequestTime = other.RequestTime;
            }
            _unknownFields = pb::UnknownFieldSet.MergeFrom(_unknownFields, other._unknownFields);
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public void MergeFrom(pb::CodedInputStream input)
        {
            uint tag;
            while ((tag = input.ReadTag()) != 0)
            {
                switch (tag)
                {
                    default:
                        _unknownFields = pb::UnknownFieldSet.MergeFieldFrom(_unknownFields, input);
                        break;
                    case 10:
                        {
                            PackageType = input.ReadString();
                            break;
                        }
                    case 18:
                        {
                            ServiceName = input.ReadString();
                            break;
                        }
                    case 26:
                        {
                            RequestId = input.ReadString();
                            break;
                        }
                    case 34:
                        {
                            ActionName = input.ReadString();
                            break;
                        }
                    case 42:
                        {
                            Data = input.ReadString();
                            break;
                        }
                    case 50:
                        {
                            RequestTime = input.ReadString();
                            break;
                        }
                }
            }
        }

    }

    public sealed partial class ResponsePackage : pb::IMessage<ResponsePackage>
    {
        private static readonly pb::MessageParser<ResponsePackage> _parser = new pb::MessageParser<ResponsePackage>(() => new ResponsePackage());
        private pb::UnknownFieldSet _unknownFields;
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public static pb::MessageParser<ResponsePackage> Parser { get { return _parser; } }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public static pbr::MessageDescriptor Descriptor
        {
            get { return global::ZayniFramework.Middle.Service.Grpc.Entity.ZayniServiceReflection.Descriptor.MessageTypes[1]; }
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        pbr::MessageDescriptor pb::IMessage.Descriptor
        {
            get { return Descriptor; }
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public ResponsePackage()
        {
            OnConstruction();
        }

        partial void OnConstruction();

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public ResponsePackage(ResponsePackage other) : this()
        {
            packageType_ = other.packageType_;
            requestId_ = other.requestId_;
            actionName_ = other.actionName_;
            isSuccess_ = other.isSuccess_;
            data_ = other.data_;
            code_ = other.code_;
            message_ = other.message_;
            _unknownFields = pb::UnknownFieldSet.Clone(other._unknownFields);
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public ResponsePackage Clone()
        {
            return new ResponsePackage(this);
        }

        /// <summary>Field number for the "packageType" field.</summary>
        public const int PackageTypeFieldNumber = 1;
        private string packageType_ = "";
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public string PackageType
        {
            get { return packageType_; }
            set
            {
                packageType_ = pb::ProtoPreconditions.CheckNotNull(value, "value");
            }
        }

        /// <summary>Field number for the "requestId" field.</summary>
        public const int RequestIdFieldNumber = 2;
        private string requestId_ = "";
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public string RequestId
        {
            get { return requestId_; }
            set
            {
                requestId_ = pb::ProtoPreconditions.CheckNotNull(value, "value");
            }
        }

        /// <summary>Field number for the "actionName" field.</summary>
        public const int ActionNameFieldNumber = 3;
        private string actionName_ = "";
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public string ActionName
        {
            get { return actionName_; }
            set
            {
                actionName_ = pb::ProtoPreconditions.CheckNotNull(value, "value");
            }
        }

        /// <summary>Field number for the "isSuccess" field.</summary>
        public const int IsSuccessFieldNumber = 4;
        private string isSuccess_ = "";
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public string IsSuccess
        {
            get { return isSuccess_; }
            set
            {
                isSuccess_ = pb::ProtoPreconditions.CheckNotNull(value, "value");
            }
        }

        /// <summary>Field number for the "data" field.</summary>
        public const int DataFieldNumber = 5;
        private string data_ = "";
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public string Data
        {
            get { return data_; }
            set
            {
                data_ = pb::ProtoPreconditions.CheckNotNull(value, "value");
            }
        }

        /// <summary>Field number for the "code" field.</summary>
        public const int CodeFieldNumber = 6;
        private string code_ = "";
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public string Code
        {
            get { return code_; }
            set
            {
                code_ = pb::ProtoPreconditions.CheckNotNull(value, "value");
            }
        }

        /// <summary>Field number for the "message" field.</summary>
        public const int MessageFieldNumber = 7;
        private string message_ = "";
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public string Message
        {
            get { return message_; }
            set
            {
                message_ = pb::ProtoPreconditions.CheckNotNull(value, "value");
            }
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public override bool Equals(object other)
        {
            return Equals(other as ResponsePackage);
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public bool Equals(ResponsePackage other)
        {
            if (ReferenceEquals(other, null))
            {
                return false;
            }
            if (ReferenceEquals(other, this))
            {
                return true;
            }
            if (PackageType != other.PackageType) return false;
            if (RequestId != other.RequestId) return false;
            if (ActionName != other.ActionName) return false;
            if (IsSuccess != other.IsSuccess) return false;
            if (Data != other.Data) return false;
            if (Code != other.Code) return false;
            if (Message != other.Message) return false;
            return Equals(_unknownFields, other._unknownFields);
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public override int GetHashCode()
        {
            int hash = 1;
            if (PackageType.Length != 0) hash ^= PackageType.GetHashCode();
            if (RequestId.Length != 0) hash ^= RequestId.GetHashCode();
            if (ActionName.Length != 0) hash ^= ActionName.GetHashCode();
            if (IsSuccess.Length != 0) hash ^= IsSuccess.GetHashCode();
            if (Data.Length != 0) hash ^= Data.GetHashCode();
            if (Code.Length != 0) hash ^= Code.GetHashCode();
            if (Message.Length != 0) hash ^= Message.GetHashCode();
            if (_unknownFields != null)
            {
                hash ^= _unknownFields.GetHashCode();
            }
            return hash;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public override string ToString()
        {
            return pb::JsonFormatter.ToDiagnosticString(this);
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public void WriteTo(pb::CodedOutputStream output)
        {
            if (PackageType.Length != 0)
            {
                output.WriteRawTag(10);
                output.WriteString(PackageType);
            }
            if (RequestId.Length != 0)
            {
                output.WriteRawTag(18);
                output.WriteString(RequestId);
            }
            if (ActionName.Length != 0)
            {
                output.WriteRawTag(26);
                output.WriteString(ActionName);
            }
            if (IsSuccess.Length != 0)
            {
                output.WriteRawTag(34);
                output.WriteString(IsSuccess);
            }
            if (Data.Length != 0)
            {
                output.WriteRawTag(42);
                output.WriteString(Data);
            }
            if (Code.Length != 0)
            {
                output.WriteRawTag(50);
                output.WriteString(Code);
            }
            if (Message.Length != 0)
            {
                output.WriteRawTag(58);
                output.WriteString(Message);
            }
            if (_unknownFields != null)
            {
                _unknownFields.WriteTo(output);
            }
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public int CalculateSize()
        {
            int size = 0;
            if (PackageType.Length != 0)
            {
                size += 1 + pb::CodedOutputStream.ComputeStringSize(PackageType);
            }
            if (RequestId.Length != 0)
            {
                size += 1 + pb::CodedOutputStream.ComputeStringSize(RequestId);
            }
            if (ActionName.Length != 0)
            {
                size += 1 + pb::CodedOutputStream.ComputeStringSize(ActionName);
            }
            if (IsSuccess.Length != 0)
            {
                size += 1 + pb::CodedOutputStream.ComputeStringSize(IsSuccess);
            }
            if (Data.Length != 0)
            {
                size += 1 + pb::CodedOutputStream.ComputeStringSize(Data);
            }
            if (Code.Length != 0)
            {
                size += 1 + pb::CodedOutputStream.ComputeStringSize(Code);
            }
            if (Message.Length != 0)
            {
                size += 1 + pb::CodedOutputStream.ComputeStringSize(Message);
            }
            if (_unknownFields != null)
            {
                size += _unknownFields.CalculateSize();
            }
            return size;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public void MergeFrom(ResponsePackage other)
        {
            if (other == null)
            {
                return;
            }
            if (other.PackageType.Length != 0)
            {
                PackageType = other.PackageType;
            }
            if (other.RequestId.Length != 0)
            {
                RequestId = other.RequestId;
            }
            if (other.ActionName.Length != 0)
            {
                ActionName = other.ActionName;
            }
            if (other.IsSuccess.Length != 0)
            {
                IsSuccess = other.IsSuccess;
            }
            if (other.Data.Length != 0)
            {
                Data = other.Data;
            }
            if (other.Code.Length != 0)
            {
                Code = other.Code;
            }
            if (other.Message.Length != 0)
            {
                Message = other.Message;
            }
            _unknownFields = pb::UnknownFieldSet.MergeFrom(_unknownFields, other._unknownFields);
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
        public void MergeFrom(pb::CodedInputStream input)
        {
            uint tag;
            while ((tag = input.ReadTag()) != 0)
            {
                switch (tag)
                {
                    default:
                        _unknownFields = pb::UnknownFieldSet.MergeFieldFrom(_unknownFields, input);
                        break;
                    case 10:
                        {
                            PackageType = input.ReadString();
                            break;
                        }
                    case 18:
                        {
                            RequestId = input.ReadString();
                            break;
                        }
                    case 26:
                        {
                            ActionName = input.ReadString();
                            break;
                        }
                    case 34:
                        {
                            IsSuccess = input.ReadString();
                            break;
                        }
                    case 42:
                        {
                            Data = input.ReadString();
                            break;
                        }
                    case 50:
                        {
                            Code = input.ReadString();
                            break;
                        }
                    case 58:
                        {
                            Message = input.ReadString();
                            break;
                        }
                }
            }
        }

    }

    #endregion

}

#endregion Designer generated code
