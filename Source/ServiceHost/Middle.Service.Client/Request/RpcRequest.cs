﻿using Newtonsoft.Json;
using System.Threading;
using ZayniFramework.Middle.Service.Entity;


namespace ZayniFramework.Middle.Service.Client
{
    /// <summary>RPC 遠端呼叫請求
    /// </summary>
    internal sealed class RPCRequest : RequestPackage
    {
        /// <summary>接收到遠端服務的 TCP Socket 訊息回應的執行緒事件
        /// </summary>
        [JsonIgnore()]
        public AutoResetEvent ReceiveEvent { get; set; }

        /// <summary>遠端服務的 TCP Socket 訊息回應的訊息回應包裹物件
        /// </summary>
        [JsonIgnore()]
        public ResponsePackage Result { get; set; }
    }
}
