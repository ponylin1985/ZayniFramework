﻿using Newtonsoft.Json;


namespace ZayniFramework.Middle.Service.Client
{
    /// <summary>通訊客戶端的設定基底
    /// </summary>
    public abstract class RemoteClientConfig
    {
        /// <summary>通訊客戶端的設定名稱
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string Name { get; internal set; }
    }
}
