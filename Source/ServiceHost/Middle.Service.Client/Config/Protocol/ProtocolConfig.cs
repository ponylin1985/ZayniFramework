﻿using Newtonsoft.Json;


namespace ZayniFramework.Middle.Service.Client
{
    /// <summary>通訊協定設定組態基底
    /// </summary>
    public abstract class ProtocolConfig
    {
        /// <summary>預設的通訊客戶端設定名稱
        /// </summary>
        [JsonProperty(PropertyName = "defaultRemoteClient")]
        public string DefaultRemoteClient { get; internal set; }
    }
}
