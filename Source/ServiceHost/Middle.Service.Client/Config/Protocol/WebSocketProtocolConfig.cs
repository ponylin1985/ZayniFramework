﻿using Newtonsoft.Json;
using System.Collections.Generic;


namespace ZayniFramework.Middle.Service.Client
{
    /// <summary>WebSocket 通訊協定的設定組態
    /// </summary>
    public sealed class WebSocketProtocolConfig : ProtocolConfig
    {
        /// <summary>通訊客戶端設定集合
        /// </summary>
        [JsonProperty(PropertyName = "remoteClients")]
        public List<WebSocketClientConfig> RemoteClients { get; internal set; }
    }
}
