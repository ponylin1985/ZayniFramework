﻿using Newtonsoft.Json;
using System.Collections.Generic;


namespace ZayniFramework.Middle.Service.Client
{
    /// <summary>TCP Socket 通訊協定的設定組態
    /// </summary>
    public sealed class TCPSocketProtocolConfig : ProtocolConfig
    {
        /// <summary>通訊客戶端設定集合
        /// </summary>
        [JsonProperty(PropertyName = "remoteClients")]
        public List<TCPSocketClientConfig> RemoteClients { get; internal set; }
    }
}
