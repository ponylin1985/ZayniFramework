﻿using Newtonsoft.Json;
using System.Collections.Generic;


namespace ZayniFramework.Middle.Service.Client
{
    /// <summary>組態設定值
    /// </summary>
    public sealed class Config
    {
        /// <summary>服務客戶端個體的設定集合
        /// </summary>
        [JsonProperty(PropertyName = "serviceClients")]
        public List<ServiceClientConfig> ServiceClients { get; internal set; }

        /// <summary>RabbitMQ 通訊協定的設定
        /// </summary>
        [JsonProperty(PropertyName = "rabbitMQProtocol")]
        public RabbitMQProtocolConfig RabbitMQProtocol { get; internal set; }

        /// <summary>TCP Socket 通訊協定的設定
        /// </summary>
        [JsonProperty(PropertyName = "tcpSocketProtocol")]
        public TCPSocketProtocolConfig TCPSocketProtocol { get; internal set; }

        /// <summary>WebSocket 通訊協定的設定
        /// </summary>
        [JsonProperty(PropertyName = "webSocketProtocol")]
        public WebSocketProtocolConfig WebSocketProtocol { get; internal set; }
    }
}
