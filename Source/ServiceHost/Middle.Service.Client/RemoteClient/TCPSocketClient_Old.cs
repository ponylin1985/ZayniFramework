﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using ZayniFramework.Common;
using ZayniFramework.Logging;
using ZayniFramework.Middle.Service.Entity;
using ZayniFramework.Serialization;


namespace ZayniFramework.Middle.Service.Client
{
    /// <summary>TCP Socket 客戶端類別
    /// </summary>
    internal sealed class TCPSocketClient_Old : RemoteClient
    {
        #region 宣告私有的欄位

        /// <summary>多執行緒鎖定物件
        /// </summary>
        private readonly object _lockThiz = new object();

        /// <summary>多執行緒鎖定物件
        /// </summary>
        private readonly object _lockMsgBuffers = new object();

        /// <summary>TCP Socket 通訊客戶端的設定組態
        /// </summary>
        private TCPSocketClientConfig _socketClientConfig;

        /// <summary>等待 TCP Socket 服務 RPC 回應訊息的逾時秒數
        /// </summary>
        private int _tcpReponseTimeout;

        /// <summary>TCP Socket 訊息的結束標記字串
        /// </summary>
        private string _tcpMsgEndingToken;

        /// <summary>TCP Socket 網路連線客戶端物件
        /// </summary>
        private TcpClient _tcpClient = null;

        /// <summary>TCP Socket 網路串流物件
        /// </summary>
        private NetworkStream _networkStream = null;

        /// <summary>RPC 請求管理容器
        /// </summary>
        private RPCRequestContainer _requestContainer = new RPCRequestContainer();

        #endregion 宣告私有的欄位


        #region 宣告私有屬性

        /// <summary>TCP Socket 訊息緩存串流
        /// </summary>
        private MemoryStream _buffer = new MemoryStream();

        /// <summary>TCP Socket 訊息緩存串流
        /// </summary>
        private MemoryStream MsgBuffers
        {
            get
            {
                lock ( _lockMsgBuffers )
                {
                    return _buffer;
                }
            }
            set
            {
                lock ( _lockMsgBuffers )
                {
                    _buffer = value;
                }
            }
        }

        #endregion 宣告私有屬性


        #region 宣告建構子

        /// <summary>多載建構子
        /// </summary>
        /// <param name="serviceClientName">服務客戶端個體的設定名稱</param>
        internal TCPSocketClient_Old( string serviceClientName ) : base( serviceClientName )
        {
            // pass
        }

        #endregion 宣告建構子


        #region 實作抽象

        /// <summary>初始化遠端服務連線
        /// </summary>
        /// <returns>連線是否正常建立</returns>
        internal override bool Initialize()
        {
            try
            {
                var svrConfig = ConfigReader.GetServiceClientConfig( ServiceClientName );
                _socketClientConfig = ConfigReader.GetTCPSocketClientConfig( svrConfig );

                _tcpClient = new TcpClient()
                {
                    NoDelay           = true,
                    SendBufferSize    = 1024 * 5,
                    ReceiveBufferSize = 1024 * 5
                };

                if ( _socketClientConfig.TCPSendBufferSize > 0 )
                {
                    _tcpClient.SendBufferSize = 1024 * _socketClientConfig.TCPSendBufferSize;
                }

                if ( _socketClientConfig.TCPReceiveBufferSize > 0 )
                {
                    _tcpClient.SendBufferSize = 1024 * _socketClientConfig.TCPReceiveBufferSize;
                }

                _tcpClient.Client.Connect( _socketClientConfig.TCPServerHost, _socketClientConfig.TCPServerPort );

                _networkStream     = _tcpClient.GetStream();
                _tcpReponseTimeout = _socketClientConfig.TCPResponseTimeout <= 0 ? 30 : _socketClientConfig.TCPResponseTimeout;
                _tcpMsgEndingToken = _socketClientConfig.TCPMsgEndToken.IsNullOrEmptyString( "^C" );
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, "Establish TCP socket connection occur exception.", "ServiceHostTrace" );
                return false;
            }

            try
            {
                Read( _networkStream );
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, "Establish TCP socket connection occur exception.", "ServiceHostTrace" );
                return false;
            }

            Logger.WriteInformationLog( this, $"Establish TCP socket connection to remote host successfully. RemoteHost: {_socketClientConfig.TCPServerHost}:{_socketClientConfig.TCPServerPort}.", nameof ( Initialize ), "ServiceHostTrace" );
            Console.WriteLine( $"Establish TCP socket connection to remote host successfully. RemoteHost: {_socketClientConfig.TCPServerHost}:{_socketClientConfig.TCPServerPort}." );
            return true;
        }

        /// <summary>發送 RPC 遠端請求訊息
        /// </summary>
        /// <param name="requestId">請求識別代碼</param>
        /// <param name="actionName">動作名稱</param>
        /// <param name="requestDataContent">請求資料內容 JSON 集合</param>
        /// <returns>執行結果</returns>
        internal override Result<ResponsePackage> Execute( string requestId, string actionName, string requestDataContent = null )
        {
            #region 初始化回傳值

            var result = new Result<ResponsePackage>()
            {
                IsSuccess = false,
                Data      = null
            };

            #endregion 初始化回傳值

            #region 設定請求包裹

            var requestPackage = new RequestPackage()
            {
                RequestId   = requestId,
                ActionName  = actionName,
                Data        = requestDataContent,
                RequestTime = DateTime.Now
            };

            #endregion 設定請求包裹

            #region 執行遠端服務動作

            ResponsePackage responsePackage;

            try
            {
                responsePackage = Send( requestPackage );
            }
            catch ( Exception ex )
            {
                result.Code    = StatusCode.CLIENT_ERROR;
                result.Message = $"Internal zayni framework service client error.";
                Logger.WriteExceptionLog( this, ex, $"Execute remote action occur exception. RequestId: {requestId}, ActionName: {actionName}.", "ServiceHostTrace" );
                return result;
            }

            #endregion 執行遠端服務動作

            #region 檢查回應包裹

            if ( responsePackage.IsNull() )
            {
                result.Code    = StatusCode.CLIENT_ERROR;
                result.Message = $"Internal zayni framework service client error.";
                Logger.WriteErrorLog( this, $"No response package from remote service. RequestId: {requestId}, ActionName: {actionName}.", nameof ( Execute ), "ServiceHostTrace" );
                return result;
            }

            #endregion 檢查回應包裹

            #region 設定回傳值
            
            result.Data      = responsePackage;
            result.Code      = responsePackage.Code;
            result.Message   = responsePackage.Message;
            result.IsSuccess = responsePackage.IsSuccess;

            #endregion 設定回傳值

            return result;
        }

        #endregion 實作抽象


        #region 宣告私有的方法

        /// <summary>讀取遠端服務的 TCP Socket 網路串流訊息
        /// </summary>
        /// <param name="networkStream">TCP Socket 網路串流</param>
        private void Read( NetworkStream networkStream )
        {
            if ( new object[] { _tcpClient, networkStream }.HasNullElements() )
            {
                Logger.WriteErrorLog( this, $"Read ResponsePackage from TCP socket message fail due to TCP socket network stream is null.", nameof ( Read ), "ServiceHostTrace" );
                return;
            }

            var thread = new Thread( () => 
            {
                while ( true )
                {
                    Thread.Sleep( TimeSpan.FromMilliseconds( 0.5 ) );

                    try
                    {
                        if ( !networkStream.CanRead )
                        {
                            continue;
                        }

                        if ( !networkStream.DataAvailable )
                        {
                            continue;
                        }

                        byte[] receiveBytes = new byte[ _tcpClient.ReceiveBufferSize ];
                        int receiveBytesLength = networkStream.Read( receiveBytes, 0, _tcpClient.ReceiveBufferSize );

                        for ( int i = 0; i < receiveBytesLength; i++ )
                        {
                            MsgBuffers.WriteByte( receiveBytes[ i ] );
                        }

                        byte lastByte = receiveBytes[ receiveBytes.Length - 1 ];

                        if ( 0 != lastByte )
                        {
                            continue;
                        }

                        MsgBuffers.Seek( 0, SeekOrigin.Begin );
                        byte[] receives = new byte[ MsgBuffers.Length ];
                        MsgBuffers.Read( receives, 0, (int)MsgBuffers.Length );
                        string receiveMsg = Encoding.UTF8.GetString( receives )?.Replace( "\0", string.Empty );

                        if ( !receiveMsg.EndsWith( _tcpMsgEndingToken ) )
                        {
                            continue;
                        }

                        MsgBuffers?.Close();
                        MsgBuffers?.Dispose();
                        MsgBuffers = new MemoryStream();

                        string[] messages = receiveMsg.SplitString( _tcpMsgEndingToken );

                        for ( int i = 0, len = messages.Length; i < len; i++ )
                        {
                            string json = messages[ i ]?.Trim();
                            ResponsePackage responsePackage = null;

                            try
                            {
                                responsePackage = JsonConvert.DeserializeObject<ResponsePackage>( json );
                            }
                            catch ( Exception ex )
                            {
                                Logger.WriteExceptionLog( this, ex, $"Read ResponsePackage from TCP socket message occur exception due to deserialize to ResponsePackage fail. Receive TCP socket message:{Environment.NewLine}{messages[ i ]}", "ServiceHostTrace" );
                                continue;
                            }

                            if ( responsePackage.IsNull() )
                            {
                                Logger.WriteErrorLog( this, $"Read ResponsePackage from TCP socket message fail due to retrive null value of ResponsePackage.", nameof ( Read ), "ServiceHostTrace" );
                                continue;
                            }

                            string requestId = responsePackage.RequestId;

                            if ( requestId.IsNullOrEmpty() )
                            {
                                Logger.WriteErrorLog( this, $"Read ResponsePackage from TCP socket message fail due to invalid ResponsePackage. RequestId is null or empty string. Receive ResponsePackage:{Environment.NewLine}{JsonConvertUtil.Serialize( responsePackage )}", nameof ( Read ), "ServiceHostTrace" );
                                continue;
                            }

                            if ( !_requestContainer.Contains( requestId ) )
                            {
                                Logger.WriteErrorLog( this, $"Read ResponsePackage from TCP socket message fail due to invalid ResponsePackage. Can not found RequestId: {requestId}.", nameof ( Read ), "ServiceHostTrace" );
                                continue;
                            }
                    
                            if ( !_requestContainer.Get( requestId, out RPCRequest rpcRequest ) )
                            {
                                Logger.WriteErrorLog( this, $"Get RPC request from container fail. Retrive ResponsePackage is:{Environment.NewLine}{JsonConvertUtil.Serialize( responsePackage )}", nameof ( Read ), "ServiceHostTrace" );
                                continue;
                            }

                            if ( rpcRequest.IsNull() )
                            {
                                Logger.WriteErrorLog( this, $"Read ResponsePackage from TCP socket message occur exception due get TcpRequestPackage from request pool fail. Receive null TcpRequestPackage. Retrive ResponsePackage is:{Environment.NewLine}{JsonConvertUtil.Serialize( responsePackage )}", nameof ( Read ), "ServiceHostTrace" );
                                continue;
                            }

                            lock ( _lockThiz )
                            {
                                rpcRequest.Result = responsePackage;
                                rpcRequest.ReceiveEvent?.Set();
                            }
                        }
                    }
                    catch ( Exception ex )
                    {
                        MsgBuffers = new MemoryStream();
                        Logger.WriteExceptionLog( this, ex, $"Read ResponsePackage message from TCP socket network stream occur exception.", "ServiceHostTrace" );
                        continue;
                    }
                }
            } );

            thread.Start();
        }

        /// <summary>發送請求包裹的 TCP Socket 網路串流訊息
        /// </summary>
        /// <param name="requestPackage">遠端服務請求包裹</param>
        private ResponsePackage Send( RequestPackage requestPackage )
        {
            #region 檢查 TCP 網路串流狀態

            if ( new object[] { _tcpClient, _networkStream }.HasNullElements() )
            {
                Logger.WriteErrorLog( this, $"Read RequestPackage to TCP socket message fail due to TCP socket network stream is null.", nameof ( Send ), "ServiceHostTrace" );
                return null;
            }

            #endregion 檢查 TCP 網路串流狀態

            #region 檢查 TCP Socket 連線

            if ( !IsConnectionAlive() )
            {
                bool reconnectSuccess = Initialize();

                if ( !reconnectSuccess )
                {
                    Logger.WriteErrorLog( this, $"Can not send TCP socket message to remote service host due to TCP socket connection is broken and reconnect is still fail. Remote service host IP address: {_socketClientConfig.TCPServerHost}:{_socketClientConfig.TCPServerPort}.", nameof ( Send ), "ServiceHostTrace" );

                    return new ResponsePackage()
                    {
                        ActionName = requestPackage.ActionName,
                        RequestId  = requestPackage.RequestId,
                        Code       = StatusCode.CONNECTION_ERROR,
                        Message    = $"TCP socket connection between remote service host is broken."
                    };
                }

                Thread.Sleep( 1 );
            }

            #endregion 檢查 TCP Socket 連線

            #region 新增 TCP Socket 請求訊息

            RPCRequest request;

            try
            {
                request = requestPackage.ConvertTo<RPCRequest>();
                request.ReceiveEvent = new AutoResetEvent( false );

                if ( !_requestContainer.Add( requestPackage.RequestId, request ) )
                {
                    Logger.WriteErrorLog( this, $"Add RPC request to container fail. RequestId: {requestPackage.RequestId}, ActionName: {requestPackage.ActionName}.", nameof ( Execute ), "ServiceHostTrace" );
                    return null;
                }
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, $"Send RequestPackage to from TCP socket network stream occur exception due to add TcpRequestPackage to request pool occur exception.", "ServiceHostTrace" );
                return null;
            }

            #endregion 新增 TCP Socket 請求訊息

            #region 序列化 TCP Socket 請求訊息

            byte[] messageBytes = null;

            try
            {
                string json  = $"{JsonConvert.SerializeObject( requestPackage )}{_tcpMsgEndingToken}";
                messageBytes = Encoding.UTF8.GetBytes( json );
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, $"Send RequestPackage to TCP socket network stream occur exception due to serialize RequestPackage object occur exception.", "ServiceHostTrace" );
                return null;
            }

            #endregion 序列化 TCP Socket 請求訊息

            #region 發送 TCP Socket RPC 請求訊息

            try
            {
                // 第一種寫法:
                //_tcpClient.Client.Send( messageBytes );

                // 第二種寫法:
                _networkStream.Write( messageBytes, 0, messageBytes.Length );
                _networkStream.Flush();
            }
            catch ( Exception ex )
            {
                _requestContainer.Remove( request.RequestId );
                Logger.WriteExceptionLog( this, ex, $"Send RequestPackage to TCP socket network stream occur exception. Write TCP socket message to network stream occur exception.", "ServiceHostTrace" );
                return null;
            }

            #endregion 發送 TCP Socket RPC 請求訊息

            #region 取得 TCP Socket RPC 遠端回應訊息包裹

            request.ReceiveEvent?.Reset();
            bool isTimeout = !request.ReceiveEvent.WaitOne( TimeSpan.FromSeconds( _tcpReponseTimeout ) );

            if ( isTimeout )
            {
                _requestContainer.Remove( request.RequestId );
                Logger.WriteErrorLog( this, $"Waiting for remote host reply ResponsePackage TCP socket timeout. RequestId: {requestPackage.RequestId}, ActionName: {requestPackage.ActionName}.", nameof ( Send ), "ServiceHostTrace" );
                return null;
            }

            if ( !_requestContainer.Get( request.RequestId, out RPCRequest rpcRequest ) )
            {
                _requestContainer.Remove( request.RequestId );
                Logger.WriteErrorLog( this, $"Get RPC request from container fail. RequestId: {requestPackage.RequestId}, ActionName: {requestPackage.ActionName}.", nameof ( Execute ), "ServiceHostTrace" );
                return null;
            }

            if ( rpcRequest.IsNull() )
            {
                _requestContainer.Remove( request.RequestId );
                Logger.WriteErrorLog( this, $"TcpRequestPackage from request pool is null. RequestId: {requestPackage.RequestId}, ActionName: {requestPackage.ActionName}.", nameof ( Send ), "ServiceHostTrace" );
                return null;
            }

            ResponsePackage responsePackage = rpcRequest.Result;

            if ( responsePackage.IsNull() )
            {
                _requestContainer.Remove( request.RequestId );
                Logger.WriteErrorLog( this, $"Retrive ResponsePackage object is null from remote host. RequestId: {requestPackage.RequestId}, ActionName: {requestPackage.ActionName}.", nameof ( Send ), "ServiceHostTrace" );
                return null;
            }

            _requestContainer.Remove( request.RequestId );

            #endregion 取得 TCP Socket RPC 遠端回應訊息包裹

            return responsePackage;
        }

        /// <summary>檢查目前的 TCP Socket 連線是否正常連線中
        /// </summary>
        /// <returns>TCP Socket 連線是否正常</returns>
        private bool IsConnectionAlive()
        {
            if ( null == _tcpClient )
            {
                Logger.WriteWarningLog( this, $"ServiceClient '{ServiceClientName}' - TCP socket connection between remote service host is broken. Remote serivce host IP address: {_socketClientConfig.TCPServerHost}:{_socketClientConfig.TCPServerPort}.", nameof ( IsConnectionAlive ), "ServiceHostTrace" );
                return false;
            }

            if ( !_tcpClient.Connected )
            {
                Logger.WriteWarningLog( this, $"ServiceClient '{ServiceClientName}' - TCP socket connection between remote service host is broken. Remote serivce host IP address: {_socketClientConfig.TCPServerHost}:{_socketClientConfig.TCPServerPort}.", nameof ( IsConnectionAlive ), "ServiceHostTrace" );
                return false;
            }

            try
            {
                if ( !_tcpClient.Client.Poll( 0, SelectMode.SelectWrite ) )
                {
                    Logger.WriteWarningLog( this, $"ServiceClient '{ServiceClientName}' - TCP socket connection between remote service host is broken. Remote serivce host IP address: {_socketClientConfig.TCPServerHost}:{_socketClientConfig.TCPServerPort}.", nameof ( IsConnectionAlive ), "ServiceHostTrace" );
                    return false;
                }

                if ( _tcpClient.Client.Poll( 0, SelectMode.SelectRead ) )
                {
                    if ( 0 == _tcpClient.Client.Receive( new byte[ 1 ], SocketFlags.Peek ) )
                    {
                        Logger.WriteWarningLog( this, $"ServiceClient '{ServiceClientName}' - TCP socket connection between remote service host is broken. Remote serivce host IP address: {_socketClientConfig.TCPServerHost}:{_socketClientConfig.TCPServerPort}.", nameof ( IsConnectionAlive ), "ServiceHostTrace" );
                        return false;
                    }
                }
            }
            catch ( Exception ex )
            {
                Logger.WriteWarningLog( this, $"Check TCP socket connection occur exception. Remote serivce host IP address: {_socketClientConfig.TCPServerHost}:{_socketClientConfig.TCPServerPort}. {Environment.NewLine}{ex.ToString()}", nameof ( IsConnectionAlive ), "ServiceHostTrace" );
                return false;
            }

            return true;
        }

        #endregion 宣告私有的方法
    }
}
