﻿using NeoSmart.AsyncLock;
using System;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using WebSocketSharp;
using ZayniFramework.Common;
using ZayniFramework.Middle.Service.Entity;
using ZayniFramework.Serialization;
using STimer = System.Timers.Timer;
using ZLog = ZayniFramework.Logging.Logger;


namespace ZayniFramework.Middle.Service.Client
{
    /// <summary>WebSocket 客戶端類別
    /// </summary>
    internal sealed class WebSocketClient : RemoteClient
    {
        #region 宣告私有的欄位

        /// <summary>非同步作業鎖定物件
        /// </summary>
        private readonly AsyncLock _asyncLock = new();

        /// <summary>非同步作業鎖定物件: 鎖定 _timer 欄位
        /// </summary>
        private readonly AsyncLock _asyncLockTimer = new();

        /// <summary>服務客戶端個體的設定組態
        /// </summary>
        private ServiceClientConfig _serviceClientConfig;

        /// <summary>WebSocket 客戶端連線組態設定
        /// </summary>
        private WebSocketClientConfig _webSocketCfg;

        /// <summary>WebSocket 通訊物件
        /// </summary>
        private WebSocket _websocket;

        /// <summary>WebSocket 連線狀態定時檢查器
        /// </summary>
        private STimer _timer = new();

        /// <summary>RPC 請求管理容器
        /// </summary>
        private readonly RPCRequestContainer _requestContainer = new();

        #endregion 宣告私有的欄位


        #region 宣告建構子

        /// <summary>多載建構子
        /// </summary>
        /// <param name="serviceClientName">服務客戶端個體的設定名稱</param>
        public WebSocketClient(string serviceClientName) : base(serviceClientName)
        {
            _timer.Interval = TimeSpan.FromSeconds(3).TotalMilliseconds;
            _timer.Elapsed += CheckConnection;
        }

        /// <summary>解構子
        /// </summary>
        ~WebSocketClient()
        {
            _timer.Elapsed -= CheckConnection;
            _timer = null;
        }

        #endregion 宣告建構子


        #region 實作抽象

        /// <summary>初始化遠端服務連線
        /// </summary>
        /// <returns>連線是否正常建立</returns>
        public override bool Initialize()
        {
            try
            {
                _serviceClientConfig = ClientConfigReader.GetServiceClientConfig(ServiceClientName);
                _webSocketCfg = ClientConfigReader.GetWebSocketClientConfig(_serviceClientConfig);

                var remoteHostUrl = $"{_webSocketCfg.WebSocketHostBaseUrl}/service";
                _websocket = new WebSocket(remoteHostUrl);
                _websocket.OnOpen += OnOpen;
                _websocket.OnClose += OnClose;
                _websocket.OnMessage += OnMessage;
                _websocket.OnError += OnError;
                _websocket.Connect();

                if (!_websocket.IsAlive)
                {
                    _websocket.OnOpen -= OnOpen;
                    _websocket.OnClose -= OnClose;
                    _websocket.OnMessage -= OnMessage;
                    _websocket.OnError -= OnError;
                    return false;
                }

                _timer.Start();
            }
            catch (Exception ex)
            {
                _timer.Start();
                ZLog.Exception(this, ex, nameof(Initialize), "Establish WebSocket connection occur exception.", "ServiceHostTrace");
                return false;
            }

            return true;
        }

        /// <summary>發送 RPC 遠端請求訊息
        /// </summary>
        /// <param name="requestId">請求識別代碼</param>
        /// <param name="actionName">動作名稱</param>
        /// <param name="requestDataContent">請求資料內容 JSON 集合</param>
        /// <returns>執行結果</returns>
        public override Result<ResponsePackage> Execute(string requestId, string actionName, string requestDataContent = null)
        {
            var result = Result.Create<ResponsePackage>();

            #region 設定請求包裹

            var requestPackage = new RequestPackage()
            {
                PackageType = PackageType.RPCRequest,
                ServiceName = _serviceClientConfig.RemoteServiceName,
                RequestId = requestId,
                ActionName = actionName,
                Data = requestDataContent,
                RequestTime = DateTime.UtcNow
            };

            ServiceClientActionLogger.Log(ServiceClientName, requestPackage);

            #endregion 設定請求包裹

            #region 檢查 WebSocket 連線

            if (!_websocket.IsAlive)
            {
                result.Code = StatusCode.CONNECTION_ERROR;
                result.Message = $"Can not send RPC request due to the WebSocket connection between remote service host is broken. Remote WebSocket Host: {_webSocketCfg.WebSocketHostBaseUrl}/service";
                ZLog.Error(this, result.Message, nameof(Execute), "ServiceHostTrace");
                return result;
            }

            #endregion 檢查 WebSocket 連線

            #region 新增 WebSocekt RPC 請求包裹

            RPCRequest request;

            try
            {
                request = requestPackage.ConvertTo<RPCRequest>();
                request.ReceiveEvent = new AutoResetEvent(false);

                if (!_requestContainer.Add(requestPackage.RequestId, request))
                {
                    result.Code = StatusCode.CLIENT_ERROR;
                    result.Message = $"Add WebSocket RPC request to container fail.";
                    ZLog.Error(this, $"Add RPC request to container fail. RequestId: {requestPackage.RequestId}, ActionName: {requestPackage.ActionName}.", nameof(Execute), "ServiceHostTrace");
                    return result;
                }
            }
            catch (Exception ex)
            {
                result.Code = StatusCode.CLIENT_ERROR;
                result.Message = $"Add WebSocket RPC request to container occur exeption.";
                ZLog.Exception(this, ex, nameof(Execute), $"Send RequestPackage to occur exception due to add RequestPackage to request pool occur exception. RequestId: {requestPackage.RequestId}, ActionName: {requestPackage.ActionName}.", "ServiceHostTrace");
                return result;
            }

            #endregion 新增 WebSocekt RPC 請求包裹

            #region 執行遠端服務動作

            try
            {
                var json = JsonSerializer.Serialize(requestPackage);
                _websocket.Send(json);
            }
            catch (Exception ex)
            {
                result.Code = StatusCode.CLIENT_ERROR;
                result.Message = $"Send RequestPackage to remote WebSocket service host occur exception. RequestId: {requestPackage.RequestId}, ActionName: {requestPackage.ActionName}.";
                _requestContainer.Remove(request.RequestId);
                ZLog.Exception(this, ex, nameof(Execute), $"Execute remote action occur exception. RequestId: {requestId}, ActionName: {actionName}.", "ServiceHostTrace");
                return result;
            }

            #endregion 執行遠端服務動作

            #region 取得回應包裹

            request.ReceiveEvent?.Reset();
            var isTimeout = !request.ReceiveEvent.WaitOne(TimeSpan.FromSeconds(_webSocketCfg.ResponseTimeout));

            if (isTimeout)
            {
                result.Code = StatusCode.CLIENT_RPC_TIMEOUT;
                result.Message = $"Waiting for remote host reply ResponsePackage timeout. RequestId: {requestPackage.RequestId}.";
                _requestContainer.Remove(request.RequestId);
                ZLog.Error(this, $"Waiting for remote host reply ResponsePackage timeout. RequestId: {requestPackage.RequestId}, ActionName: {requestPackage.ActionName}.", nameof(Execute), "ServiceHostTrace");
                return result;
            }

            if (!_requestContainer.Get(requestId, out var rpcRequest))
            {
                result.Code = StatusCode.CLIENT_ERROR;
                result.Message = $"Get RPC response occur error.";
                _requestContainer.Remove(request.RequestId);
                ZLog.Error(this, $"Get RPC request from container fail. RequestId: {requestPackage.RequestId}, ActionName: {requestPackage.ActionName}.", nameof(Execute), "ServiceHostTrace");
                return result;
            }

            if (rpcRequest.IsNull())
            {
                result.Code = StatusCode.CLIENT_ERROR;
                result.Message = $"Get RPC response occur error. RequestId: {requestPackage.RequestId}.";
                _requestContainer.Remove(request.RequestId);
                ZLog.Error(this, $"RPC request from container is null. RequestId: {requestPackage.RequestId}, ActionName: {requestPackage.ActionName}.", nameof(Execute), "ServiceHostTrace");
                return result;
            }

            var responsePackage = rpcRequest.Result;

            if (responsePackage.IsNull())
            {
                result.Code = StatusCode.CLIENT_RPC_INVALID_RES;
                result.Message = $"Retrive invalid RPC response. ResponsePackage is null. RequestId: {requestPackage.RequestId}.";
                _requestContainer.Remove(request.RequestId);
                ZLog.Error(this, $"Retrive ResponsePackage object is null from remote host. RequestId: {requestPackage.RequestId}, ActionName: {requestPackage.ActionName}.", nameof(Execute), "ServiceHostTrace");
                return result;
            }

            _requestContainer.Remove(request.RequestId);

            #endregion 取得回應包裹

            #region 設定回傳值

            if (!responsePackage.Success)
            {
                result.Code = responsePackage.Code;
                result.Message = responsePackage.Message;
                return result;
            }

            result.Data = responsePackage;
            result.Message = responsePackage.Message;
            result.Success = responsePackage.Success;

            #endregion 設定回傳值

            return result;
        }

        /// <summary>非同步作業發送 RPC 遠端請求訊息
        /// </summary>
        /// <param name="requestId">請求識別代碼</param>
        /// <param name="actionName">動作名稱</param>
        /// <param name="requestDataContent">請求資料內容 JSON 集合</param>
        /// <returns>執行結果</returns>
        public override Task<Result<ResponsePackage>> ExecuteAsync(string requestId, string actionName, string requestDataContent = null) =>
            Task.FromResult(Execute(requestId, actionName, requestDataContent));

        /// <summary>發佈訊息至遠端服務
        /// </summary>
        /// <param name="messageId">訊息識別代碼</param>
        /// <param name="actionName">動作名稱</param>
        /// <param name="messageContent">訊息資料內容 JSON 集合</param>
        /// <returns>發佈結果</returns>
        public override Result Publish(string messageId, string actionName, string messageContent = null)
        {
            var result = Result.Create();

            #region 設定請求包裹

            var messagePackage = new MessagePackage()
            {
                PackageType = PackageType.Publish,
                ServiceName = _serviceClientConfig.RemoteServiceName,
                MessageId = messageId,
                ActionName = actionName,
                Data = messageContent,
                PublishTime = DateTime.UtcNow
            };

            #endregion 設定請求包裹

            #region 檢查 WebSocket 連線

            if (!_websocket.IsAlive)
            {
                result.Code = StatusCode.CONNECTION_ERROR;
                result.Message = $"Can not publish message package due to the WebSocket connection between remote service host is broken. Remote WebSocket Host: {_webSocketCfg.WebSocketHostBaseUrl}/service";
                ZLog.Error(this, result.Message, nameof(Execute), "ServiceHostTrace");
                return result;
            }

            #endregion 檢查 WebSocket 連線

            #region 發佈事件訊息

            try
            {
                var json = JsonSerializer.Serialize(messagePackage);
                _websocket.Send(json);
            }
            catch (Exception ex)
            {
                result.Code = StatusCode.CLIENT_ERROR;
                result.Message = $"Internal zayni framework service client error.";
                ZLog.Exception(this, ex, nameof(Publish), $"Publish message package to remote service occur exception. MessageId: {messageId}, ActionName: {actionName}.", "ServiceHostTrace");
                return result;
            }

            #endregion 發佈事件訊息

            result.Success = true;
            return result;
        }

        #endregion 實作抽象


        #region 宣告私有的方法

        /// <summary>當 WebSocket 連線成功建立的處理
        /// </summary>
        /// <param name="sender">觸發事件來源物件</param>
        /// <param name="e">事件參數</param>
        private void OnOpen(object sender, EventArgs e)
        {
            // pass
        }

        /// <summary>當 WebSocket 連線關閉的處理
        /// </summary>
        /// <param name="sender">觸發事件來源物件</param>
        /// <param name="e">事件參數</param>
        private void OnClose(object sender, CloseEventArgs e)
        {
            ZLog.Warn(sender, $"{nameof(WebSocketClient)}, websocket connection is broken. ServiceClientName: {ServiceClientName}, WebSocket Service Host Url: {_webSocketCfg.WebSocketHostBaseUrl}/service", nameof(OnError), "ServiceHostTrace");
            HandlerInvoker.Invoke(ConnectionBroken);
        }

        /// <summary>接收到 WebSocket 通訊的訊息的處理
        /// </summary>
        /// <param name="sender">觸發事件來源物件</param>
        /// <param name="e">訊息事件參數</param>
        private void OnMessage(object sender, MessageEventArgs e)
        {
            if (!e.IsText)
            {
                return;
            }

            var json = e.Data;

            if (json.IsNullOrEmpty())
            {
                ZLog.Error(this, $"Retrive null or empty message from remotre WebSocket host.", nameof(OnMessage), "ServiceHostTrace");
                return;
            }

            var g = GetMessagePackage(json);

            if (!g.Success)
            {
                ZLog.Error(this, $"Read message package from WebSocket message fail. {Environment.NewLine}{g.Message}", nameof(OnMessage), "ServiceHostTrace");
                return;
            }

            var package = g.Data;

            var r = Process(package);

            if (!r.Success)
            {
                ZLog.Error(this, $"Process message package from WebSocket message fail. {Environment.NewLine}{r.Message}", nameof(OnMessage), "ServiceHostTrace");
                return;
            }
        }

        /// <summary>WebSocket 通訊發生異常的處理
        /// </summary>
        /// <param name="sender">觸發事件來源物件</param>
        /// <param name="e">異常事件參數</param>
        private void OnError(object sender, ErrorEventArgs e)
        {
            ZLog.Error(sender, $"{nameof(WebSocketClient)} occur error. {Environment.NewLine}{e.Message}", nameof(OnError), "ServiceHostTrace");
            e.Exception.IsNotNull(ex => ZLog.Exception(sender, e.Exception, $"{nameof(WebSocketClient)} occur exception. {Environment.NewLine}{e.Message}", "ServiceHostTrace"));
        }

        /// <summary>定時檢查 WebSocket 連線
        /// </summary>
        /// <param name="sender">事件觸發物件</param>
        /// <param name="e">定時器事件參數</param>
        private void CheckConnection(object sender, System.Timers.ElapsedEventArgs e)
        {
            using (_asyncLockTimer.Lock())
            {
                if (_websocket.IsAlive)
                {
                    return;
                }

                _websocket.OnOpen -= OnOpen;
                _websocket.OnClose -= OnClose;
                _websocket.OnMessage -= OnMessage;
                _websocket.OnError -= OnError;

                if (!Initialize())
                {
                    var errorMsg = $"ServiceClient '{ServiceClientName}' WebSocket connection timer reconnect to remote service host fail. Remote WebSocket serivce host URL: {_websocket.Url}.";
                    ZLog.Error(this, errorMsg, nameof(CheckConnection), "ServiceHostTrace");
                    return;
                }

                var message = $"ServiceClient '{ServiceClientName}' WebSocket connection timer reconnect to remote service host success. Remote WebSocket serivce host URL: {_websocket.Url}.";
                ZLog.Info(this, message, nameof(CheckConnection), "ServiceHostTrace");
                HandlerInvoker.Invoke(ConnectionRestored);
            }
        }

        /// <summary>取得通訊資料包裹
        /// </summary>
        /// <param name="json">通訊 JSON 資料字串</param>
        /// <returns>取得結果</returns>
        private Result<IPackage> GetMessagePackage(string json)
        {
            var result = Result.Create<IPackage>();
            IPackage messagePackage;

            try
            {
                var package = JsonSerializer.Deserialize(json, typeof(object));
                var jsonElement = (JsonElement)package;
                var packageType = jsonElement.GetProperty("packageType").GetString(); ;

                messagePackage = packageType switch
                {
                    PackageType.Publish => JsonSerializer.Deserialize<MessagePackage>(json),
                    _ => JsonSerializer.Deserialize<ResponsePackage>(json),
                };
            }
            catch (Exception ex)
            {
                result.Message = $"Deserialize receive JSON data to {nameof(IPackage)} object occur exception. {Environment.NewLine}{ex}";
                return result;
            }

            if (messagePackage.IsNull())
            {
                result.Message = $"Deserialize receive JSON data fail due to result object is null.";
                return result;
            }

            result.Data = messagePackage;
            result.Success = true;
            return result;
        }

        /// <summary>處理訊息包裹
        /// </summary>
        /// <param name="package">通訊資料包裹</param>
        /// <returns>處理結果</returns>
        private Result Process(IPackage package)
        {
            var result = Result.Create();

            if (package.IsNull())
            {
                result.Success = true;
                return result;
            }

            return package switch
            {
                ResponsePackage requestPackage => Process(requestPackage),
                MessagePackage messagePackage => Process(messagePackage),
                _ => Process((ResponsePackage)package),
            };
        }

        /// <summary>處理客戶端的請求包裹
        /// </summary>
        /// <param name="responsePackage">服務回應包裹</param>
        /// <returns>處理結果</returns>
        private Result Process(ResponsePackage responsePackage)
        {
            var result = Result.Create();

            if (responsePackage.IsNull())
            {
                ZLog.Error(this, $"Read ResponsePackage from WebSocket message fail due to ResponsePackage is null.", nameof(OnMessage), "ServiceHostTrace");
                return result;
            }

            var requestId = responsePackage.RequestId;

            if (requestId.IsNullOrEmpty())
            {
                ZLog.Error(this, $"Read ResponsePackage from WebSocket message fail due to invalid ResponsePackage. RequestId is null or empty string. Receive ResponsePackage:{Environment.NewLine}{NewtonsoftJsonConvert.Serialize(responsePackage)}", nameof(OnMessage), "ServiceHostTrace");
                return result;
            }

            if (!_requestContainer.Contains(requestId))
            {
                ZLog.Error(this, $"Read ResponsePackage from WebSocket message fail due to invalid ResponsePackage. Can not found RequestId: {requestId}.", nameof(OnMessage), "ServiceHostTrace");
                return result;
            }

            if (!_requestContainer.Get(requestId, out var rpcRequest))
            {
                ZLog.Error(this, $"Get RpcRequest from container fail. Retrive ResponsePackage is:{Environment.NewLine}{NewtonsoftJsonConvert.Serialize(responsePackage)}", nameof(OnMessage), "ServiceHostTrace");
                return result;
            }

            if (rpcRequest.IsNull())
            {
                ZLog.Error(this, $"Read ResponsePackage from WebSocket message occur exception due get TcpRequestPackage from request pool fail. Receive null TcpRequestPackage. Retrive ResponsePackage is:{Environment.NewLine}{NewtonsoftJsonConvert.Serialize(responsePackage)}", nameof(OnMessage), "ServiceHostTrace");
                return result;
            }

            using (_asyncLock.Lock())
            {
                rpcRequest.Result = responsePackage;
                rpcRequest.ReceiveEvent?.Set();
            }

            result.Success = true;
            return result;
        }

        /// <summary>處理客戶端觸發的事件訊息包裹
        /// </summary>
        /// <param name="messagePackage">通訊訊息資料包裹</param>
        /// <returns>處理結果</returns>
        private Result Process(MessagePackage messagePackage)
        {
            var result = Result.Create();

            try
            {
                if (messagePackage.IsNull())
                {
                    result.Message = $"Read MessagePackage from TCP socket message fail due to retrive null value of MessagePackage.";
                    return result;
                }

                var eventProcess = base.EventHandlers.Get(messagePackage.ActionName);

                if (eventProcess.IsNull())
                {
                    result.Message = $"Read MessagePackage from TCP socket message fail due to retrive no relative EventProcessor register. Receive action name: {messagePackage.ActionName}.";
                    return result;
                }

                var data = messagePackage.Data;

                if (data.IsNotNullOrEmpty())
                {
                    eventProcess.RawData = JsonSerializer.Deserialize(messagePackage.Data, eventProcess.DataType);
                }

                ServiceClientActionLogger.Log(ServiceClientName, 4, messagePackage);
                eventProcess.Execute();
            }
            catch (Exception ex)
            {
                result.Message = $"Process MessagePackage from TCP socke message ocur excepion. Receive MessagePackage:{Environment.NewLine}{NewtonsoftJsonConvert.Serialize(messagePackage)}{Environment.NewLine}{ex}";
                return result;
            }

            result.Success = true;
            return result;
        }



        #endregion 宣告私有的方法
    }
}
