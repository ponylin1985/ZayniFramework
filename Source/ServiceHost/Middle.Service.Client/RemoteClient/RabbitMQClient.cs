﻿using RabbitMQ.Message.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Common.Tasks;
using ZayniFramework.Logging;
using ZayniFramework.Middle.Service.Entity;
using ZayniFramework.Serialization;


namespace ZayniFramework.Middle.Service.Client
{
    /// <summary>RabbitMQ 通訊客戶端類別
    /// </summary>
    internal sealed class RabbitMQClient : RemoteClient
    {
        #region 宣告私有的欄位

        /// <summary>服務客戶端個體的設定組態
        /// </summary>
        private ServiceClientConfig _serviceClientConfig;

        /// <summary>等待 RPC 回應訊息的逾時秒數
        /// </summary>
        private int _rpcTimeout;

        /// <summary>RabbitMQ 服務的客戶端物件
        /// </summary>
        private IServiceEvent _messageClient;

        /// <summary>事件訊息處理佇列
        /// </summary>
        private readonly TaskQueue _messageQueue = new(false);

        /// <summary>訊息資料內容的格式
        /// </summary>
        private static readonly string _contentType = "application/json";

        /// <summary>發送 RPC 請求的 RoutingKey 前綴字
        /// </summary>
        private string _rpcRequestRoutingKey;

        /// <summary>RabbitMQ 發佈訊息的 RoutingKey 前綴字
        /// </summary>
        private string _publishRoutingKey;

        /// <summary>接收到訊息的 RoutingKey 前綴字
        /// </summary>
        private string _receiveMsgRoutingKey;

        #endregion 宣告私有的欄位


        #region 宣告建構子

        /// <summary>多載建構子
        /// </summary>
        /// <param name="serviceClientName">服務客戶端個體的設定名稱</param>
        public RabbitMQClient(string serviceClientName) : base(serviceClientName)
        {
        }

        #endregion 宣告建構子


        #region 實作抽象

        /// <summary>初始化遠端服務連線
        /// </summary>
        /// <returns>連線是否正常建立</returns>
        public override bool Initialize()
        {
            try
            {
                var svrConfig = ClientConfigReader.GetServiceClientConfig(ServiceClientName);
                var cfg = ClientConfigReader.GetRabbitMQClientConfig(svrConfig);

                _serviceClientConfig = svrConfig;
                var module = new RabbitMQModule();

                // 此為主動發佈事件訊息時，預設的目標 Exchange 名稱，目前此設定值不會影響 RPC 請求與回應。
                module.SetDefaultExchange(cfg.DefaultExchange);

                // 作為 Producer 主動發佈事件訊息的目標 Exchange 使用。
                if (cfg.PublishConfig.IsNotNull())
                {
                    module.SetRoutingExchangeMapper(cfg.PublishConfig.RoutingKey, cfg.PublishConfig.Exchange);
                }

                // 作為 RPC 遠端呼叫請求端的設定: 主要設定監聽的 ReplyQueue 名稱，目前 RPC Exchange 和 RoutingKey 沒有特別作用。
                if (cfg.RpcConfig.IsNotNull())
                {
                    module.SetRoutingExchangeMapper(cfg.RpcConfig.RoutingKey, cfg.RpcConfig.Exchange);
                    module.SetDefaultRPCReplyQueue(cfg.RpcConfig?.RpcReplyQueue);
                }

                module.SetConnectionPolicy(RabbitMQConnectionPolicy.CreatePolicy(cfg.MbHost, cfg.MbPort, cfg.MbUserId, cfg.MbPassword, cfg.MbVHost));

                _messageClient = ServiceEventFactory.Create(module);
                _rpcTimeout = Convert.ToInt32(cfg.RpcConfig?.RpcResponseTimeout ?? 30);
                _rpcRequestRoutingKey = cfg.RpcConfig?.RoutingKey;
                _publishRoutingKey = cfg.PublishConfig?.RoutingKey;

                if (cfg.ListenConfig.IsNotNull())
                {
                    // 支援 auto-scaling 機制: 監聽指定的 Exchange 訊息，動態產生 Consumer 的 Queue 接收 Exchange 的訊息。
                    if (cfg.ListenConfig.ListenExchange.IsNotNullOrEmpty() && cfg.ListenConfig.ListenQueue.IsNullOrEmpty())
                    {
                        _messageClient.ListenExchange(cfg.ListenConfig.ListenQueuePrefix, cfg.ListenConfig.ListenExchange, $"{cfg.ListenConfig.ListenExchangeRoutingKey}*", ProcessMessage);
                        _receiveMsgRoutingKey = cfg.ListenConfig.ListenExchangeRoutingKey;
                    }
                    // 不支援 auto-scling 機制: 直接指定監聽 ListenQueue 的訊息。
                    else if (cfg.ListenConfig.ListenQueue.IsNotNullOrEmpty() && cfg.ListenConfig.ListenExchange.IsNullOrEmpty())
                    {
                        _messageClient.Listen(cfg.ListenConfig.ListenQueue, ProcessMessage);
                        _receiveMsgRoutingKey = cfg.ListenConfig.ListenRoutingKey;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Exception(this, ex, nameof(Initialize), "Establish message broker connection occur exception.", "ServiceHostTrace");
                return false;
            }

            return true;
        }

        /// <summary>發送 RPC 遠端請求訊息
        /// </summary>
        /// <param name="requestId">請求識別代碼</param>
        /// <param name="actionName">動作名稱</param>
        /// <param name="requestDataContent">請求資料內容 JSON 集合</param>
        /// <returns>執行結果</returns>
        public override Result<ResponsePackage> Execute(string requestId, string actionName, string requestDataContent = null)
        {
            var result = Result.Create<ResponsePackage>();

            #region 設定請求包裹

            var requestPackage = new RequestPackage()
            {
                ServiceName = ClientConfigReader.GetServiceClientConfig(ServiceClientName)?.RemoteServiceName,
                RequestId = requestId,
                ActionName = actionName,
                Data = requestDataContent,
                RequestTime = DateTime.UtcNow
            };

            ServiceClientActionLogger.Log(ServiceClientName, requestPackage);

            #endregion 設定請求包裹

            #region 執行遠端服務動作

            List<IEventMessage> responses = null;

            try
            {
                var timeout = (ushort)(1000 * _rpcTimeout);
                var routingKey = $"{_rpcRequestRoutingKey}{actionName}";
                responses = _messageClient.RPC(routingKey, _contentType, requestDataContent, requestId, requestPackage.RequestTime.ToUnixUtcTimestamp(), timeout, true);
            }
            catch (Exception ex)
            {
                result.Code = StatusCode.CLIENT_ERROR;
                result.Message = $"Send RequestPackage to remote service host occur exception. RequestId: {requestPackage.RequestId}, ActionName: {requestPackage.ActionName}.";
                Logger.Exception(this, ex, nameof(Initialize), $"Execute remote action occur exception. RequestId: {requestId}, ActionName: {actionName}.", "ServiceHostTrace");
                return result;
            }

            #endregion 執行遠端服務動作

            #region 檢查動作執行結果

            if (responses.IsNullOrEmpty())
            {
                result.Code = StatusCode.CLIENT_RPC_TIMEOUT;
                result.Message = $"Waiting for remote host reply ResponsePackage timeout. RequestId: {requestPackage.RequestId}.";
                Logger.Error(this, $"Waiting for remote host reply ResponsePackage timeout. RequestId: {requestPackage.RequestId}, ActionName: {requestPackage.ActionName}.", nameof(Execute), "ServiceHostTrace");
                return result;
            }

            var response = responses.FirstOrDefault();

            if (response.IsNull())
            {
                result.Code = StatusCode.CLIENT_RPC_INVALID_RES;
                result.Message = $"Get RPC response occur error.";
                Logger.Error(this, $"No RPC response from remote service. RequestId: {requestId}, ActionName: {actionName}.", nameof(Execute), "ServiceHostTrace");
                return result;
            }

            var resDataContent = response.GetBody();

            if (resDataContent.IsNullOrEmpty())
            {
                result.Code = StatusCode.CLIENT_RPC_INVALID_RES;
                result.Message = $"Get RPC response occur error. Retrive ResponsePackage is null. RequestId: {requestId}.";
                Logger.Error(this, $"No ResponsePackage from remote service. RequestId: {requestId}, ActionName: {actionName}.", nameof(Execute), "ServiceHostTrace");
                return result;
            }

            #endregion 檢查動作執行結果

            #region 取得遠端回應包裹

            ResponsePackage responsePackage;

            try
            {
                responsePackage = JsonSerializer.Deserialize<ResponsePackage>(resDataContent);
            }
            catch (Exception ex)
            {
                result.Code = StatusCode.CLIENT_RPC_INVALID_RES;
                result.Message = $"Retrive invalid RPC response. Can not deserialize to ResponsePackage. RequestId: {requestId}. Retrive RPC Raw Data:{Environment.NewLine}{resDataContent}";
                Logger.Exception(this, ex, nameof(Execute), $"Deserialize response data content from JSON occur exception. RequestId: {requestId}, ActionName: {actionName}.", "ServiceHostTrace");
                return result;
            }

            #endregion 取得遠端回應包裹

            #region 設定回傳值

            result.Data = responsePackage;
            result.Success = responsePackage.Success;

            #endregion 設定回傳值

            return result;
        }

        /// <summary>非同步作業發送 RPC 遠端請求訊息
        /// </summary>
        /// <param name="requestId">請求識別代碼</param>
        /// <param name="actionName">動作名稱</param>
        /// <param name="requestDataContent">請求資料內容 JSON 集合</param>
        /// <returns>執行結果</returns>
        public override Task<Result<ResponsePackage>> ExecuteAsync(string requestId, string actionName, string requestDataContent = null) =>
            Task.FromResult(Execute(requestId, actionName, requestDataContent));

        /// <summary>發佈訊息至遠端服務
        /// </summary>
        /// <param name="messageId">訊息識別代碼</param>
        /// <param name="actionName">動作名稱</param>
        /// <param name="messageContent">訊息資料內容 JSON 集合</param>
        /// <returns>發佈結果</returns>
        public override Result Publish(string messageId, string actionName, string messageContent = null)
        {
            var result = Result.Create();

            #region 設定請求包裹

            var messagePackage = new MessagePackage()
            {
                PackageType = PackageType.Publish,
                ServiceName = _serviceClientConfig.RemoteServiceName,
                MessageId = messageId,
                ActionName = actionName,
                Data = messageContent,
                PublishTime = DateTime.UtcNow
            };

            #endregion 設定請求包裹

            #region 執行發佈事件訊息

            try
            {
                var routingKey = $"{_publishRoutingKey}{actionName}";
                _messageClient.Emit(routingKey, _contentType, messageContent, messageId, messagePackage.PublishTime.ToUnixUtcTimestamp());
            }
            catch (Exception ex)
            {
                result.Code = StatusCode.CLIENT_ERROR;
                result.Message = $"Publish message package occur exception. MessageId: {messageId}, ActionName: {actionName}.{Environment.NewLine}{ex}";
                Logger.Exception(this, ex, nameof(Publish), $"Publish message package occur exception. MessageId: {messageId}, ActionName: {actionName}.", "ServiceHostTrace");
                return result;
            }

            ServiceClientActionLogger.Log(ServiceClientName, 3, messagePackage);

            #endregion 執行發佈事件訊息

            #region 設定回傳值

            result.Code = StatusCode.ACTION_SUCCESS;
            result.Success = true;

            #endregion 設定回傳值

            return result;
        }

        #endregion 實作抽象


        #region 宣告私有的方法

        /// <summary>處理接收到的事件訊息
        /// </summary>
        /// <param name="eventMessage">事件訊息</param>
        /// <returns></returns>
        [RabbitMQListenPolicy(ReQueue = false)]
        private bool ProcessMessage(IEventMessage eventMessage)
        {
            dynamic action = new QueueAction(ProcessMessage);
            action.EventMsg = eventMessage;
            _messageQueue.EnQueue(action);
            eventMessage.Act();
            return false;
        }

        /// <summary>處理接收到的事件訊息
        /// </summary>
        /// <param name="args">參數集合</param>
        private void ProcessMessage(dynamic args)
        {
            IEventMessage eventMessage = args.EventMsg;

            var messageId = eventMessage.GetMessageID();
            var eventName = eventMessage.GetEvent();
            var data = eventMessage.GetBody();
            var pubTime = eventMessage.GetTimestamp();

            var messagePackage = new MessagePackage()
            {
                MessageId = messageId,
                Data = data,
                PublishTime = pubTime.ToDateTimeFromUnixTimestamp()
            };

            _receiveMsgRoutingKey.IsNotNullOrEmpty(k => messagePackage.ActionName = eventName.RemoveFirstAppeared(_receiveMsgRoutingKey));

            try
            {
                ProcessMessagePackage(messagePackage);
            }
            catch (Exception ex)
            {
                Logger.Exception(this, ex, $"Execute remote event message package occur exception. MessageId: {messageId}. EventName: {eventName}.", "ServiceHostTrace");
                return;
            }
        }

        /// <summary>處理遠端觸發的事件訊息包裹
        /// </summary>
        /// <param name="messagePackage">通訊訊息資料包裹</param>
        /// <returns>處理結果</returns>
        private Result ProcessMessagePackage(MessagePackage messagePackage)
        {
            var result = Result.Create();

            try
            {
                if (messagePackage.IsNull())
                {
                    result.Message = $"Read MessagePackage from TCP socket message fail due to retrive null value of MessagePackage.";
                    return result;
                }

                var eventProcess = base.EventHandlers.Get(messagePackage.ActionName);

                if (eventProcess.IsNull())
                {
                    result.Message = $"Read MessagePackage from TCP socket message fail due to retrive no relative EventProcessor register. Receive action name: {messagePackage.ActionName}.";
                    return result;
                }

                var data = messagePackage.Data;

                if (data.IsNotNullOrEmpty())
                {
                    eventProcess.RawData = JsonSerializer.Deserialize(messagePackage.Data, eventProcess.DataType);
                }

                ServiceClientActionLogger.Log(ServiceClientName, 4, messagePackage);
                eventProcess.Execute();
            }
            catch (Exception ex)
            {
                result.Message = $"Process MessagePackage from TCP socke message ocur excepion. Receive MessagePackage:{Environment.NewLine}{NewtonsoftJsonConvert.Serialize(messagePackage)}{Environment.NewLine}{ex}";
                return result;
            }

            result.Success = true;
            return result;
        }

        #endregion 宣告私有的方法
    }
}
