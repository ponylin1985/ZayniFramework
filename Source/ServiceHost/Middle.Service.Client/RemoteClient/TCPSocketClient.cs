﻿using NeoSmart.AsyncLock;
using System;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Logging;
using ZayniFramework.Middle.Service.Entity;
using ZayniFramework.Serialization;
using ST = System.Timers;

namespace ZayniFramework.Middle.Service.Client
{
    /// <summary>TCP Socket 客戶端類別
    /// </summary>
    internal sealed class TCPSocketClient : RemoteClient
    {
        #region 宣告私有的欄位

        /// <summary>非同步作業鎖定物件
        /// </summary>
        private readonly AsyncLock _asyncLock = new();

        /// <summary>非同步作業鎖定物件: 鎖定 _buffer 欄位
        /// </summary>
        private readonly AsyncLock _asyncLockMsgBuffers = new();

        /// <summary>非同步作業鎖定物件: 鎖定 _timer 欄位
        /// </summary>
        private readonly AsyncLock _asyncLockTimer = new();

        /// <summary>TCP Socket 通訊客戶端的設定組態
        /// </summary>
        private TCPSocketClientConfig _socketClientConfig;

        /// <summary>TCP Socket 連線狀態定時檢查器
        /// </summary>
        private ST.Timer _timer = new();

        /// <summary>前一次的 TCP Socket 連線狀態是否正常
        /// </summary>
        private bool _lastConnectionState;

        /// <summary>等待 TCP Socket 服務 RPC 回應訊息的逾時秒數
        /// </summary>
        private int _tcpReponseTimeout;

        /// <summary>TCP Socket 訊息的結束標記字串
        /// </summary>
        private string _tcpMsgEndingToken;

        /// <summary>TCP Socket 網路連線客戶端物件
        /// </summary>
        private TcpClient _tcpClient;

        /// <summary>TCP Socket 網路串流物件
        /// </summary>
        private NetworkStream _networkStream;

        /// <summary>TCP Socket 訊息緩存串流
        /// </summary>
        private MemoryStream _buffer = new();

        /// <summary>RPC 請求管理容器
        /// </summary>
        private readonly RPCRequestContainer _requestContainer = new();

        #endregion 宣告私有的欄位


        #region 宣告私有屬性

        /// <summary>TCP Socket 訊息緩存串流
        /// </summary>
        private MemoryStream MsgBuffers
        {
            get
            {
                using (_asyncLockMsgBuffers.Lock())
                {
                    return _buffer;
                }
            }
            set
            {
                using (_asyncLockMsgBuffers.Lock())
                {
                    _buffer = value;
                }
            }
        }

        #endregion 宣告私有屬性


        #region 宣告建構子

        /// <summary>多載建構子
        /// </summary>
        /// <param name="serviceClientName">服務客戶端個體的設定名稱</param>
        public TCPSocketClient(string serviceClientName) : base(serviceClientName)
        {
            _timer.Interval = TimeSpan.FromSeconds(5).TotalMilliseconds;
            _timer.Elapsed += CheckConnection;
        }

        /// <summary>解構子
        /// </summary>
        ~TCPSocketClient()
        {
            _timer.Elapsed -= CheckConnection;
            _timer = null;
        }

        #endregion 宣告建構子


        #region 實作抽象

        /// <summary>初始化遠端服務連線
        /// </summary>
        /// <returns>連線是否正常建立</returns>
        public override bool Initialize()
        {
            try
            {
                var svrConfig = ClientConfigReader.GetServiceClientConfig(ServiceClientName);
                _socketClientConfig = ClientConfigReader.GetTCPSocketClientConfig(svrConfig);

                _tcpClient = new TcpClient()
                {
                    NoDelay = true,
                    SendBufferSize = 1024 * 5,
                    ReceiveBufferSize = 1024 * 5
                };

                if (_socketClientConfig.TCPSendBufferSize > 0)
                {
                    _tcpClient.SendBufferSize = 1024 * _socketClientConfig.TCPSendBufferSize;
                }

                if (_socketClientConfig.TCPReceiveBufferSize > 0)
                {
                    _tcpClient.SendBufferSize = 1024 * _socketClientConfig.TCPReceiveBufferSize;
                }

                _tcpClient.Client.Connect(_socketClientConfig.TCPServerHost, _socketClientConfig.TCPServerPort);

                var connectTimeout = _socketClientConfig.ConnecionTimeout <= 0 ? 500 : _socketClientConfig.ConnecionTimeout * 1000;
                SpinWait.SpinUntil(() => false, connectTimeout);
                _timer.Start();

                if (!IsConnectionAlive())
                {
                    _tcpClient.Client.Dispose();
                    _tcpClient = null;
                    return false;
                }

                _networkStream = _tcpClient.GetStream();
                _tcpReponseTimeout = _socketClientConfig.TCPResponseTimeout <= 0 ? 30 : _socketClientConfig.TCPResponseTimeout;
                _tcpMsgEndingToken = _socketClientConfig.TCPMsgEndToken.IsNullOrEmpty() ? GetEndTokenString("u0003") : GetEndTokenString(_socketClientConfig.TCPMsgEndToken);
            }
            catch (Exception ex)
            {
                _timer.Start();
                Logger.Exception(this, ex, $"Establish TCP socket connection occur exception. Remote service host: {_socketClientConfig.TCPServerHost}:{_socketClientConfig.TCPServerPort}.", "ServiceHostTrace");
                return false;
            }

            try
            {
                Read(_networkStream);
            }
            catch (Exception ex)
            {
                Logger.Exception(this, ex, nameof(Initialize), "Establish TCP socket connection occur exception.", "ServiceHostTrace");
                return false;
            }

            Logger.Info(this, $"Establish TCP socket connection to remote host successfully. Remote service host: {_socketClientConfig.TCPServerHost}:{_socketClientConfig.TCPServerPort}.", nameof(Initialize), "ServiceHostTrace");
            ConsoleLogger.WriteLine($"Establish TCP socket connection to remote host successfully. Remote service host: {_socketClientConfig.TCPServerHost}:{_socketClientConfig.TCPServerPort}.");
            return true;
        }

        /// <summary>發送 RPC 遠端請求訊息
        /// </summary>
        /// <param name="requestId">請求識別代碼</param>
        /// <param name="actionName">動作名稱</param>
        /// <param name="requestDataContent">請求資料內容 JSON 集合</param>
        /// <returns>執行結果</returns>
        public override Result<ResponsePackage> Execute(string requestId, string actionName, string requestDataContent = null)
        {
            var result = Result.Create<ResponsePackage>();

            #region 設定請求包裹

            var requestPackage = new RequestPackage()
            {
                PackageType = PackageType.RPCRequest,
                RequestId = requestId,
                ActionName = actionName,
                Data = requestDataContent,
                RequestTime = DateTime.UtcNow
            };

            #endregion 設定請求包裹

            #region 執行遠端服務動作

            ResponsePackage responsePackage;

            try
            {
                responsePackage = Send(requestPackage);
            }
            catch (Exception ex)
            {
                result.Code = StatusCode.CLIENT_ERROR;
                result.Message = $"Internal zayni framework service client error.";
                Logger.Exception(this, ex, $"Execute remote action occur exception. RequestId: {requestId}, ActionName: {actionName}.", "ServiceHostTrace");
                return result;
            }

            ServiceClientActionLogger.Log(ServiceClientName, requestPackage);

            #endregion 執行遠端服務動作

            #region 檢查回應包裹

            if (responsePackage.IsNull())
            {
                result.Code = StatusCode.CLIENT_ERROR;
                result.Message = $"Internal zayni framework service client error.";
                Logger.Error(this, $"No response package from remote service. RequestId: {requestId}, ActionName: {actionName}.", nameof(Execute), "ServiceHostTrace");
                return result;
            }

            #endregion 檢查回應包裹

            #region 設定回傳值

            result.Data = responsePackage;
            result.Code = responsePackage.Code;
            result.Message = responsePackage.Message;
            result.Success = responsePackage.Success;

            #endregion 設定回傳值

            return result;
        }

        /// <summary>非同步作業發送 RPC 遠端請求訊息
        /// </summary>
        /// <param name="requestId">請求識別代碼</param>
        /// <param name="actionName">動作名稱</param>
        /// <param name="requestDataContent">請求資料內容 JSON 集合</param>
        /// <returns>執行結果</returns>
        public override Task<Result<ResponsePackage>> ExecuteAsync(string requestId, string actionName, string requestDataContent = null) =>
            Task.FromResult(Execute(requestId, actionName, requestDataContent));

        /// <summary>發佈訊息至遠端服務
        /// </summary>
        /// <param name="messageId">訊息識別代碼</param>
        /// <param name="actionName">動作名稱</param>
        /// <param name="messageContent">訊息資料內容 JSON 集合</param>
        /// <returns>發佈結果</returns>
        public override Result Publish(string messageId, string actionName, string messageContent = null)
        {
            var result = Result.Create();

            #region 設定請求包裹

            var messagePackage = new MessagePackage()
            {
                PackageType = PackageType.Publish,
                MessageId = messageId,
                ActionName = actionName,
                Data = messageContent,
                PublishTime = DateTime.UtcNow
            };

            #endregion 設定請求包裹

            #region 執行發佈事件訊息

            Result r;

            try
            {
                r = Send(messagePackage);
            }
            catch (Exception ex)
            {
                result.Code = StatusCode.CLIENT_ERROR;
                result.Message = $"Internal zayni framework service client error. Publish message package occur exception. {Environment.NewLine}{ex}";
                Logger.Exception(this, ex, $"Publish message package occur exception. MessageId: {messageId}, ActionName: {actionName}.", "ServiceHostTrace");
                return result;
            }

            #endregion 執行發佈事件訊息

            #region 設定回傳值

            if (!r.Success)
            {
                result.Code = r.Code.IsNullOrEmptyString(StatusCode.CLIENT_ERROR);
                result.Message = r.Message;
                return result;
            }

            ServiceClientActionLogger.Log(ServiceClientName, 3, messagePackage);

            result.Code = r.Code;
            result.Message = r.Message;
            result.Success = true;

            #endregion 設定回傳值

            return result;
        }

        #endregion 實作抽象


        #region 宣告私有的方法

        /// <summary>取得正確的訊息結束字元字符
        /// </summary>
        /// <param name="endToken">原始 Config 組態的結束字元字符</param>
        /// <returns>結束字元字符</returns>
        private static string GetEndTokenString(string endToken)
        {
            if (endToken.StartsWith('u'))
            {
                return Convert.ToChar(int.Parse(endToken.RemoveFirstAppeared("u").TrimStart('0'))).ToString();
            }

            return endToken;
        }

        /// <summary>讀取遠端服務的 TCP Socket 網路串流訊息
        /// </summary>
        /// <param name="networkStream">TCP Socket 網路串流</param>
        private void Read(NetworkStream networkStream)
        {
            if (new object[] { _tcpClient, networkStream }.HasNullElements())
            {
                Logger.Error(this, $"Read ResponsePackage from TCP socket message fail due to TCP socket network stream is null.", nameof(Read), "ServiceHostTrace");
                return;
            }

            var thread = new Thread(() =>
            {
                while (true)
                {
                    // Thread.Sleep( TimeSpan.FromMilliseconds( 0.5 ) );
                    SpinWait.SpinUntil(() => false, TimeSpan.FromMilliseconds(0.5));

                    try
                    {
                        #region 檢查網路串流是否允許讀取

                        if (!networkStream.CanRead)
                        {
                            continue;
                        }

                        if (!networkStream.DataAvailable)
                        {
                            continue;
                        }

                        #endregion 檢查網路串流是否允許讀取

                        #region 讀取 Socket 網路串流資料

                        var receiveBytes = new byte[_tcpClient.ReceiveBufferSize];
                        var receiveBytesLength = networkStream.Read(receiveBytes, 0, _tcpClient.ReceiveBufferSize);

                        #endregion 讀取 Socket 網路串流資料

                        #region 資料緩存處理

                        MsgBuffers.Write(receiveBytes, 0, receiveBytesLength);

                        var lastByte = receiveBytes[^1];

                        if (0 != lastByte)
                        {
                            continue;
                        }

                        MsgBuffers.Seek(0, SeekOrigin.Begin);
                        var receives = new byte[MsgBuffers.Length];
                        MsgBuffers.Read(receives, 0, (int)MsgBuffers.Length);
                        var receiveMsg = Encoding.UTF8.GetString(receives)?.Replace("\0", string.Empty);

                        #endregion 資料緩存處理

                        #region 檢查訊息是否包含「結束字元」

                        if (!receiveMsg.Contains(_tcpMsgEndingToken))
                        {
                            continue;
                        }

                        #endregion 檢查訊息是否包含「結束字元」

                        #region 處理訊息

                        var endsWith = receiveMsg.EndsWith(_tcpMsgEndingToken);
                        var messages = receiveMsg.SplitString(_tcpMsgEndingToken);
                        var messagesLength = endsWith ? messages.Length : messages.Length - 1;

                        for (int i = 0, len = messagesLength; i < len; i++)
                        {
                            var json = messages[i]?.Trim();
                            var r = GetMessagePackage(json);

                            if (!r.Success)
                            {
                                Logger.Error(this, $"Read message from TCP socket message occur error. {r.Message}", nameof(Read), "ServiceHostTrace");
                                continue;
                            }

                            var package = r.Data;
                            var p = Process(package);

                            if (!p.Success)
                            {
                                Logger.Error(this, $"Process message package daat from TCP socket streaming occur error. {p.Message}", nameof(Read), "ServiceHostTrace");
                                continue;
                            }
                        }

                        MsgBuffers?.Close();
                        MsgBuffers?.Dispose();
                        MsgBuffers = endsWith ? new MemoryStream() : new MemoryStream(Encoding.UTF8.GetBytes(messages.LastOrDefault()));

                        #endregion 處理訊息
                    }
                    catch (Exception ex)
                    {
                        MsgBuffers = new MemoryStream();
                        Logger.Exception(this, ex, nameof(Read), $"Read ResponsePackage message from TCP socket network stream occur exception.", "ServiceHostTrace");
                        continue;
                    }
                }
            });

            thread.Start();
        }

        /// <summary>發送請求包裹的 TCP Socket 網路串流訊息
        /// </summary>
        /// <param name="requestPackage">遠端服務請求包裹</param>
        private ResponsePackage Send(RequestPackage requestPackage)
        {
            #region 初始化回傳值

            var responsePackage = new ResponsePackage()
            {
                ActionName = requestPackage.ActionName,
                RequestId = requestPackage.RequestId
            };

            #endregion 初始化回傳值

            #region 檢查 TCP 網路串流狀態

            if (new object[] { _tcpClient, _networkStream }.HasNullElements())
            {
                responsePackage.Code = StatusCode.CONNECTION_ERROR;
                responsePackage.Message = $"TCP socket connection state error.";
                Logger.Error(this, $"Can not send TCP socket message to remote service host due to TcpClient or NetworkStream is null. RequestId: {requestPackage.RequestId}, ActionName: {requestPackage.ActionName}. Remote service host IP address: {_socketClientConfig.TCPServerHost}:{_socketClientConfig.TCPServerPort}.", nameof(Send), "ServiceHostTrace");
                return responsePackage;
            }

            #endregion 檢查 TCP 網路串流狀態

            #region 檢查 TCP Socket 連線

            if (!IsConnectionAlive())
            {
                responsePackage.Code = StatusCode.CONNECTION_ERROR;
                responsePackage.Message = $"TCP socket connection between remote service host is broken.";
                Logger.Error(this, $"Can not send TCP socket message to remote service host due to TCP socket connection is broken and reconnect is still fail. RequestId: {requestPackage.RequestId}, ActionName: {requestPackage.ActionName}. Remote service host IP address: {_socketClientConfig.TCPServerHost}:{_socketClientConfig.TCPServerPort}.", nameof(Send), "ServiceHostTrace");
                return responsePackage;
            }

            #endregion 檢查 TCP Socket 連線

            #region 新增 TCP Socket 請求訊息

            RPCRequest request;

            try
            {
                request = requestPackage.ConvertTo<RPCRequest>();
                request.ReceiveEvent = new AutoResetEvent(false);

                if (!_requestContainer.Add(requestPackage.RequestId, request))
                {
                    responsePackage.Code = StatusCode.CLIENT_ERROR;
                    responsePackage.Message = $"Add TCP socket RPC request to container fail.";
                    Logger.Error(this, $"Add RPC request to container fail. RequestId: {requestPackage.RequestId}, ActionName: {requestPackage.ActionName}.", nameof(Execute), "ServiceHostTrace");
                    return responsePackage;
                }
            }
            catch (Exception ex)
            {
                responsePackage.Code = StatusCode.CLIENT_ERROR;
                responsePackage.Message = $"Add TCP socket RPC request to container occur exception.";
                Logger.Exception(this, ex, nameof(Send), $"Send RequestPackage to TCP socket network stream occur exception due to add TcpRequestPackage to request pool occur exception. RequestId: {requestPackage.RequestId}, ActionName: {requestPackage.ActionName}.", "ServiceHostTrace");
                return responsePackage;
            }

            #endregion 新增 TCP Socket 請求訊息

            #region 序列化 TCP Socket 請求訊息

            byte[] messageBytes = null;

            try
            {
                var json = $"{JsonSerializer.Serialize(requestPackage)}{_tcpMsgEndingToken}";
                messageBytes = Encoding.UTF8.GetBytes(json);
            }
            catch (Exception ex)
            {
                responsePackage.Code = StatusCode.CLIENT_ERROR;
                responsePackage.Message = $"Send RequestPackage to TCP socket network stream occur exception due to serialize RequestPackage occur exception.";
                _requestContainer.Remove(request.RequestId);
                Logger.Exception(this, ex, nameof(Send), $"Send RequestPackage to TCP socket network stream occur exception due to serialize RequestPackage object occur exception. RequestId: {requestPackage.RequestId}, ActionName: {requestPackage.ActionName}.", "ServiceHostTrace");
                return responsePackage;
            }

            #endregion 序列化 TCP Socket 請求訊息

            #region 發送 TCP Socket RPC 請求訊息

            try
            {
                // 第一種寫法:
                //_tcpClient.Client.Send( messageBytes );

                // 第二種寫法:
                _networkStream.Write(messageBytes, 0, messageBytes.Length);
                _networkStream.Flush();
            }
            catch (Exception ex)
            {
                responsePackage.Code = StatusCode.CLIENT_ERROR;
                responsePackage.Message = $"Send RequestPackage to TCP socket network stream occur exception.";
                _requestContainer.Remove(request.RequestId);
                Logger.Exception(this, ex, nameof(Send), $"Send RequestPackage to TCP socket network stream occur exception. Write TCP socket message to network stream occur exception. RequestId: {requestPackage.RequestId}, ActionName: {requestPackage.ActionName}.", "ServiceHostTrace");
                return responsePackage;
            }

            #endregion 發送 TCP Socket RPC 請求訊息

            #region 取得 TCP Socket RPC 遠端回應訊息包裹

            request.ReceiveEvent?.Reset();
            var isTimeout = !request.ReceiveEvent.WaitOne(TimeSpan.FromSeconds(_tcpReponseTimeout));

            if (isTimeout)
            {
                responsePackage.Code = StatusCode.CLIENT_RPC_TIMEOUT;
                responsePackage.Message = $"Waiting for remote host reply ResponsePackage TCP socket timeout.";
                _requestContainer.Remove(request.RequestId);
                Logger.Error(this, $"Waiting for remote host reply ResponsePackage TCP socket timeout. RequestId: {requestPackage.RequestId}, ActionName: {requestPackage.ActionName}.", nameof(Send), "ServiceHostTrace");
                return responsePackage;
            }

            if (!_requestContainer.Get(request.RequestId, out var rpcRequest))
            {
                responsePackage.Code = StatusCode.CLIENT_ERROR;
                responsePackage.Message = $"Get RPC response occur error.";
                _requestContainer.Remove(request.RequestId);
                Logger.Error(this, $"Get RPC request from container fail. RequestId: {requestPackage.RequestId}, ActionName: {requestPackage.ActionName}.", nameof(Execute), "ServiceHostTrace");
                return responsePackage;
            }

            if (rpcRequest.IsNull())
            {
                responsePackage.Code = StatusCode.CLIENT_ERROR;
                responsePackage.Message = $"Get RPC response occur error. RequestId: {requestPackage.RequestId}.";
                _requestContainer.Remove(request.RequestId);
                Logger.Error(this, $"TcpRequestPackage from request pool is null. RequestId: {requestPackage.RequestId}, ActionName: {requestPackage.ActionName}.", nameof(Send), "ServiceHostTrace");
                return responsePackage;
            }

            responsePackage = rpcRequest.Result;

            if (responsePackage.IsNull())
            {
                responsePackage.Code = StatusCode.CLIENT_RPC_INVALID_RES;
                responsePackage.Message = $"Retrive invalid RPC response. ResponsePackage is null. RequestId: {requestPackage.RequestId}.";
                _requestContainer.Remove(request.RequestId);
                Logger.Error(this, $"Retrive ResponsePackage object is null from remote host. RequestId: {requestPackage.RequestId}, ActionName: {requestPackage.ActionName}.", nameof(Send), "ServiceHostTrace");
                return responsePackage;
            }

            _requestContainer.Remove(request.RequestId);

            #endregion 取得 TCP Socket RPC 遠端回應訊息包裹

            return responsePackage;
        }

        /// <summary>發送請求包裹的 TCP Socket 網路串流訊息
        /// </summary>
        /// <param name="messagePackage">遠端服務請求包裹</param>
        private Result Send(MessagePackage messagePackage)
        {
            #region 初始化回傳值

            var result = new Result()
            {
                Success = false,
                Message = null
            };

            #endregion 初始化回傳值

            #region 檢查 TCP 網路串流狀態

            if (new object[] { _tcpClient, _networkStream }.HasNullElements())
            {
                result.Code = StatusCode.CONNECTION_ERROR;
                result.Message = $"TCP socket connection status error.";
                Logger.Error(this, $"Can not publish message package to remote service. ServiceClient: {ServiceClientName}, ActionName: {messagePackage.ActionName}. {result.Message}", nameof(Send), "ServiceHostTrace");
                return result;
            }

            #endregion 檢查 TCP 網路串流狀態

            #region 檢查 TCP Socket 連線

            if (!IsConnectionAlive())
            {
                result.Code = StatusCode.CONNECTION_ERROR;
                result.Message = $"TCP socket connection between remote service host is broken.";
                Logger.Error(this, $"Can not publish message package to remote service. ServiceClient: {ServiceClientName}, ActionName: {messagePackage.ActionName}. {result.Message}", nameof(Send), "ServiceHostTrace");
                return result;
            }

            #endregion 檢查 TCP Socket 連線

            #region 序列化 TCP Socket 請求訊息

            byte[] messageBytes = null;

            try
            {
                var json = $"{JsonSerializer.Serialize(messagePackage)}{_tcpMsgEndingToken}";
                messageBytes = Encoding.UTF8.GetBytes(json);
            }
            catch (Exception ex)
            {
                result.Code = StatusCode.CLIENT_ERROR;
                result.Message = $"Serialize message package to JSON occur exception. {Environment.NewLine}{ex}";
                Logger.Exception(this, ex, nameof(Send), $"Can not publish message package to remote service. Send MessagePackage to TCP socket network stream occur exception due to serialize MessagePackage object occur exception.", "ServiceHostTrace");
                return result;
            }

            #endregion 序列化 TCP Socket 請求訊息

            #region 發送 TCP Socket 訊息

            try
            {
                // 第一種寫法:
                //_tcpClient.Client.Send( messageBytes );

                // 第二種寫法:
                _networkStream.Write(messageBytes, 0, messageBytes.Length);
                _networkStream.Flush();
            }
            catch (Exception ex)
            {
                result.Code = StatusCode.CLIENT_ERROR;
                result.Message = $"Write TCP socket message to network stream occur exception. {Environment.NewLine}{ex}";
                Logger.Exception(this, ex, nameof(Send), $"Send RequestPackage to TCP socket network stream occur exception. Write TCP socket message to network stream occur exception.", "ServiceHostTrace");
                return result;
            }

            #endregion 發送 TCP Socket 訊息

            result.Success = true;
            return result;
        }

        /// <summary>定時檢查 TCP Socket 連線
        /// </summary>
        /// <param name="sender">事件觸發物件</param>
        /// <param name="e">定時器事件參數</param>
        private void CheckConnection(object sender, ST.ElapsedEventArgs e)
        {
            using (_asyncLockTimer.Lock())
            {
                if (IsConnectionAlive())
                {
                    _lastConnectionState = true;
                    return;
                }

                if (_lastConnectionState)
                {
                    HandlerInvoker.Invoke(ConnectionBroken);
                }

                if (!Initialize())
                {
                    _lastConnectionState = false;
                    Logger.Error(this, $"ServiceClient '{ServiceClientName}' TCP socket connection timer reconnect to remote service host fail. Remote serivce host IP address: {_socketClientConfig.TCPServerHost}:{_socketClientConfig.TCPServerPort}.", nameof(CheckConnection), "ServiceHostTrace");
                    return;
                }

                _lastConnectionState = true;
                Logger.Info(this, $"ServiceClient '{ServiceClientName}' TCP socket connection timer reconnect to remote service host success. Remote serivce host IP address: {_socketClientConfig.TCPServerHost}:{_socketClientConfig.TCPServerPort}.", nameof(CheckConnection), "ServiceHostTrace");
                HandlerInvoker.Invoke(ConnectionRestored);
            }
        }

        /// <summary>檢查目前的 TCP Socket 連線是否正常連線中
        /// </summary>
        /// <returns>TCP Socket 連線是否正常</returns>
        private bool IsConnectionAlive()
        {
            if (_tcpClient.IsNull())
            {
                Logger.Warn(this, $"ServiceClient '{ServiceClientName}' - TCP socket connection between remote service host is broken. Remote serivce host IP address: {_socketClientConfig.TCPServerHost}:{_socketClientConfig.TCPServerPort}.", nameof(IsConnectionAlive), "ServiceHostTrace");
                return false;
            }

            if (!_tcpClient.Connected)
            {
                Logger.Warn(this, $"ServiceClient '{ServiceClientName}' - TCP socket connection between remote service host is broken. Remote serivce host IP address: {_socketClientConfig.TCPServerHost}:{_socketClientConfig.TCPServerPort}.", nameof(IsConnectionAlive), "ServiceHostTrace");
                return false;
            }

            try
            {
                // 不要檢查 TcpClient.Socket 寫入的狀態，來判斷 Socket 連線是否中斷
                // 因為，有可能檢查的當下，本地端正在大量寫入資料進入網路串流，導致網路串流 IO 咬住，造成誤判!
                //if ( !_tcpClient.Client.Poll( 0, SelectMode.SelectWrite ) )
                //{
                //    Logger.Warn( this, $"ServiceClient '{ServiceClientName}' - TCP socket connection between remote service host is broken. Remote serivce host IP address: {_socketClientConfig.TCPServerHost}:{_socketClientConfig.TCPServerPort}.", nameof ( IsConnectionAlive ), "ServiceHostTrace" );
                //    return false;
                //}

                if (_tcpClient.Client.Poll(0, SelectMode.SelectRead))
                {
                    if (0 == _tcpClient.Client.Receive(new byte[1], SocketFlags.Peek))
                    {
                        Logger.Warn(this, $"ServiceClient '{ServiceClientName}' - TCP socket connection between remote service host is broken. Remote serivce host IP address: {_socketClientConfig.TCPServerHost}:{_socketClientConfig.TCPServerPort}.", nameof(IsConnectionAlive), "ServiceHostTrace");
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Warn(this, $"Check TCP socket connection occur exception. Remote serivce host IP address: {_socketClientConfig.TCPServerHost}:{_socketClientConfig.TCPServerPort}. {Environment.NewLine}{ex}", nameof(IsConnectionAlive), "ServiceHostTrace");
                return false;
            }

            return true;
        }

        /// <summary>取得通訊訊息資料包裹
        /// </summary>
        /// <param name="json">通訊 JSON 資料字串</param>
        /// <returns>取得結果</returns>
        private Result<IPackage> GetMessagePackage(string json)
        {
            var result = Result.Create<IPackage>();

            IPackage messagePackage;

            try
            {
                var package = JsonSerializer.Deserialize(json, typeof(object));
                var jsonElement = (JsonElement)package;
                var packageType = jsonElement.GetProperty("packageType").GetString(); ;

                messagePackage = packageType switch
                {
                    PackageType.Publish => JsonSerializer.Deserialize<MessagePackage>(json),
                    _ => JsonSerializer.Deserialize<ResponsePackage>(json),
                };
            }
            catch (Exception ex)
            {
                result.Message = $"Deserialize receive JSON data to {nameof(IPackage)} object occur exception. {Environment.NewLine}{ex}";
                return result;
            }

            if (messagePackage.IsNull())
            {
                result.Message = $"Deserialize receive JSON data fail due to result object is null.";
                return result;
            }

            result.Data = messagePackage;
            result.Success = true;
            return result;
        }

        /// <summary>處理訊息包裹
        /// </summary>
        /// <param name="package">通訊資料包裹</param>
        /// <returns>處理結果</returns>
        private Result Process(IPackage package)
        {
            return package switch
            {
                ResponsePackage responsePackage => ProcessResponsePackage(responsePackage),
                MessagePackage messagePackage => ProcessMessagePackage(messagePackage),
                _ => ProcessResponsePackage((ResponsePackage)package),
            };
        }

        /// <summary>處理遠端回應包裹
        /// </summary>
        /// <param name="responsePackage">遠端服務回應包裹</param>
        /// <returns>處理結果</returns>
        private Result ProcessResponsePackage(ResponsePackage responsePackage)
        {
            var result = Result.Create();

            try
            {
                if (responsePackage.IsNull())
                {
                    result.Message = $"Read ResponsePackage from TCP socket message fail due to retrive null value of ResponsePackage.";
                    return result;
                }

                var requestId = responsePackage.RequestId;

                if (requestId.IsNullOrEmpty())
                {
                    result.Message = $"Read ResponsePackage from TCP socket message fail due to invalid ResponsePackage. RequestId is null or empty string. Receive ResponsePackage:{Environment.NewLine}{NewtonsoftJsonConvert.Serialize(responsePackage)}";
                    return result;
                }

                if (!_requestContainer.Contains(requestId))
                {
                    result.Message = $"Read ResponsePackage from TCP socket message fail due to invalid ResponsePackage. Can not found RequestId: {requestId}.";
                    return result;
                }

                if (!_requestContainer.Get(requestId, out var rpcRequest))
                {
                    result.Message = $"Get RPC request from container fail. Retrive ResponsePackage is:{Environment.NewLine}{NewtonsoftJsonConvert.Serialize(responsePackage)}";
                    return result;
                }

                if (rpcRequest.IsNull())
                {
                    result.Message = $"Read ResponsePackage from TCP socket message occur exception due get TcpRequestPackage from request pool fail. Receive null TcpRequestPackage. Retrive ResponsePackage is:{Environment.NewLine}{NewtonsoftJsonConvert.Serialize(responsePackage)}";
                    return result;
                }

                using (_asyncLock.Lock())
                {
                    rpcRequest.Result = responsePackage;
                    rpcRequest.ReceiveEvent?.Set();
                }
            }
            catch (Exception ex)
            {
                result.Message = $"Process ResponsePackage from TCP socke message ocur excepion. Receive ResponsePackage:{Environment.NewLine}{NewtonsoftJsonConvert.Serialize(responsePackage)}{Environment.NewLine}{ex}";
                return result;
            }

            result.Success = true;
            return result;
        }

        /// <summary>處理遠端觸發的事件訊息包裹
        /// </summary>
        /// <param name="messagePackage">通訊訊息資料包裹</param>
        /// <returns>處理結果</returns>
        private Result ProcessMessagePackage(MessagePackage messagePackage)
        {
            var result = Result.Create();

            try
            {
                if (messagePackage.IsNull())
                {
                    result.Message = $"Read MessagePackage from TCP socket message fail due to retrive null value of MessagePackage.";
                    return result;
                }

                var eventProcess = base.EventHandlers.Get(messagePackage.ActionName);

                if (eventProcess.IsNull())
                {
                    result.Message = $"Read MessagePackage from TCP socket message fail due to retrive no relative EventProcessor register. Receive action name: {messagePackage.ActionName}.";
                    return result;
                }

                var data = messagePackage.Data;

                if (data.IsNotNullOrEmpty())
                {
                    eventProcess.RawData = JsonSerializer.Deserialize(messagePackage.Data, eventProcess.DataType);
                }

                ServiceClientActionLogger.Log(ServiceClientName, 4, messagePackage);
                eventProcess.Execute();
            }
            catch (Exception ex)
            {
                result.Message = $"Process MessagePackage from TCP socke message ocur excepion. Receive MessagePackage:{Environment.NewLine}{NewtonsoftJsonConvert.Serialize(messagePackage)}{Environment.NewLine}{ex}";
                return result;
            }

            result.Success = true;
            return result;
        }

        #endregion 宣告私有的方法
    }
}
