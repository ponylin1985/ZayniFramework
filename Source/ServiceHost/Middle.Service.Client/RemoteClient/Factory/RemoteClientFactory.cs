﻿using NeoSmart.AsyncLock;
using System;
using System.Collections.Generic;
using System.Linq;
using ZayniFramework.Common;
using ZayniFramework.Logging;
using ZayniFramework.Middle.Service.Entity;


namespace ZayniFramework.Middle.Service.Client
{
    /// <summary>服務客戶端物件工廠
    /// </summary>
    public static class RemoteClientFactory
    {
        #region 宣告私有的欄位

        /// <summary>非同步作業鎖定物件
        /// </summary>
        private static readonly AsyncLock _asyncLock = new();

        /// <summary>服務客戶端種類的容器池<para/>
        /// Key 值: IRemoteClient 的種類名稱 (RemoteHostType)<para/>
        /// Value 值: IRemoteClient 的型別
        /// </summary>
        /// <returns></returns>
        private static readonly Dictionary<string, Type> _remoteClientTypes = [];

        /// <summary>
        /// </summary>
        private static readonly string[] _sourceArray = ["InProcess", "TCPSocket", "WebSocket", "RabbitMQ"];

        #endregion 宣告私有的欄位


        #region 宣告靜態建構子

        /// <summary>靜態建構子
        /// </summary>
        static RemoteClientFactory()
        {
            using (_asyncLock.Lock())
            {
                _remoteClientTypes.Add("InProcess", typeof(InProcessClient));
                _remoteClientTypes.Add("TCPSocket", typeof(TCPSocketClient));
                _remoteClientTypes.Add("WebSocket", typeof(WebSocketClient));
                _remoteClientTypes.Add("RabbitMQ", typeof(RabbitMQClient));
            }
        }

        #endregion 宣告靜態建構子


        #region 宣告內部的工廠方法

        /// <summary>註冊 IRemoteClient 服務客戶端的型別
        /// </summary>
        /// <param name="remoteClientTypeName">IRemoteClient 的種類名稱 (RemoteHostType)</param>
        /// <typeparam name="TRemoteClient">服務客戶端的種類泛型</typeparam>
        /// <returns>註冊結果</returns>
        public static IResult RegisterRemoteClientType<TRemoteClient>(string remoteClientTypeName)
            where TRemoteClient : IRemoteClient
        {
            var result = new Result()
            {
                Success = false,
                Message = null
            };

            try
            {
                using (_asyncLock.Lock())
                {
                    if (remoteClientTypeName.IsNullOrEmpty())
                    {
                        result.Message = $"The {nameof(remoteClientTypeName)} can not be null.";
                        Logger.Error(nameof(RemoteClientFactory), result.Message, nameof(RegisterRemoteClientType), "ServiceHostTrace");
                        return result;
                    }

                    if (_sourceArray.Contains(remoteClientTypeName))
                    {
                        result.Success = true;
                        return result;
                    }

                    if (_remoteClientTypes.ContainsKey(remoteClientTypeName))
                    {
                        result.Message = $"Duplicate {nameof(remoteClientTypeName)} in container. remoteHostTypeName: {remoteClientTypeName}.";
                        Logger.Error(nameof(RemoteClientFactory), result.Message, nameof(RegisterRemoteClientType), "ServiceHostTrace");
                        return result;
                    }

                    _remoteClientTypes.Add(remoteClientTypeName, typeof(TRemoteClient));
                }
            }
            catch (Exception ex)
            {
                result.ExceptionObject = ex;
                result.Message = $"Register remote client type into container occur exception. {Environment.NewLine}{ex}";
                Logger.Exception(nameof(RemoteClientFactory), ex, result.Message, "ServiceHostTrace");
                return result;
            }

            result.Success = true;
            return result;
        }

        /// <summary>建立服務客戶端個體
        /// </summary>
        /// <param name="serviceClientName">服務客戶端個體的設定名稱</param>
        /// <returns>服務客戶端個體建立結果</returns>
        internal static Result<IRemoteClient> Create(string serviceClientName)
        {
            var result = new Result<IRemoteClient>()
            {
                Success = false,
                Data = null
            };

            try
            {
                var config = ClientConfigReader.GetServiceClientConfig(serviceClientName);

                if (config.IsNull())
                {
                    Logger.Error(nameof(RemoteClientFactory), $"Can not get service client of '{serviceClientName}' from serviceClientConfig.json.", nameof(Create), "ServiceHostTrace");
                    return result;
                }

                if (_remoteClientTypes.TryGetValue(config.RemoteHostType, out var clientType))
                {
                    var ctor = clientType.GetConstructor([typeof(string)]);
                    result.Data = (IRemoteClient)ctor.Invoke([config.ServiceClientName]);
                    result.Success = true;
                    return result;
                }
                else
                {
                    result.Data = new TCPSocketClient(config.ServiceClientName);
                    result.Success = true;
                    return result;
                }
            }
            catch (Exception ex)
            {
                result.Code = StatusCode.CLIENT_ERROR;
                result.Message = $"Create remote client object occur exception. {Environment.NewLine}{ex}";
                return result;
            }
        }

        #endregion 宣告內部的工廠方法
    }
}
