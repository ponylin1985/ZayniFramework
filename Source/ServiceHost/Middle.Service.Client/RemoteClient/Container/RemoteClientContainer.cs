﻿using NeoSmart.AsyncLock;
using System;
using System.Collections.Generic;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.Middle.Service.Client
{
    /// <summary>服務客戶端管理容器
    /// </summary>
    internal sealed class RemoteClientContainer
    {
        #region 宣告私有的欄位

        /// <summary>非同步作業鎖定物件
        /// </summary>
        private static readonly AsyncLock _asyncLock = new();

        /// <summary>服務客戶端物件字典容器<para/>
        /// Key 值: 服務客戶端個體的設定名稱 (serviceHostConfig.js 中的 serviceHosts/serviceName 設定值)<para/>
        /// Value 值: RemoteClient 物件實體
        /// </summary>
        private static readonly Dictionary<string, IRemoteClient> _remoteClients = [];

        #endregion 宣告私有的欄位


        #region 宣告內部的方法

        /// <summary>取得服務客戶端個體
        /// </summary>
        /// <param name="serviceClientName">服務客戶端個體的設定名稱</param>
        /// <returns>取得結果</returns>
        internal static Result<IRemoteClient> Get(string serviceClientName)
        {
            using (_asyncLock.Lock())
            {
                var result = Result.Create<IRemoteClient>();

                if (serviceClientName.IsNullOrEmpty())
                {
                    result.Message = $"Argument '{nameof(serviceClientName)}' can not be null or empty string.";
                    Logger.Error(nameof(RemoteClientContainer), result.Message, nameof(Get), "ServiceHostTrace");
                    return result;
                }

                try
                {
                    if (_remoteClients.TryGetValue(serviceClientName, out var remoteClient))
                    {
                        if (remoteClient.IsNull())
                        {
                            result.Message = $"Get remote client object from container is null. ServiceClientName: {serviceClientName}.";
                            Logger.Error(nameof(RemoteClientContainer), result.Message, nameof(Get), "ServiceHostTrace");
                            return result;
                        }

                        result.Data = remoteClient;
                        result.Success = true;
                        return result;
                    }

                    var r = RemoteClientFactory.Create(serviceClientName);

                    if (!r.Success)
                    {
                        result.Message = r.Message;
                        Logger.Error(nameof(RemoteClientContainer), result.Message, nameof(Get), "ServiceHostTrace");
                        return result;
                    }

                    remoteClient = r.Data;

                    if (remoteClient.IsNull())
                    {
                        result.Message = $"Retrive remote client object from factory is null. ServiceClientName: {serviceClientName}.";
                        Logger.Error(nameof(RemoteClientContainer), result.Message, nameof(Get), "ServiceHostTrace");
                        return result;
                    }

                    _remoteClients.Add(serviceClientName, remoteClient);

                    result.Data = remoteClient;
                    result.Success = true;
                    return result;
                }
                catch (Exception ex)
                {
                    result.Message = $"Get remote client object from container occur exception. {ex}";
                    Logger.Exception(nameof(RemoteClientContainer), ex, result.Message, "ServiceHostTrace");
                    return result;
                }
            }
        }

        #endregion 宣告內部的方法
    }
}
