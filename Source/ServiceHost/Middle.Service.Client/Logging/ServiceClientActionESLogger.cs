using NeoSmart.AsyncLock;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Logging;
using ZayniFramework.Middle.Service.Entity;


namespace ZayniFramework.Middle.Service.Client
{
    /// <summary>客戶端動作 ElasticSearch 日誌器
    /// </summary>
    public class ServiceClientActionESLogger
    {
        #region Private Fields

        /// <summary>非同步作業的鎖定物件
        /// </summary>
        /// <returns></returns>
        private static readonly AsyncLock _asyncLock = new();

        #endregion Private Fields


        #region Internal Methods

        /// <summary>記錄客戶端的請求動作日誌
        /// </summary>
        /// <param name="serviceClientName">服務客戶端的設定名稱</param>
        /// <param name="requestPackage">服務請求包裹</param>
        internal async Task LogAsync(string serviceClientName, RequestPackage requestPackage)
        {
            using (await _asyncLock.LockAsync())
            {
                try
                {
                    var config = GlobalClientContext.Config.ServiceClients?.Where(cl => cl.ServiceClientName == serviceClientName).FirstOrDefault();
                    var model = ConvertTo(config, requestPackage);
                    var esLogger = ElasticsearchLoggerContainer.Get(config.ESLoggerName);
                    await esLogger?.InsertLogAsync<service_client_action_log>(model, model.LogSrNo);
                }
                catch (Exception ex)
                {
                    Logger.Exception(nameof(ServiceClientActionLogRepo), ex, "Write request FS_SERVICE_CLIENT_ACTION_LOG to ElasticSearch occur exception.", "ServiceHostTrace");
                }
            }
        }

        /// <summary>記錄客戶端的回應動作日誌
        /// </summary>
        /// <param name="serviceClientName">服務客戶端的設定名稱</param>
        /// <param name="responsePackage">服務回應包裹</param>
        internal async Task LogAsync(string serviceClientName, ResponsePackage responsePackage)
        {
            using (await _asyncLock.LockAsync())
            {
                try
                {
                    var config = GlobalClientContext.Config.ServiceClients?.Where(cl => cl.ServiceClientName == serviceClientName).FirstOrDefault();
                    var model = ConvertTo(config, responsePackage);
                    var esLogger = ElasticsearchLoggerContainer.Get(config.ESLoggerName);
                    await esLogger?.InsertLogAsync<service_client_action_log>(model, model.LogSrNo);
                }
                catch (Exception ex)
                {
                    Logger.Exception(nameof(ServiceClientActionLogRepo), ex, "Write response FS_SERVICE_CLIENT_ACTION_LOG to ElasticSearch occur exception.", "ServiceHostTrace");
                }
            }
        }

        /// <summary>紀錄客戶端的事件訊息包裹的日誌
        /// </summary>
        /// <param name="serviceClientName">服務客戶端的設定名稱</param>
        /// <param name="direction">動作方向: 3: PublishMessage，4: ReceiveMessage。</param>
        /// <param name="messagePackage">事件訊息包裹</param>
        internal async Task LogAsync(string serviceClientName, int direction, MessagePackage messagePackage)
        {
            using (await _asyncLock.LockAsync())
            {
                try
                {
                    var config = GlobalClientContext.Config.ServiceClients?.Where(cl => cl.ServiceClientName == serviceClientName).FirstOrDefault();
                    var model = ConvertTo(config, direction, messagePackage);
                    var esLogger = ElasticsearchLoggerContainer.Get(config.ESLoggerName);
                    await esLogger?.InsertLogAsync<service_client_action_log>(model, model.LogSrNo);
                }
                catch (Exception ex)
                {
                    Logger.Exception(nameof(ServiceClientActionLogRepo), ex, "Write message package FS_SERVICE_CLIENT_ACTION_LOG to ElasticSearch occur exception.", "ServiceHostTrace");
                }
            }
        }

        #endregion Internal Methods


        #region Private Methods

        /// <summary>轉換服務動作日誌紀錄的資料模型
        /// </summary>
        /// <param name="config">服務客戶端個體的設定組態</param>
        /// <param name="requestPackage">服務請求包裹</param>
        /// <returns>服務動作日誌紀錄的資料模型</returns>
        private static service_client_action_log ConvertTo(ServiceClientConfig config, RequestPackage requestPackage)
        {
            var model = new service_client_action_log
            {
                LogSrNo = RandomTextHelper.CreateInt64String(20),
                RequestId = requestPackage.RequestId,
                ServiceClientName = config.ServiceClientName,
                ServiceClientHost = "InProcess" == config.RemoteHostType ? "in-process localhost" : GetLocalIpAddress(),
                Direction = 1,
                ActionName = requestPackage.ActionName,
                RequestTime = requestPackage.RequestTime,
                LogTime = DateTime.UtcNow,
                RemoteServiceName = config?.RemoteServiceName,
                RemoteServiceHost = GetRemoteHostString(config)
            };

            requestPackage.Data.IsNotNullOrEmpty(d => model.DataContent = JsonConvert.DeserializeObject(requestPackage.Data));
            return model;
        }

        /// <summary>轉換服務動作日誌紀錄的資料模型
        /// </summary>
        /// <param name="config">服務客戶端個體的設定組態</param>
        /// <param name="responsePackage">服務回應包裹</param>
        /// <returns>服務動作日誌紀錄的資料模型</returns>
        private static service_client_action_log ConvertTo(ServiceClientConfig config, ResponsePackage responsePackage)
        {
            var nowTime = DateTime.UtcNow;

            var model = new service_client_action_log
            {
                LogSrNo = RandomTextHelper.CreateInt64String(20),
                RequestId = responsePackage.RequestId,
                ServiceClientName = config.ServiceClientName,
                ServiceClientHost = "InProcess" == config.RemoteHostType ? "in-process localhost" : GetLocalIpAddress(),
                Direction = 2,
                ActionName = responsePackage.ActionName,
                IsSuccess = responsePackage.Success,
                Code = responsePackage.Code,
                Message = responsePackage.Message,
                ResponseTime = nowTime,
                LogTime = DateTime.UtcNow,
                RemoteServiceName = config?.RemoteServiceName,
                RemoteServiceHost = GetRemoteHostString(config)
            };

            responsePackage.Data.IsNotNullOrEmpty(d => model.DataContent = JsonConvert.DeserializeObject(responsePackage.Data));
            return model;
        }

        /// <summary>轉換服務動作日誌紀錄的資料模型
        /// </summary>
        /// <param name="config">服務客戶端個體的設定組態</param>
        /// <param name="direction">動作方向: 3: PublishMessage，4: ReceiveMessage。</param>
        /// <param name="messagePackage">訊息資料包裹</param>
        /// <returns>服務動作日誌紀錄的資料模型</returns>
        private static service_client_action_log ConvertTo(ServiceClientConfig config, int direction, MessagePackage messagePackage)
        {
            var nowTime = DateTime.UtcNow;

            var model = new service_client_action_log()
            {
                LogSrNo = RandomTextHelper.CreateInt64String(20),
                RequestId = messagePackage.MessageId,
                ServiceClientName = config.ServiceClientName,
                ServiceClientHost = GetLocalIpAddress(),
                Direction = direction,
                ActionName = messagePackage.ActionName,
                LogTime = nowTime
            };

            switch (direction)
            {
                case 3:
                    model.RequestTime = messagePackage.PublishTime;
                    break;

                case 4:
                    model.ResponseTime = messagePackage.PublishTime;
                    break;
            }

            model.RemoteServiceName = config?.RemoteServiceName;
            model.RemoteServiceHost = GetRemoteHostString(config);

            messagePackage.Data.IsNotNullOrEmpty(d => model.DataContent = JsonConvert.DeserializeObject(messagePackage.Data));
            return model;
        }

        /// <summary>取得客戶端本機的 IP 位址
        /// </summary>
        /// <returns>客戶端本機的 IP 位址</returns>
        private static string GetLocalIpAddress()
        {
            return IPAddressHelper.GetLocalIPAddress();
        }

        /// <summary>取得遠端服務的主機連線資訊字串
        /// </summary>
        /// <param name="config">服務客戶端個體的設定組態</param>
        /// <returns>遠端服務的主機連線資訊字串</returns>
        private static string GetRemoteHostString(ServiceClientConfig config)
        {
            if (config.IsNull())
            {
                return null;
            }

            if (config.RemoteHostType.IsNullOrEmpty())
            {
                return null;
            }

            switch (config.RemoteHostType)
            {
                case "TCPSocket":
                    var tcpSocketConfig = GlobalClientContext.Config.TCPSocketProtocol.RemoteClients.Where(q => q.Name == config.RemoteClientName)?.FirstOrDefault();
                    return tcpSocketConfig.IsNull() ? null : $"tcp-socket://{tcpSocketConfig.TCPServerHost}:{tcpSocketConfig.TCPServerPort}";

                case "WebSocket":
                    var websocketConfig = GlobalClientContext.Config.WebSocketProtocol.RemoteClients.Where(q => q.Name == config.RemoteClientName)?.FirstOrDefault();
                    return websocketConfig.IsNull() ? null : websocketConfig.WebSocketHostBaseUrl.IsNullOrEmptyString(null);

                case "RabbitMQ":
                    var amqpConfig = GlobalClientContext.Config.RabbitMQProtocol.RemoteClients.Where(q => q.Name == config.RemoteClientName)?.FirstOrDefault();
                    return amqpConfig.IsNull() ? null : GetAmqpConnectionString(amqpConfig);

                // Pony Says: 因為 gRPC 屬於 Zayni Framework Middle.Service.Client 內建的 extension module，所以這邊我允許自行處理 FS_SERVICE_CLIENT_ACTION_LOG.REMOTE_HOST 欄位
                case "gRPC":
                    var gRPCClientCfgs = ((JToken)GlobalClientContext.JsonConfig.gRPCProtocol.remoteClients).ToObject<List<dynamic>>();
                    dynamic gRPCClientCfg = ServiceClientActionESLogger.GetDynamicClientConfig(gRPCClientCfgs, config.RemoteClientName);
                    return null == gRPCClientCfg ? null : $"grpc://{gRPCClientCfg.serverHost}:{gRPCClientCfg.serverPort}";

                case "HttpCore":
                    var httpClientCfgs = ((JToken)GlobalClientContext.JsonConfig.httpCoreProtocol.remoteClients).ToObject<List<dynamic>>();
                    dynamic httpClientCfg = ServiceClientActionESLogger.GetDynamicClientConfig(httpClientCfgs, config.RemoteClientName);
                    return null == httpClientCfg ? null : $"{httpClientCfg.httpHostUrl}";

                case "InProcess":
                    return $"in-process localhost";
            }

            return null;
        }

        /// <summary>取得 extension module 的 config 組態設定值
        /// </summary>
        /// <param name="extensionClientCfgs">Extension module Client 的組態設定集合</param>
        /// <param name="remoteClientName">通訊客戶端設定名稱</param>
        /// <returns>Extension Client 的 config 組態設定值</returns>
        private static dynamic GetDynamicClientConfig(List<dynamic> extensionClientCfgs, string remoteClientName)
        {
            foreach (var extensionClientCfg in extensionClientCfgs)
            {
                if (extensionClientCfg.name == remoteClientName)
                {
                    return extensionClientCfg;
                }
            }

            return null;
        }

        /// <summary>取得 RabbitMQ AMQP 的 URI 連線字串
        /// </summary>
        /// <param name="amqpConfig">RabbitMQ 通訊客戶端的設定組態</param>
        /// <returns>RabbitMQ AMQP 的 URI 連線字串</returns>
        private static string GetAmqpConnectionString(RabbitMQClientConfig amqpConfig)
        {
            var rpcUri = amqpConfig.RpcConfig.IsNotNull() ?
                $", RPCExchange={amqpConfig.RpcConfig.Exchange}, RPCRoutingKey={amqpConfig.RpcConfig.RoutingKey}, RPCReplyQueue={amqpConfig.RpcConfig.RpcReplyQueue}" : string.Empty;

            var publishUri = amqpConfig.PublishConfig.IsNotNull() ?
                $", PublishExchange={amqpConfig.PublishConfig.Exchange}, PublishRoutingKey={amqpConfig.PublishConfig.RoutingKey}" : string.Empty;

            var uri = "amqp://{uid}:{psw}@{host}:{port}/{vhost} DefaultExchange={defaultExchange}{rpc}{publish}".Format(
                    uid => amqpConfig.MbUserId,
                    psw => amqpConfig.MbPassword,
                    host => amqpConfig.MbHost,
                    port => amqpConfig.MbPort,
                    vhost => amqpConfig.MbVHost,
                    defaultExchange => amqpConfig.DefaultExchange,
                    rpc => rpcUri,
                    publish => publishUri
                );

            return uri;
        }

        #endregion Private Methods
    }
}
