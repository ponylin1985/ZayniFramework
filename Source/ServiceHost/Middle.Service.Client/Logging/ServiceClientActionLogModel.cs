﻿using Newtonsoft.Json;
using System;
using ZayniFramework.Common.ORM;
using System.Text.Json.Serialization;


namespace ZayniFramework.Middle.Service.Client
{
    /// <summary>客戶端動作請求日誌紀錄的資料模型 (ORM對應: FS_SERVICE_CLIENT_ACTION_LOG)
    /// </summary>
    [MappingTable(TableName = "FS_SERVICE_CLIENT_ACTION_LOG")]
    internal sealed class ServiceClientActionLogModel
    {
        /// <summary>日誌紀錄流水號
        /// </summary>
        [JsonProperty(PropertyName = "LOG_SRNO")]
        [TableColumn(ColumnName = "LOG_SRNO", IsPrimaryKey = true, OrderBy = true, OrderByDesc = true)]
        public long LogSrNo { get; set; }

        /// <summary>動作請求代碼
        /// </summary>
        [JsonProperty(PropertyName = "REQUEST_ID")]
        [TableColumn(ColumnName = "REQUEST_ID", IsAllowNull = true, DefaultValue = null)]
        public string RequestId { get; set; }

        /// <summary>請求客戶端的設定名稱
        /// </summary>
        [JsonProperty(PropertyName = "CLIENT_NAME")]
        [TableColumn(ColumnName = "CLIENT_NAME", IsAllowNull = true, DefaultValue = null)]
        public string ServiceClientName { get; set; }

        /// <summary>請求客戶端的主機位址
        /// </summary>
        [JsonProperty(PropertyName = "CLIENT_HOST")]
        [TableColumn(ColumnName = "CLIENT_HOST", IsAllowNull = true, DefaultValue = null)]
        public string ServiceClientHost { get; set; }

        /// <summary>遠端服務的設定名稱
        /// </summary>
        [JsonProperty(PropertyName = "REMOTE_NAME")]
        [TableColumn(ColumnName = "REMOTE_NAME", IsAllowNull = true, DefaultValue = null)]
        public string RemoteServiceName { get; set; }

        /// <summary>遠端服務的主機位址
        /// </summary>
        [JsonProperty(PropertyName = "REMOTE_HOST")]
        [TableColumn(ColumnName = "REMOTE_HOST", IsAllowNull = true, DefaultValue = null)]
        public string RemoteServiceHost { get; set; }

        /// <summary>動作方向<para/>
        /// 1: RPC 請求訊息。<para/>
        /// 2: RPC 回應訊息。
        /// </summary>
        [JsonProperty(PropertyName = "DIRECTION")]
        [TableColumn(ColumnName = "DIRECTION")]
        public int Direction { get; set; }

        /// <summary>動作名稱
        /// </summary>
        [JsonProperty(PropertyName = "ACTION_NAME")]
        [TableColumn(ColumnName = "ACTION_NAME")]
        public string ActionName { get; set; }

        /// <summary>資料集合，資料內容格式為 JSON 字串。
        /// </summary>
        [JsonProperty(PropertyName = "DATA_CONTENT")]
        [TableColumn(ColumnName = "DATA_CONTENT", IsAllowNull = true, DefaultValue = null)]
        public string DataContent { get; set; }

        /// <summary>原始請求時間
        /// </summary>
        [JsonProperty(PropertyName = "REQUEST_TIME")]
        [TableColumn(ColumnName = "REQUEST_TIME", IsAllowNull = true, DefaultValue = null)]
        public DateTime? RequestTime { get; set; }

        /// <summary>服務回應的時間
        /// </summary>
        [JsonProperty(PropertyName = "RESPONSE_TIME")]
        [TableColumn(ColumnName = "RESPONSE_TIME", IsAllowNull = true, DefaultValue = null)]
        public DateTime? ResponseTime { get; set; }

        /// <summary>服務執行動作是否成功
        /// </summary>
        [JsonProperty(PropertyName = "IS_SUCCESS")]
        [TableColumn(ColumnName = "IS_SUCCESS", IsAllowNull = true, DefaultValue = null)]
        public bool? IsSuccess { get; set; }

        /// <summary>服務回應的代碼
        /// </summary>
        [JsonProperty(PropertyName = "CODE")]
        [TableColumn(ColumnName = "CODE", IsAllowNull = true, DefaultValue = null)]
        public string Code { get; set; }

        /// <summary>服務回應的訊息
        /// </summary>
        [JsonProperty(PropertyName = "MESSAGE")]
        [TableColumn(ColumnName = "MESSAGE", IsAllowNull = true, DefaultValue = null)]
        public string Message { get; set; }

        /// <summary>日誌時間
        /// </summary>
        [JsonProperty(PropertyName = "LOG_TIME")]
        [TableColumn(ColumnName = "LOG_TIME")]
        public DateTime LogTime { get; set; }
    }

    /// <summary>客戶端動作請求日誌紀錄的資料模型 (ElasticSearch 日誌紀錄資料模型)
    /// </summary>
    internal sealed class service_client_action_log
    {
        /// <summary>日誌紀錄流水號
        /// </summary>
        [JsonPropertyName("log_srno")]
        [JsonProperty(PropertyName = "log_srno")]
        public string LogSrNo { get; set; }

        /// <summary>動作請求代碼
        /// </summary>
        [JsonPropertyName("request_id")]
        [JsonProperty(PropertyName = "request_id")]
        public string RequestId { get; set; }

        /// <summary>請求客戶端的設定名稱
        /// </summary>
        [JsonPropertyName("client_name")]
        [JsonProperty(PropertyName = "client_name")]
        public string ServiceClientName { get; set; }

        /// <summary>請求客戶端的主機位址
        /// </summary>
        [JsonPropertyName("client_host")]
        [JsonProperty(PropertyName = "client_host")]
        public string ServiceClientHost { get; set; }

        /// <summary>遠端服務的設定名稱
        /// </summary>
        [JsonPropertyName("remote_name")]
        [JsonProperty(PropertyName = "remote_name")]
        public string RemoteServiceName { get; set; }

        /// <summary>遠端服務的主機位址
        /// </summary>
        [JsonPropertyName("remote_host")]
        [JsonProperty(PropertyName = "remote_host")]
        public string RemoteServiceHost { get; set; }

        /// <summary>動作方向<para/>
        /// 1: RPC 請求訊息。<para/>
        /// 2: RPC 回應訊息。
        /// </summary>
        [JsonPropertyName("direction")]
        [JsonProperty(PropertyName = "direction")]
        public int Direction { get; set; }

        /// <summary>動作名稱
        /// </summary>
        [JsonPropertyName("action_name")]
        [JsonProperty(PropertyName = "action_name")]
        public string ActionName { get; set; }

        /// <summary>資料集合，資料內容格式為 JSON 字串。
        /// </summary>
        [JsonPropertyName("data_content")]
        [JsonProperty(PropertyName = "data_content")]
        public object DataContent { get; set; }

        /// <summary>原始請求時間
        /// </summary>
        [JsonPropertyName("request_time")]
        [JsonProperty(PropertyName = "request_time")]
        public DateTime? RequestTime { get; set; }

        /// <summary>服務回應的時間
        /// </summary>
        [JsonPropertyName("response_time")]
        [JsonProperty(PropertyName = "response_time")]
        public DateTime? ResponseTime { get; set; }

        /// <summary>服務執行動作是否成功
        /// </summary>
        [JsonPropertyName("is_success")]
        [JsonProperty(PropertyName = "is_success")]
        public bool? IsSuccess { get; set; }

        /// <summary>服務回應的代碼
        /// </summary>
        [JsonPropertyName("code")]
        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }

        /// <summary>服務回應的訊息
        /// </summary>
        [JsonPropertyName("message")]
        [JsonProperty(PropertyName = "message")]
        public string Message { get; set; }

        /// <summary>日誌時間
        /// </summary>
        [JsonPropertyName("log_time")]
        [JsonProperty(PropertyName = "log_time")]
        public DateTime LogTime { get; set; }
    }
}
