﻿using Newtonsoft.Json.Linq;
using System;
using System.Text;
using ZayniFramework.Common;
using ZayniFramework.Common.Tasks;
using ZayniFramework.Logging;
using ZayniFramework.Middle.Service.Entity;
using ZayniFramework.Serialization;


namespace ZayniFramework.Middle.Service.Client
{
    /// <summary>客戶端動作日誌器
    /// </summary>
    public static class ServiceClientActionLogger
    {
        #region 宣告私有的欄位

        /// <summary>非同步日誌工作佇列處理器
        /// </summary>
        /// <returns></returns>
        private static readonly AsyncWorker _worker = AsyncWorkerContainer.Resolve("ZayniFrameworkMiddleServiceHostLoggerAsyncWorker");

        /// <summary>資料倉儲物件
        /// </summary>
        private static readonly ServiceClientActionLogRepo _repo = new();

        /// <summary>客戶端動作 ElasticSearch 日誌器
        /// </summary>
        /// <returns></returns>
        private static readonly ServiceClientActionESLogger _esLogger = new();

        /// <summary>日誌設定名稱
        /// </summary>
        private static readonly string _loggerName = "ServiceClientActionLog";

        #endregion 宣告私有的欄位


        #region 宣告靜態建構子

        /// <summary>靜態建構子
        /// </summary>
        /// <returns></returns>
        static ServiceClientActionLogger() => _worker.Start();

        #endregion 宣告靜態建構子


        #region 宣告內部的方法

        /// <summary>記錄客戶端的請求動作日誌
        /// </summary>
        /// <param name="serviceClientName">服務客戶端的設定名稱</param>
        /// <param name="request">服務請求包裹</param>
        /// <param name="reqDTO">請求資料載體</param>
        public static void Log(string serviceClientName, RequestPackage request, object reqDTO)
        {
            reqDTO.IsNotNull(d => request.Data = NewtonsoftJsonConvert.Serialize(reqDTO));
            Log(serviceClientName, request);
        }

        /// <summary>記錄客戶端的請求動作日誌
        /// </summary>
        /// <param name="serviceClientName">服務客戶端的設定名稱</param>
        /// <param name="request">服務請求包裹</param>
        public static void Log(string serviceClientName, RequestPackage request)
        {
            When.True(CanDbLog(serviceClientName), () => _worker.Enqueue(async () => await _repo.LogAsync(serviceClientName, request)));
            _worker.Enqueue(async () => await _esLogger.LogAsync(serviceClientName, request));
            Log(request);
        }

        /// <summary>記錄客戶端的回應動作日誌
        /// </summary>
        /// <param name="serviceClientName">服務客戶端的設定名稱</param>
        /// <param name="response">服務回應包裹</param>
        /// <param name="resDTO">回應資料載體</param>
        public static void Log(string serviceClientName, ResponsePackage response, object resDTO)
        {
            resDTO.IsNotNull(d => response.Data = NewtonsoftJsonConvert.Serialize(resDTO));
            Log(serviceClientName, response);
        }

        /// <summary>記錄客戶端的回應動作日誌
        /// </summary>
        /// <param name="serviceClientName">服務客戶端的設定名稱</param>
        /// <param name="response">服務回應包裹</param>
        public static void Log(string serviceClientName, ResponsePackage response)
        {
            When.True(CanDbLog(serviceClientName), () => _worker.Enqueue(async () => await _repo.LogAsync(serviceClientName, response)));
            _worker.Enqueue(async () => await _esLogger.LogAsync(serviceClientName, response));
            Log(response);
        }

        /// <summary>紀錄客戶端發佈或接收訊息的動作日誌
        /// </summary>
        /// <param name="serviceClientName">服務客戶端的設定名稱</param>
        /// <param name="direction">動作方向: 3: PublishMessage，4: ReceiveMessage。</param>
        /// <param name="messagePackage">事件訊息資料包裹</param>
        public static void Log(string serviceClientName, int direction, MessagePackage messagePackage)
        {
            When.True(CanDbLog(serviceClientName), () => _worker.Enqueue(async () => await _repo.LogAsync(serviceClientName, direction, messagePackage)));
            _worker.Enqueue(async () => await _esLogger.LogAsync(serviceClientName, direction, messagePackage));
            Log(messagePackage);
        }

        #endregion 宣告內部的方法


        #region 宣告私有的方法

        /// <summary>檢查是否允許進行資料庫動作追蹤日誌紀錄
        /// </summary>
        /// <param name="serviceClientName">服務客戶端的設定名稱</param>
        /// <returns>是否允許進行資料庫動作追蹤日誌紀錄</returns>
        private static bool CanDbLog(string serviceClientName)
        {
            try
            {
                var config = ClientConfigReader.GetServiceClientConfig(serviceClientName);
                return config.EnableDbLogging;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>執行日誌紀錄
        /// </summary>
        /// <param name="package">通訊包裹集合</param>
        private static void Log(IPackage package)
        {
            var sbMessage = new StringBuilder();

            switch (package)
            {
                case RequestPackage request:
                    var req = request.Data;

                    if (request.Data.IsNotNullOrEmpty())
                    {
                        req = JObject.Parse(request.Data).ToString(Newtonsoft.Json.Formatting.Indented);
                    }

                    sbMessage.AppendLine();
                    sbMessage.AppendLine($"  RequestPackage detail");
                    sbMessage.AppendLine($"  - RequestId: {request.RequestId}");
                    sbMessage.AppendLine($"  - ActionName: {request.ActionName}");
                    sbMessage.AppendLine($"  - Data: {req}");
                    sbMessage.AppendLine($"  - RequestTime: {request.RequestTime}");
                    break;

                case ResponsePackage response:
                    sbMessage.AppendLine();
                    sbMessage.AppendLine($"  ResponsePackage detail");
                    sbMessage.AppendLine($"  - RequestId: {response.RequestId}");
                    sbMessage.AppendLine($"  - ActionName: {response.ActionName}");
                    sbMessage.AppendLine($"  - IsSuccess: {response.Success}");
                    sbMessage.AppendLine($"  - Code: {response.Code}");
                    sbMessage.AppendLine($"  - Data: {response.Data}");
                    sbMessage.AppendLine($"  - Message: {response.Message}");
                    break;

                case MessagePackage messagePackage:
                    sbMessage.AppendLine();
                    sbMessage.AppendLine($"  MessagePackage detail");
                    sbMessage.AppendLine($"  - MessageId: {messagePackage.MessageId}");
                    sbMessage.AppendLine($"  - Direction: {messagePackage.PackageType}");
                    sbMessage.AppendLine($"  - ActionName: {messagePackage.ActionName}");
                    sbMessage.AppendLine($"  - Data: {messagePackage.Data}");
                    sbMessage.AppendLine($"  - PublishTime: {messagePackage.PublishTime}");
                    break;
            }

            Logger.Info(nameof(ServiceClientActionLogger), sbMessage.ToString(), Logger.GetTraceLogTitle(nameof(ServiceClientActionLogger), nameof(Log)), _loggerName);
        }

        #endregion 宣告私有的方法
    }
}
