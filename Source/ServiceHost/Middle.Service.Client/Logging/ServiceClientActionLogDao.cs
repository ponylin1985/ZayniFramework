﻿using System;
using System.Threading.Tasks;
using ZayniFramework.DataAccess;
using ZayniFramework.Serialization;


namespace ZayniFramework.Middle.Service.Client
{
    /// <summary>客戶端動作日誌紀錄的資料存取類別
    /// </summary>
    internal sealed class ServiceClientActionLogDao : BaseDataAccess
    {
        #region 宣告私有的欄位

        /// <summary>資料庫連線名稱
        /// </summary>
        private static readonly string _dbName = "Zayni";

        #endregion 宣告私有的欄位


        #region 宣告建構子

        /// <summary>預設建構子
        /// </summary>
        internal ServiceClientActionLogDao() : base(_dbName)
        {
        }

        #endregion 宣告建構子


        #region 宣告內部的方法

        /// <summary>寫入客戶端請求動作日誌
        /// </summary>
        /// <param name="model">日誌資料模型</param>
        internal async Task InsertLogAsync(ServiceClientActionLogModel model)
        {
            try
            {
                #region 宣告 SQL 字串

                var sql = @"
                    -- Insert FS_SERVICE_CLIENT_ACTION_LOG
                    INSERT INTO `FS_SERVICE_CLIENT_ACTION_LOG` (
                          `REQUEST_ID`
                        , `CLIENT_NAME`
                        , `CLIENT_HOST`
                        , `REMOTE_NAME`
                        , `REMOTE_HOST`

                        , `DIRECTION`
                        , `ACTION_NAME`
                        , `DATA_CONTENT`
                        , `REQUEST_TIME`
                        , `RESPONSE_TIME`

                        , `IS_SUCCESS`
                        , `CODE`
                        , `MESSAGE`
                        , `LOG_TIME`
                    ) VALUES (
                          @RequestId
                        , @ServiceClientName
                        , @ServiceClientHost
                        , @RemoteServiceName
                        , @RemoteServiceHost

                        , @Direction
                        , @ActionName
                        , @DataContent
                        , @RequestTime
                        , @ResponseTime

                        , @IsSuccess
                        , @Code
                        , @Message
                        , @LogTime
                    ) ";

                #endregion 宣告 SQL 字串

                #region 執行資料新增

                using var conn = await base.CreateConnectionAsync();
                var cmd = base.GetSqlStringCommand(sql, conn);
                cmd.CommandTimeout = 30;

                base.AddInParameter(cmd, "@RequestId", DbColumnType.String, model.RequestId);
                base.AddInParameter(cmd, "@ServiceClientName", DbColumnType.String, model.ServiceClientName);
                base.AddInParameter(cmd, "@ServiceClientHost", DbColumnType.String, model.ServiceClientHost);
                base.AddInParameter(cmd, "@RemoteServiceName", DbColumnType.String, model.RemoteServiceName);
                base.AddInParameter(cmd, "@RemoteServiceHost", DbColumnType.String, model.RemoteServiceHost);

                base.AddInParameter(cmd, "@Direction", DbColumnType.Byte, model.Direction);
                base.AddInParameter(cmd, "@ActionName", DbColumnType.String, model.ActionName);
                base.AddInParameter(cmd, "@DataContent", DbColumnType.String, model.DataContent);
                base.AddInParameter(cmd, "@RequestTime", DbColumnType.DateTime, model.RequestTime);
                base.AddInParameter(cmd, "@ResponseTime", DbColumnType.DateTime, model.ResponseTime);

                base.AddInParameter(cmd, "@IsSuccess", DbColumnType.Boolean, model.IsSuccess);
                base.AddInParameter(cmd, "@Code", DbColumnType.String, model.Code);
                base.AddInParameter(cmd, "@Message", DbColumnType.String, model.Message);
                base.AddInParameter(cmd, "@LogTime", DbColumnType.DateTime, model.LogTime);

                if (!await base.ExecuteNonQueryAsync(cmd))
                {
                    await conn.CloseAsync();
                    return;
                }

                await conn.CloseAsync();

                #endregion 執行資料新增
            }
            catch
            {
                await ConsoleOutputLogger.LogAsync($"ZayniFramework service host client action log:{Environment.NewLine}{NewtonsoftJsonConvert.Serialize(model)}");
            }
        }

        #endregion 宣告內部的方法
    }
}
