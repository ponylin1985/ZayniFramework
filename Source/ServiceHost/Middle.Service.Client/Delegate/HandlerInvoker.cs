using System;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.Middle.Service.Client
{
    /// <summary>服務處理委派調用器
    /// </summary>
    public static class HandlerInvoker
    {
        /// <summary>調用遠端服務的連線中斷的處理委派
        /// </summary>
        /// <param name="handler">遠端服務的連線中斷的處理委派</param>
        public static void Invoke(ConnectionBrokenHandler handler)
        {
            var logTitle = Logger.GetTraceLogTitle(nameof(HandlerInvoker), nameof(Invoke));

            try
            {
                if (handler.IsNull())
                {
                    return;
                }

                handler?.Invoke();
            }
            catch (Exception ex)
            {
                Logger.Error(nameof(HandlerInvoker), $"Invoke {nameof(ConnectionBrokenHandler)} occur exception. {Environment.NewLine}{ex}", logTitle, "ServiceHostTrace");
            }

            Logger.Info(nameof(HandlerInvoker), $"Invoke {nameof(ConnectionBrokenHandler)} successfully.", logTitle, "ServiceHostTrace");
        }

        /// <summary>調用遠端服務的連線重新恢復的處理委派
        /// </summary>
        /// <param name="handler">遠端服務的連線重新恢復的處理委派</param>
        public static void Invoke(ConnectionRestoreHandler handler)
        {
            var logTitle = Logger.GetTraceLogTitle(nameof(HandlerInvoker), nameof(Invoke));

            try
            {
                if (handler.IsNull())
                {
                    return;
                }

                handler?.Invoke();
            }
            catch (Exception ex)
            {
                Logger.Error(nameof(HandlerInvoker), $"Invoke {nameof(ConnectionRestoreHandler)} occur exception. {Environment.NewLine}{ex}", logTitle, "ServiceHostTrace");
            }

            Logger.Info(nameof(HandlerInvoker), $"Invoke {nameof(ConnectionRestoreHandler)} successfully.", logTitle, "ServiceHostTrace");
        }
    }
}