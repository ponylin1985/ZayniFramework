﻿using System;


namespace ZayniFramework.Middle.Service.Client
{
    /// <summary>事件訊息處理器基底
    /// </summary>
    public abstract class EventProcessor
    {
        #region 宣告建構子

        /// <summary>多載建構子
        /// </summary>
        /// <param name="actionName">動作名稱 (事件名稱)</param>
        /// <param name="dataContentType">訊息資料內容的型別</param>
        public EventProcessor(string actionName, Type dataContentType)
        {
            Name = actionName;
            DataType = dataContentType;
        }

        /// <summary>解構子
        /// </summary>
        ~EventProcessor()
        {
            Name = null;
            DataType = null;
            RawData = null;
        }

        #endregion 宣告建構子


        #region 宣告公開的屬性

        /// <summary>動作名稱 (事件名稱)
        /// </summary>
        public string Name { get; private set; }

        #endregion 宣告公開的屬性


        #region 宣告內部的屬性

        /// <summary>訊息資料內容的型別
        /// </summary>
        internal Type DataType { get; private set; }

        /// <summary>訊息資料內容
        /// </summary>
        internal object RawData { get; set; }

        /// <summary>是否需要自動對請求參數集合進行資料驗證，程式預設為 true。
        /// </summary>
        internal bool RequestValidation { get; set; } = true;

        #endregion 宣告內部的屬性


        #region 宣告抽象

        /// <summary>執行事件訊息處理
        /// </summary>
        public abstract void Execute();

        #endregion 宣告抽象
    }

    /// <summary>事件訊息處理器基底
    /// </summary>
    /// <typeparam name="TDataContent">訊息資料內容的泛型</typeparam>
    public abstract class EventProcessor<TDataContent> : EventProcessor
    {
        #region 宣告建構子

        /// <summary>多載建構子
        /// </summary>
        /// <param name="actionName">動作名稱 (事件名稱)</param>
        public EventProcessor(string actionName) : base(actionName, typeof(TDataContent))
        {
            // pass
        }

        #endregion 宣告建構子


        #region 宣告內部的屬性

        /// <summary>訊息資料內容
        /// </summary>
        protected TDataContent DataContent => (TDataContent)base.RawData;

        #endregion 宣告內部的屬性
    }
}
