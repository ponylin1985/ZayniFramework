using Newtonsoft.Json;
using ZayniFramework.Middle.Service.Client;


namespace ZayniFramework.Middle.Service.HttpCore.Client
{
    /// <summary>HttpCore 通訊客戶端的設定組態
    /// </summary>
    public sealed class HttpCoreClientConfig : RemoteClientConfig
    {
        /// <summary>HttpCore Service Host Web API 服務的 URL 服務位址。<para/>
        /// * httpHostUrl 設定值假若為 `/action` 結尾，代表呼叫一般 Non-async 的 ServiceAction，譬如: `http://localhost:5110/action/`
        /// * httpHostUrl 設定值假若為 `/action/async` 結尾，代表呼叫非同步作業 Async 的 ServiceActionAsync，譬如: `http://localhost:5100/action/async/`。
        /// </summary>
        [JsonProperty(PropertyName = "httpHostUrl")]
        public string HttpHostUrl { get; internal set; }

        /// <summary>HttpCore Service Host 支援的 http 通訊協定版本，預設為 1.1。<para/>
        /// * 此設定值暫時無作用，原因是 Middle.ServiceHttpCore.Client 為 .NET Standard 2.0 的 assembly 組件，暫時無法支援 http/2 通訊協定，也無法設置 `HttpClient.DefaultRequestVersion` 屬性。<para/>
        /// * `1.1`: 代表 http 1.1 通訊協定。<para/>
        /// * `2`: 代表 http/2 通訊協定。
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "httpProtocolVersion")]
        public string HttpProtocolVersion { get; internal set; } = "1.1";

        /// <summary>HttpCore 請求後等待 Http Response 回應的逾時秒數
        /// </summary>
        [JsonProperty(PropertyName = "httpResponseTimeout")]
        public int HttpResponseTimeout { get; internal set; }
    }
}