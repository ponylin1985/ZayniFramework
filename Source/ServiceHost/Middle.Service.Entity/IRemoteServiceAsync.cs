using System.Threading.Tasks;


namespace ZayniFramework.Middle.Service.Entity
{
    /// <summary>非同步遠端服務介面
    /// </summary>
    public interface IRemoteServiceAsync
    {
        /// <summary>執行遠端服務動作
        /// </summary>
        /// <param name="request">遠端服務請求包裹</param>
        /// <returns>遠端服務回應包裹</returns>
        Task<ResponsePackage> ExecuteAsync(RequestPackage request);
    }
}
