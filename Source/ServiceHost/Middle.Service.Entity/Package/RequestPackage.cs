﻿using Newtonsoft.Json;
using System;
using System.Text.Json.Serialization;
using ZayniFramework.Validation;


namespace ZayniFramework.Middle.Service.Entity
{
    /// <summary>遠端服務請求包裹
    /// </summary>
    public class RequestPackage : IPackage
    {
        /// <summary>包裹種類
        /// </summary>
        [JsonPropertyName("packageType")]
        [JsonProperty(PropertyName = "packageType")]
        public virtual string PackageType { get; set; }

        /// <summary>服務的設定名稱 (在 WebSocket 與 HttpCore 通訊技術下，必須要要傳入此欄位)
        /// </summary>
        [JsonPropertyName("serviceName")]
        [JsonProperty(PropertyName = "serviceName")]
        public virtual string ServiceName { get; set; }

        /// <summary>請求代碼
        /// </summary>
        [JsonPropertyName("reqId")]
        [JsonProperty(PropertyName = "reqId")]
        [NotNullOrEmpty(Message = "The 'reqId' is null or empty string.")]
        public virtual string RequestId { get; set; }

        /// <summary>請求動作名稱
        /// </summary>
        [JsonPropertyName("action")]
        [JsonProperty(PropertyName = "action")]
        [NotNullOrEmpty(Message = "The 'action' is null or empty string.")]
        public virtual string ActionName { get; set; }

        /// <summary>資料集合，資料內容格式為 JSON 字串。
        /// </summary>
        [JsonPropertyName("data")]
        [JsonProperty(PropertyName = "data")]
        public virtual string Data { get; set; }

        /// <summary>請求時間戳記
        /// </summary>
        [JsonPropertyName("reqTime")]
        [JsonProperty(PropertyName = "reqTime")]
        [NotDefaultDateTime(Message = "The 'reqTime' can not be .NET default value of DateTime.")]
        public virtual DateTime RequestTime { get; set; }
    }
}
