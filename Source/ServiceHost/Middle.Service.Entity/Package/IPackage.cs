﻿namespace ZayniFramework.Middle.Service.Entity
{
    /// <summary>通訊資料包裹介面
    /// </summary>
    public interface IPackage
    {
        /// <summary>包裹種類
        /// </summary>
        string PackageType { get; set; }

        /// <summary>動作名稱
        /// </summary>
        string ActionName { get; set; }

        /// <summary>資料集合，資料內容格式為 JSON 字串。
        /// </summary>
        string Data { get; set; }
    }
}
