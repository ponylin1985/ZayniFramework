﻿namespace ZayniFramework.Middle.Service.Entity
{
    /// <summary>包裹種類
    /// </summary>
    public sealed class PackageType
    {
        /// <summary>RPC 遠端請求
        /// </summary>
        public const string RPCRequest = "Req";

        /// <summary>RPC 遠端回應
        /// </summary>
        public const string RPCResponse = "Res";

        /// <summary>事件訊息發佈
        /// </summary>
        public const string Publish = "Publish";
    }
}
