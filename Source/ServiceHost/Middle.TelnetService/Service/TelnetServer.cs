﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using ZayniFramework.Common;


namespace ZayniFramework.Middle.TelnetService
{
    /// <summary>Telnet 服務
    /// </summary>
    public sealed class TelnetServer
    {
        #region 宣告私有的欄位

        /// <summary>TCP 監聽器物件
        /// </summary>
        private TcpListener _tcpListener;

        /// <summary>TcpListener 是否開始監聽
        /// </summary>
        private bool _isStarted = false;

        /// <summary>Telnet 服務的密碼
        /// </summary>
        private string _password;

        #endregion 宣告私有的欄位


        #region 宣告公開的屬性

        /// <summary>Telnet 服務的密碼
        /// </summary>
        /// <value></value>
        public string Password => _password;

        #endregion 宣告公開的屬性


        #region 宣告建構子

        /// <summary>預設建構子
        /// </summary>
        /// <param name="port"></param>
        /// <param name="password"></param>
        public TelnetServer(int port, string password = null)
        {
            _tcpListener = new TcpListener(IPAddress.Any, port);
            _password = password;
        }

        /// <summary>解構子
        /// </summary>
        ~TelnetServer()
        {
            _tcpListener = null;
            _password = null;
        }

        #endregion 宣告建構子


        #region 宣告公開的方法

        /// <summary>啟動 Telnet Service 服物
        /// </summary>
        /// <param name="telnetClientCallback">Telnet 客戶端連線成功的回呼委派</param>
        public async Task StartServiceAsync(Action<StreamReader, StreamWriter, TcpClient> telnetClientCallback)
        {
            if (_isStarted)
            {
                return;
            }

            _tcpListener.Start();
            _isStarted = true;

            await Task.Factory.StartNew(() =>
            {
                while (true)
                {
                    var client = _tcpListener.AcceptTcpClient();
                    var ns = client.GetStream();

                    var writer = new StreamWriter(ns);
                    var reader = new StreamReader(ns);

                    if (_password.IsNotNullOrEmpty())
                    {
                        writer.WriteLine("Please enter password:");
                        writer.Flush();

                        var password = reader.ReadLine();

                        if (password != _password)
                        {
                            writer.WriteLine($"Incorrect telnet service password.");
                            writer.Flush();
                            client.Close();
                            client.Dispose();
                            writer.Close();
                            writer.Dispose();
                            reader.Close();
                            reader.Dispose();
                            continue;
                        }
                    }

                    writer.WriteLine($"Please enter command...");
                    writer.Flush();

                    Task.Factory.StartNew(() => telnetClientCallback?.Invoke(reader, writer, client));
                }
            });
        }

        /// <summary>啟動 Telnet Service 服物
        /// </summary>
        /// <param name="telnetClientCallback">Telnet 客戶端連線成功的回呼委派</param>
        public async Task StartAsyncServiceAsync(Action<StreamReader, StreamWriter, TcpClient> telnetClientCallback)
        {
            if (_isStarted)
            {
                return;
            }

            _tcpListener.Start();
            _isStarted = true;

            await Task.Factory.StartNew(async () =>
            {
                while (true)
                {
                    var client = _tcpListener.AcceptTcpClient();
                    var ns = client.GetStream();

                    var writer = new StreamWriter(ns);
                    var reader = new StreamReader(ns);

                    if (_password.IsNotNullOrEmpty())
                    {
                        await writer.WriteLineAsync("Please enter password:");
                        await writer.FlushAsync();

                        var password = await reader.ReadLineAsync();

                        if (password != _password)
                        {
                            await writer.WriteLineAsync($"Incorrect telnet service password.");
                            await writer.FlushAsync();
                            client.Close();
                            client.Dispose();
                            writer.Close();
                            writer.Dispose();
                            reader.Close();
                            reader.Dispose();
                            continue;
                        }
                    }

                    await writer.WriteLineAsync($"Please enter command...");
                    await writer.FlushAsync();
                    await Task.Factory.StartNew(() => telnetClientCallback?.Invoke(reader, writer, client));
                }
            });
        }

        #endregion 宣告公開的方法
    }
}
