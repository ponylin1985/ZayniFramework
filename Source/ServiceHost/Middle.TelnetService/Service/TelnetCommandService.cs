using System;
using System.IO;
using System.Net.Sockets;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.Middle.TelnetService
{
    /// <summary>Zayni Framework 框架內建預設的 Telnet Command 處理服務<para/>
    /// 在使用此 Telnet 服務之前需要在 zayni.json 的 appSettings 區段中設定以下的組態值:<para/>
    /// 1. TelnetServerPort: 此為 Telnet Server 的 Port 號，此為必要設定值，如果沒有設定，則不會開啟 Telnet Server。
    /// 2. TelnetServerPassword: 此為 Telnet Server 的連線密碼，此為非必要設定值。<para/>
    /// </summary>
    public class TelnetCommandService
    {
        /// <summary>啟動預設的 Telnet 命令接收服務。<para/>
        /// 在使用此 Telnet 服務之前需要在 zayni.json  的 appSettings 區段中設定以下的組態值:<para/>
        /// 1. TelnetServerPort: 此為 Telnet Server 的 Port 號，此為必要設定值，如果沒有設定，則不會開啟 Telnet Server。<para/>
        /// 2. TelnetServerPassword: 此為 Telnet Server 的連線密碼，此為非必要設定值。<para/>
        /// * 如果在 docker container 執行中，呼叫此方法仍不能將 process block 住，請呼叫 ConsoleCommandService.StartConsoleHostAsync 方法。
        /// </summary>
        /// <param name="commandContainer">命令容器池</param>
        /// <returns>啟動結果</returns>
        public static async Task<IResult> StartDefaultTelnetServiceAsync(CommandContainer commandContainer)
        {
            var result = Result.Create();

            if (commandContainer.IsNull())
            {
                result.Message = $"Can not start the telnet service due to the {nameof(CommandContainer)} argument is null.";
                return result;
            }

            var config = ConfigManager.GetZayniFrameworkSettings().TelnetServerSettings;

            if (!config.EnableTelnetServer)
            {
                result.Message = $"Can not start the telnet service due to the TelnetServerSettings.EnableTelnetServer is false.";
                return result;
            }

            var port = config.TelnetServerPort;

            if (port <= 0)
            {
                result.Message = $"Can not start the telnet service due to no 'TelnetServerPort' appSetting value is invalie. TelnetServerPort: {port}.";
                return result;
            }

            var telnetService = new TelnetServer(port, config.TelnetServerPassword);
            await telnetService.StartAsyncServiceAsync(async (reader, writer, client) => await ProcessTelnetCommandAsync(reader, writer, client, commandContainer));
            ConsoleLogger.WriteLine($"Start telnet server success. Listen tcp port on {port}.", ConsoleColor.Yellow);

            result.Success = true;
            return result;
        }

        /// <summary>處理 Telnet 的遠端 Command 指令
        /// </summary>
        /// <param name="reader">Telnet 網路輸入串流讀取器</param>
        /// <param name="writer">Telnet 網路輸出串流寫入器</param>
        /// <param name="tcpClient">Telnet 客戶端</param>
        /// <param name="container">命令容器</param>
        private static async Task ProcessTelnetCommandAsync(StreamReader reader, StreamWriter writer, TcpClient tcpClient, CommandContainer container)
        {
            if (reader.IsNull() || writer.IsNull() || !tcpClient.Connected)
            {
                return;
            }

            if (!reader.BaseStream.CanRead || !writer.BaseStream.CanWrite)
            {
                return;
            }

            while (true)
            {
                try
                {
                    // 20190530 Pony Bugfix: 修正在透過 RemoteCommand 把 telnet 連線中斷時，可以直接跳出 while 迴圈，而不會產生 runtime exception。
                    if (reader.IsNull() || writer.IsNull() || tcpClient.IsNull() || !tcpClient.Connected)
                    {
                        break;
                    }

                    if (!reader.BaseStream.CanRead || !writer.BaseStream.CanWrite)
                    {
                        break;
                    }

                    var commandText = await reader.ReadLineAsync();
                    var command = await container.GetAsync<RemoteCommand>(commandText);

                    if (command.IsNull())
                    {
                        await Task.Delay(2);
                        continue;
                    }

                    command.Parameters["CommandText"] = commandText;
                    command.Parameters["StreamReader"] = reader;
                    command.Parameters["StreamWriter"] = writer;
                    command.Parameters["TelnetClient"] = tcpClient;

                    command.CommandText = commandText;
                    command.StreamReader = reader;
                    command.StreamWriter = writer;
                    command.TelnetClient = tcpClient;
                    await command.ExecuteAsync();
                }
                catch (Exception ex)
                {
                    Logger.Exception(nameof(TelnetCommandService), ex, $"Process telnet command occur exception. {Environment.NewLine}{ex}");
                    await Task.Delay(2);
                    continue;
                }

                await Task.Delay(2);
            }
        }
    }
}