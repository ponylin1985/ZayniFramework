using System.Threading.Tasks;
using ZayniFramework.Common;


namespace ZayniFramework.Middle.TelnetService
{

    /// <summary>Unknow 遠端指令處理器 (未知的主控台指令)
    /// </summary>
    public class UnknowRemoteCommand : RemoteCommand, IUnknowCommand
    {
        /// <summary>執行命令處理
        /// </summary>
        /// <param name="parameters">命令參數集合</param>
        /// <returns>執行結果</returns>
        public override async Task<IResult> ExecuteAsync(ParameterCollection parameters)
        {
            await StdoutAsync($"Unknow command! {CommandText}");
            await StdoutAsync("Please enter 'help' to discover all support commands.");
            Result.Success = true;
            return Result;
        }
    }
}