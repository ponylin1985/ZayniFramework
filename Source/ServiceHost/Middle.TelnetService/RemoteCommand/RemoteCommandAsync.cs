using System;
using System.IO;
using System.Net.Sockets;
using System.Threading.Tasks;
using ZayniFramework.Common;


namespace ZayniFramework.Middle.TelnetService
{
    /// <summary>主控台命令
    /// </summary>
    public abstract class RemoteCommand : ConsoleCommand, IRemoteCommand
    {
        #region 宣告建構子

        /// <summary>預設建構子
        /// </summary>
        public RemoteCommand()
        {
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="command">原始輸入的命令字串</param>
        /// <param name="streamReader">串流讀取器物件</param>
        /// <param name="streamWriter">串流輸出器物件</param>
        public RemoteCommand(string command, StreamReader streamReader = null, StreamWriter streamWriter = null)
        {
            CommandText = command;
            StreamReader = streamReader;
            StreamWriter = streamWriter;

            Parameters["CommandText"] = command;
            Parameters["StreamReader"] = streamReader;
            Parameters["StreamWriter"] = streamWriter;
        }

        /// <summary>解構子
        /// </summary>
        ~RemoteCommand()
        {
            CommandText = null;
            StreamReader = null;
            StreamWriter = null;

            Parameters["CommandText"] = null;
            Parameters["StreamReader"] = null;
            Parameters["StreamWriter"] = null;
        }

        #endregion 宣告建構子


        #region 宣告內部的屬性

        /// <summary>串流讀取器物件
        /// </summary>
        /// <value></value>
        public StreamReader StreamReader { get; set; }

        /// <summary>串流輸出器物件
        /// </summary>
        /// <value></value>
        public StreamWriter StreamWriter { get; set; }

        /// <summary>Telnet 客戶端
        /// </summary>
        public TcpClient TelnetClient { get; set; }

        #endregion 宣告內部的屬性


        #region 宣告公開的方法

        /// <summary>寫入訊息
        /// </summary>
        public override async Task StdoutLnAsync()
        {
            await StreamWriter.IsNull(async () => await base.StdoutLnAsync());
            await WriteToStreamAsync(Environment.NewLine);
        }

        /// <summary>寫入錯誤訊息
        /// </summary>
        /// <param name="message">寫入錯誤訊息</param>
        public override async Task StdoutErrAsync(string message)
        {
            await StreamWriter.IsNull(async () => await base.StdoutErrAsync(message));
            await WriteToStreamAsync(message);
        }

        /// <summary>寫入訊息
        /// </summary>
        /// <param name="message">訊息內容</param>
        /// <param name="color">訊息字體顏色</param>
        public override async Task StdoutAsync(string message, ConsoleColor color = ConsoleColor.Gray)
        {
            await StreamWriter.IsNull(async () => await base.StdoutAsync(message, color));
            await WriteToStreamAsync(message);
        }

        #endregion 宣告公開的方法


        #region 宣告私有的方法

        /// <summary>寫入訊息到網路串流中
        /// </summary>
        /// <param name="message">訊息內容</param>
        private async Task WriteToStreamAsync(string message = "")
        {
            try
            {
                if (new object[] { TelnetClient, StreamWriter }.HasNullElements())
                {
                    return;
                }

                if (!TelnetClient.Connected)
                {
                    return;
                }

                await StreamWriter?.WriteLineAsync(message);
                await StreamWriter?.FlushAsync();
            }
            catch
            {
                // pass
            }
        }

        #endregion 宣告私有的方法
    }
}