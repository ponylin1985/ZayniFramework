using ZayniFramework.Common;


namespace ZayniFramework.Middle.TelnetService
{
    /// <summary>Telnet 遠端指令處理介面
    /// </summary>
    public interface IRemoteCommand : ICommand
    {
    }
}