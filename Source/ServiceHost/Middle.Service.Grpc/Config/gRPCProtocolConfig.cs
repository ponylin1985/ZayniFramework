using Newtonsoft.Json;


namespace ZayniFramework.Middle.Service.Grpc
{
    /// <summary>gRPC 通訊連線組態設定
    /// </summary>

    public class gRPCProtocolConfig : ProtocolConfig
    {
        /// <summary>gRPC Server 監聽的 domain 或 IP 位址
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "gRPCHostServer")]
        public string GrpcHostServer { get; set; }

        /// <summary>gRPC Server 監聽的 Port
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "port")]
        public int Port { get; set; }
    }
}