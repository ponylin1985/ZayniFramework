using Grpc.Core;
using NeoSmart.AsyncLock;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Logging;
using ZayniFramework.Middle.Service.Entity;
using ZayniFramework.Middle.Service.Grpc.Entity;


namespace ZayniFramework.Middle.Service.Grpc
{
    /// <summary>gRPC 遠端自我裝載服務
    /// </summary>
    public class gRPCRemoteHost : GRPCServiceHost.GRPCServiceHostBase, IRemoteHost
    {
        #region 宣告私有的欄位

        /// <summary>非同步作業鎖定物件
        /// </summary>
        private readonly AsyncLock _asyncLock = new();

        /// <summary>IsHostReady 屬性的非同步作業鎖定物件
        /// </summary>
        private readonly AsyncLock _asyncLockService = new();

        /// <summary>遠端服務是否註冊與啟動完成
        /// </summary>
        private bool _isHostReady;

        /// <summary>遠端服務
        /// </summary>
        private RemoteService _remoteService;

        /// <summary>gRPC 的 Server 服務
        /// </summary>
        private Server _gRPCServer;

        /// <summary>gRPC Host 的通訊設定組態
        /// </summary>
        private gRPCProtocolConfig _config;

        #endregion 宣告私有的欄位


        #region 宣告公開的屬性

        /// <summary>服務設定名稱
        /// </summary>
        public string ServiceName { get; set; }

        /// <summary>服務託管 Host 的設定名稱
        /// </summary>
        public string HostName { get; set; }

        /// <summary>遠端服務是否註冊與啟動完成。
        /// </summary>
        public bool IsHostReady
        {
            get
            {
                using (_asyncLockService.Lock())
                {
                    return _isHostReady;
                }
            }
            private set
            {
                using (_asyncLockService.Lock())
                {
                    _isHostReady = value;
                }
            }
        }

        #endregion 宣告公開的屬性


        #region 宣告公開的方法

        /// <summary>初始化
        /// </summary>
        /// <param name="serviceName">服務設定名稱</param>
        public void Initialize(string serviceName = null)
        {
            ServiceName = serviceName.IsNullOrEmptyString(GlobalContext.Config.ServiceHosts?.FirstOrDefault()?.ServiceName);
            HostName = ConfigReader.GetHostName(ServiceName);   // 這邊 serviceHostConfig.json 設定上的限制為: 不能使用 defaultHost，一定要明確指定 hostName 屬性!

            _remoteService = new RemoteService(ServiceName);
            RemoteServiceContainer.Add(ServiceName, _remoteService);

            dynamic config = GlobalContext.JsonConfig;
            JToken hostConfigs = config.gRPCHostSettings.hosts;

            var hosts = hostConfigs.ToObject<List<gRPCProtocolConfig>>();
            var hostCfg = hosts.Where(h => h.HostName == HostName)?.FirstOrDefault();
            _config = hostCfg;

            _gRPCServer = new Server
            {
                Services = { GRPCServiceHost.BindService(this) },
                Ports = { new ServerPort(hostCfg.GrpcHostServer, hostCfg.Port, ServerCredentials.Insecure) }
            };
        }

        /// <summary>啟動自我裝載服務
        /// </summary>
        /// <returns>啟動結果</returns>
        public Result StartHost()
        {
            using (_asyncLock.Lock())
            {
                Middle.Service.ConsoleOutputLogger.Log($"Starting gRPCRemoteHost service.", ConsoleColor.Yellow, true, true);
                Logger.Info(this, $"Starting gRPCRemoteHost service.", nameof(StartHost), "ServiceHostTrace");

                var result = Result.Create();

                #region 檢查 ServiceHost 的啟動狀態

                if (IsHostReady)
                {
                    result.Success = true;
                    return result;
                }

                #endregion 檢查 ServiceHost 的啟動狀態

                #region 啟動 ServiceHost 服務

                try
                {
                    _gRPCServer.Start();
                }
                catch (Exception ex)
                {
                    result.Message = $"Start gRPCRemoteHost service occur exception: {ex}";
                    Logger.Exception(this, ex, result.Message, "ServiceHostTrace");
                    return result;
                }

                #endregion 啟動 ServiceHost 服務

                #region 設定回傳值

                IsHostReady = true;
                result.Success = true;
                ConsoleOutputLogger.Log($"Start gRPCRemoteHost service success. Listening gRPC on {_config.GrpcHostServer}:{_config.Port}.");

                #endregion 設定回傳值

                Middle.Service.ConsoleOutputLogger.Log($"End of starting gRPCRemoteHost service.", ConsoleColor.Yellow, true, true);
                Logger.Info(this, $"End of starting gRPCRemoteHost service.", nameof(StartHost), "ServiceHostTrace");
                return result;
            }
        }

        /// <summary>關閉自我裝載服務
        /// </summary>
        /// <returns>關閉結果</returns>
        public Result StopHost()
        {
            using (_asyncLock.Lock())
            {
                Middle.Service.ConsoleOutputLogger.Log($"Stoping gRPCRemoteHost service.", ConsoleColor.Yellow, true, true);
                Logger.Info(this, $"Stoping gRPCRemoteHost service.", nameof(StartHost), "ServiceHostTrace");

                var result = Result.Create();

                #region 檢查 ServiceHost 的啟動狀態

                if (!IsHostReady)
                {
                    result.Success = true;
                    return result;
                }

                #endregion 檢查 ServiceHost 的啟動狀態

                #region 關閉 ServiceHost 服務

                try
                {
                    _gRPCServer.ShutdownAsync().ConfigureAwait(false);
                }
                catch (Exception ex)
                {
                    result.Message = $"Stop gRPCRemoteHost service occur exception: {ex}";
                    Logger.Exception(this, ex, result.Message, "ServiceHostTrace");
                    return result;
                }

                #endregion 關閉 ServiceHost 服務

                #region 設定回傳值

                IsHostReady = false;
                result.Success = true;

                #endregion 設定回傳值

                Middle.Service.ConsoleOutputLogger.Log($"End of stoping gRPCRemoteHost service.", ConsoleColor.Yellow, true, true);
                Logger.Info(this, $"End of stoping gRPCRemoteHost service.", nameof(StartHost), "ServiceHostTrace");
                return result;
            }
        }

        /// <summary>執行遠端服務動作
        /// </summary>
        /// <param name="request">遠端服務請求包裹</param>
        /// <returns>遠端服務回應包裹</returns>
        public Service.Entity.ResponsePackage Execute(Service.Entity.RequestPackage request) =>
            RemoteServiceContainer.Get(ServiceName).Execute(request);

        /// <summary>發佈事件訊息
        /// </summary>
        /// <param name="messagePackage">訊息資料包裹</param>
        /// <returns>訊息發佈結果</returns>
        public Result Publish(MessagePackage messagePackage) =>
            throw new NotSupportedException("gRPCRemoteHost does not support message publish now...");

        #endregion 宣告公開的方法


        #region 宣告 gRPC 公開的服務方法

        /// <summary>處理 gRPC 請求
        /// </summary>
        /// <param name="gRequestPackage">gRPC 請求資料載體</param>
        /// <param name="context"></param>
        /// <returns>gRPC 回應資料載體</returns>
        public override async Task<Middle.Service.Grpc.Entity.ResponsePackage> Execute(Middle.Service.Grpc.Entity.RequestPackage gRequestPackage, ServerCallContext context)
        {
            try
            {
                var requestPackage = new Service.Entity.RequestPackage()
                {
                    PackageType = PackageType.RPCRequest,
                    ServiceName = gRequestPackage.ServiceName,
                    RequestId = gRequestPackage.RequestId,
                    ActionName = gRequestPackage.ActionName,
                    Data = gRequestPackage.Data,
                    RequestTime = Convert.ToInt64(gRequestPackage.RequestTime).ToDateTimeFromUnixTimestamp(withMilliseconds: true)
                };

                Service.Entity.ResponsePackage responsePackage;

                var remoteService = RemoteServiceContainer.Get(ServiceName);

                if (requestPackage.ActionName.EndsLike("Async"))
                {
                    responsePackage = await remoteService.ExecuteAsync(requestPackage);
                }
                else
                {
                    responsePackage = remoteService.Execute(requestPackage);
                }

                // 就目前的 .proto 產生的 ProtoBuf 這個欄位不允許為 Null 值
                var gResponsePackage = new Middle.Service.Grpc.Entity.ResponsePackage()
                {
                    PackageType = PackageType.RPCResponse,
                    RequestId = gRequestPackage.RequestId,
                    ActionName = gRequestPackage.ActionName,
                    IsSuccess = responsePackage.Success.ToString(),
                    Code = responsePackage.Code.IsNullOrEmptyString(""),
                    Data = responsePackage.Data.IsNullOrEmptyString(""),
                    Message = responsePackage.Message.IsNullOrEmptyString("")
                };

                return gResponsePackage;
            }
            catch (Exception ex)
            {
                var errorResponse = new Middle.Service.Grpc.Entity.ResponsePackage()
                {
                    PackageType = PackageType.RPCResponse,
                    RequestId = gRequestPackage.RequestId,
                    ActionName = gRequestPackage.ActionName,
                    IsSuccess = false.ToString(),
                    Code = Middle.Service.Entity.StatusCode.INTERNAL_ERROR,
                    Data = "",
                    Message = $"gRPCRemoteHost process RPC request occur exception."
                };

                Logger.Exception(this, ex, errorResponse.Message, "ServiceHostTrace");
                return errorResponse;
            }
        }

        #endregion 宣告 gRPC 公開的服務方法
    }
}