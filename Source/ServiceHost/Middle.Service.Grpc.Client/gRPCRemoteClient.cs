﻿using Grpc.Core;
using NeoSmart.AsyncLock;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using ZayniFramework.Common;
using ZayniFramework.Logging;
using ZayniFramework.Middle.Service.Client;
using ZayniFramework.Middle.Service.Entity;
using ZayniFramework.Middle.Service.Grpc.Entity;
using StatusCode = ZayniFramework.Middle.Service.Entity.StatusCode;


namespace ZayniFramework.Middle.Service.Grpc.Client
{
    /// <summary>gRPC 客戶端類別
    /// </summary>
    public class gRPCRemoteClient : RemoteClient
    {
        #region Private Fields

        /// <summary>非同步作業鎖定物件
        /// </summary>
        /// <returns></returns>
        private readonly AsyncLock _asyncLock = new();

        /// <summary>非同步作業鎖定物件: 鎖定 _timer 欄位
        /// </summary>
        private readonly AsyncLock _asyncLockTimer = new();

        /// <summary>服務客戶端個體的設定組態
        /// </summary>
        private ServiceClientConfig _serviceClientConfig;

        /// <summary>gRPC 客戶端連線組態設定
        /// </summary>
        private gRPCClientConfig _gRPCClientConfig;

        /// <summary>gRPC 遠端呼叫的逾時秒數
        /// </summary>
        private double _rpcTimeout;

        /// <summary>gRPC 的通道物件
        /// </summary>
        private Channel _channel;

        /// <summary>前一次檢查到到 gRPC Channel 連線狀態
        /// </summary>
        private ChannelState _lastChannelState;

        /// <summary>gRPC Client 客戶端物件
        /// </summary>
        private GRPCServiceHost.GRPCServiceHostClient _client;

        /// <summary>gRPC 連線狀態定時檢查器
        /// </summary>
        private Timer _timer = new();

        #endregion Private Fields


        #region Constructors

        /// <summary>多載建構子
        /// </summary>
        /// <param name="serviceClientName">服務客戶端個體設定名稱</param>
        public gRPCRemoteClient(string serviceClientName) : base(serviceClientName)
        {
            _timer.Interval = TimeSpan.FromSeconds(2).TotalMilliseconds;
            _timer.Elapsed += CheckConnection;
        }

        /// <summary>解構子
        /// </summary>
        ~gRPCRemoteClient()
        {
            _timer.Elapsed -= CheckConnection;
            _timer = null;
        }

        /// <summary>定時檢查 WebSocket 連線
        /// </summary>
        /// <param name="sender">事件觸發物件</param>
        /// <param name="e">定時器事件參數</param>
        private void CheckConnection(object sender, System.Timers.ElapsedEventArgs e)
        {
            using (_asyncLockTimer.Lock())
            {
                try
                {
                    if (_channel.State != ChannelState.Ready)
                    {
                        _channel.ConnectAsync(DateTime.UtcNow.AddSeconds(1)).ConfigureAwait(false).GetAwaiter().GetResult();
                    }
                }
                catch (Exception)
                {
                    // Do nothting here...
                }

                if (_channel.State == ChannelState.Ready && _lastChannelState == ChannelState.Ready)
                {
                    return;
                }

                if (_lastChannelState == ChannelState.Ready && _channel.State != ChannelState.Ready)
                {
                    _lastChannelState = _channel.State;
                    HandlerInvoker.Invoke(ConnectionBroken);
                    return;
                }

                if (_lastChannelState != ChannelState.Ready && _channel.State == ChannelState.Ready)
                {
                    _lastChannelState = ChannelState.Ready;
                    HandlerInvoker.Invoke(ConnectionRestored);
                    return;
                }

                _lastChannelState = _channel.State;
            }
        }

        #endregion Constructors


        #region Public Methods

        /// <summary>初始化遠端服務連線
        /// </summary>
        /// <returns>連線是否正常建立</returns>
        public override bool Initialize()
        {
            using (_asyncLock.Lock())
            {
                try
                {
                    _serviceClientConfig = ClientConfigReader.GetServiceClientConfig(ServiceClientName);

                    dynamic config = GlobalClientContext.JsonConfig;
                    JToken rClientCfg = config.gRPCProtocol.remoteClients;

                    var remoteClients = rClientCfg.ToObject<List<gRPCClientConfig>>();
                    var remoteClientCfg = remoteClients.Where(n => n.Name == _serviceClientConfig.RemoteClientName)?.FirstOrDefault();
                    _gRPCClientConfig = remoteClientCfg;
                    _rpcTimeout = remoteClientCfg.RpcTimeout ?? 15;

                    _channel = new Channel($"{_gRPCClientConfig.ServerHost}:{_gRPCClientConfig.ServerPort}", ChannelCredentials.Insecure);
                    _client = new GRPCServiceHost.GRPCServiceHostClient(_channel);
                    _timer.Start();
                }
                catch (Exception ex)
                {
                    Logger.Exception(this, ex, nameof(Initialize), "Establish gRPCRemoteClient channel occur exception.", "ServiceHostTrace");
                    return false;
                }

                return true;
            }
        }

        /// <summary>發送 RPC 遠端請求訊息
        /// </summary>
        /// <param name="requestId">請求識別代碼</param>
        /// <param name="actionName">動作名稱</param>
        /// <param name="requestDataContent">請求資料內容 JSON 集合</param>
        /// <returns>執行結果</returns>
        public override Result<ZayniFramework.Middle.Service.Entity.ResponsePackage> Execute(string requestId, string actionName, string requestDataContent = null)
        {
            var result = Result.Create<ZayniFramework.Middle.Service.Entity.ResponsePackage>();

            #region 設定請求包裹

            var reqTime = DateTime.UtcNow;

            var grpcRequestPackage = new ZayniFramework.Middle.Service.Grpc.Entity.RequestPackage()
            {
                PackageType = PackageType.RPCRequest,
                ServiceName = _serviceClientConfig.RemoteServiceName,
                RequestId = requestId,
                ActionName = actionName,
                Data = requestDataContent,
                RequestTime = reqTime.ToUnixUtcTimestamp(withMilliseconds: true).ToString()
            };

            var requestPackage = new ZayniFramework.Middle.Service.Entity.RequestPackage()
            {
                PackageType = grpcRequestPackage.PackageType,
                ServiceName = grpcRequestPackage.ServiceName,
                RequestId = grpcRequestPackage.RequestId,
                ActionName = grpcRequestPackage.ActionName,
                Data = grpcRequestPackage.Data,
                RequestTime = reqTime
            };

            ServiceClientActionLogger.Log(ServiceClientName, requestPackage);

            #endregion 設定請求包裹

            #region 執行 gRPC 遠端呼叫

            ZayniFramework.Middle.Service.Grpc.Entity.ResponsePackage grpcResponsePackage = null;

            try
            {
                grpcResponsePackage = _client.Execute(grpcRequestPackage, deadline: DateTime.UtcNow.AddSeconds(_rpcTimeout));
            }
            catch (Exception ex)
            {
                // 目前發現新版本的 C# gRPC Client 套件，如果在 alpine 3.9 以上的 docker container 執行時，只要使用「非 IP address」進行對 gRPC service 連線，就會產生以下的 exception，暫時找不到解決方法。
                // Grpc.Core.RpcException: Status(StatusCode=Unavailable, Detail="DNS resolution failed")
                // https://github.com/grpc/grpc/issues/18680
                // 但如果使用 ubuntu 18.04 bionic 的 docker container 執行，就可以使用 donaim name 對 gRPC service 進行連線，gRPC DNS Resolve 就會正常，因此，如果要使用 gRPC Client 建議使用 ubuntu 18.04 bionic 的 docker image。
                result.ExceptionObject = ex;
                result.Code = StatusCode.CLIENT_ERROR;
                result.Message = $"Send RequestPackage to remote gRPC service host occur exception. ChannelState: {_channel.State}, RequestId: {requestPackage.RequestId}, ActionName: {requestPackage.ActionName}, RemoteHost: {_gRPCClientConfig?.ServerHost}:{_gRPCClientConfig?.ServerPort}.";
                Logger.Exception(this, ex, nameof(Execute), result.Message, "ServiceHostTrace");
                return result;
            }

            #endregion 執行 gRPC 遠端呼叫

            #region 取得 gRPC 遠端呼叫的回應包裹

            var responsePackage = new ZayniFramework.Middle.Service.Entity.ResponsePackage()
            {
                PackageType = grpcResponsePackage.PackageType,
                RequestId = grpcResponsePackage.RequestId,
                ActionName = grpcResponsePackage.ActionName,
                Success = Convert.ToBoolean(grpcResponsePackage.IsSuccess),
                Code = grpcResponsePackage.Code,
                Data = grpcResponsePackage.Data,
                Message = grpcResponsePackage.Message
            };

            #endregion 取得 gRPC 遠端呼叫的回應包裹

            #region 設定回傳值

            if (!responsePackage.Success)
            {
                result.Code = responsePackage.Code;
                result.Message = responsePackage.Message;
                return result;
            }

            result.Data = responsePackage;
            result.Message = responsePackage.Message;
            result.Success = responsePackage.Success;

            #endregion 設定回傳值

            return result;
        }

        /// <summary>非同步作業發送 RPC 遠端請求訊息
        /// </summary>
        /// <param name="requestId">請求識別代碼</param>
        /// <param name="actionName">動作名稱</param>
        /// <param name="requestDataContent">請求資料內容 JSON 集合</param>
        /// <returns>執行結果</returns>
        public override async Task<Result<Service.Entity.ResponsePackage>> ExecuteAsync(string requestId, string actionName, string requestDataContent = null)
        {
            var result = Result.Create<ZayniFramework.Middle.Service.Entity.ResponsePackage>();

            #region 設定請求包裹

            var reqTime = DateTime.UtcNow;

            var grpcRequestPackage = new ZayniFramework.Middle.Service.Grpc.Entity.RequestPackage()
            {
                PackageType = PackageType.RPCRequest,
                ServiceName = _serviceClientConfig.RemoteServiceName,
                RequestId = requestId,
                ActionName = actionName,
                Data = requestDataContent,
                RequestTime = reqTime.ToUnixUtcTimestamp(withMilliseconds: true).ToString()
            };

            var requestPackage = new ZayniFramework.Middle.Service.Entity.RequestPackage()
            {
                PackageType = grpcRequestPackage.PackageType,
                ServiceName = grpcRequestPackage.ServiceName,
                RequestId = grpcRequestPackage.RequestId,
                ActionName = grpcRequestPackage.ActionName,
                Data = grpcRequestPackage.Data,
                RequestTime = reqTime
            };

            ServiceClientActionLogger.Log(ServiceClientName, requestPackage);

            #endregion 設定請求包裹

            #region 執行 gRPC 遠端呼叫

            ZayniFramework.Middle.Service.Grpc.Entity.ResponsePackage grpcResponsePackage = null;

            try
            {
                grpcResponsePackage = await _client.ExecuteAsync(grpcRequestPackage, deadline: DateTime.UtcNow.AddSeconds(_rpcTimeout));
            }
            catch (Exception ex)
            {
                // 目前發現新版本的 C# gRPC Client 套件，如果在 alpine 3.9 以上的 docker container 執行時，只要使用「非 IP address」進行對 gRPC service 連線，就會產生以下的 exception，暫時找不到解決方法。
                // Grpc.Core.RpcException: Status(StatusCode=Unavailable, Detail="DNS resolution failed")
                // https://github.com/grpc/grpc/issues/18680
                // 但如果使用 ubuntu 18.04 bionic 的 docker container 執行，就可以使用 donaim name 對 gRPC service 進行連線，gRPC DNS Resolve 就會正常，因此，如果要使用 gRPC Client 建議使用 ubuntu 18.04 bionic 的 docker image。
                result.ExceptionObject = ex;
                result.Code = StatusCode.CLIENT_ERROR;
                result.Message = $"Send RequestPackage to remote gRPC service host occur exception. ChannelState: {_channel.State}, RequestId: {requestPackage.RequestId}, ActionName: {requestPackage.ActionName}, RemoteHost: {_gRPCClientConfig?.ServerHost}:{_gRPCClientConfig?.ServerPort}.";
                Logger.Exception(this, ex, nameof(Execute), result.Message, "ServiceHostTrace");
                return result;
            }

            #endregion 執行 gRPC 遠端呼叫

            #region 取得 gRPC 遠端呼叫的回應包裹

            var responsePackage = new ZayniFramework.Middle.Service.Entity.ResponsePackage()
            {
                PackageType = grpcResponsePackage.PackageType,
                RequestId = grpcResponsePackage.RequestId,
                ActionName = grpcResponsePackage.ActionName,
                Success = Convert.ToBoolean(grpcResponsePackage.IsSuccess),
                Code = grpcResponsePackage.Code,
                Data = grpcResponsePackage.Data,
                Message = grpcResponsePackage.Message
            };

            #endregion 取得 gRPC 遠端呼叫的回應包裹

            #region 設定回傳值

            if (!responsePackage.Success)
            {
                result.Code = responsePackage.Code;
                result.Message = responsePackage.Message;
                return result;
            }

            result.Data = responsePackage;
            result.Message = responsePackage.Message;
            result.Success = responsePackage.Success;

            #endregion 設定回傳值

            return result;
        }

        #endregion Public Methods
    }
}
