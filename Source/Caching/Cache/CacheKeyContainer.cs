﻿using NeoSmart.AsyncLock;
using System;
using System.Collections.Generic;
using ZayniFramework.Logging;


namespace ZayniFramework.Caching
{
    /// <summary>資料快取 Key 值容器
    /// </summary>
    internal static class CacheKeyContainer
    {
        #region 宣告私有的欄位

        /// <summary>非同步作業鎖定物件
        /// </summary>
        private static readonly AsyncLock _asyncLock = new();

        /// <summary>所有資料快取的識別碼集合
        /// </summary>
        private static readonly HashSet<string> _cacheKeys = new();

        #endregion 宣告私有的欄位


        #region 宣告內部的屬性

        /// <summary>快取資料的數量 (CacheProperty + HashProperty 的資料總數)
        /// </summary>
        internal static int Count
        {
            get
            {
                using (_asyncLock.Lock())
                {
                    return _cacheKeys.Count;
                }
            }
        }

        #endregion 宣告內部的屬性


        #region 宣告內部的方法

        /// <summary>檢查是否存在指定的快取識別代碼
        /// </summary>
        /// <param name="cacheId">資料快取識別代碼</param>
        /// <returns>是否存在指定的快取識別代碼</returns>
        internal static bool Exists(string cacheId)
        {
            using (_asyncLock.Lock())
            {
                try
                {
                    return _cacheKeys.Contains(cacheId);
                }
                catch (Exception ex)
                {
                    Logger.Exception(nameof(CacheKeyContainer), ex, eventTitle: Logger.GetTraceLogTitle(nameof(CacheKeyContainer), nameof(Exists)), $"Check cacheId exists occur exception. CacheId: {cacheId}.");
                    return false;
                }
            }
        }

        /// <summary>新增指定的快取識別代碼
        /// </summary>
        /// <param name="cacheId">資料快取識別代碼</param>
        /// <returns>新增是否成功</returns>
        internal static bool Add(string cacheId)
        {
            using (_asyncLock.Lock())
            {
                try
                {
                    return _cacheKeys.Add(cacheId);
                }
                catch (Exception ex)
                {
                    Logger.Exception(nameof(CacheKeyContainer), ex, Logger.GetTraceLogTitle(nameof(CacheKeyContainer), nameof(Add)), $"Add cacheId to {nameof(CacheKeyContainer)} occur exception. CacheId: {cacheId}.");
                    return false;
                }
            }
        }

        /// <summary>移除指定的快取識別代碼
        /// </summary>
        /// <param name="cacheId">資料快取識別代碼</param>
        /// <returns>移除是否成功</returns>
        internal static bool Remove(string cacheId)
        {
            using (_asyncLock.Lock())
            {
                try
                {
                    return _cacheKeys.Remove(cacheId);
                }
                catch (Exception ex)
                {
                    Logger.Exception(nameof(CacheKeyContainer), ex, Logger.GetTraceLogTitle(nameof(CacheKeyContainer), nameof(Remove)), $"Remove cacheId to {nameof(CacheKeyContainer)} occur exception. CacheId: {cacheId}.");
                    return false;
                }
            }
        }

        /// <summary>清空所有的快取識別代碼
        /// </summary>
        /// <returns>是否成功清空所有快去識別碼</returns>
        internal static bool Clear()
        {
            using (_asyncLock.Lock())
            {
                try
                {
                    _cacheKeys.Clear();
                }
                catch (Exception ex)
                {
                    Logger.Exception(nameof(CacheKeyContainer), ex, Logger.GetTraceLogTitle(nameof(CacheKeyContainer), nameof(Clear)), $"Clear all cacheId from {nameof(CacheKeyContainer)} occur exception.");
                    return false;
                }

                return true;
            }
        }

        #endregion 宣告內部的方法
    }
}
