﻿using NeoSmart.AsyncLock;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using ZayniFramework.Common;
using ZayniFramework.Logging;
using ZayniFramework.Serialization;


namespace ZayniFramework.Caching
{
    /// <summary>MySQL Memory Engine TempTable 遠端資料快取
    /// </summary>
    public sealed class MySqlMemoryCache
    {
        #region 宣告私有的欄位

        /// <summary>非同步作業鎖定物件
        /// </summary>
        private static readonly AsyncLock _asyncLock = new();

        /// <summary>非同步作業鎖定物件: 鎖定 _message 欄位
        /// </summary>
        private static readonly AsyncLock _asyncLockMessage = new();

        /// <summary>MySQL 資料庫連線名稱
        /// </summary>
        private string _name;

        /// <summary>MySQL Memory 快取資料存取物件
        /// </summary>
        private MySqlMemoryStorageDao _dao;

        /// <summary>訊息
        /// </summary>
        private string _message;

        #endregion 宣告私有的欄位


        #region 宣告建構子

        /// <summary>多載建構子
        /// </summary>
        /// <param name="name">MySQL 資料庫連線名稱</param>
        public MySqlMemoryCache(string name)
        {
            _name = name;
            _dao = new MySqlMemoryStorageDao(name);
        }

        /// <summary>
        /// </summary>
        ~MySqlMemoryCache()
        {
            _name = null;
            _dao = null;
        }

        #endregion 宣告建構子


        #region 宣告公開的屬性

        /// <summary>訊息
        /// </summary>
        public string Message
        {
            get
            {
                using (_asyncLockMessage.Lock())
                {
                    return _message;
                }
            }
            set
            {
                using (_asyncLockMessage.Lock())
                {
                    _message = value;
                }
            }
        }

        /// <summary>快取池中目前存放的資料個數
        /// </summary>
        public int Count => (int)_dao.Count()?.Data;

        #endregion 宣告公開的屬性


        #region 宣告內部的方法

        /// <summary>取得訊息
        /// </summary>
        /// <returns>失敗/錯誤訊息</returns>
        internal string GetMessage() => Message;

        /// <summary>設定訊息
        /// </summary>
        /// <param name="value">失敗/錯誤訊息</param>
        internal void SetMessage(string value) => Message = value;

        /// <summary>取得快取池中目前存放的資料個數
        /// </summary>
        /// <returns>快取池中目前存放的資料個數</returns>
        internal int GetCount() => Count;

        #endregion 宣告內部的方法


        #region 宣告公開的方法

        /// <summary>取得快取倉儲中的 Key 值集合
        /// </summary>
        /// <param name="pattern">Key 值的規則</param>
        /// <returns>取得結果</returns>
        public Result<List<string>> GetCacheKeys(string pattern = null) => _dao.SelectKeys(pattern);

        /// <summary>取得指定 CacheId 識別碼的快取資料
        /// </summary>
        /// <param name="cacheId">快取資料識別碼</param>
        /// <param name="handler">重新從資料來源取得資料的委派</param>
        /// <param name="timeoutMinuntes">資料逾時時間 (單位為分鐘)</param>
        /// <returns>取得資料結果</returns>
        public GetCacheResult Get(string cacheId, GetSomethingFromDataSource handler = null, int? timeoutMinuntes = null)
        {
            var result = new GetCacheResult()
            {
                Success = false,
                CacheData = null
            };

            var r = _dao.Select(cacheId);

            if (!r.Success)
            {
                result.Message = r.Message;
                return result;
            }

            var cacheContent = r.Data;

            if (cacheContent.IsNullOrEmpty())
            {
                if (handler.IsNull())
                {
                    result.Message = $"No cacheId: '{cacheId}' in in-process memory cache pool.";
                    Logger.Info(this, $"{GetLogActionName(nameof(Get))}, {result.Message}", GetLogActionName(nameof(Get)));
                    return result;
                }

                var isSuccess = CachingHandlerExecutor.ExecuteGetSomethingCallback(handler, out var resultData, out var message);

                if (!isSuccess)
                {
                    result.Message = $"Can not found cacheId: '{cacheId}' from in-process memory cache pool. Try to execute {nameof(GetSomethingFromDataSource)} handler occur exception. {message}";
                    Logger.Info(this, $"{GetLogActionName(nameof(Get))}, {result.Message}", GetLogActionName(nameof(Get)));
                    return result;
                }

                var cacheProp = new CacheProperty()
                {
                    CacheId = cacheId,
                    Data = resultData,
                    ExpireInterval = timeoutMinuntes
                };

                // 如果重新向資料來源取得資料成功，就自動把資料存放到記憶體快取池中
                if (!Put(cacheProp, out var dataId, out var refreshTime))
                {
                    result.Message = $"Execute {nameof(GetSomethingFromDataSource)} handler success, but put data to in-process memory cache pool fail. {Message}";
                    Logger.Info(this, $"{GetLogActionName(nameof(Get))}, {result.Message}", GetLogActionName(nameof(Get)));
                }

                result.DataId = dataId;
                result.CacheData = resultData;
                result.RefreshTime = refreshTime;
                result.Success = true;
                return result;
            }

            CacheProperty cacheProperty = null;

            try
            {
                cacheProperty = NewtonsoftJsonConvert.DeserializeFromCamelCase<CacheProperty>(cacheContent);
            }
            catch (Exception ex)
            {
                result.Message = $"Deserialize to CacheProperty occur exception. CacheId: {cacheId}. {ex}";
                Logger.Error(this, $"{GetLogActionName(nameof(Get))}, {result.Message}", GetLogActionName(nameof(Get)));
                return result;
            }

            if (cacheProperty.IsNull())
            {
                result.Message = $"Deserialize to CacheProperty retrive null object. CacheId: {cacheId}.";
                Logger.Error(this, $"{GetLogActionName(nameof(Get))}, {result.Message}", GetLogActionName(nameof(Get)));
                return result;
            }

            result.DataId = cacheProperty.DataId;
            result.CacheData = cacheProperty.Data;
            result.RefreshTime = cacheProperty.RefreshTime;
            result.Success = true;

            return result;
        }

        /// <summary>取得指定 CacheId 識別碼的快取資料
        /// </summary>
        /// <typeparam name="TData">資料載體的泛型</typeparam>
        /// <param name="cacheId">快取資料識別碼</param>
        /// <param name="handler">重新從資料來源取得資料的委派</param>
        /// <param name="timeoutMinuntes">資料逾時時間 (單位為分鐘)</param>
        /// <returns>取得資料結果</returns>
        public GetCacheResult<TData> Get<TData>(string cacheId, GetSomethingFromDataSource handler = null, int? timeoutMinuntes = null)
        {
            #region 初始化回傳值

            var result = new GetCacheResult<TData>()
            {
                Success = false,
                Message = null
            };

            #endregion 初始化回傳值

            #region 取得 MySQL Server 中的目標資料

            var g = Get(cacheId, handler, timeoutMinuntes);

            if (!g.Success)
            {
                Message = g.Message;
                result.Message = g.Message;
                return result;
            }

            result.Message = g.Message;
            result.Success = g.Success;

            #endregion 取得 MySQL Server 中的目標資料

            #region 對目標資料進行轉型處理

            var data = default(TData);

            try
            {
                data = g.CacheData switch
                {
                    JToken json => json.ToObject<TData>(),
                    _ => (TData)g.CacheData,
                };
            }
            catch (JsonSerializationException jsonException)
            {
                try
                {
                    var jToken = g.CacheData as JToken;

                    if (jToken.IsNull())
                    {
                        result.Success = false;
                        result.CacheData = default;
                        result.Message = $"Casting Json.NET object data to {nameof(TData)} occur exception. CacheId: {cacheId}. {Environment.NewLine}{jsonException}";
                        Logger.Exception(this, jsonException, result.Message);
                        return result;
                    }

                    data = JsonConvert.DeserializeObject<TData>(jToken.ToString());
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.CacheData = default;
                    result.Message = $"Deserialize Json.NET object data to {nameof(TData)} occur exception. CacheId: {cacheId}. {Environment.NewLine}{ex}{Environment.NewLine}{g.CacheData}";
                    Logger.Exception(this, ex, result.Message);
                    return result;
                }

                result.Success = false;
                result.CacheData = default;
                result.Message = $"Casting Json.NET object data to {nameof(TData)} occur exception. CacheId: {cacheId}. {Environment.NewLine}{jsonException}";
                Logger.Exception(this, jsonException, result.Message);
                return result;
            }
            catch (InvalidCastException castException)
            {
                result.Success = false;
                result.CacheData = default;
                result.Message = $"Casting data object occur exception: {castException}";
                Logger.Exception(this, castException, result.Message);
                return result;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.CacheData = default;
                result.Message = $"Get cache data occur exception: {ex}";
                Logger.Exception(this, ex, result.Message);
                return result;
            }

            #endregion 對目標資料進行轉型處理

            #region 設定回傳值屬性

            result.CacheData = data;
            result.DataId = g.DataId;
            result.RefreshTime = g.RefreshTime;
            result.Success = true;

            #endregion 設定回傳值屬性

            return result;
        }

        /// <summary>將資料放入 MySQL Server 快取中
        /// </summary>
        /// <param name="cacheProperty">快取資料儲存屬性</param>
        /// <returns>是否成功將資料放入 MySQL Server 快取中</returns>
        public bool Put(CacheProperty cacheProperty)
        {
            return Put(cacheProperty, out _, out _);
        }

        /// <summary>將資料放入 MySQL Server 快取中
        /// </summary>
        /// <param name="cacheProperty">快取資料儲存屬性</param>
        /// <param name="dataId">資料異動識別 ID 代碼，每次更新後資料後改變。</param>
        /// <param name="refreshTime">資料最後更新時間 (資料新增或更新至快取倉儲時的時間戳記)</param>
        /// <returns>是否成功將資料放入 MySQL Server 快取中</returns>
        public bool Put(CacheProperty cacheProperty, out string dataId, out DateTime refreshTime)
        {
            #region 初始化輸出參數

            dataId = null;
            refreshTime = default;

            #endregion 初始化輸出參數

            #region 檢查快取資料儲存屬性值

            if (cacheProperty.IsNull())
            {
                Message = $"Augument '{nameof(cacheProperty)}' can not be null.";
                Logger.Error(this, $"{GetLogActionName(nameof(Put))}, {Message}", GetLogActionName(nameof(Put)));
                return false;
            }

            var cacheId = cacheProperty.CacheId;

            if (cacheId.IsNullOrEmpty())
            {
                Message = $"Augument '{nameof(cacheId)}' can not be null or empty string.";
                Logger.Error(this, $"{GetLogActionName(nameof(Put))}, {Message}", GetLogActionName(nameof(Put)));
                return false;
            }

            if (new int?[] { cacheProperty.ExpireInterval, cacheProperty.RefreshInterval }.All(j => j.IsNotNull() && j > 0))
            {
                Message = $"Augument '{nameof(cacheProperty.ExpireInterval)}' or '{nameof(cacheProperty.RefreshInterval)}' is invalid.";
                Logger.Error(this, $"{GetLogActionName(nameof(Put))}, {Message}", GetLogActionName(nameof(Put)));
                return false;
            }

            #endregion 檢查快取資料儲存屬性值

            #region 設定快取儲存的屬性

            var nowTime = DateTime.Now;
            cacheProperty.RefreshTime = nowTime;
            cacheProperty.DataId = RandomTextHelper.Create(36);

            var expireInterval = CacheDataOption.TimeoutInterval;

            if (cacheProperty.ExpireInterval.IsNotNull() && cacheProperty.ExpireInterval > 0)
            {
                expireInterval = (int)cacheProperty.ExpireInterval;
            }

            if (expireInterval > 0)
            {
                cacheProperty.ExpireInterval = expireInterval;
                cacheProperty.ExpireTime = nowTime.AddMinutes(expireInterval);
            }

            var refreshInterval = CachePoolOption.RefreshInterval;

            if (cacheProperty.RefreshInterval.IsNotNull() && cacheProperty.RefreshInterval > 0)
            {
                refreshInterval = (int)cacheProperty.RefreshInterval;
            }

            var needRefresh = refreshInterval > 0 && cacheProperty.RefreshCallback.IsNotNull();

            if (needRefresh)
            {
                cacheProperty.RefreshInterval = refreshInterval;
                cacheProperty.ExpireInterval = null;
                cacheProperty.ExpireTime = null;
            }

            #endregion 設定快取儲存的屬性

            #region 將目標資料放入 MySQL Memory 快取池中

            try
            {
                var json = NewtonsoftJsonConvert.SerializeInCamelCaseNoFormatting(cacheProperty);

                var model = new MySQLMemoryModel()
                {
                    CacheId = cacheProperty.CacheId,
                    CacheValue = json
                };

                var r = _dao.Insert(model);

                if (!r.Success)
                {
                    Message = $"Put data to MySQL memory cache fail. CacheId: {cacheId}. {r.Message}";
                    Logger.Error(this, $"{GetLogActionName(nameof(Put))}, {Message}", GetLogActionName(nameof(Put)));
                    return false;
                }
            }
            catch (Exception ex)
            {
                Message = $"Put data to MySQL memory cache occur exception. CacheId: {cacheId}. {Environment.NewLine}{ex}";
                Logger.Exception(this, ex, $"{GetLogActionName(nameof(Put))}");
                return false;
            }

            #endregion 將目標資料放入 MySQL Memory 快取池中

            #region 設定輸出參數

            dataId = cacheProperty.DataId;
            refreshTime = cacheProperty.RefreshTime;

            #endregion 設定輸出參數

            return true;
        }

        /// <summary>更新 MySQL Server 中的快取資料
        /// </summary>
        /// <param name="cacheProperty">快取資料儲存屬性</param>
        /// <returns>是否成功將資料更新至 MySQL Server 中</returns>
        public bool Update(CacheProperty cacheProperty)
        {
            return Update(cacheProperty, out _, out _);
        }

        /// <summary>更新 MySQL Server 中的快取資料
        /// </summary>
        /// <param name="cacheProperty">快取資料儲存屬性</param>
        /// <param name="dataId">資料異動識別 ID 代碼，每次更新後資料後改變。</param>
        /// <param name="refreshTime">資料最後更新時間 (資料新增或更新至快取倉儲時的時間戳記)</param>
        /// <returns>是否成功將資料更新至 MySQL Server 中</returns>
        public bool Update(CacheProperty cacheProperty, out string dataId, out DateTime refreshTime)
        {
            #region 初始化輸出參數

            dataId = null;
            refreshTime = default;

            #endregion 初始化輸出參數

            #region 檢查快取資料儲存屬性值

            if (cacheProperty.IsNull())
            {
                Message = $"Augument '{nameof(cacheProperty)}' can not be null.";
                Logger.Error(this, $"{GetLogActionName(nameof(Update))}, {Message}", GetLogActionName(nameof(Update)));
                return false;
            }

            var cacheId = cacheProperty.CacheId;

            if (cacheId.IsNullOrEmpty())
            {
                Message = $"Augument '{nameof(cacheId)}' can not be null or empty string.";
                Logger.Error(this, $"{GetLogActionName(nameof(Update))}, {Message}", GetLogActionName(nameof(Update)));
                return false;
            }

            if (new int?[] { cacheProperty.ExpireInterval, cacheProperty.RefreshInterval }.All(j => j.IsNotNull() && j > 0))
            {
                Message = $"Augument '{nameof(cacheProperty.ExpireInterval)}' or '{nameof(cacheProperty.RefreshInterval)}' is invalid.";
                Logger.Error(this, $"{GetLogActionName(nameof(Update))}, {Message}", GetLogActionName(nameof(Update)));
                return false;
            }

            #endregion 檢查快取資料儲存屬性值

            #region 設定快取儲存的屬性

            var nowTime = DateTime.Now;
            cacheProperty.RefreshTime = nowTime;
            cacheProperty.DataId = RandomTextHelper.Create(36);

            var expireInterval = CacheDataOption.TimeoutInterval;

            if (cacheProperty.ExpireInterval.IsNotNull() && cacheProperty.ExpireInterval > 0)
            {
                expireInterval = (int)cacheProperty.ExpireInterval;
            }

            if (expireInterval > 0)
            {
                cacheProperty.ExpireInterval = expireInterval;
                cacheProperty.ExpireTime = nowTime.AddMinutes(expireInterval);
            }

            var refreshInterval = CachePoolOption.RefreshInterval;

            if (cacheProperty.RefreshInterval.IsNotNull() && cacheProperty.RefreshInterval > 0)
            {
                refreshInterval = (int)cacheProperty.RefreshInterval;
            }

            var needRefresh = refreshInterval > 0 && cacheProperty.RefreshCallback.IsNotNull();

            if (needRefresh)
            {
                cacheProperty.RefreshInterval = refreshInterval;
                cacheProperty.ExpireInterval = null;
                cacheProperty.ExpireTime = null;
            }

            #endregion 設定快取儲存的屬性

            #region 將目標資料更新至 MySQL Memory 快取池中

            try
            {
                var json = NewtonsoftJsonConvert.SerializeInCamelCaseNoFormatting(cacheProperty);

                var model = new MySQLMemoryModel()
                {
                    CacheId = cacheProperty.CacheId,
                    CacheValue = json
                };

                var r = _dao.Update(model);

                if (!r.Success)
                {
                    Message = $"Update data to MySQL memory cache fail. CacheId: {cacheId}. {r.Message}";
                    Logger.Error(this, $"{GetLogActionName(nameof(Update))}, {Message}", GetLogActionName(nameof(Update)));
                    return false;
                }
            }
            catch (Exception ex)
            {
                Message = $"Update data to MySQL memory cache occur exception. CacheId: {cacheId}. {Environment.NewLine}{ex}";
                Logger.Exception(this, ex, $"{GetLogActionName(nameof(Update))}");
                return false;
            }

            #endregion 將目標資料更新至 MySQL Memory 快取池中

            #region 設定輸出參數

            dataId = cacheProperty.DataId;
            refreshTime = cacheProperty.RefreshTime;

            #endregion 設定輸出參數

            return true;
        }

        /// <summary>移除 MySQL Server 中的快取資料
        /// </summary>
        /// <param name="cacheId">快取資料識別碼</param>
        /// <returns>移除資料是否成功</returns>
        public bool Remove(string cacheId)
        {
            try
            {
                var g = _dao.Contains(cacheId);

                if (g.Success && !g.Data)
                {
                    return true;
                }

                var r = _dao.Delete(cacheId);

                if (!r.Success)
                {
                    Message = r.Message;
                    return false;
                }
            }
            catch (Exception ex)
            {
                Message = $"Remove data from MySQL memory cache occur exception. CacheId: {cacheId}. {Environment.NewLine}{ex}";
                Logger.Exception(this, ex, $"{GetLogActionName(nameof(Remove))}");
                return false;
            }

            return true;
        }

        /// <summary>檢查是否存在指定資料識別碼的快取資料
        /// </summary>
        /// <param name="cacheId">快取資料識別碼</param>
        /// <returns>資料是否存在</returns>
        public bool Contains(string cacheId)
        {
            var r = _dao.Contains(cacheId);

            if (!r.Success)
            {
                return false;
            }

            var exists = r.Data;
            return exists;
        }

        /// <summary>清空 MySQL Server 上所有的快取資料
        /// </summary>
        /// <returns>是否成功清除所有資料</returns>
        public bool Clear() => _dao.Clear().Success;

        /// <summary>取得 HashProperty 資料結構的快取資料
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <param name="subkey">HashProperty 的資料識別 Key 值</param>
        /// <returns>取得資料結果</returns>
        public GetHashCacheResult GetHashProperty(string cacheId, string subkey)
        {
            #region 初始化回傳值

            var result = new GetHashCacheResult()
            {
                Success = false,
                Data = null
            };

            #endregion 初始化回傳值

            #region 檢查傳入的參數

            if (cacheId.IsNullOrEmpty())
            {
                result.Message = $"Argument '{nameof(cacheId)}' can not be null or empty string.";
                Logger.Error(this, result.Message, Logger.GetTraceLogTitle(nameof(MySqlMemoryCache), nameof(GetHashProperty)));
                return result;
            }

            if (subkey.IsNullOrEmpty())
            {
                result.Message = $"Argument '{nameof(subkey)}' can not be null or empty string.";
                Logger.Error(this, result.Message, Logger.GetTraceLogTitle(nameof(MySqlMemoryCache), nameof(GetHashProperty)));
                return result;
            }

            #endregion 檢查傳入的參數

            #region 從 MySQL Memory 快取池中取得 Hash 資料

            var r = _dao.SelectHashValue(cacheId, subkey);

            if (!r.Success)
            {
                result.Message = $"Get hash data from MySQL memory cache fail. CacheId: {cacheId}, Subkey: {subkey}. {r.Message}";
                Logger.Error(this, $"{GetLogActionName(nameof(GetHashProperty))}, {result.Message}", GetLogActionName(nameof(GetHashProperty)));
                return result;
            }

            var value = r.Data;

            #endregion 從 MySQL Memory 快取池中取得 Hash 資料

            #region 設定回傳值

            result.Data = value;
            result.Success = true;

            #endregion 設定回傳值

            return result;
        }

        /// <summary>取得 HashProperty 資料集合的快取資料
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <returns>取得資料結果</returns>
        public GetHashPropertiesCacheResult GetHashProperties(string cacheId)
        {
            #region 初始化回傳值

            var result = new GetHashPropertiesCacheResult()
            {
                Success = false,
                Data = null
            };

            #endregion 初始化回傳值

            #region 檢查傳入的參數

            if (cacheId.IsNullOrEmpty())
            {
                result.Message = $"Argument '{nameof(cacheId)}' can not be null or empty string.";
                Logger.Error(this, result.Message, Logger.GetTraceLogTitle(nameof(MySqlMemoryCache), nameof(GetHashProperties)));
                return result;
            }

            #endregion 檢查傳入的參數

            #region 從 MySQL Memory 快取池取得 Hash 資料

            var r = _dao.SelectHash(cacheId);

            if (!r.Success)
            {
                result.Message = $"Get hash data from MySQL memory cache fail. CacheId: {cacheId}. {r.Message}";
                Logger.Error(this, $"{GetLogActionName(nameof(GetHashProperties))}, {result.Message}", GetLogActionName(nameof(GetHashProperties)));
                return result;
            }

            var models = r.Data;

            if (models.IsNullOrEmpty())
            {
                result.Message = $"No hash data found from MySQL memory cache. Retrive null or empty collection from MySQL memory cache. CacheId: {cacheId}.";
                Logger.Error(this, $"{GetLogActionName(nameof(GetHashProperties))}, {result.Message}", GetLogActionName(nameof(GetHashProperties)));
                return result;
            }

            #endregion 從 MySQL Memory 快取池取得 Hash 資料

            #region 轉換 HashProperty 資料集合

            HashProperty[] hashProperties;

            try
            {
                hashProperties = HashPropertyConverter.ConvertToHashProperty(models);
            }
            catch (Exception ex)
            {
                result.Message = $"Convert MySQLMemoryHashModel list to HashProperty array occur exception. CacheId: {cacheId}.";
                Logger.Exception(this, ex, GetLogActionName(nameof(GetHashProperties)), result.Message);
                return result;
            }

            if (hashProperties.IsNullOrEmpty())
            {
                result.Message = $"Get hash data from MySQL memory fail. Retrive null or empty array. CacheId: {cacheId}.";
                Logger.Error(this, result.Message, Logger.GetTraceLogTitle(nameof(MySqlMemoryCache), nameof(GetHashProperties)));
                return result;
            }

            #endregion 轉換 HashProperty 資料集合

            #region 設定回傳值

            result.Data = hashProperties;
            result.Success = true;

            #endregion 設定回傳值

            return result;
        }

        /// <summary>存放 HashProperty 資料<para/>
        /// 此功能有以下限制:<para/>
        /// 1. 暫時不支援快取資料 TTL Expiry 過其自動回收的功能，代表以 HashProperty 資料結構儲存在 in-process memory cache 的資料都是常駐型快取資料。<para/>
        /// 2. 暫時不支援快取資料定時自動重新更新的功能。
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <param name="hashProperties">Hash 快取資料結構集合</param>
        /// <returns>存放是否成功</returns>
        public bool PutHashProperty(string cacheId, HashProperty[] hashProperties)
        {
            using (_asyncLock.Lock())
            {
                #region 檢查傳入的參數

                if (cacheId.IsNullOrEmpty())
                {
                    Message = $"Argument '{nameof(cacheId)}' can not be null or empty string.";
                    Logger.Error(this, Message, Logger.GetTraceLogTitle(nameof(MySqlMemoryCache), nameof(PutHashProperty)));
                    return false;
                }

                if (hashProperties.IsNullOrEmpty())
                {
                    Message = $"Argument '{nameof(hashProperties)}' can not be null or empty array.";
                    Logger.Error(this, Message, Logger.GetTraceLogTitle(nameof(MySqlMemoryCache), nameof(PutHashProperty)));
                    return false;
                }

                #endregion 檢查傳入的參數

                #region 轉換 MySQLMemoryHashModel 資料集合

                MySQLMemoryHashModel[] hashModels;

                try
                {
                    hashModels = HashPropertyConverter.ConvertToHashModel(cacheId, hashProperties);
                }
                catch (Exception ex)
                {
                    Message = $"Convert HashProperty array to MySQLMemoryHashModel array occur exception. CacheId: {cacheId}.";
                    Logger.Exception(this, ex, Logger.GetTraceLogTitle(this, nameof(PutHashProperty)), Message);
                    return false;
                }

                if (hashModels.IsNullOrEmpty())
                {
                    Message = $"Convert HashProperty array to MySQLMemoryHashModel array occur error. Retrive null or empty array. CacheId: {cacheId}.";
                    Logger.Error(this, Message, Logger.GetTraceLogTitle(this, nameof(PutHashProperty)));
                    return false;
                }

                #endregion 轉換 MySQLMemoryHashModel 資料集合

                #region 將目標資料放入 MySQL Memory 快取池中

                var r = _dao.InsertOrUpdateHash(hashModels);

                if (!r.Success)
                {
                    Message = $"Put hash data to MySQL memory cache fail. CacheId: {cacheId}. {r.Message}";
                    Logger.Error(this, $"{GetLogActionName(nameof(PutHashProperty))}, {Message}", GetLogActionName(nameof(PutHashProperty)));
                    return false;
                }

                #endregion 將目標資料放入 MySQL Memory 快取池中

                return true;
            }
        }

        /// <summary>移除指定的 CacheId 中特定 Subkey 的 HashProperty 欄位快取資料，CacheId 不會被整個移除掉。
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <param name="subkey">HashProperty 的資料識別 Key 值</param>
        /// <returns>移除是否成功</returns>
        public bool RemoveHashProperty(string cacheId, string subkey)
        {
            using (_asyncLock.Lock())
            {
                #region 檢查傳入的參數

                if (cacheId.IsNullOrEmpty())
                {
                    Message = $"Argument '{nameof(cacheId)}' can not be null or empty string.";
                    Logger.Error(this, Message, Logger.GetTraceLogTitle(nameof(MySqlMemoryCache), nameof(RemoveHashProperty)));
                    return false;
                }

                if (subkey.IsNullOrEmpty())
                {
                    Message = $"Argument '{nameof(subkey)}' can not be null or empty string.";
                    Logger.Error(this, Message, Logger.GetTraceLogTitle(nameof(MySqlMemoryCache), nameof(RemoveHashProperty)));
                    return false;
                }

                #endregion 檢查傳入的參數

                #region 移除 MySQL Memory 快取池目標的 Hash 資料

                var d = _dao.DeleteHash(cacheId, subkey);

                if (!d.Success)
                {
                    Message = $"Remove hash data from MySQL memory cache fail. CacheId: {cacheId}, Subkey: {subkey}. {d.Message}";
                    Logger.Error(this, $"{GetLogActionName(nameof(RemoveHashProperty))}, {Message}", GetLogActionName(nameof(RemoveHashProperty)));
                    return false;
                }

                #endregion 移除 MySQL Memory 快取池目標的 Hash 資料

                return true;
            }
        }

        /// <summary>以 HashProperty 存放整個完整的 Entity 物件的資料。
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <param name="data">Entity 資料物件</param>
        /// <returns>存放資料結果</returns>
        public Result PutHashObject(string cacheId, object data)
        {
            using (_asyncLock.Lock())
            {
                var result = new Result()
                {
                    Success = false,
                    Message = null
                };

                HashProperty[] hashProperties;

                try
                {
                    hashProperties = HashPropertyConverter.ConvertTo(data);
                }
                catch (Exception ex)
                {
                    result.Message = $"Convert data entity to HashProperty array occur exception. CacheId: {cacheId}.";
                    Logger.Exception(this, ex, Logger.GetTraceLogTitle(this, nameof(PutHashObject)), result.Message);
                    return result;
                }

                if (!PutHashProperty(cacheId, hashProperties))
                {
                    result.Message = Message;
                    Logger.Error(this, $"Put hash property object into MySQL memory repository fail. CacheId: {cacheId}. {result.Message}", Logger.GetTraceLogTitle(this, nameof(PutHashObject)));
                    return result;
                }

                result.Success = true;
                return result;
            }
        }

        /// <summary>依照指定的 CacheId 取得完整的 HashProperty 資料結構的快取資料，並依照指定的泛型 Entity 回傳。
        /// </summary>
        /// <typeparam name="TData">Entity 資料物件的泛型</typeparam>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <returns>取得結果</returns>
        public Result<TData> GetHashObject<TData>(string cacheId)
        {
            var result = new Result<TData>()
            {
                Success = false,
                Data = default
            };

            var g = GetHashProperties(cacheId);

            if (!g.Success)
            {
                result.Code = g.Code;
                result.Message = g.Message;
                return result;
            }

            var hashProperties = g.Data;

            if (hashProperties.IsNullOrEmpty())
            {
                result.Message = $"No data found from MySQL memory cache. Key: {cacheId}.";
                Logger.Error(this, result.Message, Logger.GetTraceLogTitle(this, nameof(GetHashObject)));
                return result;
            }

            var data = default(TData);

            try
            {
                data = HashPropertyConverter.ConvertTo<TData>(hashProperties);
            }
            catch (Exception ex)
            {
                result.Message = $"Convert hash property array to entity object occur exception. Key: {cacheId}.";
                Logger.Exception(this, ex, Logger.GetTraceLogTitle(this, nameof(GetHashObject)), result.Message);
                return result;
            }

            result.Data = data;
            result.Success = true;
            return result;
        }

        #endregion 宣告公開的方法


        #region 宣告私有的方法

        /// <summary>取得方法名稱的日誌訊息
        /// </summary>
        /// <param name="methodName">目標方法名稱</param>
        /// <returns>方法名稱的日誌訊息</returns>
        private static string GetLogActionName(string methodName) => $"{nameof(MySqlMemoryCache)}.{methodName}";

        #endregion 宣告私有的方法
    }
}
