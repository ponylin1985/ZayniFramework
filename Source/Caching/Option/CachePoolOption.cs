﻿namespace ZayniFramework.Caching
{
    /// <summary>快取池設定值
    /// </summary>
    internal static class CachePoolOption
    {
        /// <summary>是否啟用回收機制
        /// </summary>
        internal static bool IsResetEnable { get; set; }

        /// <summary>回收時間間隔 (單位為秒，程式預設 60 秒)
        /// </summary>
        internal static int ResetInterval { get; set; } = 60;

        /// <summary>快取倉儲的最大容量數目 (程式預設 5000)
        /// </summary>
        internal static int MaxCapcaity { get; set; } = 5000;

        /// <summary>常注資料更新時間間隔 (單位為分鐘)
        /// </summary>
        internal static int RefreshInterval { get; set; }
    }
}
