﻿using System;
using System.Collections.Generic;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.Caching
{
    /// <summary>Caching 模組的 Config 設定值讀取器
    /// </summary>
    public static class ConfigReader
    {
        #region Private Fields

        /// <summary>The configuration of ZayniFramework.
        /// </summary>
        /// <returns></returns>
        private static readonly ZayniFrameworkSettings _zayniSettings = ConfigManager.GetZayniFrameworkSettings();

        #endregion Private Fields


        #region Public Methods

        /// <summary>讀取快取類型設定值
        /// </summary>
        /// <returns>取快取類型設定值</returns>
        public static string LoadCacheManagerMode()
        {
            try
            {
                return _zayniSettings.CachingSettings.Mode;
            }
            catch (Exception ex)
            {
                ConsoleLogger.LogError($"Load ZayniFramework:CachingSettings:Mode occur exception. {ex}");
                Logger.Exception(nameof(ConfigReader), ex, GetLogTitle(nameof(LoadCacheManagerMode)), $"Load ZayniFramework:CachingSettings:Mode occur exception.");
                throw;
            }
        }

        /// <summary>讀取記憶體快取機制的所有的相關Config設定值
        /// </summary>
        public static void LoadMemoryCacheSettings()
        {
            try
            {
                var cacheSettings = _zayniSettings.CachingSettings;

                if (null == cacheSettings)
                {
                    return;
                }

                CachePoolOption.IsResetEnable = cacheSettings.CachePoolSetting.ResetEnable;
                CachePoolOption.ResetInterval = cacheSettings.CachePoolSetting.CleanerIntervalSecond.IsDotNetDefault(60);
                CachePoolOption.MaxCapcaity = cacheSettings.CachePoolSetting.MaxCapacity.IsDotNetDefault(5000);
                CachePoolOption.RefreshInterval = cacheSettings.CachePoolSetting.RefreshIntervalMinute;
                CacheDataOption.TimeoutInterval = cacheSettings.CacheDataSetting.TimeoutInterval;
            }
            catch (Exception ex)
            {
                ConsoleLogger.LogError($"Load ZayniFramework/CachingSettings/CachePool occur exception. {ex}");
                Logger.Exception(nameof(ConfigReader), ex, GetLogTitle(nameof(LoadMemoryCacheSettings)), $"Load ZayniFramework/CachingSettings/CachePool occur exception.");
                throw;
            }
        }

        /// <summary>讀取 Redis 服務快取的 Config 設定值
        /// </summary>
        public static void LoadRedisCacheSettings()
        {
            try
            {
                var cacheSettings = _zayniSettings.CachingSettings;
                var defaultCacheConfig = GetDefaultRedisCacheConfig();

                if (null == defaultCacheConfig)
                {
                    return;
                }

                RedisCacheOption.DefaultHost = defaultCacheConfig.Host;
                RedisCacheOption.DefaultPort = defaultCacheConfig.Port;
                RedisCacheOption.DefaultPassword = defaultCacheConfig.Password;
                RedisCacheOption.DefaultDatabaseNo = defaultCacheConfig.DatabaseNo;

                CachePoolOption.MaxCapcaity = cacheSettings.CachePoolSetting.MaxCapacity.IsDotNetDefault(5000);
                CachePoolOption.RefreshInterval = cacheSettings.CachePoolSetting.RefreshIntervalMinute;
                CacheDataOption.TimeoutInterval = cacheSettings.CacheDataSetting.TimeoutInterval;
            }
            catch (Exception ex)
            {
                ConsoleLogger.LogError($"Load ZayniFramework/CachingSettings/RedisCache config section occur exception. {ex}");
                Logger.Exception(nameof(ConfigReader), ex, GetLogTitle(nameof(LoadRedisCacheSettings)), $"Load ZayniFramework/CachingSettings/RedisCache config section occur exception.");
                RedisCacheOption.DefaultPort = 6379;
                throw;
            }
        }

        /// <summary>取得預設的 Redis 快取連線設定
        /// </summary>
        /// <returns>預設的 Redis 快取連線設定</returns>
        public static RedisCacheSetting GetDefaultRedisCacheConfig() =>
            _zayniSettings.CachingSettings.RedisCacheSettings.Find(s => s.IsDefault);

        /// <summary>取得指定組態名稱的 Redis 連線設定
        /// </summary>
        /// <param name="name">Redis 快取的 Config 組態名稱</param>
        /// <returns>Redis 服務連線設定</returns>
        public static RedisCacheSetting GetCacheConfig(string name) =>
            _zayniSettings.CachingSettings.RedisCacheSettings.Find(s => s.Name == name);

        /// <summary>取得所有 Redis 快取連線的設定集合
        /// </summary>
        /// <returns>所有 Redis 快取連線的設定集合</returns>
        public static List<RedisCacheSetting> GetCacheConfigs() =>
            _zayniSettings.CachingSettings.RedisCacheSettings;

        /// <summary>取得預設的 MySQL Memory 快取資料庫連線名稱
        /// </summary>
        /// <returns>預設的 MySQL Memory 快取資料庫連線名稱</returns>
        public static string GetDefaultMySqlMemoryCache()
        {
            try
            {
                var cacheSettings = _zayniSettings.CachingSettings;

                if (null == cacheSettings)
                {
                    return null;
                }

                CachePoolOption.MaxCapcaity = cacheSettings.CachePoolSetting.MaxCapacity.IsDotNetDefault(5000);
                CachePoolOption.RefreshInterval = cacheSettings.CachePoolSetting.RefreshIntervalMinute;
                CacheDataOption.TimeoutInterval = cacheSettings.CacheDataSetting.TimeoutInterval;
                return cacheSettings.MySqlMemoryCacheSettings?.DefaultConnectionName.IsNullOrEmptyString(string.Empty);
            }
            catch (Exception ex)
            {
                ConsoleLogger.LogError($"Load MySQLMemoryCache/defaultConnectionName config occur exception. {ex}");
                Logger.Exception(nameof(ConfigReader), ex, GetLogTitle(nameof(GetDefaultMySqlMemoryCache)), "Load MySQLMemoryCache/defaultConnectionName config occur exception.");
                return null;
            }
        }

        #endregion Public Methods


        #region Private Methods

        /// <summary>取得日誌的標題
        /// </summary>
        /// <param name="methodName">方法名稱</param>
        /// <returns>日誌的標題</returns>
        private static string GetLogTitle(string methodName) => $"{nameof(ConfigReader)}.{methodName}";

        #endregion Private Methods
    }
}
