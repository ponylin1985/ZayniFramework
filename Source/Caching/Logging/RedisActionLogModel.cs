﻿using System;
using Newtonsoft.Json;
using ZayniFramework.Common.ORM;


namespace ZayniFramework.Caching
{
    /// <summary>Redis 服務的動作日誌紀錄的資料模型
    /// </summary>
    internal sealed class RedisActionLogModel
    {
        /// <summary>日誌流水號
        /// </summary>
        [TableColumn(ColumnName = "LOG_SRNO")]
        public long LogSrNo { get; set; }

        /// <summary>Redis Server 服務的位址
        /// </summary>
        [TableColumn(ColumnName = "REDIS_HOST")]
        public string RedisHost { get; set; }

        /// <summary>Redis Server 的連接阜號
        /// </summary>
        [TableColumn(ColumnName = "REDIS_PORT")]
        public int RedisPort { get; set; }

        /// <summary>原始請求識別代碼
        /// </summary>
        [TableColumn(ColumnName = "REQUEST_ID")]
        public string RequestId { get; set; }

        /// <summary>動作方向，1: 對 Redis 發送指令請求，2: Redis 執行指令的回應。
        /// </summary>
        [TableColumn(ColumnName = "DIRECTION")]
        public int Direction { get; set; }

        /// <summary>動作名稱
        /// </summary>
        [TableColumn(ColumnName = "ACTION")]
        public string Action { get; set; }

        /// <summary>資料內容
        /// </summary>
        [TableColumn(ColumnName = "DATA_CONTENT")]
        public string DataContent { get; set; }

        /// <summary>框架內部訊息
        /// </summary>
        [TableColumn(ColumnName = "MESSAGE")]
        public string Message { get; set; }

        /// <summary>日誌紀錄時間
        /// </summary>
        [TableColumn(ColumnName = "LOG_TIME")]
        public DateTime LogTime { get; set; }
    }

    /// <summary>Redis 服務的動作日誌紀錄的 Elasticsearch 文件資料模型
    /// </summary>
    internal sealed class caching_redis_action_log
    {
        /// <summary>日誌流水號
        /// </summary>
        [JsonProperty(PropertyName = "log_srno")]
        public string LogSrNo { get; set; }

        /// <summary>Redis Server 服務的位址
        /// </summary>
        [JsonProperty(PropertyName = "redis_host")]
        public string RedisHost { get; set; }

        /// <summary>Redis Server 的連接阜號
        /// </summary>
        [JsonProperty(PropertyName = "redis_port")]
        public int RedisPort { get; set; }

        /// <summary>原始請求識別代碼
        /// </summary>
        [JsonProperty(PropertyName = "request_id")]
        public string RequestId { get; set; }

        /// <summary>動作方向，1: 對 Redis 發送指令請求，2: Redis 執行指令的回應。
        /// </summary>
        [JsonProperty(PropertyName = "direction")]
        public int Direction { get; set; }

        /// <summary>動作名稱
        /// </summary>
        [JsonProperty(PropertyName = "action")]
        public string Action { get; set; }

        /// <summary>資料內容
        /// </summary>
        [JsonProperty(PropertyName = "data_content")]
        public string DataContent { get; set; }

        /// <summary>框架內部訊息
        /// </summary>
        [JsonProperty(PropertyName = "message")]
        public string Message { get; set; }

        /// <summary>日誌紀錄時間
        /// </summary>
        [JsonProperty(PropertyName = "log_time")]
        public DateTime LogTime { get; set; }
    }
}
