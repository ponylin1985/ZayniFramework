﻿using System;
using System.Threading.Tasks;
using ZayniFramework.DataAccess;
using ZayniFramework.Logging;
using ZayniFramework.Serialization;


namespace ZayniFramework.Caching
{
    /// <summary>Redis 服務動作日誌紀錄的資料存取類別
    /// </summary>
    internal sealed class RedisActionLogDao : BaseDataAccess
    {
        #region 宣告私有的欄位

        /// <summary>資料庫連線名稱
        /// </summary>
        private static readonly string _dbName = "Zayni";

        #endregion 宣告私有的欄位


        #region 宣告建構子

        /// <summary>預設建構子
        /// </summary>
        internal RedisActionLogDao() : base(_dbName)
        {
        }

        #endregion 宣告建構子


        #region 宣告內部的方法

        /// <summary>寫入動作日誌
        /// </summary>
        /// <param name="model">日誌紀錄資料模型</param>
        internal async Task InsertLogAsync(RedisActionLogModel model)
        {
            try
            {
                var sql = @"
                    -- Insert FS_CACHING_REDIS_ACTION_LOG
                    INSERT INTO `FS_CACHING_REDIS_ACTION_LOG` (
                          `REDIS_HOST`
                        , `REDIS_PORT`
                        , `REQUEST_ID`
                        , `DIRECTION`
                        , `ACTION`

                        , `DATA_CONTENT`
                        , `LOG_TIME`
                    ) VALUES (
                          @REDIS_HOST
                        , @REDIS_PORT
                        , @REQUEST_ID
                        , @DIRECTION
                        , @ACTION

                        , @DATA_CONTENT
                        , @LOG_TIME
                    ) ";

                using var conn = await base.CreateConnectionAsync();
                var cmd = GetSqlStringCommand(sql, conn);
                cmd.CommandTimeout = 10;

                base.AddInParameter(cmd, "@REDIS_HOST", DbColumnType.String, model.RedisHost);
                base.AddInParameter(cmd, "@REDIS_PORT", DbColumnType.Int32, model.RedisPort);
                base.AddInParameter(cmd, "@REQUEST_ID", DbColumnType.String, model.RequestId);
                base.AddInParameter(cmd, "@DIRECTION", DbColumnType.Int32, model.Direction);
                base.AddInParameter(cmd, "@ACTION", DbColumnType.String, model.Action);

                base.AddInParameter(cmd, "@DATA_CONTENT", DbColumnType.String, model.DataContent);
                base.AddInParameter(cmd, "@LOG_TIME", DbColumnType.DateTime, model.LogTime);

                if (!await base.ExecuteNonQueryAsync(cmd))
                {
                    await conn.CloseAsync();
                    return;
                }

                await conn.CloseAsync();
            }
            catch
            {
                await ConsoleLogger.LogAsync($"{nameof(RedisActionLogDao)} Origin Log:{Environment.NewLine}{NewtonsoftJsonConvert.Serialize(model)}");
            }
        }

        #endregion 宣告內部的方法
    }
}
