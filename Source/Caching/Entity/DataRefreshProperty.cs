﻿using NeoSmart.AsyncLock;
using System.Timers;


namespace ZayniFramework.Caching
{
    /// <summary>資料自動更新參數
    /// </summary>
    internal sealed class DataRefreshProperty
    {
        /// <summary>快取資料識別碼
        /// </summary>
        [System.Text.Json.Serialization.JsonIgnore()]
        [Newtonsoft.Json.JsonIgnore()]
        internal string CacheId { get; set; }

        /// <summary>自動資料更新定時器
        /// </summary>
        [System.Text.Json.Serialization.JsonIgnore()]
        [Newtonsoft.Json.JsonIgnore()]
        internal Timer RefreshTimer { get; set; }

        /// <summary>資料更新處理委派
        /// </summary>
        [System.Text.Json.Serialization.JsonIgnore()]
        [Newtonsoft.Json.JsonIgnore()]
        internal RefreshCacheDataHandler RefreshHandler { get; set; }

        /// <summary>內部的非同步作業鎖定物件
        /// </summary>
        /// <value></value>
        [System.Text.Json.Serialization.JsonIgnore()]
        [Newtonsoft.Json.JsonIgnore()]
        internal AsyncLock InternalAsyncLock { get; private set; } = new AsyncLock();
    }
}
