﻿namespace ZayniFramework.Caching
{
    /// <summary>Hash 快取資料結構
    /// </summary>
    public class HashProperty
    {
        /// <summary>預設建構子
        /// </summary>
        public HashProperty()
        {
            // pass
        }

        /// <summary>多仔建構子
        /// </summary>
        /// <param name="subkey">HashProperty 的資料識別 Key 值</param>
        /// <param name="value">HashProperty 子結構的資料值</param>
        public HashProperty(string subkey, object value)
        {
            Subkey = subkey;
            Value = value;
        }

        /// <summary>解構子
        /// </summary>
        ~HashProperty()
        {
            Subkey = null;
            Value = null;
        }

        /// <summary>HashProperty 的資料識別 Key 值
        /// </summary>
        public string Subkey { get; set; }

        /// <summary>HashProperty 子結構的資料值
        /// </summary>
        public object Value { get; set; }
    }
}
