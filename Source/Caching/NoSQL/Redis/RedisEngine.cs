﻿using System;
using System.Configuration;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Common.Tasks;
using ZayniFramework.Logging;
using ZayniFramework.Serialization;


namespace ZayniFramework.Caching
{
    /// <summary>Redis 快取服務的指令引擎
    /// </summary>
    internal sealed class RedisEngine
    {
        #region 宣告私有的欄位

        /// <summary>RedisCache 快取連線的組態名稱
        /// </summary>
        private string _redisCacheName;

        /// <summary>RedisCache 快取連線的組態設定
        /// </summary>
        private RedisCacheSetting _config;

        /// <summary>Redis 服務動作日誌的非同步工作佇列處理器
        /// </summary>
        /// <returns></returns>
        private static readonly AsyncWorker _worker = AsyncWorkerContainer.Resolve("ZayniFrameworkLoggerAsyncWorker");

        #endregion 宣告私有的欄位


        #region 宣告建構子

        /// <summary>靜態建構子
        /// </summary>
        /// <returns></returns>
        static RedisEngine() => _worker.Start();

        /// <summary>預設建構子
        /// </summary>
        /// <param name="redisCacheName"></param>
        internal RedisEngine(string redisCacheName)
        {
            _redisCacheName = redisCacheName;

            var config = ConfigReader.GetCacheConfig(_redisCacheName);

            if (config.IsNull())
            {
                throw new ConfigurationErrorsException($"{nameof(RedisEngine)} ctor error due to can not find '{redisCacheName}' config in ZayniFramework/CachingSettings/RedisCache section.");
            }

            _config = config;
        }

        /// <summary>解構子
        /// </summary>
        ~RedisEngine()
        {
            _redisCacheName = null;
            _config = null;
        }

        #endregion 宣告建構子


        #region 宣告內部的方法

        /// <summary>執行 Redis 快取服務的指令
        /// </summary>
        /// <typeparam name="TResponse">Redis服務回傳的資料泛型</typeparam>
        /// <param name="commandArgs">Redis服務的指令參數</param>
        /// <param name="command">執行 Redis 快取服務指令的委派</param>
        /// <returns>執行結果</returns>
        internal Result<TResponse> Execute<TResponse>(RedisCommandArgs commandArgs, ExecuteRedisCommandHandler<TResponse> command)
        {
            var result = Result.Create<TResponse>();

            var requestId = Guid.NewGuid().ToString();
            Log(_config.Host, _config.Port, requestId, 1, commandArgs.Action, commandArgs.Data);

            TResponse rst = default;

            try
            {
                rst = command(commandArgs);
            }
            catch (Exception ex)
            {
                result.Message = $"{nameof(RedisEngine)}, execute redis command occur exception.";
                Log(_config.Host, _config.Port, requestId, 2, commandArgs.Action, new { });
                Logger.Exception(this, ex, result.Message);
                return result;
            }

            result.Data = rst;
            result.Success = true;

            Log(_config.Host, _config.Port, requestId, 2, commandArgs.Action, rst);
            return result;
        }

        /// <summary>執行 Redis 快取服務的指令
        /// </summary>
        /// <typeparam name="TResponse">Redis服務回傳的資料泛型</typeparam>
        /// <param name="commandArgs">Redis服務的指令參數</param>
        /// <param name="command">執行 Redis 快取服務指令的委派</param>
        /// <returns>執行結果</returns>
        internal async Task<Result<TResponse>> ExecuteAsync<TResponse>(RedisCommandArgs commandArgs, ExecuteRedisCommandAsyncHandler<TResponse> command)
        {
            var result = Result.Create<TResponse>();

            var requestId = Guid.NewGuid().ToString();
            Log(_config.Host, _config.Port, requestId, 1, commandArgs.Action, commandArgs.Data);

            TResponse rst = default;

            try
            {
                rst = await command(commandArgs);
            }
            catch (Exception ex)
            {
                result.Message = $"{nameof(RedisEngine)}, execute redis command occur exception.";
                Log(_config.Host, _config.Port, requestId, 2, commandArgs.Action, new { });
                Logger.Exception(this, ex, result.Message);
                return result;
            }

            result.Data = rst;
            result.Success = true;

            Log(_config.Host, _config.Port, requestId, 2, commandArgs.Action, rst);
            return result;
        }

        /// <summary>執行 Redis 快取服務的指令
        /// </summary>
        /// <typeparam name="TResponse">Redis服務回傳的資料泛型</typeparam>
        /// <param name="commandArgs">Redis服務的指令參數</param>
        /// <param name="command">執行 Redis 快取服務指令的委派</param>
        /// <returns>執行結果</returns>
        internal Result<TResponse> Execute<TResponse>(RedisHashCommandArgs commandArgs, ExecuteRedisHashCommandHandler<TResponse> command)
        {
            var result = Result.Create<TResponse>();

            var requestId = Guid.NewGuid().ToString();
            object data = commandArgs.HashEntries;

            if (commandArgs.HashEntries.IsNullOrEmpty())
            {
                data = $"{commandArgs.CacheKey} {commandArgs.Subkey}";
            }

            Log(_config.Host, _config.Port, requestId, 1, commandArgs.Action, data);

            TResponse rst = default;

            try
            {
                rst = command(commandArgs);
            }
            catch (Exception ex)
            {
                result.Message = $"{nameof(RedisEngine)}, execute redis command occur exception.";
                Log(_config.Host, _config.Port, requestId, 2, commandArgs.Action, new { });
                Logger.Exception(this, ex, result.Message);
                return result;
            }

            result.Data = rst;
            result.Success = true;
            Log(_config.Host, _config.Port, requestId, 2, commandArgs.Action, rst);
            return result;
        }

        /// <summary>執行 Redis 快取服務的指令
        /// </summary>
        /// <typeparam name="TResponse">Redis服務回傳的資料泛型</typeparam>
        /// <param name="commandArgs">Redis服務的指令參數</param>
        /// <param name="command">執行 Redis 快取服務指令的委派</param>
        /// <returns>執行結果</returns>
        internal async Task<Result<TResponse>> ExecuteAsync<TResponse>(RedisHashCommandArgs commandArgs, ExecuteRedisHashCommandAsyncHandler<TResponse> command)
        {
            var result = Result.Create<TResponse>();

            var requestId = Guid.NewGuid().ToString();
            object data = commandArgs.HashEntries;

            if (commandArgs.HashEntries.IsNullOrEmpty())
            {
                data = $"{commandArgs.CacheKey} {commandArgs.Subkey}";
            }

            Log(_config.Host, _config.Port, requestId, 1, commandArgs.Action, data);

            TResponse rst = default;

            try
            {
                rst = await command(commandArgs);
            }
            catch (Exception ex)
            {
                result.Message = $"{nameof(RedisEngine)}, execute redis command occur exception.";
                Log(_config.Host, _config.Port, requestId, 2, commandArgs.Action, new { });
                Logger.Exception(this, ex, result.Message);
                return result;
            }

            result.Data = rst;
            result.Success = true;
            Log(_config.Host, _config.Port, requestId, 2, commandArgs.Action, rst);
            return result;
        }

        #endregion 宣告內部的方法


        #region 宣告私有的方法

        /// <summary>寫入動作日誌紀錄到資料庫
        /// </summary>
        /// <param name="redisHost">Redis服務位址</param>
        /// <param name="redisPort">Redis服務連線連接阜</param>
        /// <param name="requestId">原始請求代碼</param>
        /// <param name="direction">動作方向，1: 對 Redis 發送指令請求，2: Redis 執行指令的回應。</param>
        /// <param name="action">動作名稱</param>
        /// <param name="data">資料內容</param>
        private void Log(string redisHost, int redisPort, string requestId, int direction, string action, object data)
        {
            if (_config.IsNull())
            {
                return;
            }

            WriteESLog(redisHost, redisPort, requestId, direction, action, data);

            if (!_config.DbActionLog)
            {
                return;
            }

            try
            {
                RedisActionLogger.Log(_config.Host, _config.Port, requestId, direction, action, data);
            }
            catch (Exception ex)
            {
                Logger.Exception(this, ex, $"{nameof(RedisEngine)}, write redis action log occur exception.");
            }
        }

        /// <summary>寫入動作日誌紀錄到 ElasticSearch 服務
        /// </summary>
        /// <param name="redisHost">Redis服務位址</param>
        /// <param name="redisPort">Redis服務連線連接阜</param>
        /// <param name="requestId">原始請求代碼</param>
        /// <param name="direction">動作方向，1: 對 Redis 發送指令請求，2: Redis 執行指令的回應。</param>
        /// <param name="action">動作名稱</param>
        /// <param name="data">資料內容</param>
        private void WriteESLog(string redisHost, int redisPort, string requestId, int direction, string action, object data)
        {
            if (_config.ElasticStackLoggerName.IsNullOrEmpty())
            {
                return;
            }

            _worker.Enqueue(async () =>
            {
                string stringContent = null;

                switch (data)
                {
                    case string jsonContent:
                        stringContent = NewtonsoftJsonConvert.FormatToCamelCase(jsonContent);
                        break;

                    default:
                        if (data is null)
                        {
                            stringContent = null;
                            break;
                        }

                        stringContent = NewtonsoftJsonConvert.SerializeInCamelCase(data);
                        break;
                }

                try
                {
                    var model = new caching_redis_action_log()
                    {
                        LogSrNo = RandomTextHelper.CreateInt64String(20),
                        RedisHost = redisHost,
                        RedisPort = redisPort,
                        RequestId = requestId,
                        Direction = direction,
                        Action = action,
                        DataContent = stringContent,
                        LogTime = DateTime.UtcNow
                    };

                    await ElasticsearchLoggerContainer.Get(_config.ElasticStackLoggerName)?.InsertLogAsync<caching_redis_action_log>(model, model.LogSrNo);
                }
                catch (Exception ex)
                {
                    Logger.Exception(this, ex, $"{nameof(RedisEngine)}, write redis action log to ElasticSearch service occur exception.");
                }
            });
        }

        #endregion 宣告私有的方法
    }
}
