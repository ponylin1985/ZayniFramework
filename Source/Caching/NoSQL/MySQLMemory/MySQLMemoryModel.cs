﻿using Newtonsoft.Json;
using System;


namespace ZayniFramework.Caching
{
    /// <summary>遠端 MySQL 記憶體快取的資料模型
    /// </summary>
    internal sealed class MySQLMemoryModel
    {
        /// <summary>資料快取流水號
        /// </summary>
        [JsonProperty("SRNO")]
        public long SrNo { get; set; }

        /// <summary>資料結構類型: CacheProperty 或 HashProperty
        /// </summary>
        [JsonProperty("DATA_TYPE")]
        public string DataType { get; set; }

        /// <summary>資料快取 Key 值
        /// </summary>
        [JsonProperty("CACHE_ID")]
        public string CacheId { get; set; }

        /// <summary>資料快取內容 (字串最大長度為20000)
        /// </summary>
        [JsonProperty("CACHE_VALUE")]
        public string CacheValue { get; set; }

        /// <summary>快取資料異動時間
        /// </summary>
        [JsonProperty("DATA_FLAG")]
        public DateTime DataFlag { get; set; }
    }
}
