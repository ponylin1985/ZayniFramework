﻿using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.Caching
{
    /// <summary>快取處理委派專員
    /// </summary>
    internal static class HandlerBroker
    {
        #region 宣告私有的欄位

        /// <summary>預設的 MySQL Memory 遠端快取物件
        /// </summary>
        private static readonly MySqlMemoryCache _mysqlCache;

        #endregion 宣告私有的欄位


        #region 宣告靜態建構子

        /// <summary>靜態建構子
        /// </summary>
        static HandlerBroker()
        {
            var defaultMySqlCacheDbName = ConfigReader.GetDefaultMySqlMemoryCache();

            if (defaultMySqlCacheDbName.IsNotNullOrEmpty())
            {
                _mysqlCache = new MySqlMemoryCache(defaultMySqlCacheDbName);
            }
        }

        #endregion 宣告靜態建構子


        #region 宣告內部的方法

        /// <summary>對快取管理員設定正確的處理委派
        /// </summary>
        /// <param name="cacheType">快取類型代碼</param>
        internal static void SetCacheHandlers(string cacheType)
        {
            switch (cacheType.ToLower())
            {
                case "memory":
                    SetMemoryCacheHandlers();
                    break;

                case "redis":
                    SetRedisCacheHandlers();
                    break;

                case "mysqlmemory":
                    SetMySQLMemoryCacheHandlers();
                    break;

                default:
                    SetMemoryCacheHandlers();
                    break;
            }
        }

        /// <summary>建立取得快取資料的委派
        /// </summary>
        /// <param name="cacheType">快取種類</param>
        /// <returns>取得快取資料的委派</returns>
        internal static GetCacheDataHandler GetCacheHandler(string cacheType)
        {
            switch (cacheType.ToLower())
            {
                case "memory":
                    return MemoryCache.Get;

                case "redis":
                    return RedisCacheContainer.GetDefault().Data.Get;

                case "mysqlmemory":
                    return _mysqlCache.Get;

                default:
                    return MemoryCache.Get;
            }
        }

        /// <summary>建立取得快取資料的委派
        /// </summary>
        /// <param name="cacheType">快取種類</param>
        /// <returns>取得快取資料的委派</returns>
        internal static GetCacheDataAsyncHandler GetCacheAsyncHandler(string cacheType)
        {
            switch (cacheType.ToLower())
            {
                case "memory":
                    return MemoryCache.GetAsync;

                case "redis":
                    return RedisCacheContainer.GetDefault().Data.GetAsync;

                // case "mysqlmemory":
                //     return _mysqlCache.Get;

                default:
                    return MemoryCache.GetAsync;
            }
        }

        /// <summary>建立取得快取資料的委派
        /// </summary>
        /// <typeparam name="TModel">資料模型泛型</typeparam>
        /// <param name="cacheType">快取種類</param>
        /// <returns>取得快取資料的委派</returns>
        internal static GetCacheDataHandler<TModel> GetCacheHandler<TModel>(string cacheType)
        {
            switch (cacheType.ToLower())
            {
                case "memory":
                    return MemoryCache.Get<TModel>;

                case "redis":
                    return RedisCacheContainer.GetDefault().Data.Get<TModel>;

                case "mysqlmemory":
                    return _mysqlCache.Get<TModel>;

                default:
                    return MemoryCache.Get<TModel>;
            }
        }

        /// <summary>建立取得快取資料的委派
        /// </summary>
        /// <typeparam name="TModel">資料模型泛型</typeparam>
        /// <param name="cacheType">快取種類</param>
        /// <returns>取得快取資料的委派</returns>
        internal static GetCacheDataAsyncHandler<TModel> GetCacheAsyncHandler<TModel>(string cacheType)
        {
            switch (cacheType.ToLower())
            {
                case "memory":
                    return MemoryCache.GetAsync<TModel>;

                case "redis":
                    return RedisCacheContainer.GetDefault().Data.GetAsync<TModel>;

                // case "mysqlmemory":
                //     return _mysqlCache.Get<TModel>;

                default:
                    return MemoryCache.GetAsync<TModel>;
            }
        }

        /// <summary>取得 GetHashObjectHandler 的委派
        /// </summary>
        /// <param name="cacheType">快取種類</param>
        /// <returns>取得快取資料的委派</returns>
        internal static GetHashObjectHandler<TData> GetHashObjectHandler<TData>(string cacheType)
        {
            switch (cacheType.ToLower())
            {
                case "memory":
                    return MemoryCache.GetHashObject<TData>;

                case "redis":
                    return RedisCacheContainer.GetDefault().Data.GetHashObject<TData>;

                case "mysqlmemory":
                    return _mysqlCache.GetHashObject<TData>;

                default:
                    return MemoryCache.GetHashObject<TData>;
            }
        }

        /// <summary>取得 GetHashObjectHandler 的委派
        /// </summary>
        /// <param name="cacheType">快取種類</param>
        /// <returns>取得快取資料的委派</returns>
        internal static GetHashObjectAsyncHandler<TData> GetHashObjectAsyncHandler<TData>(string cacheType)
        {
            switch (cacheType.ToLower())
            {
                case "memory":
                    return MemoryCache.GetHashObjectAsync<TData>;

                case "redis":
                    return RedisCacheContainer.GetDefault().Data.GetHashObjectAsync<TData>;

                // case "mysqlmemory":
                //     return _mysqlCache.GetHashObject<TData>;

                default:
                    return MemoryCache.GetHashObjectAsync<TData>;
            }
        }

        #endregion 宣告內部的方法


        #region 宣告私有的方法

        /// <summary>設定記憶體快取處理委派
        /// </summary>
        private static void SetMemoryCacheHandlers()
        {
            CacheManager.GetCacheKeysCallback = MemoryCache.GetCacheKeys;
            CacheManager.PutCallback = MemoryCache.Put;
            CacheManager.PutAsyncCallback = MemoryCache.PutAsync;
            CacheManager.UpdateCallback = MemoryCache.Update;
            CacheManager.UpdateAsyncCallback = MemoryCache.UpdateAsync;
            CacheManager.RemoveCallback = MemoryCache.Remove;
            CacheManager.RemoveAsyncCallback = MemoryCache.RemoveAsync;
            CacheManager.ClearCallback = MemoryCache.Clear;
            CacheManager.ClearAsyncCallback = MemoryCache.ClearAsync;
            CacheManager.ContainsCallback = MemoryCache.Contains;
            CacheManager.ContainsAsyncCallback = MemoryCache.ContainsAsync;
            CacheManager.GetMessageCallback = MemoryCache.GetMessage;
            CacheManager.SetMessageCallback = MemoryCache.SetMessage;
            CacheManager.GetCountCallback = MemoryCache.GetCount;

            CacheManager.ContainsHashCallback = MemoryCache.ContainsHash;
            CacheManager.ContainsHashAsyncCallback = MemoryCache.ContainsHashAsync;
            CacheManager.GetHashPropertyCallback = MemoryCache.GetHashProperty;
            CacheManager.GetHashPropertyAsyncCallback = MemoryCache.GetHashPropertyAsync;
            CacheManager.GetHashPropertiesCallback = MemoryCache.GetHashProperties;
            CacheManager.GetHashPropertiesAsyncCallback = MemoryCache.GetHashPropertiesAsync;
            CacheManager.PutHashPropertyCallback = MemoryCache.PutHashProperty;
            CacheManager.PutHashPropertyAsyncCallback = MemoryCache.PutHashPropertyAsync;
            CacheManager.RemoveHashPropertyCallback = MemoryCache.RemoveHashProperty;
            CacheManager.RemoveHashPropertyAsyncCallback = MemoryCache.RemoveHashPropertyAsync;
            CacheManager.RemoveHashPropertyFieldCallback = MemoryCache.RemoveHashProperty;
            CacheManager.RemoveHashPropertyFieldAsyncCallback = MemoryCache.RemoveHashPropertyAsync;
            CacheManager.PutHashObjectCallback = MemoryCache.PutHashObject;
            CacheManager.PutHashObjectAsyncCallback = MemoryCache.PutHashObjectAsync;
        }

        /// <summary>設定 Redis 服務快取的處理委派
        /// </summary>
        private static void SetRedisCacheHandlers()
        {
            var r = RedisCacheContainer.GetDefault();

            if (!r.Success)
            {
                Logger.Error(nameof(HandlerBroker), "Get default redis cache instance fail. Please check the config of RedisCache.", nameof(SetRedisCacheHandlers));
                return;
            }

            var redisCache = r.Data;

            CacheManager.GetCacheKeysCallback = redisCache.GetCacheKeys;
            CacheManager.PutCallback = redisCache.Put;
            CacheManager.PutAsyncCallback = redisCache.PutAsync;
            CacheManager.UpdateCallback = redisCache.Update;
            CacheManager.UpdateAsyncCallback = redisCache.UpdateAsync;
            CacheManager.RemoveCallback = redisCache.Remove;
            CacheManager.RemoveAsyncCallback = redisCache.RemoveAsync;
            CacheManager.ClearCallback = redisCache.Clear;
            CacheManager.ClearAsyncCallback = redisCache.ClearAsync;
            CacheManager.ContainsCallback = redisCache.Contains;
            CacheManager.ContainsAsyncCallback = redisCache.ContainsAsync;
            CacheManager.GetMessageCallback = redisCache.GetMessage;
            CacheManager.SetMessageCallback = redisCache.SetMessage;
            CacheManager.GetCountCallback = redisCache.GetCount;

            // CacheManager.ContainsHashCallback                 = redisCache.ContainsHash;
            // CacheManager.ContainsHashAsyncCallback            = redisCache.ContainsHashAsync;
            CacheManager.GetHashPropertyCallback = redisCache.GetHashProperty;
            CacheManager.GetHashPropertyAsyncCallback = redisCache.GetHashPropertyAsync;
            CacheManager.GetHashPropertiesCallback = redisCache.GetHashProperties;
            CacheManager.GetHashPropertiesAsyncCallback = redisCache.GetHashPropertiesAsync;
            CacheManager.PutHashPropertyCallback = redisCache.PutHashProperty;
            CacheManager.PutHashPropertyAsyncCallback = redisCache.PutHashPropertyAsync;
            // CacheManager.RemoveHashPropertyCallback           = redisCache.RemoveHashProperty;
            // CacheManager.RemoveHashPropertyAsyncCallback      = redisCache.RemoveHashPropertyAsync;
            CacheManager.RemoveHashPropertyFieldCallback = redisCache.RemoveHashProperty;
            CacheManager.RemoveHashPropertyFieldAsyncCallback = redisCache.RemoveHashPropertyAsync;
            CacheManager.PutHashObjectCallback = redisCache.PutHashObject;
            CacheManager.PutHashObjectAsyncCallback = redisCache.PutHashObjectAsync;
        }

        /// <summary>設定 MySQL Memory 服務快取的處理委派
        /// </summary>
        private static void SetMySQLMemoryCacheHandlers()
        {
            CacheManager.GetCacheKeysCallback = _mysqlCache.GetCacheKeys;
            CacheManager.PutCallback = _mysqlCache.Put;
            CacheManager.UpdateCallback = _mysqlCache.Update;
            CacheManager.RemoveCallback = _mysqlCache.Remove;
            CacheManager.ClearCallback = _mysqlCache.Clear;
            CacheManager.ContainsCallback = _mysqlCache.Contains;
            CacheManager.GetMessageCallback = _mysqlCache.GetMessage;
            CacheManager.SetMessageCallback = _mysqlCache.SetMessage;
            CacheManager.GetCountCallback = _mysqlCache.GetCount;

            //CacheManager.ContainsHashCallback            = _mysqlCache.ContainsHash;
            CacheManager.GetHashPropertyCallback = _mysqlCache.GetHashProperty;
            CacheManager.GetHashPropertiesCallback = _mysqlCache.GetHashProperties;
            CacheManager.PutHashPropertyCallback = _mysqlCache.PutHashProperty;
            //CacheManager.RemoveHashPropertyCallback      = _mysqlCache.RemoveHashProperty;
            CacheManager.RemoveHashPropertyFieldCallback = _mysqlCache.RemoveHashProperty;
            CacheManager.PutHashObjectCallback = _mysqlCache.PutHashObject;
        }

        #endregion 宣告私有的方法
    }
}
