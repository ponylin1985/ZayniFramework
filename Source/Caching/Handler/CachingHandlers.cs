﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ZayniFramework.Common;


namespace ZayniFramework.Caching
{
    #region Public Delegates

    /// <summary>更新快取池中常駐資料的處理委派
    /// </summary>
    public delegate object RefreshCacheDataHandler();

    /// <summary>重新從資料來源取得資料的委派
    /// </summary>
    /// <returns>從資料來源取得的資料</returns>
    public delegate object GetSomethingFromDataSource();

    /// <summary>重新從資料來源取得資料的委派
    /// </summary>
    /// <returns>從資料來源取得的資料</returns>
    public delegate Task<object> GetSomethingFromDataSourceAsync();

    /// <summary>遠端快取倉儲的連線中斷的處理委派
    /// </summary>
    public delegate void CacheRepositoryConnectionBrokenHandler();

    /// <summary>遠端快取倉儲的連線重新恢復的處理委派
    /// </summary>
    public delegate void CacheRepositoryConnectionRestoreHandler();

    #endregion Public Delegates


    #region Internal Delegates

    /// <summary>取得快取倉儲中的 Key 值集合的委派
    /// </summary>
    /// <param name="pattern">Key 值的規則</param>
    /// <returns>取得結果</returns>
    internal delegate Result<List<string>> GetCacheKeysHandler(string pattern = null);

    /// <summary>取得快取資料的委派
    /// </summary>
    /// <param name="cacheId">快取ID值</param>
    /// <param name="handler">重新從資料來源取得資料的回呼委派</param>
    /// <param name="timeoutMinuntes">資料逾時時間間隔 或 更新資料時間間隔(單位為分鐘)</param>
    /// <returns>從快取池中取得資料的結果</returns>
    internal delegate GetCacheResult GetCacheDataHandler(string cacheId, GetSomethingFromDataSource handler = null, int? timeoutMinuntes = null);

    /// <summary>取得快取資料的委派
    /// </summary>
    /// <param name="cacheId">快取ID值</param>
    /// <param name="handler">重新從資料來源取得資料的回呼委派</param>
    /// <param name="timeoutMinuntes">資料逾時時間間隔 或 更新資料時間間隔(單位為分鐘)</param>
    /// <returns>從快取池中取得資料的結果</returns>
    internal delegate Task<GetCacheResult> GetCacheDataAsyncHandler(string cacheId, GetSomethingFromDataSourceAsync handler = null, int? timeoutMinuntes = null);

    /// <summary>取得快取資料的委派
    /// </summary>
    /// <typeparam name="TModel">資料模型泛型</typeparam>
    /// <param name="cacheId">快取ID值</param>
    /// <param name="handler">重新從資料來源取得資料的回呼委派</param>
    /// <param name="timeoutMinuntes">資料逾時時間間隔 或 更新資料時間間隔(單位為分鐘)</param>
    /// <returns>從快取池中取得資料的結果</returns>
    internal delegate GetCacheResult<TModel> GetCacheDataHandler<TModel>(string cacheId, GetSomethingFromDataSource handler = null, int? timeoutMinuntes = null);

    /// <summary>取得快取資料的委派
    /// </summary>
    /// <typeparam name="TModel">資料模型泛型</typeparam>
    /// <param name="cacheId">快取ID值</param>
    /// <param name="handler">重新從資料來源取得資料的回呼委派</param>
    /// <param name="timeoutMinuntes">資料逾時時間間隔 或 更新資料時間間隔(單位為分鐘)</param>
    /// <returns>從快取池中取得資料的結果</returns>
    internal delegate Task<GetCacheResult<TModel>> GetCacheDataAsyncHandler<TModel>(string cacheId, GetSomethingFromDataSourceAsync handler = null, int? timeoutMinuntes = null);

    /// <summary>存放資料到快取的委派
    /// </summary>
    /// <param name="cacheProperty">快取資料儲存屬性</param>
    /// <returns>是否成功將資料放入快取池</returns>
    internal delegate bool PutCacheDataHandler(CacheProperty cacheProperty);

    /// <summary>存放資料到快取的委派
    /// </summary>
    /// <param name="cacheProperty">快取資料儲存屬性</param>
    /// <returns>是否成功將資料放入快取池</returns>
    internal delegate Task<bool> PutCacheDataAsyncHandler(CacheProperty cacheProperty);

    /// <summary>更新快取資料的委派
    /// </summary>
    /// <param name="cacheProperty">快取資料儲存屬性</param>
    /// <returns>是否更新成功</returns>
    internal delegate bool UpdateCacheDataHandler(CacheProperty cacheProperty);

    /// <summary>更新快取資料的委派
    /// </summary>
    /// <param name="cacheProperty">快取資料儲存屬性</param>
    /// <returns>是否更新成功</returns>
    internal delegate Task<bool> UpdateCacheDataAsyncHandler(CacheProperty cacheProperty);

    /// <summary>移除快取資料的委派
    /// </summary>
    /// <param name="cacheId">快取ID值</param>
    /// <returns>是否成功移除資料</returns>
    internal delegate bool RemoveCacheDataHandler(string cacheId);

    /// <summary>移除快取資料的委派
    /// </summary>
    /// <param name="cacheId">快取ID值</param>
    /// <returns>是否成功移除資料</returns>
    internal delegate Task<bool> RemoveCacheDataAsyncHandler(string cacheId);

    /// <summary>清空快取所有快取資料的委派
    /// </summary>
    /// <returns>是否成功清空</returns>
    internal delegate bool ClearCacheDataHandler();

    /// <summary>清空快取所有快取資料的委派
    /// </summary>
    /// <returns>是否成功清空</returns>
    internal delegate Task<bool> ClearCacheDataAsyncHandler();

    /// <summary>檢查快取池中是否包含指定的快取ID的委派
    /// </summary>
    /// <param name="cacheId">快取ID值</param>
    /// <returns>快取池中是否包含指定的快取ID的資料</returns>
    internal delegate bool ContainsCacheDataHandler(string cacheId);

    /// <summary>檢查快取池中是否包含指定的快取ID的委派
    /// </summary>
    /// <param name="cacheId">快取ID值</param>
    /// <returns>快取池中是否包含指定的快取ID的資料</returns>
    internal delegate Task<bool> ContainsCacheDataAsyncHandler(string cacheId);

    /// <summary>檢查 Hash 資料快取池中是否包含指定識別碼的資料的委派
    /// </summary>
    /// <param name="cacheId">資料快取識別碼</param>
    /// <returns>是否包含指定識別碼的資料</returns>
    internal delegate bool ContainsHashHandler(string cacheId);

    /// <summary>檢查 Hash 資料快取池中是否包含指定識別碼的資料的委派
    /// </summary>
    /// <param name="cacheId">資料快取識別碼</param>
    /// <returns>是否包含指定識別碼的資料</returns>
    internal delegate Task<bool> ContainsHashAsyncHandler(string cacheId);

    /// <summary>取得 Hash 資料結構的 Hash Field 資料值的委派
    /// </summary>
    /// <param name="cacheId">資料快取識別碼</param>
    /// <param name="subkey">Hash 子結構的資料值</param>
    /// <returns>取得資料結果</returns>
    internal delegate GetHashCacheResult GetHashPropertyHandler(string cacheId, string subkey);

    /// <summary>取得 Hash 資料結構的 Hash Field 資料值的委派
    /// </summary>
    /// <param name="cacheId">資料快取識別碼</param>
    /// <param name="subkey">Hash 子結構的資料值</param>
    /// <returns>取得資料結果</returns>
    internal delegate Task<GetHashCacheResult> GetHashPropertyAsyncHandler(string cacheId, string subkey);

    /// <summary>取得 HashProperty 資料集合的快取資料的委派
    /// </summary>
    /// <param name="cacheId">資料快取識別碼</param>
    /// <returns>取得資料結果</returns>
    internal delegate GetHashPropertiesCacheResult GetHashPropertiesHandler(string cacheId);

    /// <summary>取得 HashProperty 資料集合的快取資料的委派
    /// </summary>
    /// <param name="cacheId">資料快取識別碼</param>
    /// <returns>取得資料結果</returns>
    internal delegate Task<GetHashPropertiesCacheResult> GetHashPropertiesAsyncHandler(string cacheId);

    /// <summary>以 Hash 資料結構存放至快取池中的委派。<para/>
    /// 此功能有以下限制:<para/>
    /// 1. 暫時不支援快取資料 TTL Expiry 過其自動回收的功能，代表以 HashProperty 資料結構儲存在 in-process memory cache 的資料都是常駐型快取資料。<para/>
    /// 2. 暫時不支援快取資料定時自動重新更新的功能。
    /// </summary>
    /// <param name="cacheId">資料快取識別碼</param>
    /// <param name="hashProperties">Hash 快取資料結構集合</param>
    /// <returns>存放是否成功</returns>
    internal delegate bool PutHashPropertyHandler(string cacheId, HashProperty[] hashProperties);

    /// <summary>以 Hash 資料結構存放至快取池中的委派。<para/>
    /// 此功能有以下限制:<para/>
    /// 1. 暫時不支援快取資料 TTL Expiry 過其自動回收的功能，代表以 HashProperty 資料結構儲存在 in-process memory cache 的資料都是常駐型快取資料。<para/>
    /// 2. 暫時不支援快取資料定時自動重新更新的功能。
    /// </summary>
    /// <param name="cacheId">資料快取識別碼</param>
    /// <param name="hashProperties">Hash 快取資料結構集合</param>
    /// <returns>存放是否成功</returns>
    internal delegate Task<bool> PutHashPropertyAsyncHandler(string cacheId, HashProperty[] hashProperties);

    /// <summary>將指定的 Hash 快取資料從快取池中移除的委派。
    /// </summary>
    /// <param name="cacheId">資料快取識別碼</param>
    /// <returns>移除是否成功</returns>
    internal delegate bool RemoveHashPropertyHandler(string cacheId);

    /// <summary>將指定的 Hash 快取資料從快取池中移除的委派。
    /// </summary>
    /// <param name="cacheId">資料快取識別碼</param>
    /// <returns>移除是否成功</returns>
    internal delegate Task<bool> RemoveHashPropertyAsyncHandler(string cacheId);

    /// <summary>移除指定的 CacheId 中特定 Subkey 的 HashProperty 欄位快取資料的委派，CacheId 不會被整個移除掉。
    /// </summary>
    /// <param name="cacheId">資料快取識別碼</param>
    /// <param name="subkey">Hash 子結構的資料值</param>
    /// <returns>移除是否成功</returns>
    internal delegate bool RemoveHashPropertyFieldHandler(string cacheId, string subkey);

    /// <summary>移除指定的 CacheId 中特定 Subkey 的 HashProperty 欄位快取資料的委派，CacheId 不會被整個移除掉。
    /// </summary>
    /// <param name="cacheId">資料快取識別碼</param>
    /// <param name="subkey">Hash 子結構的資料值</param>
    /// <returns>移除是否成功</returns>
    internal delegate Task<bool> RemoveHashPropertyFieldAsyncHandler(string cacheId, string subkey);

    /// <summary>以 HashProperty 存放整個完整的 Entity 物件資料的委派
    /// </summary>
    /// <param name="cacheId">資料快取識別碼</param>
    /// <param name="data">Entity 資料物件</param>
    /// <returns>存放資料結果</returns>
    internal delegate Result PutHashObjectHandler(string cacheId, object data);

    /// <summary>以 HashProperty 存放整個完整的 Entity 物件資料的委派
    /// </summary>
    /// <param name="cacheId">資料快取識別碼</param>
    /// <param name="data">Entity 資料物件</param>
    /// <returns>存放資料結果</returns>
    internal delegate Task<Result> PutHashObjectAsyncHandler(string cacheId, object data);

    /// <summary>依照指定的 CacheId 取得完整的 HashProperty 資料結構的快取資料，並依照指定的泛型 Entity 回傳。
    /// </summary>
    /// <typeparam name="TData">Entity 資料物件的泛型</typeparam>
    /// <param name="cacheId">資料快取識別碼</param>
    /// <returns>取得結果</returns>
    internal delegate Result<TData> GetHashObjectHandler<TData>(string cacheId);

    /// <summary>依照指定的 CacheId 取得完整的 HashProperty 資料結構的快取資料，並依照指定的泛型 Entity 回傳。
    /// </summary>
    /// <typeparam name="TData">Entity 資料物件的泛型</typeparam>
    /// <param name="cacheId">資料快取識別碼</param>
    /// <returns>取得結果</returns>
    internal delegate Task<Result<TData>> GetHashObjectAsyncHandler<TData>(string cacheId);

    /// <summary>取得錯誤訊息的委派
    /// </summary>
    /// <returns>錯誤訊息</returns>
    internal delegate string GetMessageHandler();

    /// <summary>設定錯誤訊息的委派
    /// </summary>
    /// <param name="message">錯誤訊息</param>
    internal delegate void SetMessageHandler(string message);

    /// <summary>取得快取資料數量的委派
    /// </summary>
    /// <returns>快取資料數量</returns>
    internal delegate int GetCountHandler();

    /// <summary>執行 Redis 快取服務指令的委派
    /// </summary>
    /// <typeparam name="TResult">Redis 指令執行後的回傳值泛型</typeparam>
    /// <param name="commandArgs">Redis 服務的指令參數</param>
    /// <returns>Redis 指令執行後的回傳值</returns>
    internal delegate TResult ExecuteRedisCommandHandler<TResult>(RedisCommandArgs commandArgs);

    /// <summary>執行 Redis 快取服務指令的委派
    /// </summary>
    /// <typeparam name="TResult">Redis 指令執行後的回傳值泛型</typeparam>
    /// <param name="commandArgs">Redis 服務的指令參數</param>
    /// <returns>Redis 指令執行後的回傳值</returns>
    internal delegate Task<TResult> ExecuteRedisCommandAsyncHandler<TResult>(RedisCommandArgs commandArgs);

    /// <summary>執行 Redis 快取服務指令的委派
    /// </summary>
    /// <typeparam name="TResult">Redis 指令執行後的回傳值泛型</typeparam>
    /// <param name="commandArgs">Redis 服務的指令參數</param>
    /// <returns>Redis 指令執行後的回傳值</returns>
    internal delegate TResult ExecuteRedisHashCommandHandler<TResult>(RedisHashCommandArgs commandArgs);

    /// <summary>執行 Redis 快取服務指令的委派
    /// </summary>
    /// <typeparam name="TResult">Redis 指令執行後的回傳值泛型</typeparam>
    /// <param name="commandArgs">Redis 服務的指令參數</param>
    /// <returns>Redis 指令執行後的回傳值</returns>
    internal delegate Task<TResult> ExecuteRedisHashCommandAsyncHandler<TResult>(RedisHashCommandArgs commandArgs);

    #endregion Internal Delegates
}


