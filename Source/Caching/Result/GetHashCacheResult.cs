﻿using ZayniFramework.Common;


namespace ZayniFramework.Caching
{
    /// <summary>取得 HashProperty 結構的快取資料的結果
    /// </summary>
    public class GetHashCacheResult<TData> : Result<TData>
    {
    }

    /// <summary>取得 HashProperty 結構的快取資料的結果
    /// </summary>

    public class GetHashCacheResult : Result<object>
    {
    }

    /// <summary>取得 HashProperty 集合的快取資料的結果
    /// </summary>
    public class GetHashPropertiesCacheResult : Result<HashProperty[]>
    {
    }
}
