﻿using Newtonsoft.Json;
using System;
using ZayniFramework.Common;


namespace ZayniFramework.Caching
{
    /// <summary>從快取池中取得資料的結果
    /// </summary>
    public class GetCacheResult<TModel> : BaseResult
    {
        /// <summary>資料異動識別 ID 代碼，每次更新後資料後改變。
        /// </summary>
        [JsonProperty(PropertyName = "dataId")]
        public string DataId { get; set; }

        /// <summary>快取資料
        /// </summary>
        [JsonProperty(PropertyName = "cacheData")]
        public TModel CacheData { get; set; }

        /// <summary>資料最後更新時間<para/>
        /// 1. 資料新增或更新至快取倉儲時的時間戳記<para/>
        /// 2. 如果對快取倉儲查詢的 CacheId 不存在，則此 RefreshTime 屬性則為 Null 值。
        /// </summary>
        [JsonProperty(PropertyName = "refreshTime")]
        public DateTime? RefreshTime { get; set; }
    }

    /// <summary>從快取池中取得資料的結果
    /// </summary>

    public class GetCacheResult : GetCacheResult<object>
    {
    }
}
