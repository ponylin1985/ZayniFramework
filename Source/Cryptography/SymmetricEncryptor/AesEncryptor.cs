﻿using System;
using System.IO;
using System.Security.Cryptography;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.Cryptography
{
    /// <summary>AES對稱式加解密演算法元件<para/>
    /// * 密碼以外的資料，若需要加密處理，建議使用此AES對稱式加解密演算法，加密強度與效能皆比較好。
    /// </summary>
    /// <remarks>
    /// 1. 密碼以外的資料，若需要加密處理，建議使用此AES對稱式加解密演算法，加密強度與效能皆比較好。<para/>
    /// 2. AES演算法的Block Size只能支援為128 bit。<para/>
    /// 3. AES演算法的Key Size可以支援: 128 或 256 bit。<para/>
    /// 參考文章如下:<para/>
    /// https://social.msdn.microsoft.com/Forums/vstudio/en-US/0d22648f-149e-4aea-918e-c3e86a30b23a/keysize-and-block-size-of-aes-128-192-and-256-bits<para/>
    /// http://stackoverflow.com/questions/17171893/algorithm-is-the-rijndaelmanaged-class-in-c-sharp-equivalent-to-aes-encryption<para/>
    /// https://msdn.microsoft.com/en-us/library/system.security.cryptography.aescryptoserviceprovider.keysize(v=vs.110).aspx
    /// </remarks>
    public class AesEncryptor : BaseEncryptor
    {
        #region 實作ICryptographyer介面方法

        /// <summary>對目標明文字串進行加密
        /// </summary>
        /// <param name="targetText">目標明文字串</param>
        /// <returns>加密過後的秘文字串</returns>
        public override string Encrypt(string targetText)
        {
            var result = "";

            try
            {
                // 20151012 Bugfix by Pony: When converting the target string to binary data here, the encoding must be UTF8. Otherwise, characters like Chinese or Japanese will become garbled when decrypted.
                using var aesProvider = GetProvider();
                var sourceBytes = targetText.ToUtf8Bytes();
                var ictE = aesProvider.CreateEncryptor();

                using var msS = new MemoryStream();
                using var csS = new CryptoStream(msS, ictE, CryptoStreamMode.Write);
                csS.Write(sourceBytes, 0, sourceBytes.Length);
                csS.FlushFinalBlock();

                var encryptedBytes = msS.ToArray();
                result = Convert.ToBase64String(encryptedBytes);
            }
            catch (Exception ex)
            {
                Logger.Exception(this, ex, $"An exception occurred while encrypting the target string in AesEncryptor: {ex}");
                HandlerExecuter.DoExceptionHandler(base.ExceptionCallback, ex);
            }

            return result;
        }

        /// <summary>對目標秘文字串進行解密
        /// </summary>
        /// <param name="cipherText">目標秘文字串</param>
        /// <returns>解密過後的明文字串</returns>
        public override string Decrypt(string cipherText)
        {
            var result = "";

            try
            {
                using var aesProvider = GetProvider();
                var rawBytes = Convert.FromBase64String(cipherText);
                var ictD = aesProvider.CreateDecryptor();

                using var msD = new MemoryStream(rawBytes, 0, rawBytes.Length);
                using var csD = new CryptoStream(msD, ictD, CryptoStreamMode.Read);
                using var stream = new StreamReader(csD);
                result = stream.ReadToEnd();
            }
            catch (Exception ex)
            {
                Logger.Exception(this, ex, $"An exception occurred while decrypting the target string in AesEncryptor: {ex}");
                HandlerExecuter.DoExceptionHandler(base.ExceptionCallback, ex);
            }

            return result;
        }

        #endregion 實作ICryptographyer介面方法


        #region 宣告私有的方法

        /// <summary>取得AES加解密演算法的Provider
        /// </summary>
        /// <returns>AES加解密演算法Provider</returns>
        private Aes GetProvider()
        {
            return (Aes)GetAlgorithmProvider(typeof(Aes));
        }

        #endregion 宣告私有的方法
    }
}
