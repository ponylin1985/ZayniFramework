﻿using System;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.Cryptography
{
    /// <summary>加解密模組Config設定值讀取器
    /// </summary>
    internal static class ConfigReader
    {
        /// <summary>The configuration of ZayniFramework.
        /// </summary>
        /// <returns></returns>
        private static readonly ZayniFrameworkSettings _zayniSettings = ConfigManager.GetZayniFrameworkSettings();

        /// <summary>讀取AES加解密元件的設定值 (預設AES KeySize為128)
        /// </summary>
        public static void LoadAesEncryptorSettings()
        {
            try
            {
                CryptographySettings.AESKeySize =
                    _zayniSettings.CryptographySettings.AesEncryptorSetting.KeySize.IsDotNetDefault(128);
            }
            catch (Exception ex)
            {
                var errorMsg = $"Get CryptographySettings/AesEncryptor/keySize from config file occur exception. {ex}";
                Logger.Exception(nameof(ConfigReader), ex, eventTitle: GetLogTitle(nameof(LoadAesEncryptorSettings)), message: errorMsg);
                Command.StdoutErr($"{GetLogTitle(nameof(LoadAesEncryptorSettings))} {errorMsg}");
                throw;
            }
        }

        /// <summary>讀取Rijndael加解密元件的設定值 (預設Rijndael的BlockSize和KeySize都是128)
        /// </summary>
        public static void LoadRijndaelEncryptorSettings()
        {
            try
            {
                CryptographySettings.RijndaelBlockSize = _zayniSettings.CryptographySettings.RijndaelEncryptorSetting.BlockSize.IsDotNetDefault(128);
                CryptographySettings.RijndaelKeySize = _zayniSettings.CryptographySettings.RijndaelEncryptorSetting.KeySize.IsDotNetDefault(128);
            }
            catch (Exception ex)
            {
                var errorMsg = $"Get CryptographySettings/RijndaelEncryptor from config file occur exception. {ex}";
                Logger.Exception(nameof(ConfigReader), ex, eventTitle: GetLogTitle(nameof(LoadRijndaelEncryptorSettings)), message: errorMsg);
                Command.StdoutErr($"{GetLogTitle(nameof(LoadRijndaelEncryptorSettings))} {errorMsg}");
                throw;
            }
        }

        /// <summary>讀取對稱式加解密金鑰完整路徑
        /// </summary>
        public static void LoadSymmetricAlgorithmKeyPath()
        {
            try
            {
                CryptographySettings.SymmetricAlgorithmKeyPath = _zayniSettings.CryptographySettings.SymmetricAlgorithmKeyPath;
            }
            catch (Exception ex)
            {
                var errorMsg = $"Get CryptographySettings/symmetricAlgorithmKeyPath from config file occur exception. {ex}";
                Logger.Exception(nameof(ConfigReader), ex, eventTitle: GetLogTitle(nameof(LoadSymmetricAlgorithmKeyPath)), message: errorMsg);
                Command.StdoutErr($"{GetLogTitle(nameof(LoadSymmetricAlgorithmKeyPath))} {errorMsg}");
                throw;
            }
        }

        /// <summary>讀取雜湊加密是否需要Salt設定值 (預設都是不啟用Salt)
        /// </summary>
        public static void LoadHashNeedSalt()
        {
            try
            {
                CryptographySettings.HashNeedSalt = _zayniSettings.CryptographySettings.HashEncryptorSetting.EnableSalt;
            }
            catch (Exception ex)
            {
                var errorMsg = $"Get CryptographySettings/HashEncryptor/needSalt from config file occur exception. {ex}";
                Logger.Exception(nameof(ConfigReader), ex, eventTitle: GetLogTitle(nameof(LoadHashNeedSalt)), message: errorMsg);
                Command.StdoutErr($"{GetLogTitle(nameof(LoadHashNeedSalt))} {errorMsg}");
                throw;
            }
        }

        /// <summary>取得日誌標題
        /// </summary>
        /// <param name="methodName">方法名稱</param>
        /// <returns>日誌標題</returns>
        private static string GetLogTitle(string methodName) => $"{nameof(ConfigReader)}.{methodName}";
    }
}
