﻿namespace ZayniFramework.Cryptography
{
    /// <summary>資料加解密模組的 Config 設定值
    /// </summary>
    public static class CryptographySettings
    {
        /// <summary>AES加解密元件金鑰長度 (預設為 128)
        /// </summary>
        public static int AESKeySize { get; set; } = 128;

        /// <summary>Rijndael加解密元件的Block長度 (預設為 128)
        /// </summary>
        public static int RijndaelBlockSize { get; set; } = 128;

        /// <summary>Rijndael加解密元件的金鑰長度 (預設為 128)
        /// </summary>
        public static int RijndaelKeySize { get; set; } = 128;

        /// <summary>雜湊加密是否需要 Salt (預設 false，不啟用 Salted)
        /// </summary>
        public static bool HashNeedSalt { get; set; }

        /// <summary>ZayniFramework 框架的對稱式加解密金鑰路徑<para/>
        /// 1. Unix-like 作業系統上，可以設置相對路徑，譬如: ./ZayniCryptoTest.key<para/>
        /// 2. Unix-like 作業系統上，也可以設置絕對路徑，譬如: /Users/pony/GitRepo/MyGitLab/ZayniFramework/Test/UnitTests/Zayni.Cryptography.Test/ZayniCryptoTest.key<para/>
        /// 3. Winodws 作業系統上，可以設置絕對路徑，譬如: C:\ZayniFramework\CryptographyKey\DefaultKey.key<para/>
        /// </summary>
        public static string SymmetricAlgorithmKeyPath { get; set; } = @"./ZayniCryptoTest.key";
    }
}
