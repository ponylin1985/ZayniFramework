﻿using ZayniFramework.Common;


namespace ZayniFramework.Cryptography;


/// <summary>The factory of salted hash computer.
/// </summary>
public static class SaltedHashComputeFactory
{
    /// <summary>Create a salted hash computer.<para/>
    /// Hash type:<para/>
    /// * sha256<para/>
    /// * sha384<para/>
    /// * sha512
    /// </summary>
    /// <param name="type">Type of SHA algorithm.<para/>
    /// * sha256<para/>
    /// * sha384<para/>
    /// * sha512
    /// </param>
    /// <returns>The salted hash computer instance.</returns>
    public static ISaltedHashComputer Create(string type = "sha256")
    {
        ISaltedHashComputer result = type.ToLower() switch
        {
            "sha256" => new Sha256SaltedHashComputer(),
            "sha384" => new Sha384SaltedHashComputer(),
            "sha512" => new Sha512SaltedHashComputer(),
            _ => new Sha256SaltedHashComputer(),
        };

        return result;
    }
}

