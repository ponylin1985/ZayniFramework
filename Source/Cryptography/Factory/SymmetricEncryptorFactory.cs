﻿using ZayniFramework.Common;


namespace ZayniFramework.Cryptography
{
    /// <summary>對稱式加解密元件工廠
    /// </summary>
    public class SymmetricEncryptorFactory
    {
        /// <summary>對稱式建立加解密元件
        /// </summary>
        /// <param name="mode">加解密演算法</param>
        /// <returns>加解密元件</returns>
        public static ISymmetricEncryptor Create(EncryptMode mode)
        {
            var provider = mode.ToString().ToLower();
            return Create(provider);
        }

        /// <summary>對稱式建立加解密元件
        /// </summary>
        /// <param name="provider">加解密演算法</param>
        /// <returns>加解密元件</returns>
        public static ISymmetricEncryptor Create(string provider = "aes")
        {
            ISymmetricEncryptor result = provider.ToLower() switch
            {
                "aes" => new AesEncryptor(),
                "des" => new DesEncryptor(),
                _ => new AesEncryptor(),
            };
            return result;
        }
    }

    /// <summary>加密演算法
    /// </summary>
    public enum EncryptMode
    {
        /// <summary>AES 對稱式加密，參考如下:<para/>
        /// https://zh.wikipedia.org/wiki/%E9%AB%98%E7%BA%A7%E5%8A%A0%E5%AF%86%E6%A0%87%E5%87%86 <para/>
        /// http://www.codedata.com.tw/social-coding/aes/
        /// </summary>
        AES,

        /// <summary>DES 對稱式加密，參考如下:<para/>
        /// https://zh.wikipedia.org/wiki/%E8%B3%87%E6%96%99%E5%8A%A0%E5%AF%86%E6%A8%99%E6%BA%96 <para/>
        /// http://david50.pixnet.net/blog/post/28795947-%5B%E7%AD%86%E8%A8%98%5Dsymmetric-encryption-%E5%B0%8D%E7%A8%B1%E5%BC%8F%E5%8A%A0%E5%AF%86
        /// </summary>
        DES,

        /// <summary>Rijndael 對稱式加密，參考如下:<para/>
        /// http://blog.sina.com.cn/s/blog_4ad042e50102e56q.html
        /// </summary>
        Rijndael
    }
}
