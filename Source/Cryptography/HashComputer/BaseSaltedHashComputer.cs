﻿using System;
using System.Text;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.Cryptography;


/// <summary>The base of salted computer.
/// </summary>
public abstract class BaseSaltedHashComputer : ISaltedHashComputer
{
    #region Static Constructors

    /// <summary>Static constructor.
    /// </summary>
    static BaseSaltedHashComputer()
    {
        ConfigReader.LoadHashNeedSalt();
    }

    #endregion Static Constructors


    #region Abstract Methods

    /// <summary>Perform salted hash on the source plaintext string. (If the hashed ciphertext requires a salt, this parameter must be passed in, and it cannot be an empty string.)
    /// </summary>
    /// <param name="source">Plaintext string.</param>
    /// <param name="saltedKey">The salted key string. (If the hashed ciphertext requires a salt, this parameter must be passed in, and it cannot be an empty string.)</param>
    /// <returns>The hashed ciphertext.</returns>
    public abstract string ComputeHash(string source, string saltedKey);

    #endregion Abstract Methods


    #region Public Properties

    /// <summary>The exception delegate callback handler.
    /// </summary>
    public ExceptionHandler ExceptionCallback { get; set; }

    #endregion Public Properties


    #region Protected Delegates

    /// <summary>The hash computer handler.
    /// </summary>
    /// <param name="source">The bytes of source plaintext.</param>
    /// <returns>The hashed ciphertext.</returns>
    protected delegate byte[] HashHandler(byte[] source);

    #endregion Protected Delegates


    #region Protected Methods

    /// <summary>Convert the bytes to plaintext string.
    /// </summary>
    /// <param name="bytes">Source bytes.</param>
    /// <returns>Plaintext string.</returns>
    protected static string ConvertBytesToString(byte[] bytes)
    {
        var sb = new StringBuilder("");

        for (var i = 0; i < bytes.Length; i++)
        {
            sb.Append(bytes[i].ToString("X2"));
        }

        return sb.ToString();
    }

    /// <summary>Create a salt string for hashing.
    /// </summary>
    /// <param name="saltedKey">The salted key string.</param>
    /// <returns>The salt string for hashing.</returns>
    protected static string CreateSalt(string saltedKey)
    {
        var userBytes = ASCIIEncoding.ASCII.GetBytes(saltedKey);
        var xORED = 0x00;

        foreach (int x in userBytes)
        {
            xORED ^= x;
        }

        var random = new Random(Convert.ToInt32(xORED));
        var salt = new StringBuilder("");
        salt.Append(random.Next());
        salt.Append(random.Next());
        salt.Append(random.Next());
        salt.Append(random.Next());
        return salt.ToString();
    }

    /// <summary>Perform salted hash on the source plaintext string.
    /// </summary>
    /// <param name="source">Plaintext string</param>
    /// <param name="saltedKey">The salted key string.</param>
    /// <param name="handler">The hash computer handler.</param>
    /// <returns>The hashed ciphertext.</returns>
    protected string ComputeHash(string source, string saltedKey, HashHandler handler)
    {
        var salt = "";

        if (CryptographySettings.HashNeedSalt)
        {
            salt = CreateSalt(saltedKey);
            source = string.Concat(salt, source);
        }

        var encoder = new UTF8Encoding();
        var bytes = default(byte[]);

        try
        {
            bytes = handler(encoder.GetBytes(source));
        }
        catch (Exception ex)
        {
            Logger.Exception(this, ex, $"An error occured while trying to compute hash. {ex}");
            HandlerExecuter.DoExceptionHandler(ExceptionCallback, ex);
            return default;
        }

        return ConvertBytesToString(bytes);
    }

    #endregion Protected Methods
}

