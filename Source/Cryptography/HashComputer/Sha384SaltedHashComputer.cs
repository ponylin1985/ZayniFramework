﻿using System.Security.Cryptography;


namespace ZayniFramework.Cryptography;


/// <summary>SHA-384 salted hash computer.
/// </summary>
public class Sha384SaltedHashComputer : BaseSaltedHashComputer
{
    /// <summary>Perform salted hash on the source plaintext string.
    /// </summary>
    /// <param name="source">Plaintext string.</param>
    /// <param name="saltedKey">The salted key string.</param>
    /// <returns>The hashed ciphertext.</returns>
    public override string ComputeHash(string source, string saltedKey)
    {
        var hasher = SHA384.Create();
        var result = base.ComputeHash(source, saltedKey, hasher.ComputeHash);
        return result;
    }
}

