﻿using System.Security.Cryptography;


namespace ZayniFramework.Cryptography;


/// <summary>SHA-256 salted hash computer.
/// </summary>
public class Sha512SaltedHashComputer : BaseSaltedHashComputer
{
    /// <summary>Perform salted hash on the source plaintext string.
    /// </summary>
    /// <param name="source">Plaintext string.</param>
    /// <param name="saltedKey">The salted key string.</param>
    /// <returns>The hashed ciphertext.</returns>
    public override string ComputeHash(string source, string saltedKey)
    {
        var hasher = SHA512.Create();
        var result = base.ComputeHash(source, saltedKey, hasher.ComputeHash);
        return result;
    }
}

