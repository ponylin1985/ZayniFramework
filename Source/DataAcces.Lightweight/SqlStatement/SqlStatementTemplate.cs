﻿namespace ZayniFramework.DataAccess.Lightweight
{
    /// <summary>SQL敘述語法的樣板
    /// </summary>
    internal class SqlStatementTemplate
    {
        #region 宣告靜態建構子

        /// <summary>靜態建構子
        /// </summary>
        static SqlStatementTemplate()
        {
            // {0}: 查詢欄位, {1}: 資料表名稱
            Select = @" 
                SELECT {0} 
                    FROM {1} ";

            // {0}: T-SQL語法 TOP語法保留位置, {1}: 查詢欄位, {2}: 資料表名稱, {3}: PL/SQL 或 MySQL TOP 語法保留位置
            SelectTop = @" 
                SELECT {0}
                    {1}
                  FROM {2}
                   {3} ";

            // {0}: 欄位名稱
            Column = @" , {0} ";

            // {0}: SQL條件敘述
            Where = @" WHERE {0} ";

            // {0}: 欄位, {1}: 運算子, {2}: 參數值
            WhereCondition = @" {0} {1} {2} ";

            And = @" AND ";

            Or = @" OR ";

            // {0}: 資料表名稱, {1}: 新增的欄位, {2}: 欄位值 (參數)
            InsertInto = @"
                INSERT INTO {0} 
                ( 
                    {1} 
                ) 
                VALUES 
                ( 
                    {2} 
                ) ";

            // {0}: 資料表名稱, {1}: 要修改的欄位, {2}: WHERE條件的欄位字串
            Update = @"
                UPDATE {0}
                   SET {1}
                 WHERE {2} ";

            // {0}: 資料表名稱, {1}: WHERE條件的欄位字串
            Delete = @"
                DELETE FROM {0}
                    WHERE {1} ";

            DeleteAll = @"DELETE FROM {table}; ";
        }

        #endregion 宣告靜態建構子


        #region 宣告內部的靜態屬性

        /// <summary>Select查詢語法樣板: SELECT {0} FROM {1}  說明-->  {0}: 查詢欄位, {1}: 資料表名稱
        /// </summary>
        internal static string Select { get; set; }

        /// <summary>Select TOP 查詢語法樣板:  SELECT {0} {1} FROM {2} {3}  說明--> {0}: T-SQL語法 TOP語法保留位置, {1}: 查詢欄位, {2}: 資料表名稱, {3}: PL/SQL TOP與法保留位置
        /// </summary>
        internal static string SelectTop { get; set; }

        /// <summary>欄位語法樣板:  , {0}  說明-->　{0}: 欄位名稱
        /// </summary>
        internal static string Column { get; set; }

        /// <summary>Where 條件語法樣版:  WHERE {0} 　說明-->　{0}: SQL條件敘述
        /// </summary>
        internal static string Where { get; set; }

        /// <summary>條件語法樣板:  {0} {1} {2} ，說明--> {0}: 欄位, {1}: 運算子, {2}: 參數值
        /// </summary>
        internal static string WhereCondition { get; set; }

        /// <summary>AND 運算子樣版:  AND 
        /// </summary>
        internal static string And { get; set; }

        /// <summary>OR 運算子樣版:  OR 
        /// </summary>
        internal static string Or { get; set; }

        /// <summary>INSERT INTO 語法敘述樣板:  INSERT INTO {0} ( {1} ) VALUES ( {2} )  說明--> {0}: 資料表名稱, {1}: 新增的欄位, {2}: 欄位值 (參數)
        /// </summary>
        internal static string InsertInto { get; set; }

        /// <summary>UPDATE 語法敘述樣板:  UPDATE {0} SET {1} WHERE {2}  說明--> {0}: 資料表名稱, {1}: 要修改的欄位, {2}: WHERE條件的欄位字串
        /// </summary>
        internal static string Update { get; set; }

        /// <summary>DELETE 語法敘述樣板:  DELETE FROM {0} WHERE {1}  說明--> {0}: 資料表名稱, {1}: WHERE條件的欄位字串
        /// </summary>
        internal static string Delete { get; set; }

        /// <summary>DELETE FROM {table}; 語法
        /// </summary>
        /// <value></value>
        internal static string DeleteAll { get; set; }

        #endregion 宣告內部的靜態屬性
    }
}
