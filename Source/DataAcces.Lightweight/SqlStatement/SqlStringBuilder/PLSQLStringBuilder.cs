﻿using ZayniFramework.Common;


namespace ZayniFramework.DataAccess.Lightweight
{
    /// <summary>Oracle 資料庫 PL-SQL 敘述字串建立者
    /// </summary>
    internal class PlSqlStringBuilder : SqlStringBuilder, ISqlStringBuilder
    {
        #region 實作ISqlStringBuilder介面方法

        /// <summary>建立資料庫查詢欄位的SQL語法 (有TOP的語法)
        /// </summary>
        /// <typeparam name="TModel">ORM資料對應泛型</typeparam>
        /// <param name="top">TOP筆數</param>
        /// <param name="sqlColumns">SQL Select查詢語法字串</param>
        /// <param name="orderByColumns">SQL Order By 排序的欄位字串</param>
        /// <returns>建立結果</returns>
        public Result<string> CreateSelectTopSql<TModel>(int top, string sqlColumns, string orderByColumns)
        {
            return CreateSelectTopSql<TModel>(top, sqlColumns, tSqlTopStmt: "", plSqlTopStmt: " WHERE ROWNUM <= {0} ", mySqlTopStmt: "", orderByColumns: orderByColumns);
        }

        /// <summary>建立SQL查詢語法完整的Where敘述
        /// </summary>
        /// <param name="whereCondition">SQL Where條件的字串</param>
        /// <returns>建立SQL字串結果</returns>
        public Result<string> CreateWhereSql(string whereCondition)
        {
            return new Result<string>()
            {
                Success = true,
                Data = $" AND {whereCondition} ",
            };
        }

        /// <summary>建立新增欄位SQL字串 (包含建立新增欄位、欄位參數、參數資料庫型別、取得參數值)
        /// </summary>
        /// <typeparam name="TModel">ORM資料對應資料模型泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <returns>建立新增欄位SQL字串結果</returns>
        public new CreateColumnsSqlResult CreateInsertIntoColumnsSql<TModel>(TModel model)
        {
            return base.CreateInsertIntoColumnsSql<TModel>(model);
        }

        /// <summary>建立新增欄位SQL字串 (包含建立新增欄位、欄位參數、參數資料庫型別、取得參數值)
        /// </summary>
        /// <typeparam name="TModel">ORM資料對應資料模型泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <param name="ignoreColumns">忽略新增的欄位</param>
        /// <returns>建立新增欄位SQL字串結果</returns>
        public new CreateColumnsSqlResult CreateInsertIntoColumnsSql<TModel>(TModel model, params string[] ignoreColumns)
        {
            return base.CreateInsertIntoColumnsSql<TModel>(model, ignoreColumns);
        }

        #endregion 實作ISqlStringBuilder介面方法


        #region 實作SqlStringBuilder抽象方法

        /// <summary>取得Oracle資料庫的空字串值
        /// </summary>
        /// <returns>Oracle資料庫的空字串值</returns>
        public override string GetDbEmptyString()
        {
            return " ";
        }

        #endregion 實作SqlStringBuilder抽象方法
    }
}
