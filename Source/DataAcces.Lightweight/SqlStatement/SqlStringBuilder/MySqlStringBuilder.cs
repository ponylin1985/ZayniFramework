﻿using System;
using ZayniFramework.Caching;
using ZayniFramework.Common;
using ZayniFramework.Common.ORM;


namespace ZayniFramework.DataAccess.Lightweight
{
    /// <summary>MySQL 資料庫 SQL 敘述字串建立者
    /// </summary>
    internal class MySqlStringBuilder : SqlStringBuilder, ISqlStringBuilder
    {
        #region 實作 ISqlStringBuilder 介面

        /// <summary>建立資料庫查詢欄位的 SQL 語法 (有 LIMIT 的語法)<para/>
        /// </summary>
        /// <typeparam name="TModel">ORM資料對應泛型</typeparam>
        /// <param name="top">MySQL LIMIT 限制筆數 (只查詢前 N 筆的資料筆數)</param>
        /// <param name="sqlColumns">SQL Select 查詢語法字串</param>
        /// <param name="orderByColumns">SQL Order By 排序的欄位字串</param>
        /// <returns>建立結果</returns>
        public Result<string> CreateSelectTopSql<TModel>(int top, string sqlColumns, string orderByColumns)
        {
            var result = new Result<string>()
            {
                Success = false,
                Data = ""
            };

            var tSqlTopStmt = string.Empty;
            var mySqlTopStmt = $" LIMIT {top} ";

            var cacheId = $"{GetCacheId<TModel>()}:BuilderCreateSelectSql";

            GetSomethingFromDataSource callback = () =>
            {
                MappingTableAttribute orm = null;

                try
                {
                    orm = Reflector.GetCustomAttribute<MappingTableAttribute>(typeof(TModel));
                }
                catch (Exception ex)
                {
                    throw new CreateSqlStatementException($"建立資料庫查詢欄位的SQL語法發生異常: {ex}");
                }

                if (orm.IsNull())
                {
                    throw new CreateSqlStatementException("建立資料庫查詢欄位的SQL語法發生異常: 反射取得資料模型的 MappingTableAttribute 中介資料為 Null 值。");
                }

                if (orm.TableName.IsNullOrEmpty())
                {
                    throw new CreateSqlStatementException("建立資料庫查詢欄位的SQL語法發生異常: 反射取得資料模型中對應資料庫的資料表名稱為空字串或 Null。");
                }

                return orm;
            };

            MappingTableAttribute metadata = null;

            var g = MemoryCache.Get<MappingTableAttribute>(cacheId, callback, 60);

            if (!g.Success)
            {
                metadata = (MappingTableAttribute)callback();
                MemoryCache.Put(cacheId, metadata, 60);
                return result;
            }
            else
            {
                metadata = g.CacheData;
            }

            if (metadata.IsNull())
            {
                result.Message = "建立資料庫查詢欄位的SQL語法發生異常: 反射取得資料模型的 MappingTableAttribute 中介資料為Null值。";
                return result;
            }

            string sql = null;
            var sqlSegment = string.Empty;

            if (orderByColumns.IsNullOrEmpty())
            {
                sqlSegment = mySqlTopStmt;
                sql = string.Format(SqlStatementTemplate.SelectTop, tSqlTopStmt, sqlColumns, metadata.TableName, sqlSegment);
            }
            else
            {
                sqlSegment = $"ORDER BY {orderByColumns} {mySqlTopStmt}";
                sql = string.Format(SqlStatementTemplate.SelectTop, tSqlTopStmt, sqlColumns, metadata.TableName, sqlSegment);
            }

            result.Data = sql;
            result.Success = true;
            return result;
        }

        /// <summary>建立 SQL 查詢語法完整的 Where 敘述<para/>
        /// </summary>
        /// <param name="whereCondition">SQL Where 條件的字串</param>
        /// <returns>建立 SQL 字串結果</returns>
        public Result<string> CreateWhereSql(string whereCondition) => SqlStatementMaker.CreateWhereSql(whereCondition);

        /// <summary>建立新增欄位 SQL 字串 (包含建立新增欄位、欄位參數、參數資料庫型別、取得參數值)<para/>
        /// </summary>
        /// <typeparam name="TModel">ORM 資料對應資料模型泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <returns>建立新增欄位 SQL 字串結果</returns>
        public new CreateColumnsSqlResult CreateInsertIntoColumnsSql<TModel>(TModel model) =>
            base.CreateInsertIntoColumnsSql<TModel>(model);

        /// <summary>建立新增欄位 SQL 字串 (包含建立新增欄位、欄位參數、參數資料庫型別、取得參數值)<para/>
        /// </summary>
        /// <typeparam name="TModel">ORM 資料對應資料模型泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <param name="ignoreColumns">忽略新增的欄位</param>
        /// <returns>建立新增欄位 SQL 字串結果</returns>
        public new CreateColumnsSqlResult CreateInsertIntoColumnsSql<TModel>(TModel model, params string[] ignoreColumns) =>
            base.CreateInsertIntoColumnsSql<TModel>(model, ignoreColumns);

        #endregion 實作 ISqlStringBuilder 介面


        #region 實作 SqlStringBuilder 抽象

        /// <summary>取得 MySQL 資料庫的空字串值
        /// </summary>
        /// <returns>MySQL 資料庫的空字串值</returns>
        public override string GetDbEmptyString() => string.Empty;

        #endregion 實作 SqlStringBuilder 抽象


        #region 宣告私有的方法

        /// <summary>取得資料快取ID
        /// </summary>
        /// <typeparam name="TModel">ORM資料對應資料模型泛型</typeparam>
        /// <returns>資料快取ID</returns>
        private static string GetCacheId<TModel>() =>
            $"Lightweight:{typeof(TModel).FullName}";

        #endregion 宣告私有的方法
    }
}
