﻿using System;


namespace ZayniFramework.DataAccess.Lightweight
{
    /// <summary>資料庫型別轉換異常
    /// </summary>
    public class DbTypeConvertErrorException : Exception
    {
        /// <summary>資料庫型別轉換異常預設建構子
        /// </summary>
        public DbTypeConvertErrorException() : base("Convert C# property type to DbColumnType occur exception.")
        {
        }
    }
}
