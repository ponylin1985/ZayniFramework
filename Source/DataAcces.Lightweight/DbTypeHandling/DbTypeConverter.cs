﻿using System;
using ZayniFramework.Common;


namespace ZayniFramework.DataAccess.Lightweight
{
    /// <summary>資料庫型別轉換器
    /// </summary>
    internal class DbTypeConverter
    {
        /// <summary>將 C# 型別轉換為資料庫型別代碼 (Zayni Framework框架定義的 DbColumnType 資料庫型別代碼)
        /// </summary>
        /// <param name="type">C# 型別</param>
        /// <returns>資料庫型別代碼 (Zayni Framework框架定義的 DbColumnType 資料庫型別代碼)</returns>
        internal static int Convert(Type type)
        {
            var typeName = type.Name;

            // 20170929 Edited by Pony: 為了支援資料庫中有 Nullable 的欄位，因此，這邊還是支援 C# Nullable 的型別轉換。
            if (Reflector.IsNullableType(type) && type.IsValueType)
            {
                typeName = Reflector.GetNullableType(type).Name;
            }

            return typeName switch
            {
                "String" => DbColumnType.String,
                "Boolean" => DbColumnType.Boolean,
                "Int32" => DbColumnType.Int32,
                "Int64" => DbColumnType.Int64,
                "Decimal" => DbColumnType.Decimal,
                "Double" => DbColumnType.Double,
                "Byte[]" => DbColumnType.Binary,
                "DateTime" => DbColumnType.DateTime,
                _ => throw new DbTypeConvertErrorException(),
            };
        }
    }
}
