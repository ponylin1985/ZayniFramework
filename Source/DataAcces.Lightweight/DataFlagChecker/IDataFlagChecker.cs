﻿using System.Data.Common;
using System.Threading.Tasks;


namespace ZayniFramework.DataAccess.Lightweight
{
    /// <summary>資料異動檢查器介面
    /// </summary>
    internal interface IDataFlagChecker
    {
        /// <summary>進行資料是否有異動的檢查
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <param name="trans">資料庫交易</param>
        /// <param name="conn">資料庫連線</param>
        /// <returns>資料是否有異動，true代表有異動，false代表沒有異動。</returns>
        Task<bool> CheckAsync(object model, DbConnection conn = null, DbTransaction trans = null);

        /// <summary>進行資料是否有異動的檢查
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <param name="trans">資料庫交易</param>
        /// <param name="conn">資料庫連線</param>
        /// <returns>資料是否有異動，true代表有異動，false代表沒有異動。</returns>
        bool Check(object model, DbConnection conn = null, DbTransaction trans = null);
    }
}
