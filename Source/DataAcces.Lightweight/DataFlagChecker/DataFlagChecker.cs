﻿using System;


namespace ZayniFramework.DataAccess.Lightweight
{
    /// <summary>資料異動檢查器基底
    /// </summary>
    internal abstract class DataFlagChecker
    {
        /// <summary>多載建構子
        /// </summary>
        /// <param name="dao">資料存取物件</param>
        internal DataFlagChecker(Dao dao)
        {
            ArgumentNullException.ThrowIfNull(dao);
            Dao = dao;
        }

        /// <summary>資料存取物件
        /// </summary>
        protected Dao Dao { get; set; }
    }
}
