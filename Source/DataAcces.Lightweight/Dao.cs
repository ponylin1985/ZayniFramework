﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;
using ZayniFramework.Common;

namespace ZayniFramework.DataAccess.Lightweight
{
    /// <summary>內部資料存取類別
    /// </summary>
    internal class Dao : BaseDataAccess
    {
        /// <summary>多載建構子
        /// </summary>
        /// <param name="dbName">資料庫連線字串名稱</param>
        internal Dao(string dbName) : base(dbName)
        {
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="dbName">資料庫連線字串名稱</param>
        /// <param name="connectionString">資料庫連線字串</param>
        /// <param name="zayniConnectionString">ZayniFramework 的資料庫連線字串 (若傳入為Null或空字串，SQL Logging 功能將停用)</param>
        internal Dao(string dbName, string connectionString, string zayniConnectionString = "") : base(dbName, connectionString, zayniConnectionString)
        {
        }

        /// <summary>錯誤訊息
        /// </summary>
        internal new string Message => base.Message;

        /// <summary>建立全新的資料庫連線
        /// </summary>
        /// <param name="dbName">資料庫連線字串名稱</param>
        /// <returns>新的資料庫連線</returns>
        internal new DbConnection CreateConnection(string dbName = "") => base.CreateConnection(dbName);

        /// <summary>建立全新的資料庫連線
        /// </summary>
        /// <param name="dbName">資料庫連線字串名稱</param>
        /// <returns>新的資料庫連線</returns>
        internal new async Task<DbConnection> CreateConnectionAsync(string dbName = "") => await base.CreateConnectionAsync(dbName);

        /// <summary>開啟資料庫交易
        /// </summary>
        /// <param name="connection">來源資料庫連線</param>
        /// <param name="isolationLevel">交易鎖定層級</param>
        /// <returns>資料庫交易</returns>
        internal new DbTransaction BeginTransaction(DbConnection connection, IsolationLevel isolationLevel = IsolationLevel.ReadCommitted) =>
            base.BeginTransaction(connection, isolationLevel);

        /// <summary>開啟資料庫交易
        /// </summary>
        /// <param name="connection">來源資料庫連線</param>
        /// <param name="isolationLevel">交易鎖定層級</param>
        /// <returns>資料庫交易</returns>
        internal new async Task<DbTransaction> BeginTransactionAsync(DbConnection connection, IsolationLevel isolationLevel = IsolationLevel.ReadCommitted) =>
            await base.BeginTransactionAsync(connection, isolationLevel);

        /// <summary>確認目標資料庫交易
        /// </summary>
        /// <param name="transaction">目標資料庫交易</param>
        /// <returns>是否確認交易成功</returns>
        internal new bool Commit(DbTransaction transaction) => base.Commit(transaction);

        /// <summary>確認目標資料庫交易
        /// </summary>
        /// <param name="transaction">目標資料庫交易</param>
        /// <returns>是否確認交易成功</returns>
        internal new async Task<bool> CommitAsync(DbTransaction transaction) => await base.CommitAsync(transaction);

        /// <summary>取消目標資料庫交易
        /// </summary>
        /// <param name="transaction">目標資料庫交易</param>
        /// <returns>是否取消交易成功</returns>
        internal new bool Rollback(DbTransaction transaction) => base.Rollback(transaction);

        /// <summary>取消目標資料庫交易
        /// </summary>
        /// <param name="transaction">目標資料庫交易</param>
        /// <returns>是否取消交易成功</returns>
        internal new async Task<bool> RollbackAsync(DbTransaction transaction) => await base.RollbackAsync(transaction);

        /// <summary>執行 DbCommand 資料庫指令。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <returns>是否執行成功</returns>
        public new async Task<bool> ExecuteNonQueryAsync(DbCommand command) => await base.ExecuteNonQueryAsync(command);

        /// <summary>執行 DbCommand 資料庫指令。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <returns>是否執行成功</returns>
        public new bool ExecuteNonQuery(DbCommand command) => base.ExecuteNonQuery(command);

        /// <summary>執行 DbCommand 查詢指令，並且輸出單一純量值。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <returns>執行是否成功</returns>
        internal new async Task<Result<object>> ExecuteScalarAsync(DbCommand command) => await base.ExecuteScalarAsync(command);

        /// <summary>執行 DbCommand 查詢指令，並且輸出單一純量值。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="result">查詢單一純量值結果</param>
        /// <returns>執行是否成功</returns>
        internal new bool ExecuteScalar(DbCommand command, out object result) => base.ExecuteScalar(command, out result);

        /// <summary>執行 DbCommand 查詢指令，輸出查詢結果資料模型集合
        /// </summary>
        /// <typeparam name="TModel">資料模型泛型</typeparam>
        /// <param name="command">資料庫指令</param>
        /// <returns>是否執行查詢成功</returns>
        internal new async Task<Result<IEnumerable<TModel>>> LoadDataToModelAsync<TModel>(DbCommand command) where TModel : new() =>
            await base.LoadDataToModelAsync<TModel>(command);

        /// <summary>執行 DbCommand 查詢指令，輸出查詢結果資料模型集合。
        /// </summary>
        /// <typeparam name="TModel">資料模型泛型</typeparam>
        /// <param name="command">資料庫指令</param>
        /// <param name="models">資料模型集合</param>
        /// <returns>是否執行查詢成功</returns>
        internal new bool LoadDataToModel<TModel>(DbCommand command, out IEnumerable<TModel> models) where TModel : new() =>
            base.LoadDataToModel<TModel>(command, out models);

        /// <summary>取得資料庫指令物件
        /// </summary>
        /// <param name="commandText">SQL 敘述字串</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <returns>資料庫指令</returns>
        internal new DbCommand GetSqlStringCommand(string commandText, DbConnection connection = null, DbTransaction transaction = null) =>
            base.GetSqlStringCommand(commandText, connection, transaction);

        /// <summary>新增資料庫指令參數
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="parameterName">資料庫指令的參數名稱</param>
        /// <param name="dbType">參數型態 (可以使用ZayniFramework 框架的 DbColumnType 靜態常數)</param>
        /// <param name="value">參數值</param>
        internal new DbCommand AddInParameter(DbCommand command, string parameterName, int dbType, object value) =>
            base.AddInParameter(command, parameterName, dbType, value);
    }
}
