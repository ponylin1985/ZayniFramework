﻿using System;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.DataAccess.Lightweight
{
    /// <summary>資料存取的設定
    /// </summary>
    internal static class DataAccessConfig
    {
        /// <summary>取得連線的資料庫供應商設定
        /// </summary>
        /// <param name="dbName">資料庫連線字串名稱</param>
        /// <returns>資料庫供應商設定</returns>
        internal static string GetDbProvider(string dbName)
        {
            try
            {
                var databaseSetting =
                    ConfigManager
                        .GetZayniFrameworkSettings()
                        .DataAccessSettings
                        .Find(s => s.Name == dbName);

                return databaseSetting.DatabaseProvider;
            }
            catch (Exception ex)
            {
                var errorMsg = $"Get DataAccessSettings/DatabaseProviders/dataBaseProvider in config file occur exception. dbName: {dbName}. {ex}";
                Logger.Error(nameof(DataAccessConfig), errorMsg, Logger.GetTraceLogTitle(nameof(DataAccessConfig), nameof(GetDbProvider)));
                ConsoleLogger.LogError($"{Logger.GetTraceLogTitle(nameof(DataAccessConfig), nameof(GetDbProvider))} {errorMsg}");
                throw;
            }
        }
    }
}
