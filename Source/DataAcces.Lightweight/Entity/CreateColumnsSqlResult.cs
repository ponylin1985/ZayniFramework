﻿using System.Collections.Generic;


namespace ZayniFramework.DataAccess.Lightweight
{
    /// <summary>建立資料庫欄位SQL語法結果
    /// </summary>
    internal sealed class CreateColumnsSqlResult
    {
        #region 宣告建構子

        /// <summary>解構子
        /// </summary>
        ~CreateColumnsSqlResult()
        {
            ColumnsString = null;
            ParametersString = null;
            WhereConditionString = null;
            OrderByColumnsString = null;
            SqlParameters = null;
            Message = null;
        }

        #endregion 宣告建構子


        #region 宣告內部的屬性

        /// <summary>是否成功
        /// </summary>
        internal bool IsSuccess { get; set; }

        /// <summary>新增欄位字串
        /// </summary>
        internal string ColumnsString { get; set; }

        /// <summary>參數字串
        /// </summary>
        internal string ParametersString { get; set; }

        /// <summary>WHERE 條件字串
        /// </summary>
        internal string WhereConditionString { get; set; }

        /// <summary>Order By 排序的欄位字串
        /// </summary>
        internal string OrderByColumnsString { get; set; }

        /// <summary>SQL 參數集合
        /// </summary>
        internal Dictionary<string, DbParameterEntry> SqlParameters { get; set; } = [];

        /// <summary>執行結果訊息
        /// </summary>
        internal string Message { get; set; }

        #endregion 宣告內部的屬性
    }
}
