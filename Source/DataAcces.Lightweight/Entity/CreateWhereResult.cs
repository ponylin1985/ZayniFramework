﻿using System.Collections.Generic;
using ZayniFramework.Common;


namespace ZayniFramework.DataAccess.Lightweight
{
    /// <summary>建立 SQL Where 條件語法的結果
    /// </summary>
    internal class CreateWhereResult : Result<List<DbParameterEntry>>
    {
        /// <summary>SQL Where 條件語法字串
        /// </summary>
        internal string SqlWhereStmt { get; set; }
    }
}
