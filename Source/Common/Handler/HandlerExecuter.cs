﻿using System;
using System.Collections.Generic;
using System.Data;


namespace ZayniFramework.Common
{
    /// <summary>ZayniFramework 框架的委派執行器
    /// </summary>
    public static class HandlerExecuter
    {
        #region 宣告公開的靜態屬性

        /// <summary>錯誤訊息
        /// </summary>
        public static string ErrorMessage { get; set; }

        #endregion 宣告公開的靜態屬性


        #region 宣告公開的靜態方法

        /// <summary>執行例外委派
        /// </summary>
        /// <param name="handler">ZayniFramework 框架的例外委派</param>
        /// <param name="exception">例外物件</param>
        /// <param name="sender">處發例外事件的物件</param>
        /// <param name="eventTitle">事件訊息</param>
        public static void DoExceptionHandler(ExceptionHandler handler, Exception exception, object sender = null, string eventTitle = "")
        {
            if (new object[] { handler, exception }.HasNullElements())
            {
                return;
            }

            try
            {
                handler(exception, sender, eventTitle);
            }
            catch (Exception ex)
            {
                ErrorMessage = $"執行例外委派發生異常: {ex}";
            }
        }

        /// <summary>執行成功委派
        /// </summary>
        /// <param name="handler">ZayniFramework 框架的成功委派</param>
        /// <param name="args">參數</param>
        public static void DoSuccessHandler(SuccessHandler handler, params object[] args)
        {
            if (handler.IsNull())
            {
                return;
            }

            try
            {
                handler(args);
            }
            catch (Exception ex)
            {
                ErrorMessage = $"執行成功委派發生異常: {ex}";
            }
        }

        /// <summary>執行失敗委派
        /// </summary>
        /// <param name="handler">ZayniFramework 框架的失敗委派</param>
        /// <param name="args">參數</param>
        public static void DoFailureHandler(FailureHandler handler, params object[] args)
        {
            if (handler.IsNull())
            {
                return;
            }

            try
            {
                handler(args);
            }
            catch (Exception ex)
            {
                ErrorMessage = $"執行失敗委派發生異常: {ex}";
            }
        }

        /// <summary>執行動作前的委派
        /// </summary>
        /// <param name="handler">ZayniFramework 框架的動作前委派</param>
        /// <param name="args">參數</param>
        public static void DoBeforeExecuteHandler(BeforeExecuteHandler handler, params object[] args)
        {
            if (handler.IsNull())
            {
                return;
            }

            try
            {
                handler(args);
            }
            catch (Exception e)
            {
                ErrorMessage = $"執行委派發生異常: {e}";
            }
        }

        /// <summary>嘗試執行字串轉型的動作
        /// </summary>
        /// <typeparam name="TSource">轉換的目標型別</typeparam>
        /// <param name="target">要進行轉換的目標字串</param>
        /// <param name="handler">ZayniFramework 框架的字串轉型委派</param>
        /// <param name="value">轉換後的值</param>
        /// <returns>轉換是否成功</returns>
        public static bool TryPareStringValue<TSource>(string target, TryParseHandler<TSource> handler, out TSource value)
        {
            value = default;

            if (target.IsNullOrEmpty())
            {
                ErrorMessage = "目標字串為Null或空字串，無法進行轉換";
                return false;
            }

            if (handler.IsNull())
            {
                ErrorMessage = "字串轉換委派為Null值，無法進行轉換";
                return false;
            }

            var result = false;

            try
            {
                result = handler(target, out value);
            }
            catch (Exception ex)
            {
                ErrorMessage = $"執行委派發生異常: {ex}";
                return false;
            }

            return result;
        }

        /// <summary>執行ORM資料繫結失敗的委派
        /// </summary>
        /// <param name="handler">Zayni.DataAccess框架執行ORM資料繫結失敗回呼處理委派</param>
        /// <param name="ds">DataSet資料集合</param>
        /// <param name="message">失敗錯誤訊息</param>
        public static void DoOrmBindingFailureHandler(DataSetOrmBindingFailureHandler handler, DataSet ds, string message)
        {
            if (new object[] { handler, ds }.HasNullElements())
            {
                return;
            }

            if (message.IsNullOrEmpty())
            {
                return;
            }

            try
            {
                handler(ds, message);
            }
            catch (Exception ex)
            {
                ErrorMessage = $"執行失敗委派發生異常: {ex}";
            }
        }

        /// <summary>執行ORM資料繫結失敗的委派
        /// </summary>
        /// <param name="handler">Zayni.DataAccess框架執行ORM資料繫結失敗回呼處理委派</param>
        /// <param name="reader">資料讀取器</param>
        /// <param name="message">失敗錯誤訊息</param>
        public static void DoOrmBindingFailureHandler(DataReaderOrmBindingFailureHanlder handler, IDataReader reader, string message)
        {
            if (new object[] { handler, reader }.HasNullElements())
            {
                return;
            }

            if (message.IsNullOrEmpty())
            {
                return;
            }

            try
            {
                handler(reader, message);
            }
            catch (Exception ex)
            {
                ErrorMessage = $"執行失敗委派發生異常: {ex}";
            }
        }

        /// <summary>執行ORM資料繫結失敗的委派
        /// </summary>
        /// <param name="handler">Zayni.DataAccess框架執行ORM資料繫結失敗回呼處理委派</param>
        /// <param name="datas">資料集合列表</param>
        /// <param name="message">失敗錯誤訊息</param>
        public static void DoOrmBindingFailureHandler(ModelSetOrmBindingFailureHanlder handler, List<object[]> datas, string message)
        {
            if (new object[] { handler, datas }.HasNullElements())
            {
                return;
            }

            if (message.IsNullOrEmpty())
            {
                return;
            }

            try
            {
                handler(datas, message);
            }
            catch (Exception ex)
            {
                ErrorMessage = $"執行失敗委派發生異常: {ex}";
            }
        }

        #endregion 宣告公開的靜態方法
    }
}
