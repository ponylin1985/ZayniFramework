using NeoSmart.AsyncLock;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace ZayniFramework.Common
{
    /// <summary>命令容器池
    /// </summary>
    public class CommandContainer
    {
        #region 宣告私有的欄位

        /// <summary>非同步作業的鎖定物件
        /// </summary>
        /// <returns></returns>
        private readonly AsyncLock _asyncLock = new();

        /// <summary>命令容器
        /// </summary>
        /// <returns></returns>
        private readonly Dictionary<string, CommandHolder> _container = [];

        #endregion 宣告私有的欄位


        #region 宣告公開的方法

        /// <summary>註冊命令物件
        /// </summary>
        /// <param name="name">命令名稱</param>
        /// <param name="condition">取得命令物件的唯一識別條件委派</param>
        /// <returns>註冊的結果</returns>
        public IResult<CommandHolder> RegisterCommand<TCommand>(string name, Func<string, bool> condition)
            where TCommand : ICommand
        {
            using (_asyncLock.Lock())
            {
                var result = Result.Create<CommandHolder>();
                CommandHolder commandHolder;

                try
                {
                    if (_container.ContainsKey(name))
                    {
                        result.Message = $"Duplicate command name. Name: {name}.";
                        return result;
                    }

                    if (new object[] { condition }.HasNullElements())
                    {
                        result.Message = $"The condition delegate or command object can not be null.";
                        return result;
                    }

                    if (name.IsNullOrEmpty())
                    {
                        result.Message = $"The '{nameof(name)}' of Command object can not be null or empty string.";
                        return result;
                    }

                    commandHolder = new CommandHolder()
                    {
                        Name = name,
                        Condition = condition,
                        TypeOfCommand = typeof(TCommand)
                    };

                    _container.Add(name, commandHolder);
                }
                catch (Exception ex)
                {
                    result.HasException = true;
                    result.ExceptionObject = ex;
                    result.Message = $"Add {nameof(ICommand)} object into container occur exception. {Environment.NewLine}{ex}";
                    return result;
                }

                result.Data = commandHolder;
                result.Success = true;
                return result;
            }
        }

        /// <summary>註冊命令物件
        /// </summary>
        /// <param name="name">命令名稱</param>
        /// <param name="condition">取得命令物件的唯一識別條件委派</param>
        /// <returns>註冊的結果</returns>
        public async Task<IResult<CommandHolder>> RegisterCommandAsync<TCommand>(string name, Func<string, bool> condition)
            where TCommand : ICommand
        {
            using (await _asyncLock.LockAsync())
            {
                var result = Result.Create<CommandHolder>();
                CommandHolder commandHolder;

                try
                {
                    if (_container.ContainsKey(name))
                    {
                        result.Message = $"Duplicate command name. Name: {name}.";
                        return result;
                    }

                    if (new object[] { condition }.HasNullElements())
                    {
                        result.Message = $"The condition delegate or command object can not be null.";
                        return result;
                    }

                    if (name.IsNullOrEmpty())
                    {
                        result.Message = $"The '{nameof(name)}' of Command object can not be null or empty string.";
                        return result;
                    }

                    commandHolder = new CommandHolder()
                    {
                        Name = name,
                        Condition = condition,
                        TypeOfCommand = typeof(TCommand)
                    };

                    _container.Add(name, commandHolder);
                }
                catch (Exception ex)
                {
                    result.HasException = true;
                    result.ExceptionObject = ex;
                    result.Message = $"Add {nameof(ICommand)} object into container occur exception. {Environment.NewLine}{ex}";
                    return result;
                }

                result.Data = commandHolder;
                result.Success = true;
                return result;
            }
        }

        /// <summary>註冊 Unknow 的命令物件
        /// </summary>
        /// <typeparam name="TCommand">Unknow 命令物件的泛型</typeparam>
        /// <returns>註冊結果</returns>
        public IResult<CommandHolder> RegisterUnknowCommand<TCommand>()
            where TCommand : IUnknowCommand
        {
            using (_asyncLock.Lock())
            {
                var result = Result.Create<CommandHolder>();

                try
                {
                    var commandHolder = new CommandHolder()
                    {
                        Name = "unknow",
                        TypeOfCommand = typeof(TCommand)
                    };

                    _container[commandHolder.Name] = commandHolder;

                    result.Success = true;
                    return result;
                }
                catch (Exception ex)
                {
                    result.HasException = true;
                    result.ExceptionObject = ex;
                    result.Message = $"Register UnknowCommand object into container occur exception. {Environment.NewLine}{ex}";
                    return result;
                }
            }
        }

        /// <summary>註冊 Unknow 的命令物件
        /// </summary>
        /// <typeparam name="TCommand">Unknow 命令物件的泛型</typeparam>
        /// <returns>註冊結果</returns>
        public async Task<IResult<CommandHolder>> RegisterUnknowCommandAsync<TCommand>()
            where TCommand : IUnknowCommand
        {
            using (await _asyncLock.LockAsync())
            {
                var result = Result.Create<CommandHolder>();

                try
                {
                    var commandHolder = new CommandHolder()
                    {
                        Name = "unknow",
                        TypeOfCommand = typeof(TCommand)
                    };

                    _container[commandHolder.Name] = commandHolder;

                    result.Success = true;
                    return result;
                }
                catch (Exception ex)
                {
                    result.HasException = true;
                    result.ExceptionObject = ex;
                    result.Message = $"Register UnknowCommand object into container occur exception. {Environment.NewLine}{ex}";
                    return result;
                }
            }
        }

        /// <summary>依照條件取得命令物件
        /// </summary>
        /// <param name="command">原始命令字串</param>
        /// <returns>命令物件</returns>
        public ICommand Get(string command)
        {
            using (_asyncLock.Lock())
            {
                try
                {
                    if (_container.IsNullOrEmpty())
                    {
                        return null;
                    }

                    var unknowKey = "unknow";
                    var type = _container.Values?.Where(d => d.Condition.IsNotNull())?.Where(q => q.Condition(command))?.FirstOrDefault()?.TypeOfCommand;

                    if (type.IsNull() && _container.TryGetValue(unknowKey, out var value))
                    {
                        type = value.TypeOfCommand;
                    }

                    return (ICommand)Activator.CreateInstance(type);
                }
                catch
                {
                    return null;
                }
            }
        }

        /// <summary>依照條件取得命令物件
        /// </summary>
        /// <param name="command">原始命令字串</param>
        /// <returns>命令物件</returns>
        public async Task<ICommand> GetAsync(string command)
        {
            using (await _asyncLock.LockAsync())
            {
                try
                {
                    if (_container.IsNullOrEmpty())
                    {
                        return null;
                    }

                    var unknowKey = "unknow";
                    var type = _container.Values?.Where(d => d.Condition.IsNotNull())?.Where(q => q.Condition(command))?.FirstOrDefault()?.TypeOfCommand;

                    if (type.IsNull() && _container.TryGetValue(unknowKey, out var value))
                    {
                        type = value.TypeOfCommand;
                    }

                    return (ICommand)Activator.CreateInstance(type);
                }
                catch
                {
                    return null;
                }
            }
        }

        /// <summary>取得命令物件
        /// </summary>
        /// <param name="commandText">原始命令字串</param>
        /// <typeparam name="TCommand">命令物件的泛型</typeparam>
        /// <returns>命令物件</returns>
        public TCommand Get<TCommand>(string commandText)
            where TCommand : ICommand
        {
            var command = Get(commandText);
            return (TCommand)command.IsNull(default(TCommand));
        }

        /// <summary>取得命令物件
        /// </summary>
        /// <param name="commandText">原始命令字串</param>
        /// <typeparam name="TCommand">命令物件的泛型</typeparam>
        /// <returns>命令物件</returns>
        public async Task<TCommand> GetAsync<TCommand>(string commandText)
            where TCommand : ICommand
        {
            var command = await GetAsync(commandText);
            return (TCommand)command.IsNull(default(TCommand));
        }

        #endregion 宣告公開的方法


        #region 宣告公開的 Chain 方法

        /// <summary>建立一個空的 CommandContainer 命令容器池
        /// </summary>
        /// <returns>命令容器池</returns>
        public static CommandContainer Build() => new();

        /// <summary>建立一個空的 CommandContainer 命令容器池
        /// </summary>
        /// <returns>命令容器池</returns>
        public static async Task<CommandContainer> BuildAsync() => await Task.FromResult(new CommandContainer());

        /// <summary>註冊命令物件
        /// </summary>
        /// <param name="name">命令名稱</param>
        /// <param name="condition">取得命令物件的唯一識別條件委派</param>
        /// <returns>命令容器池</returns>
        public CommandContainer Register<TCommand>(string name, Func<string, bool> condition) where TCommand : ICommand =>
            RegisterCommand<TCommand>(name, condition).Success ? this : null;

        /// <summary>註冊命令物件
        /// </summary>
        /// <param name="name">命令名稱</param>
        /// <param name="condition">取得命令物件的唯一識別條件委派</param>
        /// <returns>命令容器池</returns>
        public async Task<CommandContainer> RegisterAsync<TCommand>(string name, Func<string, bool> condition) where TCommand : ICommand =>
            (await RegisterCommandAsync<TCommand>(name, condition)).Success ? this : null;

        /// <summary>註冊 Unknow 的命令物件
        /// </summary>
        /// <typeparam name="TCommand">Unknow 命令物件的泛型</typeparam>
        /// <returns>註冊結果</returns>
        public CommandContainer RegisterUnknow<TCommand>() where TCommand : IUnknowCommand =>
            RegisterUnknowCommand<TCommand>().Success ? this : null;

        /// <summary>註冊 Unknow 的命令物件
        /// </summary>
        /// <typeparam name="TCommand">Unknow 命令物件的泛型</typeparam>
        /// <returns>註冊結果</returns>
        public async Task<CommandContainer> RegisterUnknowAsync<TCommand>() where TCommand : IUnknowCommand =>
            (await RegisterUnknowCommandAsync<TCommand>()).Success ? this : null;

        #endregion 宣告公開的 Chain 方法
    }
}