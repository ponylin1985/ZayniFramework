using System;
using System.Runtime.Loader;
using System.Threading;
using System.Threading.Tasks;


namespace ZayniFramework.Common
{
    /// <summary>Zayni Framework 框架內建的預設主控台 Console 命令處理服務
    /// </summary>
    public class ConsoleCommandService
    {
        /// <summary>啟動主控台命令處理服務<para/>
        /// * 此為 Block 方法，代表呼叫此方法，後續的程式碼如果執行在同一條執行緒會被 Block 住!<para/>
        /// * 如果在 docker container 執行中，呼叫此方法仍不能將 process block 住，請呼叫 ConsoleCommandService.StartConsoleHostAsync 方法。
        /// </summary>
        /// <param name="container">命令容器</param>
        public static async Task StartDefaultConsoleServiceAsync(CommandContainer container)
        {
            if (container.IsNull())
            {
                return;
            }

            await Command.StdoutAsync($"Waitting for console command now...", ConsoleColor.Yellow);

            while (true)
            {
                string commandText = null;

                try
                {
                    commandText = await Console.In.ReadLineAsync();

                    if (commandText.IsNullOrEmpty())
                    {
                        await Task.Delay(2);
                        continue;
                    }

                    var command = await container.GetAsync<ConsoleCommand>(commandText);

                    if (command.IsNull())
                    {
                        await Task.Delay(2);
                        continue;
                    }

                    command.Parameters["CommandText"] = commandText;
                    command.CommandText = commandText;
                    await command.ExecuteAsync();
                }
                catch (Exception ex)
                {
                    await Command.StdoutErrAsync($"Process console command occur exception. Console command: {commandText}. {Environment.NewLine}{ex}");
                    await Task.Delay(2);
                    continue;
                }

                await Task.Delay(2);
            }
        }

        /// <summary>啟動主控台應用程式裝載，用以避免應用程式在 docker container 中執行過早結束 process。<para/>
        /// *  如果呼叫 ConsoleCommandService.StartDefaultConsoleService 或 ConsoleCommandService.StartDefaultConsoleServiceAsync 或 TelnetCommandService.StartDefaultAsyncTelnetService 方法之後，
        ///    在 docker container 中仍然無法使用 ReadLine 讓 process 不會中止掉，可以嘗試呼叫此方法，讓 process 在 docker container 中可以持續運行。<para/>
        /// * [Reference](https://stackoverflow.com/questions/46867532/how-to-keep-net-core-console-app-alive-in-docker-container)
        /// </summary>
        public static void StartConsoleHost()
        {
            var ended = new ManualResetEventSlim();
            var starting = new ManualResetEventSlim();

            AssemblyLoadContext.Default.Unloading += ctx =>
            {
                System.Console.WriteLine("Unloding fired.");
                starting.Set();
                System.Console.WriteLine("Waiting for completion.");
                ended.Wait();
            };

            Console.Out.WriteLine("Waiting for signals.");
            starting.Wait();

            Console.Out.WriteLine("Received signal gracefully shutting down.");
            Thread.Sleep(5000);
            ended.Set();
        }

        /// <summary>啟動主控台應用程式裝載，用以避免應用程式在 docker container 中執行過早結束 process。<para/>
        /// *  如果呼叫 ConsoleCommandService.StartDefaultConsoleService 或 ConsoleCommandService.StartDefaultConsoleServiceAsync 或 TelnetCommandService.StartDefaultAsyncTelnetService 方法之後，
        ///    在 docker container 中仍然無法使用 ReadLine 讓 process 不會中止掉，可以嘗試呼叫此方法，讓 process 在 docker container 中可以持續運行。<para/>
        /// * [Reference](https://stackoverflow.com/questions/46867532/how-to-keep-net-core-console-app-alive-in-docker-container)
        /// </summary>
        public static async Task StartConsoleHostAsync()
        {
            var ended = new ManualResetEventSlim();
            var starting = new ManualResetEventSlim();

            AssemblyLoadContext.Default.Unloading += ctx =>
            {
                System.Console.WriteLine("Unloding fired.");
                starting.Set();
                System.Console.WriteLine("Waiting for completion.");
                ended.Wait();
            };

            await Console.Out.WriteLineAsync("Waiting for signals.");
            starting.Wait();

            await Console.Out.WriteLineAsync("Received signal gracefully shutting down.");
            Thread.Sleep(5000);
            ended.Set();
        }
    }
}