using System.Threading.Tasks;


namespace ZayniFramework.Common
{
    /// <summary>未知的命令介面 (標記介面、空介面)
    /// </summary>
    public interface IUnknowCommand : ICommand
    {
    }

    /// <summary>Unknow 主控台指令處理器 (未知的主控台指令)
    /// </summary>
    public class UnknowConsoleCommandAsync : ConsoleCommand, IUnknowCommand
    {
        /// <summary>執行命令處理
        /// </summary>
        /// <param name="parameters">命令參數集合</param>
        /// <returns>執行結果</returns>
        public override async Task<IResult> ExecuteAsync(ParameterCollection parameters)
        {
            await StdoutAsync($"Unknow command! {CommandText}");
            await StdoutAsync("Please enter 'help' to discover all support commands.");
            Result.Success = true;
            return Result;
        }
    }
}