using System;
using System.Threading.Tasks;


namespace ZayniFramework.Common
{
    /// <summary>Clear主控台指令處理器 (原始指令: cls)
    /// </summary>
    public sealed class ClearConsoleCommand : ConsoleCommand
    {
        /// <summary>執行命令處理
        /// </summary>
        /// <param name="parameters">命令參數集合</param>
        /// <returns>執行結果</returns>
        public override async Task<IResult> ExecuteAsync(ParameterCollection parameters)
        {
            await Task.Run(() => Console.Clear());
            Result.Success = true;
            return Result;
        }
    }
}