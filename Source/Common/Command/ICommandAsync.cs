using System.Threading.Tasks;


namespace ZayniFramework.Common
{
    /// <summary>命令處理介面
    /// </summary>
    public interface ICommand
    {
        /// <summary>參數集合
        /// </summary>
        ParameterCollection Parameters { get; }

        /// <summary>執行結果
        /// </summary>
        IResult Result { get; }

        /// <summary>執行主控台指令
        /// </summary>
        Task ExecuteAsync();
    }
}