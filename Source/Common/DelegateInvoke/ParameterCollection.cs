﻿using NeoSmart.AsyncLock;
using System.Collections.Generic;


namespace ZayniFramework.Common
{
    /// <summary>委派的參數集合包裹
    /// </summary>
    public sealed class ParameterCollection
    {
        #region 宣告私有的欄位

        /// <summary>非同步作業鎖定物件
        /// </summary>
        /// <returns></returns>
        private readonly AsyncLock _asyncLock = new();

        /// <summary>委派的參數集合
        /// </summary>
        /// <returns></returns>
        private readonly Dictionary<string, Parameter> _parameters = [];

        #endregion 宣告私有的欄位


        #region 宣告建構子

        /// <summary>多載建構子
        /// </summary>
        /// <param name="parameters">委派的參數</param>
        public ParameterCollection(params Parameter[] parameters)
        {
            if (parameters.IsNullOrEmpty())
            {
                return;
            }

            using (_asyncLock.Lock())
            {
                foreach (var parameter in parameters)
                {
                    _parameters.Add(parameter.Name, parameter);
                }
            }
        }

        #endregion 宣告建構子


        #region 宣告公開的索引子

        /// <summary>委派參數值索引子<para/>
        /// get 存取子:<para/>
        /// 當存取的參數名稱不存在，則回傳 Null 值。<para/>
        /// set 存取子:<para/>
        /// 對傳入的 name 參數名稱進行，進行 Add or Update 的操作。
        /// </summary>
        /// <example>
        /// <code>
        /// var parameters = new ParameterCollection();
        /// parameters[ "A" ] = "AAA";  // 設定參數 A 的值為 "AAA"
        /// parameters[ "B" ] = 123;    // 設定參數 B 的值為 123
        /// var a = parameters[ "A" ];  // a is "AAA" string type.
        /// var b = parameters[ "B" ];  // b is 123 Int32 type.
        /// var c = parameters[ "C" ];  // c is null.
        /// </code>
        /// </example>
        /// <param name="name">參數名稱</param>
        /// <returns>參數值</returns>
        public object this[string name]
        {
            get
            {
                using (_asyncLock.Lock())
                {
                    if (name.IsNullOrEmpty())
                    {
                        return null;
                    }

                    return _parameters.TryGetValue(name, out var parameter) ? parameter?.Value : null;
                }
            }
            set
            {
                using (_asyncLock.Lock())
                {
                    if (name.IsNullOrEmpty())
                    {
                        return;
                    }

                    _parameters[name] = new Parameter(name, value);
                }
            }
        }

        #endregion 宣告公開的索引子
    }
}
