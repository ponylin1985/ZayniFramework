﻿using System;


namespace ZayniFramework.Common
{
    /// <summary>日期時間轉換器
    /// </summary>
    public static class DateTimeConverter
    {
        /// <summary>依照指定的日期格式，將日期時間字串轉換成 DateTime 物件，預設格式為 yyyyMMdd。
        /// </summary>
        /// <param name="dateTimeString">來源日期時間字串</param>
        /// <param name="format">日期時間格式</param>
        /// <returns>日期時間物件</returns>
        public static DateTime Convert(string dateTimeString, string format = "yyyyMMdd")
        {
            if (dateTimeString.IsNullOrEmpty())
            {
                throw new ArgumentException($"Invalid value of '{nameof(dateTimeString)}' argument. Can not be a null or empty string.");
            }

            try
            {
                return DateTime.ParseExact(dateTimeString, format, null);
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException($"{nameof(DateTimeConverter)}.{nameof(Convert)} occur exception. {ex}");
            }
        }
    }
}
