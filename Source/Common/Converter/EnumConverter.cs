﻿using System;


namespace ZayniFramework.Common
{
    /// <summary>列舉轉換器
    /// </summary>
    public static class EnumConverter
    {
        /// <summary>轉換字串成指定的列舉值<para/>
        /// 1. 傳入的列舉值泛型，可以接受 Nullable 的列舉，譬如: ConsoleColor? 或 ConsoleColor 或 DbType? 或 DbType 都可以正常轉換。<para/>
        /// 2. 進行轉換時，可以接受來源字串值為「忽略大小寫」(Case-insensitive) 進行轉換成 enum 列舉值。<para/>
        /// 範例:<para/>
        /// string color = "Green";<para/>
        /// ConsoleColor consoleColor = color.ParseEnum&lt;ConsoleColor&gt;();<para/>
        /// string strState = "Disable";<para/>
        /// State state = strState.ParseEnum&lt;State&gt;();<para/>
        /// </summary>
        /// <example>
        /// <code>
        /// string color = "Green";
        /// ConsoleColor consoleColor = color.ParseEnum&lt;ConsoleColor&gt;();
        /// string strState = "Disable";
        /// State state = strState.ParseEnum&lt;State&gt;();
        /// </code>
        /// </example>
        /// <typeparam name="TEnum">列舉值的泛型</typeparam>
        /// <param name="value"></param>
        /// <returns>列舉值</returns>
        public static TEnum ParseEnum<TEnum>(string value) where TEnum : struct, IConvertible =>
           (TEnum)Enum.Parse(typeof(TEnum), value, true);
    }
}
