﻿namespace ZayniFramework.Common
{
    /// <summary>反序列化結果
    /// </summary>
    /// <typeparam name="TModel">資料模型泛型</typeparam>
    public class DeserializeResult<TModel> : BaseResult
        where TModel : class, new()
    {
        /// <summary>資料集合
        /// </summary>
        public TModel Datas { get; set; }
    }

    /// <summary>反序列化結果
    /// </summary>

    public class DeserializeResult : BaseResult
    {
        /// <summary>資料集合
        /// </summary>
        public object Datas { get; set; }
    }
}
