﻿using System;


namespace ZayniFramework.Common.ORM
{
    /// <summary>資料庫預存程序 ORM 對應的中介資料
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class StoredProcedureAttribute : Attribute
    {
        #region 宣告建構子

        /// <summary>預設建構子
        /// </summary>
        public StoredProcedureAttribute()
        {
            // pass
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="dbName">資料庫連線字串名稱</param>
        /// <param name="procedureName">資料庫預存程序的名稱</param>
        public StoredProcedureAttribute(string dbName, string procedureName)
        {
            DbName = dbName;
            ProcedureName = procedureName;
        }

        /// <summary>解構子
        /// </summary>
        ~StoredProcedureAttribute()
        {
            DbName = null;
            ProcedureName = null;
        }

        #endregion 宣告建構子


        #region 宣告公開的屬性

        /// <summary>資料庫連線字串名稱
        /// </summary>
        public string DbName { get; set; }

        /// <summary>資料庫預存程序的名稱
        /// </summary>
        public string ProcedureName { get; set; }

        #endregion 宣告公開的屬性
    }
}
