using System.Collections.Generic;


namespace ZayniFramework.Common;


/// <summary>
/// </summary>
public class ZayniFrameworkSettingRoot
{
    /// <summary>
    /// </summary>
    /// <value></value>
    public ZayniFrameworkSettings ZayniFrameworkSettings { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public List<AppSetting> AppSettings { get; set; }
}

/// <summary>
/// </summary>
public class ZayniFrameworkSettings
{
    /// <summary>
    /// </summary>
    /// <value></value>
    public ThreadPoolSettings ThreadPoolSettings { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public CryptographySetting CryptographySettings { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public LoggingSettings LoggingSettings { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public ExceptionHandlingSettings ExceptionHandlingSettings { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public List<RedisClientSetting> RedisClientSettings { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public CachingSettings CachingSettings { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public FormattingSettings FormattingSettings { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public List<DataAccessSetting> DataAccessSettings { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public TelnetServerSetting TelnetServerSettings { get; set; }
}

/// <summary>
/// </summary>
public class ThreadPoolSettings
{
    /// <summary>
    /// </summary>
    /// <value></value>
    public int MinWorkerThreads { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public int MinIOCPThreads { get; set; }
}

/// <summary>
/// </summary>
public class CryptographySetting
{
    /// <summary>
    /// </summary>
    /// <value></value>
    public string SymmetricAlgorithmKeyPath { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public AesEncryptorSetting AesEncryptorSetting { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public RijndaelEncryptorSetting RijndaelEncryptorSetting { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public HashEncryptorSetting HashEncryptorSetting { get; set; }
}

/// <summary>
/// </summary>
public class AesEncryptorSetting
{
    /// <summary>
    /// </summary>
    /// <value></value>
    public int KeySize { get; set; }
}

/// <summary>
/// </summary>
public class RijndaelEncryptorSetting
{
    /// <summary>
    /// </summary>
    /// <value></value>
    public int BlockSize { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public int KeySize { get; set; }
}

/// <summary>
/// </summary>
public class HashEncryptorSetting
{
    /// <summary>
    /// </summary>
    /// <value></value>
    public bool EnableSalt { get; set; }
}

/// <summary>
/// </summary>
public class LoggingSettings
{
    /// <summary>
    /// </summary>
    /// <value></value>
    public bool EnableLog { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public bool EnableWindowsEventLog { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public int EsLogResetDays { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public int DbLogResetDays { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public List<LoggerSetting> LoggerSettings { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public List<EmailNotifyLoggerSetting> EmailNotifyLoggerSettings { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public List<ElasticStackLoggerSetting> ElasticStackLoggerSettings { get; set; }
}

/// <summary>
/// </summary>
public class LoggerSetting
{
    /// <summary>
    /// </summary>
    /// <value></value>
    public string Name { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public bool EnableLogger { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public int MaxFileSize { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public bool EnableConsoleOutput { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string ConsoleColor { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string DbLoggerName { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string DbLoggerType { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string EmailNotifyLoggerName { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string ElasticStackLoggerName { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public FileLogSetting FileLogSetting { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public LogFilterSetting Filter { get; set; }
}

/// <summary>
/// </summary>
public class FileLogSetting
{
    /// <summary>
    /// </summary>
    /// <value></value>
    public string Path { get; set; }
}

/// <summary>
/// </summary>
public class LogFilterSetting
{
    /// <summary>
    /// </summary>
    /// <value></value>
    public string FilterType { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string Category { get; set; }
}

/// <summary>
/// </summary>
public class EmailNotifyLoggerSetting
{
    /// <summary>
    /// </summary>
    /// <value></value>
    public string Name { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string SmtpHost { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public int SmtpPort { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public bool EnableSsl { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string SmtpDomain { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string SmtpAccount { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string SmtpPassword { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string FromEmailAddress { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string FromDisplayName { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string ToEmailAddress { get; set; }
}

/// <summary>
/// </summary>
public class ElasticStackLoggerSetting
{
    /// <summary>
    /// </summary>
    /// <value></value>
    public string Name { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public bool IsDefault { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string HostUrl { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string DefaultIndex { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public bool EnableRetry { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public int RetryTimes { get; set; }
}

/// <summary>
/// </summary>
public class ExceptionHandlingSettings
{
    /// <summary>
    /// </summary>
    /// <value></value>
    public FileLoggingPolicySetting FileLoggingPolicySetting { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public EmailNotifyPolicySetting EmailNotifyPolicySetting { get; set; }
}

/// <summary>
/// </summary>
/// <value></value>
public class FileLoggingPolicySetting
{
    /// <summary>
    /// </summary>
    /// <value></value>
    public bool EnableDefaultPolicy { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public bool EnableDefaultPolicyEventLog { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public bool EnableRethrow { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string DefaultPolicyLogFilePath { get; set; }
}

/// <summary>
/// </summary>
public class EmailNotifyPolicySetting
{
    /// <summary>
    /// </summary>
    /// <value></value>
    public bool EnableTextLog { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string TextLogPath { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public bool EnableEventLog { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public bool EnableRethrow { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public bool EnableEmailNotify { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public int SmtpPort { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string SmtpHost { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public bool EnableSsl { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string SmtpDomain { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string SmtpAccount { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string SmtpPassword { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string FromEmailAddress { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string FromDisplayName { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string ToEmailAddress { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string MailSubject { get; set; }
}

/// <summary>
/// </summary>
public class FormattingSettings
{
    /// <summary>
    /// </summary>
    /// <value></value>
    public List<DateTimeFormatStyleSetting> DateTimeFormatStyleSettings { get; set; }
}

/// <summary>
/// </summary>
public class DateTimeFormatStyleSetting
{
    /// <summary>
    /// </summary>
    /// <value></value>
    public string Language { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string Format { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public bool IsDefault { get; set; }
}

/// <summary>
/// </summary>
public class RedisClientSetting
{
    /// <summary>
    /// </summary>
    /// <value></value>
    public string Name { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public bool IsDefault { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string TraceLoggerName { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string Host { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public int Port { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string Password { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public int DatabaseNo { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public int ConnectTimeout { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public int SyncTimeout { get; set; }
}

/// <summary>
/// </summary>
public class CachingSettings
{
    /// <summary>
    /// </summary>
    /// <value></value>
    public string Mode { get; set; } = "memory";

    /// <summary>
    /// </summary>
    /// <value></value>
    public CachePoolSetting CachePoolSetting { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public CacheDataSetting CacheDataSetting { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public List<RedisCacheSetting> RedisCacheSettings { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public MySqlMemoryCacheSettings MySqlMemoryCacheSettings { get; set; }
}

/// <summary>
/// </summary>
public class CachePoolSetting
{
    /// <summary>
    /// </summary>
    /// <value></value>
    public bool ResetEnable { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public int CleanerIntervalSecond { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public int MaxCapacity { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public int RefreshIntervalMinute { get; set; }
}

/// <summary>
/// </summary>
public class CacheDataSetting
{
    /// <summary>
    /// </summary>
    /// <value></value>
    public int TimeoutInterval { get; set; }
}

/// <summary>
/// </summary>
public class RedisCacheSetting
{
    /// <summary>
    /// </summary>
    /// <value></value>
    public string Name { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public bool IsDefault { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public bool DbActionLog { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string Host { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public int Port { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string Password { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public int DatabaseNo { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public int ConnectTimeout { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public int SyncTimeout { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string ElasticStackLoggerName { get; set; }
}

/// <summary>
/// </summary>
public class MySqlMemoryCacheSettings
{
    /// <summary>
    /// </summary>
    /// <value></value>
    public string DefaultConnectionName { get; set; }
}

/// <summary>
/// </summary>
public class DataAccessSetting
{
    /// <summary>
    /// </summary>
    /// <value></value>
    public string Name { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string DatabaseProvider { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string ConnectionString { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string EncryptKey { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public bool EnableSqlLogProfile { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string ElasticStackLoggerName { get; set; }
}

/// <summary>
/// </summary>
public class TelnetServerSetting
{
    /// <summary>
    /// </summary>
    /// <value></value>
    public bool EnableTelnetServer { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public int TelnetServerPort { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string TelnetServerPassword { get; set; }
}

/// <summary>
/// </summary>
public class AppSetting
{
    /// <summary>
    /// </summary>
    /// <value></value>
    public string Key { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string Value { get; set; }
}

