using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;


namespace ZayniFramework.Common
{
    /// <summary>The configuration manager.
    /// </summary>
    public static class ConfigManager
    {
        #region Private Fields

        /// <summary>The configuration of zayniframework.
        /// </summary>
        private static ZayniFrameworkSettings _zayniFrameworkConfig;

        /// <summary>The configuration of application settings.
        /// </summary>
        private static readonly Dictionary<string, string> _appSettings = [];

        #endregion Private Fields


        #region Public Methods

        /// <summary>Initialize the configuration of zayniframework.
        /// </summary>
        /// <param name="configPath">The path of zayni.json.</param>
        /// <param name="cryptoKey">The encryption key string.</param>
        public static void Init(string configPath, string cryptoKey = null)
        {
            var rawData = File.ReadAllText(configPath, Encoding.UTF8);

            if (cryptoKey.IsNotNullOrEmpty())
            {
                rawData = AesDecryptBase64(rawData, cryptoKey);
            }

            var configSetting = JsonConvert.DeserializeObject<ZayniFrameworkSettingRoot>(rawData);
            _zayniFrameworkConfig = configSetting.ZayniFrameworkSettings;
            configSetting.AppSettings?.ForEach(x => _appSettings.Add(x.Key, x.Value));
        }

        /// <summary>Initialize the configuration of zayniframework.
        /// </summary>
        /// <param name="configPath">The path of zayni.json.</param>
        /// <param name="cryptoKey">The encryption key string.</param>
        public static async Task InitAsync(string configPath, string cryptoKey = null)
        {
            var rawData = await File.ReadAllTextAsync(configPath, Encoding.UTF8);

            if (cryptoKey.IsNotNullOrEmpty())
            {
                rawData = AesDecryptBase64(rawData, cryptoKey);
            }

            var configSetting = JsonConvert.DeserializeObject<ZayniFrameworkSettingRoot>(rawData);
            _zayniFrameworkConfig = configSetting.ZayniFrameworkSettings;
            configSetting.AppSettings?.ForEach(x => _appSettings.Add(x.Key, x.Value));
        }

        /// <summary>取得指定路徑的 json config 組態檔案。<para/>
        /// <para>* 這邊的 configPath 參數，目前不可接受 unix-like 或 linux 作業系統，以 ~/ 字元當作家目錄的路徑。</para>
        /// <para>* 必須要傳入 windows 或 unix-like 作業系統的完整絕對路徑。</para>
        /// <para>* 如果有確實傳入 cryptoKey 金鑰字串，代表 json config 設定檔有被 dotnet zch 加密過。</para>
        /// <para>* 此方法以 Newtonsoft.Json 實作 JSON 字串反序列化。</para>
        /// </summary>
        /// <typeparam name="TConfig">Config 組態的資料泛型</typeparam>
        /// <param name="configPath">Config 組態的完整路徑</param>
        /// <param name="cryptoKey">zch dotnet tool CLI 加密的 AES 金鑰字串。</param>
        /// <returns>強型別的 json config 組態設定</returns>
        public static TConfig GetConfig<TConfig>(string configPath, string cryptoKey = null)
            where TConfig : class
        {
            try
            {
                var rawData = File.ReadAllText(configPath, Encoding.UTF8);

                if (IsValidJson(rawData))
                {
                    return JsonConvert.DeserializeObject<TConfig>(rawData);
                }

                var json = AesDecryptBase64(rawData, cryptoKey);
                return JsonConvert.DeserializeObject<TConfig>(json);
            }
            catch (Exception ex)
            {
                Command.StdoutErr($"Load json config file occur exception. configPath: {configPath} {ex}");
                throw;
            }
        }

        /// <summary>取得指定路徑的 json config 組態檔案。<para/>
        /// <para>* 這邊的 configPath 參數，目前不可接受 unix-like 或 linux 作業系統，以 ~/ 字元當作家目錄的路徑。</para>
        /// <para>* 必須要傳入 windows 或 unix-like 作業系統的完整絕對路徑。</para>
        /// <para>* 如果有確實傳入 cryptoKey 金鑰字串，代表 json config 設定檔有被 dotnet zch 加密過。</para>
        /// <para>* 此方法以 Newtonsoft.Json 實作 JSON 字串反序列化。</para>
        /// </summary>
        /// <param name="configPath">Config 組態的完整路徑</param>
        /// <param name="cryptoKey">zch dotnet tool CLI 加密的 AES 金鑰字串。</param>
        /// <returns>dynamic 型別的 json config 組態設定</returns>
        public static dynamic GetConfig(string configPath, string cryptoKey = null)
        {
            try
            {
                // Pony Says: 這邊的 File.ReadAllText() 方法接受的路徑參數，不可以接受 linux like 作業系統，以 ~/ 字元當作家目錄。
                // 必須要傳入完整的絕對路徑才可以正常取得設定檔。
                var rawData = File.ReadAllText(configPath, Encoding.UTF8);

                if (IsValidJson(rawData))
                {
                    return JsonConvert.DeserializeObject(rawData);
                }

                var json = AesDecryptBase64(rawData, cryptoKey);
                return JsonConvert.DeserializeObject(json);
            }
            catch (Exception ex)
            {
                Command.StdoutErr($"Load json config file occur exception. configPath: {configPath} {ex}");
                throw;
            }
        }

        #endregion Public Methods


        #region Public Getter Methods

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public static ZayniFrameworkSettings GetZayniFrameworkSettings() => _zayniFrameworkConfig;

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, string> GetAppSettings() => _appSettings;

        #endregion Public Getter Methods


        #region Private Methods

        /// <summary>AES 對稱式字串解密
        /// </summary>
        /// <param name="source">解密前字串</param>
        /// <param name="cryptoKey">解密金鑰</param>
        /// <returns>解密後字串</returns>
        private static string AesDecryptBase64(string source, string cryptoKey)
        {
            var decrypt = "";
            var keyString = $"rNduE{cryptoKey}NdjwZtcSk";

            try
            {
                var aes = Aes.Create();
                var md5 = MD5.Create();
                var sha256 = SHA256.Create();

                var key = sha256.ComputeHash(Encoding.UTF8.GetBytes(keyString));
                var iv = md5.ComputeHash(Encoding.UTF8.GetBytes(keyString));

                aes.Key = key;
                aes.IV = iv;

                var dataByteArray = Convert.FromBase64String(source);

                using var ms = new MemoryStream();
                using var cs = new CryptoStream(ms, aes.CreateDecryptor(), CryptoStreamMode.Write);
                cs.Write(dataByteArray, 0, dataByteArray.Length);
                cs.FlushFinalBlock();
                decrypt = Encoding.UTF8.GetString(ms.ToArray());
            }
            catch (Exception ex)
            {
                Command.StdoutErr(ex.ToString());
                throw;
            }

            return decrypt;
        }

        /// <summary>檢查傳入的來源字串是否為合法的 JSON 格式字串<para/>
        /// Reference: https://stackoverflow.com/questions/14977848/how-to-make-sure-that-string-is-valid-json-using-json-net
        /// </summary>
        /// <param name="source">來源字串</param>
        /// <returns>是否為合法的 JSON 格式字串</returns>
        public static bool IsValidJson(string source)
        {
            if (source.IsNullOrEmpty())
            {
                return false;
            }

            source = source.Trim();

            if ((source.StartsWith('{') && source.EndsWith('}')) ||
                (source.StartsWith('[') && source.EndsWith(']')))
            {
                try
                {
                    var obj = JToken.Parse(source);
                    return true;
                }
                catch (JsonReaderException)
                {
                    return false;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        #endregion Private Methods
    }
}
