using System;


namespace ZayniFramework.Common
{
    /// <summary>Zayni Framework 程式例外，代表進行 ORM 資料繫結轉換處理時期發生異常。
    /// </summary>
    public sealed class OrmTransferingException : Exception
    {
        /// <summary>預設建構子
        /// </summary>s
        public OrmTransferingException() : base("An error occur when processing ORM transferinig in ZayniFramework.")
        {
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="message">錯誤訊息</param>
        /// <returns></returns>
        public OrmTransferingException(string message) : base(message)
        {
        }
    }
}