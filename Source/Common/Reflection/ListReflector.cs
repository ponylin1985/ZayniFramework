﻿using System;
using System.Collections.Generic;


namespace ZayniFramework.Common
{
    /// <summary>專門處理 List 相關的反射器
    /// </summary>
    public static class ListReflector
    {
        /// <summary>反射產生指定泛型的 List 型別
        /// </summary>
        /// <param name="typeArgs">List 的泛型型別</param>
        /// <returns>指定泛型的List型別</returns>
        public static Type CreateGenericListType(params Type[] typeArgs)
        {
            Type result = null;
            var listType = typeof(List<>);

            try
            {
                result = listType.MakeGenericType(typeArgs);
            }
            catch (Exception ex)
            {
                Command.StdoutErr(ex.ToString());
                throw;
            }

            return result;
        }

        /// <summary>反射產生指定泛型的 List 物件
        /// </summary>
        /// <param name="typeArgs">List 的泛型型別</param>
        /// <returns>指定泛型的 List 物件</returns>
        public static object CreateGenericList(params Type[] typeArgs)
        {
            var iType = typeof(List<>);

            Type type = null;
            object result = null;

            try
            {
                type = iType.MakeGenericType(typeArgs);
                result = Activator.CreateInstance(type);
            }
            catch (Exception ex)
            {
                Command.StdoutErr(ex.ToString());
                throw;
            }

            return result;
        }

        /// <summary>利用反射將指定的Item加入到目標 List 中，必須要傳入正確的泛型 List 物件，與正確型別的 Item 物件
        /// </summary>
        /// <param name="target">目標 List</param>
        /// <param name="item">指定的項目</param>
        /// <returns>是否成功</returns>
        public static bool AddItemToGenericList(object target, object item)
        {
            var iType = target.GetType();

            try
            {
                var addMethod = iType.GetMethod("Add");
                _ = addMethod.Invoke(target, [item]);
            }
            catch (Exception ex)
            {
                Command.StdoutErr(ex.ToString());
                throw;
            }

            return true;
        }

        /// <summary>取得目標List型別的第一個泛型型別
        /// </summary>
        /// <param name="targetType">目標List型別</param>
        /// <returns>List的第一個泛型型別</returns>
        public static Type GetListFirstGenericType(Type targetType)
        {
            return GetListGenericType(targetType, 0);
        }

        /// <summary>取得目標List型別中指定索引的泛型型別
        /// </summary>
        /// <param name="targetType">目標List型別</param>
        /// <param name="genericIndex">第幾個泛型型別 (不可以小於0)</param>
        /// <returns>List的第幾個泛型型別</returns>
        public static Type GetListGenericType(Type targetType, int genericIndex)
        {
            if (targetType.IsNull())
            {
                return null;
            }

            if (genericIndex < 0)
            {
                return null;
            }

            if (!targetType.IsGenericType)
            {
                return null;
            }

            Type result = null;

            try
            {
                result = targetType.GetGenericArguments()[genericIndex];
            }
            catch (Exception ex)
            {
                Command.StdoutErr(ex.ToString());
                throw;
            }

            return result;
        }
    }
}
