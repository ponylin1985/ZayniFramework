using System;
using System.Linq.Expressions;


namespace ZayniFramework.Common
{
    /// <summary>物件實體 Creator 建立者
    /// </summary>
    public class ObjectCreator
    {
        #region Private Fields

        /// <summary>目標物件的型別
        /// </summary>
        /// <returns></returns>
        private readonly Type _type;

        /// <summary>LINQ Expression 表達式
        /// </summary>
        /// <returns></returns>
        private readonly Expression[] _expression;

        /// <summary>BlockExpression 物件
        /// </summary>
        /// <returns></returns>
        private readonly BlockExpression _block;

        /// <summary>LINQ Expression Lambda 快取物件
        /// </summary>
        /// <returns></returns>
        private readonly Func<object> _builder;

        #endregion Private Fields


        #region Constructor

        /// <summary>多載建構子
        /// </summary>
        /// <param name="type">目標物件型別</param>
        public ObjectCreator(Type type)
        {
            _type = type;
            _expression = [Expression.New(_type)];
            _block = Expression.Block(_type, _expression);
            _builder = Expression.Lambda<Func<object>>(_block).Compile();
        }

        #endregion Constructor


        #region Public Methods

        /// <summary>建立物件實體，以目標型別預設無參數建構子建立物件
        /// </summary>
        /// <returns>物件實體</returns>
        public object CreateInstance() => _builder();

        #endregion Public Methods
    }


    /// <summary>物件實體 Creator 建立者<para/>
    /// </summary>
    /// <reference>* 參考:<para/>
    /// * https://dotblogs.com.tw/code6421/2019/08/15/csharpfastobject?fbclid=IwAR1j8Ndaqs-8AWzOuiwI25g_bSqdxBdtLhXYkut2zRRBVPyh_C_7rFqSijA<para/>
    /// * http://mattgabriel.co.uk/2016/02/10/object-creation-using-lambda-expression/<para/>
    /// </reference>
    /// <typeparam name="TObject">目標物件型別</typeparam>
    public static class ObjectCreator<TObject>
    {
        #region Private Fields

        /// <summary>目標物件的型別
        /// </summary>
        /// <returns></returns>
        private static readonly Type _type;

        /// <summary>LINQ Expression 表達式
        /// </summary>
        /// <returns></returns>
        private static readonly Expression[] _expression;

        /// <summary>BlockExpression 物件
        /// </summary>
        /// <returns></returns>
        private static readonly BlockExpression _block;

        /// <summary>LINQ Expression Lambda 快取物件
        /// </summary>
        /// <returns></returns>
        private static readonly Func<TObject> _builder;

        #endregion Private Fields


        #region Static Constructor

        /// <summary>靜態建構子
        /// </summary>
        static ObjectCreator()
        {
            _type = typeof(TObject);
            _expression = [Expression.New(_type)];
            _block = Expression.Block(_type, _expression);
            _builder = Expression.Lambda<Func<TObject>>(_block).Compile();
        }

        #endregion Static Constructor


        #region Public Methods

        /// <summary>建立物件實體，以目標型別預設無參數建構子建立物件
        /// </summary>
        /// <returns>物件實體</returns>
        public static TObject CreateInstance() => _builder();

        #endregion Public Methods
    }
}