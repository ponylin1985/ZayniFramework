﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;


namespace ZayniFramework.Common
{
    /// <summary>反射器公用類別
    /// </summary>
    public static class Reflector
    {
        #region 處理 List 相關的反射

        /// <summary>反射產生指定泛型的 List 物件
        /// </summary>
        /// <param name="typeArgs">List的泛型型別</param>
        /// <returns>指定泛型的 List 物件</returns>
        public static object CreateGenericList(params Type[] typeArgs) =>
            ListReflector.CreateGenericList(typeArgs);

        /// <summary>利用反射將指定的 Item 加入到目標 List 中，必須要傳入正確的泛型 List 物件，與正確型別的 Item 物件
        /// </summary>
        /// <param name="target">目標 List</param>
        /// <param name="item">指定的項目</param>
        /// <returns>是否成功</returns>
        public static bool AddItemToGenericList(object target, object item) =>
            ListReflector.AddItemToGenericList(target, item);

        #endregion 處理 List 相關的反射


        #region 宣告公開的靜態方法

        //  This function is adapated from: http://www.codeguru.com/forum/showthread.php?t=450171
        //  My thanks to Carl Quirion, for making it "nullable-friendly".
        /// <summary>取得 Nullable 的型別
        /// </summary>
        /// <param name="t">目標型別</param>
        /// <returns>Nullable 的型別</returns>
        public static Type GetNullableType(Type t)
        {
            var result = t;

            if (t.IsGenericType && t.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
            {
                result = Nullable.GetUnderlyingType(t);
            }

            return result;
        }

        /// <summary>檢查傳入的型別是否為 Nullable 型別
        /// </summary>
        /// <param name="type">目標受檢型別</param>
        /// <returns>是否為 Nullable 型別</returns>
        public static bool IsNullableType(Type type)
        {
            return (type == typeof(string) || type.IsArray ||
            (
                type.IsGenericType &&
                type.GetGenericTypeDefinition() == typeof(Nullable<>)
            )
            );
        }

        /// <summary>檢查是否為布林值型別
        /// </summary>
        /// <param name="type">型別</param>
        /// <returns>是否為布林值型別</returns>
        public static bool IsBooleanType(Type type)
        {
            var typeName = type.Name;

            if (IsNullableType(type) && type.IsValueType)
            {
                typeName = GetNullableType(type).Name;
            }

            var isBoolean = 0 == string.Compare(typeName, "Boolean", true);
            return isBoolean;
        }

        /// <summary>嘗試取得目標型別中指定名稱的公開屬性
        /// </summary>
        /// <param name="type">目標型別</param>
        /// <param name="propertyName">屬性名稱</param>
        /// <param name="property">屬性資訊</param>
        /// <returns>是否包含指定的屬性</returns>
        public static bool TryGetProperty(Type type, string propertyName, out PropertyInfo property)
        {
            property = null;

            try
            {
                var properties = type.GetProperties();

                if (!properties.Any(p => propertyName == p.Name))
                {
                    return false;
                }

                property = properties.Where(g => propertyName == g.Name).FirstOrDefault();
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        /// <summary>嘗試取得物件的屬性值
        /// </summary>
        /// <param name="obj">目標物件</param>
        /// <param name="propertyName">目標屬性名稱</param>
        /// <param name="value">輸出屬性值</param>
        /// <returns>是否成功取得目標屬性值</returns>
        public static bool TryGetPropertyValue(object obj, string propertyName, out object value)
        {
            value = null;

            var isSuccess = TryGetProperty(obj.GetType(), propertyName, out var property);

            if (!isSuccess)
            {
                return false;
            }

            try
            {
                value = property.GetValue(obj);
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        /// <summary>取得物件的屬性值
        /// </summary>
        /// <param name="obj">目標物件</param>
        /// <param name="propertyName">目標屬性名稱</param>
        /// <returns>屬性值</returns>
        public static object GetPropertyValue(object obj, string propertyName)
        {
            if (!Reflector.TryGetPropertyValue(obj, propertyName, out var value))
            {
                return null;
            }

            return value;
        }

        /// <summary>取得物件的屬性值
        /// </summary>
        /// <typeparam name="TResult">屬性的泛型</typeparam>
        /// <param name="obj">目標物件</param>
        /// <param name="propertyName">目標屬性名稱</param>
        /// <returns>屬性值</returns>
        public static TResult GetPropertyValue<TResult>(object obj, string propertyName) =>
            (TResult)GetPropertyValue(obj, propertyName);

        // http://technico.qnownow.com/how-to-set-property-value-using-reflection-in-c/
        /// <summary>對目標物件設定屬性值<para/>
        /// 1. 支援各種 Property 的型別，因為使用 Convert.ChangeType() 進行 runtime 轉型。<para/>
        /// 2. 支援 Nullable 的 Value Type 屬性值設地。<para/>
        /// 3. 以 PropertyInfo.SetValue() 反射的機制實作屬性值的設定。
        /// </summary>
        /// <param name="obj">目標物件</param>
        /// <param name="property">目標屬性</param>
        /// <param name="value">屬性值 (資料值)</param>
        public static void SetPropertyValue(object obj, PropertyInfo property, object value)
        {
            var type = obj.GetType();
            var propertyType = property.PropertyType;

            if (value is string stringValue && stringValue.IsNullOrEmpty())
            {
                value = IsItNullableType(property.PropertyType) ? null : Activator.CreateInstance(propertyType);
                property.SetValue(obj, value, null);
            }
            else if (value.IsNotNull())
            {
                var targetType = IsItNullableType(property.PropertyType) ? Nullable.GetUnderlyingType(property.PropertyType) : property.PropertyType;
                value = Convert.ChangeType(value, targetType);
                property.SetValue(obj, value, null);
            }
            else
            {
                if (IsItNullableType(property.PropertyType))
                {
                    value = null;
                }
                else
                {
                    try
                    {
                        // 20180305 Edited by Pony: 為了避免 Non-Nullable 型別的屬性，沒有無參數 public 的預設建構子的情況!
                        value = Activator.CreateInstance(propertyType);
                    }
                    catch (Exception)
                    {
                        // 20180305 Pony Says: 如果傳入的 value 為 Null 值，而且指定的 Property 屬性又不允許為 Null 的情況，又沒有無參數預設建構子
                        return;
                    }
                }

                //value = IsItNullableType( property.PropertyType ) ? null : Activator.CreateInstance( propertyType );
                property.SetValue(obj, value, null);
            }
        }

        // http://technico.qnownow.com/how-to-set-property-value-using-reflection-in-c/
        /// <summary>對目標物件設定屬性值<para/>
        /// 1. 支援各種 Property 的型別，因為使用 Convert.ChangeType() 進行 runtime 轉型。<para/>
        /// 2. 支援 Nullable 的 Value Type 屬性值設地。<para/>
        /// 3. 以 PropertyInfo.SetValue() 反射的機制實作屬性值的設定。
        /// </summary>
        /// <param name="obj">目標物件</param>
        /// <param name="propertyName">屬性名稱</param>
        /// <param name="value">屬性值 (資料值)</param>
        public static void SetPropertyValue(object obj, string propertyName, object value)
        {
            var property = obj.GetType().GetProperty(propertyName);
            SetPropertyValue(obj, property, value);
        }

        /// <summary>嘗試對目標物件設定屬性值<para/>
        /// 1. 支援各種 Property 的型別，因為使用 Convert.ChangeType() 進行 runtime 轉型。<para/>
        /// 2. 支援 Nullable 的 Value Type 屬性值設地。<para/>
        /// 3. 以 PropertyInfo.SetValue() 反射的機制實作屬性值的設定。
        /// </summary>
        /// <param name="obj">目標物件</param>
        /// <param name="propertyName">屬性名稱</param>
        /// <param name="value">屬性值 (資料值)</param>
        /// <param name="message">錯誤訊息</param>
        /// <returns>屬性值設定是否成功</returns>
        public static bool TrySetPropertyValue(object obj, string propertyName, object value, out string message)
        {
            message = null;

            try
            {
                SetPropertyValue(obj, propertyName, value);
            }
            catch (Exception ex)
            {
                message = ex.ToString();
                return false;
            }

            return true;
        }

        /// <summary>檢查是否為 Nullable 型別
        /// </summary>
        /// <param name="type">型別</param>
        /// <returns>是否為 Nullable 型別</returns>
        private static bool IsItNullableType(Type type) =>
            type.IsGenericType && type.GetGenericTypeDefinition().Equals(typeof(Nullable<>));

        /// <summary>反射取得物件的方法資訊
        /// </summary>
        /// <param name="obj">目標物件</param>
        /// <param name="methodName">方法名稱</param>
        /// <returns>方法資訊</returns>
        public static MethodInfo GetMethodInfo(object obj, string methodName) => obj.GetType().GetMethod(methodName);

        /// <summary>以反射的方式呼叫物件的方法
        /// </summary>
        /// <param name="obj">目標物件</param>
        /// <param name="methodName">方法名稱</param>
        /// <param name="parameters">方法的參數陣列</param>
        /// <returns>呼叫方法後的回傳值</returns>
        public static object InvokeMethod(object obj, string methodName, object[] parameters) =>
            obj.GetType().GetMethod(methodName).Invoke(obj, parameters);

        /// <summary>反射取得目標屬性的指定 Attribute 中介資料
        /// </summary>
        /// <typeparam name="TAttribute">目標Attribute的泛型</typeparam>
        /// <param name="propertyInfo">目標屬性</param>
        /// <returns>目標Attribute中介資料</returns>
        public static TAttribute GetCustomAttribute<TAttribute>(PropertyInfo propertyInfo)
            where TAttribute : Attribute
        {
            if (propertyInfo.IsNull())
            {
                return null;
            }

            var attribute = Attribute.GetCustomAttributes(propertyInfo).Where(r => r is TAttribute)?.FirstOrDefault();
            return (TAttribute)attribute;
        }

        /// <summary>反射取得目標型別的指定Attribute中介資料
        /// </summary>
        /// <typeparam name="TAttribute">目標Attribute的泛型</typeparam>
        /// <param name="type">目標型別</param>
        /// <returns>目標Attribute中介資料</returns>
        public static TAttribute GetCustomAttribute<TAttribute>(Type type)
            where TAttribute : Attribute
        {
            TAttribute result = null;

            if (type.IsNull())
            {
                return null;
            }

            var attributes = Attribute.GetCustomAttributes(type);

            foreach (var attribute in attributes)
            {
                if (attribute is TAttribute attribute1)
                {
                    result = attribute1;
                    break;
                }
            }

            return result;
        }

        /// <summary>檢查目標物件是否為一種委派
        /// </summary>
        /// <param name="target">目標物件</param>
        /// <returns>目標物件是否為一種委派</returns>
        public static bool IsItDelegate(object target) => target is Delegate;

        /// <summary>檢查目標動態物件是否為一種委派
        /// </summary>
        /// <param name="target">目標動態物件</param>
        /// <returns>目標動態物件是否為一種委派</returns>
        public static bool IsThatDelegate(dynamic target) => target is Delegate;

        /// <summary>根據來源 MethodInfo 與來源物件，動態的建立這個來源方法的執行時期委派
        /// </summary>
        /// <param name="method">方法資訊物件</param>
        /// <param name="obj">來源物件</param>
        /// <returns>來源方法的執行時期委派</returns>
        public static Delegate CreateDelegate(MethodInfo method, object obj)
        {
            if (new bool[] { method.IsNull(), obj.IsNull(), method.IsStatic, method.IsGenericMethod }.Any(j => j))
            {
                return null;
            }

            Delegate result = null;

            try
            {
                var parameters = method
                    .GetParameters()
                    .Select(p => Expression.Parameter(p.ParameterType, p.Name))
                    .ToArray();

                var instance = Expression.Constant(obj);
                var call = Expression.Call(instance, method, parameters);
                var lambda = Expression.Lambda(call, parameters);
                result = lambda.Compile();
            }
            catch (Exception ex)
            {
                Command.StdoutErr(ex.ToString());
                throw;
            }

            return result;
        }

        /// <summary>檢查目標物件是否為指定父型別的子型態物件
        /// </summary>
        /// <param name="target">目標受檢物件</param>
        /// <param name="type">父型別 Type</param>
        /// <returns>目標物件是否為指定父型別的子型態物件</returns>
        public static bool IsTypeof(object target, Type type)
        {
            if (new object[] { target, type }.HasNullElements())
            {
                return false;
            }

            if (target.GetType().FullName == type.FullName)
            {
                return true;
            }

            if (type.IsInterface)
            {
                return target.GetType().GetInterface(type.Name).IsNotNull();
            }

            return target.GetType().IsSubclassOf(type);
        }

        /// <summary>檢查目標物件是否為指定父型別的子型態物件
        /// </summary>
        /// <param name="target">目標受檢物件</param>
        /// <param name="typeFullName">父型別 Type 完整名稱字串</param>
        /// <returns>目標物件是否為指定父型別的子型態物件</returns>
        public static bool IsTypeof(object target, string typeFullName)
        {
            if (new string[] { target + "", typeFullName }.HasNullElements())
            {
                return false;
            }

            Type type = null;

            try
            {
                type = Type.GetType(typeFullName);
            }
            catch (Exception ex)
            {
                Command.StdoutErr(ex.ToString());
                throw;
            }

            return IsTypeof(target, type);
        }

        /// <summary>檢查目標物件是否為指定父型別的子型態物件
        /// </summary>
        /// <param name="target">目標受檢物件</param>
        /// <param name="assemblyPath">來源組件路徑</param>
        /// <param name="typeFullName">目標型別完整名稱</param>
        /// <returns>目標物件是否為指定父型別的子型態物件</returns>
        public static bool IsTypeof(object target, string assemblyPath, string typeFullName)
        {
            if (new string[] { target + "", assemblyPath, typeFullName }.HasNullElements())
            {
                return false;
            }

            var r = LoadType(assemblyPath, typeFullName);

            if (!r.Success)
            {
                return false;
            }

            return IsTypeof(target, r.Data);
        }

        /// <summary>載入來源組件中的目標型別
        /// </summary>
        /// <param name="assemblyPath">來源組件路徑</param>
        /// <param name="typeFullName">目標型別完整名稱</param>
        /// <returns>載入目標型別結果</returns>
        public static IResult<Type> LoadType(string assemblyPath, string typeFullName)
        {
            #region 初始化回傳值

            var result = Result.Create<Type>();

            #endregion 初始化回傳值

            #region 檢查傳入的引數

            if (new string[] { assemblyPath, typeFullName }.HasNullOrEmptyElemants())
            {
                result.Message = "來源組件路徑或目標型別完整名稱為空字串或Null值，無法進行型別載入。";
                return result;
            }

            #endregion 檢查傳入的引數

            #region 載入組件

            Assembly assembly = null;

            try
            {
                assembly = Assembly.LoadFile(assemblyPath);
            }
            catch (Exception ex)
            {
                result.HasException = true;
                result.ExceptionObject = ex;
                result.Message = $"載入 {assemblyPath} 組件發生程式異常，異常原因: {ex}";
                return result;
            }

            if (assembly.IsNull())
            {
                result.Message = $"載入 {assemblyPath} 組件失敗。";
                return result;
            }

            #endregion 載入組件

            #region 載入型別

            Type type = null;

            try
            {
                type = assembly.GetType(typeFullName);
            }
            catch (Exception ex)
            {
                result.HasException = true;
                result.ExceptionObject = ex;
                result.Message = $"從 {assemblyPath} 組件載入目標型別 {typeFullName} 發生異常，異常原因: {ex}";
                return result;
            }

            if (type.IsNull())
            {
                result.Message = $"載入 {typeFullName} 目標型別失敗";
                return result;
            }

            #endregion 載入型別

            #region 設定回傳值屬性

            result.Data = type;
            result.Success = true;

            #endregion 設定回傳值屬性

            return result;
        }

        /// <summary>載入目標路徑下所有的 dll 組件
        /// </summary>
        /// <param name="path">目標路徑</param>
        /// <param name="assemblyNamePattern">組件搜尋規則</param>
        /// <param name="stopWhenException">當載入 dll 組件發生任何異常時，是否中斷 dll 組件載入的流程，程式預設為 false，代表不中斷。</param>
        /// <returns>載入結果</returns>
        public static IResult<IEnumerable<Assembly>> LoadAssemblies(string path, string assemblyNamePattern = "*.dll", bool stopWhenException = false)
        {
            var result = Result.Create<IEnumerable<Assembly>>();
            var assemblies = new List<Assembly>();

            string[] assemblyPaths;

            try
            {
                assemblyPaths = Directory.GetFiles(path, assemblyNamePattern, SearchOption.TopDirectoryOnly);
            }
            catch (Exception ex)
            {
                result.HasException = true;
                result.ExceptionObject = ex;
                result.Message = $"Get dll assemblies from '{path}' occur exception. {ex}";
                return result;
            }


            if (assemblyPaths.IsNullOrEmpty())
            {
                result.Message = $"Assembly path is null. Can not load assemblies.";
                return result;
            }

            foreach (var assemblyPath in assemblyPaths)
            {
                try
                {
                    var assembly = Assembly.LoadFile(assemblyPath);

                    if (assembly.IsNull())
                    {
                        continue;
                    }

                    assemblies.Add(assembly);
                }
                catch (Exception ex)
                {
                    if (stopWhenException)
                    {
                        result.HasException = true;
                        result.ExceptionObject = ex;
                        result.Message = $"Load all dll assembly from '{assemblyPath}' occur exception. {ex}";
                        return result;
                    }

                    continue;
                }
            }

            result.Data = assemblies;
            result.Success = true;
            return result;
        }

        #endregion 宣告公開的靜態方法
    }
}
