﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;


namespace ZayniFramework.Common.Dynamic
{
    /// <summary>ZayniFramework 框架的動態物件處理專員
    /// </summary>
    public static class DynamicHelper
    {
        #region 宣告公開的靜態方法

        /// <summary>錯誤訊息
        /// </summary>
        public static string ErrorMessage { get; set; }

        #endregion 宣告公開的靜態方法


        #region 宣告公開的靜態方法

        /// <summary>建立動態物件
        /// </summary>
        /// <returns>動態物件</returns>
        public static dynamic CreateDynamicObject()
        {
            try
            {
                return new ExpandoObject();
            }
            catch (Exception ex)
            {
                ErrorMessage = $"An error occured while try to create dynamic ExpandoObject. {ex}";
                Command.StdoutErr(ErrorMessage);
                return null;
            }
        }

        /// <summary>根據傳入的來源物件，建立執行時的動態物件，並且會將來源物件的公開實體屬性、公開的實體方，動態繫結到新建立的動態物件上。呼叫範例:<para/>
        /// </summary>
        /// <remarks>
        /// var model = new<para/>
        /// {<para/>
        ///     Name        = "Amy",<para/>
        ///     Sex         = 2,<para/>
        ///     Birthday    = new DateTime( 1990, 3, 24 ),<para/>
        ///     DoSomething = (Func&lt;string&gt;)( () => $"Hi my name is Amy." )<para/>
        /// };<para/>
        /// dynamic obj = DynamicBroker.CreateDynamicObject( model );<para/>
        /// ============================================<para/>
        /// var source  = new MemberModel() { Name = "Katie" };<para/>
        /// dynamic d   = DynamicBroker.CreateDynamicObject( source );<para/>
        /// </remarks>
        /// <example>
        /// <code>
        /// var model = new
        /// {
        ///     Name        = "Amy",
        ///     Sex         = 2,
        ///     Birthday    = new DateTime( 1990, 3, 24 ),
        ///     DoSomething = (Func&lt;string&gt;)( () => $"Hi my name is Amy." )
        /// };
        /// dynamic obj = DynamicBroker.CreateDynamicObject( model );
        /// ============================================
        /// var source  = new MemberModel() { Name = "Katie" };
        /// dynamic d   = DynamicBroker.CreateDynamicObject( source );
        /// </code>
        /// </example>
        /// <param name="source">來源物件</param>
        /// <returns>動態物件</returns>
        public static dynamic CreateDynamicObject(object source)
        {
            try
            {
                var result = new ExpandoObject();
                var type = source.GetType();

                foreach (var prop in type.GetProperties())
                {
                    if (!BindProperty(result, prop.Name, prop.GetValue(source)))
                    {
                        return null;
                    }
                }

                foreach (var method in type.GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic).Where(m => !m.IsSpecialName))
                {
                    if (!BindMethod(result, method.Name, CreateDelegateFromMethod(method, source)))
                    {
                        return null;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                ErrorMessage = $"An error occured while try to create dynamic ExpandoObject from source object. {ex}";
                Command.StdoutErr(ErrorMessage);
                return null;
            }
        }

        /// <summary>動態新增或修改屬性到目標動態物件
        /// </summary>
        /// <param name="target">目標動態物件</param>
        /// <param name="propertyName">屬性名稱</param>
        /// <param name="propertyValue">屬性值</param>
        /// <returns>是否成功新增屬性到目標動態物件</returns>
        public static bool BindProperty(dynamic target, string propertyName, dynamic propertyValue = null)
        {
            if (null == target)
            {
                return false;
            }

            if (propertyName.IsNullOrEmpty())
            {
                return false;
            }

            try
            {
                var dynamicDict = (IDictionary<string, dynamic>)target;
                dynamicDict[propertyName] = propertyValue;
            }
            catch (Exception ex)
            {
                ErrorMessage = $"An error occured while try to bind property to dynamic ExpandoObject. PropertyName: {propertyName}, PropertyValue: {propertyValue}. {ex}";
                Command.StdoutErr(ErrorMessage);
                return false;
            }

            return true;
        }

        /// <summary>動態移除目標動態物件身上的屬性
        /// </summary>
        /// <param name="target">目標動態物件</param>
        /// <param name="propertyName">屬性名稱</param>
        /// <returns>是否成功移除目標動態物件身上的屬性</returns>
        public static bool UnbindProperty(dynamic target, string propertyName)
        {
            if (null == target)
            {
                return false;
            }

            if (propertyName.IsNullOrEmpty())
            {
                return false;
            }

            IDictionary<string, dynamic> dynamicDict = null;

            try
            {
                dynamicDict = (IDictionary<string, dynamic>)target;
            }
            catch (Exception ex)
            {
                ErrorMessage = $"An error occured while try to unbind property to dynamic ExpandoObject. PropertyName: {propertyName}. {ex}";
                Command.StdoutErr(ErrorMessage);
                return false;
            }

            if (!dynamicDict.ContainsKey(propertyName))
            {
                return true;    // 20131213 Pony Says: 反正這個屬性本來就不存在於這個動態物件身上了，就當作是移除成功了吧
            }

            try
            {
                dynamicDict.Remove(propertyName);
            }
            catch (Exception ex)
            {
                ErrorMessage = $"An error occured while try to unbind property to dynamic ExpandoObject. PropertyName: {propertyName}. {ex}";
                Command.StdoutErr(ErrorMessage);
                return false;
            }

            return true;
        }

        /// <summary>檢查目標動態物件是否包含指定的屬性
        /// </summary>
        /// <param name="target">動態物件</param>
        /// <param name="propertyName">屬性名稱</param>
        /// <returns>目標動態物件是否包含指定的屬性</returns>
        public static bool HasProperty(dynamic target, string propertyName)
        {
            if (null == target)
            {
                return false;
            }

            if (propertyName.IsNullOrEmpty())
            {
                return false;
            }

            IDictionary<string, dynamic> dynamicDict = null;

            try
            {
                dynamicDict = (IDictionary<string, dynamic>)target;
            }
            catch (Exception ex)
            {
                ErrorMessage = $"An error occured while try to check property is existed on the dynamic ExpandoObject. PropertyName: {propertyName}. {ex}";
                Command.StdoutErr(ErrorMessage);
                return false;
            }

            var result = dynamicDict.ContainsKey(propertyName);
            return result;
        }

        /// <summary>動態新增或修改方法到目標動態物件
        /// </summary>
        /// <typeparam name="THandler">委派泛型</typeparam>
        /// <param name="target">目編動態物件</param>
        /// <param name="methodName">方法名稱</param>
        /// <param name="handler">處理委派</param>
        /// <returns>是否成功新增方法到目標動態物件</returns>
        public static bool BindMethod<THandler>(dynamic target, string methodName, THandler handler)
        {
            if (null == target)
            {
                return false;
            }

            if (methodName.IsNullOrEmpty())
            {
                return false;
            }

            try
            {
                var dynamicDict = (IDictionary<string, dynamic>)target;
                dynamicDict[methodName] = (THandler)(handler);
            }
            catch (Exception ex)
            {
                ErrorMessage = $"An error occured while try to bind delegate to the dynamic ExpandoObject. MethodName: {methodName}. {ex}";
                Command.StdoutErr(ErrorMessage);
                return false;
            }

            return true;
        }

        /// <summary>動態新增或修改方法到目標動態物件
        /// </summary>
        /// <param name="target">目編動態物件</param>
        /// <param name="methodName">方法名稱</param>
        /// <param name="handler">處理委派</param>
        /// <returns>是否成功新增方法到目標動態物件</returns>
        public static bool BindMethod(dynamic target, string methodName, Delegate handler)
        {
            if (null == target)
            {
                return false;
            }

            if (methodName.IsNullOrEmpty())
            {
                return false;
            }

            try
            {
                var dynamicDict = (IDictionary<string, dynamic>)target;
                dynamicDict[methodName] = handler;
            }
            catch (Exception ex)
            {
                ErrorMessage = $"An error occured while try to bind delegate to the dynamic ExpandoObject. MethodName: {methodName}. {ex}";
                Command.StdoutErr(ErrorMessage);
                return false;
            }

            return true;
        }

        /// <summary>動態移除目標動態物件身上的方法
        /// </summary>
        /// <param name="target">目標動態物件</param>
        /// <param name="methodName">方法名稱</param>
        /// <returns>是否成功移除目標動態物件身上的方法</returns>
        public static bool UnbindMethod(dynamic target, string methodName)
        {
            bool result = UnbindProperty(target, methodName);
            return result;
        }

        /// <summary>檢查目標動態物件是否包含指定的方法
        /// </summary>
        /// <param name="target">動態物件</param>
        /// <param name="methodName">方法名稱</param>
        /// <returns>目標動態物件是否包含指定的方法</returns>
        public static bool HasMethod(dynamic target, string methodName)
        {
            bool result = HasProperty(target, methodName);
            return result;
        }

        /// <summary>對目標動態物件執行迭代
        /// * 在迭代帶動作中，仍然無法修改成員的值
        /// </summary>
        /// <param name="target">目標動態物件</param>
        /// <param name="handler">對動態物件的迭代委派</param>
        public static void ForEach(dynamic target, DynamicEachHandler handler)
        {
            if (new object[] { target, handler }.Any(r => r.IsNull()))
            {
                return;
            }

            IDictionary<string, dynamic> dynamicDict = null;

            try
            {
                dynamicDict = (IDictionary<string, dynamic>)target;
            }
            catch (Exception ex)
            {
                ErrorMessage = $"An error occured while try to iterate on the dynamic ExpandoObject. {ex}";
                Command.StdoutErr(ErrorMessage);
                return;
            }

            foreach (var member in dynamicDict)
            {
                var name = member.Key;
                dynamic value = member.Value;

                try
                {
                    handler(name, value);
                }
                catch (Exception ex)
                {
                    ErrorMessage = $"An error occured while try to iterate on the dynamic ExpandoObject. {ex}";
                    Command.StdoutErr(ErrorMessage);
                    return;
                }
            }
        }

        /// <summary>Builds a Delegate instance from the supplied MethodInfo object and a target to invoke against.<para/>
        /// Reference:<para/>
        /// * http://stackoverflow.com/questions/1124563/builds-a-delegate-from-methodinfo
        /// </summary>
        /// <param name="method">The source method info.</param>
        /// <param name="target">The target object.</param>
        /// <returns>The delegate instance of the method info.</returns>
        private static Delegate CreateDelegateFromMethod(MethodInfo method, object target)
        {
            ArgumentNullException.ThrowIfNull(method);

            Type delegateType;

            var typeArgs =
                method
                    .GetParameters()
                    .Select(p => p.ParameterType)
                    .ToList();

            // builds a delegate type
            if (method.ReturnType == typeof(void))
            {
                delegateType = Expression.GetActionType(typeArgs.ToArray());
            }
            else
            {
                typeArgs.Add(method.ReturnType);
                delegateType = Expression.GetFuncType(typeArgs.ToArray());
            }

            // creates a binded delegate if target is supplied
            var result = null == target
                ? Delegate.CreateDelegate(delegateType, method)
                : Delegate.CreateDelegate(delegateType, target, method);

            return result;
        }

        #endregion 宣告公開的靜態方法
    }
}
