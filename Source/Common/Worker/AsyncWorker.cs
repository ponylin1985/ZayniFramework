using NeoSmart.AsyncLock;
using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;


namespace ZayniFramework.Common.Tasks
{
    /// <summary>非同步工作佇列處理器
    /// </summary>
    public class AsyncWorker
    {
        #region Private Fields

        /// <summary>Enable 的非同步作業鎖定物件
        /// </summary>
        /// <returns></returns>
        private readonly AsyncLock _asyncLock = new();

        /// <summary>Enqueue 的非同步作業鎖定物件
        /// </summary>
        /// <returns></returns>
        private readonly AsyncLock _asyncEnqueueLock = new();

        /// <summary>Dequeue 的非同步作業鎖定物件
        /// </summary>
        /// <returns></returns>
        private readonly AsyncLock _asynDequeueLock = new();

        /// <summary>非同步作業工作佇列
        /// </summary>
        /// <returns></returns>
        private readonly BlockingCollection<Func<Task>> _queue = [];

        /// <summary>工作處理器執行緒
        /// </summary>
        /// <returns></returns>
        private Thread _consumer;

        /// <summary>是否持續處理工作
        /// </summary>
        private bool _enable = true;

        /// <summary>Runner 執行時間間隔，單位為 ms 毫秒數，不可小於或等於 0。
        /// </summary>
        private readonly int _interval = 3;

        /// <summary>工作處理器 Worker 名稱
        /// </summary>
        private readonly string _name;

        #endregion Private Fields


        #region Constructors

        /// <summary>多載建構子
        /// </summary>
        /// <param name="interval">Runner 執行時間間隔，單位為 ms 毫秒數，不可小於或等於 0</param>
        /// <param name="name">工作處理器 Worker 名稱</param>
        internal AsyncWorker(int interval = 3, string name = null)
        {
            ArgumentOutOfRangeException.ThrowIfNegativeOrZero(interval);

            _interval = interval;
            _name = name;
        }

        #endregion Constructors


        #region Public Methods

        /// <summary>加入工作任務到 AsyncWorker 的工作佇列中
        /// </summary>
        /// <param name="asyncFunc">非同步作業工作委派，回傳值必須為 Task 非同步作業任務。</param>
        /// <returns>執行結果</returns>
        public IResult<int> Enqueue(Func<Task> asyncFunc)
        {
            using (_asyncEnqueueLock.Lock())
            {
                try
                {
                    var success = _queue.TryAdd(asyncFunc, 2);
                    return Result.Create<int>(success, _queue.Count);
                }
                catch (Exception ex)
                {
                    return Result.Create<int>(false, message: $"Start {nameof(AsyncWorker)} occur exception. {ex}");
                }
            }
        }

        /// <summary>啟動 AsyncWorker 非同步工作處理器
        /// </summary>
        /// <returns>啟動結果</returns>
        public IResult Start()
        {
            using (_asyncLock.Lock())
            {
                try
                {
                    if (_enable && _consumer.IsNotNull())
                    {
                        return Result.Create(true);
                    }

                    _consumer = new Thread(async () => await RunAsync());
                    _consumer.Start();
                    _enable = true;
                }
                catch (Exception ex)
                {
                    return Result.Create(false, message: $"Start {nameof(AsyncWorker)} occur exception. {ex}");
                }
            }

            return Result.Create(true);
        }

        /// <summary>關閉 AsyncWorker 非同步工作處理器
        /// </summary>
        /// <returns>關閉結果</returns>
        public IResult Stop()
        {
            using (_asyncLock.Lock())
            {
                try
                {
                    _enable = false;
                    _consumer = null;
                }
                catch (Exception ex)
                {
                    return Result.Create(false, message: $"Stop {nameof(AsyncWorker)} occur exception. {ex}");
                }
            }

            return Result.Create(true);
        }

        #endregion Public Methods


        #region Private Methods

        /// <summary>執行非同步作業工作內容
        /// </summary>
        private async Task RunAsync()
        {
            while (true)
            {
                if (!_enable)
                {
                    break;
                }

                using (await _asynDequeueLock.LockAsync())
                {
                    try
                    {
                        // BlockingCollection.Take 方法會自動 Block 住目前的 Thread，因此不需要特別 Thread.Sleep() 或 SpinWait.SpinUtil()，反而更有效率並且節省 CPU 使用量。
                        var task = _queue.Take();

                        if (task.IsNull())
                        {
                            return;
                        }

                        await Task.Yield();
                        await task.Invoke();
                    }
                    catch (Exception ex)
                    {
                        await Command.StdoutErrAsync($"{nameof(AsyncWorker)} execute action occur exception. AsyncWorker Name: {_name}. {Environment.NewLine}{ex}");
                        await Task.Delay(5);
                        continue;
                    }
                }
            }
        }

        #endregion Private Methods
    }
}