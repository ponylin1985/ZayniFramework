using NeoSmart.AsyncLock;
using System;
using System.Collections.Generic;


namespace ZayniFramework.Common.Tasks
{
    /// <summary>非同步工作佇列管理容器
    /// </summary>
    public static class AsyncWorkerContainer
    {
        #region Private Fields

        /// <summary>非同步作業鎖定物件
        /// </summary>
        /// <returns></returns>
        private static readonly AsyncLock _asyncLock = new();

        /// <summary>非同步工作容器
        /// </summary>
        /// <returns></returns>
        private static readonly Dictionary<string, AsyncWorker> _container = [];

        #endregion Private Fields


        #region Static Constructor

        /// <summary>靜態建構子
        /// </summary>
        static AsyncWorkerContainer()
        {
            Resolve("ZayniFrameworkLoggerAsyncWorker");
            Resolve("ZayniFrameworkMiddleServiceHostLoggerAsyncWorker");
        }

        #endregion Static Constructor


        #region Public Methods

        /// <summary>註冊或取得非同步作業處理器。<para/>
        /// * 如果指定名稱的非同步作業處理器，尚未註冊進管理容器，則會自動註冊盡管理容器中並且回傳。<para/>
        /// * 如果指定名稱的非同步作業處理器，已經註冊於管理容器中，則會直接從管理容器中取出並且回傳。
        /// </summary>
        /// <param name="name">非同步作業工作處理器的設定名稱</param>
        /// <param name="interval">執行時間間隔，單位為 ms 毫秒數，不可小於或等於 0</param>
        /// <returns>非同步作業處理器實體</returns>
        public static AsyncWorker Resolve(string name, int interval = 3)
        {
            using (_asyncLock.Lock())
            {
                try
                {
                    if (name.IsNullOrEmpty())
                    {
                        var errorMsg = $"Resolve ${nameof(AsyncWorker)} from ${nameof(AsyncWorkerContainer)} fail. The '{nameof(name)}' argument can not be null or empty string.";
                        Command.StdoutErr(errorMsg);
                        throw new ArgumentNullException(errorMsg);
                    }

                    if (_container.TryGetValue(name, out var value))
                    {
                        return value;
                    }

                    var worker = new AsyncWorker(name: name, interval: interval);
                    _container.Add(name, worker);
                    return worker;
                }
                catch (Exception ex)
                {
                    var errorMsg = $"An exception occur while resolve ${nameof(AsyncWorker)} from ${nameof(AsyncWorkerContainer)}. Name: {name}. {Environment.NewLine}{ex}";
                    Command.StdoutErr(errorMsg);
                    throw;
                }
            }
        }

        #endregion Public Methods
    }
}