using System;
using System.Collections.Concurrent;
using System.Threading;


namespace ZayniFramework.Common.Tasks
{
    /// <summary>工作佇列處理器
    /// </summary>
    public class Worker
    {
        #region Private Fields

        /// <summary>Enable 的多執行緒鎖定物件
        /// </summary>
        /// <returns></returns>
        private readonly object _lockThiz = new();

        /// <summary>Enqueue 的多執行緒鎖定物件
        /// </summary>
        /// <returns></returns>
        private readonly object _lockEnqueue = new();

        /// <summary>Dequeue 的多執行緒鎖定物件
        /// </summary>
        /// <returns></returns>
        private readonly object _lockDequeue = new();

        /// <summary>工作佇列集合
        /// </summary>
        /// <returns></returns>
        private readonly BlockingCollection<Action> _queue = [];

        /// <summary>工作處理器執行緒
        /// </summary>
        /// <returns></returns>
        private Thread _consumer;

        /// <summary>是否持續處理工作
        /// </summary>
        private bool _enable = true;

        /// <summary>Runner 執行時間間隔，單位為 ms 毫秒數，不可小於或等於 0。
        /// </summary>
        private readonly int _interval = 3;

        /// <summary>工作處理器 Worker 名稱
        /// </summary>
        private readonly string _name;

        #endregion Private Fields


        #region Constructors

        /// <summary>多載建構子
        /// </summary>
        /// <param name="interval">Runner 執行時間間隔，單位為 ms 毫秒數，不可小於或等於 0</param>
        /// <param name="name">工作處理器 Worker 名稱</param>
        internal Worker(int interval = 3, string name = null)
        {
            ArgumentOutOfRangeException.ThrowIfNegativeOrZero(interval);

            _interval = interval;
            _name = name;
        }

        #endregion Constructors


        #region Public Methods

        /// <summary>加入工作任務到 Worker 的工作佇列中
        /// </summary>
        /// <param name="action">工作內容委派</param>
        /// <returns>執行結果</returns>
        public IResult<int> Enqueue(Action action)
        {
            lock (_lockEnqueue)
            {
                try
                {
                    var success = _queue.TryAdd(action, 2);
                    return Result.Create<int>(success, _queue.Count);
                }
                catch (Exception ex)
                {
                    return Result.Create<int>(false, message: $"Start {nameof(Worker)} occur exception. {ex}");
                }
            }
        }

        /// <summary>啟動 Worker 非同步工作處理器
        /// </summary>
        /// <returns>啟動結果</returns>
        public IResult Start()
        {
            lock (_lockThiz)
            {
                try
                {
                    if (_enable && _consumer.IsNotNull())
                    {
                        return Result.Create(true);
                    }

                    _enable = true;
                    _consumer = new Thread(() => Run());
                    _consumer.Start();
                }
                catch (Exception ex)
                {
                    return Result.Create(false, message: $"Start {nameof(Worker)} occur exception. {ex}");
                }

                return Result.Create(true);
            }
        }

        /// <summary>關閉 Worker 非同步工作處理器
        /// </summary>
        /// <returns>關閉結果</returns>
        public IResult Stop()
        {
            lock (_lockThiz)
            {
                try
                {
                    _enable = false;
                    _consumer = null;
                }
                catch (Exception ex)
                {
                    return Result.Create(false, message: $"Stop {nameof(Worker)} occur exception. {ex}");
                }

                return Result.Create(true);
            }
        }

        #endregion Public Methods


        #region Private Methods

        /// <summary>執行工作內容
        /// </summary>
        private void Run()
        {
            while (true)
            {
                if (!_enable)
                {
                    break;
                }

                lock (_lockDequeue)
                {
                    try
                    {
                        // BlockingCollection.Take 方法會自動 Block 住目前的 Thread，因此不需要特別 Sleep，反而更有效率並且節省 CPU 使用量。
                        _queue.Take()?.Invoke();
                    }
                    catch (Exception ex)
                    {
                        var errorMsg = $"{nameof(Worker)} execute action occur exception. Worker Name: {_name}. {Environment.NewLine}{ex}";
                        Command.StdoutErr(errorMsg);
                        SpinWait.SpinUntil(() => false, 3);
                        continue;
                    }
                }
            }
        }

        #endregion Private Methods
    }
}