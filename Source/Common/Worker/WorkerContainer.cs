using NeoSmart.AsyncLock;
using System;
using System.Collections.Generic;


namespace ZayniFramework.Common.Tasks
{
    /// <summary>工作佇列管理容器
    /// </summary>
    public static class WorkerContainer
    {
        #region Private Fields

        /// <summary>非同步作業鎖定物件
        /// </summary>
        /// <returns></returns>
        private static readonly AsyncLock _asyncLock = new();

        /// <summary>非同步工作容器
        /// </summary>
        /// <returns></returns>
        private static readonly Dictionary<string, Worker> _container = [];

        #endregion Private Fields


        #region Public Methods

        /// <summary>註冊或取得工作佇列。<para/>
        /// * 如果指定名稱的工作佇列，尚未註冊進管理容器，則會自動註冊盡管理容器中並且回傳。<para/>
        /// * 如果指定名稱的工作佇列，已經註冊於管理容器中，則會直接從管理容器中取出並且回傳。
        /// </summary>
        /// <param name="name">工作佇列的設定名稱</param>
        /// <param name="interval">執行時間間隔，單位為 ms 毫秒數，不可小於或等於 0</param>
        /// <returns>工作佇列實體</returns>
        public static Worker Resolve(string name, int interval = 3)
        {
            using (_asyncLock.Lock())
            {
                try
                {
                    if (name.IsNullOrEmpty())
                    {
                        var errorMsg = $"Resolve ${nameof(Worker)} from ${nameof(WorkerContainer)} fail. The '{nameof(name)}' argument can not be null or empty string.";
                        Command.StdoutErr(errorMsg);
                        throw new ArgumentNullException(errorMsg);
                    }

                    if (_container.TryGetValue(name, out var value))
                    {
                        return value;
                    }

                    var worker = new Worker(name: name, interval: interval);
                    _container.Add(name, worker);
                    return worker;
                }
                catch (Exception ex)
                {
                    var errorMsg = $"An exception occur while resolve ${nameof(Worker)} from ${nameof(WorkerContainer)}. Name: {name}. {Environment.NewLine}{ex}";
                    Command.StdoutErr(errorMsg);
                    throw;
                }
            }
        }

        #endregion Public Methods
    }
}