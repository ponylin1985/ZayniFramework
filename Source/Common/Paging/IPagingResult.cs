﻿using System.Collections.Generic;


namespace ZayniFramework.Common
{
    /// <summary>分頁處理結果介面
    /// </summary>
    /// <typeparam name="TModel">資料模型泛型</typeparam>
    public interface IPagingResult<TModel>
    {
        /// <summary>資料總筆數
        /// </summary>
        int TotalCount { get; set; }

        /// <summary>分頁處理過後的資料集合
        /// </summary>
        IEnumerable<TModel> PagedData { get; set; }
    }
}
