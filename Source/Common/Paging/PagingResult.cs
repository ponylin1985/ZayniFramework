﻿using System.Collections.Generic;


namespace ZayniFramework.Common
{
    /// <summary>分頁處理結果
    /// </summary>
    /// <typeparam name="TModel">資料模型泛型</typeparam>
    public abstract class PagingResult<TModel> : IPagingResult<TModel>
    {
        /// <summary>資料總筆數
        /// </summary>
        public int TotalCount { get; set; }

        /// <summary>分頁處理過後的資料集合
        /// </summary>
        public IEnumerable<TModel> PagedData { get; set; }

        /// <summary>未進行分頁處理的原始資料集合
        /// </summary>
        public IEnumerable<TModel> SourceData { get; set; }
    }
}
