﻿namespace ZayniFramework.Common
{
    /// <summary>分頁處理選項
    /// </summary>
    public abstract class PagingOption : IPaging
    {
        /// <summary>資料快取ID值
        /// </summary>
        public string DataCacheId { get; set; }

        /// <summary>目前要從第幾頁開始進行查詢
        /// </summary>
        public int Page { get; set; }

        /// <summary>每一個分頁要取幾筆資料
        /// </summary>
        public int Take { get; set; }

        /// <summary>是否要強制重新取得資料
        /// </summary>
        public bool Refresh { get; set; }
    }
}
