﻿namespace ZayniFramework.Common
{
    /// <summary>可以進行分頁的資料模型介面合約
    /// </summary>
    public interface IPaging
    {
        /// <summary>目前要從第幾頁開始進行分頁
        /// </summary>
        int Page { get; set; }

        /// <summary>每一個分頁要取幾筆資料
        /// </summary>
        int Take { get; set; }

        /// <summary>是否要強制重新取得資料
        /// </summary>
        bool Refresh { get; set; }
    }
}
