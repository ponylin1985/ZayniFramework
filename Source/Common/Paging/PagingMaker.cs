﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace ZayniFramework.Common
{
    /// <summary>資料分頁處理器
    /// </summary>
    public static class PagingMaker
    {
        /// <summary>執行資料分頁
        /// </summary>
        /// <typeparam name="TModel">資料模型泛型</typeparam>
        /// <param name="models">資料模型集合</param>
        /// <param name="page">目前要從第幾頁開始進行分頁</param>
        /// <param name="take">每一個分頁要取幾筆資料</param>
        /// <returns>分頁過後的資料模型集合</returns>
        public static IEnumerable<TModel> Paging<TModel>(IEnumerable<TModel> models, int page, int take)
        {
            var result = Enumerable.Empty<TModel>();

            try
            {
                result = models.Skip((page - 1) * take).Take(take);
            }
            catch (Exception ex)
            {
                Command.StdoutErr(ex.ToString());
                throw;
            }

            return result;
        }

        /// <summary>執行資料分頁
        /// </summary>
        /// <typeparam name="TModel">資料模型泛型</typeparam>
        /// <typeparam name="TResult">分頁結果資料模型泛型</typeparam>
        /// <param name="models">資料模型集合</param>
        /// <param name="page">目前要從第幾頁開始進行分頁</param>
        /// <param name="take">每一個分頁要取幾筆資料</param>
        /// <returns>分頁處理結果</returns>
        public static TResult Paging<TModel, TResult>(IEnumerable<TModel> models, int page, int take)
            where TResult : IPagingResult<TModel>
        {
            var result = default(TResult);
            var data = Enumerable.Empty<TModel>();

            try
            {
                data = models.Skip((page - 1) * take).Take(take).ToList();
            }
            catch (Exception ex)
            {
                Command.StdoutErr(ex.ToString());
                throw;
            }

            result = Activator.CreateInstance<TResult>();
            result.TotalCount = models.Count();
            result.PagedData = data;
            return result;
        }

        /// <summary>執行資料分頁 (如果有大量資料要進行分頁，建議呼叫 PagingAsync 方法)
        /// </summary>
        /// <typeparam name="TModel">資料模型泛型</typeparam>
        /// <param name="models">資料模型集合</param>
        /// <param name="pagingInfo">分頁處理資訊</param>
        /// <returns>分頁過後的資料模型集合</returns>
        public static IEnumerable<TModel> Paging<TModel>(IEnumerable<TModel> models, IPaging pagingInfo)
        {
            var result = Enumerable.Empty<TModel>();

            try
            {
                result = models.Skip((pagingInfo.Page - 1) * pagingInfo.Take).Take(pagingInfo.Take);
            }
            catch (Exception ex)
            {
                Command.StdoutErr(ex.ToString());
                throw;
            }

            return result;
        }

        /// <summary>執行資料分頁
        /// </summary>
        /// <typeparam name="TModel">資料模型泛型</typeparam>
        /// <typeparam name="TResult">分頁結果資料模型泛型</typeparam>
        /// <param name="models">資料模型集合</param>
        /// <param name="pagingInfo">分頁處理資訊</param>
        /// <returns>分頁處理結果</returns>
        public static TResult Paging<TModel, TResult>(IEnumerable<TModel> models, IPaging pagingInfo)
            where TResult : IPagingResult<TModel>
        {
            var result = default(TResult);
            var data = Enumerable.Empty<TModel>();

            try
            {
                data = models.Skip((pagingInfo.Page - 1) * pagingInfo.Take).Take(pagingInfo.Take).ToList();
            }
            catch (Exception ex)
            {
                Command.StdoutErr(ex.ToString());
                throw;
            }

            result = Activator.CreateInstance<TResult>();
            result.TotalCount = models.Count();
            result.PagedData = data;
            return result;
        }
    }
}
