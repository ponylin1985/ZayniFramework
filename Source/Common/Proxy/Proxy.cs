using System;
using System.Threading;
using System.Threading.Tasks;


namespace ZayniFramework.Common
{
    /// <summary>動作代理人
    /// </summary>
    public class Proxy
    {
        #region Public Properties

        /// <summary>目標動作委派
        /// </summary>
        /// <value></value>
        public Delegate Action { get; set; }

        /// <summary>呼叫前的處理委派
        /// </summary>
        /// <value></value>
        public Func<object[], object[]> BeforeInvokeHandler { get; set; }

        #endregion Public Properties


        #region Public Static Methdos

        /// <summary>建立代理人物件
        /// </summary>
        /// <returns>代理人物件</returns>
        public static Proxy Create() => new();

        /// <summary>建立代理人物件
        /// </summary>
        /// <param name="action">目標動作委派</param>
        /// <param name="beforeInvoke">呼叫前的處理委派</param>
        /// <returns>代理人物件</returns>
        public static Proxy Create(Delegate action, Func<object[], object[]> beforeInvoke = null) =>
            new() { Action = action, BeforeInvokeHandler = beforeInvoke };

        /// <summary>建立代理人物件
        /// </summary>
        /// <typeparam name="TReturn">目標動作回傳值的泛型</typeparam>
        /// <returns>代理人物件</returns>
        public static Proxy<TReturn> Create<TReturn>() => new();

        /// <summary>建立代理人物件
        /// </summary>
        /// <param name="action">目標動作委派</param>
        /// <param name="beforeInvoke">呼叫前的處理委派</param>
        /// <param name="afterInvoke">呼叫後的處理委派</param>
        /// <typeparam name="TReturn">目標動作回傳值的泛型</typeparam>
        /// <returns>代理人物件</returns>
        public static Proxy<TReturn> Create<TReturn>(Delegate action, Func<object[], object[]> beforeInvoke = null, Func<object[], TReturn, TReturn> afterInvoke = null) =>
            new() { Action = action, BeforeInvokeHandler = beforeInvoke, AfterInvokeHandler = afterInvoke };

        #endregion Public Static Methdos


        #region Public Methods

        /// <summary>呼叫執行目標動作
        /// </summary>
        /// <param name="parameters">目標動作委派的執行時期參數</param>
        /// <returns>目標動作的回傳值</returns>
        public void Invoke(params object[] parameters)
        {
            BeforeInvokeHandler.IsNotNull(b => parameters = BeforeInvokeHandler.Invoke(parameters));
            DelegateInvoker.Exec(Action, parameters);
        }

        /// <summary>呼叫執行目標動作
        /// </summary>
        /// <param name="retryOption">重新嘗試選項</param>
        /// <param name="parameters">目標動作委派的執行時期參數</param>
        /// <returns>目標動作的回傳值</returns>
        public void Invoke(IRetryOption retryOption, params object[] parameters)
        {
            if (retryOption.IsNull() || retryOption?.EnableRetry == false || retryOption.RetryFrequencies.IsNullOrEmpty())
            {
                Execute();
                return;
            }

            if (retryOption.RetryFrequencies.Length > 5)
            {
                throw new ArgumentOutOfRangeException($"Invalid argument. The value of '{nameof(IRetryOption.RetryFrequencies)}' is out of range.");
            }

            var lastException = default(Exception);

            foreach (var retryInterval in retryOption.RetryFrequencies)
            {
                try
                {
                    Execute();
                }
                catch (Exception ex)
                {
                    lastException = ex;
                    SpinWait.SpinUntil(() => false, retryInterval);
                    continue;
                }

                return;
            }

            void Execute()
            {
                BeforeInvokeHandler.IsNotNull(b => parameters = BeforeInvokeHandler.Invoke(parameters));
                DelegateInvoker.Exec(Action, parameters);
            }
        }

        #endregion Public Methods


        #region Public Static Methods

        /// <summary>執行目標動作委派。<para/>
        /// * 假若有指定 `beforeAction` 或 `afterAction` 前處理或後處理委派，並且有指定要執行 retry 機制，則會在每一次 retry 執行時，都呼叫 beforeAction 和 afterAction 委派。
        /// </summary>
        /// <param name="action">目標動作委派</param>
        /// <param name="beforeAction">目標動作前處理委派</param>
        /// <param name="afterAction">目標動作後處理委派</param>
        /// <param name="retryOption">重新嘗試選項</param>
        public static void Invoke(
            Action action,
            Action beforeAction = null,
            Action afterAction = null,
            IRetryOption retryOption = null)
        {
            if (retryOption.IsNull() || retryOption?.EnableRetry == false || retryOption.RetryFrequencies.IsNullOrEmpty())
            {
                Execute();
                return;
            }

            if (retryOption.RetryFrequencies.Length > 5)
            {
                throw new ArgumentOutOfRangeException($"Invalid argument. The value of '{nameof(IRetryOption.RetryFrequencies)}' is out of range.");
            }

            var lastException = default(Exception);

            foreach (var retryInterval in retryOption.RetryFrequencies)
            {
                try
                {
                    Execute();
                }
                catch (Exception ex)
                {
                    lastException = ex;
                    SpinWait.SpinUntil(() => false, retryInterval);
                    continue;
                }

                return;
            }

            void Execute()
            {
                beforeAction?.Invoke();
                action();
                afterAction?.Invoke();
            }
        }

        /// <summary>執行目標動作委派。<para/>
        /// * 假若有指定 `beforeAction` 或 `afterAction` 前處理或後處理委派，並且有指定要執行 retry 機制，則會在每一次 retry 執行時，都呼叫 beforeAction 和 afterAction 委派。
        /// </summary>
        /// <param name="asyncFunc">目標動作委派</param>
        /// <param name="beforeFunc">目標動作前處理委派</param>
        /// <param name="afterFunc">目標動作後處理委派</param>
        /// <param name="retryOption">重新嘗試選項</param>
        public static async Task InvokeAsync(
            Func<Task> asyncFunc,
            Func<Task> beforeFunc = null,
            Func<Task> afterFunc = null,
            IRetryOption retryOption = null)
        {
            if (retryOption.IsNull() || retryOption?.EnableRetry == false || retryOption.RetryFrequencies.IsNullOrEmpty())
            {
                await ExecuteAsync();
                return;
            }

            if (retryOption.RetryFrequencies.Length > 5)
            {
                throw new ArgumentOutOfRangeException($"Invalid argument. The value of '{nameof(IRetryOption.RetryFrequencies)}' is out of range.");
            }

            var lastException = default(Exception);

            foreach (var retryInterval in retryOption.RetryFrequencies)
            {
                try
                {
                    await ExecuteAsync();
                }
                catch (Exception ex)
                {
                    lastException = ex;
                    SpinWait.SpinUntil(() => false, retryInterval);
                    continue;
                }

                return;
            }

            async Task ExecuteAsync()
            {
                if (beforeFunc.IsNotNull())
                {
                    await beforeFunc();
                }

                await asyncFunc();

                if (afterFunc.IsNotNull())
                {
                    await afterFunc.Invoke();
                }
            }
        }

        /// <summary>執行目標動作委派，並且回傳動作的結果。<para/>
        /// * 假若有指定 `beforeAction` 或 `afterFunc` 前處理或後處理委派，並且有指定要執行 retry 機制，則會在每一次 retry 執行時，都呼叫 beforeAction 和 afterFunc 委派。
        /// </summary>
        /// <param name="func">目標動作委派</param>
        /// <param name="beforeAction">目標動作前處理委派</param>
        /// <param name="afterFunc">目標動作後處理委派</param>
        /// <param name="retryOption">重新嘗試選項</param>
        /// <param name="retryWhen">重新進行 retry 執行目標動作的條件</param>
        /// <param name="returnWhenRetryFail">當 retry 執行全部失敗後，最終決定回傳的處理委派</param>
        /// <typeparam name="TReturn">目標動作回傳值的泛型</typeparam>
        /// <returns>目標動作的回傳值</returns>
        public static TReturn Invoke<TReturn>(
            Func<TReturn> func,
            IRetryOption retryOption = null,
            Action beforeAction = null,
            Func<TReturn, TReturn> afterFunc = null,
            Func<TReturn, bool> retryWhen = null,
            Func<TReturn, TReturn> returnWhenRetryFail = null)
        {
            if (retryOption.IsNull() || retryOption?.EnableRetry == false || retryOption.RetryFrequencies.IsNullOrEmpty())
            {
                return Execute();
            }

            if (retryOption.RetryFrequencies.Length > 5)
            {
                throw new ArgumentOutOfRangeException($"Invalid argument. The value of '{nameof(IRetryOption.RetryFrequencies)}' is out of range.");
            }

            var retryTimes = 0;
            var lastException = default(Exception);
            var lastResult = default(TReturn);

            while (true)
            {
                if (retryTimes >= retryOption.RetryFrequencies.Length)
                {
                    if (returnWhenRetryFail.IsNotNull())
                    {
                        return returnWhenRetryFail(lastResult);
                    }

                    if (lastException.IsNotNull())
                    {
                        throw lastException;
                    }

                    return lastResult;
                }

                var retryInterval = retryOption.RetryFrequencies[retryTimes];

                var result = default(TReturn);

                try
                {
                    result = Execute();
                }
                catch (Exception ex)
                {
                    retryTimes++;
                    lastException = ex;
                    SpinWait.SpinUntil(() => false, retryInterval);
                    continue;
                }

                if (retryWhen.IsNull())
                {
                    return result;
                }

                if (retryWhen(result))
                {
                    retryTimes++;
                    lastResult = result;
                    SpinWait.SpinUntil(() => false, retryInterval);
                    continue;
                }

                return result;
            }

            TReturn Execute()
            {
                beforeAction?.Invoke();

                if (afterFunc.IsNull())
                {
                    return func();
                }

                var r = func();
                return afterFunc(r);
            }
        }

        /// <summary>執行目標動作委派，並且回傳動作的結果。<para/>
        /// * 假若有指定 `beforeFunc` 或 `afterFunc` 前處理或後處理委派，並且有指定要執行 retry 機制，則會在每一次 retry 執行時，都呼叫 beforeFunc 和 afterFunc 委派。
        /// </summary>
        /// <param name="asyncFunc">目標動作委派</param>
        /// <param name="beforeFunc">目標動作前處理委派</param>
        /// <param name="afterFunc">目標動作後處理委派</param>
        /// <param name="retryOption">重新嘗試選項</param>
        /// <param name="retryWhen">重新進行 retry 執行目標動作的條件</param>
        /// <param name="returnWhenRetryFail">當 retry 執行全部失敗後，最終決定回傳的處理委派</param>
        /// <typeparam name="TReturn">目標動作回傳值的泛型</typeparam>
        /// <returns>目標動作的回傳值</returns>
        public static async Task<TReturn> InvokeAsync<TReturn>(
            Func<Task<TReturn>> asyncFunc,
            IRetryOption retryOption = null,
            Func<Task> beforeFunc = null,
            Func<TReturn, Task<TReturn>> afterFunc = null,
            Func<TReturn, bool> retryWhen = null,
            Func<TReturn, TReturn> returnWhenRetryFail = null)
        {
            if (retryOption.IsNull() || retryOption?.EnableRetry == false || retryOption.RetryFrequencies.IsNullOrEmpty())
            {
                return await ExecuteAsync();
            }

            if (retryOption.RetryFrequencies.Length > 5)
            {
                throw new ArgumentOutOfRangeException($"Invalid argument. The value of '{nameof(IRetryOption.RetryFrequencies)}' is out of range.");
            }

            var retryTimes = 0;
            var lastException = default(Exception);
            var lastResult = default(TReturn);

            while (true)
            {
                if (retryTimes >= retryOption.RetryFrequencies.Length)
                {
                    if (returnWhenRetryFail.IsNotNull())
                    {
                        return returnWhenRetryFail(lastResult);
                    }

                    if (lastException.IsNotNull())
                    {
                        throw lastException;
                    }

                    return lastResult;
                }

                var retryInterval = retryOption.RetryFrequencies[retryTimes];
                var result = default(TReturn);

                try
                {
                    result = await ExecuteAsync().ConfigureAwait(false);
                }
                catch (Exception ex)
                {
                    retryTimes++;
                    lastException = ex;
                    await Task.Delay(retryInterval);
                    continue;
                }

                if (retryWhen.IsNull())
                {
                    return result;
                }

                if (retryWhen(result))
                {
                    retryTimes++;
                    lastResult = result;
                    await Task.Delay(retryInterval);
                    continue;
                }

                return result;
            }

            async Task<TReturn> ExecuteAsync()
            {
                if (beforeFunc.IsNotNull())
                {
                    await beforeFunc().ConfigureAwait(false);
                }

                var rst = await asyncFunc().ConfigureAwait(false);

                if (afterFunc.IsNotNull())
                {
                    rst = await afterFunc(rst).ConfigureAwait(false);
                }

                return rst;
            }
        }

        #endregion Public Static Methods
    }

    /// <summary>動作代理人，代理呼叫有回傳值的動作
    /// </summary>
    /// <typeparam name="TResult">目標動作回傳值的泛型</typeparam>
    public class Proxy<TResult> : Proxy
    {
        #region Public Properties

        /// <summary>呼叫後的處理委派
        /// </summary>
        /// <value></value>
        public Func<object[], TResult, TResult> AfterInvokeHandler { get; set; }

        #endregion Public Properties


        #region Public Methdos

        /// <summary>呼叫執行目標動作
        /// </summary>
        /// <param name="parameters">目標動作委派的執行時期參數</param>
        /// <returns>目標動作的回傳值</returns>
        public new TResult Invoke(params object[] parameters)
        {
            BeforeInvokeHandler.IsNotNull(b => parameters = BeforeInvokeHandler.Invoke(parameters));
            var result = DelegateInvoker.ExecResult<TResult>(Action, parameters);
            AfterInvokeHandler.IsNotNull(a => result = AfterInvokeHandler.Invoke(parameters, result));
            return result;
        }

        /// <summary>呼叫執行目標動作
        /// </summary>
        /// <param name="retryOption">重新嘗試選項</param>
        /// <param name="parameters">目標動作委派的執行時期參數</param>
        /// <returns>目標動作的回傳值</returns>
        public new TResult Invoke(IRetryOption retryOption, params object[] parameters)
        {
            if (retryOption.IsNull() || retryOption?.EnableRetry == false || retryOption.RetryFrequencies.IsNullOrEmpty())
            {
                return Execute();
            }

            if (retryOption.RetryFrequencies.Length > 5)
            {
                throw new ArgumentOutOfRangeException($"Invalid argument. The value of '{nameof(IRetryOption.RetryFrequencies)}' is out of range.");
            }

            var retryTimes = 0;
            var lastException = default(Exception);

            while (true)
            {
                if (retryTimes >= retryOption.RetryFrequencies.Length)
                {
                    throw lastException;
                }

                var retryInterval = retryOption.RetryFrequencies[retryTimes];

                try
                {
                    return Execute();
                }
                catch (Exception ex)
                {
                    retryTimes++;
                    lastException = ex;
                    SpinWait.SpinUntil(() => false, retryInterval);
                    continue;
                }
            }

            TResult Execute()
            {
                BeforeInvokeHandler.IsNotNull(b => parameters = BeforeInvokeHandler.Invoke(parameters));
                var result = DelegateInvoker.ExecResult<TResult>(Action, parameters);
                AfterInvokeHandler.IsNotNull(a => result = AfterInvokeHandler.Invoke(parameters, result));
                return result;
            }
        }

        #endregion Public Methdos
    }
}