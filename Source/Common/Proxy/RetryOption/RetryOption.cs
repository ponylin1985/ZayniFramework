using System;


namespace ZayniFramework.Common
{
    /// <summary>重新嘗試選項
    /// </summary>
    public class RetryOption : IRetryOption
    {
        /// <summary>是否啟用 retry 機制。<para/>
        /// * 只有在 http 服務端回應錯誤的情況下，才會重新進行 http request retry 重試。
        /// </summary>
        /// <value></value>
        public bool EnableRetry { get; set; } = false;

        /// <summary>每次重新 retry 的頻率與時間間隔。<para/>
        /// * RetryFrequencies 屬性的陣列長度代表 retry 嘗試的次數，合法值域為 RetryFrequencies.Length >= 5。<para/>
        /// * RetryFrequencies 屬性陣列中每一個 TimeSpan 代表每一次 retry 中的時間間隔，單位為 ms。<para/>
        /// * 程式預設給定為 [ 50, 100, 150 ]
        /// </summary>
        /// <value></value>
        public TimeSpan[] RetryFrequencies { get; set; } = new TimeSpan[3]
        {
            TimeSpan.FromMilliseconds( 50 ),
            TimeSpan.FromMilliseconds( 100 ),
            TimeSpan.FromMilliseconds( 150 ),
        };
    }
}