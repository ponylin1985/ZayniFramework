using System;
using System.Collections;
using System.Threading.Tasks;


namespace ZayniFramework.Common
{
    /// <summary>IResult 的擴充類別
    /// </summary>
    public static class IResultExtension
    {
        #region Pipeline Methods

        /// <summary>串接 IResult 方法鏈
        /// </summary>
        /// <param name="result">來源 IResult 物件</param>
        /// <param name="handler">方法鏈串接處理委派</param>
        /// <param name="errorHandler">異常處理委派</param>
        /// <typeparam name="TSource">串接處理接受的參數泛型</typeparam>
        /// <typeparam name="TResult">串接處理後的回傳值泛型</typeparam>
        /// <returns>串接處理後的 IResult 結果</returns>
        public static IResult<TResult> Pipe<TSource, TResult>(
            this IResult<TSource> result,
            Func<TSource, IResult<TResult>> handler,
            Func<Exception, TSource, IResult<TResult>> errorHandler = null)
        {
            try
            {
                return handler.Invoke(result.Data);
            }
            catch (Exception ex)
            {
                return result.OccurError<TSource, TResult>(ex);
            }
        }

        /// <summary>串接 IResult 方法鏈
        /// </summary>
        /// <param name="result">來源 IResult 物件</param>
        /// <param name="handler">方法鏈串接處理委派</param>
        /// <param name="errorHandler">異常處理委派</param>
        /// <typeparam name="TResult">串接處理後的回傳值泛型</typeparam>
        /// <returns>串接處理後的 IResult 結果</returns>
        public static IResult<TResult> Pipe<TResult>(
            this IResult result,
            Func<IResult<TResult>> handler,
            Func<Exception, IResult<TResult>> errorHandler = null)
        {
            try
            {
                return handler.Invoke();
            }
            catch (Exception ex)
            {


                return result.OccurError<TResult>(ex);
            }
        }

        /// <summary>串接 IResult 方法鏈
        /// </summary>
        /// <param name="result">來源 IResult 物件</param>
        /// <param name="handler">方法鏈串接處理委派</param>
        /// <typeparam name="TSource">串接處理接受的參數泛型</typeparam>
        /// <returns>串接處理後的 IResult 結果</returns>
        public static IResult Pipe<TSource>(this IResult<TSource> result, Func<TSource, IResult> handler)
        {
            try
            {
                return handler.Invoke(result.Data);
            }
            catch (Exception ex)
            {
                return result.OccurError(ex);
            }
        }

        /// <summary>串接 IResult 方法鏈
        /// </summary>
        /// <param name="result">來源 IResult 物件</param>
        /// <param name="handler">方法鏈串接處理委派</param>
        /// <returns>串接處理後的 IResult 結果</returns>
        public static IResult Pipe(this IResult result, Func<IResult> handler)
        {
            try
            {
                return handler.Invoke();
            }
            catch (Exception ex)
            {
                return result.OccurError(ex);
            }
        }

        /// <summary>當 IResult 串接方法鏈方發異常時的處理
        /// </summary>
        /// <param name="result">來源 IResult 物件</param>
        /// <param name="errorHandler">應用程式異常的例外處理</param>
        /// <returns>異常處理後的 IResult 結果</returns>
        public static IResult WhenError(this IResult result, Func<Exception, IResult, IResult> errorHandler)
        {
            if (result is BaseResult r && r.ExceptionObject.IsNotNull())
            {
                return errorHandler?.Invoke(r.ExceptionObject, result);
            }

            return result;
        }

        /// <summary>當 IResult 串接方法鏈方發異常時的處理
        /// </summary>
        /// <param name="result">來源 IResult 物件</param>
        /// <param name="errorHandler">應用程式異常的例外處理</param>
        /// <returns>異常處理後的 IResult 結果</returns>
        public static IResult WhenError(this IResult result, Action<Exception> errorHandler)
        {
            if (result is BaseResult r && r.ExceptionObject.IsNotNull())
            {
                errorHandler?.Invoke(r.ExceptionObject);
            }

            return result;
        }

        #endregion Pipeline Methods


        #region Async Pipeline Methods

        /// <summary>串接 IResult 非同步作業的方法鏈
        /// </summary>
        /// <param name="result">來源 IResult 物件</param>
        /// <param name="handler">非同步作業的方法鏈處理委派</param>
        /// <param name="errorHandler">異常處理委派</param>
        /// <typeparam name="TSource">串接處理接受的參數泛型</typeparam>
        /// <typeparam name="TResult">串接處理後的回傳值泛型</typeparam>
        /// <returns>串接處理後的 IResult 結果</returns>
        public static async Task<IResult<TResult>> PipeAsync<TSource, TResult>(
            this IResult<TSource> result,
            Func<TSource, Task<IResult<TResult>>> handler,
            Func<Exception, TSource, Task<IResult<TResult>>> errorHandler = null)
        {
            try
            {
                return await handler.Invoke(result.Data);
            }
            catch (Exception ex)
            {
                return result.OccurError<TSource, TResult>(ex);
            }
        }

        /// <summary>串接 IResult 非同步作業的方法鏈
        /// </summary>
        /// <param name="result">來源 IResult 物件</param>
        /// <param name="handler">非同步作業的方法鏈處理委派</param>
        /// <param name="errorHandler">異常處理委派</param>
        /// <typeparam name="TResult">串接處理後的回傳值泛型</typeparam>
        /// <returns>串接處理後的 IResult 結果</returns>
        public static async Task<IResult<TResult>> PipeAsync<TResult>(
            this IResult result,
            Func<Task<IResult<TResult>>> handler,
            Func<Exception, Task<IResult<TResult>>> errorHandler = null)
        {
            try
            {
                return await handler.Invoke();
            }
            catch (Exception ex)
            {
                return result.OccurError<TResult>(ex);
            }
        }

        /// <summary>串接 IResult 非同步作業的方法鏈
        /// </summary>
        /// <param name="result">來源 IResult 物件</param>
        /// <param name="handler">非同步作業的方法鏈處理委派</param>
        /// <param name="errorHandler">異常處理委派</param>
        /// <typeparam name="TSource">串接處理接受的參數泛型</typeparam>
        /// <returns>串接處理後的 IResult 結果</returns>
        public static async Task<IResult> PipeAsync<TSource>(
            this IResult<TSource> result,
            Func<TSource, Task<IResult>> handler,
            Func<Exception, TSource, Task<IResult>> errorHandler = null)
        {
            try
            {
                return await handler.Invoke(result.Data);
            }
            catch (Exception ex)
            {
                return result.OccurError(ex);
            }
        }

        /// <summary>串接 IResult 非同步作業的方法鏈
        /// </summary>
        /// <param name="result">來源 IResult 物件</param>
        /// <param name="handler">非同步作業的方法鏈處理委派</param>
        /// <param name="errorHandler">異常處理委派</param>
        /// <returns>串接處理後的 IResult 結果</returns>
        public static async Task<IResult> Pipe(
            this IResult result,
            Func<Task<IResult>> handler,
            Func<Exception, Task<IResult>> errorHandler = null)
        {
            try
            {
                return await handler.Invoke();
            }
            catch (Exception ex)
            {
                return result.OccurError(ex);
            }
        }

        #endregion Async Pipeline Methods


        #region Success Handlers

        /// <summary>當執行成功的後處理
        /// </summary>
        /// <param name="result">執行結果</param>
        /// <param name="successHandler">執行成功的處理回呼</param>
        /// <returns>執行結果</returns>
        public static IResult WhenSuccess(this IResult result, Func<IResult, IResult> successHandler)
        {
            if (result.Success)
            {
                return successHandler(result);
            }

            return result;
        }

        /// <summary>當執行成功的非同步後處理
        /// </summary>
        /// <param name="result">執行結果</param>
        /// <param name="successHandlerAsync">執行成功的非同步處理回呼</param>
        /// <returns>執行結果</returns>
        public static async Task<IResult> WhenSuccessAsync(this IResult result, Func<IResult, Task<IResult>> successHandlerAsync)
        {
            if (result.Success)
            {
                return await successHandlerAsync(result);
            }

            return result;
        }

        /// <summary>當執行成功的後處理
        /// </summary>
        /// <param name="result">執行結果</param>
        /// <param name="predicate">處理回呼的條件</param>
        /// <param name="successHandler">執行成功的處理回呼</param>
        /// <returns>執行結果</returns>
        public static IResult WhenSuccess(this IResult result, Func<IResult, bool> predicate, Func<IResult, IResult> successHandler)
        {
            if (result.Success && predicate(result))
            {
                return successHandler(result);
            }

            return result;
        }

        /// <summary>當執行成功的非同步後處理
        /// </summary>
        /// <param name="result">執行結果</param>
        /// <param name="predicate">處理回呼的條件</param>
        /// <param name="successHandlerAsync">執行成功的非同步處理回呼</param>
        /// <returns>執行結果</returns>
        public static async Task<IResult> WhenSuccessAsync(this IResult result, Func<IResult, bool> predicate, Func<IResult, Task<IResult>> successHandlerAsync)
        {
            if (result.Success && predicate(result))
            {
                return await successHandlerAsync(result);
            }

            return result;
        }

        /// <summary>當執行成功的後處理
        /// </summary>
        /// <param name="result">執行結果</param>
        /// <param name="successHandler">執行成功的處理回呼</param>
        /// <typeparam name="T">執行結果的資料集合泛型</typeparam>
        /// <returns>執行結果</returns>
        public static IResult<T> WhenSuccess<T>(this IResult<T> result, Func<IResult<T>, IResult<T>> successHandler)
        {
            if (result.Success)
            {
                return successHandler(result);
            }

            return result;
        }

        /// <summary>當執行成功的非同步後處理
        /// </summary>
        /// <param name="result">執行結果</param>
        /// <param name="successHandlerAsync">執行成功的非同步處理回呼</param>
        /// <typeparam name="T">執行結果的資料集合泛型</typeparam>
        /// <returns>執行結果</returns>
        public static async Task<IResult<T>> WhenSuccessAsync<T>(this IResult<T> result, Func<IResult<T>, Task<IResult<T>>> successHandlerAsync)
        {
            if (result.Success)
            {
                return await successHandlerAsync(result);
            }

            return result;
        }

        /// <summary>當執行成功的後處理
        /// </summary>
        /// <param name="result">執行結果</param>
        /// <param name="predicate">處理回呼的條件</param>
        /// <param name="successHandler">執行成功的處理回呼</param>
        /// <typeparam name="T">執行結果的資料集合泛型</typeparam>
        /// <returns>執行結果</returns>
        public static IResult<T> WhenSuccess<T>(this IResult<T> result, Func<IResult<T>, bool> predicate, Func<IResult<T>, IResult<T>> successHandler)
        {
            if (result.Success && predicate(result))
            {
                return successHandler(result);
            }

            return result;
        }

        /// <summary>當執行成功的非同步後處理
        /// </summary>
        /// <param name="result">執行結果</param>
        /// <param name="predicate">處理回呼的條件</param>
        /// <param name="successHandlerAsync">執行成功的非同步處理回呼</param>
        /// <typeparam name="T">執行結果的資料集合泛型</typeparam>
        /// <returns>執行結果</returns>
        public static async Task<IResult<T>> WhenSuccessAsync<T>(this IResult<T> result, Func<IResult<T>, bool> predicate, Func<IResult<T>, Task<IResult<T>>> successHandlerAsync)
        {
            if (result.Success && predicate(result))
            {
                return await successHandlerAsync(result);
            }

            return result;
        }

        /// <summary>當執行成功並且有回傳資料集合的後處理
        /// </summary>
        /// <param name="result">執行結果</param>
        /// <param name="successHandler">執行成功的處理回呼</param>
        /// <typeparam name="T">執行結果的資料集合泛型</typeparam>
        /// <returns>執行結果</returns>
        public static IResult<T> WhenSuccessWithData<T>(this IResult<T> result, Func<IResult<T>, IResult<T>> successHandler)
            where T : IEnumerable
        {
            if (result.Success && result.Data.IsNotNullOrEmpty())
            {
                return successHandler(result);
            }

            return result;
        }

        /// <summary>當執行成功並且有回傳資料集合的非同步後處理
        /// </summary>
        /// <param name="result">執行結果</param>
        /// <param name="successHandlerAsync">執行成功的非同步處理回呼</param>
        /// <typeparam name="T">執行結果的資料集合泛型</typeparam>
        /// <returns>執行結果</returns>
        public static async Task<IResult<T>> WhenSuccessWithDataAsync<T>(this IResult<T> result, Func<IResult<T>, Task<IResult<T>>> successHandlerAsync)
            where T : IEnumerable
        {
            if (result.Success && result.Data.IsNotNullOrEmpty())
            {
                return await successHandlerAsync(result);
            }

            return result;
        }

        #endregion Success Handlers


        #region Failure Handlers

        /// <summary>當執行失敗的後處理
        /// </summary>
        /// <param name="result">執行結果</param>
        /// <param name="failureHanlder">執行失敗的處理回呼</param>
        /// <returns>執行結果</returns>
        public static IResult WhenFailure(this IResult result, Func<IResult, IResult> failureHanlder)
        {
            if (!result.Success)
            {
                return failureHanlder(result);
            }

            return result;
        }

        /// <summary>當執行失敗的非同步後處理
        /// </summary>
        /// <param name="result">執行結果</param>
        /// <param name="failureHandlerAsync">執行失敗的非同步處理回呼</param>
        /// <returns>執行結果</returns>
        public static async Task<IResult> WhenFailureAsync(this IResult result, Func<IResult, Task<IResult>> failureHandlerAsync)
        {
            if (!result.Success)
            {
                return await failureHandlerAsync(result);
            }

            return result;
        }

        /// <summary>當執行失敗的後處理
        /// </summary>
        /// <param name="result">執行結果</param>
        /// <param name="failurePredicate">失敗回呼條件</param>
        /// <param name="failureHanlder">執行失敗的處理回呼</param>
        /// <returns>執行結果</returns>
        public static IResult WhenFailure(this IResult result, Func<IResult, bool> failurePredicate, Func<IResult, IResult> failureHanlder)
        {
            if (!result.Success && failurePredicate(result))
            {
                return failureHanlder(result);
            }

            return result;
        }

        /// <summary>當執行失敗的非同步後處理
        /// </summary>
        /// <param name="result">執行結果</param>
        /// <param name="failurePredicate">失敗回呼條件</param>
        /// <param name="failureHanlderAsync">執行失敗的非同步處理回呼</param>
        /// <returns>執行結果</returns>
        public static async Task<IResult> WhenFailureAsync(this IResult result, Func<IResult, bool> failurePredicate, Func<IResult, Task<IResult>> failureHanlderAsync)
        {
            if (!result.Success && failurePredicate(result))
            {
                return await failureHanlderAsync(result);
            }

            return result;
        }

        /// <summary>當執行失敗的後處理
        /// </summary>
        /// <param name="result">執行結果</param>
        /// <param name="failureHandler">執行失敗的處理回呼</param>
        /// <typeparam name="T">執行結果的資料集合泛型</typeparam>
        /// <returns>執行結果</returns>
        public static IResult<T> WhenFailure<T>(this IResult<T> result, Func<IResult<T>, IResult<T>> failureHandler)
        {
            if (!result.Success)
            {
                return failureHandler(result);
            }

            return result;
        }

        /// <summary>當執行失敗的非同步後處理
        /// </summary>
        /// <param name="result">執行結果</param>
        /// <param name="failureHandlerAsync">執行失敗的非同步處理回呼</param>
        /// <typeparam name="T">執行結果的資料集合泛型</typeparam>
        /// <returns>執行結果</returns>
        public static async Task<IResult<T>> WhenFailureAsync<T>(this IResult<T> result, Func<IResult<T>, Task<IResult<T>>> failureHandlerAsync)
        {
            if (!result.Success)
            {
                return await failureHandlerAsync(result);
            }

            return result;
        }

        /// <summary>當執行失敗的後處理
        /// </summary>
        /// <param name="result">執行結果</param>
        /// <param name="failurePredicate">失敗回呼條件</param>
        /// <param name="failureHandler">執行失敗的處理回呼</param>
        /// <typeparam name="T">執行結果的資料集合泛型</typeparam>
        /// <returns>執行結果</returns>
        public static IResult<T> WhenFailure<T>(this IResult<T> result, Func<IResult<T>, bool> failurePredicate, Func<IResult<T>, IResult<T>> failureHandler)
        {
            if (!result.Success && failurePredicate(result))
            {
                return failureHandler(result);
            }

            return result;
        }

        /// <summary>當執行失敗的非同步後處理
        /// </summary>
        /// <param name="result">執行結果</param>
        /// <param name="failurePredicate">失敗回呼條件</param>
        /// <param name="failureHandlerAsync">執行失敗的非同步處理回呼</param>
        /// <typeparam name="T">執行結果的資料集合泛型</typeparam>
        /// <returns>執行結果</returns>
        public static async Task<IResult<T>> WhenFailureAsync<T>(this IResult<T> result, Func<IResult<T>, bool> failurePredicate, Func<IResult<T>, Task<IResult<T>>> failureHandlerAsync)
        {
            if (!result.Success && failurePredicate(result))
            {
                return await failureHandlerAsync(result);
            }

            return result;
        }

        #endregion Failure Handlers


        #region Predicate Handlers

        /// <summary>當條件成立時，執行指定的處理回呼
        /// </summary>
        /// <param name="result">執行結果</param>
        /// <param name="predicate">條件成立委派</param>
        /// <param name="handler">處理回呼委派</param>
        /// <returns>執行結果</returns>
        public static IResult When(this IResult result, Func<IResult, bool> predicate, Func<IResult, IResult> handler)
        {
            if (predicate(result))
            {
                return handler(result);
            }

            return handler(result);
        }

        /// <summary>當條件成立時，執行指定的處理回呼
        /// </summary>
        /// <param name="result">執行結果</param>
        /// <param name="predicate">條件成立委派</param>
        /// <param name="handler">處理回呼委派</param>
        /// <returns>執行結果</returns>
        public static void When(this IResult result, Func<IResult, bool> predicate, Action<IResult> handler)
        {
            if (predicate(result))
            {
                handler(result);
                return;
            }

            handler(result);
        }

        /// <summary>當條件成立時，執行指定的處理回呼
        /// </summary>
        /// <param name="result">執行結果</param>
        /// <param name="predicate">條件成立委派</param>
        /// <param name="handler">處理回呼委派</param>
        /// <returns>執行結果</returns>
        public static IResult<T> When<T>(this IResult<T> result, Func<IResult<T>, bool> predicate, Func<IResult<T>, IResult<T>> handler)
        {
            if (predicate(result))
            {
                return handler(result);
            }

            return handler(result);
        }

        /// <summary>當條件成立時，執行指定的處理回呼
        /// </summary>
        /// <param name="result">執行結果</param>
        /// <param name="predicate">條件成立委派</param>
        /// <param name="handler">處理回呼委派</param>
        /// <returns>執行結果</returns>
        public static void When<T>(this IResult<T> result, Func<IResult<T>, bool> predicate, Action<IResult<T>> handler)
        {
            if (predicate(result))
            {
                handler(result);
                return;
            }

            handler(result);
        }

        /// <summary>當條件成立時，執行指定的非同步處理回呼
        /// </summary>
        /// <param name="result">執行結果</param>
        /// <param name="predicate">條件成立委派</param>
        /// <param name="asyncHandler">非同步處理回呼委派</param>
        /// <returns>執行結果</returns>
        public static async Task<IResult> WhenAsync(this IResult result, Func<IResult, bool> predicate, Func<IResult, Task<IResult>> asyncHandler)
        {
            if (predicate(result))
            {
                return await asyncHandler(result);
            }

            return await asyncHandler(result);
        }

        /// <summary>當條件成立時，執行指定的非同步處理回呼
        /// </summary>
        /// <param name="result">執行結果</param>
        /// <param name="predicate">條件成立委派</param>
        /// <param name="asyncHandler">非同步處理回呼委派</param>
        /// <returns>執行結果</returns>
        public static async Task WhenAsync(this IResult result, Func<IResult, bool> predicate, Func<IResult, Task> asyncHandler)
        {
            if (predicate(result))
            {
                await asyncHandler(result);
                return;
            }

            await asyncHandler(result);
        }

        /// <summary>當條件成立時，執行指定的非同步處理回呼
        /// </summary>
        /// <param name="result">執行結果</param>
        /// <param name="predicate">條件成立委派</param>
        /// <param name="asyncHandler">非同步處理回呼委派</param>
        /// <returns>執行結果</returns>
        public static async Task<IResult<T>> WhenAsync<T>(this IResult<T> result, Func<IResult<T>, bool> predicate, Func<IResult<T>, Task<IResult<T>>> asyncHandler)
        {
            if (predicate(result))
            {
                return await asyncHandler(result);
            }

            return await asyncHandler(result);
        }

        /// <summary>當條件成立時，執行指定的處理回呼
        /// </summary>
        /// <param name="result">執行結果</param>
        /// <param name="predicate">條件成立委派</param>
        /// <param name="asyncHandler">處理回呼委派</param>
        /// <returns>執行結果</returns>
        public static async Task WhenAsync<T>(this IResult<T> result, Func<IResult<T>, bool> predicate, Func<IResult<T>, Task> asyncHandler)
        {
            if (predicate(result))
            {
                await asyncHandler(result);
                return;
            }

            await asyncHandler(result);
        }

        #endregion Predicate Handlers


        #region Private Methods

        /// <summary>紀錄方法 IResult 串接時的應用程式異常
        /// </summary>
        /// <param name="result">來源 IResult 物件</param>
        /// <param name="ex">應用程式異常</param>
        /// <returns>串接處理後的 IResult 結果</returns>
        private static IResult<TResult> OccurError<TSource, TResult>(this IResult<TSource> result, Exception ex)
        {
            var errorResult = Result.Create<TResult>(false, data: default(TResult));
            errorResult.ExceptionObject = ex;
            return errorResult;
        }

        /// <summary>紀錄方法 IResult 串接時的應用程式異常
        /// </summary>
        /// <param name="result">來源 IResult 物件</param>
        /// <param name="ex">應用程式異常</param>
        /// <returns>串接處理後的 IResult 結果</returns>
        private static IResult<TSource> OccurError<TSource>(this IResult result, Exception ex)
        {
            var errorResult = Result.Create(false, data: default(TSource));
            errorResult.ExceptionObject = ex;
            return errorResult;
        }

        /// <summary>紀錄方法 IResult 串接時的應用程式異常
        /// </summary>
        /// <param name="result">來源 IResult 物件</param>
        /// <param name="ex">應用程式異常</param>
        /// <returns>串接處理後的 IResult 結果</returns>
        private static IResult OccurError(this IResult result, Exception ex)
        {
            var errorResult = Result.Create(false);
            errorResult.ExceptionObject = ex;
            return errorResult;
        }

        #endregion Private Methods
    }
}