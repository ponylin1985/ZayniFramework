﻿using System.Collections;
using System.Collections.Generic;
using ZayniFramework.Common.Dynamic;


namespace ZayniFramework.Common
{
    /// <summary>字典集合的擴充方法
    /// </summary>
    public static class IDictionaryExtension
    {
        /// <summary>轉換成 ExpandoObject 動態物件。<para/>
        /// var source = new Dictionary&lt;string, object&gt;()<para/>
        /// {<para/>
        ///     [ "HappyBear" ]   = "Ya我是Happy Bear",<para/>
        ///     [ "Birthday" ]    = DateTime.Now,<para/>
        ///     [ "MagicNumber" ] = 7765,<para/>
        ///     [ "UserInfo" ]    = new UserModel() { Name = "Sylvia", Sex = "female", Age = 27 }<para/>
        /// };<para/>
        /// dynamic d = source.ConvertToDynamicObj();<para/>
        /// Assert.AreEqual( source[ "HappyBear" ],   d.HappyBear );<para/>
        /// Assert.AreEqual( source[ "Birthday" ],    d.Birthday );<para/>
        /// Assert.AreEqual( source[ "MagicNumber" ], d.MagicNumber );<para/>
        /// </summary>
        /// <example>
        /// <code>
        /// var source = new Dictionary&lt;string, object&gt;()
        /// {
        ///     [ "HappyBear" ]   = "Ya我是Happy Bear",
        ///     [ "Birthday" ]    = DateTime.Now,
        ///     [ "MagicNumber" ] = 7765,
        ///     [ "UserInfo" ]    = new UserModel() { Name = "Sylvia", Sex = "female", Age = 27 }
        /// };
        /// dynamic d = source.ConvertToDynamicObj();
        /// Assert.AreEqual( source[ "HappyBear" ],   d.HappyBear );
        /// Assert.AreEqual( source[ "Birthday" ],    d.Birthday );
        /// Assert.AreEqual( source[ "MagicNumber" ], d.MagicNumber );
        /// </code>
        /// </example>
        /// <param name="source">來源字典物件</param>
        /// <returns>動態物件</returns>
        public static dynamic ConvertToDynamicObj(this IDictionary source)
        {
            if (null == source)
            {
                return null;
            }

            dynamic result = DynamicHelper.CreateDynamicObject();

            foreach (var key in source.Keys)
            {
                var name = key + "";
                var value = source[key];
                DynamicHelper.BindProperty(result, name, value);
            }

            return result;
        }

        /* 使用說明Sample Code: (或請參考Unit Test專案的 DictionaryExtensionTester.FetchKeysTest測試方法)
         * 
         * Dictionary<string, string> dict = new Dictionary<string, string>();
    
            dict.Add( "a", "1" );
            dict.Add( "b", "2" );
            dict.Add( "c", "3" );
            dict.Add( "d", "4" );
            dict.Add( "e", "5" );
    
            List<string> list = new List<string>() 
            {
                "a", "b", "c"
            };

            List<string> result = dict.FetchKeys( list ).ToList();
            
            --> result的結果會是包含 "a", "b", "c" 的List
         * 
         */
        /// <summary>從來源集合中的字串值，擷取出字典物件中的 Key 集合
        /// </summary>
        /// <typeparam name="TValue">字典集合的 Value 泛型</typeparam>
        /// <param name="self"></param>
        /// <param name="source">來源字串集合</param>
        /// <returns>符合來源字串集合中的 Keys 集合</returns>
        public static IEnumerable<string> FetchKeys<TValue>(this Dictionary<string, TValue> self, IEnumerable<string> source)
        {
            var result = new List<string>();

            foreach (var s in source)
            {
                if (self.ContainsKey(s))
                {
                    yield return s;
                }
            }
        }

        /* 使用說明Sample Code: (或請參考Unit Test專案的 DictionaryExtensionTester.FetchValuesTest測試方法)
         * 
            Dictionary<string, string> dict = new Dictionary<string, string>();
    
            dict.Add( "a", "1" );
            dict.Add( "b", "2" );
            dict.Add( "c", "3" );
            dict.Add( "d", "4" );
            dict.Add( "e", "5" );
    
            List<string> list = new List<string>() 
            {
                "a", "b", "c"
            };

            List<string> result = dict.FetchValues<string>( list ).ToList();
         
            --> result的結果會是包含 "1", "2", "3" 的List
         * 
         */
        /// <summary>從來源集合中的字串值，擷取出字典物件中對應 Key 的 Values 集合
        /// </summary>
        /// <typeparam name="TValue">字典集合的 Value 泛型</typeparam>
        /// <param name="self"></param>
        /// <param name="source">來源字串集合</param>
        /// <returns>符合來源字串集合中的 Key 對應的 Values 集合</returns>
        public static IEnumerable<TValue> FetchValues<TValue>(this Dictionary<string, TValue> self, IEnumerable<string> source)
        {
            var result = new List<TValue>();

            foreach (var s in source)
            {
                if (self.TryGetValue(s, out var value))
                {
                    yield return value;
                }
            }
        }
    }
}
