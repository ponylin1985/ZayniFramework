﻿using System;
using System.Threading.Tasks;


namespace ZayniFramework.Common
{
    /// <summary>TimeSpan 功能擴充類別
    /// </summary>
    public static class TimeSpanExtension
    {
        /// <summary>檢查 TimeSpan 是否為 .NET 的 TimeSpan 預設值。
        /// </summary>
        /// <param name="self">TimeSpan 來源</param>
        /// <returns></returns>
        public static bool IsDotNetDefault(this TimeSpan self) => self == default(TimeSpan);

        /// <summary>檢查 TimeSpan 是否為 .NET 的 TimeSpan 預設值，如果是則將自動使用傳入的 TimeSpan 整數值取代，如果不是，則不會有取代的效果。
        /// </summary>
        /// <param name="self">TimeSpan 來源</param>
        /// <param name="value">取代預設值的 TimeSpan 整數</param>
        /// <returns></returns>
        public static TimeSpan IsDotNetDefault(this TimeSpan self, TimeSpan value) => default(TimeSpan) == self ? value : self;

        /// <summary>檢查 TimeSpan? 數值是否為 null 或 .NET TimeSpan 的預設值。
        /// </summary>
        /// <param name="self">TimeSpan? 來源</param>
        /// <returns>是否為 null 或 .NET TimeSpan 的預設值。</returns>
        public static bool IsNullOrDefault(this TimeSpan? self) => null == self || self == default(TimeSpan);

        /// <summary>檢查 TimeSpan? 數值是否不為 null 或 .NET TimeSpan 的預設值。
        /// </summary>
        /// <param name="self">TimeSpan? 來源</param>
        /// <returns>是否不為 null 或 .NET TimeSpan 的預設值。</returns>
        public static bool IsNotNullOrDefault(this TimeSpan? self) => !self.IsNullOrDefault();

        /// <summary>檢查 TimeSpan? 數值是否為 null 或 .NET TimeSpan 的預設值，如果為 true 的情況則自動使用指定的 TimeSpan 數值回傳。
        /// </summary>
        /// <param name="self">TimeSpan? 來源</param>
        /// <param name="value">取代的 TimeSpan </param>
        /// <returns>取代的 TimeSpan </returns>
        public static TimeSpan? IsNullOrDefault(this TimeSpan? self, TimeSpan value) => self.IsNullOrDefault() ? value : self;

        /// <summary>當為 null 或 .NET TimeSpan 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">TimeSpan? 來源</param>
        /// <param name="handler">處理委派</param>
        /// <returns>原始 TimeSpan? </returns>
        public static TimeSpan? WhenNullOrDefault(this TimeSpan? self, Action handler)
        {
            if (self.IsNullOrDefault())
            {
                try
                {
                    handler();
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }

        /// <summary>當為 null 或 .NET TimeSpan 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">TimeSpan? 來源</param>
        /// <param name="asyncFunc">非同步作業處理委派</param>
        /// <returns>原始 TimeSpan? </returns>
        public static async Task<TimeSpan?> WhenNullOrDefault(this TimeSpan? self, Func<Task> asyncFunc)
        {
            if (self.IsNullOrDefault())
            {
                try
                {
                    await asyncFunc();
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }

        /// <summary>當為 null 或 .NET TimeSpan 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">TimeSpan? 來源</param>
        /// <param name="handler">處理委派</param>
        /// <returns>處理後的 TimeSpan? </returns>
        public static TimeSpan? WhenNullOrDefault(this TimeSpan? self, Func<TimeSpan?> handler)
        {
            if (self.IsNullOrDefault())
            {
                try
                {
                    return handler();
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }

        /// <summary>當為 null 或 .NET TimeSpan 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">TimeSpan? 來源</param>
        /// <param name="asyncFunc">非同步作業處理委派</param>
        /// <returns>處理後的 TimeSpan? </returns>
        public static async Task<TimeSpan?> WhenNullOrDefault(this TimeSpan? self, Func<Task<TimeSpan?>> asyncFunc)
        {
            if (self.IsNullOrDefault())
            {
                try
                {
                    return await asyncFunc();
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }

        /// <summary>當不為 null 或 .NET TimeSpan 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">TimeSpan? 來源</param>
        /// <param name="handler">處理委派</param>
        /// <returns>原始 TimeSpan? </returns>
        public static TimeSpan? WhenNotNullOrDefault(this TimeSpan? self, Action<TimeSpan?> handler)
        {
            if (self.IsNotNullOrDefault())
            {
                try
                {
                    handler(self);
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }

        /// <summary>當不為 null 或 .NET TimeSpan 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">TimeSpan? 來源</param>
        /// <param name="asyncFunc">非同步作業處理委派</param>
        /// <returns>原始 TimeSpan? </returns>
        public static async Task<TimeSpan?> WhenNotNullOrDefault(this TimeSpan? self, Func<TimeSpan?, Task> asyncFunc)
        {
            if (self.IsNotNullOrDefault())
            {
                try
                {
                    await asyncFunc(self);
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }

        /// <summary>當不為 null 或 .NET TimeSpan 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">TimeSpan? 來源</param>
        /// <param name="handler">處理委派</param>
        /// <returns>處理後的 TimeSpan? </returns>
        public static TimeSpan? WhenNotNullOrDefault(this TimeSpan? self, Func<TimeSpan?, TimeSpan?> handler)
        {
            if (self.IsNotNullOrDefault())
            {
                try
                {
                    return handler(self);
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }

        /// <summary>當不為 null 或 .NET TimeSpan 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">TimeSpan? 來源</param>
        /// <param name="asyncFunc">非同步作業處理委派</param>
        /// <returns>處理後的 TimeSpan? </returns>
        public static async Task<TimeSpan?> WhenNotNullOrDefault(this TimeSpan? self, Func<TimeSpan?, Task<TimeSpan?>> asyncFunc)
        {
            if (self.IsNotNullOrDefault())
            {
                try
                {
                    return await asyncFunc(self);
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }
    }
}
