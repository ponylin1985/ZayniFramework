﻿using System;
using System.Threading.Tasks;


namespace ZayniFramework.Common
{
    /// <summary>Int32 的擴充類別
    /// </summary>
    public static class Int32Extension
    {
        /// <summary>檢查 Int32 數字是否為 .NET CLR 的系統預設值 0，如果是則將自動使用傳入的 Int32 整數值取代，如果不是，則不會有取代的效果。
        /// </summary>
        /// <param name="self">Int32 來源數值</param>
        /// <param name="value">取代預設值 0 的 Int32 整數</param>
        /// <returns></returns>
        public static int IsDotNetDefault(this int self, int value) => 0 == self ? value : self;

        /// <summary>檢查 Int32? 數值是否為 null 或 .NET Int32 的預設值。
        /// </summary>
        /// <param name="self">Int32? 來源數值</param>
        /// <returns>是否為 null 或 .NET Int32 的預設值。</returns>
        public static bool IsNullOrDefault(this int? self) => null == self || self == default(int);

        /// <summary>檢查 Int32? 數值是否不為 null 或 .NET Int32 的預設值。
        /// </summary>
        /// <param name="self">Int32? 來源數值</param>
        /// <returns>是否不為 null 或 .NET Int32 的預設值。</returns>
        public static bool IsNotNullOrDefault(this int? self) => !self.IsNullOrDefault();

        /// <summary>檢查 Int32? 數值是否為 null 或 .NET Int32 的預設值，如果為 true 的情況則自動使用指定的 Int32 數值回傳。
        /// </summary>
        /// <param name="self">Int32? 來源數值</param>
        /// <param name="value">取代的 Int32 數值</param>
        /// <returns>取代的 Int32 數值</returns>
        public static int? IsNullOrDefault(this int? self, int value) => self.IsNullOrDefault() ? value : self;

        /// <summary>當為 null 或 .NET Int32 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">Int32? 來源數值</param>
        /// <param name="handler">處理委派</param>
        /// <returns>原始 Int32? 數值</returns>
        public static int? WhenNullOrDefault(this int? self, Action handler)
        {
            if (self.IsNullOrDefault())
            {
                try
                {
                    handler();
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }

        /// <summary>當為 null 或 .NET Int32 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">Int32? 來源數值</param>
        /// <param name="asyncFunc">非同步作業處理委派</param>
        /// <returns>原始 Int32? 數值</returns>
        public static async Task<int?> WhenNullOrDefault(this int? self, Func<Task> asyncFunc)
        {
            if (self.IsNullOrDefault())
            {
                try
                {
                    await asyncFunc();
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }

        /// <summary>當為 null 或 .NET Int32 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">Int32? 來源數值</param>
        /// <param name="handler">處理委派</param>
        /// <returns>處理後的 Int32? 數值</returns>
        public static int? WhenNullOrDefault(this int? self, Func<int?> handler)
        {
            if (self.IsNullOrDefault())
            {
                try
                {
                    return handler();
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }

        /// <summary>當為 null 或 .NET Int32 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">Int32? 來源數值</param>
        /// <param name="asyncFunc">非同步作業處理委派</param>
        /// <returns>處理後的 Int32? 數值</returns>
        public static async Task<int?> WhenNullOrDefault(this int? self, Func<Task<int?>> asyncFunc)
        {
            if (self.IsNullOrDefault())
            {
                try
                {
                    return await asyncFunc();
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }

        /// <summary>當不為 null 或 .NET Int32 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">Int32? 來源數值</param>
        /// <param name="handler">處理委派</param>
        /// <returns>原始 Int32? 數值</returns>
        public static int? WhenNotNullOrDefault(this int? self, Action<int?> handler)
        {
            if (self.IsNotNullOrDefault())
            {
                try
                {
                    handler(self);
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }

        /// <summary>當不為 null 或 .NET Int32 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">Int32? 來源數值</param>
        /// <param name="asyncFunc">非同步作業處理委派</param>
        /// <returns>原始 Int32? 數值</returns>
        public static async Task<int?> WhenNotNullOrDefault(this int? self, Func<int?, Task> asyncFunc)
        {
            if (self.IsNotNullOrDefault())
            {
                try
                {
                    await asyncFunc(self);
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }

        /// <summary>當不為 null 或 .NET Int32 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">Int32? 來源數值</param>
        /// <param name="handler">處理委派</param>
        /// <returns>處理後的 Int32? 數值</returns>
        public static int? WhenNotNullOrDefault(this int? self, Func<int?, int?> handler)
        {
            if (self.IsNotNullOrDefault())
            {
                try
                {
                    return handler(self);
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }

        /// <summary>當不為 null 或 .NET Int32 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">Int32? 來源數值</param>
        /// <param name="asyncFunc">非同步作業處理委派</param>
        /// <returns>處理後的 Int32? 數值</returns>
        public static async Task<int?> WhenNotNullOrDefault(this int? self, Func<int?, Task<int?>> asyncFunc)
        {
            if (self.IsNotNullOrDefault())
            {
                try
                {
                    return await asyncFunc(self);
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }
    }
}
