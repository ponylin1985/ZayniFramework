using System;
using System.Threading.Tasks;


namespace ZayniFramework.Common
{
    /// <summary>DateTimeOffset 的擴充類別
    /// </summary>
    public static class DateTimeOffsetExtension
    {
        /// <summary>檢查 DateTimeOffset? 是否為 null 或 .NET DateTimeOffset 的預設值。
        /// </summary>
        /// <param name="self">DateTimeOffset? 來源</param>
        /// <returns>是否為 null 或 .NET DateTimeOffset 的預設值。</returns>
        public static bool IsNullOrDefault(this DateTimeOffset? self) => null == self || self == default(DateTimeOffset);

        /// <summary>檢查 DateTimeOffset? 是否不為 null 或 .NET DateTimeOffset 的預設值。
        /// </summary>
        /// <param name="self">DateTimeOffset? 來源</param>
        /// <returns>是否不為 null 或 .NET DateTimeOffset 的預設值。</returns>
        public static bool IsNotNullOrDefault(this DateTimeOffset? self) => !self.IsNullOrDefault();

        /// <summary>檢查 DateTimeOffset? 是否為 null 或 .NET DateTimeOffset 的預設值，如果為 true 的情況則自動使用指定的 DateTimeOffset 回傳。
        /// </summary>
        /// <param name="self">DateTimeOffset? 來源</param>
        /// <param name="value">取代的 DateTimeOffset </param>
        /// <returns>取代的 DateTimeOffset </returns>
        public static DateTimeOffset? IsNullOrDefault(this DateTimeOffset? self, DateTimeOffset value) => self.IsNullOrDefault() ? value : self;

        /// <summary>當為 null 或 .NET DateTimeOffset 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">DateTimeOffset? 來源</param>
        /// <param name="handler">處理委派</param>
        /// <returns>原始 DateTimeOffset? </returns>
        public static DateTimeOffset? WhenNullOrDefault(this DateTimeOffset? self, Action handler)
        {
            if (self.IsNullOrDefault())
            {
                try
                {
                    handler();
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }

        /// <summary>當為 null 或 .NET DateTimeOffset 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">DateTimeOffset? 來源</param>
        /// <param name="asyncFunc">非同步作業處理委派</param>
        /// <returns>原始 DateTimeOffset? </returns>
        public static async Task<DateTimeOffset?> WhenNullOrDefault(this DateTimeOffset? self, Func<Task> asyncFunc)
        {
            if (self.IsNullOrDefault())
            {
                try
                {
                    await asyncFunc();
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }

        /// <summary>當為 null 或 .NET DateTimeOffset 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">DateTimeOffset? 來源</param>
        /// <param name="handler">處理委派</param>
        /// <returns>處理後的 DateTimeOffset? </returns>
        public static DateTimeOffset? WhenNullOrDefault(this DateTimeOffset? self, Func<DateTimeOffset?> handler)
        {
            if (self.IsNullOrDefault())
            {
                try
                {
                    return handler();
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }

        /// <summary>當為 null 或 .NET DateTimeOffset 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">DateTimeOffset? 來源</param>
        /// <param name="asyncFunc">非同步作業處理委派</param>
        /// <returns>處理後的 DateTimeOffset? </returns>
        public static async Task<DateTimeOffset?> WhenNullOrDefault(this DateTimeOffset? self, Func<Task<DateTimeOffset?>> asyncFunc)
        {
            if (self.IsNullOrDefault())
            {
                try
                {
                    return await asyncFunc();
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }

        /// <summary>當不為 null 或 .NET DateTimeOffset 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">DateTimeOffset? 來源</param>
        /// <param name="handler">處理委派</param>
        /// <returns>原始 DateTimeOffset? </returns>
        public static DateTimeOffset? WhenNotNullOrDefault(this DateTimeOffset? self, Action<DateTimeOffset?> handler)
        {
            if (self.IsNotNullOrDefault())
            {
                try
                {
                    handler(self);
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }

        /// <summary>當不為 null 或 .NET DateTimeOffset 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">DateTimeOffset? 來源</param>
        /// <param name="asyncFunc">非同步作業處理委派</param>
        /// <returns>原始 DateTimeOffset? </returns>
        public static async Task<DateTimeOffset?> WhenNotNullOrDefault(this DateTimeOffset? self, Func<DateTimeOffset?, Task> asyncFunc)
        {
            if (self.IsNotNullOrDefault())
            {
                try
                {
                    await asyncFunc(self);
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }

        /// <summary>當不為 null 或 .NET DateTimeOffset 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">DateTimeOffset? 來源</param>
        /// <param name="handler">處理委派</param>
        /// <returns>處理後的 DateTimeOffset? </returns>
        public static DateTimeOffset? WhenNotNullOrDefault(this DateTimeOffset? self, Func<DateTimeOffset?, DateTimeOffset?> handler)
        {
            if (self.IsNotNullOrDefault())
            {
                try
                {
                    return handler(self);
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }

        /// <summary>當不為 null 或 .NET DateTimeOffset 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">DateTimeOffset? 來源</param>
        /// <param name="asyncFunc">非同步作業處理委派</param>
        /// <returns>處理後的 DateTimeOffset? </returns>
        public static async Task<DateTimeOffset?> WhenNotNullOrDefault(this DateTimeOffset? self, Func<DateTimeOffset?, Task<DateTimeOffset?>> asyncFunc)
        {
            if (self.IsNotNullOrDefault())
            {
                try
                {
                    return await asyncFunc(self);
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }
    }
}