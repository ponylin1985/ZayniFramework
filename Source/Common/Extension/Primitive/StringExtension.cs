﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;


namespace ZayniFramework.Common
{
    /// <summary>String 型別的擴充類別
    /// </summary>
    public static class StringExtension
    {
        #region String Extension Methods

        /// <summary>檢查來源字串是否以指定的字串開頭，並且忽略大小寫檢查。
        /// * Case-insensitive StartsWith
        /// </summary>
        /// <param name="source">來源自串</param>
        /// <param name="value">指定的字串值</param>
        /// <returns>是否以指定的字串開頭，並且忽略大小寫檢查</returns>
        public static bool StartsLike(this string source, string value) => source.ToLower().StartsWith(value.ToLower());

        /// <summary>檢查來源字串是否以指定的字串結尾，並且忽略大小寫檢查。
        /// * Case-insensitive EndsWith
        /// </summary>
        /// <param name="source">來源自串</param>
        /// <param name="value">指定的字串值</param>
        /// <returns>是否以指定的字串結尾，並且忽略大小寫檢查</returns>
        public static bool EndsLike(this string source, string value) => source.ToLower().EndsWith(value.ToLower());

        /// <summary>轉換字串成指定的列舉值<para/>
        /// 1. 傳入的列舉值泛型，可以接受 Nullable 的列舉，譬如: ConsoleColor? 或 ConsoleColor 或 DbType? 或 DbType 都可以正常轉換。<para/>
        /// 2. 進行轉換時，可以接受來源字串值為「忽略大小寫」(Case-insensitive) 進行轉換成 enum 列舉值。<para/>
        /// 範例:<para/>
        /// string color = "Green";<para/>
        /// ConsoleColor consoleColor = color.ParseEnum&lt;ConsoleColor&gt;();<para/>
        /// string strState = "Disable";<para/>
        /// State state = strState.ParseEnum&lt;State&gt;();<para/>
        /// </summary>
        /// <example>
        /// <code>
        /// string color = "Green";
        /// ConsoleColor consoleColor = color.ParseEnum&lt;ConsoleColor&gt;();
        /// string strState = "Disable";
        /// State state = strState.ParseEnum&lt;State&gt;();
        /// </code>
        /// </example>
        /// <typeparam name="TEnum">列舉值的泛型</typeparam>
        /// <param name="value"></param>
        /// <returns>列舉值</returns>
        public static TEnum ParseEnum<TEnum>(this string value) where TEnum : struct, IConvertible => EnumConverter.ParseEnum<TEnum>(value);

        /// <summary>移除字串中第一個出現的「目標字串」，假若目標字串不存在，則直接回傳原始字串。<para/>
        /// ex: 原始字串 "abcdefg,ggee,gg,ewew,ggfe,123".RemoveLastString( "," ) 呼叫過此擴充方法，<para/>
        /// 結果為: abcdefgggee,gg,ewew,ggfe,123
        /// </summary>
        /// <param name="self"></param>
        /// <param name="target">要移除的目標字串</param>
        /// <returns>移除字串中第一個出現的「目標字串」的結果</returns>
        public static string RemoveFirstAppeared(this string self, string target)
        {
            var index = self.IndexOf(target);
            return index >= 0 ? self.Remove(index, target.Length) : self;
        }

        /// <summary>移除字串中最後出現的「目標字串」，假若目標字串不存在，則直接回傳原始字串。<para/>
        /// ex: 原始字串 "abcdefg,ggee,gg,ewew,ggfe,123".RemoveLastString( "," ) 呼叫過此擴充方法，<para/>
        /// 結果為: abcdefg,ggee,gg,ewew,ggfe123
        /// </summary>
        /// <param name="self"></param>
        /// <param name="target">要移除的目標字串</param>
        /// <returns>移除字串中最後出現的「目標字串」的結果</returns>
        public static string RemoveLastAppeared(this string self, string target)
        {
            var index = self.LastIndexOf(target);
            return index >= 0 ? self.Remove(index, target.Length) : self;
        }

        /// <summary>根據指定的起始索引與長度，進行對目標字元的置換
        /// ex: 原始字串 "博暉科技股份有限公司".ReplaceChar( 'X', 1, 5 ) 呼叫過此擴充方法之後，結果為: 博XXXXX有限公司
        /// </summary>
        /// <param name="source">來源字串</param>
        /// <param name="target">要被置換的字元</param>
        /// <param name="startIndex">從哪個起始位置開始置換 (zero-base index)</param>
        /// <param name="length">置換多少個長度的字元</param>
        /// <returns>置換過後的字串</returns>
        public static string ReplaceChar(this string source, char target, int startIndex, int length = -1)
        {
            if (-1 == length)
            {
                length = source.Length;
            }

            length = startIndex + length;

            // 算是一種防止爆 Exception 的機制，也就是說會回傳一個字串，把指定的字元全部置換掉，一直到字串的尾端
            if (length > source.Length)
            {
                length = source.Length;
            }

            var result = "";
            var tokens = source.ToCharArray();

            for (var i = startIndex; i < length; i++)
            {
                tokens[i] = target;
            }

            result = new string(tokens);
            return result;
        }

        /// <summary>檢查是否為 Null 值或長度為 0 的空字串 (預設會先進行 Trim，在進行 Null 或空字串的檢查。)
        /// </summary>
        /// <param name="source">來源字串</param>
        /// <param name="trim">檢查之前是否會先自動 Trim 過，再進行檢查</param>
        /// <returns>是否為 Null 值或長度為 0 的空字串</returns>
        public static bool IsNullOrEmpty(this string source, bool trim = true)
        {
            var result = trim ? string.IsNullOrWhiteSpace(source) : string.IsNullOrEmpty(source);
            return result;
        }

        /// <summary>檢查是否為 Null 值或空字串，如果為 Null 或空字串會執行回呼處理委派 (預設會先進行 Trim)
        /// </summary>
        /// <param name="source">來源字串</param>
        /// <param name="handler">回呼處理委派</param>
        /// <param name="trim">檢查之前是否會先自動 Trim 過，再進行檢查</param>
        /// <returns>是否為 Null 值或空字串</returns>
        public static bool IsNullOrEmpty(this string source, Action handler, bool trim = true)
        {
            var result = source.IsNullOrEmpty(trim);

            if (result)
            {
                try
                {
                    handler();
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return result;
        }

        /// <summary>檢查是否為 Null 值或空字串，如果為 Null 或空字串會執行回呼處理委派 (預設會先進行 Trim)
        /// </summary>
        /// <param name="source">來源字串</param>
        /// <param name="handler">回呼處理委派</param>
        /// <param name="trim">檢查之前是否會先自動 Trim 過，再進行檢查</param>
        /// <returns>是否為 Null 值或空字串</returns>
        public static async Task<bool> IsNullOrEmpty(this string source, Func<Task> handler, bool trim = true)
        {
            var result = source.IsNullOrEmpty(trim);

            if (result)
            {
                try
                {
                    await handler();
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return result;
        }

        /// <summary>檢查是否為 Null 或空字串，如果是 Null 或空字串的情況，會自動回傳預設值，如果檢查失敗，則回傳原本的字串 (預設不會先進行 Trim)
        /// </summary>
        /// <param name="source">來源字串</param>
        /// <param name="value">當檢查到目標字串為 Null 或空字串時候取代的預設值</param>
        /// <param name="trim">檢查之前是否會先自動 Trim 過，再進行檢查</param>
        /// <returns></returns>
        public static string IsNullOrEmptyString(this string source, string value, bool trim = false)
        {
            var result = trim ? string.IsNullOrWhiteSpace(source) : string.IsNullOrEmpty(source);
            return result ? value : source;
        }

        /// <summary>檢查是否不為 Null 值或空字串，檢查前會先自動 Trim
        /// </summary>
        /// <param name="source">來源字串</param>
        /// <returns>是否不為 Null 值或空字串</returns>
        public static bool IsNotNullOrEmpty(this string source)
        {
            var result = !string.IsNullOrWhiteSpace(source);
            return result;
        }

        /// <summary>檢查是否不為 Null 值或空字串，如果不是 Null 或空字串會執行回呼處理委派，檢查前會先自動 Trim
        /// </summary>
        /// <param name="source">來源字串</param>
        /// <param name="handler">回呼處理委派</param>
        /// <returns>是否不為 Null 值或空字串</returns>
        public static bool IsNotNullOrEmpty(this string source, Action<string> handler)
        {
            var result = source.IsNotNullOrEmpty();

            if (result)
            {
                try
                {
                    handler(source);
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return result;
        }

        /// <summary>檢查是否不為 Null 值或空字串，如果不是 Null 或空字串會執行回呼處理委派，檢查前會先自動 Trim
        /// </summary>
        /// <param name="source">來源字串</param>
        /// <param name="asyncFunc">回呼處理委派</param>
        /// <returns>是否不為 Null 值或空字串</returns>
        public static async Task<bool> IsNotNullOrEmpty(this string source, Func<string, Task> asyncFunc)
        {
            var result = source.IsNotNullOrEmpty();

            if (result)
            {
                try
                {
                    await asyncFunc(source);
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return result;
        }

        /// <summary>具名參數字串格式化。使用方式如下:<para/>
        /// string message = "Hello {name}, how are you".Format( name => "Pony Lin" );  // message為 Hello Pony Lin<para/>
        /// string msg = "{framework} is very cool".Format( framework => new StringBuilder( "Zayni Framework" ) );     // msg為 Zayni Framework is very cool
        /// </summary>
        /// <example>
        /// <code>
        /// string message = "Hello {name}, how are you".Format( name => "Pony Lin" );
        /// string msg = "{framework} is very cool".Format( framework => new StringBuilder( "Zayni Framework" ) );
        /// </code>
        /// </example>
        /// <param name="source">格式化字串</param>
        /// <param name="args">參數表達式</param>
        /// <returns>格式化後的字串</returns>
        public static string Format(this string source, params Expression<Func<string, object>>[] args)
        {
            if (source.IsNullOrEmpty(true))
            {
                return string.Empty;
            }

            if (args.IsNullOrEmpty())
            {
                return source;
            }

            var parameters = args.ToDictionary(p => string.Format("{{{0}}}", p.Parameters[0].Name), g => g.Compile()(g.Parameters[0].Name));
            var sb = new StringBuilder(source);

            foreach (var parameter in parameters)
            {
                sb.Replace(parameter.Key, null != parameter.Value ? parameter.Value?.ToString() : "");
            }

            return sb.ToString();
        }

        /// <summary>以指定的字串當作分格符號，進行字串分割
        /// </summary>
        /// <param name="self"></param>
        /// <param name="delimiter">字串分隔符號</param>
        /// <param name="trim">是否要對分隔之後的字符陣列中的元素進行Trim的動作</param>
        /// <returns>分隔成功後的字符陣列</returns>
        public static string[] SplitString(this string self, string delimiter, bool trim = false)
        {
            var result = self.Split(new string[] { delimiter }, StringSplitOptions.RemoveEmptyEntries);

            if (trim)
            {
                result.TrimElements();
            }

            return result;
        }

        /// <summary>串連字串陣列的所有元素，並在每個元素之間使用指定的分隔符號
        /// </summary>
        /// <param name="self"></param>
        /// <param name="separator">字串分隔符號</param>
        /// <returns>串連完成之後的字串</returns>
        public static string Join(this string[] self, string separator) => string.Join(separator, self);

        /// <summary>對字串陣列中的所有元素進行 Trim 的動作
        /// </summary>
        /// <param name="self"></param>
        public static void TrimElements(this string[] self)
        {
            for (int i = 0, len = self.Length; i < len; i++)
            {
                var token = self[i] + "";
                self[i] = token.Trim();
            }
        }

        /// <summary>對字串針對指定的字元進行 Trim 的動作
        /// </summary>
        /// <param name="self"></param>
        /// <param name="trimChars">要被 Trim 掉的字元</param>
        public static string TrimChars(this string self, params char[] trimChars) => self.Trim(trimChars);

        /// <summary>移除字串中的不可視字元
        /// </summary>
        /// <param name="self"></param>
        /// <param name="removeControlCharacter">是否移除掉字串中的 ASCII Code 控制字元，預設會移除。</param>
        /// <returns>結果字串</returns>
        public static string TrimNonVisualCharacter(this string self, bool removeControlCharacter = true)
        {
            var result = string.Empty;

            if (self.IsNullOrEmpty())
            {
                return result;
            }

            result = self.Trim();

            var whitSpacesChars = new char[] {

                // SpaceSeparator category
                '\u0020', '\u1680', '\u180E', '\u2000', '\u2001', '\u2002', '\u2003',
                '\u2004', '\u2005', '\u2006', '\u2007', '\u2008', '\u2009', '\u200A',
                '\u202F', '\u205F', '\u3000',

                // LineSeparator category
                '\u2028',

                // ParagraphSeparator category
                '\u2029',

                // Latin1 characters
                '\u0009', '\u000A', '\u000B', '\u000C', '\u000D', '\u0085', '\u00A0',

                // ZERO WIDTH SPACE (U+200B) & ZERO WIDTH NO-BREAK SPACE (U+FEFF)
                '\u200B', '\uFEFF',

                // 全型空格
                '　',
            };

            result = result.Trim(whitSpacesChars);

            if (result.IsNullOrEmpty())
            {
                return result;
            }

            if (removeControlCharacter)
            {
                result = new string(result.Where(c => !char.IsControl(c)).ToArray());
            }

            result = (result + "").Trim();
            return result;
        }

        /// <summary>字串陣列中是否包含任何 Null 或空字串的元素 (預設不會先對所有字串元素先進行 Trim 的動作)
        /// </summary>
        /// <param name="self"></param>
        /// <param name="trim">是否要先對所有字串元素先進行 Trim 的動作，預設是不進行</param>
        /// <returns>是否包含任何 Null 或空字串的元素</returns>
        public static bool HasNullOrEmptyElemants(this string[] self, bool trim = false)
        {
            if (trim)
            {
                self.TrimElements();
            }

            return self.Any(q => q.IsNullOrEmpty());
        }

        /// <summary>根據指定的開始字串與結束字串，擷取子字串集合
        /// ex: "AAkkkBBAAiiiBB".FetchSubstring( "AA", "BB" ).ToArray(); 得到的結果為
        /// tokens[ 0 ] --> "kkk" ,
        /// tokens[ 1 ] --> "iii"
        /// </summary>
        /// <param name="self"></param>
        /// <param name="start">起始字串</param>
        /// <param name="end">結束字串</param>
        /// <param name="isIgnoreCare">起始與結束字串是否忽略大小寫(預設不忽略)</param>
        /// <param name="includeBoundary">結果子字串是否包含起始與結束字串(預設不包含)</param>
        /// <returns>子字串陣列集合</returns>
        public static IEnumerable<string> FetchSubstring(this string self, string start, string end, bool isIgnoreCare = false, bool includeBoundary = false)
        {
            var result = default(IEnumerable<string>);

            if (new string[] { start, end }.HasNullOrEmptyElemants())
            {
                return result;
            }

            try
            {
                var regex = string.Format("{0}(.*?){1}", Regex.Escape(start), Regex.Escape(end));

                var matches = isIgnoreCare ?
                                Regex.Matches(self, regex, RegexOptions.Singleline | RegexOptions.IgnoreCase) :
                                Regex.Matches(self, regex, RegexOptions.Singleline);

                var tokens = matches.Cast<Match>().Select(m => m.Groups[1].Value);
                result = includeBoundary ? tokens.Select(q => $"{start}{q}{end}") : tokens;
            }
            catch (Exception ex)
            {
                Command.StdoutErr(ex.ToString());
                throw;
            }

            return result;
        }

        #endregion String Extension Methods


        #region Exteions Methods for DateTime String

        /// <summary>依照指定的日期時間格式，嘗試轉換日期時間字串。使用方式如下:
        /// DateTime dt;
        /// bool isSuccess;
        /// isSuccess = "20160526".TryParseDateTime( "yyyyMMdd", out dt );
        /// isSuccess = "201605".TryParseDateTime( "yyyyMM", out dt );
        /// isSuccess = "2016-05/26   14:23:45".TryParseDateTime( "yyyy-MM/dd   HH:mm:ss", out dt );
        /// </summary>
        /// <example>
        /// <code>
        /// DateTime dt;
        /// bool isSuccess;
        /// isSuccess = "20160526".TryParseDateTime( "yyyyMMdd", out dt );
        /// isSuccess = "201605".TryParseDateTime( "yyyyMM", out dt );
        /// isSuccess = "2016-05/26   14:23:45".TryParseDateTime( "yyyy-MM/dd   HH:mm:ss", out dt );
        /// </code>
        /// </example>
        /// <param name="source">來源日期時間字串</param>
        /// <param name="dateTimeFormat">日期時間格式字串</param>
        /// <param name="dateTime">輸出日期時間物件</param>
        /// <returns>轉換是否成功</returns>
        public static bool TryParseDateTime(this string source, string dateTimeFormat, out DateTime dateTime)
        {
            dateTime = default(DateTime);

            if (new string[] { source, dateTimeFormat }.HasNullOrEmptyElemants())
            {
                return false;
            }

            try
            {
                dateTime = DateTime.ParseExact(source, dateTimeFormat, null);
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        /// <summary>依照指定的日期時間格式，將字串轉換為日期時間
        /// </summary>
        /// <param name="source">來源日期時間字串</param>
        /// <param name="dateTimeFormat">日期時間格式字串</param>
        /// <returns>日期時間物件</returns>
        public static DateTime ParseDateTime(this string source, string dateTimeFormat) =>
            DateTime.ParseExact(source, dateTimeFormat, null);

        /// <summary>嘗試轉換 ISO-8601 日期時間格式的字串為 DateTime? 型別的物件
        /// <para>* 任何非 UTC 時區的日期時間字串，不會被判定為合法，也不會進行 DateTime 型別轉換。</para>
        /// <para>* 如果為合法的 ISO-8601 日期時間格式字串，會回傳 `true`，必且 `DateTime? value` 參數會輸出 `DateTimeKind.Utc` 的 `DateTime?` 物件。</para>
        /// <para>* 可以接受的 ISO-8601 UTC 日期時間格式字串如下:</para>
        /// <para>  * `2020-07-05T00:00:00Z`</para>
        /// <para>  * `2020-07-05T00:00:00.000Z`</para>
        /// <para>  * `2020-07-05T00:00:00.000123Z`</para>
        /// <para>  * `2020-11-30T00:00:00+00:00`</para>
        /// <para>  * `2020-11-30T00:00:00.000+00:00`</para>
        /// <para>  * `2020-11-30T00:00:00.000115+00:00`</para>
        /// </summary>
        /// <param name="source">來源字串</param>
        /// <param name="value">轉換成功的 DateTime? 物件
        /// * 如果轉換成功，`DateTime.Value.Kind` 屬性輸出為 `DateTimeKind.Utc`
        /// </param>
        /// <returns>轉換是否成功</returns>
        public static bool TryParseISO8601UtcDateTime(this string source, out DateTime? value)
        {
            if (!TryParseISO8601UtcDateTime(source, out DateTime dt))
            {
                value = null;
                return false;
            }

            value = (DateTime?)dt;
            return true;
        }

        /// <summary>嘗試轉換 ISO-8601 日期時間格式的字串為 DateTime 型別的物件
        /// <para>* 任何非 UTC 時區的日期時間字串，不會被判定為合法，也不會進行 DateTime 型別轉換。</para>
        /// <para>* 如果為合法的 ISO-8601 日期時間格式字串，會回傳 `true`，必且 `DateTime value` 參數會輸出 `DateTimeKind.Utc` 的 `DateTime` 物件。</para>
        /// <para>* 可以接受的 ISO-8601 UTC 日期時間格式字串如下:</para>
        /// <para>  * `2020-07-05T00:00:00Z`</para>
        /// <para>  * `2020-07-05T00:00:00.000Z`</para>
        /// <para>  * `2020-07-05T00:00:00.000123Z`</para>
        /// <para>  * `2020-11-30T00:00:00+00:00`</para>
        /// <para>  * `2020-11-30T00:00:00.000+00:00`</para>
        /// <para>  * `2020-11-30T00:00:00.000115+00:00`</para>
        /// </summary>
        /// <param name="source">來源字串</param>
        /// <param name="value">轉換成功的 DateTime? 物件
        /// * 如果轉換成功，`DateTime.Kind` 屬性輸出為 `DateTimeKind.Utc`
        /// </param>
        /// <returns>轉換是否成功</returns>
        public static bool TryParseISO8601UtcDateTime(this string source, out DateTime value)
        {
            var iso8601Format = new string[]
            {
                // Basic formats
                "yyyyMMddTHHmmsszzz",
                "yyyyMMddTHHmmssfffzzz",
                "yyyyMMddTHHmmssffffffzzz",
                "yyyyMMddTHHmmssfffffffzzz",
                "yyyyMMddTHHmmsszz",
                "yyyyMMddTHHmmssfffzz",
                "yyyyMMddTHHmmssffffffzz",
                "yyyyMMddTHHmmssfffffffzz",
                "yyyyMMddTHHmmssZ",
                "yyyyMMddTHHmmssfffZ",
                "yyyyMMddTHHmmssffffffZ",
                "yyyyMMddTHHmmssfffffffZ",

                // Extended formats
                "yyyy-MM-ddTHH:mm:sszzz",
                "yyyy-MM-ddTHH:mm:ss.fffzzz",
                "yyyy-MM-ddTHH:mm:ss.ffffffzzz",
                "yyyy-MM-ddTHH:mm:ss.fffffffzzz",
                "yyyy-MM-ddTHH:mm:sszz",
                "yyyy-MM-ddTHH:mm:ss.fffzz",
                "yyyy-MM-ddTHH:mm:ss.ffffffzz",
                "yyyy-MM-ddTHH:mm:ss.fffffffzz",
                "yyyy-MM-ddTHH:mm:ssZ",
                "yyyy-MM-ddTHH:mm:ss.fffZ",
                "yyyy-MM-ddTHH:mm:ss.ffffffZ",
                "yyyy-MM-ddTHH:mm:ss.fffffffZ",

                // All of the above with reduced accuracy
                "yyyyMMddTHHmmzzz",
                "yyyyMMddTHHmmzz",
                "yyyyMMddTHHmmZ",
                "yyyy-MM-ddTHH:mmzzz",
                "yyyy-MM-ddTHH:mmzz",
                "yyyy-MM-ddTHH:mmZ",

                // Accuracy reduced to hours
                "yyyyMMddTHHzzz",
                "yyyyMMddTHHzz",
                "yyyyMMddTHHZ",
                "yyyy-MM-ddTHHzzz",
                "yyyy-MM-ddTHHzz",
                "yyyy-MM-ddTHHZ",

                "o"
            };

            if (!DateTime.TryParseExact(source, iso8601Format, CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal, out value))
            {
                return false;
            }

            value = new DateTime(value.Ticks, DateTimeKind.Utc);
            return true;
        }

        #endregion Exteions Methods for DateTime String


        #region Extension Methods for Encoding

        /// <summary>對字串進行 UTF8 編碼之後，轉換成二進位 Byte 陣列資料
        /// </summary>
        /// <param name="self"></param>
        /// <returns>二進位 Byte 陣列資料</returns>
        public static byte[] ToUtf8Bytes(this string self) => Encoding.UTF8.GetBytes(self);

        /// <summary>將字串使用 UTF8 格式進行 Base64 字串編碼
        /// </summary>
        /// <param name="self"></param>
        /// <returns>Base64 編碼過後的字串</returns>
        public static string Base64Encode(this string self)
        {
            string result;

            if (self.IsNullOrEmpty())
            {
                return string.Empty;
            }

            try
            {
                var bytes = UTF8Encoding.UTF8.GetBytes(self);
                result = Convert.ToBase64String(bytes);
            }
            catch (Exception ex)
            {
                Command.StdoutErr(ex.ToString());
                throw;
            }

            return result;
        }

        /// <summary>將編碼過的字串使用 UTF8 格式進行 Base64 字串解碼
        /// </summary>
        /// <param name="self"></param>
        /// <returns>解碼過後的字串</returns>
        public static string Base64Decode(this string self)
        {
            string result;

            if (self.IsNullOrEmpty())
            {
                return string.Empty;
            }

            try
            {
                var bytes = Convert.FromBase64String(self);
                result = UTF8Encoding.UTF8.GetString(bytes);
            }
            catch (Exception ex)
            {
                Command.StdoutErr(ex.ToString());
                throw;
            }

            return result;
        }

        #endregion Extension Methods for Encoding


        #region Extension Methods for Compression

        /// <summary>將字串壓縮成GZip壓縮格式Base64編碼的字串
        /// </summary>
        /// <param name="self"></param>
        /// <returns>GZip壓縮格式Base64編碼的字串</returns>
        public static string GZipCompress(this string self)
        {
            try
            {
                var bytes = Encoding.UTF8.GetBytes(self);

                using var msi = new MemoryStream(bytes);
                using var mso = new MemoryStream();
                using (var gs = new GZipStream(mso, CompressionMode.Compress))
                {
                    msi.CopyTo(gs);
                }

                return Convert.ToBase64String(mso.ToArray());
            }
            catch (Exception ex)
            {
                Command.StdoutErr(ex.ToString());
                throw;
            }
        }

        /// <summary>將字串壓縮成 GZip 壓縮格式Base64編碼的字串
        /// </summary>
        /// <param name="self"></param>
        /// <returns>GZip 壓縮格式的二進位資料</returns>
        public static byte[] GZipCompress2Binary(this string self)
        {
            try
            {
                var bytes = Encoding.UTF8.GetBytes(self);

                using var msi = new MemoryStream(bytes);
                using var mso = new MemoryStream();
                using (var gs = new GZipStream(mso, CompressionMode.Compress))
                {
                    msi.CopyTo(gs);
                }

                return mso.ToArray();
            }
            catch (Exception ex)
            {
                Command.StdoutErr(ex.ToString());
                throw;
            }
        }

        /// <summary>將 GZip 壓縮格式 Base64 編碼的字串解壓縮成一般字串
        /// </summary>
        /// <param name="self"></param>
        /// <returns>一般字串</returns>
        public static string GZipDecompress(this string self)
        {
            try
            {
                var bytes = Convert.FromBase64String(self);

                using var msi = new MemoryStream(bytes);
                using var mso = new MemoryStream();
                using (var gs = new GZipStream(msi, CompressionMode.Decompress))
                {
                    gs.CopyTo(mso);
                }

                return Encoding.UTF8.GetString(mso.ToArray());
            }
            catch (Exception ex)
            {
                Command.StdoutErr(ex.ToString());
                throw;
            }
        }

        #endregion Extension Methods for Compression
    }
}
