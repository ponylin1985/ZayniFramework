using System;
using System.Threading.Tasks;


namespace ZayniFramework.Common
{
    /// <summary>Double 或 Double? 型別的擴充類別
    /// </summary>
    public static class DoubleExtension
    {
        /// <summary>檢查 Double 是否為 .NET 的 Double 預設值。
        /// </summary>
        /// <param name="self">Double 來源</param>
        /// <returns></returns>
        public static bool IsDotNetDefault(this double self) => self == default(double);

        /// <summary>檢查 Double 數字是否為 .NET CLR 的系統預設值 0，如果是則將自動使用傳入的 Double 整數值取代，如果不是，則不會有取代的效果。
        /// </summary>
        /// <param name="self">Double 來源數值</param>
        /// <param name="value">取代預設值 0 的 Double 整數</param>
        /// <returns></returns>
        public static double IsDotNetDefault(this double self, double value) => default(double) == self ? value : self;

        /// <summary>檢查 Double? 數值是否為 null 或 .NET Double 的預設值。
        /// </summary>
        /// <param name="self">Double? 來源數值</param>
        /// <returns>是否為 null 或 .NET Double 的預設值。</returns>
        public static bool IsNullOrDefault(this double? self) => null == self || self == default(double);

        /// <summary>檢查 Double? 數值是否不為 null 或 .NET Double 的預設值。
        /// </summary>
        /// <param name="self">Double? 來源數值</param>
        /// <returns>是否不為 null 或 .NET Double 的預設值。</returns>
        public static bool IsNotNullOrDefault(this double? self) => !self.IsNullOrDefault();

        /// <summary>檢查 Double? 數值是否為 null 或 .NET Double 的預設值，如果為 true 的情況則自動使用指定的 Double 數值回傳。
        /// </summary>
        /// <param name="self">Double? 來源數值</param>
        /// <param name="value">取代的 Double 數值</param>
        /// <returns>取代的 Double 數值</returns>
        public static double? IsNullOrDefault(this double? self, double value) => self.IsNullOrDefault() ? value : self;

        /// <summary>當為 null 或 .NET Double 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">Double? 來源數值</param>
        /// <param name="handler">處理委派</param>
        /// <returns>原始 Double? 數值</returns>
        public static double? WhenNullOrDefault(this double? self, Action handler)
        {
            if (self.IsNullOrDefault())
            {
                try
                {
                    handler();
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }

        /// <summary>當為 null 或 .NET Double 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">Double? 來源數值</param>
        /// <param name="asyncFunc">非同步作業處理委派</param>
        /// <returns>原始 Double? 數值</returns>
        public static async Task<double?> WhenNullOrDefault(this double? self, Func<Task> asyncFunc)
        {
            if (self.IsNullOrDefault())
            {
                try
                {
                    await asyncFunc();
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }

        /// <summary>當為 null 或 .NET Double 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">Double? 來源數值</param>
        /// <param name="handler">處理委派</param>
        /// <returns>處理後的 Double? 數值</returns>
        public static double? WhenNullOrDefault(this double? self, Func<double?> handler)
        {
            if (self.IsNullOrDefault())
            {
                try
                {
                    return handler();
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }

        /// <summary>當為 null 或 .NET Double 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">Double? 來源數值</param>
        /// <param name="asyncFunc">非同步作業處理委派</param>
        /// <returns>處理後的 Double? 數值</returns>
        public static async Task<double?> WhenNullOrDefault(this double? self, Func<Task<double?>> asyncFunc)
        {
            if (self.IsNullOrDefault())
            {
                try
                {
                    return await asyncFunc();
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }

        /// <summary>當不為 null 或 .NET Double 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">Double? 來源數值</param>
        /// <param name="handler">處理委派</param>
        /// <returns>原始 Double? 數值</returns>
        public static double? WhenNotNullOrDefault(this double? self, Action<double?> handler)
        {
            if (self.IsNotNullOrDefault())
            {
                try
                {
                    handler(self);
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }

        /// <summary>當不為 null 或 .NET Double 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">Double? 來源數值</param>
        /// <param name="asyncFunc">非同步作業處理委派</param>
        /// <returns>原始 Double? 數值</returns>
        public static async Task<double?> WhenNotNullOrDefault(this double? self, Func<double?, Task> asyncFunc)
        {
            if (self.IsNotNullOrDefault())
            {
                try
                {
                    await asyncFunc(self);
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }

        /// <summary>當不為 null 或 .NET Double 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">Double? 來源數值</param>
        /// <param name="handler">處理委派</param>
        /// <returns>處理後的 Double? 數值</returns>
        public static double? WhenNotNullOrDefault(this double? self, Func<double?, double?> handler)
        {
            if (self.IsNotNullOrDefault())
            {
                try
                {
                    return handler(self);
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }

        /// <summary>當不為 null 或 .NET Double 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">Double? 來源數值</param>
        /// <param name="asyncFunc">非同步作業處理委派</param>
        /// <returns>處理後的 Double? 數值</returns>
        public static async Task<double?> WhenNotNullOrDefault(this double? self, Func<double?, Task<double?>> asyncFunc)
        {
            if (self.IsNotNullOrDefault())
            {
                try
                {
                    return await asyncFunc(self);
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }

        /// <summary>檢查是否為一個真正有浮點數的小數值的 Double 數值
        /// </summary>
        /// <param name="source"></param>
        /// <returns>是否為一個真正的小數值的 Double 數值</returns>
        public static bool IsRealDouble(this double source)
        {
            var target = source + "";

            if (!target.Contains("."))
            {
                return false;
            }

            var tokens = target.Split('.');

            if (2 != tokens.Length)
            {
                return false;
            }

            var decimalPart = "";

            try
            {
                decimalPart = tokens[1];
            }
            catch (Exception)
            {
                return false;
            }

            var i = int.Parse(decimalPart);
            return 0 != i;
        }

        /// <summary>檢查是否為一個真正有浮點數的小數值的 Double 數值
        /// </summary>
        /// <param name="source"></param>
        /// <returns>是否為一個真正的小數值的 Double 數值</returns>
        public static bool IsRealDouble(this double? source)
        {
            if (source.IsNull())
            {
                return false;
            }

            var target = source + "";

            if (target.IsNullOrEmpty() || !target.Contains("."))
            {
                return false;
            }

            var tokens = target.Split('.');

            if (2 != tokens.Length)
            {
                return false;
            }

            var decimalPart = "";

            try
            {
                decimalPart = tokens[1];
            }
            catch (Exception)
            {
                return false;
            }

            var i = int.Parse(decimalPart);
            return 0 != i;
        }

        /// <summary>取得 double 數值小數位數完整的長度，包括小數位數尾數為 0 的長度。<para/>
        /// * 當不包含小數位數值，則直接回傳 0。<para/>
        /// </summary>
        /// <param name="source">來源 double 數值</param>
        /// <returns>double 數值小數位數完整的長度，包括小數位數尾數為 0 的長度。</returns>
        public static int LengthOfDecimalPlaces(this double source)
        {
            var doubleNumber = source.ToString();

            var tokens = doubleNumber.Split('.');

            if (tokens.Length != 2)
            {
                return 0;
            }

            return tokens[1].Length;
        }

        /// <summary>取得 double? 數值小數位數完整的長度，包括小數位數尾數為 0 的長度。<para/>
        /// * 當不包含小數位數值，則直接回傳 0。<para/>
        /// * 當 double? 為 null 時，則直接回傳 0。<para/>
        /// </summary>
        /// <param name="source">來源 double? 數值</param>
        /// <returns>double? 數值小數位數完整的長度，包括小數位數尾數為 0 的長度。</returns>
        public static int LengthOfDecimalPlaces(this double? source)
        {
            if (source.IsNull() || !source.HasValue)
            {
                return 0;
            }

            return source.Value.LengthOfDecimalPlaces();
        }

        /// <summary>取得 double 有效的小數位數長度，代表小數位數尾數為 0 的長度不包含。<para/>
        /// * 當不包含小數位數值，則直接回傳 0。<para/>
        /// </summary>
        /// <param name="source">來源 double 數值</param>
        /// <returns>double 有效的小數位數長度，代表小數位數尾數為 0 的長度不包含。</returns>
        public static int LengthOfEfftiveDecimalPlaces(this double source)
        {
            var doubleNumber = source.ToString().TrimEnd('0');

            var tokens = doubleNumber.Split('.');

            if (tokens.Length != 2)
            {
                return 0;
            }

            return tokens[1].Length;
        }

        /// <summary>取得 double? 有效的小數位數長度，代表小數位數尾數為 0 的長度不包含。<para/>
        /// * 當不包含小數位數值，則直接回傳 0。<para/>
        /// </summary>
        /// <param name="source">來源 double? 數值</param>
        /// <returns>double? 有效的小數位數長度，代表小數位數尾數為 0 的長度不包含。</returns>
        public static int LengthOfEfftiveDecimalPlaces(this double? source)
        {
            if (source.IsNull() || !source.HasValue)
            {
                return 0;
            }

            return source.Value.LengthOfEfftiveDecimalPlaces();
        }

        /// <summary>對來源的 double 浮點數以指定的小數位數進行「無條件捨去」處理。<para/>
        /// * lengthOfDoublePlaces 參數合法值域為: 0 &lt;= lengthOfDoublePlaces &lt; 38<para/>
        /// </summary>
        /// <param name="source">來源的 double 數值</param>
        /// <param name="lengthOfDecimalPlaces">小數位數長度<para/>
        /// * 合法值域為: 0 &lt;= lengthOfDoublePlaces &lt; 38
        /// </param>
        /// <returns>依照指定小數位數長度無條件捨去處理後的 double 數值</returns>
        public static double FloorDecimalPlaces(this double source, int lengthOfDecimalPlaces = 4)
        {
            if (source.LengthOfDecimalPlaces() == lengthOfDecimalPlaces)
            {
                return source;
            }

            // 這邊故意將小位數位上限設定為 38 是因為 MSSQL 接受 double 參數時最大的小數位數上限就是 38。
            if (lengthOfDecimalPlaces <= 0 || lengthOfDecimalPlaces > 38)
            {
                throw new ArgumentOutOfRangeException(
                    $"Invalid argument. The '{nameof(lengthOfDecimalPlaces)}' is out of range. {nameof(lengthOfDecimalPlaces)}: {lengthOfDecimalPlaces}.");
            }

            var zeroString = "";
            For.Do(lengthOfDecimalPlaces, () => zeroString += "0");
            For.Reset();

            var decimalPlaces = int.Parse($"1{zeroString}");
            var doubleNumber = Math.Floor(source * decimalPlaces) / decimalPlaces;         // 依照指定的小數位數，進行無條件捨去。
            var result = double.Parse(doubleNumber.ToString($"#0.{zeroString}"));  // 如果在尾數不足的情況下，強制進行補 0，確保回傳的小數位數數值會是指定的長度。
            return result;
        }

        /// <summary>對來源的 double? 浮點數以指定的小數位數進行「無條件捨去」處理。<para/>
        /// * lengthOfDoublePlaces 參數合法值域為: 0 &lt;= lengthOfDoublePlaces &lt; 38<para/>
        /// * 當來源的 double? 為 null 時，則直接回傳 0M，但仍會包含指定的小數位數長度。
        /// </summary>
        /// <param name="source">來源的 double? 數值</param>
        /// <param name="lengthOfDecimalPlaces">小數位數長度<para/>
        /// * 合法值域為: 0 &lt;= lengthOfDoublePlaces &lt; 38
        /// </param>
        /// <returns>依照指定小數位數長度無條件捨去處理後的 double 數值</returns>
        public static double FloorDecimalPlaces(this double? source, int lengthOfDecimalPlaces = 4)
        {
            // 這邊故意將小位數位上限設定為 38 是因為 MSSQL 接受 double 參數時最大的小數位數上限就是 38。
            if (lengthOfDecimalPlaces <= 0 || lengthOfDecimalPlaces > 38)
            {
                throw new ArgumentOutOfRangeException(
                    $"Invalid argument. The '{nameof(lengthOfDecimalPlaces)}' is out of range. {nameof(lengthOfDecimalPlaces)}: {lengthOfDecimalPlaces}.");
            }

            if (source.IsNull() || !source.HasValue)
            {
                return 0.0000D.FloorDecimalPlaces(lengthOfDecimalPlaces);
            }

            return source.Value.FloorDecimalPlaces(lengthOfDecimalPlaces);
        }
    }
}