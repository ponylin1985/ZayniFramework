using System;
using System.Threading.Tasks;


namespace ZayniFramework.Common
{
    /// <summary>Guid 的擴充類別
    /// </summary>
    public static class GuidExtension
    {
        /// <summary>檢查 Guid 是否為 .NET 的 Guid 預設值。
        /// </summary>
        /// <param name="self">Guid 來源</param>
        /// <returns></returns>
        public static bool IsDotNetDefault(this Guid self) => self == default(Guid) || self == Guid.Empty;

        /// <summary>檢查 Guid 是否為 .NET 的 Guid 預設值，如果 true 則使用指定的替代值回傳。
        /// </summary>
        /// <param name="self">Guid 來源</param>
        /// <param name="value">取代的 Guid</param>
        /// <returns>取代的 Guid</returns>
        public static Guid IsDotNetDefault(this Guid self, Guid value) => self.IsDotNetDefault() ? value : self;

        /// <summary>檢查 Guid? 是否為 null 或 .NET Guid 的預設值。
        /// </summary>
        /// <param name="self">Guid? 來源</param>
        /// <returns>是否為 null 或 .NET Guid 的預設值。</returns>
        public static bool IsNullOrDefault(this Guid? self) => null == self || self == default(Guid);

        /// <summary>檢查 Guid? 是否不為 null 或 .NET Guid 的預設值。
        /// </summary>
        /// <param name="self">Guid? 來源</param>
        /// <returns>是否不為 null 或 .NET Guid 的預設值。</returns>
        public static bool IsNotNullOrDefault(this Guid? self) => !self.IsNullOrDefault();

        /// <summary>檢查 Guid? 是否為 null 或 .NET Guid 的預設值，如果為 true 的情況則自動使用指定的 Guid 回傳。
        /// </summary>
        /// <param name="self">Guid? 來源</param>
        /// <param name="value">取代的 Guid </param>
        /// <returns>取代的 Guid </returns>
        public static Guid? IsNullOrDefault(this Guid? self, Guid value) => self.IsNullOrDefault() ? value : self;

        /// <summary>當為 null 或 .NET Guid 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">Guid? 來源</param>
        /// <param name="handler">處理委派</param>
        /// <returns>原始 Guid? </returns>
        public static Guid? WhenNullOrDefault(this Guid? self, Action handler)
        {
            if (self.IsNullOrDefault())
            {
                try
                {
                    handler();
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }

        /// <summary>當為 null 或 .NET Guid 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">Guid? 來源</param>
        /// <param name="asyncFunc">非同步作業處理委派</param>
        /// <returns>原始 Guid? </returns>
        public static async Task<Guid?> WhenNullOrDefault(this Guid? self, Func<Task> asyncFunc)
        {
            if (self.IsNullOrDefault())
            {
                try
                {
                    await asyncFunc();
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }

        /// <summary>當為 null 或 .NET Guid 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">Guid? 來源</param>
        /// <param name="handler">處理委派</param>
        /// <returns>處理後的 Guid? </returns>
        public static Guid? WhenNullOrDefault(this Guid? self, Func<Guid?> handler)
        {
            if (self.IsNullOrDefault())
            {
                try
                {
                    return handler();
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }

        /// <summary>當為 null 或 .NET Guid 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">Guid? 來源</param>
        /// <param name="asyncFunc">非同步作業處理委派</param>
        /// <returns>處理後的 Guid? </returns>
        public static async Task<Guid?> WhenNullOrDefault(this Guid? self, Func<Task<Guid?>> asyncFunc)
        {
            if (self.IsNullOrDefault())
            {
                try
                {
                    return await asyncFunc();
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }

        /// <summary>當不為 null 或 .NET Guid 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">Guid? 來源</param>
        /// <param name="handler">處理委派</param>
        /// <returns>原始 Guid? </returns>
        public static Guid? WhenNotNullOrDefault(this Guid? self, Action<Guid?> handler)
        {
            if (self.IsNotNullOrDefault())
            {
                try
                {
                    handler(self);
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }

        /// <summary>當不為 null 或 .NET Guid 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">Guid? 來源</param>
        /// <param name="asyncFunc">非同步作業處理委派</param>
        /// <returns>原始 Guid? </returns>
        public static async Task<Guid?> WhenNotNullOrDefault(this Guid? self, Func<Guid?, Task> asyncFunc)
        {
            if (self.IsNotNullOrDefault())
            {
                try
                {
                    await asyncFunc(self);
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }

        /// <summary>當不為 null 或 .NET Guid 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">Guid? 來源</param>
        /// <param name="handler">處理委派</param>
        /// <returns>處理後的 Guid? </returns>
        public static Guid? WhenNotNullOrDefault(this Guid? self, Func<Guid?, Guid?> handler)
        {
            if (self.IsNotNullOrDefault())
            {
                try
                {
                    return handler(self);
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }

        /// <summary>當不為 null 或 .NET Guid 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">Guid? 來源</param>
        /// <param name="asyncFunc">非同步作業處理委派</param>
        /// <returns>處理後的 Guid? </returns>
        public static async Task<Guid?> WhenNotNullOrDefault(this Guid? self, Func<Guid?, Task<Guid?>> asyncFunc)
        {
            if (self.IsNotNullOrDefault())
            {
                try
                {
                    return await asyncFunc(self);
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }
    }
}