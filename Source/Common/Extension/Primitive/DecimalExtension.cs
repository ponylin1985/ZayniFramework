﻿using System;
using System.Threading.Tasks;


namespace ZayniFramework.Common
{
    /// <summary>Decimal 或 Decimal? 型別的擴充類別
    /// </summary>
    public static class DecimalExtension
    {
        /// <summary>檢查 Decimal 是否為 .NET 的 Decimal 預設值。
        /// </summary>
        /// <param name="self">Decimal 來源</param>
        /// <returns></returns>
        public static bool IsDotNetDefault(this decimal self) => self == default(decimal);

        /// <summary>檢查 Decimal 數字是否為 .NET CLR 的系統預設值 0，如果是則將自動使用傳入的 Decimal 整數值取代，如果不是，則不會有取代的效果。
        /// </summary>
        /// <param name="self">Decimal 來源數值</param>
        /// <param name="value">取代預設值 0 的 Decimal 整數</param>
        /// <returns></returns>
        public static decimal IsDotNetDefault(this decimal self, decimal value) => default(decimal) == self ? value : self;

        /// <summary>檢查 Decimal? 數值是否為 null 或 .NET Decimal 的預設值。
        /// </summary>
        /// <param name="self">Decimal? 來源數值</param>
        /// <returns>是否為 null 或 .NET Decimal 的預設值。</returns>
        public static bool IsNullOrDefault(this decimal? self) => null == self || self == default(decimal);

        /// <summary>檢查 Decimal? 數值是否不為 null 或 .NET Decimal 的預設值。
        /// </summary>
        /// <param name="self">Decimal? 來源數值</param>
        /// <returns>是否不為 null 或 .NET Decimal 的預設值。</returns>
        public static bool IsNotNullOrDefault(this decimal? self) => !self.IsNullOrDefault();

        /// <summary>檢查 Decimal? 數值是否為 null 或 .NET Decimal 的預設值，如果為 true 的情況則自動使用指定的 Decimal 數值回傳。
        /// </summary>
        /// <param name="self">Decimal? 來源數值</param>
        /// <param name="value">取代的 Decimal 數值</param>
        /// <returns>取代的 Decimal 數值</returns>
        public static decimal? IsNullOrDefault(this decimal? self, decimal value) => self.IsNullOrDefault() ? value : self;

        /// <summary>當為 null 或 .NET Decimal 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">Decimal? 來源數值</param>
        /// <param name="handler">處理委派</param>
        /// <returns>原始 Decimal? 數值</returns>
        public static decimal? WhenNullOrDefault(this decimal? self, Action handler)
        {
            if (self.IsNullOrDefault())
            {
                try
                {
                    handler();
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }

        /// <summary>當為 null 或 .NET Decimal 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">Decimal? 來源數值</param>
        /// <param name="asyncFunc">非同步作業處理委派</param>
        /// <returns>原始 Decimal? 數值</returns>
        public static async Task<decimal?> WhenNullOrDefault(this decimal? self, Func<Task> asyncFunc)
        {
            if (self.IsNullOrDefault())
            {
                try
                {
                    await asyncFunc();
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }

        /// <summary>當為 null 或 .NET Decimal 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">Decimal? 來源數值</param>
        /// <param name="handler">處理委派</param>
        /// <returns>處理後的 Decimal? 數值</returns>
        public static decimal? WhenNullOrDefault(this decimal? self, Func<decimal?> handler)
        {
            if (self.IsNullOrDefault())
            {
                try
                {
                    return handler();
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }

        /// <summary>當為 null 或 .NET Decimal 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">Decimal? 來源數值</param>
        /// <param name="asyncFunc">非同步作業處理委派</param>
        /// <returns>處理後的 Decimal? 數值</returns>
        public static async Task<decimal?> WhenNullOrDefault(this decimal? self, Func<Task<decimal?>> asyncFunc)
        {
            if (self.IsNullOrDefault())
            {
                try
                {
                    return await asyncFunc();
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }

        /// <summary>當不為 null 或 .NET Decimal 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">Decimal? 來源數值</param>
        /// <param name="handler">處理委派</param>
        /// <returns>原始 Decimal? 數值</returns>
        public static decimal? WhenNotNullOrDefault(this decimal? self, Action<decimal?> handler)
        {
            if (self.IsNotNullOrDefault())
            {
                try
                {
                    handler(self);
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }

        /// <summary>當不為 null 或 .NET Decimal 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">Decimal? 來源數值</param>
        /// <param name="asyncFunc">非同步作業處理委派</param>
        /// <returns>原始 Decimal? 數值</returns>
        public static async Task<decimal?> WhenNotNullOrDefault(this decimal? self, Func<decimal?, Task> asyncFunc)
        {
            if (self.IsNotNullOrDefault())
            {
                try
                {
                    await asyncFunc(self);
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }

        /// <summary>當不為 null 或 .NET Decimal 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">Decimal? 來源數值</param>
        /// <param name="handler">處理委派</param>
        /// <returns>處理後的 Decimal? 數值</returns>
        public static decimal? WhenNotNullOrDefault(this decimal? self, Func<decimal?, decimal?> handler)
        {
            if (self.IsNotNullOrDefault())
            {
                try
                {
                    return handler(self);
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }

        /// <summary>當不為 null 或 .NET Decimal 的預設值時，執行指定的處理委派。
        /// </summary>
        /// <param name="self">Decimal? 來源數值</param>
        /// <param name="asyncFunc">非同步作業處理委派</param>
        /// <returns>處理後的 Decimal? 數值</returns>
        public static async Task<decimal?> WhenNotNullOrDefault(this decimal? self, Func<decimal?, Task<decimal?>> asyncFunc)
        {
            if (self.IsNotNullOrDefault())
            {
                try
                {
                    return await asyncFunc(self);
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return self;
        }

        /// <summary>檢查是否為一個真正有浮點數的小數值的 Decimal 數值
        /// </summary>
        /// <param name="source"></param>
        /// <returns>是否為一個真正的小數值的 Decimal 數值</returns>
        public static bool IsRealDecimal(this decimal source)
        {
            var target = source + "";

            if (!target.Contains("."))
            {
                return false;
            }

            var tokens = target.Split('.');

            if (2 != tokens.Length)
            {
                return false;
            }

            var decimalPart = "";

            try
            {
                decimalPart = tokens[1];
            }
            catch (Exception)
            {
                return false;
            }

            var i = decimal.Parse(decimalPart);
            return 0 != i;
        }

        /// <summary>檢查是否為一個真正有浮點數的小數值的 Decimal 數值
        /// </summary>
        /// <param name="source"></param>
        /// <returns>是否為一個真正的小數值的 Decimal 數值</returns>
        public static bool IsRealDecimal(this decimal? source)
        {
            if (source.IsNull())
            {
                return false;
            }

            var target = source + "";

            if (target.IsNullOrEmpty() || !target.Contains("."))
            {
                return false;
            }

            var tokens = target.Split('.');

            if (2 != tokens.Length)
            {
                return false;
            }

            var decimalPart = "";

            try
            {
                decimalPart = tokens[1];
            }
            catch (Exception)
            {
                return false;
            }

            var i = decimal.Parse(decimalPart);
            return 0 != i;
        }

        /// <summary>取得 decimal 數值小數位數完整的長度，包括小數位數尾數為 0 的長度。<para/>
        /// * 當不包含小數位數值，則直接回傳 0。<para/>
        /// </summary>
        /// <param name="source">來源 decimal 數值</param>
        /// <returns>decimal 數值小數位數完整的長度，包括小數位數尾數為 0 的長度。</returns>
        public static int LengthOfDecimalPlaces(this decimal source)
        {
            var decimalNumber = source.ToString();

            var tokens = decimalNumber.Split('.');

            if (tokens.Length != 2)
            {
                return 0;
            }

            return tokens[1].Length;
        }

        /// <summary>取得 decimal? 數值小數位數完整的長度，包括小數位數尾數為 0 的長度。<para/>
        /// * 當不包含小數位數值，則直接回傳 0。<para/>
        /// * 當 decimal? 為 null 時，則直接回傳 0。<para/>
        /// </summary>
        /// <param name="source">來源 decimal? 數值</param>
        /// <returns>decimal? 數值小數位數完整的長度，包括小數位數尾數為 0 的長度。</returns>
        public static int LengthOfDecimalPlaces(this decimal? source)
        {
            if (source.IsNull() || !source.HasValue)
            {
                return 0;
            }

            return source.Value.LengthOfDecimalPlaces();
        }

        /// <summary>取得 decimal 有效的小數位數長度，代表小數位數尾數為 0 的長度不包含。<para/>
        /// * 當不包含小數位數值，則直接回傳 0。<para/>
        /// </summary>
        /// <param name="source">來源 decimal 數值</param>
        /// <returns>decimal 有效的小數位數長度，代表小數位數尾數為 0 的長度不包含。</returns>
        public static int LengthOfEfftiveDecimalPlaces(this decimal source)
        {
            var decimalNumber = source.ToString().TrimEnd('0');

            var tokens = decimalNumber.Split('.');

            if (tokens.Length != 2)
            {
                return 0;
            }

            return tokens[1].Length;
        }

        /// <summary>取得 decimal? 有效的小數位數長度，代表小數位數尾數為 0 的長度不包含。<para/>
        /// * 當不包含小數位數值，則直接回傳 0。<para/>
        /// </summary>
        /// <param name="source">來源 decimal? 數值</param>
        /// <returns>decimal? 有效的小數位數長度，代表小數位數尾數為 0 的長度不包含。</returns>
        public static int LengthOfEfftiveDecimalPlaces(this decimal? source)
        {
            if (source.IsNull() || !source.HasValue)
            {
                return 0;
            }

            return source.Value.LengthOfEfftiveDecimalPlaces();
        }

        /// <summary>對來源的 decimal 浮點數以指定的小數位數進行「無條件捨去」處理。<para/>
        /// * lengthOfDecimalPlaces 參數合法值域為: 0 &lt;= lengthOfDecimalPlaces &lt; 38<para/>
        /// </summary>
        /// <param name="source">來源的 decimal 數值</param>
        /// <param name="lengthOfDecimalPlaces">小數位數長度<para/>
        /// * 合法值域為: 0 &lt;= lengthOfDecimalPlaces &lt; 38
        /// </param>
        /// <returns>依照指定小數位數長度無條件捨去處理後的 decimal 數值</returns>
        public static decimal FloorDecimalPlaces(this decimal source, int lengthOfDecimalPlaces = 4)
        {
            if (source.LengthOfDecimalPlaces() == lengthOfDecimalPlaces)
            {
                return source;
            }

            // 這邊故意將小位數位上限設定為 38 是因為 MSSQL 接受 decimal 參數時最大的小數位數上限就是 38。
            if (lengthOfDecimalPlaces <= 0 || lengthOfDecimalPlaces > 38)
            {
                throw new ArgumentOutOfRangeException(
                    $"Invalid argument. The '{nameof(lengthOfDecimalPlaces)}' is out of range. {nameof(lengthOfDecimalPlaces)}: {lengthOfDecimalPlaces}.");
            }

            var zeroString = "";
            For.Do(lengthOfDecimalPlaces, () => zeroString += "0");
            For.Reset();

            var decimalPlaces = int.Parse($"1{zeroString}");
            var decimalNumber = Math.Floor(source * decimalPlaces) / decimalPlaces;           // 依照指定的小數位數，進行無條件捨去。
            var result = decimal.Parse(decimalNumber.ToString($"#0.{zeroString}"));  // 如果在尾數不足的情況下，強制進行補 0，確保回傳的小數位數數值會是指定的長度。
            return result;
        }

        /// <summary>對來源的 decimal? 浮點數以指定的小數位數進行「無條件捨去」處理。<para/>
        /// * lengthOfDecimalPlaces 參數合法值域為: 0 &lt;= lengthOfDecimalPlaces &lt; 38<para/>
        /// * 當來源的 decimal? 為 null 時，則直接回傳 0M，但仍會包含指定的小數位數長度。
        /// </summary>
        /// <param name="source">來源的 decimal? 數值</param>
        /// <param name="lengthOfDecimalPlaces">小數位數長度<para/>
        /// * 合法值域為: 0 &lt;= lengthOfDecimalPlaces &lt; 38
        /// </param>
        /// <returns>依照指定小數位數長度無條件捨去處理後的 decimal 數值</returns>
        public static decimal FloorDecimalPlaces(this decimal? source, int lengthOfDecimalPlaces = 4)
        {
            // 這邊故意將小位數位上限設定為 38 是因為 MSSQL 接受 decimal 參數時最大的小數位數上限就是 38。
            if (lengthOfDecimalPlaces <= 0 || lengthOfDecimalPlaces > 38)
            {
                throw new ArgumentOutOfRangeException(
                    $"Invalid argument. The '{nameof(lengthOfDecimalPlaces)}' is out of range. {nameof(lengthOfDecimalPlaces)}: {lengthOfDecimalPlaces}.");
            }

            if (source.IsNull() || !source.HasValue)
            {
                return 0.0000M.FloorDecimalPlaces(lengthOfDecimalPlaces);
            }

            return source.Value.FloorDecimalPlaces(lengthOfDecimalPlaces);
        }
    }
}
