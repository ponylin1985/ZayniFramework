﻿using System.IO;


namespace ZayniFramework.Common
{
    /// <summary>Stream 的擴充類別
    /// </summary>
    public static class StreamExtension
    {
        /// <summary>從來源的串流轉換成二進位位元陣列
        /// </summary>
        /// <param name="source">來源串流</param>
        /// <returns>二進位位元陣列</returns>
        public static byte[] ReadBytes(this Stream source) => StreamHelper.ReadBytes(source);

        /// <summary>從來源串流轉換成 Base64 編碼的字串
        /// </summary>
        /// <param name="source">來源串流</param>
        /// <returns>Base64 編碼的字串</returns>
        public static string ReadBase64String(this Stream source) => StreamHelper.ConvertToBase64String(source);
    }
}
