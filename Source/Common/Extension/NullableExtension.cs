namespace ZayniFramework.Common
{
    /// <summary>Nullable 的擴充類別
    /// </summary>
    public static class NullableExtension
    {
        /// <summary>檢查 bool? 是否為 Null 或為 false 值。
        /// </summary>
        /// <param name="source"></param>
        /// <returns>bool? 是否為 Null 或為 false 值</returns>
        public static bool IsNullOrFalse(this bool? source) =>
            null == source || source.Value == false;

        /// <summary>檢查 bool? 是否確實有值，並且為 true 值。
        /// </summary>
        /// <param name="source"></param>
        /// <returns>是否確實為 true 值</returns>
        public static bool IsNotNullAndTrue(this bool? source) =>
            null != source && source.HasValue && source.Value == true;
    }
}