using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Web;
using static System.Text.Json.JsonSerializer;


namespace ZayniFramework.Common
{
    /// <summary>HttpClient 客戶端擴充類別
    /// </summary>
    public static class HttpClientExtension
    {
        #region Http GET Methods

        /// <summary>發送 HTTP GET 的請求至遠端 http service。<para/>
        /// * 回應的 `Code` 假若回傳 `40099`，代表請求的 `HttpRequest` 參數不合法，無法執行 http GET 請求發送。<para/>
        /// * 回應的 `Code` 假若回傳 `40098`，代表發送 http GET 請求過程中發生程式異常。<para/>
        /// * 請求的 `HttpRequest` 如果有傳入 QueryStrings 查詢字串參數，會自動以 `HttpUtility.UrlEncode()` 進行編碼處理。<para/>
        /// * 回應的 `Successs = true` 則 `Code = 20000`，其中 `Data` 代表 http service 服務端回應的資料字串。<para/>
        /// </summary>
        /// <param name="httpClient">HttpClient 客戶端物件</param>
        /// <param name="request">Http 請求資訊封裝</param>
        /// <param name="consoleOutputLog">是否進行主控台日誌輸出，預設不輸出。</param>
        /// <param name="reqHandler">Http 請求前的處理委派</param>
        /// <param name="resHandler">Http 請求後的處理委派</param>
        /// <param name="httpRetryOption">Http 請求的重新嘗試選項</param>
        /// <returns>請求執行結果</returns>
        public static HttpResponse ExecuteGet(this HttpClient httpClient, HttpRequest request,
            bool consoleOutputLog = false, Func<HttpRequest, Task> reqHandler = null, Func<HttpRequest, HttpResponse, Task> resHandler = null, IRetryOption httpRetryOption = null)
        {
            return Proxy.Invoke<HttpResponse>(
                HttpGet,
                retryOption: httpRetryOption,
                retryWhen: httpResponse => !httpResponse.Success && "40099" != httpResponse.Code,
                returnWhenRetryFail: httpResponse => new HttpResponse()
                {
                    StatusCode = httpResponse.StatusCode,
                    HttpResponseMessaage = httpResponse.HttpResponseMessaage,
                    Message = $"Send http request failure. This http GET operation has been retry over {httpRetryOption.RetryFrequencies.Length} times but still retrive http server side error.{Environment.NewLine}Last retry error message:{Environment.NewLine}{httpResponse.Message}",
                    Code = $"40097",
                });

            HttpResponse HttpGet() => ExecuteGetAsync(httpClient, request, consoleOutputLog, reqHandler, resHandler).ConfigureAwait(false).GetAwaiter().GetResult();
        }

        /// <summary>發送 HTTP GET 的請求至遠端 http service。<para/>
        /// * 回應的 `Code` 假若回傳 `40099`，代表請求的 `HttpRequest` 參數不合法，無法執行 http GET 請求發送。<para/>
        /// * 回應的 `Code` 假若回傳 `40098`，代表發送 http GET 請求過程中發生程式異常。<para/>
        /// * 回應的 `Code` 假若回傳 `40097`，代表發送 http GET 請求已經重新 retry 嘗試發送達到上限，但仍然發生錯誤。<para/>
        /// * 請求的 `HttpRequest` 如果有傳入 QueryStrings 查詢字串參數，會自動以 `HttpUtility.UrlEncode()` 進行編碼處理。<para/>
        /// * 回應的 `Successs = true` 則 `Code = 20000`，其中 `Data` 代表 http service 服務端回應的資料字串。<para/>
        /// </summary>
        /// <param name="httpClient">HttpClient 客戶端物件</param>
        /// <param name="request">Http 請求資訊封裝</param>
        /// <param name="consoleOutputLog">是否進行主控台日誌輸出，預設不輸出。</param>
        /// <param name="reqHandler">Http 請求前的處理委派</param>
        /// <param name="resHandler">Http 請求後的處理委派</param>
        /// <param name="httpRetryOption">Http 請求的重新嘗試選項</param>
        /// <returns>請求執行結果</returns>
        public static async Task<HttpResponse> ExecuteGetAsync(this HttpClient httpClient, HttpRequest request,
            bool consoleOutputLog = false, Func<HttpRequest, Task> reqHandler = null, Func<HttpRequest, HttpResponse, Task> resHandler = null, IRetryOption httpRetryOption = null)
        {
            return await Proxy.InvokeAsync<HttpResponse>(
                HttpGetAsync,
                retryOption: httpRetryOption,
                retryWhen: httpResponse => !httpResponse.Success && "40099" != httpResponse.Code,
                returnWhenRetryFail: httpResponse => new HttpResponse()
                {
                    StatusCode = httpResponse.StatusCode,
                    HttpResponseMessaage = httpResponse.HttpResponseMessaage,
                    Message = $"Send http request failure. This http GET operation has been retry over {httpRetryOption.RetryFrequencies.Length} times but still retrive http server side error.{Environment.NewLine}Last retry error message:{Environment.NewLine}{httpResponse.Message}",
                    Code = $"40097",
                }).ConfigureAwait(false);

            async Task<HttpResponse> HttpGetAsync() =>
                await ExecuteGetOrDeleteAsync(
                    httpClient,
                    request,
                    async path => await httpClient.GetAsync(path).ConfigureAwait(false),
                    consoleOutputLog,
                    reqHandler,
                    resHandler).ConfigureAwait(false);
        }

        /// <summary>發送 HTTP GET 的請求至遠端 http service。<para/>
        /// * 回應的 `Code` 假若回傳 `40099`，代表請求的 `HttpRequest` 參數不合法，無法執行 http GET 請求發送。<para/>
        /// * 回應的 `Code` 假若回傳 `40098`，代表發送 http GET 請求過程中發生程式異常。<para/>
        /// * 回應的 `Code` 假若回傳 `40097`，代表發送 http GET 請求已經重新 retry 嘗試發送達到上限，但仍然發生錯誤。<para/>
        /// * 請求的 `HttpRequest` 如果有傳入 QueryStrings 查詢字串參數，會自動以 `HttpUtility.UrlEncode()` 進行編碼處理。<para/>
        /// * 回應的 `Successs = true` 則 `Code = 20000`，其中 `Data` 代表 http service 服務端回應的資料字串。<para/>
        /// </summary>
        /// <param name="httpClient">HttpClient 客戶端物件</param>
        /// <param name="request">Http 請求資訊封裝</param>
        /// <param name="consoleOutputLog">是否進行主控台日誌輸出，預設不輸出。</param>
        /// <param name="reqHandler">Http 請求前的處理委派</param>
        /// <param name="resHandler">Http 請求後的處理委派</param>
        /// <param name="withHttp2">是否啟用 HTTP/2 通訊協定，需要 Http 服務端支援 HTTP/2 通訊協定，效能較佳。</param>
        /// <param name="httpRetryOption">Http 請求的重新嘗試選項</param>
        /// <returns>請求執行結果</returns>
        public static async Task<HttpResponse> ExecuteHttp2GetAsync(this HttpClient httpClient, HttpRequest request,
            bool consoleOutputLog = false, Func<HttpRequest, Task> reqHandler = null, Func<HttpRequest, HttpResponse, Task> resHandler = null, bool withHttp2 = false, IRetryOption httpRetryOption = null)
        {
            return await Proxy.InvokeAsync<HttpResponse>(
                HttpGetAsync,
                retryOption: httpRetryOption,
                retryWhen: httpResponse => !httpResponse.Success && "40099" != httpResponse.Code,
                returnWhenRetryFail: httpResponse => new HttpResponse()
                {
                    StatusCode = httpResponse.StatusCode,
                    HttpResponseMessaage = httpResponse.HttpResponseMessaage,
                    Message = $"Send http request failure. This http GET operation has been retry over {httpRetryOption.RetryFrequencies.Length} times but still retrive http server side error.{Environment.NewLine}Last retry error message:{Environment.NewLine}{httpResponse.Message}",
                    Code = $"40097",
                }).ConfigureAwait(false);

            async Task<HttpResponse> HttpGetAsync()
            {
                if (!withHttp2)
                {
                    return await ExecuteGetOrDeleteAsync(
                        httpClient,
                        request,
                        async path => await httpClient.GetAsync(path).ConfigureAwait(false),
                        consoleOutputLog,
                        reqHandler,
                        resHandler).ConfigureAwait(false);
                }

                var httpHandlerAsync = new Func<string, Task<HttpResponseMessage>>(async reqPath =>
                {
                    var httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, reqPath)
                    {
                        Version = new Version(2, 0)
                    };

                    return await httpClient.SendAsync(httpRequestMessage).ConfigureAwait(false);
                });

                return await ExecuteGetOrDeleteAsync(
                    httpClient,
                    request,
                    httpHandlerAsync,
                    consoleOutputLog,
                    reqHandler,
                    resHandler).ConfigureAwait(false);
            }
        }

        /// <summary>發送 HTTP GET 的請求至遠端 http service。<para/>
        /// * 回應的 `Code` 假若回傳 `40099`，代表請求的 `HttpRequest` 參數不合法，無法執行 http GET 請求發送。<para/>
        /// * 回應的 `Code` 假若回傳 `40098`，代表發送 http GET 請求過程中發生程式異常。<para/>
        /// * 請求的 `HttpRequest` 如果有傳入 QueryStrings 查詢字串參數，會自動以 `HttpUtility.UrlEncode()` 進行編碼處理。<para/>
        /// * 回應的 `Successs = true` 則 `Code = 20000`，其中 `Data` 代表 http service 服務端回應的資料字串。<para/>
        /// </summary>
        /// <param name="httpClient">HttpClient 客戶端物件</param>
        /// <param name="request">Http 請求資訊封裝</param>
        /// <param name="consoleOutputLog">是否進行主控台日誌輸出，預設不輸出。</param>
        /// <param name="reqHandler">Http 請求前的處理委派</param>
        /// <param name="resHandler">Http 請求後的處理委派</param>
        /// <param name="httpRetryOption">Http 請求的重新嘗試選項</param>
        /// <typeparam name="TResponseDTO">回應資料載體泛型</typeparam>
        /// <returns>請求執行結果</returns>
        public static HttpResponse<TResponseDTO> ExecuteGet<TResponseDTO>(
            this HttpClient httpClient,
            HttpRequest request,
            bool consoleOutputLog = false,
            Func<HttpRequest, Task> reqHandler = null,
            Func<HttpRequest, HttpResponse, Task> resHandler = null,
            IRetryOption httpRetryOption = null)
            where TResponseDTO : class
        {
            var response = new HttpResponse<TResponseDTO>();
            var r = ExecuteGet(httpClient, request, consoleOutputLog, reqHandler, resHandler, httpRetryOption);

            response.RequestId = r.RequestId;
            response.StatusCode = r.StatusCode;
            response.HttpResponseMessaage = r.HttpResponseMessaage;
            response.Data = r.Data;
            response.Code = r.Code;
            response.Message = r.Message;

            if (!r.Success)
            {
                response.Payload = null;
                response.Success = false;
                return response;
            }

            var payload = default(TResponseDTO);
            response.Data.IsNotNullOrEmpty(res => payload = Deserialize<TResponseDTO>(res));
            response.Payload = payload;
            response.StatusCode = HttpStatusCode.OK;
            response.Code = "20000";
            response.Success = true;
            return response;
        }

        /// <summary>發送 HTTP GET 的請求至遠端 http service。<para/>
        /// * 回應的 `Code` 假若回傳 `40099`，代表請求的 `HttpRequest` 參數不合法，無法執行 http GET 請求發送。<para/>
        /// * 回應的 `Code` 假若回傳 `40098`，代表發送 http GET 請求過程中發生程式異常。<para/>
        /// * 回應的 `Code` 假若回傳 `40097`，代表發送 http GET 請求已經重新 retry 嘗試發送達到上限，但仍然發生錯誤。<para/>
        /// * 請求的 `HttpRequest` 如果有傳入 QueryStrings 查詢字串參數，會自動以 `HttpUtility.UrlEncode()` 進行編碼處理。<para/>
        /// * 回應的 `Successs = true` 則 `Code = 20000`，其中 `Data` 代表 http service 服務端回應的資料字串。<para/>
        /// </summary>
        /// <param name="httpClient">HttpClient 客戶端物件</param>
        /// <param name="request">Http 請求資訊封裝</param>
        /// <param name="consoleOutputLog">是否進行主控台日誌輸出，預設不輸出。</param>
        /// <param name="reqHandler">Http 請求前的處理委派</param>
        /// <param name="resHandler">Http 請求後的處理委派</param>
        /// <param name="httpRetryOption">Http 請求的重新嘗試選項</param>
        /// <typeparam name="TResponseDTO">回應資料載體泛型</typeparam>
        /// <returns>請求執行結果</returns>
        public static async Task<HttpResponse<TResponseDTO>> ExecuteGetAsync<TResponseDTO>(
            this HttpClient httpClient, HttpRequest request,
            bool consoleOutputLog = false,
            Func<HttpRequest, Task> reqHandler = null,
            Func<HttpRequest, HttpResponse, Task> resHandler = null,
            IRetryOption httpRetryOption = null)
            where TResponseDTO : class
        {
            var response = new HttpResponse<TResponseDTO>();
            var r = await ExecuteGetAsync(httpClient, request, consoleOutputLog, reqHandler, resHandler, httpRetryOption).ConfigureAwait(false);

            response.RequestId = r.RequestId;
            response.StatusCode = r.StatusCode;
            response.HttpResponseMessaage = r.HttpResponseMessaage;
            response.Data = r.Data;
            response.Code = r.Code;
            response.Message = r.Message;

            if (!r.Success)
            {
                response.Payload = null;
                response.Success = false;
                return response;
            }

            var payload = default(TResponseDTO);
            response.Data.IsNotNullOrEmpty(res => payload = Deserialize<TResponseDTO>(res));
            response.Payload = payload;
            response.StatusCode = HttpStatusCode.OK;
            response.Code = "20000";
            response.Success = true;
            return response;
        }

        /// <summary>發送 HTTP GET 的請求至遠端 http service。<para/>
        /// * 回應的 `Code` 假若回傳 `40099`，代表請求的 `HttpRequest` 參數不合法，無法執行 http GET 請求發送。<para/>
        /// * 回應的 `Code` 假若回傳 `40098`，代表發送 http GET 請求過程中發生程式異常。<para/>
        /// * 回應的 `Code` 假若回傳 `40097`，代表發送 http GET 請求已經重新 retry 嘗試發送達到上限，但仍然發生錯誤。<para/>
        /// * 請求的 `HttpRequest` 如果有傳入 QueryStrings 查詢字串參數，會自動以 `HttpUtility.UrlEncode()` 進行編碼處理。<para/>
        /// * 回應的 `Successs = true` 則 `Code = 20000`，其中 `Data` 代表 http service 服務端回應的資料字串。<para/>
        /// </summary>
        /// <param name="httpClient">HttpClient 客戶端物件</param>
        /// <param name="request">Http 請求資訊封裝</param>
        /// <param name="consoleOutputLog">是否進行主控台日誌輸出，預設不輸出。</param>
        /// <param name="reqHandler">Http 請求前的處理委派</param>
        /// <param name="resHandler">Http 請求後的處理委派</param>
        /// <param name="withHttp2">是否啟用 HTTP/2 通訊協定，需要 Http 服務端支援 HTTP/2 通訊協定，效能較佳。</param>
        /// <param name="httpRetryOption">Http 請求的重新嘗試選項</param>
        /// <typeparam name="TResponseDTO">回應資料載體泛型</typeparam>
        /// <returns>請求執行結果</returns>
        public static async Task<HttpResponse<TResponseDTO>> ExecuteHttp2GetAsync<TResponseDTO>(
            this HttpClient httpClient,
            HttpRequest request,
            bool consoleOutputLog = false,
            Func<HttpRequest, Task> reqHandler = null,
            Func<HttpRequest, HttpResponse, Task> resHandler = null,
            bool withHttp2 = false,
            IRetryOption httpRetryOption = null)
            where TResponseDTO : class
        {
            var response = new HttpResponse<TResponseDTO>();
            var r = await ExecuteHttp2GetAsync(httpClient, request, consoleOutputLog, reqHandler, resHandler, withHttp2, httpRetryOption).ConfigureAwait(false);

            response.RequestId = r.RequestId;
            response.StatusCode = r.StatusCode;
            response.HttpResponseMessaage = r.HttpResponseMessaage;
            response.Data = r.Data;
            response.Code = r.Code;
            response.Message = r.Message;

            if (!r.Success)
            {
                response.Payload = null;
                response.Success = false;
                return response;
            }

            var payload = default(TResponseDTO);
            response.Data.IsNotNullOrEmpty(res => payload = Deserialize<TResponseDTO>(res));
            response.Payload = payload;
            response.StatusCode = HttpStatusCode.OK;
            response.Code = "20000";
            response.Success = true;
            return response;
        }

        #endregion Http GET Methods


        #region Http POST Methods

        /// <summary>發送 HTTP POST 的請求至遠端 http service。<para/>
        /// * 請求的 `Content-Type` 會強制固定為 `application/json`，無論 `HttpRequest` 是否有特別指定 `Content-Type` 格式。<para/>
        /// * 回應的 `Code` 假若回傳 `40099`，代表請求的 `HttpRequest` 參數不合法，無法執行 http POST 請求發送。<para/>
        /// * 回應的 `Code` 假若回傳 `40098`，代表發送 http POST 請求過程中發生程式異常。<para/>
        /// * 回應的 `Code` 假若回傳 `40097`，代表發送 http POST 請求已經重新 retry 嘗試發送達到上限，但仍然發生錯誤。<para/>
        /// * 回應的 `Successs = true` 則 `Code = 20000`，其中 `Data` 代表 http service 服務端回應的資料字串。<para/>
        /// </summary>
        /// <param name="httpClient">HttpClient 客戶端物件</param>
        /// <param name="request">Http 請求資訊封裝</param>
        /// <param name="consoleOutputLog">是否進行主控台日誌輸出，預設不輸出。</param>
        /// <param name="reqHandler">Http 請求前的處理委派</param>
        /// <param name="resHandler">Http 請求後的處理委派</param>
        /// <param name="httpRetryOption">Http 請求的重新嘗試選項</param>
        /// <returns>請求執行結果</returns>
        public static HttpResponse ExecutePostJson(this HttpClient httpClient, HttpRequest request,
            bool consoleOutputLog = false, Func<HttpRequest, Task> reqHandler = null, Func<HttpRequest, HttpResponse, Task> resHandler = null, IRetryOption httpRetryOption = null)
        {
            return Proxy.Invoke<HttpResponse>(
                HttpPost,
                retryOption: httpRetryOption,
                retryWhen: httpResponse => !httpResponse.Success && "40099" != httpResponse.Code,
                returnWhenRetryFail: httpResponse => new HttpResponse()
                {
                    StatusCode = httpResponse.StatusCode,
                    HttpResponseMessaage = httpResponse.HttpResponseMessaage,
                    Message = $"Send http request failure. This http POST operation has been retry over {httpRetryOption.RetryFrequencies.Length} times but still retrive http server side error.{Environment.NewLine}Last retry error message:{Environment.NewLine}{httpResponse.Message}",
                    Code = $"40097",
                });

            HttpResponse HttpPost() => ExecutePostJsonAsync(httpClient, request, consoleOutputLog, reqHandler, resHandler).ConfigureAwait(false).GetAwaiter().GetResult();
        }

        /// <summary>發送 HTTP POST 的請求至遠端 http service。<para/>
        /// * 請求的 `Content-Type` 會強制固定為 `application/json`，無論 `HttpRequest` 是否有特別指定 `Content-Type` 格式。<para/>
        /// * 回應的 `Code` 假若回傳 `40099`，代表請求的 `HttpRequest` 參數不合法，無法執行 http POST 請求發送。<para/>
        /// * 回應的 `Code` 假若回傳 `40098`，代表發送 http POST 請求過程中發生程式異常。<para/>
        /// * 回應的 `Code` 假若回傳 `40097`，代表發送 http POST 請求已經重新 retry 嘗試發送達到上限，但仍然發生錯誤。<para/>
        /// * 回應的 `Successs = true` 則 `Code = 20000`，其中 `Data` 代表 http service 服務端回應的資料字串。<para/>
        /// </summary>
        /// <param name="httpClient">HttpClient 客戶端物件</param>
        /// <param name="request">Http 請求資訊封裝</param>
        /// <param name="consoleOutputLog">是否進行主控台日誌輸出，預設不輸出。</param>
        /// <param name="reqHandler">Http 請求前的處理委派</param>
        /// <param name="resHandler">Http 請求後的處理委派</param>
        /// <param name="httpRetryOption">Http 請求的重新嘗試選項</param>
        /// <returns>請求執行結果</returns>
        public static async Task<HttpResponse> ExecutePostJsonAsync(this HttpClient httpClient, HttpRequest request,
            bool consoleOutputLog = false, Func<HttpRequest, Task> reqHandler = null, Func<HttpRequest, HttpResponse, Task> resHandler = null, IRetryOption httpRetryOption = null)
        {
            return await Proxy.InvokeAsync<HttpResponse>(
                HttpPostAsync,
                retryOption: httpRetryOption,
                retryWhen: httpResponse => !httpResponse.Success && "40099" != httpResponse.Code,
                returnWhenRetryFail: httpResponse => new HttpResponse()
                {
                    StatusCode = httpResponse.StatusCode,
                    HttpResponseMessaage = httpResponse.HttpResponseMessaage,
                    Message = $"Send http request failure. This http POST operation has been retry over {httpRetryOption.RetryFrequencies.Length} times but still retrive http server side error.{Environment.NewLine}Last retry error message:{Environment.NewLine}{httpResponse.Message}",
                    Code = $"40097",
                }).ConfigureAwait(false);

            async Task<HttpResponse> HttpPostAsync() =>
                await ExecutePostOrPutAsync(
                    httpClient,
                    request,
                    async (path, content) => await httpClient.PostAsync(path, content).ConfigureAwait(false),
                    consoleOutputLog,
                    reqHandler,
                    resHandler).ConfigureAwait(false);
        }

        /// <summary>發送 HTTP POST 的請求至遠端 http service。<para/>
        /// * 請求的 `Content-Type` 會強制固定為 `application/json`，無論 `HttpRequest` 是否有特別指定 `Content-Type` 格式。<para/>
        /// * 回應的 `Code` 假若回傳 `40099`，代表請求的 `HttpRequest` 參數不合法，無法執行 http POST 請求發送。<para/>
        /// * 回應的 `Code` 假若回傳 `40098`，代表發送 http POST 請求過程中發生程式異常。<para/>
        /// * 回應的 `Code` 假若回傳 `40097`，代表發送 http POST 請求已經重新 retry 嘗試發送達到上限，但仍然發生錯誤。<para/>
        /// * 回應的 `Successs = true` 則 `Code = 20000`，其中 `Data` 代表 http service 服務端回應的資料字串。<para/>
        /// </summary>
        /// <param name="httpClient">HttpClient 客戶端物件</param>
        /// <param name="request">Http 請求資訊封裝</param>
        /// <param name="consoleOutputLog">是否進行主控台日誌輸出，預設不輸出。</param>
        /// <param name="reqHandler">Http 請求前的處理委派</param>
        /// <param name="resHandler">Http 請求後的處理委派</param>
        /// <param name="withHttp2">是否啟用 HTTP/2 通訊協定，需要 Http 服務端支援 HTTP/2 通訊協定，效能較佳。</param>
        /// <param name="httpRetryOption">Http 請求的重新嘗試選項</param>
        /// <returns>請求執行結果</returns>
        public static async Task<HttpResponse> ExecuteHttp2PostJsonAsync(this HttpClient httpClient, HttpRequest request,
            bool consoleOutputLog = false, Func<HttpRequest, Task> reqHandler = null, Func<HttpRequest, HttpResponse, Task> resHandler = null, bool withHttp2 = false, IRetryOption httpRetryOption = null)
        {
            return await Proxy.InvokeAsync<HttpResponse>(
                HttpPostAsync,
                retryOption: httpRetryOption,
                retryWhen: httpResponse => !httpResponse.Success && "40099" != httpResponse.Code,
                returnWhenRetryFail: httpResponse => new HttpResponse()
                {
                    StatusCode = httpResponse.StatusCode,
                    HttpResponseMessaage = httpResponse.HttpResponseMessaage,
                    Message = $"Send http request failure. This http POST operation has been retry over {httpRetryOption.RetryFrequencies.Length} times but still retrive http server side error.{Environment.NewLine}Last retry error message:{Environment.NewLine}{httpResponse.Message}",
                    Code = $"40097",
                }).ConfigureAwait(false);

            async Task<HttpResponse> HttpPostAsync()
            {
                if (!withHttp2)
                {
                    return await ExecutePostOrPutAsync(
                        httpClient,
                        request,
                        async (path, content) => await httpClient.PostAsync(path, content).ConfigureAwait(false),
                        consoleOutputLog,
                        reqHandler,
                        resHandler).ConfigureAwait(false);
                }

                var httpHandlerAsync = new Func<string, HttpContent, Task<HttpResponseMessage>>(async (reqPath, reqContent) =>
                {
                    var httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, reqPath)
                    {
                        Version = new Version(2, 0),
                        Content = reqContent
                    };

                    return await httpClient.SendAsync(httpRequestMessage).ConfigureAwait(false);
                });

                return await ExecutePostOrPutAsync(
                    httpClient,
                    request,
                    httpHandlerAsync,
                    consoleOutputLog,
                    reqHandler,
                    resHandler).ConfigureAwait(false);
            }
        }

        /// <summary>發送 HTTP POST 的請求至遠端 http service。<para/>
        /// * 請求的 `Content-Type` 會強制固定為 `application/json`，無論 `HttpRequest` 是否有特別指定 `Content-Type` 格式。<para/>
        /// * 回應的 `Code` 假若回傳 `40099`，代表請求的 `HttpRequest` 參數不合法，無法執行 http POST 請求發送。<para/>
        /// * 回應的 `Code` 假若回傳 `40098`，代表發送 http POST 請求過程中發生程式異常。<para/>
        /// * 回應的 `Code` 假若回傳 `40097`，代表發送 http POST 請求已經重新 retry 嘗試發送達到上限，但仍然發生錯誤。<para/>
        /// * 回應的 `Successs = true` 則 `Code = 20000`，其中 `Data` 代表 http service 服務端回應的資料字串。<para/>
        /// </summary>
        /// <param name="httpClient">HttpClient 客戶端物件</param>
        /// <param name="request">Http 請求資訊封裝</param>
        /// <param name="requestDTO">Http 請求資料載體，將使用 System.Text.Json.JsonSerializer 序列化處理。</param>
        /// <param name="consoleOutputLog">是否進行主控台日誌輸出，預設不輸出。</param>
        /// <param name="reqHandler">Http 請求前的處理委派</param>
        /// <param name="resHandler">Http 請求後的處理委派</param>
        /// <param name="httpRetryOption">Http 請求的重新嘗試選項</param>
        /// <typeparam name="TRequestDTO">請求資料載體泛型</typeparam>
        /// <typeparam name="TResponseDTO">回應資料載體泛型</typeparam>
        /// <returns>請求執行結果</returns>
        public static HttpResponse<TResponseDTO> ExecutePostJson<TRequestDTO, TResponseDTO>(
            this HttpClient httpClient,
            HttpRequest request,
            TRequestDTO requestDTO = null,
            bool consoleOutputLog = false,
            Func<HttpRequest, Task> reqHandler = null,
            Func<HttpRequest, HttpResponse, Task> resHandler = null,
            IRetryOption httpRetryOption = null)
            where TRequestDTO : class
            where TResponseDTO : class
        {
            var response = new HttpResponse<TResponseDTO>();
            var requestBody = default(string);
            request.IsNotNull(req => requestBody = Serialize(requestDTO));
            request.Body = requestBody;

            var r = ExecutePostJson(httpClient, request, consoleOutputLog, reqHandler, resHandler, httpRetryOption);

            response.RequestId = r.RequestId;
            response.StatusCode = r.StatusCode;
            response.HttpResponseMessaage = r.HttpResponseMessaage;
            response.Data = r.Data;
            response.Code = r.Code;
            response.Message = r.Message;

            if (!r.Success)
            {
                response.Payload = null;
                response.Success = false;
                return response;
            }

            var payload = default(TResponseDTO);
            response.Data.IsNotNullOrEmpty(res => payload = Deserialize<TResponseDTO>(res));
            response.Payload = payload;
            response.StatusCode = HttpStatusCode.OK;
            response.Code = "20000";
            response.Success = true;
            return response;
        }

        /// <summary>發送 HTTP POST 的請求至遠端 http service。<para/>
        /// * 請求的 `Content-Type` 會強制固定為 `application/json`，無論 `HttpRequest` 是否有特別指定 `Content-Type` 格式。<para/>
        /// * 回應的 `Code` 假若回傳 `40099`，代表請求的 `HttpRequest` 參數不合法，無法執行 http POST 請求發送。<para/>
        /// * 回應的 `Code` 假若回傳 `40098`，代表發送 http POST 請求過程中發生程式異常。<para/>
        /// * 回應的 `Code` 假若回傳 `40097`，代表發送 http POST 請求已經重新 retry 嘗試發送達到上限，但仍然發生錯誤。<para/>
        /// * 回應的 `Successs = true` 則 `Code = 20000`，其中 `Data` 代表 http service 服務端回應的資料字串。<para/>
        /// </summary>
        /// <param name="httpClient">HttpClient 客戶端物件</param>
        /// <param name="request">Http 請求資訊封裝</param>
        /// <param name="requestDTO">Http 請求資料載體，將使用 System.Text.Json.JsonSerializer 序列化處理。</param>
        /// <param name="consoleOutputLog">是否進行主控台日誌輸出，預設不輸出。</param>
        /// <param name="reqHandler">Http 請求前的處理委派</param>
        /// <param name="resHandler">Http 請求後的處理委派</param>
        /// <param name="httpRetryOption">Http 請求的重新嘗試選項</param>
        /// <typeparam name="TRequestDTO">請求資料載體泛型</typeparam>
        /// <typeparam name="TResponseDTO">回應資料載體泛型</typeparam>
        /// <returns>請求執行結果</returns>
        public static async Task<HttpResponse<TResponseDTO>> ExecutePostJsonAsync<TRequestDTO, TResponseDTO>(
            this HttpClient httpClient,
            HttpRequest request,
            TRequestDTO requestDTO = null,
            bool consoleOutputLog = false,
            Func<HttpRequest, Task> reqHandler = null,
            Func<HttpRequest, HttpResponse, Task> resHandler = null,
            IRetryOption httpRetryOption = null)
            where TRequestDTO : class
            where TResponseDTO : class
        {
            var response = new HttpResponse<TResponseDTO>();
            var requestBody = default(string);
            request.IsNotNull(req => requestBody = Serialize(requestDTO));
            request.Body = requestBody;

            var r = await ExecutePostJsonAsync(httpClient, request, consoleOutputLog, reqHandler, resHandler, httpRetryOption).ConfigureAwait(false);

            response.RequestId = r.RequestId;
            response.StatusCode = r.StatusCode;
            response.HttpResponseMessaage = r.HttpResponseMessaage;
            response.Data = r.Data;
            response.Code = r.Code;
            response.Message = r.Message;

            if (!r.Success)
            {
                response.Payload = null;
                response.Success = false;
                return response;
            }

            var payload = default(TResponseDTO);
            response.Data.IsNotNullOrEmpty(res => payload = Deserialize<TResponseDTO>(res));
            response.Payload = payload;
            response.StatusCode = HttpStatusCode.OK;
            response.Code = "20000";
            response.Success = true;
            return response;
        }

        /// <summary>發送 HTTP POST 的請求至遠端 http service。<para/>
        /// * 請求的 `Content-Type` 會強制固定為 `application/json`，無論 `HttpRequest` 是否有特別指定 `Content-Type` 格式。<para/>
        /// * 回應的 `Code` 假若回傳 `40099`，代表請求的 `HttpRequest` 參數不合法，無法執行 http POST 請求發送。<para/>
        /// * 回應的 `Code` 假若回傳 `40098`，代表發送 http POST 請求過程中發生程式異常。<para/>
        /// * 回應的 `Code` 假若回傳 `40097`，代表發送 http POST 請求已經重新 retry 嘗試發送達到上限，但仍然發生錯誤。<para/>
        /// * 回應的 `Successs = true` 則 `Code = 20000`，其中 `Data` 代表 http service 服務端回應的資料字串。<para/>
        /// </summary>
        /// <param name="httpClient">HttpClient 客戶端物件</param>
        /// <param name="request">Http 請求資訊封裝</param>
        /// <param name="requestDTO">Http 請求資料載體，將使用 System.Text.Json.JsonSerializer 序列化處理。</param>
        /// <param name="consoleOutputLog">是否進行主控台日誌輸出，預設不輸出。</param>
        /// <param name="reqHandler">Http 請求前的處理委派</param>
        /// <param name="resHandler">Http 請求後的處理委派</param>
        /// <param name="withHttp2">是否啟用 HTTP/2 通訊協定，需要 Http 服務端支援 HTTP/2 通訊協定，效能較佳。</param>
        /// <param name="httpRetryOption">Http 請求的重新嘗試選項</param>
        /// <typeparam name="TRequestDTO">請求資料載體泛型</typeparam>
        /// <typeparam name="TResponseDTO">回應資料載體泛型</typeparam>
        /// <returns>請求執行結果</returns>
        public static async Task<HttpResponse<TResponseDTO>> ExecuteHttp2PostJsonAsync<TRequestDTO, TResponseDTO>(
            this HttpClient httpClient,
            HttpRequest request,
            TRequestDTO requestDTO = null,
            bool consoleOutputLog = false,
            Func<HttpRequest, Task> reqHandler = null,
            Func<HttpRequest, HttpResponse, Task> resHandler = null,
            bool withHttp2 = false,
            IRetryOption httpRetryOption = null)
            where TRequestDTO : class
            where TResponseDTO : class
        {
            var response = new HttpResponse<TResponseDTO>();
            var requestBody = default(string);
            request.IsNotNull(req => requestBody = Serialize(requestDTO));
            request.Body = requestBody;

            var r = await ExecuteHttp2PostJsonAsync(httpClient, request, consoleOutputLog, reqHandler, resHandler, withHttp2, httpRetryOption).ConfigureAwait(false);

            response.RequestId = r.RequestId;
            response.StatusCode = r.StatusCode;
            response.HttpResponseMessaage = r.HttpResponseMessaage;
            response.Data = r.Data;
            response.Code = r.Code;
            response.Message = r.Message;

            if (!r.Success)
            {
                response.Payload = null;
                response.Success = false;
                return response;
            }

            var payload = default(TResponseDTO);
            response.Data.IsNotNullOrEmpty(res => payload = Deserialize<TResponseDTO>(res));
            response.Payload = payload;
            response.StatusCode = HttpStatusCode.OK;
            response.Code = "20000";
            response.Success = true;
            return response;
        }

        #endregion Http POST Methods


        #region Http PUT Methods

        /// <summary>發送 HTTP PUT 的請求至遠端 http service。<para/>
        /// * 請求的 `Content-Type` 會強制固定為 `application/json`，無論 `HttpRequest` 是否有特別指定 `Content-Type` 格式。<para/>
        /// * 回應的 `Code` 假若回傳 `40099`，代表請求的 `HttpRequest` 參數不合法，無法執行 http PUT 請求發送。<para/>
        /// * 回應的 `Code` 假若回傳 `40098`，代表發送 http PUT 請求過程中發生程式異常。<para/>
        /// * 回應的 `Code` 假若回傳 `40097`，代表發送 http PUT 請求已經重新 retry 嘗試發送達到上限，但仍然發生錯誤。<para/>
        /// * 回應的 `Successs = true` 則 `Code = 20000`，其中 `Data` 代表 http service 服務端回應的資料字串。<para/>
        /// </summary>
        /// <param name="httpClient">HttpClient 客戶端物件</param>
        /// <param name="request">Http 請求資訊封裝</param>
        /// <param name="consoleOutputLog">是否進行主控台日誌輸出，預設不輸出。</param>
        /// <param name="reqHandler">Http 請求前的處理委派</param>
        /// <param name="resHandler">Http 請求後的處理委派</param>
        /// <param name="httpRetryOption">Http 請求的重新嘗試選項</param>
        /// <returns>請求執行結果</returns>
        public static HttpResponse ExecutePutJson(this HttpClient httpClient, HttpRequest request,
            bool consoleOutputLog = false, Func<HttpRequest, Task> reqHandler = null, Func<HttpRequest, HttpResponse, Task> resHandler = null, IRetryOption httpRetryOption = null)
        {
            return Proxy.Invoke<HttpResponse>(
                HttpPut,
                retryOption: httpRetryOption,
                retryWhen: httpResponse => !httpResponse.Success && "40099" != httpResponse.Code,
                returnWhenRetryFail: httpResponse => new HttpResponse()
                {
                    StatusCode = httpResponse.StatusCode,
                    HttpResponseMessaage = httpResponse.HttpResponseMessaage,
                    Message = $"Send http request failure. This http PUT operation has been retry over {httpRetryOption.RetryFrequencies.Length} times but still retrive http server side error.{Environment.NewLine}Last retry error message:{Environment.NewLine}{httpResponse.Message}",
                    Code = $"40097",
                });

            HttpResponse HttpPut() => ExecutePutJsonAsync(httpClient, request, consoleOutputLog, reqHandler, resHandler).ConfigureAwait(false).GetAwaiter().GetResult();
        }

        /// <summary>發送 HTTP PUT 的請求至遠端 http service。<para/>
        /// * 請求的 `Content-Type` 會強制固定為 `application/json`，無論 `HttpRequest` 是否有特別指定 `Content-Type` 格式。<para/>
        /// * 回應的 `Code` 假若回傳 `40099`，代表請求的 `HttpRequest` 參數不合法，無法執行 http PUT 請求發送。<para/>
        /// * 回應的 `Code` 假若回傳 `40098`，代表發送 http PUT 請求過程中發生程式異常。<para/>
        /// * 回應的 `Code` 假若回傳 `40097`，代表發送 http PUT 請求已經重新 retry 嘗試發送達到上限，但仍然發生錯誤。<para/>
        /// * 回應的 `Successs = true` 則 `Code = 20000`，其中 `Data` 代表 http service 服務端回應的資料字串。<para/>
        /// </summary>
        /// <param name="httpClient">HttpClient 客戶端物件</param>
        /// <param name="request">Http 請求資訊封裝</param>
        /// <param name="consoleOutputLog">是否進行主控台日誌輸出，預設不輸出。</param>
        /// <param name="reqHandler">Http 請求前的處理委派</param>
        /// <param name="resHandler">Http 請求後的處理委派</param>
        /// <param name="httpRetryOption">Http 請求的重新嘗試選項</param>
        /// <returns>請求執行結果</returns>
        public static async Task<HttpResponse> ExecutePutJsonAsync(this HttpClient httpClient, HttpRequest request,
            bool consoleOutputLog = false, Func<HttpRequest, Task> reqHandler = null, Func<HttpRequest, HttpResponse, Task> resHandler = null, IRetryOption httpRetryOption = null)
        {
            return await Proxy.InvokeAsync<HttpResponse>(
                HttpPutAsync,
                retryOption: httpRetryOption,
                retryWhen: httpResponse => !httpResponse.Success && "40099" != httpResponse.Code,
                returnWhenRetryFail: httpResponse => new HttpResponse()
                {
                    StatusCode = httpResponse.StatusCode,
                    HttpResponseMessaage = httpResponse.HttpResponseMessaage,
                    Message = $"Send http request failure. This http PUT operation has been retry over {httpRetryOption.RetryFrequencies.Length} times but still retrive http server side error.{Environment.NewLine}Last retry error message:{Environment.NewLine}{httpResponse.Message}",
                    Code = $"40097",
                }).ConfigureAwait(false);

            async Task<HttpResponse> HttpPutAsync() =>
                await ExecutePostOrPutAsync(
                    httpClient,
                    request,
                    async (path, content) => await httpClient.PutAsync(path, content).ConfigureAwait(false),
                    consoleOutputLog,
                    reqHandler,
                    resHandler).ConfigureAwait(false);
        }

        /// <summary>發送 HTTP PUT 的請求至遠端 http service。<para/>
        /// * 請求的 `Content-Type` 會強制固定為 `application/json`，無論 `HttpRequest` 是否有特別指定 `Content-Type` 格式。<para/>
        /// * 回應的 `Code` 假若回傳 `40099`，代表請求的 `HttpRequest` 參數不合法，無法執行 http PUT 請求發送。<para/>
        /// * 回應的 `Code` 假若回傳 `40098`，代表發送 http PUT 請求過程中發生程式異常。<para/>
        /// * 回應的 `Code` 假若回傳 `40097`，代表發送 http PUT 請求已經重新 retry 嘗試發送達到上限，但仍然發生錯誤。<para/>
        /// * 回應的 `Successs = true` 則 `Code = 20000`，其中 `Data` 代表 http service 服務端回應的資料字串。<para/>
        /// </summary>
        /// <param name="httpClient">HttpClient 客戶端物件</param>
        /// <param name="request">Http 請求資訊封裝</param>
        /// <param name="consoleOutputLog">是否進行主控台日誌輸出，預設不輸出。</param>
        /// <param name="reqHandler">Http 請求前的處理委派</param>
        /// <param name="resHandler">Http 請求後的處理委派</param>
        /// <param name="withHttp2">是否啟用 HTTP/2 通訊協定，需要 Http 服務端支援 HTTP/2 通訊協定，效能較佳。</param>
        /// <param name="httpRetryOption">Http 請求的重新嘗試選項</param>
        /// <returns>請求執行結果</returns>
        public static async Task<HttpResponse> ExecuteHttp2PutJsonAsync(this HttpClient httpClient, HttpRequest request,
            bool consoleOutputLog = false, Func<HttpRequest, Task> reqHandler = null, Func<HttpRequest, HttpResponse, Task> resHandler = null, bool withHttp2 = false, IRetryOption httpRetryOption = null)
        {
            return await Proxy.InvokeAsync<HttpResponse>(
                HttpPutAsync,
                retryOption: httpRetryOption,
                retryWhen: httpResponse => !httpResponse.Success && "40099" != httpResponse.Code,
                returnWhenRetryFail: httpResponse => new HttpResponse()
                {
                    StatusCode = httpResponse.StatusCode,
                    HttpResponseMessaage = httpResponse.HttpResponseMessaage,
                    Message = $"Send http request failure. This http PUT operation has been retry over {httpRetryOption.RetryFrequencies.Length} times but still retrive http server side error.{Environment.NewLine}Last retry error message:{Environment.NewLine}{httpResponse.Message}",
                    Code = $"40097",
                }).ConfigureAwait(false);

            async Task<HttpResponse> HttpPutAsync()
            {
                if (!withHttp2)
                {
                    return await ExecutePostOrPutAsync(
                        httpClient,
                        request,
                        async (path, content) => await httpClient.PutAsync(path, content).ConfigureAwait(false),
                        consoleOutputLog,
                        reqHandler,
                        resHandler).ConfigureAwait(false);
                }

                var httpHandlerAsync = new Func<string, HttpContent, Task<HttpResponseMessage>>(async (reqPath, reqContent) =>
                {
                    var httpRequestMessage = new HttpRequestMessage(HttpMethod.Put, reqPath)
                    {
                        Version = new Version(2, 0),
                        Content = reqContent
                    };

                    return await httpClient.SendAsync(httpRequestMessage).ConfigureAwait(false);
                });

                return await ExecutePostOrPutAsync(
                    httpClient,
                    request,
                    httpHandlerAsync,
                    consoleOutputLog,
                    reqHandler,
                    resHandler).ConfigureAwait(false);
            }
        }

        /// <summary>發送 HTTP PUT 的請求至遠端 http service。<para/>
        /// * 請求的 `Content-Type` 會強制固定為 `application/json`，無論 `HttpRequest` 是否有特別指定 `Content-Type` 格式。<para/>
        /// * 回應的 `Code` 假若回傳 `40099`，代表請求的 `HttpRequest` 參數不合法，無法執行 http PUT 請求發送。<para/>
        /// * 回應的 `Code` 假若回傳 `40098`，代表發送 http PUT 請求過程中發生程式異常。<para/>
        /// * 回應的 `Code` 假若回傳 `40097`，代表發送 http PUT 請求已經重新 retry 嘗試發送達到上限，但仍然發生錯誤。<para/>
        /// * 回應的 `Successs = true` 則 `Code = 20000`，其中 `Data` 代表 http service 服務端回應的資料字串。<para/>
        /// </summary>
        /// <param name="httpClient">HttpClient 客戶端物件</param>
        /// <param name="request">Http 請求資訊封裝</param>
        /// <param name="requestDTO">Http 請求資料載體，將使用 System.Text.Json.JsonSerializer 序列化處理。</param>
        /// <param name="consoleOutputLog">是否進行主控台日誌輸出，預設不輸出。</param>
        /// <param name="reqHandler">Http 請求前的處理委派</param>
        /// <param name="resHandler">Http 請求後的處理委派</param>
        /// <param name="httpRetryOption">Http 請求的重新嘗試選項</param>
        /// <typeparam name="TRequestDTO">請求資料載體泛型</typeparam>
        /// <typeparam name="TResponseDTO">回應資料載體泛型</typeparam>
        /// <returns>請求執行結果</returns>
        public static HttpResponse<TResponseDTO> ExecutePutJson<TRequestDTO, TResponseDTO>(
            this HttpClient httpClient,
            HttpRequest request,
            TRequestDTO requestDTO = null,
            bool consoleOutputLog = false,
            Func<HttpRequest, Task> reqHandler = null,
            Func<HttpRequest, HttpResponse, Task> resHandler = null,
            IRetryOption httpRetryOption = null)
            where TRequestDTO : class
            where TResponseDTO : class
        {
            var response = new HttpResponse<TResponseDTO>();
            var requestBody = default(string);
            request.IsNotNull(req => requestBody = Serialize(requestDTO));
            request.Body = requestBody;

            var r = ExecutePutJson(httpClient, request, consoleOutputLog, reqHandler, resHandler, httpRetryOption);

            response.RequestId = r.RequestId;
            response.StatusCode = r.StatusCode;
            response.HttpResponseMessaage = r.HttpResponseMessaage;
            response.Data = r.Data;
            response.Code = r.Code;
            response.Message = r.Message;

            if (!r.Success)
            {
                response.Payload = null;
                response.Success = false;
                return response;
            }

            var payload = default(TResponseDTO);
            response.Data.IsNotNullOrEmpty(res => payload = Deserialize<TResponseDTO>(res));
            response.Payload = payload;
            response.StatusCode = HttpStatusCode.OK;
            response.Code = "20000";
            response.Success = true;
            return response;
        }

        /// <summary>發送 HTTP PUT 的請求至遠端 http service。<para/>
        /// * 請求的 `Content-Type` 會強制固定為 `application/json`，無論 `HttpRequest` 是否有特別指定 `Content-Type` 格式。<para/>
        /// * 回應的 `Code` 假若回傳 `40099`，代表請求的 `HttpRequest` 參數不合法，無法執行 http PUT 請求發送。<para/>
        /// * 回應的 `Code` 假若回傳 `40098`，代表發送 http PUT 請求過程中發生程式異常。<para/>
        /// * 回應的 `Code` 假若回傳 `40097`，代表發送 http PUT 請求已經重新 retry 嘗試發送達到上限，但仍然發生錯誤。<para/>
        /// * 回應的 `Successs = true` 則 `Code = 20000`，其中 `Data` 代表 http service 服務端回應的資料字串。<para/>
        /// </summary>
        /// <param name="httpClient">HttpClient 客戶端物件</param>
        /// <param name="request">Http 請求資訊封裝</param>
        /// <param name="requestDTO">Http 請求資料載體，將使用 System.Text.Json.JsonSerializer 序列化處理。</param>
        /// <param name="consoleOutputLog">是否進行主控台日誌輸出，預設不輸出。</param>
        /// <param name="reqHandler">Http 請求前的處理委派</param>
        /// <param name="resHandler">Http 請求後的處理委派</param>
        /// <param name="httpRetryOption">Http 請求的重新嘗試選項</param>
        /// <typeparam name="TRequestDTO">請求資料載體泛型</typeparam>
        /// <typeparam name="TResponseDTO">回應資料載體泛型</typeparam>
        /// <returns>請求執行結果</returns>
        public static async Task<HttpResponse<TResponseDTO>> ExecutePutJsonAsync<TRequestDTO, TResponseDTO>(
            this HttpClient httpClient,
            HttpRequest request,
            TRequestDTO requestDTO = null,
            bool consoleOutputLog = false,
            Func<HttpRequest, Task> reqHandler = null,
            Func<HttpRequest, HttpResponse, Task> resHandler = null,
            IRetryOption httpRetryOption = null)
            where TRequestDTO : class
            where TResponseDTO : class
        {
            var response = new HttpResponse<TResponseDTO>();
            var requestBody = default(string);
            request.IsNotNull(req => requestBody = Serialize(requestDTO));
            request.Body = requestBody;

            var r = await ExecutePutJsonAsync(httpClient, request, consoleOutputLog, reqHandler, resHandler, httpRetryOption).ConfigureAwait(false);

            response.RequestId = r.RequestId;
            response.StatusCode = r.StatusCode;
            response.HttpResponseMessaage = r.HttpResponseMessaage;
            response.Data = r.Data;
            response.Code = r.Code;
            response.Message = r.Message;

            if (!r.Success)
            {
                response.Payload = null;
                response.Success = false;
                return response;
            }

            var payload = default(TResponseDTO);
            response.Data.IsNotNullOrEmpty(res => payload = Deserialize<TResponseDTO>(res));
            response.Payload = payload;
            response.StatusCode = HttpStatusCode.OK;
            response.Code = "20000";
            response.Success = true;
            return response;
        }

        /// <summary>發送 HTTP PUT 的請求至遠端 http service。<para/>
        /// * 請求的 `Content-Type` 會強制固定為 `application/json`，無論 `HttpRequest` 是否有特別指定 `Content-Type` 格式。<para/>
        /// * 回應的 `Code` 假若回傳 `40099`，代表請求的 `HttpRequest` 參數不合法，無法執行 http PUT 請求發送。<para/>
        /// * 回應的 `Code` 假若回傳 `40098`，代表發送 http PUT 請求過程中發生程式異常。<para/>
        /// * 回應的 `Code` 假若回傳 `40097`，代表發送 http PUT 請求已經重新 retry 嘗試發送達到上限，但仍然發生錯誤。<para/>
        /// * 回應的 `Successs = true` 則 `Code = 20000`，其中 `Data` 代表 http service 服務端回應的資料字串。<para/>
        /// </summary>
        /// <param name="httpClient">HttpClient 客戶端物件</param>
        /// <param name="request">Http 請求資訊封裝</param>
        /// <param name="requestDTO">Http 請求資料載體，將使用 System.Text.Json.JsonSerializer 序列化處理。</param>
        /// <param name="consoleOutputLog">是否進行主控台日誌輸出，預設不輸出。</param>
        /// <param name="reqHandler">Http 請求前的處理委派</param>
        /// <param name="resHandler">Http 請求後的處理委派</param>
        /// <param name="withHttp2">是否啟用 HTTP/2 通訊協定，需要 Http 服務端支援 HTTP/2 通訊協定，效能較佳。</param>
        /// <param name="httpRetryOption">Http 請求的重新嘗試選項</param>
        /// <typeparam name="TRequestDTO">請求資料載體泛型</typeparam>
        /// <typeparam name="TResponseDTO">回應資料載體泛型</typeparam>
        /// <returns>請求執行結果</returns>
        public static async Task<HttpResponse<TResponseDTO>> ExecuteHttp2PutJsonAsync<TRequestDTO, TResponseDTO>(
            this HttpClient httpClient,
            HttpRequest request,
            TRequestDTO requestDTO = null,
            bool consoleOutputLog = false,
            Func<HttpRequest, Task> reqHandler = null,
            Func<HttpRequest, HttpResponse, Task> resHandler = null,
            bool withHttp2 = false,
            IRetryOption httpRetryOption = null)
            where TRequestDTO : class
            where TResponseDTO : class
        {
            var response = new HttpResponse<TResponseDTO>();
            var requestBody = default(string);
            request.IsNotNull(req => requestBody = Serialize(requestDTO));
            request.Body = requestBody;

            var r = await ExecuteHttp2PutJsonAsync(httpClient, request, consoleOutputLog, reqHandler, resHandler, withHttp2, httpRetryOption).ConfigureAwait(false);

            response.RequestId = r.RequestId;
            response.StatusCode = r.StatusCode;
            response.HttpResponseMessaage = r.HttpResponseMessaage;
            response.Data = r.Data;
            response.Code = r.Code;
            response.Message = r.Message;

            if (!r.Success)
            {
                response.Payload = null;
                response.Success = false;
                return response;
            }

            var payload = default(TResponseDTO);
            response.Data.IsNotNullOrEmpty(res => payload = Deserialize<TResponseDTO>(res));
            response.Payload = payload;
            response.StatusCode = HttpStatusCode.OK;
            response.Code = "20000";
            response.Success = true;
            return response;
        }

        #endregion Http PUT Methods


        #region Http DELETE Methods

        /// <summary>發送 HTTP DELETE 的請求至遠端 http service。<para/>
        /// * 回應的 `Code` 假若回傳 `40099`，代表請求的 `HttpRequest` 參數不合法，無法執行 http DELETE 請求發送。<para/>
        /// * 回應的 `Code` 假若回傳 `40098`，代表發送 http DELETE 請求過程中發生程式異常。<para/>
        /// * 回應的 `Code` 假若回傳 `40097`，代表發送 http DELETE 請求已經重新 retry 嘗試發送達到上限，但仍然發生錯誤。<para/>
        /// * 請求的 `HttpRequest` 如果有傳入 QueryStrings 查詢字串參數，會自動以 `HttpUtility.UrlEncode()` 進行編碼處理。<para/>
        /// * 回應的 `Successs = true` 則 `Code = 20000`，其中 `Data` 代表 http service 服務端回應的資料字串。<para/>
        /// </summary>
        /// <param name="httpClient">HttpClient 客戶端物件</param>
        /// <param name="request">Http 請求資訊封裝</param>
        /// <param name="consoleOutputLog">是否進行主控台日誌輸出，預設不輸出。</param>
        /// <param name="reqHandler">Http 請求前的處理委派</param>
        /// <param name="resHandler">Http 請求後的處理委派</param>
        /// <param name="httpRetryOption">Http 請求的重新嘗試選項</param>
        /// <returns>請求執行結果</returns>
        public static HttpResponse ExecuteDelete(this HttpClient httpClient, HttpRequest request,
            bool consoleOutputLog = false, Func<HttpRequest, Task> reqHandler = null, Func<HttpRequest, HttpResponse, Task> resHandler = null, IRetryOption httpRetryOption = null)
        {
            return Proxy.Invoke<HttpResponse>(
                HttpDelete,
                retryOption: httpRetryOption,
                retryWhen: httpResponse => !httpResponse.Success && "40099" != httpResponse.Code,
                returnWhenRetryFail: httpResponse => new HttpResponse()
                {
                    StatusCode = httpResponse.StatusCode,
                    HttpResponseMessaage = httpResponse.HttpResponseMessaage,
                    Message = $"Send http request failure. This http DELETE operation has been retry over {httpRetryOption.RetryFrequencies.Length} times but still retrive http server side error.{Environment.NewLine}Last retry error message:{Environment.NewLine}{httpResponse.Message}",
                    Code = $"40097",
                });

            HttpResponse HttpDelete() => ExecuteDeleteAsync(httpClient, request, consoleOutputLog, reqHandler, resHandler).ConfigureAwait(false).GetAwaiter().GetResult();
        }

        /// <summary>發送 HTTP DELETE 的請求至遠端 http service。<para/>
        /// * 回應的 `Code` 假若回傳 `40099`，代表請求的 `HttpRequest` 參數不合法，無法執行 http DELETE 請求發送。<para/>
        /// * 回應的 `Code` 假若回傳 `40098`，代表發送 http DELETE 請求過程中發生程式異常。<para/>
        /// * 回應的 `Code` 假若回傳 `40097`，代表發送 http DELETE 請求已經重新 retry 嘗試發送達到上限，但仍然發生錯誤。<para/>
        /// * 回應的 `Successs = true` 則 `Code = 20000`，其中 `Data` 代表 http service 服務端回應的資料字串。<para/>
        /// </summary>
        /// <param name="httpClient">HttpClient 客戶端物件</param>
        /// <param name="request">Http 請求資訊封裝</param>
        /// <param name="consoleOutputLog">是否進行主控台日誌輸出，預設不輸出。</param>
        /// <param name="reqHandler">Http 請求前的處理委派</param>
        /// <param name="resHandler">Http 請求後的處理委派</param>
        /// <param name="httpRetryOption">Http 請求的重新嘗試選項</param>
        /// <returns>請求執行結果</returns>
        public static async Task<HttpResponse> ExecuteDeleteAsync(this HttpClient httpClient, HttpRequest request,
            bool consoleOutputLog = false, Func<HttpRequest, Task> reqHandler = null, Func<HttpRequest, HttpResponse, Task> resHandler = null, IRetryOption httpRetryOption = null)
        {
            return await Proxy.InvokeAsync<HttpResponse>(
                HttpDeleteAsync,
                retryOption: httpRetryOption,
                retryWhen: httpResponse => !httpResponse.Success && "40099" != httpResponse.Code,
                returnWhenRetryFail: httpResponse => new HttpResponse()
                {
                    StatusCode = httpResponse.StatusCode,
                    HttpResponseMessaage = httpResponse.HttpResponseMessaage,
                    Message = $"Send http request failure. This http DELETE operation has been retry over {httpRetryOption.RetryFrequencies.Length} times but still retrive http server side error.{Environment.NewLine}Last retry error message:{Environment.NewLine}{httpResponse.Message}",
                    Code = $"40097",
                }).ConfigureAwait(false);

            async Task<HttpResponse> HttpDeleteAsync() =>
                await ExecuteGetOrDeleteAsync(
                    httpClient,
                    request,
                    async path => await httpClient.DeleteAsync(path).ConfigureAwait(false),
                    consoleOutputLog,
                    reqHandler,
                    resHandler).ConfigureAwait(false);
        }

        /// <summary>發送 HTTP DELETE 的請求至遠端 http service。<para/>
        /// * 回應的 `Code` 假若回傳 `40099`，代表請求的 `HttpRequest` 參數不合法，無法執行 http DELETE 請求發送。<para/>
        /// * 回應的 `Code` 假若回傳 `40098`，代表發送 http DELETE 請求過程中發生程式異常。<para/>
        /// * 回應的 `Code` 假若回傳 `40097`，代表發送 http DELETE 請求已經重新 retry 嘗試發送達到上限，但仍然發生錯誤。<para/>
        /// * 回應的 `Successs = true` 則 `Code = 20000`，其中 `Data` 代表 http service 服務端回應的資料字串。<para/>
        /// </summary>
        /// <param name="httpClient">HttpClient 客戶端物件</param>
        /// <param name="request">Http 請求資訊封裝</param>
        /// <param name="consoleOutputLog">是否進行主控台日誌輸出，預設不輸出。</param>
        /// <param name="reqHandler">Http 請求前的處理委派</param>
        /// <param name="resHandler">Http 請求後的處理委派</param>
        /// <param name="withHttp2">是否啟用 HTTP/2 通訊協定，需要 Http 服務端支援 HTTP/2 通訊協定，效能較佳。</param>
        /// <param name="httpRetryOption">Http 請求的重新嘗試選項</param>
        /// <returns>請求執行結果</returns>
        public static async Task<HttpResponse> ExecuteHttp2DeleteAsync(this HttpClient httpClient, HttpRequest request,
            bool consoleOutputLog = false, Func<HttpRequest, Task> reqHandler = null, Func<HttpRequest, HttpResponse, Task> resHandler = null, bool withHttp2 = false, IRetryOption httpRetryOption = null)
        {
            return await Proxy.InvokeAsync<HttpResponse>(
                HttpDeleteAsync,
                retryOption: httpRetryOption,
                retryWhen: httpResponse => !httpResponse.Success && "40099" != httpResponse.Code,
                returnWhenRetryFail: httpResponse => new HttpResponse()
                {
                    StatusCode = httpResponse.StatusCode,
                    HttpResponseMessaage = httpResponse.HttpResponseMessaage,
                    Message = $"Send http request failure. This http DELETE operation has been retry over {httpRetryOption.RetryFrequencies.Length} times but still retrive http server side error.{Environment.NewLine}Last retry error message:{Environment.NewLine}{httpResponse.Message}",
                    Code = $"40097",
                }).ConfigureAwait(false);

            async Task<HttpResponse> HttpDeleteAsync()
            {
                if (!withHttp2)
                {
                    return await ExecuteGetOrDeleteAsync(
                        httpClient,
                        request,
                        async path => await httpClient.DeleteAsync(path).ConfigureAwait(false),
                        consoleOutputLog,
                        reqHandler,
                        resHandler).ConfigureAwait(false);
                }

                var httpHandlerAsync = new Func<string, Task<HttpResponseMessage>>(async reqPath =>
                {
                    var httpRequestMessage = new HttpRequestMessage(HttpMethod.Delete, reqPath)
                    {
                        Version = new Version(2, 0)
                    };

                    return await httpClient.SendAsync(httpRequestMessage).ConfigureAwait(false);
                });

                return await ExecuteGetOrDeleteAsync(
                    httpClient,
                    request,
                    httpHandlerAsync,
                    consoleOutputLog,
                    reqHandler,
                    resHandler).ConfigureAwait(false);
            }
        }

        #endregion Http DELETE Methods


        #region Private Methods

        /// <summary>發送 HTTP GET or DELETE 的請求至遠端 http service。<para/>
        /// * 回應的 `Code` 假若回傳 `40099`，代表請求的 `HttpRequest` 參數不合法，無法執行 http GET or DELETE 請求發送。<para/>
        /// * 回應的 `Code` 假若回傳 `40098`，代表發送 http GET or DELETE 請求過程中發生程式異常。<para/>
        /// * 請求的 `HttpRequest` 如果有傳入 QueryStrings 查詢字串參數，會自動以 `HttpUtility.UrlEncode()` 進行編碼處理。<para/>
        /// * 回應的 `Successs = true` 則 `Code = 20000`，其中 `Data` 代表 http service 服務端回應的資料字串。<para/>
        /// </summary>
        /// <param name="httpClient">HttpClient 客戶端物件</param>
        /// <param name="request">Http 請求資訊封裝</param>
        /// <param name="httpHandlerAsync">Http GET 或 Http DELETE 請求的處理委派</param>
        /// <param name="consoleOutputLog">是否進行主控台日誌輸出，預設不輸出。</param>
        /// <param name="reqHandler">Http 請求前的處理委派</param>
        /// <param name="resHandler">Http 請求後的處理委派</param>
        /// <returns>請求執行結果</returns>
        private static async Task<HttpResponse> ExecuteGetOrDeleteAsync(HttpClient httpClient, HttpRequest request, Func<string, Task<HttpResponseMessage>> httpHandlerAsync,
            bool consoleOutputLog = false, Func<HttpRequest, Task> reqHandler = null, Func<HttpRequest, HttpResponse, Task> resHandler = null)
        {
            var result = new HttpResponse
            {
                Success = false,
                Code = $"40099",
            };

            try
            {
                if (httpClient.IsNull())
                {
                    result.Message = $"Invalid request. The HttpClient instance is null. Can not send http request to remote http service.";
                    return result;
                }

                if (request.IsNull())
                {
                    result.Message = $"Invalid request. The HttpRequest argument is null. Can not send http request to remote http service.";
                    return result;
                }

                if (request.Path.IsNotNullOrEmpty() && request.Path.StartsWith('/'))
                {
                    request.Path = request.Path?.RemoveFirstAppeared("/");
                }

                httpClient.DefaultRequestHeaders.Clear();

                if (request.AuthorizationHeaderKey.IsNotNullOrEmpty() && request.AuthorizationHeaderValue.IsNotNullOrEmpty())
                {
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(request.AuthorizationHeaderKey, request.AuthorizationHeaderValue);
                }

                if (request.Headers.IsNotNullOrEmpty())
                {
                    if (!request.Headers.ContainsKey("Accept"))
                    {
                        httpClient.DefaultRequestHeaders.Add("Accept", "application/json");
                    }

                    foreach (var header in request.Headers)
                    {
                        if (header.IsNull() || header.Key.IsNullOrEmpty() || header.Value.IsNullOrEmpty())
                        {
                            continue;
                        }

                        if (0 == string.Compare(header.Key, "Content-Type", true))
                        {
                            continue;
                        }

                        httpClient.DefaultRequestHeaders.Add(header.Key, header.Value);
                    }
                }

                if (request.QueryStrings.IsNotNullOrEmpty())
                {
                    var sbQueryStrings = new StringBuilder();

                    foreach (var query in request.QueryStrings)
                    {
                        if (query.IsNull() || query.Key.IsNullOrEmpty() || query.Value.IsNullOrEmpty())
                        {
                            continue;
                        }

                        sbQueryStrings.Append($"{query.Key.Trim()}={HttpUtility.UrlEncode(query.Value.Trim())}&");
                    }

                    var queryString = sbQueryStrings.ToString()?.RemoveLastAppeared("&");

                    if (queryString.IsNotNullOrEmpty())
                    {
                        if (request.Path.IsNotNullOrEmpty() && request.Path.EndsWith('/'))
                        {
                            request.Path = request.Path.RemoveLastAppeared("/");
                        }

                        if (request.Path.IsNotNullOrEmpty() && request.Path.Contains('?'))
                        {
                            request.Path = request.Path[..^(request.Path.Length - request.Path.IndexOf('?'))];
                        }

                        request.Path = $"{request.Path}?{queryString}";
                    }
                }

                request.RequestId.IsNullOrEmpty(() => request.RequestId = Guid.NewGuid().ToString());
                await ExecuteHandlerAsync(request, consoleOutputLog, reqHandler).ConfigureAwait(false);

                var httpResponse = await httpHandlerAsync(request.Path).ConfigureAwait(false);
                result.RequestId = request.RequestId;
                result.StatusCode = httpResponse.StatusCode;
                result.HttpResponseMessaage = httpResponse;

                if (httpResponse.IsNull() || !httpResponse.IsSuccessStatusCode)
                {
                    result.Code = ((int)httpResponse.StatusCode) + "";
                    result.Message = httpResponse.ReasonPhrase;
                    await ExecuteHandlerAsync(request, result, consoleOutputLog, resHandler).ConfigureAwait(false);
                    return result;
                }

                var resContent = await httpResponse.Content.ReadAsStringAsync().ConfigureAwait(false);

                result.Data = resContent;
                result.Code = "20000";
                result.Success = true;
                await ExecuteHandlerAsync(request, result, consoleOutputLog, resHandler).ConfigureAwait(false);
                return result;
            }
            catch (Exception ex)
            {
                result.Code = $"40098";
                result.Message = $"An error occured while try to send http request to remote http api service.{Environment.NewLine}{ex}";
                result.ExceptionObject = ex;
                result.HasException = true;
                return result;
            }
        }

        /// <summary>發送 HTTP POST or PUT 的請求至遠端 http service。<para/>
        /// * 請求的 `Content-Type` 會強制固定為 `application/json`，無論 `HttpRequest` 是否有特別指定 `Content-Type` 格式。<para/>
        /// * 回應的 `Code` 假若回傳 `40099`，代表請求的 `HttpRequest` 參數不合法，無法執行 http POST or PUT 請求發送。<para/>
        /// * 回應的 `Code` 假若回傳 `40098`，代表發送 http POST or PUT 請求過程中發生程式異常。<para/>
        /// * 回應的 `Successs = true` 則 `Code = 20000`，其中 `Data` 代表 http service 服務端回應的資料字串。<para/>
        /// </summary>
        /// <param name="httpClient">HttpClient 客戶端物件</param>
        /// <param name="request">Http 請求資訊封裝</param>
        /// <param name="httpHandlerAsync">Http POST 或 Http PUT 請求的處理委派</param>
        /// <param name="consoleOutputLog">是否進行主控台日誌輸出，預設不輸出。</param>
        /// <param name="reqHandler">Http 請求前的處理委派</param>
        /// <param name="resHandler">Http 請求後的處理委派</param>
        /// <returns>請求執行結果</returns>
        private static async Task<HttpResponse> ExecutePostOrPutAsync(HttpClient httpClient, HttpRequest request, Func<string, HttpContent, Task<HttpResponseMessage>> httpHandlerAsync,
            bool consoleOutputLog = false, Func<HttpRequest, Task> reqHandler = null, Func<HttpRequest, HttpResponse, Task> resHandler = null)
        {
            var result = new HttpResponse
            {
                Success = false,
                Code = $"40099",
            };

            try
            {
                if (httpClient.IsNull())
                {
                    result.Message = $"Invalid request. The HttpClient instance is null. Can not send http request to remote http service.";
                    return result;
                }

                if (request.IsNull())
                {
                    result.Message = $"Invalid request. The HttpRequest argument is null. Can not send http request to remote http service.";
                    return result;
                }

                if (request.Path.IsNotNullOrEmpty() && request.Path.StartsWith('/'))
                {
                    request.Path = request.Path?.RemoveFirstAppeared("/");
                }

                httpClient.DefaultRequestHeaders.Clear();

                if (request.AuthorizationHeaderKey.IsNotNullOrEmpty() && request.AuthorizationHeaderValue.IsNotNullOrEmpty())
                {
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(request.AuthorizationHeaderKey, request.AuthorizationHeaderValue);
                }

                if (request.Headers.IsNotNullOrEmpty())
                {
                    if (!request.Headers.ContainsKey("Accept"))
                    {
                        httpClient.DefaultRequestHeaders.Add("Accept", "application/json");
                    }

                    foreach (var header in request.Headers)
                    {
                        if (header.IsNull() || header.Key.IsNullOrEmpty() || header.Value.IsNullOrEmpty())
                        {
                            continue;
                        }

                        if (0 == string.Compare(header.Key, "Content-Type", true))
                        {
                            continue;
                        }

                        httpClient.DefaultRequestHeaders.Add(header.Key, header.Value);
                    }
                }

                HttpContent reqContent = null;

                if (request.Body.IsNotNullOrEmpty())
                {
                    reqContent = new StringContent(request.Body, Encoding.UTF8, "application/json");
                }

                request.RequestId.IsNullOrEmpty(() => request.RequestId = Guid.NewGuid().ToString());
                await ExecuteHandlerAsync(request, consoleOutputLog, reqHandler).ConfigureAwait(false);

                var httpResponse = await httpHandlerAsync(request.Path, reqContent).ConfigureAwait(false);
                result.RequestId = request.RequestId;
                result.StatusCode = httpResponse.StatusCode;
                result.HttpResponseMessaage = httpResponse;

                if (httpResponse.IsNull() || !httpResponse.IsSuccessStatusCode)
                {
                    result.Code = ((int)httpResponse.StatusCode) + "";
                    result.Message = httpResponse.ReasonPhrase;
                    await ExecuteHandlerAsync(request, result, consoleOutputLog, resHandler).ConfigureAwait(false);
                    return result;
                }

                var resContent = await httpResponse.Content.ReadAsStringAsync().ConfigureAwait(false);

                result.Data = resContent;
                result.Code = "20000";
                result.Success = true;
                await ExecuteHandlerAsync(request, result, consoleOutputLog, resHandler).ConfigureAwait(false);
                return result;
            }
            catch (Exception ex)
            {
                result.Code = $"40098";
                result.Message = $"An error occured while try to send http request to remote http api service.{Environment.NewLine}{ex}";
                result.ExceptionObject = ex;
                result.HasException = true;
                return result;
            }
        }

        /// <summary>觸發請求前的處理委派
        /// </summary>
        /// <param name="consoleOutputLog">是否進行主控台日誌輸出，預設不輸出。</param>
        /// <param name="request">Http 請求資訊封裝</param>
        /// <param name="reqHandler">請求前的處理委派</param>
        private static async Task<int> ExecuteHandlerAsync(HttpRequest request, bool consoleOutputLog = false, Func<HttpRequest, Task> reqHandler = null)
        {
            if (consoleOutputLog)
            {
                await Command.StdoutAsync($"Http client request log. RequestId: {request.RequestId}.{Environment.NewLine}{JsonSerializer.Serialize(request)}", ConsoleColor.Magenta).ConfigureAwait(false);
            }

            if (reqHandler.IsNotNull())
            {
                await reqHandler(request).ConfigureAwait(false);
            }

            return await Task.FromResult(0).ConfigureAwait(false);
        }

        /// <summary>觸發請求後的處理委派
        /// </summary>
        /// <param name="request">Http 請求資訊封裝</param>
        /// <param name="response">Http 回應資訊封裝</param>
        /// <param name="consoleOutputLog">是否進行主控台日誌輸出，預設不輸出。</param>
        /// <param name="resHandler">請求前的處理委派</param>
        private static async Task<int> ExecuteHandlerAsync(HttpRequest request, HttpResponse response, bool consoleOutputLog = false, Func<HttpRequest, HttpResponse, Task> resHandler = null)
        {
            if (consoleOutputLog)
            {
                await Command.StdoutAsync($"Http client response log. RequestId: {response.RequestId}.{Environment.NewLine}{JsonSerializer.Serialize(request)}", ConsoleColor.Magenta).ConfigureAwait(false);
            }

            if (resHandler.IsNotNull())
            {
                await resHandler(request, response).ConfigureAwait(false);
            }

            return await Task.FromResult(0).ConfigureAwait(false);
        }

        #endregion Private Methods
    }
}