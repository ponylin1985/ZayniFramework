using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using static System.Text.Json.JsonSerializer;


namespace ZayniFramework.Common
{
    /// <summary>針對所有 .NET 泛型物件的擴充類別
    /// </summary>
    public static class GenericObjectExtension
    {
        #region When Extension Methods

        /// <summary>當條件成立時，執行 handler 委派並且回傳。
        /// </summary>
        /// <param name="source"></param>
        /// <param name="predicate">條件判斷委派</param>
        /// <param name="handler">條件成立時的回呼委派</param>
        /// <typeparam name="T">物件的泛型</typeparam>
        /// <returns>回呼委派的執行結果</returns>
        public static T When<T>(this T source, Func<T, bool> predicate, Func<T, T> handler)
        {
            if (predicate(source))
            {
                return handler(source);
            }

            return source;
        }

        /// <summary>當條件成立時，執行 handler 委派並且回傳。
        /// </summary>
        /// <param name="source"></param>
        /// <param name="predicate">條件判斷委派</param>
        /// <param name="asyncFunc">條件成立時的回呼委派</param>
        /// <typeparam name="T">物件的泛型</typeparam>
        /// <returns>回呼委派的執行結果</returns>
        public static async Task<T> When<T>(this T source, Func<T, bool> predicate, Func<T, Task<T>> asyncFunc)
        {
            if (predicate(source))
            {
                return await asyncFunc(source);
            }

            return source;
        }

        /// <summary>當條件成立時，執行 handler 委派動作
        /// </summary>
        /// <param name="source"></param>
        /// <param name="predicate">條件判斷委派</param>
        /// <param name="handler">條件成立時的回呼委派</param>
        /// <typeparam name="T">物件的泛型</typeparam>
        public static void When<T>(this T source, Func<T, bool> predicate, Action<T> handler)
        {
            if (predicate(source))
            {
                handler(source);
            }
        }

        /// <summary>當條件成立時，執行 handler 委派動作
        /// </summary>
        /// <param name="source"></param>
        /// <param name="predicate">條件判斷委派</param>
        /// <param name="asyncFunc">條件成立時的回呼委派</param>
        /// <typeparam name="T">物件的泛型</typeparam>
        public static async Task When<T>(this T source, Func<T, bool> predicate, Func<T, Task> asyncFunc)
        {
            if (predicate(source))
            {
                await asyncFunc(source);
            }
        }

        /// <summary>當為真的情況下，執行 hanlder 委派動作
        /// </summary>
        /// <param name="source"></param>
        /// <param name="handler">為真的情況下執行的回呼委派</param>
        public static void WhenTrue(this bool source, Action handler)
        {
            if (source)
            {
                handler();
            }
        }

        /// <summary>當為真的情況下，執行 hanlder 委派動作
        /// </summary>
        /// <param name="source"></param>
        /// <param name="asyncFunc">為真的情況下執行的回呼委派</param>
        public static async Task WhenTrue(this bool source, Func<Task> asyncFunc)
        {
            if (source)
            {
                await asyncFunc();
            }
        }

        /// <summary>當為假的情況下，執行 hanlder 委派動作
        /// </summary>
        /// <param name="source"></param>
        /// <param name="handler">為假的情況下執行的回呼委派</param>
        public static void WhenFalse(this bool source, Action handler)
        {
            if (!source)
            {
                handler();
            }
        }

        /// <summary>當為假的情況下，執行 hanlder 委派動作
        /// </summary>
        /// <param name="source"></param>
        /// <param name="asyncFunc">為假的情況下執行的回呼委派</param>
        public static async Task WhenFalse(this bool source, Func<Task> asyncFunc)
        {
            if (!source)
            {
                await asyncFunc();
            }
        }

        #endregion When Extension Methods


        #region Map Extension Methods

        /// <summary>資料對應轉換
        /// </summary>
        /// <param name="source">來源資料載體物件</param>
        /// <param name="mapperFunc">資料對應轉換委派</param>
        /// <typeparam name="TSource">來源資料載體的泛型</typeparam>
        /// <typeparam name="TConvert">目標資料載體的泛型</typeparam>
        /// <returns>目標資料載體物件</returns>
        public static TConvert Map<TSource, TConvert>(this TSource source, Func<TSource, TConvert> mapperFunc)
            where TConvert : new()
        {
            if (null == source)
            {
                return default;
            }

            return mapperFunc(source);
        }

        #endregion Map Extension Methods


        #region Deep Clone Extension Methods

        /// <summary>物件深度複製。<para/>
        /// 1. 物件本身必須要可以支援被 Json.NET (Newtonsoft.Json.dll) 的 JSON 序列化與反序列化。<para/>
        /// 2. 傳入的來源物件，其中 Property 的實作方式，必須按照 C# Auto-Implemented Property 規範，否則複製出的物件屬性值可能會有不正確的情況。<para/>
        /// https://msdn.microsoft.com/zh-tw/library/bb384054.aspx
        /// </summary>
        /// <remarks>
        /// 1. 物件本身必須要可以支援被 Json.NET (Newtonsoft.Json.dll) 的 JSON 序列化與反序列化。
        /// 2. 傳入的來源物件，其中 Property 的實作方式，必須按照 C# Auto-Implemented Property 規範，否則複製出的物件屬性值可能會有不正確的情況。
        /// https://msdn.microsoft.com/zh-tw/library/bb384054.asp
        /// </remarks>
        /// <typeparam name="TSource">被深度複製的泛型，物件本身必須要可以支援被 Json.NET (Newtonsoft.Json) 序列化。</typeparam>
        /// <param name="source">來源物件</param>
        /// <returns>複製出的物件</returns>
        public static TSource CloneObject<TSource>(this TSource source)
            where TSource : class
        {
            if (null == source)
            {
                return default;
            }

            return JsonConvert.DeserializeObject<TSource>(JsonConvert.SerializeObject(source));
        }

        /// <summary>物件深度複製，採用 System.Text.Json.JsonSerializer 進行序列化機制進行物件屬性深度複製。<para/>
        /// * 物件本身需要可以支援 System.Text.Json.JsonSerializer 進行序列化與反序列化。
        /// * 物件本身的 Property 宣告需要正確使用 System.Text.Json 的 [JsonPropertyName] 進行 JSON 屬性名稱標記，必且只宣告為 public property。
        /// </summary>
        /// <typeparam name="TSource">被深度複製的泛型，物件本身必須要可以支援被 System.Text.Json.JsonSerializer 序列化。</typeparam>
        /// <param name="source">來源物件</param>
        /// <returns>複製出的物件</returns>
        public static TSource CloneObjectJson<TSource>(this TSource source)
        {
            if (null == source)
            {
                return default;
            }

            return Deserialize<TSource>(Serialize(source));
        }

        #endregion Deep Clone Extension Methods
    }
}