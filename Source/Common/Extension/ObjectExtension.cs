﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;


namespace ZayniFramework.Common
{
    /// <summary>Object 物件的擴充類別
    /// </summary>
    public static class ObjectExtension
    {
        #region IsNull Extension Methods

        /// <summary>檢查變數是否指向 Null
        /// </summary>
        /// <param name="source"></param>
        /// <returns>變數是否指向Null</returns>
        public static bool IsNull(this object source) => null == source;

        /// <summary>檢查變數使否指向 Null，如果 Null 值，就回傳預設值，如果不是 Null 就回傳原本的物件。
        /// </summary>
        /// <param name="source"></param>
        /// <param name="value">預設值</param>
        /// <returns>如果 Null 值，就回傳預設值，如果不是 Null 就回傳原本的物件。</returns>
        public static object IsNull(this object source, object value) => source ?? value;

        /// <summary>檢查變數使否指向 Null，如果指向 Null 就會執行回呼處理的委派。
        /// </summary>
        /// <param name="source"></param>
        /// <param name="handler">回呼處理委派</param>
        /// <returns>變數使否指向 Null</returns>
        public static bool IsNull(this object source, Action handler)
        {
            var result = null == source;

            if (result)
            {
                try
                {
                    handler();
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return result;
        }

        /// <summary>檢查變數使否指向 Null，如果指向 Null 則以非同步作業執行回呼的委派。
        /// </summary>
        /// <param name="source"></param>
        /// <param name="asyncFunc">非同步作業的回呼委派</param>
        /// <returns>變數使否指向 Null</returns>
        public static async Task<bool> IsNull(this object source, Func<Task> asyncFunc)
        {
            var result = null == source;

            if (result)
            {
                try
                {
                    await asyncFunc();
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return result;
        }

        /// <summary>變數是否沒有指向 Null。
        /// </summary>
        /// <param name="source"></param>
        /// <returns>變數是否沒有指向Null</returns>
        public static bool IsNotNull(this object source) => null != source;

        /// <summary>變數是否沒有指向 Null，如果沒有指向 Null 就會執行回呼處理的委派。
        /// </summary>
        /// <param name="source"></param>
        /// <param name="handler">回呼處理委派</param>
        /// <returns>變數是否沒有指向 Null</returns>
        public static bool IsNotNull<T>(this T source, Action<T> handler)
        {
            var result = null != source;

            if (result)
            {
                try
                {
                    handler(source);
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return result;
        }

        /// <summary>變數是否沒有指向 Null，如果沒有指向 Null，則以非同步的作業執行回呼的委派
        /// </summary>
        /// <param name="source"></param>
        /// <param name="asyncFunc">非同步作業的回呼委派</param>
        /// <returns>變數是否沒有指向 Null</returns>
        public static async Task<bool> IsNotNull<T>(this T source, Func<T, Task> asyncFunc)
        {
            var result = null != source;

            if (result)
            {
                try
                {
                    await asyncFunc(source);
                }
                catch (Exception ex)
                {
                    Command.StdoutErr(ex.ToString());
                    throw;
                }
            }

            return result;
        }

        #endregion IsNull Extension Methods


        #region Object Reflection Extension Methods

        /// <summary>執行 Zayni Framework 框架的 AOP 切面攔截呼叫
        /// </summary>
        /// <param name="source"></param>
        /// <param name="methodName">目標方法名稱</param>
        /// <param name="methodParameters">方法的參數</param>
        /// <returns>執行回傳值</returns>
        public static object AspectInvoke(this object source, string methodName, params object[] methodParameters)
        {
            var result = AspectInvoker.Invoke(methodName, source, methodParameters: methodParameters);
            return result.ReturnValue;
        }

        /// <summary>取得目標物件的屬性值，C# 的 dynamic 物件無法呼叫調用此擴充方法。
        /// </summary>
        /// <param name="source">目標物件</param>
        /// <param name="propertyName">屬性值名稱</param>
        /// <returns>屬性值</returns>
        public static object GetPropertyValue(this object source, string propertyName)
        {
            if (!Reflector.TryGetPropertyValue(source, propertyName, out var value))
            {
                return null;
            }

            return value;
        }

        /// <summary>取得目標物件的屬性值
        /// </summary>
        /// <typeparam name="TResult">屬性的泛型</typeparam>
        /// <param name="source">目標物件</param>
        /// <param name="propertyName">屬性值名稱</param>
        /// <returns>屬性值</returns>
        public static TResult GetPropertyValue<TResult>(this object source, string propertyName) =>
            (TResult)GetPropertyValue(source, propertyName);

        /// <summary>對目標物件設定屬性值<para/>
        /// 1. 支援各種 Property 的型別，因為使用 Convert.ChangeType() 進行 runtime 轉型。<para/>
        /// 2. 支援 Nullable 的 Value Type 屬性值設地。<para/>
        /// 3. 以 PropertyInfo.SetValue() 反射的機制實作屬性值的設定。
        /// </summary>
        /// <param name="source">目標物件</param>
        /// <param name="propertyName">屬性名稱</param>
        /// <param name="value">屬性值 (資料值)</param>
        public static void SetPropertyValue(this object source, string propertyName, object value) =>
            Reflector.SetPropertyValue(source, propertyName, value);

        /// <summary>嘗試對目標物件設定屬性值<para/>
        /// 1. 支援各種 Property 的型別，因為使用 Convert.ChangeType() 進行 runtime 轉型。<para/>
        /// 2. 支援 Nullable 的 Value Type 屬性值設地。<para/>
        /// 3. 以 PropertyInfo.SetValue() 反射的機制實作屬性值的設定。
        /// </summary>
        /// <param name="source">目標物件</param>
        /// <param name="propertyName">屬性名稱</param>
        /// <param name="value">屬性值 (資料值)</param>
        /// <param name="message">錯誤訊息</param>
        /// <returns>屬性值設定是否成功</returns>
        public static bool TrySetPropertyValue(this object source, string propertyName, object value, out string message) =>
            Reflector.TrySetPropertyValue(source, propertyName, value, out message);

        /// <summary>反射取得物件的方法資訊
        /// </summary>
        /// <param name="source">目標物件</param>
        /// <param name="methodName">方法名稱</param>
        /// <returns>方法資訊</returns>
        public static MethodInfo GetMethod(this object source, string methodName) => Reflector.GetMethodInfo(source, methodName);

        /// <summary>以反射的方式呼叫物件的方法
        /// </summary>
        /// <param name="source">目標物件</param>
        /// <param name="methodName">方法名稱</param>
        /// <param name="parameters">方法的參數陣列</param>
        /// <returns>呼叫方法後的回傳值</returns>
        public static object MethodInvoke(this object source, string methodName, params object[] parameters) =>
            Reflector.InvokeMethod(source, methodName, parameters);

        /// <summary>以反射的方式呼叫物件的方法
        /// </summary>
        /// <typeparam name="TResult">回傳值的泛型</typeparam>
        /// <param name="source">目標物件</param>
        /// <param name="methodName">方法名稱</param>
        /// <param name="parameters">方法的參數陣列</param>
        /// <returns>呼叫方法後的回傳值</returns>
        public static TResult MethodInvoke<TResult>(this object source, string methodName, params object[] parameters) =>
            (TResult)Reflector.InvokeMethod(source, methodName, parameters);

        /// <summary>檢查是否為指定父型別的子型態物件
        /// </summary>
        /// <param name="source"></param>
        /// <param name="assemblyPath">來源組件路徑</param>
        /// <param name="typeFullName">目標型別完整名稱</param>
        /// <returns>是否為指定父型別的子型態物件</returns>
        public static bool IsTypeof(this object source, string assemblyPath, string typeFullName) => Reflector.IsTypeof(source, assemblyPath, typeFullName);

        /// <summary>檢查是否為指定父型別的子型態物件
        /// </summary>
        /// <param name="source"></param>
        /// <param name="typeFullName">父型別Type完整名稱字串</param>
        /// <returns>是否為指定父型別的子型態物件</returns>
        public static bool IsTypeof(this object source, string typeFullName) => Reflector.IsTypeof(source, typeFullName);

        /// <summary>檢查是否為指定父型別的子型態物件
        /// </summary>
        /// <param name="source"></param>
        /// <param name="type">父型別Type</param>
        /// <returns>是否為指定父型別的子型態物件</returns>
        public static bool IsTypeof(this object source, Type type) => Reflector.IsTypeof(source, type);

        #endregion Object Reflection Extension Methods


        #region Object Converting Extension Methods

        /// <summary>根據指定的泛型型別，將物件轉換成指定的型別。<para/>
        /// * 使用 Reflection 進行對應轉換，效能可能較差。
        /// </summary>
        /// <param name="source">來源物件</param>
        /// <typeparam name="TConvert">目標物件的泛型</typeparam>
        /// <returns>指定的型別的物件</returns>
        public static TConvert ConvertTo<TConvert>(this object source)
            where TConvert : new()
        {
            if (null == source)
            {
                return default;
            }

            var convert = new TConvert();
            var sourceProps = TypeDescriptor.GetProperties(source).Cast<PropertyDescriptor>();

            var convertProps = convert
                .GetType()
                .GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

            foreach (var sourceProp in sourceProps)
            {
                var convertProp = convertProps.FirstOrDefault(m => m.Name == sourceProp.Name);

                if (null == convertProp)
                {
                    continue;
                }

                try
                {
                    if (convertProp.PropertyType == typeof(DateTime?))
                    {
                        var dtValue = sourceProp.GetValue(source)?.ToString();
                        var dt = dtValue.IsNotNull() ? (DateTime.TryParse(dtValue, out var d) ? (DateTime?)d : null) : null;
                        convertProp.SetValue(convert, dt, null);
                        continue;
                    }

                    if (Reflector.IsNullableType(convertProp.PropertyType))
                    {
                        var type = Reflector.GetNullableType(convertProp.PropertyType);
                        var val = Convert.ChangeType(sourceProp.GetValue(source), type, CultureInfo.InvariantCulture);
                        convertProp.SetValue(convert, val, null);
                        continue;
                    }

                    var value = Convert.ChangeType(sourceProp.GetValue(source), convertProp.PropertyType, CultureInfo.InvariantCulture);
                    convertProp.SetValue(convert, value, null);
                }
                catch
                {
                    continue;
                }
            }

            return convert;
        }

        /// <summary>轉換成 IDictionary Key/Value Pair 的字典資料集合。<para/>
        /// var model = new UserModel<para/>
        /// {<para/>
        ///     Name = "Amber",<para/>
        ///     Age  = 23,<para/>
        ///     Sex  = "female"<para/>
        /// };<para/>
        /// IDictionary dict = model.ConvertToDictionary();<para/>
        /// string name = dict[ "Name" ] + "";<para/>
        /// string sex  = dict[ "Sex" ]  + "";<para/>
        /// int    age  = int.Parse( dict[ "Age" ] + "" );<para/>
        /// </summary>
        /// <example>
        /// <code>
        /// var model = new UserModel
        /// {
        ///     Name = "Amber",
        ///     Age  = 23,
        ///     Sex  = "female"
        /// };
        /// IDictionary dict = model.ConvertToDictionary();
        /// string name = dict[ "Name" ] + "";
        /// string sex  = dict[ "Sex" ]  + "";
        /// int    age  = int.Parse( dict[ "Age" ] + "" );
        /// var obj = new
        /// {
        ///     Message = "Hello World",
        ///     DoB     = DateTime.Now,
        ///     Money   = 324.52M,
        ///     Cash    = 76.123D,
        ///     Age     = 33,
        ///     User    = model
        /// };
        /// dict = obj.ConvertToDictionary();
        /// </code>
        /// </example>
        /// <param name="source">來源物件</param>
        /// <returns>IDictionary Key/Value Pair 的字典資料集合</returns>
        public static IDictionary ConvertToDictionary(this object source)
        {
            if (null == source)
            {
                return null;
            }

            var result = new Dictionary<string, object>();

            foreach (var property in source.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                var name = property.Name;
                var value = property.GetValue(source);
                result.Add(name, value);
            }

            return result;
        }

        /// <summary>轉換成 IDictionary 字串 String Key/String Value Pair 的純字串字典資料集合。<para/>
        /// var source = new UserModel<para/>
        /// {<para/>
        ///     Name = "Sylvia",<para/>
        ///     Age  = 26,<para/>
        ///     Sex  = "F"<para/>
        /// };<para/>
        /// Dictionary&lt;string, string&gt; d = source.ConvertToStringDictionary();<para/>
        /// string name2 = d[ "Name" ];<para/>
        /// string sex2  = d[ "Sex" ];<para/>
        /// string age2  = d[ "Age" ];
        /// </summary>
        /// <example>
        /// <code>
        /// var source = new UserModel
        /// {
        ///     Name = "Sylvia",
        ///     Age  = 26,
        ///     Sex  = "F"
        /// };
        /// Dictionary&lt;string, string&gt; d = source.ConvertToStringDictionary();
        /// string name2 = d[ "Name" ];
        /// string sex2  = d[ "Sex" ];
        /// string age2  = d[ "Age" ];
        /// </code>
        /// </example>
        /// <param name="source">來源物件</param>
        /// <returns>IDictionary String Key/String Value Pair 的純字串字典資料集合</returns>
        public static Dictionary<string, string> ConvertToStringDictionary(this object source)
        {
            if (null == source)
            {
                return null;
            }

            var result = new Dictionary<string, string>();

            foreach (var property in source.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                var name = property.Name;
                var value = property.GetValue(source);
                result.Add(name, value + "");
            }

            return result;
        }

        #endregion Object Converting Extension Methods


        #region Object[] Extension Methods

        /// <summary>物件陣列中是否有包含任何Null的元素
        /// </summary>
        /// <param name="source"></param>
        /// <returns>是否有包含任何Null的元素</returns>
        public static bool HasNullElements(this object[] source) => source.Any(m => m.IsNull());

        /// <summary>根據指定的泛型型別，將物件陣列轉換成指定型別的泛型 IList
        /// </summary>
        /// <typeparam name="TSource">目標型別泛型</typeparam>
        /// <param name="source">物件陣列</param>
        /// <returns>目標型別的泛型List</returns>
        public static IList<TSource> ToList<TSource>(this object[] source)
            where TSource : new()
        {
            var models = ConvertTo<TSource>(source);
            var result = new List<TSource>(models);
            return result;

            static IEnumerable<T> ConvertTo<T>(object[] src) where T : new()
            {
                for (int i = 0, len = src.Length; i < len; i++)
                {
                    yield return (T)src[i];
                }
            }
        }

        #endregion Object[] Extension Methods
    }
}
