﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;


namespace ZayniFramework.Common.Tasks
{
    /// <summary>非同步的工作佇列
    /// </summary>
    public sealed class TaskQueue
    {
        #region 宣告私有的欄位

        /// <summary>EnQueue 的多執行緒鎖定物件
        /// </summary>
        private readonly object _lockEnqueue = new();

        /// <summary>DeQueue 的多執行緒鎖定物件
        /// </summary>
        private readonly object _lockDequeue = new();

        /// <summary>「先進先出」的非同步工作佇列
        /// </summary>
        private readonly Queue<QueueAction> _queue = new();

        /// <summary>是否依序等待執行工作
        /// </summary>
        private readonly bool _executeSequentially;

        #endregion 宣告私有的欄位


        #region 宣告公開的屬性

        /// <summary>佇列工作數量
        /// </summary>
        public int Count
        {
            get
            {
                lock (_lockEnqueue)
                {
                    return _queue.Count;
                }
            }
        }

        #endregion 宣告公開的屬性


        #region 宣告建構子

        /// <summary>多載建構子<para/>
        /// </summary>
        /// <param name="executeSequentially">是否依序等待執行工作</param>
        public TaskQueue(bool executeSequentially)
        {
            _executeSequentially = executeSequentially;
        }

        #endregion 宣告建構子


        #region 宣告公開的方法

        /// <summary>加入工作內容到佇列中
        /// </summary>
        /// <param name="action">工作動作委派</param>
        public void EnQueue(QueueAction action)
        {
            try
            {
                lock (_lockEnqueue)
                {
                    _queue.Enqueue(action);
                }

                if (_executeSequentially)
                {
                    Task.Factory.StartNew(ExecuteSequentially);
                }
                else
                {
                    Task.Factory.StartNew(Execute);
                }
            }
            catch (Exception ex)
            {
                Command.StdoutErr($"{nameof(TaskQueue.EnQueue)} Try enqueue a task occur exception: {ex}");
                throw;
            }
        }

        /// <summary>加入工作內容到佇列中，並且等待工作執行結果
        /// </summary>
        /// <param name="queueAction">工作動作委派</param>
        /// <param name="timeout">等待執行結果毫秒數</param>
        /// <returns>工作執行結果</returns>
        public IResult EnQueueForResult(QueueAction queueAction, int timeout = 500)
        {
            IResult result = Result.Create();

            if (timeout <= 0)
            {
                result.Code = "T200";
                result.Message = $"Argument '{nameof(timeout)}' can not less than or equal to 0.";
                return result;
            }

            try
            {
                lock (_lockEnqueue)
                {
                    queueAction.FinishEvent = new AutoResetEvent(false);
                    _queue.Enqueue(queueAction);
                }

                ExecuteSequentially();

                if (!queueAction.FinishEvent.WaitOne(timeout))
                {
                    queueAction.IsCancel = true;
                    result.Code = "T100";
                    result.Message = "Waiting for task result timeout.";
                    return result;
                }

                lock (_lockEnqueue)
                {
                    result = queueAction.Result;
                }
            }
            catch (Exception ex)
            {
                result.Code = "T500";
                result.Message = $"{nameof(TaskQueue.EnQueueForResult)} Try enqueue or execute a task occur exception: {ex}";
                return result;
            }

            return result;
        }

        #endregion 宣告公開的方法


        #region 宣告私有的方法

        /// <summary>執行非同步作業
        /// </summary>
        /// <param name="state"></param>
        private void Execute(object state)
        {
            Execute();
        }

        /// <summary>執行非同步作業
        /// </summary>
        private void Execute()
        {
            try
            {
                if (_queue.Count <= 0)
                {
                    return;
                }

                var action = _queue.Dequeue();
                Task.Factory.StartNew(() => action.Action(action))?.Wait();
            }
            catch (Exception ex)
            {
                Command.StdoutErr($"{nameof(TaskQueue.Execute)} execute async task occur exception: {ex}");
            }
        }

        /// <summary>依序執行非同步作業 (依序「先進先出」等待上一個非同步工作執行完成之後，才會執行下一個非同步工作)
        /// </summary>
        /// <remarks>依序「先進先出」等待上一個非同步工作執行完成之後，才會執行下一個非同步工作</remarks>
        private void ExecuteSequentially()
        {
            try
            {
                lock (_lockDequeue)
                {
                    if (_queue.Count <= 0)
                    {
                        return;
                    }

                    var action = _queue.Dequeue();
                    Task.Factory.StartNew(() => action.Action(action))?.Wait();
                    action.FinishEvent?.Set();
                }
            }
            catch (Exception ex)
            {
                Command.StdoutErr($"{nameof(TaskQueue.ExecuteSequentially)} execute async task occur exception: {ex}");
            }
        }

        #endregion 宣告私有的方法
    }
}
