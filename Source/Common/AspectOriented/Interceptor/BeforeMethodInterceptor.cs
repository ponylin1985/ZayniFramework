﻿namespace ZayniFramework.Common
{
    /// <summary>方法執行前的攔截器
    /// </summary>
    public abstract class BeforeMethodInterceptor : BaseMethodInterceptor
    {
        /// <summary>攔截器的切面執行方法。(建議呼叫此方法時，請使用 C# 4.0 的具名參數語法)<para/>
        /// 1. 如果在執行攔截動作時，需要執行時期的參數，可以使用Zayni Framework框架的DynamicBroker.CreateDynamicObject()方法建立動態物件，將所有參數傳入。<para/>
        /// ex: dynamic dynamicParameters  = DynamicBroker.CreateDynamicObject();<para/>
        ///     dynamicParameters.Language = language;<para/>
        /// 2. 建議呼叫此方法時，請使用C# 4.0的具名參數語法。<para/>
        /// ex: methodParameters = beforeInterceptor.AspectExecute( aspectParameters: interceptorParameters, parameters: methodParameters );<para/>
        /// </summary>
        /// <param name="aspectParameters">攔截動作的參數集合 (選擇性參數)</param>
        /// <param name="parameters">被攔截的目標方法的參數列</param>
        /// <returns></returns>
        public abstract object[] AspectExecute(dynamic aspectParameters = null, params object[] parameters);
    }
}
