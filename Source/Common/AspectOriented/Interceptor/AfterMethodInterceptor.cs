﻿namespace ZayniFramework.Common
{
    /// <summary>方法執行後的攔截器
    /// </summary>
    public abstract class AfterMethodInterceptor : BaseMethodInterceptor
    {
        /// <summary>攔截器的切面執行方法。<para/>
        /// 1. 如果在執行攔截動作時，需要執行時期的參數，可以使用Zayni Framework框架的DynamicBroker.CreateDynamicObject()方法建立動態物件，將所有參數傳入。<para/>
        /// ex: dynamic dynamicParameters  = DynamicBroker.CreateDynamicObject();<para/>
        ///     dynamicParameters.Language = language;<para/>
        /// </summary>
        /// <param name="result">被攔截的目標方法的回傳值</param>
        /// <param name="interceptorParameters">攔截動作的參數集合 (選擇性參數)</param>
        public abstract object AspectExecute(object result, dynamic interceptorParameters = null);
    }
}
