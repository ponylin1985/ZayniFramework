﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;


namespace ZayniFramework.Common
{
    /// <summary>ZayniFramework 框架的 AOP 切面方法呼叫器
    /// </summary>
    public static class AspectInvoker
    {
        #region 宣告公開的委派

        /// <summary>目標方法執行前的委派
        /// </summary>
        /// <param name="beforeInterceptor">方法執行前的攔截器</param>
        /// <param name="parameters">目標方法參數陣列</param>
        /// <param name="interceptorParameters">攔截器動作的參數集合</param>
        /// <returns>攔截器執行結果</returns>
        public delegate object[] BeforeInvokeHandler(BeforeMethodInterceptor beforeInterceptor, object[] parameters, dynamic interceptorParameters = null);

        /// <summary>呼叫目標方法的委派
        /// </summary>
        /// <param name="actionName">Zayni.Service框架的動作方法名稱</param>
        /// <param name="method">目標方法(Zayni.Service框架的動作方法)</param>
        /// <param name="obj">目標物件(Zayni.Service框架的控制器物件)</param>
        /// <param name="parameters">目標方法參數陣列</param>
        /// <returns>執行目標方法後的結果物件</returns>
        public delegate object InvokeHandler(string actionName, MethodInfo method, object obj, params object[] parameters);

        #endregion 宣告公開的委派


        #region 宣告公開的靜態方法

        /// <summary>攔截並且呼叫目標方法
        /// </summary>
        /// <param name="targetMethodName">目標方法名稱</param>
        /// <param name="targetObj">目標物件</param>
        /// <param name="interceptorParameters">攔截器動作的參數集合</param>
        /// <param name="methodParameters">目標方法需要的參數列</param>
        /// <returns>攔截呼叫目標方法的執行結果</returns>
        public static InterceptResult Invoke(string targetMethodName, object targetObj, dynamic interceptorParameters = null, params object[] methodParameters)
        {
            #region 初始化回傳值

            var result = new InterceptResult()
            {
                Success = false,
                ReturnValue = null
            };

            #endregion 初始化回傳值

            #region 檢查傳入的引數

            if (targetMethodName.IsNullOrEmpty())
            {
                result.Message = "傳入的目標方法名稱為Null值或空字串，引數不合法無法進行攔截呼叫。";
                return result;
            }

            if (targetObj.IsNull())
            {
                result.Message = "傳入的目標物件為Null值，引數不合法無法進行攔截呼叫。";
                return result;
            }

            #endregion 檢查傳入的引數

            #region 攔截並且呼叫

            MethodInfo targetMethod = null;

            try
            {
                targetMethod = targetObj.GetType().GetMethod(targetMethodName);
            }
            catch (Exception ex)
            {
                result.Message = $"攔截呼叫目標方法發生異常: {ex}";
                return result;
            }

            result = Invoke(targetMethod, targetObj, interceptorParameters, methodParameters);

            #endregion 攔截並且呼叫

            return result;
        }

        /// <summary>攔截並且呼叫目標方法
        /// </summary>
        /// <param name="targetMethod">目標方法</param>
        /// <param name="targetObj">目標物件</param>
        /// <param name="interceptorParameters">攔截器動作的參數集合</param>
        /// <param name="methodParameters">目標方法需要的參數列</param>
        /// <returns>攔截呼叫目標方法的執行結果</returns>
        public static InterceptResult Invoke(MethodInfo targetMethod, object targetObj, dynamic interceptorParameters = null, params object[] methodParameters)
        {
            #region 初始化回傳值

            var result = new InterceptResult()
            {
                Success = false,
                ReturnValue = null
            };

            #endregion 初始化回傳值

            #region 檢查傳入的引數

            if (new object[] { targetMethod, targetObj }.Any(y => y.IsNull()))
            {
                result.Message = "傳入的目標方法或目標物件為Null值，引數不合法無法進行攔截呼叫。";
                return result;
            }

            #endregion 檢查傳入的引數

            #region 取得方法執行前的攔截器，並且執行攔截器的切面動作

            IEnumerable<BeforeMethodInterceptor> beforeInterceptors = null;

            try
            {
                beforeInterceptors = targetMethod.GetCustomAttributes<BeforeMethodInterceptor>().OrderBy(m => m.Order);
            }
            catch (Exception ex)
            {
                result.Message = $"取得方法執行前的攔截器發生程式異常，無法進行攔截呼叫: {ex}";
                return result;
            }

            if (beforeInterceptors.IsNotNull())
            {
                foreach (var beforeInterceptor in beforeInterceptors)
                {
                    try
                    {
                        methodParameters = beforeInterceptor.AspectExecute(aspectParameters: interceptorParameters, parameters: methodParameters);
                    }
                    catch (Exception ex)
                    {
                        result.Message = $"執行目標方法前攔截器動作發生程式異常，無法進行攔截呼叫: {ex}";
                        return result;
                    }
                }
            }

            #endregion 取得方法執行前的攔截器，並且執行攔截器的切面動作

            #region 執行被攔截的目標方法

            object resultObj = null;

            try
            {
                resultObj = targetMethod.Invoke(targetObj, methodParameters);
            }
            catch (Exception ex)
            {
                result.Message = $"執行目標方法發生程式異常，無法進行攔截呼叫: {ex}";
                return result;
            }

            #endregion 執行被攔截的目標方法

            #region 取得方法執行後的攔截器，並且執行攔截器的切面動作

            IEnumerable<AfterMethodInterceptor> afterInterceptors = null;

            try
            {
                afterInterceptors = targetMethod.GetCustomAttributes<AfterMethodInterceptor>().OrderBy(q => q.Order);
            }
            catch (Exception ex)
            {
                result.Message = $"取得方法執行後的攔截器發生程式異常，無法進行攔截呼叫: {ex}";
                return result;
            }

            if (afterInterceptors.IsNotNull())
            {
                foreach (var afterInterceptor in afterInterceptors)
                {
                    try
                    {
                        resultObj = afterInterceptor.AspectExecute(resultObj, interceptorParameters);
                    }
                    catch (Exception ex)
                    {
                        result.Message = $"執行目標方法後攔截器動作發生程式異常，無法進行攔截呼叫: {ex}";
                        return result;
                    }
                }
            }

            #endregion 取得方法執行後的攔截器，並且執行攔截器的切面動作

            #region 準備執行成功的回傳值

            result.ReturnValue = resultObj;
            result.Success = true;

            #endregion 準備執行成功的回傳值

            return result;
        }

        /// <summary>攔截並且呼叫Zayni.Service框架的目標動作方法 (請注意，此方法只給Zayni.Service框架ActionInvoker呼叫才會有正確的效果!)
        /// </summary>
        /// <param name="actionName">Zayni.Service框架的動作方法名稱</param>
        /// <param name="targetMethod">目標方法 (服務端的動作方法)</param>
        /// <param name="targetObj">目標物件 (服務端的制器物件)</param>
        /// <param name="beforeHandler">動作方法執行前的委派</param>
        /// <param name="invokerHandler">呼叫動作方法的委派</param>
        /// <param name="interceptorParameters">攔截器動作的參數集合</param>
        /// <param name="methodParameters">目標方法參數陣列</param>
        /// <returns>攔截呼叫目標方法的執行結果</returns>
        public static InterceptResult ServiceInvoke(string actionName, MethodInfo targetMethod, object targetObj, BeforeInvokeHandler beforeHandler, InvokeHandler invokerHandler,
            dynamic interceptorParameters, params object[] methodParameters)
        {
            #region 初始化回傳值

            var result = new InterceptResult()
            {
                Success = false,
                ReturnValue = null
            };

            #endregion 初始化回傳值

            #region 檢查傳入的引數

            if (new object[] { targetMethod, targetObj }.Any(y => y.IsNull()))
            {
                result.Message = "傳入的目標方法或目標物件為Null值，引數不合法無法進行攔截呼叫。";
                return result;
            }

            #endregion 檢查傳入的引數

            #region 取得方法執行前的攔截器，並且執行攔截器的切面動作

            IEnumerable<BeforeMethodInterceptor> beforeInterceptors = null;

            try
            {
                beforeInterceptors = targetMethod.GetCustomAttributes<BeforeMethodInterceptor>().OrderBy(m => m.Order);
            }
            catch (Exception ex)
            {
                result.Message = $"取得方法執行前的攔截器發生程式異常，無法進行攔截呼叫: {ex}";
                return result;
            }

            if (beforeInterceptors.IsNotNull())
            {
                foreach (var beforeInterceptor in beforeInterceptors)
                {
                    try
                    {
                        methodParameters = beforeHandler(beforeInterceptor, methodParameters, interceptorParameters);
                    }
                    catch (Exception ex)
                    {
                        result.Message = $"執行目標方法前攔截器動作發生程式異常，無法進行攔截呼叫: {ex}";
                        return result;
                    }
                }
            }

            #endregion 取得方法執行前的攔截器，並且執行攔截器的切面動作

            #region 執行被攔截的目標方法

            object resultObj = null;

            try
            {
                resultObj = invokerHandler.IsNotNull() ? invokerHandler(actionName, targetMethod, targetObj, methodParameters) : targetMethod.Invoke(targetObj, methodParameters);
            }
            catch (Exception ex)
            {
                result.Message = $"執行目標方法發生程式異常，無法進行攔截呼叫: {ex}";
                return result;
            }

            #endregion 執行被攔截的目標方法

            #region 取得方法執行後的攔截器，並且執行攔截器的切面動作

            IEnumerable<AfterMethodInterceptor> afterInterceptors = null;

            try
            {
                afterInterceptors = targetMethod.GetCustomAttributes<AfterMethodInterceptor>().OrderBy(g => g.Order);
            }
            catch (Exception ex)
            {
                result.Message = $"取得方法執行後的攔截器發生程式異常，無法進行攔截呼叫: {ex}";
                return result;
            }

            if (afterInterceptors.IsNotNull())
            {
                foreach (var afterInterceptor in afterInterceptors)
                {
                    try
                    {
                        resultObj = afterInterceptor.AspectExecute(resultObj, interceptorParameters);
                    }
                    catch (Exception ex)
                    {
                        result.Message = $"執行目標方法後攔截器動作發生程式異常，無法進行攔截呼叫: {ex}";
                        return result;
                    }
                }
            }

            #endregion 取得方法執行後的攔截器，並且執行攔截器的切面動作

            #region 準備執行成功的回傳值

            result.ReturnValue = resultObj;
            result.Success = true;

            #endregion 準備執行成功的回傳值

            return result;
        }

        #endregion 宣告公開的靜態方法
    }
}
