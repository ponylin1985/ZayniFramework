﻿namespace ZayniFramework.Common
{
    /// <summary>AOP 切面攔截執行結果
    /// </summary>
    public class InterceptResult : BaseResult
    {
        /// <summary>被攔截的方法回傳值
        /// </summary>
        public object ReturnValue { get; set; }
    }
}
