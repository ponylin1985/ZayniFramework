using Microsoft.Extensions.DependencyInjection;
using System;
using System.Net.Http;


namespace ZayniFramework.Common
{
    /// <summary>HttpClient 客戶端物件工廠<para/>
    /// * 自封式的 ASP.NET Core IHttpClientFactory 工廠。<para/>
    /// * 支援 .NET IHttpClientFactory 的 keep-alive 連線控制，對於相同 domain name 的請求，可以重複使用 http connection，以達到效能優化，以及 http connection 也交由 .NET 底層的 DI container 進行控管。<para/>
    /// * 對於 http service 的 DNS domain name resolve 問題，也交由 IHttpClientClientFactory 與 .NET 底層的 Http DI container 控管處理。
    /// </summary>
    public class HttpClientFactory
    {
        #region Private Static Fields

        /// <summary>服務集合
        /// </summary>
        private static readonly IServiceCollection _serviceCollection;

        /// <summary>服務定位器供應者，自封式受控管的 HttpClient 服務定位容器
        /// </summary>
        private static readonly IServiceProvider _serviceProvider;

        #endregion Private Static Fields


        #region Static Constructor

        /// <summary>靜態建構子，初始化 HttpClient 的服務定位器容器
        /// </summary>
        static HttpClientFactory()
        {
            _serviceCollection = new ServiceCollection();
            _serviceCollection.AddHttpClient<HttpClientFactory>();
            _serviceProvider = _serviceCollection.BuildServiceProvider();
        }

        #endregion Static Constructor


        #region Private Fields

        /// <summary>HttpClient 客戶端物件
        /// </summary>
        private readonly HttpClient _httpClient;

        #endregion Private Fields


        #region Constructor

        /// <summary>多載建構子
        /// </summary>
        /// <param name="httpClient">HttpClient 客戶端物件</param>
        public HttpClientFactory(HttpClient httpClient) =>
            _httpClient = httpClient;

        #endregion Constructor


        #region Public Static Methods

        /// <summary>建立 HttpClient 客戶端物件<para/>
        /// * 假若傳入的 baseUrl 參數結尾不是斜線結尾，會自動強制補上斜線。<para/>
        /// * baseUrl 參數可以接受「是斜線結尾」，也可以接受「非斜線結尾」，重點是搭配 HttpRequest.Path 參數時，http 請求的完整 URL 路徑正確即可。
        /// </summary>
        /// <param name="baseUrl">遠端 http service 服務的 Base URL 位址字串。<para/>
        /// * 假若結尾不是斜線結尾，會自動強制補上斜線。<para/>
        /// * 可以接受「是斜線結尾」，也可以接受「非斜線結尾」，重點是搭配 HttpRequest.Path 參數時，http 請求的完整 URL 路徑正確即可。
        /// </param>
        /// <param name="httpResponseTimeoutSeconds">等待 http response 請求的逾時秒數，預設為 5 秒</param>
        /// <returns>HttpClient 客戶端物件</returns>
        public static HttpClient Create(string baseUrl, int httpResponseTimeoutSeconds = 5)
        {
            var httpClientFactory = _serviceProvider.GetService<HttpClientFactory>();
            return httpClientFactory.GetHttpClient(baseUrl, httpResponseTimeoutSeconds);
        }

        #endregion Public Static Methods


        #region Private Methods

        /// <summary>取得從 ServiceProvider 服務定位容器中的 HttpClient 客戶端物件<para/>
        /// * 假若傳入的 baseUrl 參數結尾不是斜線結尾，會自動強制補上斜線。<para/>
        /// * baseUrl 參數可以接受「是斜線結尾」，也可以接受「非斜線結尾」，重點是搭配 HttpRequest.Path 參數時，http 請求的完整 URL 路徑正確即可。
        /// </summary>
        /// <param name="baseUrl">遠端 http service 服務的 Base URL 位址字串。<para/>
        /// * 假若結尾不是斜線結尾，會自動強制補上斜線。<para/>
        /// * 可以接受「是斜線結尾」，也可以接受「非斜線結尾」，重點是搭配 HttpRequest.Path 參數時，http 請求的完整 URL 路徑正確即可。
        /// </param>
        /// <param name="httpResponseTimeoutSeconds">等待 http response 請求的逾時秒數，預設為 5 秒</param>
        /// <returns>HttpClient 客戶端物件</returns>
        private HttpClient GetHttpClient(string baseUrl, int httpResponseTimeoutSeconds = 5)
        {
            baseUrl.When(s => !s.EndsWith('/'), _ => baseUrl = $"{baseUrl}/");
            _httpClient.BaseAddress = new Uri(baseUrl);
            _httpClient.Timeout = TimeSpan.FromSeconds(httpResponseTimeoutSeconds);
            return _httpClient;
        }

        #endregion Private Methods
    }
}