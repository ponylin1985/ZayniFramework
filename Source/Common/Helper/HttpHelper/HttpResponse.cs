using Newtonsoft.Json;
using System.Net;
using System.Net.Http;
using System.Text.Json.Serialization;


namespace ZayniFramework.Common
{
    /// <summary>Http 回應資訊封裝
    /// </summary>
    public class HttpResponse : Result<string>
    {
        /// <summary>Http 請求識別編號
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "req_id")]
        [JsonPropertyName("req_id")]
        public string RequestId { get; internal set; }

        /// <summary>Http 回應的狀態碼
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "status_code")]
        [JsonPropertyName("status_code")]
        public HttpStatusCode StatusCode { get; set; }

        /// <summary>Http 回應資訊
        /// </summary>
        /// <value></value>
        [Newtonsoft.Json.JsonIgnore()]
        [System.Text.Json.Serialization.JsonIgnore()]
        public HttpResponseMessage HttpResponseMessaage { get; set; }
    }

    /// <summary>Http 回應資訊封裝
    /// </summary>
    /// <typeparam name="TResponseDTO">回應資料載體泛型</typeparam>
    public class HttpResponse<TResponseDTO> : Result<string>
        where TResponseDTO : class
    {
        /// <summary>Http 請求識別編號
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "req_id")]
        [JsonPropertyName("req_id")]
        public string RequestId { get; internal set; }

        /// <summary>Http 回應的狀態碼
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "status_code")]
        [JsonPropertyName("status_code")]
        public HttpStatusCode StatusCode { get; set; }

        /// <summary>Http 回應資料載體
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "payload")]
        [JsonPropertyName("payload")]
        public TResponseDTO Payload { get; set; }

        /// <summary>Http 回應資訊
        /// </summary>
        /// <value></value>
        [Newtonsoft.Json.JsonIgnore()]
        [System.Text.Json.Serialization.JsonIgnore()]
        public HttpResponseMessage HttpResponseMessaage { get; set; }
    }
}