using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text.Json.Serialization;


namespace ZayniFramework.Common
{
    /// <summary>Http 請求資訊封裝
    /// </summary>
    public class HttpRequest
    {
        /// <summary>Http 請求識別編號
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "req_id")]
        [JsonPropertyName("req_id")]
        public string RequestId { get; set; }

        /// <summary>Http 請求路徑
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "path")]
        [JsonPropertyName("path")]
        public string Path { get; set; }

        /// <summary>查詢字串集合
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "querys")]
        [JsonPropertyName("querys")]
        public List<KeyValuePair<string, string>> QueryStrings { get; set; } = [];

        /// <summary>Http 請求的 Authorization Header 的 Key 值
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "auth_header_key")]
        [JsonPropertyName("auth_header_key")]
        public string AuthorizationHeaderKey { get; set; }

        /// <summary>Http 請求的 Authorization Header 的 Value 值
        /// </summary>
        /// <value></value>
        [Newtonsoft.Json.JsonIgnore()]
        [System.Text.Json.Serialization.JsonIgnore()]
        public string AuthorizationHeaderValue { get; set; }

        /// <summary>Http 請求的標頭集合
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "headers")]
        [JsonPropertyName("headers")]
        public Dictionary<string, string> Headers { get; set; } = [];

        /// <summary>Http 請求的內文字串
        /// </summary>
        /// <value></value>
        [JsonProperty(PropertyName = "body")]
        [JsonPropertyName("body")]
        public string Body { get; set; }
    }
}