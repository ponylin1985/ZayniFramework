﻿using System.Text;
using System.Xml;


namespace ZayniFramework.Common.Xml
{
    /// <summary>XML文件格式美化器
    /// </summary>
    public static class XmlBeautifier
    {
        /// <summary>將傳入的XML字串格式美化
        /// </summary>
        /// <param name="xml">來元的XML字串</param>
        /// <returns>Beautiful化過後的XML字串</returns>
        public static string BeautifyXml(string xml)
        {
            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xml);
            return BeautifyXml(xmlDoc);
        }

        // 參考: http://stackoverflow.com/questions/203528/what-is-the-simplest-way-to-get-indented-xml-with-line-breaks-from-xmldocument
        /// <summary>將傳入的XML文件內容美化格式化
        /// </summary>
        /// <remark>
        /// 參考: http://stackoverflow.com/questions/203528/what-is-the-simplest-way-to-get-indented-xml-with-line-breaks-from-xmldocument
        /// </remark>
        /// <param name="xmlDoc">來源XML文件物件</param>
        /// <returns>Beautiful化過後的XML字串</returns>
        public static string BeautifyXml(XmlDocument xmlDoc)
        {
            var sb = new StringBuilder();
            var settings = new XmlWriterSettings
            {
                Indent = true,
                IndentChars = "  ",
                NewLineChars = "\r\n",
                Encoding = Encoding.UTF8,
                NewLineHandling = NewLineHandling.Replace
            };

            using (var writer = XmlWriter.Create(sb, settings))
            {
                xmlDoc.Save(writer);
            }

            return sb.ToString();
        }
    }
}
