﻿using System.Xml;


namespace ZayniFramework.Common.Xml
{
    /// <summary>XML文件格是美化擴充類別
    /// </summary>
    public static class XmlBeautifierExtension
    {
        /// <summary>將傳入的XML字串格式美化
        /// </summary>
        /// <param name="xml">來元的XML字串</param>
        /// <returns>Beautiful化過後的XML字串</returns>
        public static string BeautifyXml(this string xml) => XmlBeautifier.BeautifyXml(xml);

        /// <summary>將傳入的XML文件內容美化格式化
        /// </summary>
        /// <param name="xmlDoc">來源XML文件物件</param>
        /// <returns>Beautiful化過後的XML字串</returns>
        public static string BeautifyXml(this XmlDocument xmlDoc) => XmlBeautifier.BeautifyXml(xmlDoc);
    }
}
