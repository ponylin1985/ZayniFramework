﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;


namespace ZayniFramework.Common
{
    /// <summary>檔案處理公用 Helper 類別
    /// </summary>
    public static class FileHelper
    {
        /// <summary>檢查目標檔案是否已經被 IO 串流鎖定 (傳入的檔案完整名稱不可以為 Null 或空字串，否則即可能會有誤判的情況!)
        /// </summary>
        /// <remarks>傳入的檔案完整名稱不可以為 Null 或空字串，否則即可能會有誤判的情況!</remarks>
        /// <returns>目標檔案是否已經被 IO 串流鎖定</returns>
        public static bool IsFileLocked(string filePath)
        {
            if (filePath.IsNullOrEmpty())
            {
                throw new ArgumentException($"Argument '{nameof(filePath)}' can not be null or empty string.");
            }

            try
            {
                using (File.Open(filePath, FileMode.Open)) { }
            }
            catch (UnauthorizedAccessException)
            {
                return true;
            }
            catch (IOException)
            {
                return true;
            }
            catch (Exception)
            {
                return true;
            }

            return false;
        }

        // 20190605 Bugfix by Pony:
        // 1. 要注意，在 .NET Core 的執行下，不可以再用 Process.GetCurrentProcess() 的方式取得目前應用程式的「執行目錄」! 至少測試下來發現在 linux 上是有問題的。
        // 2. 另外，應該盡量避免使用 Path.Combine() 方法，這問題很多! 如果是 .NET Core project 建議直接使用 Path.Join() 方法替代。
        // 3. 因為這邊是 .NET Standard 2.0 的專案，無法直接使用 Path.Join() 方法，在進行目錄串接時，要格外使用 Path.DirectorySeparatorChar 字元處理不同作業系統的目錄分隔符號。
        /// <summary>取得來源相對路徑在應用程式 runtime 執行目錄下的完整目錄路徑。<para/>
        /// * relativePath 參數: 建議採用 unix-like 作業系統的路徑，譬如: ./SomeDir/Configs
        /// * prefix 參數: 預設值為 ./
        /// </summary>
        /// <param name="relativePath">原始相對路徑<para/>
        /// * 建議採用 unix-like 作業系統的路徑，譬如: ./SomeDir/Configs
        /// </param>
        /// <param name="prefix">相對路徑前綴字<para/>
        /// * 預設值為 ./
        /// </param>
        /// <returns>完整目錄路徑</returns>
        public static string GetFullPath(string relativePath, string prefix = "./")
        {
            var separator = Path.DirectorySeparatorChar.ToString();
            var runtimeAssemblyPath = Assembly.GetEntryAssembly().Location;
            var runtimeDirPath = Path.GetDirectoryName(runtimeAssemblyPath);
            runtimeDirPath = TrimLastSlash(runtimeDirPath);

            if (relativePath.IsNullOrEmpty())
            {
                return runtimeDirPath;
            }

            if (prefix.IsNotNullOrEmpty())
            {
                relativePath = relativePath.RemoveFirstAppeared(prefix);
            }

            if (relativePath.StartsWith(separator))
            {
                relativePath = relativePath.RemoveFirstAppeared(separator);
            }

            var fullPath = $"{runtimeDirPath}{separator}{relativePath}";
            return fullPath;
        }

        /// <summary>根據指定的目錄資訊搜尋指定的檔案
        /// </summary>
        /// <param name="fileName">目標檔案名稱</param>
        /// <param name="root">要開始尋找的根目錄</param>
        /// <returns>符合條件的 FileInfo[] 陣列</returns>
        public static FileInfo[] SearchFile(string fileName, DirectoryInfo root)
        {
            var info = root.GetFiles(fileName);

            if (info.Length > 0)
            {
                return info;
            }

            var files = new List<FileInfo>();
            var dirs = root.GetDirectories();

            foreach (var d in dirs)
            {
                // 遞迴去搜尋
                var fInfo = SearchFile(fileName, d);

                if (null != fInfo && fInfo.Length > 0)
                {
                    files.AddRange(fInfo);
                }
            }

            var result = files.ToArray();
            return result;
        }

        /// <summary>根據指定的目錄資訊搜尋指定的檔案
        /// </summary>
        /// <param name="targetFileName">目標檔案名稱 (完整路徑名稱)</param>
        /// <param name="rootPath">要開始尋找的根目錄</param>
        /// <returns>檔案搜尋結果，資料為: 符合條件的FileInfo[]陣列</returns>
        public static IResult<FileInfo[]> SearchFile(string targetFileName, string rootPath)
        {
            var result = Result.Create<FileInfo[]>();

            if (new string[] { targetFileName, rootPath }.HasNullOrEmptyElements())
            {
                result.Message = "傳入的參數為包含Null或空字串，無法進行檔案搜尋。";
                return result;
            }

            FileInfo[] files = null;

            try
            {
                var rootDir = new DirectoryInfo(rootPath);
                files = SearchFile(targetFileName, rootDir);
            }
            catch (Exception ex)
            {
                result.HasException = true;
                result.ExceptionObject = ex;
                result.Message = $"根據指定的目錄資訊搜尋指定的檔案發稱程式異常: {ex}";
                return result;
            }

            if (files.IsNullOrEmpty())
            {
                result.Message = "檔案搜尋失敗: 搜尋不指定的檔案。";
                return result;
            }

            result.Data = files;
            result.Success = true;
            return result;
        }

        /// <summary>調整目標路徑，將路徑最後的「目錄分隔字元」移除掉。
        /// * 如果路徑結尾不包含作業系統的「目錄分隔字元」，則不會進行移除。
        /// * 當路徑結尾確實包含作業系統的「目錄分隔字元」時，則進行移除。
        /// </summary>
        /// <param name="path">目標檔案路徑</param>
        /// <returns>目標路徑，將路徑最後的「目錄分隔字元」移除掉。</returns>
        public static string TrimLastSlash(string path)
        {
            var separator = Path.DirectorySeparatorChar;
            var contains = path[path.Length - 1] == separator;
            var result = contains ? path.RemoveLastAppeared(separator.ToString()) : path;
            return result;
        }
    }
}
