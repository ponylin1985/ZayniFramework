﻿using System;
using System.Text;


namespace ZayniFramework.Common
{
    /// <summary>亂數字串產生器
    /// </summary>
    public static class RandomTextHelper
    {
        #region Private Fileds

        /// <summary>產生字串用的亂數器
        /// </summary>
        /// <returns></returns>
        private static readonly Random _randomText = new(Guid.NewGuid().GetHashCode());

        /// <summary>產生數字字串用的亂數器
        /// </summary>
        /// <returns></returns>
        private static readonly Random _randomInt64 = new(Guid.NewGuid().GetHashCode());

        #endregion Private Fileds


        #region Public Methods

        /// <summary>產生指定長度的字串亂數
        /// </summary>
        /// <param name="length">字串亂數的長度</param>
        /// <param name="withSpecialChars">是否包含特殊字元，譬如: !, @, #, $, %, ^, *, ?,` 等字元，預設不包含!</param>
        /// <returns>字串亂數</returns>
        public static string Create(int length, bool withSpecialChars = false)
        {
            var charString = "0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z";
            var next = 61;

            if (withSpecialChars)
            {
                charString += ",!,@,#,$,%,^,&,*,?,`";
                next = 71;
            }

            var chars = charString.Split(',');
            var result = new StringBuilder("");
            var temp = -1;

            var random = _randomText;

            for (var i = 1; i < length + 1; i++)
            {
                if (-1 == temp)
                {
                    // random = new Random( Guid.NewGuid().GetHashCode() );
                    random = _randomText;
                }

                var t = random.Next(next);

                if (-1 == temp && t == temp)
                {
                    return Create(length);      // 這邊必須要遞迴
                }

                temp = t;
                result.Append(chars[t]);
            }

            return result.ToString();
        }

        /// <summary>產生指定長度的字串亂數
        /// </summary>
        /// <param name="length">字串亂數的長度</param>
        /// <param name="withSpecialChars">是否包含特殊字元，譬如: !, @, #, $, %, ^, *, ?,` 等字元，預設不包含!</param>
        /// <returns>產生字串亂數結果</returns>
        public static IResult<string> CreateRandom(int length, bool withSpecialChars = false)
        {
            var result = Result.Create<string>();

            if (length <= 0)
            {
                result.Message = $"Invalid argument {nameof(length)}. The value must be greater than 0 integer.";
                return result;
            }

            try
            {
                result.Data = Create(length, withSpecialChars);
            }
            catch (Exception ex)
            {
                result.HasException = true;
                result.ExceptionObject = ex;
                result.Message = $"Create random text occur exception. {ex}";
                return result;
            }

            result.Success = true;
            return result;
        }

        /// <summary>產生指定長度的「數字」亂數字串
        /// </summary>
        /// <param name="length">亂數長度</param>
        /// <returns>「數字」亂數字串</returns>
        public static string CreateInt64String(int length)
        {
            if (length <= 0)
            {
                return string.Empty;
            }

            var text = new StringBuilder();

            for (var i = 0; i < length; i++)
            {
                _ = text.Append(_randomInt64.Next(0, 9));
            }

            return text.ToString();
        }

        #endregion Public Methods
    }
}
