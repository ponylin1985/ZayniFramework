﻿using System;
using System.Threading.Tasks;


namespace ZayniFramework.Common
{
    /// <summary>真假值條件檢查類別
    /// </summary>
    public static class When
    {
        /// <summary>當為真值時，自動執行指定的回呼委派。呼叫範例:<para/>
        /// int  j = 9;<para/>
        /// bool b = 9 == j;<para/>
        /// When.True( b, () => Console.WriteLine( "This is TRUE." ) );<para/>
        /// </summary>
        /// <example>
        /// <code>
        /// int  j = 9;
        /// bool b = 9 == j;
        /// When.True( b, () => Console.WriteLine( "This is TRUE." ) );
        /// </code>
        /// </example>
        /// <param name="isTrue">布林真假值</param>
        /// <param name="action">為真時執行的回呼</param>
        public static void True(bool isTrue, Action action) => True(() => isTrue, action);

        /// <summary>當為真值時，自動執行指定的非同步回呼委派，但非同步委派需要自行處理 try/catch exception handling。<para/>
        /// * 當條件成立執行非同步 asyncFunc 委派時，屬於 async 的 fire and forget 呼叫處理。
        /// </summary>
        /// <param name="isTrue">布林真假值</param>
        /// <param name="asyncFunc">條件成立時得非同步作業回呼，採用 fire and forget 的非同步方式執行。</param>
        public static async void True(bool isTrue, Func<Task> asyncFunc) => await True(() => isTrue, asyncFunc);

        /// <summary>當條件為真時，自動執行指定的回呼委派。呼叫範例:<para/>
        /// int j = 9;<para/>
        /// When.True( () => 9 == j, () => Console.WriteLine( $"I am {j}." ) );<para/>
        /// </summary>
        /// <example>
        /// <code>
        /// int j = 9;
        /// When.True( () => 9 == j, () => Console.WriteLine( $"I am {j}." ) );
        /// </code>
        /// </example>
        /// <param name="condition">真假值條件</param>
        /// <param name="action">條件為真時執行的回呼</param>
        public static void True(Func<bool> condition, Action action)
        {
            if (condition())
            {
                action();
            }
        }

        /// <summary>當條件為真時，自動執行指定的非同步回呼委派。<para/>
        /// </summary>
        /// <param name="condition">真假值條件</param>
        /// <param name="asyncFunc">條件成立時執行的非同步回呼</param>
        /// <returns>非同步作業</returns>
        public static async Task True(Func<bool> condition, Func<Task> asyncFunc)
        {
            if (condition())
            {
                try
                {
                    await asyncFunc();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>當為假值時，自動執行指定的回呼委派。呼叫範例:<para/>
        /// int  j = 9;<para/>
        /// bool b = 123 = j;<para/>
        /// When.False( b, () => Console.WriteLine( "j is 9." ) );<para/>
        /// </summary>
        /// <example>
        /// <code>
        /// int  j = 9;
        /// bool b = 123 = j;
        /// When.False( b, () => Console.WriteLine( "j is 9." ) );
        /// </code>
        /// </example>
        /// <param name="isFalse">布林真假值</param>
        /// <param name="action">為假時執行的回呼</param>
        public static void False(bool isFalse, Action action) => False(() => isFalse, action);

        /// <summary>當為假值時，自動執行指定的非同步回呼委派，但非同步委派需要自行處理 try/catch exception handling。<para/>
        /// * 當條件不成立執行非同步 asyncFunc 委派時，屬於 async 的 fire and forget 呼叫處理。
        /// </summary>
        /// <param name="isFalse">布林真假值</param>
        /// <param name="asyncFunc">條件不成立執行的非同步回呼</param>
        public static async void False(bool isFalse, Func<Task> asyncFunc) => await False(() => isFalse, asyncFunc);

        /// <summary>當條件為假時，自動執行指定的回呼委派。呼叫範例:<para/>
        /// int j = 9;<para/>
        /// When.False( () => 123 == j, () => {<para/>
        ///     j = 9;<para/>
        ///     Console.WriteLine( $"Hey hey, I am still {j}." );<para/>
        /// } );<para/>
        /// </summary>
        /// <example>
        /// <code>
        /// int j = 9;
        /// When.False( () => 123 == j, () => {
        ///     j = 9;
        ///     Console.WriteLine( $"Hey hey, I am still {j}." );
        /// } );
        /// </code>
        /// </example>
        /// <param name="condition">真假值條件</param>
        /// <param name="action">條件為假時執行的回呼</param>
        public static void False(Func<bool> condition, Action action)
        {
            if (!condition())
            {
                action();
            }
        }

        /// <summary>當條件為假時，自動執行指定的回呼委派。<para/>
        /// </summary>
        /// <param name="condition">真假值條件</param>
        /// <param name="asyncFunc">條件不成立時執行的非同步回呼</param>
        /// <returns>非同步作業</returns>
        public static async Task False(Func<bool> condition, Func<Task> asyncFunc)
        {
            if (!condition())
            {
                try
                {
                    await asyncFunc();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
    }

    /// <summary>This class is obsoleted. Please use the When static class for instead.
    /// </summary>
    [Obsolete("This class is obsoleted. Please use the When static class for instead.", true)]
    public static class Check { }
}
