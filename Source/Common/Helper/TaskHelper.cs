using System;
using System.Threading;
using System.Threading.Tasks;


namespace ZayniFramework.Common
{
    /// <summary>Task 非同步任務的 Helper 類別。<para/>
    /// * 可以支援將 non-async 的 blocking method 包裹成 async method 並且執行，但實際上仍會是以 sync method 的同步作業執行，只是允許外界的 async method 採用 await 方式呼叫。<para/>
    /// * 可以支援將 async method 強制以 async/await 的非同步作業執行。<para/>
    /// * Reference:<para/>
    ///   * https://stackoverflow.com/questions/22645024/when-would-i-use-task-yield<para/>
    ///   * https://cloud.tencent.com/developer/article/1621725<para/>
    /// </summary>
    public static class TaskHelper
    {
        /// <summary>將沒有回傳值的 non-async method 方法包裹成 async method 執行。
        /// </summary>
        /// <param name="action">沒有回傳值的 non-async 動作委派</param>
        /// <returns>Task 執行任務</returns>
        public static async Task RunAsync(Action action)
        {
            await Task.Yield();
            action();
        }

        /// <summary>將有回傳值的 non-async method 方法包裹成 async method 執行。
        /// </summary>
        /// <param name="func">有回傳值的 non-async 動作委派</param>
        /// <typeparam name="TResult">委派的回傳值泛型</typeparam>
        /// <returns>Task 執行任務</returns>
        public static async Task<TResult> RunAsync<TResult>(Func<TResult> func)
        {
            await Task.Yield();
            return func();
        }

        /// <summary>將沒有回傳值的 async method 強制以 async 非同步的方式執行。
        /// </summary>
        /// <param name="func">沒有回傳值的 async 動作委派</param>
        /// <returns>Task 執行任務</returns>
        public static async Task ForceAsync(Func<Task> func)
        {
            await Task.Yield();
            await func();
        }

        /// <summary>將有回傳值的 async method 強制以 async 非同步的方式執行。
        /// </summary>
        /// <param name="func">有回傳值的 async 動作委派</param>
        /// <typeparam name="TResult">委派的回傳值泛型</typeparam>
        /// <returns>Task 執行任務</returns>
        public static async Task<TResult> ForceAsync<TResult>(Func<Task<TResult>> func)
        {
            await Task.Yield();
            return await func();
        }

        /// <summary>延遲非同步作業，直到指定的條件成立
        /// </summary>
        /// <param name="predicate">中斷延遲非同步作業的條件委派</param>
        /// <param name="timeoutMilliseconds">最大延遲毫秒數<para/>
        /// 此為選擇性參數，假若傳入 0 或沒有傳入，則一直延遲到 predicate 條件成立才會中斷延遲。
        /// </param>
        /// <param name="waitMilliseconds">每次延遲多少毫秒數，檢查 preicate 條件，預設為 20 毫秒。</param>
        /// <returns>非同步作業</returns>
        public static async Task DelayUntilAsync(Func<bool> predicate, int? timeoutMilliseconds = null, int waitMilliseconds = 20)
        {
            var count = 0;

            while (true)
            {
                if (timeoutMilliseconds.IsNotNull() && timeoutMilliseconds > 0 && waitMilliseconds * count >= timeoutMilliseconds.Value)
                {
                    break;
                }

                await Task.Delay(waitMilliseconds);

                if (predicate())
                {
                    break;
                }

                count++;
            }
        }

        /// <summary>延遲非同步作業，直到指定的條件成立
        /// </summary>
        /// <param name="predicate">中斷延遲非同步作業的條件委派</param>
        /// <param name="cancellationToken">取消延遲非同步作業的訊號</param>
        /// <param name="timeoutMilliseconds">最大延遲毫秒數<para/>
        /// 此為選擇性參數，假若傳入 0 或沒有傳入，則一直延遲到 predicate 條件成立才會中斷延遲。
        /// </param>
        /// <param name="waitMilliseconds">每次延遲多少毫秒數，檢查 preicate 條件，預設為 20 毫秒。</param>
        /// <returns>非同步作業</returns>
        public static async Task DelayUntilAsync(Func<bool> predicate, CancellationToken cancellationToken, int? timeoutMilliseconds = null, int waitMilliseconds = 20)
        {
            var count = 0;

            while (true)
            {
                if (timeoutMilliseconds.IsNotNull() && timeoutMilliseconds > 0 && waitMilliseconds * count >= timeoutMilliseconds.Value)
                {
                    break;
                }

                await Task.Delay(waitMilliseconds, cancellationToken);

                if (predicate())
                {
                    break;
                }

                count++;
            }
        }
    }
}