﻿namespace ZayniFramework.Common
{
    /// <summary>Salted one-way hash compute service interface.
    /// </summary>
    public interface ISaltedHashComputer
    {
        /// <summary>Perform salted hash on the source plaintext string. (If the hashed ciphertext requires a salt, this parameter must be passed in, and it cannot be an empty string.)
        /// </summary>
        /// <param name="source">Plaintext string.</param>
        /// <param name="saltedKey">The salted key string. (If the hashed ciphertext requires a salt, this parameter must be passed in, and it cannot be an empty string.)</param>
        /// <returns>The hashed ciphertext.</returns>
        string ComputeHash(string source, string saltedKey);
    }
}
