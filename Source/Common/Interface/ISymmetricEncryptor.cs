﻿namespace ZayniFramework.Common
{
    /// <summary>對稱式加解密元件合約
    /// </summary>
    public interface ISymmetricEncryptor
    {
        /// <summary>對目標明文字串進行加密
        /// </summary>
        /// <param name="targetText">目標明文字串</param>
        /// <returns>加密過後的秘文字串</returns>
        string Encrypt(string targetText);

        /// <summary>對目標秘文字串進行解密
        /// </summary>
        /// <param name="cipherText">目標秘文字串</param>
        /// <returns>解密過後的明文字串</returns>
        string Decrypt(string cipherText);

        /// <summary>執行加解密過程中發生例外的處理回呼委派
        /// </summary>
        ExceptionHandler ExceptionCallback { get; set; }
    }
}
