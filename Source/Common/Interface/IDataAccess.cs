﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;


namespace ZayniFramework.Common
{
    /// <summary>資料庫存取基礎介面
    /// </summary>
    public interface IDataAccess
    {
        #region Public Properties

        /// <summary>執行結果訊息、執行結果錯誤訊息
        /// </summary>
        string Message { get; set; }

        /// <summary>執行結果影響資料列數 (唯獨屬性)
        /// </summary>
        int DataCount { get; }

        /// <summary>資料庫異常物件
        /// </summary>
        Exception ExceptionObject { get; }

        /// <summary>資料庫連線字串
        /// </summary>
        /// <value></value>
        string ConnectionString { get; }

        #endregion Public Properties


        #region DbConnection and DbTransaction Methods

        /// <summary>建立並且開啟資料庫連線
        /// </summary>
        /// <param name="dbName">資料庫連線字串設定名稱</param>
        /// <returns>新的資料庫連線</returns>
        Task<DbConnection> CreateConnectionAsync(string dbName = "");

        /// <summary>建立並且開啟資料庫連線
        /// </summary>
        /// <param name="dbName">資料庫連線字串設定名稱</param>
        /// <returns>新的資料庫連線</returns>
        DbConnection CreateConnection(string dbName = "");

        /// <summary>開啟資料庫交易
        /// </summary>
        /// <param name="connection">來源資料庫連線</param>
        /// <param name="isolationLevel">交易鎖定層級</param>
        /// <returns>資料庫交易</returns>
        DbTransaction BeginTransaction(DbConnection connection, IsolationLevel isolationLevel = IsolationLevel.ReadCommitted);

        /// <summary>開啟資料庫交易
        /// </summary>
        /// <param name="connection">來源資料庫連線</param>
        /// <param name="isolationLevel">交易鎖定層級</param>
        /// <returns>資料庫交易</returns>
        Task<DbTransaction> BeginTransactionAsync(DbConnection connection, IsolationLevel isolationLevel = IsolationLevel.ReadCommitted);

        /// <summary>確認目標資料庫交易
        /// </summary>
        /// <param name="transaction">目標資料庫交易</param>
        /// <returns>是否確認交易成功</returns>
        bool Commit(DbTransaction transaction);

        /// <summary>確認目標資料庫交易
        /// </summary>
        /// <param name="transaction">目標資料庫交易</param>
        /// <returns>是否確認交易成功</returns>
        Task<bool> CommitAsync(DbTransaction transaction);

        /// <summary>取消目標資料庫交易
        /// </summary>
        /// <param name="transaction">目標資料庫交易</param>
        /// <returns>是否取消交易成功</returns>
        bool Rollback(DbTransaction transaction);

        /// <summary>取消目標資料庫交易
        /// </summary>
        /// <param name="transaction">目標資料庫交易</param>
        /// <returns>是否取消交易成功</returns>
        Task<bool> RollbackAsync(DbTransaction transaction);

        #endregion DbConnection and DbTransaction Methods


        #region SQL Execute Methods

        /// <summary>執行 DbCommand 指令
        /// </summary>
        /// <param name="command">要執行的命令</param>
        /// <returns>是否執行成功</returns>
        Task<bool> ExecuteNonQueryAsync(DbCommand command);

        /// <summary>執行 DbCommand 指令
        /// </summary>
        /// <param name="command">要執行的命令</param>
        /// <returns>是否執行成功</returns>
        bool ExecuteNonQuery(DbCommand command);

        /// <summary>執行 DbCommand 命令並回傳資料讀取器
        /// </summary>
        /// <param name="command">要執行的命令</param>
        /// <returns>執行查詢SQL資料讀取器是否成功</returns>
        Task<Result<IDataReader>> ExecuteReaderAsync(DbCommand command);

        /// <summary>執行 DbCommand 命令並回傳資料讀取器
        /// </summary>
        /// <param name="command">要執行的命令</param>
        /// <param name="reader">資料讀取器</param>
        /// <returns>執行查詢SQL資料讀取器是否成功</returns>
        bool ExecuteReader(DbCommand command, out IDataReader reader);

        /// <summary>執行 DbCommand 查詢指令，並且輸出單一純量值。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <returns>執行是否成功</returns>
        Task<Result<object>> ExecuteScalarAsync(DbCommand command);

        /// <summary>執行 DbCommand 命令並回傳執行結果
        /// </summary>
        /// <param name="command">要執行的命令</param>
        /// <param name="result">執行結果物件</param>
        /// <returns>執行是否成功</returns>
        bool ExecuteScalar(DbCommand command, out object result);

        /// <summary>執行 DbCommand 查詢指令，回傳執行使否成功。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="ds">資料集合</param>
        /// <param name="tableNames">資料表名稱</param>
        /// <returns>是否執行查詢成功</returns>
        Task<bool> LoadDataSetAsync(DbCommand command, DataSet ds, params string[] tableNames);

        /// <summary>執行 DbCommand 查詢指令，回傳執行使否成功
        /// </summary>
        /// <param name="command">要執行的指令</param>
        /// <param name="ds">資料集合</param>
        /// <param name="tableNames">資料表名稱</param>
        /// <returns>是否執行查詢成功</returns>
        bool LoadDataSet(DbCommand command, DataSet ds, params string[] tableNames);

        /// <summary>執行 DbCommand 查詢指令，輸出查詢結果資料模型集合
        /// </summary>
        /// <typeparam name="TModel">資料模型泛型</typeparam>
        /// <param name="command">資料庫指令</param>
        /// <returns>是否執行查詢成功</returns>
        Task<Result<IEnumerable<TModel>>> LoadDataToModelAsync<TModel>(DbCommand command)
            where TModel : new();

        /// <summary>執行 DbCommand 查詢指令，輸出查詢結果資料模型集合。
        /// </summary>
        /// <typeparam name="TModel">資料模型泛型</typeparam>
        /// <param name="command">資料庫指令</param>
        /// <param name="models">資料模型集合</param>
        /// <returns>是否執行查詢成功</returns>
        bool LoadDataToModel<TModel>(DbCommand command, out IEnumerable<TModel> models)
            where TModel : new();

        /// <summary>執行 DbCommand 查詢指令，輸出動態物件資料集合。
        /// * ORM 資料綁定機制: 單一資料模型 ORM 對應，應用在 SQL 查詢語法中只有一個 SELECT 查詢語法，回傳單一資料表結果集合。
        /// * 對查詢單一結果資料表，進行動態 ORM 資料綁定。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <returns>是否執行查詢成功</returns>
        Task<Result<IEnumerable<dynamic>>> LoadDataToDynamicModelAsync(DbCommand command);

        /// <summary>執行 DbCommand 查詢指令，輸出動態物件資料集合。
        /// * ORM 資料綁定機制: 單一資料模型 ORM 對應，應用在 SQL 查詢語法中只有一個 SELECT 查詢語法，回傳單一資料表結果集合。
        /// * 對查詢單一結果資料表，進行動態 ORM 資料綁定。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="models">動態資料模型串列集合</param>
        /// <returns>是否執行查詢成功</returns>
        bool LoadDataToDynamicModel(DbCommand command, out IEnumerable<dynamic> models);

        /// <summary>執行 DbCommand 查詢指令，依照指定的 Type 陣列回傳多組 ORM 綁定的資料模型集合。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="modelTypes">資料模型型別陣列</param>
        /// <returns>是否執行查詢成功</returns>
        Task<Result<IEnumerable<object[]>>> LoadDataToModelsAsync(DbCommand command, Type[] modelTypes);

        /// <summary>執行 DbCommand 查詢指令，依照指定的 Type 陣列回傳多組 ORM 綁定的資料模型集合。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="modelTypes">資料模型型別陣列</param>
        /// <param name="result">查詢結果資料模型集合</param>
        /// <returns>是否執行查詢成功</returns>
        bool LoadDataToModels(DbCommand command, Type[] modelTypes, out IEnumerable<object[]> result);

        /// <summary>執行 DbCommand 查詢指令，DbCommand 的 SQL 查詢語法可以支援多組 SELECT 查詢，並且輸出多組動態 ORM 資料綁定的查詢結果集合。
        /// </summary>
        /// <param name="command">資料庫指令
        /// * Key 值: 依照順序傳入的 tableName 參數名稱。
        /// * Value 值: 動態物件資料模型串列。
        /// </param>
        /// <param name="tableName">資料表名稱</param>
        /// <returns>是否執行查詢成功</returns>
        Task<Result<Dictionary<string, IEnumerable<dynamic>>>> LoadDataToDynamicCollectionAsync(DbCommand command, params string[] tableName);

        /// <summary>執行 DbCommand 查詢指令，DbCommand 的 SQL 查詢語法可以支援多組 SELECT 查詢，並且輸出多組動態 ORM 資料綁定的查詢結果集合。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="modelCollection">查詢結果的資料集合，動態物件字典集合。<para/>
        /// * Key 值: 依照順序傳入的 tableName 參數名稱。
        /// * Value 值: 動態物件資料模型串列。
        /// </param>
        /// <param name="tableName">資料表名稱</param>
        /// <returns>是否執行查詢成功</returns>
        bool LoadDataToDynamicCollection(DbCommand command, out Dictionary<string, IEnumerable<dynamic>> modelCollection, params string[] tableName);

        /// <summary>執行 DbCommand 查詢指令，輸出自定義的 ModelSet 資料集合中。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="modelTypes">資料模型型別陣列</param>
        /// <param name="modelSet">查詢結果資料集合</param>
        /// <returns>是否執行查詢成功</returns>
        Task<bool> LoadModelSetAsync(DbCommand command, ModelSet modelSet, params Type[] modelTypes);

        /// <summary>執行 DbCommand 查詢指令，輸出自定義的 ModelSet 資料集合中。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="modelTypes">資料模型型別陣列</param>
        /// <param name="modelSet">查詢結果資料集合</param>
        /// <returns>是否執行查詢成功</returns>
        bool LoadModelSet(DbCommand command, ModelSet modelSet, params Type[] modelTypes);

        #endregion SQL Execute Methods


        #region DbCommand Methods

        /// <summary>取得資料庫指令物件
        /// </summary>
        /// <param name="commandText">SQL 敘述字串</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <returns>資料庫指令</returns>
        DbCommand GetSqlStringCommand(string commandText, DbConnection connection = null, DbTransaction transaction = null);

        /// <summary>取得資料庫預存程序的資料庫指令
        /// </summary>
        /// <param name="storedProcName">資料庫預存程序名稱</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <returns>資料庫指令</returns>
        DbCommand GetStoredProcCommand(string storedProcName, DbConnection connection = null, DbTransaction transaction = null);

        /// <summary>新增資料庫指令參數
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="parameterName">資料庫指令的參數名稱</param>
        /// <param name="dbType">參數型態 (可以使用ZayniFramework 框架的 DbColumnType 靜態常數)</param>
        /// <param name="value">參數值</param>
        DbCommand AddInParameter(DbCommand command, string parameterName, int dbType, object value);

        /// <summary>新增資料庫指令的輸出參數
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="parameterName">資料庫指令的參數名稱</param>
        /// <param name="dbType">參數型態 (可以使用ZayniFramework 框架的 DbColumnType 靜態常數)</param>
        /// <param name="maxSize">變數的最大長度</param>
        DbCommand AddOutParameter(DbCommand command, string parameterName, int dbType, int maxSize);

        /// <summary>檢查是否包含指定的資料庫參數
        /// </summary>
        /// <param name="command">來源資料庫命令</param>
        /// <param name="parameterName">資料庫參數名稱</param>
        /// <returns>是否包含目標參數</returns>
        bool ContainsParameter(DbCommand command, string parameterName);

        /// <summary>取得資料庫指令參數值
        /// </summary>
        /// <param name="command">目標資料庫命令</param>
        /// <param name="parameterName">資料庫參數名稱</param>
        /// <returns>資料庫參數值</returns>
        object GetParameterValue(DbCommand command, string parameterName);

        /// <summary>設定資料庫指令的參數值
        /// </summary>
        /// <param name="command">目標資料庫命令</param>
        /// <param name="parameterName">資料庫參數名稱</param>
        /// <param name="value">參數值</param>
        /// <returns>設定參數值是否成功</returns>
        bool SetParameterValue(DbCommand command, string parameterName, object value);

        /// <summary>清除資料庫指令的所有參數
        /// </summary>
        /// <param name="command">目標資料庫命令</param>
        /// <returns>是否清除成功</returns>
        bool ClearAllParameters(DbCommand command);

        /// <summary>取得資料庫的空字串值
        /// </summary>
        /// <returns>資料庫的空字串值</returns>
        string GetDBEmptyString();

        #endregion DbCommand Methods
    }
}
