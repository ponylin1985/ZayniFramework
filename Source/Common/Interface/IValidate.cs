﻿namespace ZayniFramework.Common
{
    /// <summary>驗證機制介面
    /// </summary>
    public interface IValidate
    {
        /// <summary>執行資料驗證
        /// </summary>
        /// <param name="target">受檢的目標資料</param>
        ValidationResult DoValidate(object target);
    }
}
