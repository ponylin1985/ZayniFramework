﻿using System;
using System.Threading.Tasks;


namespace ZayniFramework.Common
{
    /// <summary>重覆執行類別
    /// </summary>
    public static class For
    {
        #region 宣告私有的欄位

        /// <summary>計數
        /// </summary>
        private static int _count = 0;

        #endregion 宣告私有的欄位


        #region 宣告公開的屬性

        /// <summary>目前執行次數
        /// </summary>
        public static int Count => _count;

        #endregion 宣告公開的屬性


        #region 宣告公開的方法

        /// <summary>重置計數
        /// </summary>
        public static void Reset() => _count = 0;

        /// <summary>重覆執行動作
        /// </summary>
        /// <param name="times">執行次數</param>
        /// <param name="callback">動作委派</param>
        public static void Do(int times, Action callback)
        {
            if (_count >= times)
            {
                return;
            }

            callback();
            _count++;

            Do(times, callback);
        }

        /// <summary>重覆執行非同步作業動作
        /// </summary>
        /// <param name="times">執行次數</param>
        /// <param name="asyncFunc">非同步作業動作</param>
        /// <returns>非同步作業任務</returns>
        public static async Task DoAsync(int times, Func<Task> asyncFunc)
        {
            if (_count >= times)
            {
                return;
            }

            await asyncFunc();
            _count++;

            await DoAsync(times, asyncFunc);
        }

        #endregion 宣告公開的方法
    }
}
