﻿using System;
using System.Threading.Tasks;


namespace ZayniFramework.Common
{
    /// <summary>遞迴執行動作
    /// </summary>
    public sealed class Recursive
    {
        #region 宣告建構子

        /// <summary>多載建構子
        /// </summary>
        /// <param name="condition">中止條件委派</param>
        public Recursive(BreakConitionHandler condition)
        {
            BreakConition = condition;
        }

        #endregion 宣告建構子


        #region 宣告公開的屬性

        /// <summary>中止條件委派
        /// </summary>
        public BreakConitionHandler BreakConition { get; set; }

        #endregion 宣告公開的屬性


        #region 宣告公開的方法

        /// <summary>遞迴執行動作
        /// </summary>
        /// <param name="callback">動作委派</param>
        public void Do(Action callback)
        {
            if (BreakConition())
            {
                return;
            }

            try
            {
                callback();
            }
            catch (Exception ex)
            {
                Command.StdoutErr(ex.ToString());
                throw;
            }

            Do(callback);
        }

        /// <summary>遞迴執行動作
        /// </summary>
        /// <param name="asyncFunc">非同步的動作委派</param>
        public async Task DoAsync(Func<Task> asyncFunc)
        {
            if (BreakConition())
            {
                return;
            }

            try
            {
                await asyncFunc();
            }
            catch (Exception ex)
            {
                Command.StdoutErr(ex.ToString());
                throw;
            }

            await DoAsync(asyncFunc);
        }

        #endregion 宣告公開的方法
    }
}
