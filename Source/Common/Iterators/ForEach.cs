﻿using System;
using System.Collections;
using System.Threading.Tasks;


namespace ZayniFramework.Common
{
    /// <summary>對集合的所有元素執行動作
    /// </summary>
    public sealed class ForEach
    {
        #region 宣告私有的欄位

        /// <summary>目標陣列
        /// </summary>
        private readonly Array _array;

        /// <summary>目標串列
        /// </summary>
        private readonly IList _list;

        /// <summary>計數
        /// </summary>
        private int _count = 0;

        #endregion 宣告私有的欄位


        #region 宣告建構子

        /// <summary>多載建構子
        /// </summary>
        /// <param name="array">目標陣列</param>
        public ForEach(Array array)
        {
            _array = array;
        }

        /// <summary>目標串列
        /// </summary>
        /// <param name="list">目標串列</param>
        public ForEach(IList list)
        {
            _list = list;
        }

        #endregion 宣告建構子


        #region 宣告公開的屬性

        /// <summary>目前執行次數
        /// </summary>
        public int Count => _count;

        #endregion 宣告公開的屬性


        #region 宣告公開的方法

        /// <summary>執行動作
        /// </summary>
        /// <param name="callback">動作委派</param>
        public void Do(Action callback)
        {
            var count = 0;

            _array.IsNotNull(a => count = _array.Length);
            _list.IsNotNull(y => count = _list.Count);

            if (0 == count)
            {
                return;
            }

            if (_count >= count)
            {
                return;
            }

            try
            {
                callback();
            }
            catch (Exception ex)
            {
                Command.StdoutErr(ex.ToString());
                throw;
            }

            _count++;
            Do(callback);
        }

        /// <summary>執行非同步作業動作
        /// </summary>
        /// <param name="asyncFunc">非同步作業動作</param>
        /// <returns>非同步作業任務</returns>
        public async Task DoAsync(Func<Task> asyncFunc)
        {
            var count = 0;

            _array.IsNotNull(a => count = _array.Length);
            _list.IsNotNull(y => count = _list.Count);

            if (0 == count)
            {
                return;
            }

            if (_count >= count)
            {
                return;
            }

            try
            {
                await asyncFunc();
            }
            catch (Exception ex)
            {
                Command.StdoutErr(ex.ToString());
                throw;
            }

            _count++;
            await DoAsync(asyncFunc);
        }

        #endregion 宣告公開的方法
    }
}
