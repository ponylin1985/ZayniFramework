﻿namespace ZayniFramework.Caching.RedisClientComponent
{
    /// <summary>RedisClient 的錯誤代碼定義
    /// </summary>
    public class RedisClientErrorCode
    {
        /// <summary>RedisClient 連線異常
        /// </summary>
        public const string CONNECTION_ERROR = "REDIS_CLIENT_CONNECTION_ERROR";

        /// <summary>RedisClient 執行發生程式異常
        /// </summary>
        public const string OCCUR_EXCEPTION = "REDIS_CLIENT_EXCEPTION";

        /// <summary>RedisClient 執行發生錯誤
        /// </summary>
        public const string OCCUR_ERROR = "REDIS_CLIENT_ERROR";

        /// <summary>RedisClient 接收到不合法的參數呼叫
        /// </summary>
        public const string INVALID_ARGS = "REDIS_CLIENT_INVALID_ARGS";
    }
}
