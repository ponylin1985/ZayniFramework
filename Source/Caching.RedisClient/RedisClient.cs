﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Configuration;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.Caching.RedisClientComponent
{
    /// <summary>Redis 客戶端個體
    /// </summary>
    public class RedisClient
    {
        #region 宣告屬性

        /// <summary>Redis Client 客戶端 Config 設定名稱
        /// </summary>
        public string Name { get; private set; }

        /// <summary>Redis Clinet 客戶端 Config 設定
        /// </summary>
        public RedisClientSetting Config { get; internal set; }

        /// <summary>Redis Server 連線
        /// </summary>
        private ConnectionMultiplexer Connection { get; set; }

        /// <summary>Redis Server 資料庫
        /// </summary>
        public IDatabase Db { get; private set; }

        /// <summary>Redis Server 連線是否正常連接中
        /// </summary>
        public bool IsConnected => !Connection.IsNull() && Connection.IsConnected;

        #endregion 宣告屬性


        #region 宣告建構子

        /// <summary>內部的多載建構子
        /// </summary>
        /// <param name="redisClientName">Redis Client 客戶端 Config 設定名稱</param>
        internal RedisClient(string redisClientName)
        {
            if (redisClientName.IsNullOrEmpty())
            {
                var ex = new ArgumentException($"Invalid {nameof(redisClientName)}. Not allowed null or empty string.");
                Logger.Exception(this, ex, eventTitle: Logger.GetTraceLogTitle(this, "ctor"), $"RedisClient ctor occur exception.");
                ConsoleLogger.LogError(ex.ToString());
                throw ex;
            }

            Name = redisClientName;
            Config = ConfigReader.GetRedisClientConfig(Name);

            if (Config.IsNull())
            {
                var ex = new ConfigurationErrorsException($"RedisClient configuration error. RedisClientName: {Name}.");
                Logger.Exception(this, ex, eventTitle: Logger.GetTraceLogTitle(this, "ctor"), $"RedisClient ctor occur exception. RedisClientName: {Name}.");
                ConsoleLogger.LogError(ex.ToString());
                throw ex;
            }

            var opt = new ConfigurationOptions()
            {
                EndPoints = { { Config.Host, Config.Port } },
                KeepAlive = 100,
                DefaultDatabase = Config.DatabaseNo,
                AllowAdmin = true,
                ConnectTimeout = Config.ConnectTimeout,
                SyncTimeout = Config.SyncTimeout,
                AbortOnConnectFail = false
            };

            Config.Password.IsNotNullOrEmpty(p => opt.Password = p);

            try
            {
                Connection = ConnectionMultiplexer.Connect(opt);
                Db = Connection.GetDatabase(Config.DatabaseNo);
            }
            catch (Exception ex)
            {
                Logger.Exception(this, ex, eventTitle: Logger.GetTraceLogTitle(this, "ctor"), $"RedisClient create redis connection occur exception. RedisClientName: {redisClientName}");
                ConsoleLogger.LogError(ex.ToString());
                throw;
            }
        }

        /// <summary>解構子
        /// </summary>
        ~RedisClient()
        {
            Name = null;
            Config = null;
            Connection = null;
            Db = null;
        }

        #endregion 宣告建構子


        #region 宣告公開的方法

        /// <summary>執行 Redis 命令
        /// </summary>
        /// <param name="action">Redis 動作委派</param>
        /// <param name="parameters">委派參數</param>
        public IResult Exec(Delegate action, params object[] parameters)
        {
            var result = Result.Create();

            if (!CheckConnectionStatus())
            {
                result.Code = RedisClientErrorCode.CONNECTION_ERROR;
                result.Message = $"The connection between RedisClient '{Name}' and Redis server is broken. Redis Server: {Config.Host}:{Config.Port}.";
                Logger.Error(this, result.Message, Logger.GetTraceLogTitle(this, nameof(ExecResult)));
                return result;
            }

            try
            {
                DelegateInvoker.Exec(action, parameters);
            }
            catch (Exception ex)
            {
                result.ExceptionObject = ex;
                result.HasException = true;
                result.Code = RedisClientErrorCode.OCCUR_EXCEPTION;
                result.Message = $"RedisClient '{Name}' execute redis command occur exception. {Environment.NewLine}{ex}";
                Logger.Exception(this, ex, eventTitle: Logger.GetTraceLogTitle(this, nameof(Exec)), result.Message);
                return result;
            }

            Config.TraceLoggerName.IsNotNullOrEmpty(n =>
            {
                try
                {
                    var reqId = RandomTextHelper.Create(16);
                    var meth = action.Method;
                    var name = meth.Name;
                    var para = meth.GetParameters();
                    var args = new List<Parameter>();

                    for (int i = 0, len = para.Length; i < len; i++)
                    {
                        args.Add(new Parameter(para[i].Name, parameters[i]));
                    }

                    RedisClientLogger.RedisRequestLog(Config.Name, $"{Config.Host}:{Config.Port}", reqId, name, args, Config.TraceLoggerName);
                }
                catch (Exception ex)
                {
                    Logger.Exception(this, ex, eventTitle: Logger.GetTraceLogTitle(this, nameof(Exec)), $"RedisClient request action trace log occur exception.");
                }
            });

            result.Success = true;
            return result;
        }

        /// <summary>執行 Redis 命令，並且取得執行結果的回傳值
        /// </summary>
        /// <typeparam name="TResult">回傳值的泛型</typeparam>
        /// <param name="action">Redis 動作委派</param>
        /// <param name="parameters">委派參數</param>
        /// <returns>執行結果的回傳值</returns>
        public IResult<TResult> ExecResult<TResult>(Delegate action, params object[] parameters)
        {
            var result = Result.Create<TResult>();

            if (!CheckConnectionStatus())
            {
                result.Code = RedisClientErrorCode.CONNECTION_ERROR;
                result.Message = $"The connection between RedisClient '{Name}' and Redis server is broken. Redis Server: {Config.Host}:{Config.Port}.";
                Logger.Error(this, result.Message, Logger.GetTraceLogTitle(this, nameof(ExecResult)));
                return result;
            }

            TResult rst;

            try
            {
                rst = DelegateInvoker.ExecResult<TResult>(action, parameters);
            }
            catch (Exception ex)
            {
                result.ExceptionObject = ex;
                result.HasException = true;
                result.Message = $"RedisClient '{Name}' execute redis command occur exception. {Environment.NewLine}{ex}";
                result.Code = RedisClientErrorCode.OCCUR_EXCEPTION;
                Logger.Exception(this, ex, eventTitle: Logger.GetTraceLogTitle(this, nameof(ExecResult)), result.Message);
                return result;
            }

            Config.TraceLoggerName.IsNotNullOrEmpty(n =>
            {
                try
                {
                    var reqId = RandomTextHelper.Create(16);
                    var meth = action.Method;
                    var name = meth.Name;
                    var para = meth.GetParameters();
                    var args = new List<Parameter>();

                    for (int i = 0, len = para.Length; i < len; i++)
                    {
                        args.Add(new Parameter(para[i].Name, parameters[i]));
                    }

                    RedisClientLogger.RedisRequestLog(Config.Name, $"{Config.Host}:{Config.Port}", reqId, name, args, Config.TraceLoggerName);
                    RedisClientLogger.RedisResponseLog(Config.Name, $"{Config.Host}:{Config.Port}", reqId, name, rst, Config.TraceLoggerName);
                }
                catch (Exception ex)
                {
                    Logger.Exception(this, ex, eventTitle: Logger.GetTraceLogTitle(this, nameof(Exec)), $"RedisClient action trace log occur exception.");
                }
            });

            result.Data = rst;
            result.Success = true;
            return result;
        }

        /// <summary>以 Hash 資料結構存放資料至 Redis Server。
        /// </summary>
        /// <param name="key">資料快取 Key 值</param>
        /// <param name="data">資料載體物件</param>
        /// <param name="expire">存在在 Redis Server 中的時間期間</param>
        /// <returns>存放結果</returns>
        public IResult PutHashObject(string key, object data, TimeSpan? expire = null)
        {
            var result = Result.Create();

            if (!CheckConnectionStatus())
            {
                result.Code = RedisClientErrorCode.CONNECTION_ERROR;
                result.Message = $"The connection between RedisClient '{Name}' and Redis server is broken. Redis Server: {Config.Host}:{Config.Port}.";
                Logger.Error(this, result.Message, Logger.GetTraceLogTitle(this, nameof(PutHashObject)));
                return result;
            }

            if (key.IsNullOrEmpty())
            {
                result.Code = RedisClientErrorCode.INVALID_ARGS;
                result.Message = $"Invalid '{nameof(key)}' argument. Can not be null or empty string.";
                Logger.Error(this, result.Message, Logger.GetTraceLogTitle(this, nameof(PutHashObject)));
                return result;
            }

            if (null == data)
            {
                result.Code = RedisClientErrorCode.INVALID_ARGS;
                result.Message = $"Invalid '{nameof(data)}' argument. Not allowed put null value to Redis server.";
                Logger.Error(this, result.Message, Logger.GetTraceLogTitle(this, nameof(PutHashObject)));
                return result;
            }

            if (data is string d && d.IsNullOrEmpty())
            {
                result.Code = RedisClientErrorCode.INVALID_ARGS;
                result.Message = $"Invalid '{nameof(data)}' argument. Not allowed put null empty string to Redis server.";
                Logger.Error(this, result.Message, Logger.GetTraceLogTitle(this, nameof(PutHashObject)));
                return result;
            }

            try
            {
                var hashEntries = new List<HashEntry>();
                var properties = data.GetType().GetProperties();

                if (properties.IsNullOrEmpty())
                {
                    result.Code = RedisClientErrorCode.OCCUR_ERROR;
                    result.Message = $"No public property member in '{data.GetType().FullName}' type. Can not set property correctly.";
                    Logger.Error(this, result.Message, Logger.GetTraceLogTitle(this, nameof(PutHashObject)));
                    return result;
                }

                foreach (var property in properties)
                {
                    var hashMetadata = Reflector.GetCustomAttribute<HashObjectAttribute>(property);

                    if (hashMetadata.IsNull())
                    {
                        continue;
                    }

                    if (hashMetadata.Name.IsNullOrEmpty())
                    {
                        continue;
                    }

                    var name = hashMetadata.Name?.Trim();
                    var val = property.GetValue(data);
                    var value = val.IsNull() && hashMetadata.DefaultValue.IsNotNullOrEmpty() ? hashMetadata.DefaultValue : val?.ToString();
                    hashEntries.Add(new HashEntry(name, value));
                }

                Db.HashSet(key, [.. hashEntries]);
                expire.IsNotNull(q => Db.KeyExpire(key, expire));

                var reqId = RandomTextHelper.Create(16);
                RedisClientLogger.RedisPutHashObjectLog(Config.Name, $"{Config.Host}:{Config.Port}", reqId, hashEntries, Config.TraceLoggerName);
            }
            catch (Exception ex)
            {
                result.Code = RedisClientErrorCode.OCCUR_EXCEPTION;
                result.Message = $"Put hash data to Redis server occur exception. Key: {key}.";
                Logger.Exception(this, ex, eventTitle: Logger.GetTraceLogTitle(this, nameof(PutHashObject)), result.Message);
                return result;
            }

            result.Success = true;
            return result;
        }

        /// <summary>取得 Redis Server 上 Hash 資料結構的所有 SubKey 的資料。
        /// </summary>
        /// <typeparam name="TData">資料模型泛型</typeparam>
        /// <param name="key">資料快取 Key 值</param>
        /// <returns>取得結果</returns>
        public IResult<TData> GetHashObject<TData>(string key)
        {
            var result = Result.Create<TData>();

            if (!CheckConnectionStatus())
            {
                result.Code = RedisClientErrorCode.CONNECTION_ERROR;
                result.Message = $"The connection between RedisClient '{Name}' and Redis server is broken. Redis Server: {Config.Host}:{Config.Port}.";
                Logger.Error(this, result.Message, Logger.GetTraceLogTitle(this, nameof(GetHashObject)));
                return result;
            }

            if (key.IsNullOrEmpty())
            {
                result.Code = RedisClientErrorCode.INVALID_ARGS;
                result.Message = $"Invalid '{nameof(key)}' argument. Can not be null or empty string.";
                Logger.Error(this, result.Message, Logger.GetTraceLogTitle(this, nameof(GetHashObject)));
                return result;
            }

            TData data = default;

            try
            {
                var reqId = RandomTextHelper.Create(16);
                RedisClientLogger.RedisGetHashObjectReqLog(Config.Name, $"{Config.Host}:{Config.Port}", reqId, key, Config.TraceLoggerName);

                var hashEntries = Db.HashGetAll(key);

                if (hashEntries.IsNullOrEmpty())
                {
                    result.Code = RedisClientErrorCode.OCCUR_ERROR;
                    result.Message = $"No data found from Redis server. Key: {key}.";
                    Logger.Error(this, result.Message, Logger.GetTraceLogTitle(this, nameof(GetHashObject)));
                    return result;
                }

                data = hashEntries.ConvertToObj<TData>(out var errorMsg);

                if (data.IsNull())
                {
                    result.Code = RedisClientErrorCode.OCCUR_ERROR;
                    result.Message = $"Convert HashEntry array retrive from Redis server to '{typeof(TData).FullName}' object occur error. Key: {key}.";
                    Logger.Error(this, result.Message, Logger.GetTraceLogTitle(this, nameof(GetHashObject)));
                    return result;
                }

                RedisClientLogger.RedisGetHashObjectResLog(Config.Name, $"{Config.Host}:{Config.Port}", reqId, hashEntries, Config.TraceLoggerName);
            }
            catch (Exception ex)
            {
                result.Code = RedisClientErrorCode.OCCUR_EXCEPTION;
                result.Message = $"Get hash data from Redis server occur exception. Key: {key}.";
                Logger.Exception(this, ex, eventTitle: Logger.GetTraceLogTitle(this, nameof(GetHashObject)), result.Message);
                return result;
            }

            result.Data = data;
            result.Success = true;
            return result;
        }

        /// <summary>清空 Redis 資料庫中的所有資料
        /// </summary>
        /// <returns>清空資料庫的結果</returns>
        public IResult ClearDatabase()
        {
            var result = Result.Create();

            try
            {
                Connection.GetServer($"{Config.Host}:{Config.Port}").FlushDatabase(Config.DatabaseNo);
            }
            catch (Exception ex)
            {
                result.Code = RedisClientErrorCode.OCCUR_EXCEPTION;
                result.ExceptionObject = ex;
                result.Message = $"Clear all keys in redis database occur exception. Database: {Config.DatabaseNo}.";
                Logger.Exception(this, ex, eventTitle: Logger.GetTraceLogTitle(this, nameof(ClearDatabase)), result.Message);
                return result;
            }

            result.Success = true;
            return result;
        }

        #endregion 宣告公開的方法


        #region 宣告私有的方法

        /// <summary>檢查目前的 Redis Server 連線是否正常連線中。
        /// </summary>
        /// <returns>目前的 Redis Server 連線是否正常連線中</returns>
        private bool CheckConnectionStatus()
        {
            if (Connection.IsNull() || Db.IsNull())
            {
                return false;
            }

            return Connection.IsConnected;
        }

        #endregion 宣告私有的方法
    }
}
