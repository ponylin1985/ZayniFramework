﻿using System;
using ZayniFramework.Common;


namespace ZayniFramework.Caching.RedisClientComponent
{
    /// <summary>組態設定讀取器
    /// </summary>
    internal static class ConfigReader
    {
        /// <summary>The configuration of ZayniFramework.
        /// </summary>
        /// <returns></returns>
        private static readonly ZayniFrameworkSettings _zayniSettings = ConfigManager.GetZayniFrameworkSettings();

        /// <summary>Get the RedisClient config by the config name.
        /// </summary>
        /// <param name="redisClientName">The config name of RedisClient in zayni.json.</param>
        /// <returns>RedisClient config.</returns>
        internal static RedisClientSetting GetRedisClientConfig(string redisClientName) =>
            _zayniSettings.RedisClientSettings.Find(c => c.Name == redisClientName);

        /// <summary>Get the default RedisClient config.
        /// </summary>
        /// <returns></returns>
        internal static RedisClientSetting GetDefaultClientConfig()
        {
            var defaultClient = _zayniSettings.RedisClientSettings.Find(c => c.IsDefault) ??
                throw new Exception($"Default RedisClient config is not found in zayni.json.");
            return defaultClient;
        }
    }
}
