﻿using StackExchange.Redis;
using System;
using System.Linq;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.Caching.RedisClientComponent
{
    /// <summary>對 StackExchange.Redis 套件的擴充方法 API 類別
    /// </summary>
    public static class RedisExtensions
    {
        /// <summary>將來源的 HashEntry[] 陣列轉換成指定泛型的 C# entity 物件。
        /// </summary>
        /// <typeparam name="TData">目標物件的泛型</typeparam>
        /// <param name="hashEntries">來源的 HashEntry[] 陣列</param>
        /// <param name="message">錯誤訊息</param>
        /// <returns>目標 C# entity 物件</returns>
        public static TData ConvertToObj<TData>(this HashEntry[] hashEntries, out string message)
        {
            message = null;

            if (hashEntries.IsNullOrEmpty())
            {
                message = $"HashEntry is null or empty array.";
                Logger.Error(nameof(RedisExtensions), message, Logger.GetTraceLogTitle(nameof(RedisExtensions), nameof(ConvertToObj)));
                return default;
            }

            var obj = default(TData);

            try
            {
                obj = Activator.CreateInstance<TData>();
            }
            catch (Exception ex)
            {
                message = $"Create instance of '{typeof(TData).FullName}' type ocur exception. {Environment.NewLine}{ex}";
                Logger.Error(nameof(RedisExtensions), message, Logger.GetTraceLogTitle(nameof(RedisExtensions), nameof(ConvertToObj)));
                return default;
            }

            if (obj.IsNull())
            {
                message = $"Convert HashEntry array to '{typeof(TData).FullName}' type fail due to create instance of target type retrive null object.";
                Logger.Error(nameof(RedisExtensions), message, Logger.GetTraceLogTitle(nameof(RedisExtensions), nameof(ConvertToObj)));
                return default;
            }

            var properties = typeof(TData).GetProperties();

            if (properties.IsNullOrEmpty())
            {
                message = $"No public property member in '{typeof(TData).FullName}' type. Can not set property correctly.";
                Logger.Error(nameof(RedisExtensions), message, Logger.GetTraceLogTitle(nameof(RedisExtensions), nameof(ConvertToObj)));
                return default;
            }

            try
            {
                foreach (var property in properties)
                {
                    var hashMetadata = Reflector.GetCustomAttribute<HashObjectAttribute>(property);

                    if (hashMetadata.IsNull())
                    {
                        continue;
                    }

                    if (hashMetadata.Name.IsNullOrEmpty())
                    {
                        continue;
                    }

                    var redisValue = (RedisValue)hashEntries.Where(h => h.Name == hashMetadata.Name)?.SingleOrDefault().Value;
                    object value = redisValue;

                    if (!redisValue.HasValue && hashMetadata.DefaultValue.IsNotNullOrEmpty())
                    {
                        value = hashMetadata.DefaultValue;
                    }

                    Reflector.SetPropertyValue(obj, property, value?.ToString());
                }
            }
            catch (Exception ex)
            {
                message = $"Set property of '{typeof(TData).FullName}' type occur exception. {Environment.NewLine}{ex}";
                Logger.Exception(nameof(RedisExtensions), ex, eventTitle: Logger.GetTraceLogTitle(nameof(RedisExtensions), nameof(ConvertToObj)), message);
                return default;
            }

            return obj;
        }
    }
}
