﻿using StackExchange.Redis;
using System.Collections.Generic;
using System.Text;
using ZayniFramework.Common;
using ZayniFramework.Logging;
using ZayniFramework.Serialization;


namespace ZayniFramework.Caching.RedisClientComponent
{
    /// <summary>RedisClient 客戶端日誌器
    /// </summary>
    internal static class RedisClientLogger
    {
        #region 宣告內部的方法

        /// <summary>RedisClient 請求的動作日誌記錄
        /// </summary>
        /// <param name="redisClientName">RedisClient 設定名稱</param>
        /// <param name="redisHost">Redis Server 的主機位址</param>
        /// <param name="reqId">請求代碼</param>
        /// <param name="actionName">動作名稱</param>
        /// <param name="args">動作參數集合</param>
        /// <param name="loggerName">日誌器名稱</param>
        internal static void RedisRequestLog(string redisClientName, string redisHost, string reqId, string actionName, List<Parameter> args, string loggerName)
        {
            var sbReqMsg = new StringBuilder();
            sbReqMsg.AppendLine($"RedisClientName: {redisClientName}.");
            sbReqMsg.AppendLine($"Redis Server: {redisHost}.");
            sbReqMsg.AppendLine($"RequestId: {reqId}.");
            sbReqMsg.AppendLine($"Action: {actionName}.");
            sbReqMsg.AppendLine($"Request Arguments:");
            sbReqMsg.AppendLine(NewtonsoftJsonConvert.Serialize(args));
            Logger.Info(nameof(RedisClient), sbReqMsg.ToString(), "RedisClient Request Trace", loggerName);
        }

        /// <summary>RedisClient 接收到 Redis Server 回應的日誌紀錄
        /// </summary>
        /// <param name="redisClientName">RedisClient 設定名稱</param>
        /// <param name="redisHost">Redis Server 的主機位址</param>
        /// <param name="reqId">原始請求代碼</param>
        /// <param name="actionName">動作名稱</param>
        /// <param name="returnValue">動作回傳值</param>
        /// <param name="loggerName">日誌器名稱</param>
        internal static void RedisResponseLog(string redisClientName, string redisHost, string reqId, string actionName, object returnValue, string loggerName)
        {
            var sbResMsg = new StringBuilder();
            sbResMsg.AppendLine($"RedisClientName: {redisClientName}.");
            sbResMsg.AppendLine($"Redis Server: {redisHost}.");
            sbResMsg.AppendLine($"RequestId: {reqId}.");
            sbResMsg.AppendLine($"Action: {actionName}.");
            sbResMsg.AppendLine($"Return Value: {returnValue}");
            Logger.Info(nameof(RedisClient), sbResMsg.ToString(), "RedisClient Response Trace", loggerName);
        }

        /// <summary>RedisClient 進行 Hash 資料結構存放的請求日誌紀錄
        /// </summary>
        /// <param name="redisClientName">RedisClient 設定名稱</param>
        /// <param name="redisHost">Redis Server 的主機位址</param>
        /// <param name="reqId">請求代碼</param>
        /// <param name="hashEntries">Hash 資料結構集合</param>
        /// <param name="loggerName">日誌器名稱</param>
        internal static void RedisPutHashObjectLog(string redisClientName, string redisHost, string reqId, List<HashEntry> hashEntries, string loggerName)
        {
            var sbReqMsg = new StringBuilder();
            sbReqMsg.AppendLine($"RedisClientName: {redisClientName}.");
            sbReqMsg.AppendLine($"Redis Server: {redisHost}.");
            sbReqMsg.AppendLine($"RequestId: {reqId}.");
            sbReqMsg.AppendLine($"Action: {nameof(IDatabase.HashSet)}.");
            sbReqMsg.AppendLine($"Request Arguments:");
            sbReqMsg.AppendLine(NewtonsoftJsonConvert.Serialize(hashEntries));
            Logger.Info(nameof(RedisClient), sbReqMsg.ToString(), "RedisClient Request Trace", loggerName);
        }

        /// <summary>RedisClient 取得 Hash 資料結構請求的日誌紀錄
        /// </summary>
        /// <param name="redisClientName">RedisClient 設定名稱</param>
        /// <param name="redisHost">Redis Server 的主機位址</param>
        /// <param name="reqId">請求代碼</param>
        /// <param name="key">資料快取 Key 值</param>
        /// <param name="loggerName">日誌器名稱</param>
        internal static void RedisGetHashObjectReqLog(string redisClientName, string redisHost, string reqId, string key, string loggerName)
        {
            var sbReqMsg = new StringBuilder();
            sbReqMsg.AppendLine($"RedisClientName: {redisClientName}.");
            sbReqMsg.AppendLine($"Redis Server: {redisHost}.");
            sbReqMsg.AppendLine($"RequestId: {reqId}.");
            sbReqMsg.AppendLine($"Action: {nameof(IDatabase.HashGetAll)}.");
            sbReqMsg.AppendLine($"Request Arguments: {key}");
            Logger.Info(nameof(RedisClient), sbReqMsg.ToString(), "RedisClient Request Trace", loggerName);
        }

        /// <summary>RedisClient 取得 Hash 資料結構回應的日誌紀錄
        /// </summary>
        /// <param name="redisClientName">RedisClient 設定名稱</param>
        /// <param name="redisHost">Redis Server 的主機位址</param>
        /// <param name="reqId">請求代碼</param>
        /// <param name="hashEntries">Hash 資料結構集合</param>
        /// <param name="loggerName">日誌器名稱</param>
        internal static void RedisGetHashObjectResLog(string redisClientName, string redisHost, string reqId, HashEntry[] hashEntries, string loggerName)
        {
            var sbResMsg = new StringBuilder();
            sbResMsg.AppendLine($"RedisClientName: {redisClientName}.");
            sbResMsg.AppendLine($"Redis Server: {redisHost}.");
            sbResMsg.AppendLine($"RequestId: {reqId}.");
            sbResMsg.AppendLine($"Action: {nameof(IDatabase.HashGetAll)}.");
            sbResMsg.AppendLine($"Return Value: {NewtonsoftJsonConvert.Serialize(hashEntries)}");
            Logger.Info(nameof(RedisClient), sbResMsg.ToString(), "RedisClient Response Trace", loggerName
                );
        }

        #endregion 宣告內部的方法
    }
}
