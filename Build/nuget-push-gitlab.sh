#!/bin/bash

version=${1:-8.0.142}
echo ${version}

dotnet nuget push ZayniFramework.Caching.${version}.symbols.nupkg --source "gitlab"
dotnet nuget push ZayniFramework.Caching.RedisClient.${version}.symbols.nupkg --source "gitlab"
dotnet nuget push ZayniFramework.Caching.RemoteCache.${version}.symbols.nupkg --source "gitlab"
dotnet nuget push ZayniFramework.Common.${version}.symbols.nupkg --source "gitlab"
dotnet nuget push ZayniFramework.Compression.${version}.symbols.nupkg --source "gitlab"
dotnet nuget push ZayniFramework.Cryptography.${version}.symbols.nupkg --source "gitlab"
dotnet nuget push ZayniFramework.DataAccess.LightORM.${version}.symbols.nupkg --source "gitlab"
dotnet nuget push ZayniFramework.DataAccess.${version}.symbols.nupkg --source "gitlab"
dotnet nuget push ZayniFramework.ExceptionHandling.${version}.symbols.nupkg --source "gitlab"
dotnet nuget push ZayniFramework.Formatting.${version}.symbols.nupkg --source "gitlab"
dotnet nuget push ZayniFramework.Logging.${version}.symbols.nupkg --source "gitlab"
dotnet nuget push ZayniFramework.Serialization.${version}.symbols.nupkg --source "gitlab"
dotnet nuget push ZayniFramework.Validation.${version}.symbols.nupkg --source "gitlab"
dotnet nuget push ZayniFramework.RabbitMQ.Msg.Client.${version}.symbols.nupkg --source "gitlab"

dotnet nuget push ZayniFramework.Middle.Service.${version}.symbols.nupkg --source "gitlab"
dotnet nuget push ZayniFramework.Middle.Service.Client.${version}.symbols.nupkg --source "gitlab"
dotnet nuget push ZayniFramework.Middle.Service.Entity.${version}.symbols.nupkg --source "gitlab"
dotnet nuget push ZayniFramework.Middle.Service.Grpc.${version}.symbols.nupkg --source "gitlab"
dotnet nuget push ZayniFramework.Middle.Service.Grpc.Client.${version}.symbols.nupkg --source "gitlab"
dotnet nuget push ZayniFramework.Middle.Service.Grpc.Entity.${version}.symbols.nupkg --source "gitlab"
dotnet nuget push ZayniFramework.Middle.Service.HttpCore.${version}.symbols.nupkg --source "gitlab"
dotnet nuget push ZayniFramework.Middle.Service.HttpCore.Client.${version}.symbols.nupkg --source "gitlab"
dotnet nuget push ZayniFramework.Middle.Service.HttpCore.Entity.${version}.symbols.nupkg --source "gitlab"
dotnet nuget push ZayniFramework.Middle.TelnetService.${version}.symbols.nupkg --source "gitlab"

dotnet nuget push zch.${version}.nupkg --source "gitlab"
dotnet nuget push zdm.${version}.nupkg --source "gitlab"

#! cd to the sln root directory
#! find . -type f -name '*.symbols.nupkg' -delete
