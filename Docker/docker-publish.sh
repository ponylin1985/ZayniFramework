#!/bin/bash

version=${1:-8.0.142}
runtime=${2:-ubuntu}

echo images runtime OS: $runtime

docker login

docker tag zayni.client.app:$runtime-x64 ponylin1985/zayni.client.app:$runtime-x64
docker tag webapi.test:$runtime-x64 ponylin1985/webapi.test:$runtime-x64

docker push ponylin1985/zayni.client.app:$runtime-x64
docker push ponylin1985/webapi.test:$runtime-x64

docker rmi -f zayni.client.app:$runtime-x64
docker rmi -f webapi.test:$runtime-x64

docker rmi -f ponylin1985/zayni.client.app:$runtime-x64
docker rmi -f ponylin1985/webapi.test:$runtime-x64