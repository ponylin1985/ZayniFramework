FROM mcr.microsoft.com/dotnet/sdk:8.0.100-1-jammy-amd64 AS build
COPY . ./project_root
WORKDIR /project_root/Test/WebAPI.Test
RUN mkdir -p ./publish/log
RUN dotnet publish \
  -c Release \
  -v n \
  -r linux-x64 \
  --self-contained true \
  -p:PublishTrimmed=false \
  -p:GenerateDocumentationFile=false \
  -p:GeneratePackageOnBuild=false \
  --output ./publish
RUN rm -rf ./publish/conf \
  && find ./ -type f -name '*.xml' -delete
RUN groupadd -r appgroup && useradd -r -u 3365 -g appgroup appuser
RUN chown -R appuser:appgroup ./publish

# ================

FROM mcr.microsoft.com/dotnet/runtime-deps:8.0.0-jammy-chiseled-amd64 AS runtime
WORKDIR /app
COPY --from=build /project_root/Test/WebAPI.Test/publish .
USER 3365
ENTRYPOINT ["./WebAPI.Test"]
