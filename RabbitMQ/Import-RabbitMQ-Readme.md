## Zayni Framework RabbitMQ configurations sample

This document explains how you can import from the `ZayniFramework-RabbitMQ-Samples.json` to RabbitMQ service.

### Install the RabbitMQ service and RabbitMQ Web Management.

-  You can install RabbitMQ service and RabbitMQ Web Management by using **docker-compose**. It is the fastest way to run a localhost RabbitMQ server in your localhost machine.
   
   ```yaml
   version: "3"

   services:
   rabbitmq:
      image: rabbitmq:3.7.8-management
      container_name: rabbitmq
      ports:
         - 5672:5672
         - 15672:15672
      environment:
         RABBITMQ_DEFAULT_USER: {The_default_user_name_here}
         RABBITMQ_DEFAULT_PASS: {The_default_user_password_here}
         RABBITMQ_ERLANG_COOKIE: pony-rabbitmq-lab
         RABBITMQ_VM_MEMORY_HIGH_WATERMARK: 0.7
      volumes:
         - {Your_localhost_RabbitMQ_data_path_here}:/var/lib/rabbitmq
      restart: always
   ```

-  You can also install RabbitMQ by other ways...

### Import RabbitMQ settings json.

-  Run the RabbitMQ service and RabbitMQ Web Management first.

-  Browse the RabbitMQ Web Management in your browser.
   -  Go to `http://localhost:15672` page
   -  Enter your user name and password to login.
   
-  Create a `Virtual Host` first and set the permission for your RabbitMQ user.
   -  You need to create a new empty virtual host on your RabbitMQ service for restore.
   -  Go to the `Admin` tag.
   -  Select the `Virtual Hosts` on the right side menu.
   -  Add a new virtual host called _**ZayniTest**_ and click `Add virtual host` button.
   -  Done.

-  Import the RabbitMQ settings.
   -  Go to the `Overview` tab.
   -  Click the `Import definitions`.
   -  Select the RabbitMQ settings json file from your computer. (The path of `ZayniFramework-RabbitMQ-Samples.json`)
   -  Select the `Virtual Host` you wish to restore.
   -  Click the `Upload brokcer definitions` button and save (override) the settings.
   -  Done.