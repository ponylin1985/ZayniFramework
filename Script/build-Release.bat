@echo off

dotnet restore ./ZayniFramework.sln -v n
dotnet clean ./ZayniFramework.sln -c Release
dotnet build ./ZayniFramework.sln -c Release