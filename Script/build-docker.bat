@echo off


docker build --no-cache --file ./Docker/api-service-host/Dockerfiles/multistage.dockerfile . -t webapi.test:ubuntu22.04-x64
docker build --no-cache --file ./Docker/service-host-clientside/Dockerfiles/multistage.dockerfile . -t zayni.client.app:ubuntu22.04-x64
