#!/bin/bash

SolutionDir=${1:-~/GitRepo/MyGitLab/ponylin1985/zayniframework}
echo ${SolutionDir}

cp -r ${SolutionDir}/Source/Caching/bin/Release/*.nupkg ${SolutionDir}/Build
cp -r ${SolutionDir}/Source/Caching.RedisClient/bin/Release/*.nupkg ${SolutionDir}/Build
cp -r ${SolutionDir}/Source/Caching.RemoteCache/bin/Release/*.nupkg ${SolutionDir}/Build
cp -r ${SolutionDir}/Source/Common/bin/Release/*.nupkg ${SolutionDir}/Build
cp -r ${SolutionDir}/Source/Compression/bin/Release/*.nupkg ${SolutionDir}/Build
cp -r ${SolutionDir}/Source/Cryptography/bin/Release/*.nupkg ${SolutionDir}/Build
cp -r ${SolutionDir}/Source/DataAcces.Lightweight/bin/Release/*.nupkg ${SolutionDir}/Build
cp -r ${SolutionDir}/Source/DataAccess/bin/Release/*.nupkg ${SolutionDir}/Build
cp -r ${SolutionDir}/Source/ExceptionHandling/bin/Release/*.nupkg ${SolutionDir}/Build
cp -r ${SolutionDir}/Source/Formatting/bin/Release/*.nupkg ${SolutionDir}/Build
cp -r ${SolutionDir}/Source/Logging/bin/Release/*.nupkg ${SolutionDir}/Build
cp -r ${SolutionDir}/Source/Serialization/bin/Release/*.nupkg ${SolutionDir}/Build
cp -r ${SolutionDir}/Source/Validation/bin/Release/*.nupkg ${SolutionDir}/Build
cp -r ${SolutionDir}/Source/RabbitMQ.Msg.Client/bin/Release/*.nupkg ${SolutionDir}/Build

cp -r ${SolutionDir}/Source/ServiceHost/Middle.ServiceCore/bin/Release/*.nupkg ${SolutionDir}/Build
cp -r ${SolutionDir}/Source/ServiceHost/Middle.Service.Client/bin/Release/*.nupkg ${SolutionDir}/Build
cp -r ${SolutionDir}/Source/ServiceHost/Middle.Service.Entity/bin/Release/*.nupkg ${SolutionDir}/Build
cp -r ${SolutionDir}/Source/ServiceHost/Middle.TelnetService/bin/Release/*.nupkg ${SolutionDir}/Build

cp -r ${SolutionDir}/Source/ServiceHost/Middle.Service.Grpc/bin/Release/*.nupkg ${SolutionDir}/Build
cp -r ${SolutionDir}/Source/ServiceHost/Middle.Service.Grpc.Client/bin/Release/*.nupkg ${SolutionDir}/Build
cp -r ${SolutionDir}/Source/ServiceHost/Middle.Service.Grpc.Entity/bin/Release/*.nupkg ${SolutionDir}/Build

cp -r ${SolutionDir}/Source/ServiceHost/Middle.Service.HttpCore/bin/Release/*.nupkg ${SolutionDir}/Build
cp -r ${SolutionDir}/Source/ServiceHost/Middle.Service.HttpCore.Client/bin/Release/*.nupkg ${SolutionDir}/Build
cp -r ${SolutionDir}/Source/ServiceHost/Middle.Service.HttpCore.Entity/bin/Release/*.nupkg ${SolutionDir}/Build

cp -r ${SolutionDir}/Source/SDKTools/Zayni.Crypto.Helper/NuGet/*.nupkg ${SolutionDir}/Build
rm -rf ${SolutionDir}/Source/SDKTools/Zayni.Crypto.Helper/NuGet/*.*
cp -r ${SolutionDir}/Source/SDKTools/Zayni.Database.Migrations/NuGet/*.nupkg ${SolutionDir}/Build
rm -rf ${SolutionDir}/Source/SDKTools/Zayni.Database.Migrations/NuGet/*.*

echo Copy nupkgs to Build success!