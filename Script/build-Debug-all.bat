@echo off
dotnet restore ./ZayniFramework.sln -v n
dotnet clean ./ZayniFramework.sln -c Debug
dotnet build ./ZayniFramework.sln -c Debug -p:GenerateDocumentationFile=false -p:GeneratePackageOnBuild=false -p:DebugType=None --no-restore