#!/bin/bash

pwd
find . -type f -name '*.nupkg' -delete
find . -type d -name "bin" -exec rm -rf {} \;
find . -type d -name "obj" -exec rm -rf {} \;

dotnet restore ./ZayniFramework.sln -v n
dotnet clean ./ZayniFramework.sln -c Debug
dotnet build ./ZayniFramework.sln -c Debug -p:GenerateDocumentationFile=false -p:GeneratePackageOnBuild=false -p:DebugType=None --no-restore
find . -type f -name '*.nupkg' -delete
