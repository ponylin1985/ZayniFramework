#!/bin/bash

find . -type f -name '*.nupkg' -delete
find . -type d -name "bin" -exec rm -rf {} \;
find . -type d -name "obj" -exec rm -rf {} \;

dotnet restore ./ZayniFramework.sln -v n
dotnet clean ./ZayniFramework.sln -c Debug
dotnet build ./ZayniFramework.sln -c Debug -p:GenerateDocumentationFile=false -p:GeneratePackageOnBuild=false -p:DebugType=None --no-restore

# Common module unit tests
dotnet test ./Test/UnitTests/Zayni.Common.Test/Zayni.Common.Test.csproj --no-build --filter TestCategory=Common.Test

# Caching module unit tests
dotnet test ./Test/UnitTests/Zayni.Caching.Test/Zayni.Caching.Test.csproj --no-build --filter ClassName=Caching.Test.CacheManagerTester
dotnet test ./Test/UnitTests/Zayni.Caching.Test/Zayni.Caching.Test.csproj --no-build --filter ClassName=Caching.Test.RedisCacheTester

# Caching.RedisClient module unit tests
dotnet test ./Test/UnitTests/Zayni.Caching.RedisClient.Test/Zayni.Caching.RedisClient.Test.csproj --no-build --filter ClassName=Caching.RedisClientComponent.Test.RedisClientTester

# DataAccess & LightORM module unit tests
dotnet test ./Test/UnitTests/Zayni.DataAccess.Test/Zayni.DataAccess.Test.csproj --no-build --filter ClassName=DataAccess.Test.BaseDataAccessTest
dotnet test ./Test/UnitTests/Zayni.DataAccess.Test/Zayni.DataAccess.Test.csproj --no-build --filter ClassName=DataAccess.Test.DataContextTester

# Cryptography module unit tests
dotnet test ./Test/UnitTests/Zayni.Cryptography.Test/Zayni.Cryptography.Test.csproj --no-build --filter ClassName=Cryptography.Test.AesEncryptorTester
# dotnet test ./Test/UnitTests/Zayni.Cryptography.Test/Zayni.Cryptography.Test.csproj --no-build --filter ClassName=Cryptography.Test.DesEncryptorTester
dotnet test ./Test/UnitTests/Zayni.Cryptography.Test/Zayni.Cryptography.Test.csproj --no-build --filter ClassName=Cryptography.Test.HashEncryptorTester

# Logging module unit tests
dotnet test ./Test/UnitTests/Zayni.Logging.Test/Zayni.Logging.Test.csproj --no-build --filter ClassName=Logging.Test.LoggerTester

# ExceptionHandling module unit tests
dotnet test ./Test/UnitTests/Zayni.ExceptionHandling.Test/Zayni.ExceptionHandling.Test.csproj --no-build --filter ClassName=ExceptionHandling.Test.ExceptionPolicyTester

# Formatting module unit tests
dotnet test ./Test/UnitTests/Zayni.Formatting.Test/Zayni.Formatting.Test.csproj --no-build --filter ClassName=Formatting.Test.DataFormatterTest
dotnet test ./Test/UnitTests/Zayni.Formatting.Test/Zayni.Formatting.Test.csproj --no-build --filter ClassName=Formatting.Test.FormatManagerTest

# Serialization module unit tests
dotnet test ./Test/UnitTests/Zayni.Serialization.Test/Zayni.Serialization.Test.csproj --no-build --filter ClassName=Serialization.Test.ISerializerTester
dotnet test ./Test/UnitTests/Zayni.Serialization.Test/Zayni.Serialization.Test.csproj --no-build --filter ClassName=Serialization.Test.NewtonsoftJsonConvertTester

# Middle.Service module unit tests
dotnet test ./Test/UnitTests/Zayni.Middle.Service.Test/Zayni.Middle.Service.Test.csproj --no-build --filter ClassName=Middle.Service.Tester.RemoteProxyTester
dotnet test ./Test/UnitTests/Zayni.Middle.Service.Test/Zayni.Middle.Service.Test.csproj --no-build --filter ClassName=Middle.Service.Tester.UserServiceTester

find . -type f -name '*.nupkg' -delete
# find . -type d -name "bin" -exec rm -rf {} +
