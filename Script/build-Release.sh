#!/bin/bash

pwd
find . -type f -name '*.nupkg' -delete
find . -type d -name "bin" -exec rm -rf {} \;
# find . -type d -name "obj" -exec rm -rf {} \;

dotnet pack ./Source/SDKTools/Zayni.Crypto.Helper/Crypto.Helper.csproj
dotnet publish ./Source/SDKTools/Zayni.Database.Migrations/Database.Migrations.CLI.csproj
# 在 ./bin/Debug/net5.0/publish 目錄下，依照以下方式，把所有最後修改日期為 1980/01/01 的 dll 修正檔案最後修改日期。
touch -mt 202001011200 /Users/pony/GitRepo/MyGitLab/ZayniFramework/Source/SDKTools/Zayni.Database.Migrations/bin/Debug/net5.0/publish/MySql.Data.dll
# https://hackernoon.com/how-to-change-a-file-s-last-modified-and-creation-dates-on-mac-os-x-494f8f76cdf4
dotnet pack ./Source/SDKTools/Zayni.Database.Migrations/Database.Migrations.CLI.csproj --no-build -o ./Source/SDKTools/Zayni.Database.Migrations/NuGet

dotnet restore ./ZayniFramework.sln -v n
dotnet clean ./ZayniFramework.sln -c Release
dotnet build ./ZayniFramework.sln -c Release -p:GenerateDocumentationFile=true -p:GeneratePackageOnBuild=true --no-restore

