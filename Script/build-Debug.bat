@echo off
dotnet clean ./ZayniFramework.Modules.sln -c Debug
dotnet build ./ZayniFramework.Modules.sln -c Debug -p:GenerateDocumentationFile=true -p:GeneratePackageOnBuild=false -p:DebugType=None --no-restore