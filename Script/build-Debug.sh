#!/bin/bash

pwd
find . -type f -name '*.nupkg' -delete
find . -type d -name "bin" -exec rm -rf {} \;
# find . -type d -name "obj" -exec rm -rf {} \;

dotnet clean ./ZayniFramework.Modules.sln -c Debug
dotnet build ./ZayniFramework.Modules.sln -c Debug -p:GenerateDocumentationFile=true -p:GeneratePackageOnBuild=false -p:DebugType=None --no-restore

find . -type f -name '*.nupkg' -delete