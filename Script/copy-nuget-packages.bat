@echo off
set SolutionDir=%~dp0

cd\
cd %SolutionDir%\Build\
del /q *.nupkg

xcopy /s/f %SolutionDir%\Source\Caching\bin\Release\*.nupkg %SolutionDir%\Build\
xcopy /s/f %SolutionDir%\Source\Caching.RedisClient\bin\Release\*.nupkg %SolutionDir%\Build\
xcopy /s/f %SolutionDir%\Source\Caching.RemoteCache\bin\Release\*.nupkg %SolutionDir%\Build\
xcopy /s/f %SolutionDir%\Source\Common\bin\Release\*.nupkg %SolutionDir%\Build\
xcopy /s/f %SolutionDir%\Source\Compression\bin\Release\*.nupkg %SolutionDir%\Build\
xcopy /s/f %SolutionDir%\Source\Cryptography\bin\Release\*.nupkg %SolutionDir%\Build\
xcopy /s/f %SolutionDir%\Source\DataAcces.Lightweight\bin\Release\*.nupkg %SolutionDir%\Build\
xcopy /s/f %SolutionDir%\Source\DataAccess\bin\Release\*.nupkg %SolutionDir%\Build\
xcopy /s/f %SolutionDir%\Source\ExceptionHandling\bin\Release\*.nupkg %SolutionDir%\Build\
xcopy /s/f %SolutionDir%\Source\Formatting\bin\Release\*.nupkg %SolutionDir%\Build\
xcopy /s/f %SolutionDir%\Source\Logging\bin\Release\*.nupkg %SolutionDir%\Build\
xcopy /s/f %SolutionDir%\Source\Serialization\bin\Release\*.nupkg %SolutionDir%\Build\
xcopy /s/f %SolutionDir%\Source\Validation\bin\Release\*.nupkg %SolutionDir%\Build\
xcopy /s/f %SolutionDir%\Source\RabbitMQ.Msg.Client\bin\Release\*.nupkg %SolutionDir%\Build\

xcopy /s/f %SolutionDir%\Source\ServiceHost\Middle.ServiceCore\bin\Release\*.nupkg %SolutionDir%\Build\
xcopy /s/f %SolutionDir%\Source\ServiceHost\Middle.Service.Client\bin\Release\*.nupkg %SolutionDir%\Build\
xcopy /s/f %SolutionDir%\Source\ServiceHost\Middle.Service.Entity\bin\Release\*.nupkg %SolutionDir%\Build\
xcopy /s/f %SolutionDir%\Source\ServiceHost\Middle.TelnetService\bin\Release\*.nupkg %SolutionDir%\Build\

xcopy /s/f %SolutionDir%\Source\ServiceHost\Middle.Service.Grpc\bin\Release\*.nupkg %SolutionDir%\Build\
xcopy /s/f %SolutionDir%\Source\ServiceHost\Middle.Service.Grpc.Client\bin\Release\*.nupkg %SolutionDir%\Build\
xcopy /s/f %SolutionDir%\Source\ServiceHost\Middle.Service.Grpc.Entity\bin\Release\*.nupkg %SolutionDir%\Build\

xcopy /s/f %SolutionDir%\Source\ServiceHost\Middle.Service.HttpCore\bin\Release\*.nupkg %SolutionDir%\Build\
xcopy /s/f %SolutionDir%\Source\ServiceHost\Middle.Service.HttpCore.Client\bin\Release\*.nupkg %SolutionDir%\Build\
xcopy /s/f %SolutionDir%\Source\ServiceHost\Middle.Service.HttpCore.Entity\bin\Release\*.nupkg %SolutionDir%\Build\

xcopy /s/f %SolutionDir%\Source\SDKTools\Zayni.Crypto.Helper\NuGet\*.nupkg %SolutionDir%\Build\
xcopy /s/f %SolutionDir%\Source\SDKTools\Zayni.Database.Migrations\NuGet\*.nupkg %SolutionDir%\Build\