@echo off
dotnet clean ./ZayniFramework.Tests.sln -c Debug
dotnet build ./ZayniFramework.Tests.sln -c Debug -p:GenerateDocumentationFile=true -p:GeneratePackageOnBuild=false -p:DebugType=None --no-restore