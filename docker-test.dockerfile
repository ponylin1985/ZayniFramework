FROM mcr.microsoft.com/dotnet/sdk:8.0.100-1-jammy-amd64 AS build
COPY . ./project_root
WORKDIR /project_root
RUN ls -la
RUN dotnet restore ./ZayniFramework.sln -v n \
  && dotnet clean ./ZayniFramework.sln -c Debug \
  && dotnet build ./ZayniFramework.sln -c Debug -p:GenerateDocumentationFile=false -p:GeneratePackageOnBuild=false -p:DebugType=None --no-restore \
  && dotnet test ./Test/UnitTests/Zayni.Common.Test/Zayni.Common.Test.csproj --no-build --filter TestCategory=Common.Test \
  # && dotnet test ./Test/UnitTests/Zayni.Caching.Test/Zayni.Caching.Test.csproj --no-build --filter ClassName=Caching.Test.CacheManagerTester \
  # && dotnet test ./Test/UnitTests/Zayni.DataAccess.Test/Zayni.DataAccess.Test.csproj --no-build --filter ClassName=DataAccess.Test.BaseDataAccessTest \
  # && dotnet test ./Test/UnitTests/Zayni.DataAccess.Test/Zayni.DataAccess.Test.csproj --no-build --filter ClassName=DataAccess.Test.DataContextTester \
  && dotnet test ./Test/UnitTests/Zayni.Cryptography.Test/Zayni.Cryptography.Test.csproj --no-build --filter ClassName=Cryptography.Test.AesEncryptorTester \
  && dotnet test ./Test/UnitTests/Zayni.Cryptography.Test/Zayni.Cryptography.Test.csproj --no-build --filter ClassName=Cryptography.Test.RijndaelEncryptorTester \
  && dotnet test ./Test/UnitTests/Zayni.Cryptography.Test/Zayni.Cryptography.Test.csproj --no-build --filter ClassName=Cryptography.Test.DesEncryptorTester \
  && dotnet test ./Test/UnitTests/Zayni.Cryptography.Test/Zayni.Cryptography.Test.csproj --no-build --filter ClassName=Cryptography.Test.HashEncryptorTester \
  && dotnet test ./Test/UnitTests/Zayni.Serialization.Test/Zayni.Serialization.Test.csproj --no-build --filter ClassName=Serialization.Test.ISerializerTester \
  && dotnet test ./Test/UnitTests/Zayni.Serialization.Test/Zayni.Serialization.Test.csproj --no-build --filter ClassName=Serialization.Test.NewtonsoftJsonConvertTester \
  && dotnet test ./Test/UnitTests/Zayni.Formatting.Test/Zayni.Formatting.Test.csproj --no-build --filter ClassName=Formatting.Test.DataFormatterTest \
  && dotnet test ./Test/UnitTests/Zayni.Formatting.Test/Zayni.Formatting.Test.csproj --no-build --filter ClassName=Formatting.Test.FormatManagerTest \
  && dotnet test ./Test/UnitTests/Zayni.Middle.Service.Test/Zayni.Middle.Service.Test.csproj --no-build --filter ClassName=Middle.Service.Tester.RemoteProxyTester \
  && dotnet test ./Test/UnitTests/Zayni.Middle.Service.Test/Zayni.Middle.Service.Test.csproj --no-build --filter ClassName=Middle.Service.Tester.UserServiceTester