﻿using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace ZayniFramework.Common.Test
{
    /// <summary>真假值條件測試類別
    /// </summary>
    [TestClass()]
    [TestCategory("Common.Test")]
    public class WhenTester
    {
        /// <summary>當條件為真的正向測試
        /// </summary>
        [TestMethod()]
        [Description("當條件為真的正向測試")]
        public void WhenTrue_Test()
        {
            var message = "";
            var j = 9;
            When.True(() => 9 == j, () => message = "ok");
            Assert.AreEqual("ok", message);

            //===========================================================================

            var k = 9;
            var check = k == j;
            When.True(check, () => message = "nice");
            Assert.AreEqual("nice", message);
        }

        /// <summary>當條件為真的反向測試
        /// </summary>
        [TestMethod()]
        [Description("當條件為真的反向測試")]
        public void WhenTrue_ReverseTest()
        {
            var message = "";
            var j = 9;
            When.True(() => 100 == j, () => message = "ok");
            Assert.AreNotEqual("ok", message);

            //===========================================================================

            var k = 7788;
            var check = k == j;
            When.True(check, () => message = "nice");
            Assert.AreNotEqual("nice", message);
        }

        /// <summary>當條件為假的正向測試
        /// </summary>
        [TestMethod()]
        [Description("當條件為假的正向測試")]
        public void WhenFalse_Test()
        {
            var message = "";
            var j = 9;
            When.False(() => 123 == j, () => message = "ok");
            Assert.AreEqual("ok", message);

            //===========================================================================

            var check = 77 == j;
            When.False(check, () => message = "nice");
            Assert.AreEqual("nice", message);
        }

        /// <summary>當條件為假的反向測試
        /// </summary>
        [TestMethod()]
        [Description("當條件為假的反向測試")]
        public void WhenFalse_ReverseTest()
        {
            var message = "";
            var j = 9;
            When.False(() => 9 == j, () => message = "ok");
            Assert.AreNotEqual("ok", message);

            //===========================================================================

            var check = 9 == j;
            When.False(check, () => message = "nice");
            Assert.AreNotEqual("nice", message);
        }
    }
}
