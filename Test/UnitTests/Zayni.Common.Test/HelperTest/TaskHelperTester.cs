using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace ZayniFramework.Common.Test
{
    /// <summary>TaskHelper 的測試類別
    /// </summary>
    [TestClass()]
    [TestCategory("Common.Test")]
    public class TaskHelperTester
    {
        /// <summary>非同步作業延遲的測試。
        /// </summary>
        /// <returns></returns>
        [TestMethod()]
        [Description("非同步作業延遲的測試。")]
        public async Task GivenAnDelayedTask_WhenDelayUntilAsync_ThenShouldDelayTheAsyncTask()
        {
            var models = default(List<object>);
            var flag = default(bool);
            var worker = default(Task);

            GivenAnDelayedTask();
            await WhenDelayUntilAsync();
            ThenShouldDelayTheAsyncTask();

            void GivenAnDelayedTask()
            {
                flag = false;
                models = [];
                worker = new Task(async () =>
                {
                    while (true)
                    {
                        await Task.Delay(50);
                        models.Add(new object());
                    }
                });
            }

            async Task WhenDelayUntilAsync()
            {
                worker.Start();
                await TaskHelper.DelayUntilAsync(() => models.Count >= 10);
                flag = true;
            }

            void ThenShouldDelayTheAsyncTask()
            {
                flag.Should().BeTrue();
                models.Should().NotBeNullOrEmpty();
                models.Count.Should().BeGreaterOrEqualTo(10);
            }
        }

        /// <summary>非同步作業延遲的測試。
        /// </summary>
        /// <returns></returns>
        [TestMethod()]
        [Description("非同步作業延遲的測試。")]
        public async Task GivenAnDelayedTask_WhenDelayUntilAsyncWithTimeout_ThenShouldDelayTheAsyncTask()
        {
            var models = default(List<object>);
            var flag = default(bool);
            var worker = default(Task);

            GivenAnDelayedTask();
            await WhenDelayUntilAsyncWithTimeout();
            ThenShouldDelayTheAsyncTask();

            void GivenAnDelayedTask()
            {
                flag = false;
                models = [];
                worker = new Task(async () =>
                {
                    while (true)
                    {
                        // 故意等很久才加一筆資料
                        await Task.Delay(1000 * 5);
                        models.Add(new object());
                    }
                });
            }

            async Task WhenDelayUntilAsyncWithTimeout()
            {
                worker.Start();

                // 最久只延遲 100 毫秒
                await TaskHelper.DelayUntilAsync(() => models.Count >= 1000, timeoutMilliseconds: 100);
                flag = true;
            }

            void ThenShouldDelayTheAsyncTask()
            {
                flag.Should().BeTrue();
                models.Should().BeEmpty();
                models.Count.Should().BeLessThan(1000);
            }
        }
    }
}