﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZayniFramework.Common.Dynamic;


namespace ZayniFramework.Common.Test
{
    /// <summary>使用者 (ZayniFramework 框架動態物件)
    /// </summary>
    public class DynamicUser : BaseDynamicObject
    {
        /// <summary>使用者姓名 (這是一個編譯時期的屬性)
        /// </summary>
        public string Name { get; set; }
    }

    /// <summary>測試用的委派
    /// </summary>
    /// <returns></returns>
    public delegate string SaySomething2();

    /// <summary>動態物件的測試類別
    /// </summary>
    [TestClass()]
    public class DynamicObjectTester
    {
        /// <summary>
        /// </summary>
        /// <returns></returns>
        private static Result DoSomething()
        {
            dynamic result = new Result()
            {
                Success = true,
                Message = "Good"
            };

            result.Russia = "BelleClaire";
            result.Usa = "SashaGrey";
            return result;
        }

        /// <summary>測試 Zayni Framework 的 Result 物件的 DynamicObject 的特性
        /// </summary>
        [TestMethod()]
        [TestCategory("Common.Dynamic - BaseDynamicObject Test")]
        [Description("測試 Zayni Framework 的 Result 物件的 DynamicObject 的特性")]
        public void DynamicResultTest()
        {
            var r = DoSomething();
            Assert.IsTrue(r.Success);
            Assert.AreEqual("Good", r.Message);

            dynamic dr = r;
            Assert.AreEqual("BelleClaire", dr.Russia);
            Assert.AreEqual("SashaGrey", dr.Usa);
        }

        [TestMethod()]
        [Description("測試ZayniFramework 框架的基底DynamicObject，利用dynamic型別關鍵字宣告的測試")]
        public void DynamicObjectTest1()
        {
            dynamic user = new DynamicUser();   // 變數宣告為dynamic動態型別

            user.Name = "Sylvia";   // 編譯時期屬性
            user.Sex = "female";
            user.Age = 37;
            user.Say = (SaySomething2)(() =>
            {
                var sex = "female" == user.Sex ? "girl" : "boy";
                return string.Format("My name is {0}. I'm a {1}. I'm {2}. I love Pony very much.", user.Name, sex, user.Age);
            });

            string message = user.Say();

            Assert.IsTrue(message.IsNotNullOrEmpty());
            Assert.AreEqual("My name is Sylvia. I'm a girl. I'm 37. I love Pony very much.", message);
        }

        [TestMethod()]
        [Description("測試ZayniFramework 框架的基底DynamicObject，利用強型別關鍵字宣告的測試")]
        public void DynamicObjectTest2()
        {
            var user = new DynamicUser
            {
                Name = "Sylvia"   // 編譯時期屬性
            };   // 變數宣告為靜態強型別

            var isOk1 = user.SetMember("Sex", "female");
            Assert.IsTrue(isOk1);

            var isOk2 = user.SetMember("Age", 37);
            Assert.IsTrue(isOk2);

            var isOk3 = user.SetMember("Say",
                (SaySomething2)(() =>
                {
                    var sex = "female" == user.GetMember("Sex") + "" ? "girl" : "boy";
                    return string.Format("My name is {0}. I'm a {1}. I'm {2}. I love Pony very much.", user.Name, sex, user.GetMember("Age"));
                })
            );

            var isOk4 = user.SetMember("Test");
            Assert.IsTrue(isOk4);

            var handler = (SaySomething2)user.GetMember("Say");
            var message = handler();

            var some = user.GetMember("Test");
            Assert.IsNull(some);

            var none = user.GetMember("HHIJskldfji");     // 故意取得一個不存在的成員
            Assert.IsNull(none);

            Assert.IsTrue(message.IsNotNullOrEmpty());
            Assert.AreEqual("My name is Sylvia. I'm a girl. I'm 37. I love Pony very much.", message);
        }
    }
}
