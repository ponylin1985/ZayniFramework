﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using ZayniFramework.Common.Dynamic;


namespace ZayniFramework.Common.Test
{
    /// <summary>測試用的委派
    /// </summary>
    /// <param name="message"></param>
    /// <returns></returns>
    public delegate string SaySomething(string message);

    /// <summary>DynamicHelper 的測試類別
    /// </summary>
    [TestClass()]
    public class DynamicHelperTester
    {
        /// <summary>建立動態物件的綜合測試
        /// </summary>
        [TestMethod()]
        [Description("建立動態物件的綜合測試")]
        public void CreateDynamicObject_Test()
        {
            var model = new
            {
                Name = "Amy",
                Sex = 2,
                Birthday = new DateTime(1990, 3, 24),
                DoSomething = (Func<string>)(() => $"Hi my name is Amy.")
            };

            dynamic obj = DynamicHelper.CreateDynamicObject(model);

            Assert.AreEqual(model.Name, obj.Name);
            Assert.AreEqual(model.Sex, obj.Sex);
            Assert.AreEqual(model.Birthday, obj.Birthday);

            string result = obj.DoSomething();
            Assert.AreEqual("Hi my name is Amy.", result);

            bool success = DynamicHelper.BindMethod(obj, "Call", (Func<string, string>)(msg => $"Hey hey hey, {msg}"));
            Assert.IsTrue(success);

            string r = obj.Call("123");
            Assert.AreEqual("Hey hey hey, 123", r);

            // ========================================================================

            var source = new MemberModel()
            {
                Name = "Katie"
            };

            dynamic d = DynamicHelper.CreateDynamicObject(source);
            Assert.AreEqual(source.Name, d.Name);

            result = d.SayHello("qqq");
            Assert.AreEqual("Hello, my name is Katie. qqq", result);

            success = DynamicHelper.BindMethod(d, "GetSecretNumber", (Func<int, string>)(secretNumber => $"Cool, {secretNumber + 1}"));
            Assert.IsTrue(success);

            r = d.GetSecretNumber(23);
            Assert.AreEqual("Cool, 24", r);
        }

        /// <summary>ZayniFramework 框架的Result物件的動態成員測試
        /// </summary>
        [TestMethod()]
        [Description("ZayniFramework 框架的Result物件的動態成員測試")]
        public void ResultTest()
        {
            var r = new Result<string>()
            {
                Data = "aaa"
            };

            dynamic d = (dynamic)r;
            d.HeHeHe = "hahah 我是完全動態的屬性啊!!!";

            Assert.AreEqual("aaa", d.Data);
            Assert.AreEqual("hahah 我是完全動態的屬性啊!!!", d.HeHeHe);

            //====================================================================

            var x = new Result()
            {
                Message = "bbb"
            };

            dynamic y = (dynamic)x;
            y.IamReallyCool = "我他媽的屌翻!";

            Assert.AreEqual("bbb", y.Message);
            Assert.AreEqual("我他媽的屌翻!", y.IamReallyCool);
        }

        /// <summary>動態新增屬性的測試
        /// </summary>
        [TestMethod()]
        [Description("動態新增屬性的測試")]
        public void BindPropertyTest()
        {
            dynamic obj = DynamicHelper.CreateDynamicObject();
            Assert.IsNotNull(obj);

            bool isOk = DynamicHelper.BindProperty(obj, "UserName", "Sylvia");    // 利用BindProperty新增動態物件的屬性
            Assert.IsTrue(isOk);

            string name = obj.UserName;
            Assert.AreEqual("Sylvia", name);

            bool isOk2 = DynamicHelper.BindProperty(obj, "UserName", "Vivian");   // 利用BindProperty修改動態物件的屬性

            Assert.IsTrue(isOk2);

            string name2 = obj.UserName;
            Assert.AreEqual("Vivian", name2);
        }

        /// <summary>動態新增方法的測試
        /// </summary>
        [TestMethod()]
        [Description("動態新增方法的測試")]
        public void BindMethodTest()
        {
            dynamic obj = DynamicHelper.CreateDynamicObject();
            Assert.IsNotNull(obj);

            bool isOk = DynamicHelper.BindProperty(obj, "UserName", "Sylvia");
            Assert.IsTrue(isOk);

            string name = obj.UserName;
            Assert.AreEqual("Sylvia", name);

            string handler(string m)
            {
                return string.Format("Hi, my name is {0}. {1}", obj.UserName, m);
            }

            bool isOk2 = DynamicHelper.BindMethod<SaySomething>(obj, "IntroduceYourself", (SaySomething)handler);
            Assert.IsTrue(isOk2);

            string message = obj.IntroduceYourself("Nice to meet you.");
            Assert.IsTrue(message.IsNotNullOrEmpty());
            Assert.AreEqual("Hi, my name is Sylvia. Nice to meet you.", message);
        }

        /// <summary>動態新增方法的測試2
        /// </summary>
        [TestMethod()]
        [Description("動態新增方法的測試2")]
        public void BindMethodTest2()
        {
            dynamic obj = DynamicHelper.CreateDynamicObject();
            Assert.IsNotNull(obj);

            bool isOk = DynamicHelper.BindProperty(obj, "UserName", "Sylvia");
            Assert.IsTrue(isOk);

            string name = obj.UserName;
            Assert.AreEqual("Sylvia", name);

            bool isOk2 = DynamicHelper.BindMethod(obj, "IntroduceYourself", (SaySomething)((m) =>
            {
                return string.Format("Hi, my name is {0}. {1}", obj.UserName, m);
            }));

            Assert.IsTrue(isOk2);

            string message = obj.IntroduceYourself("Nice to meet you.");
            Assert.IsTrue(message.IsNotNullOrEmpty());
            Assert.AreEqual("Hi, my name is Sylvia. Nice to meet you.", message);
        }

        /// <summary>動態移除屬性的測試
        /// </summary>
        [TestMethod()]
        [Description("動態移除屬性的測試")]
        public void UnbindPropertyTest()
        {
            #region 先動態新增一個屬性到動態物件上

            dynamic obj = DynamicHelper.CreateDynamicObject();
            Assert.IsNotNull(obj);

            bool isOk = DynamicHelper.BindProperty(obj, "UserName", "Sylvia");
            Assert.IsTrue(isOk);

            #endregion 先動態新增一個屬性到動態物件上

            #region 測試用的DynamicObject把屬性動態移除掉

            string name = obj.UserName;
            Assert.AreEqual("Sylvia", name);

            bool isUnbindOK = DynamicHelper.UnbindProperty(obj, "UserName");
            Assert.IsTrue(isUnbindOK);

            var flag = false;

            try
            {
                string strName = obj.UserName;
            }
            catch (Exception)
            {
                flag = true;
            }

            Assert.IsTrue(flag);

            #endregion 測試用的DynamicObject把屬性動態移除掉
        }

        /// <summary>動態移除屬性的測試 - 測試故意傳入的動態物件不是ZayniFramework 框架產生的DynamicObject
        /// </summary>
        [TestMethod()]
        [Description("動態移除屬性的測試 - 測試故意傳入的動態物件不是ZayniFramework 框架產生的DynamicObject")]
        public void UnbindPropertyOppsiteTest1()
        {
            dynamic test = new
            {
                MyUserName = "Vivian"
            };

            bool isUnbindOK2 = DynamicHelper.UnbindProperty(test, "UserName");
            Assert.IsFalse(isUnbindOK2);      // 20131213 Pony Says: .Commom.DynamicObject的API方法，只能針對ZayniFramework 框架產生出的DynamicObject進行動態移除屬性!!!
        }

        /// <summary>動態移除屬性的測試 - 測試故意嘗試移除一個不存在的屬性
        /// </summary>
        [TestMethod()]
        [Description("動態移除屬性的測試 - 測試故意嘗試移除一個不存在的屬性")]
        public void UnbindPropertyOppsiteTest2()
        {
            #region 先動態新增一個屬性到動態物件上

            dynamic obj = DynamicHelper.CreateDynamicObject();
            Assert.IsNotNull(obj);

            bool isOk = DynamicHelper.BindProperty(obj, "UserName", "Sylvia");
            Assert.IsTrue(isOk);

            #endregion 先動態新增一個屬性到動態物件上

            bool isUnbindOk = DynamicHelper.UnbindProperty(obj, "Hello");
            Assert.IsTrue(isUnbindOk);
        }

        /// <summary>動態移除方法的測試
        /// </summary>
        [TestMethod()]
        [Description("動態移除方法的測試")]
        public void UnbindMethodTest()
        {
            #region 先動態的新增一個方法到目標動態物件上

            static string handler(string msg)
            {
                return $"訊息是: {msg}";
            }

            dynamic obj = DynamicHelper.CreateDynamicObject();

            bool isBindOk = DynamicHelper.BindMethod<SaySomething>(obj, "Say", (SaySomething)handler);
            Assert.IsTrue(isBindOk);

            string message = obj.Say("JJ");
            Assert.AreEqual("訊息是: JJ", message);

            #endregion 先動態的新增一個方法到目標動態物件上

            #region 測試用的DynamicObject把方法移除掉

            bool isUnbindOk = DynamicHelper.UnbindMethod(obj, "Say");
            Assert.IsTrue(isUnbindOk);

            var flag = false;

            try
            {
                obj.Say();
            }
            catch (Exception)
            {
                flag = true;
            }

            Assert.IsTrue(flag);

            #endregion 測試用的DynamicObject把方法移除掉
        }

        /// <summary>動態移除方法的測試 - 測試故意嘗試移除一個不存在的方法
        /// </summary>
        [TestMethod()]
        [Description("動態移除方法的測試 - 測試故意嘗試移除一個不存在的方法")]
        public void UnbindMethodOppsiteTest()
        {
            dynamic obj = DynamicHelper.CreateDynamicObject();
            bool isUnbindSuccess = DynamicHelper.UnbindMethod(obj, "Hello");
            Assert.IsTrue(isUnbindSuccess);
        }

        /// <summary>檢查動態物件是否包含指定屬性的測試
        /// </summary>
        [TestMethod()]
        [Description("檢查動態物件是否包含指定屬性的測試")]
        public void HasPropertyTest()
        {
            #region 先動態新增一個屬性到動態物件上

            dynamic obj = DynamicHelper.CreateDynamicObject();
            Assert.IsNotNull(obj);

            bool isOk = DynamicHelper.BindProperty(obj, "UserName", "Sylvia");
            Assert.IsTrue(isOk);

            #endregion 先動態新增一個屬性到動態物件上

            bool has = DynamicHelper.HasProperty(obj, "UserName");
            Assert.IsTrue(has);

            bool has2 = DynamicHelper.HasProperty(obj, "JJJ");
            Assert.IsFalse(has2);
        }

        /// <summary>檢查動態物件是否包含指定方法的測試
        /// </summary>
        [TestMethod()]
        [Description("檢查動態物件是否包含指定方法的測試")]
        public void HasMethodTest()
        {
            #region 先動態的新增一個方法到目標動態物件上

            static string handler(string msg)
            {
                return $"訊息是: {msg}";
            }

            dynamic obj = DynamicHelper.CreateDynamicObject();

            bool isBindOk = DynamicHelper.BindMethod<SaySomething>(obj, "Say", (SaySomething)handler);
            Assert.IsTrue(isBindOk);

            string message = obj.Say("JJ");
            Assert.AreEqual("訊息是: JJ", message);

            #endregion 先動態的新增一個方法到目標動態物件上

            bool has = DynamicHelper.HasMethod(obj, "Say");
            Assert.IsTrue(has);

            bool has2 = DynamicHelper.HasMethod(obj, "JJJ");
            Assert.IsFalse(has2);
        }

        /// <summary>對動態物件進行迭代的測試
        /// </summary>
        [TestMethod()]
        [Description("對動態物件進行迭代的測試")]
        public void ForEachTest()
        {
            dynamic model = DynamicHelper.CreateDynamicObject();

            string[] names = ["A1", "A2", "A3", "A4"];

            foreach (var name in names)
            {
                DynamicHelper.BindProperty(model, name, "VVV");

                if ("A3" == name)
                {
                    DynamicHelper.BindProperty(model, name, "VVV3");
                }
            }

            DynamicHelper.BindMethod<SaySomething>(model, "Say", (SaySomething)((m) =>
            {
                return "Hello " + m;
            }));

            DynamicHelper.BindMethod<SaySomething>(model, "SaySomething", (SaySomething)((m) =>
            {
                return "Hi " + m;
            }));

            var flag = false;
            var flag2 = false;

            var result = "";
            var result2 = "";

            // 20131213 Pony Says: 請注意，可以對動態物件進行迭代，但要非常小心成員值的型別，另外，無法在迭代過程中修改成員的屬性值!
            DynamicHelper.ForEach(model, (DynamicEachHandler)((string name, dynamic value) =>
            {
                if ("A2" == name)
                {
                    flag = true;
                }

                if ("VVV3" == value + "")
                {
                    flag2 = true;
                }

                if ("Say" == name)
                {
                    result = value("Sylvia");
                }

                if (Reflector.IsThatDelegate(value) && "SaySomething" == name)
                {
                    result2 = value("Vivian");
                }
            }));

            Assert.IsTrue(flag);
            Assert.IsTrue(flag2);
            Assert.AreEqual("Hello Sylvia", result);
            Assert.AreEqual("Hi Vivian", result2);
        }
    }
}
