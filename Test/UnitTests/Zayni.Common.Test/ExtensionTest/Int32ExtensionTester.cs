using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace ZayniFramework.Common.Test
{
    /// <summary>Int32 或 Int32? 擴充方法的測試類別
    /// </summary>
    [TestClass()]
    [TestCategory("Common.Test")]
    public class Int32ExtensionTester
    {
        /// <summary>給定 null 的 Int32 數值，檢查是否為 null 或是 .NET 預設值的測試。
        /// </summary>
        [TestMethod()]
        [Description("對於 Nullable Struct 進行是否為 null 或 .NET 預設值的測試。")]
        public void GivenNullOfInt32_WhenCheckIsNullOrDefault_ThenShouldRetriveTrue()
        {
            GivenNullOfInt32();
            WhenCheckIsNullOrDefault();
            ThenShouldRetriveTrue();

            int? source;
            void GivenNullOfInt32() => source = null;
            bool result;
            void WhenCheckIsNullOrDefault() => result = source.IsNullOrDefault();
            void ThenShouldRetriveTrue() => result.Should().BeTrue();

            int? source1 = null;
            _ = source1.IsNullOrDefault().Should().BeTrue();
            _ = source1.IsNotNullOrDefault().Should().BeFalse();

            int? source2 = default(int);
            _ = source2.IsNullOrDefault().Should().BeTrue();
            _ = source2.IsNotNullOrDefault().Should().BeFalse();

            int? source3 = 0;
            _ = source3.IsNullOrDefault().Should().BeTrue();
            _ = source3.IsNotNullOrDefault().Should().BeFalse();

            int? source4 = -234;
            _ = source4.IsNullOrDefault().Should().BeFalse();
            _ = source4.IsNotNullOrDefault().Should().BeTrue();
        }

        /// <summary>給定 null 的 Int32 數值，檢查是否為 null 或是 .NET 預設值的測試。
        /// </summary>
        [TestMethod()]
        [Description("對於 Nullable Struct 進行是否為 null 或 .NET 預設值的測試。")]
        public void GivenNullOfInt32_WhenCheckIsNotNullOrDefault_ThenShouldRetriveFalse()
        {
            GivenNullOfInt32();
            WhenCheckIsNotNullOrDefault();
            ThenShouldRetriveFalse();

            int? source;
            void GivenNullOfInt32() => source = null;
            bool result;
            void WhenCheckIsNotNullOrDefault() => result = source.IsNotNullOrDefault();
            void ThenShouldRetriveFalse() => result.Should().BeFalse();
        }

        /// <summary>給定 0 的 Int32 數值，檢查是否為 null 或是 .NET 預設值的測試。
        /// </summary>
        [TestMethod()]
        [Description("對於 Nullable Struct 進行是否為 null 或 .NET 預設值的測試。")]
        public void GivenZeroOfInt32_WhenCheckIsNullOrDefault_ThenShouldRetriveTrue()
        {
            GivenZeroOfInt32();
            WhenCheckIsNullOrDefault();
            ThenShouldRetriveTrue();

            int? source;
            void GivenZeroOfInt32() => source = 0;
            bool result;
            void WhenCheckIsNullOrDefault() => result = source.IsNullOrDefault();
            void ThenShouldRetriveTrue() => result.Should().BeTrue();
        }

        /// <summary>給定 .NET default 的 Int32 數值，檢查是否為 null 或是 .NET 預設值的測試。
        /// </summary>
        [TestMethod()]
        [Description("對於 Nullable Struct 進行是否為 null 或 .NET 預設值的測試。")]
        public void GivenDefaultOfInt32_WhenCheckIsNullOrDefault_ThenShouldRetriveTrue()
        {
            GivenDefaultOfInt32();
            WhenCheckIsNullOrDefault();
            ThenShouldRetriveTrue();

            int? source;
            void GivenDefaultOfInt32() => source = default(int);
            bool result;
            void WhenCheckIsNullOrDefault() => result = source.IsNullOrDefault();
            void ThenShouldRetriveTrue() => result.Should().BeTrue();
        }
    }
}