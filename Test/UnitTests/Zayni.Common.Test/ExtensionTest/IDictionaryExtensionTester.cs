﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using ZayniFramework.Common;


namespace ZayniFramework.Common.Test.Extension.Test
{
    /// <summary>字典擴充方法的測試類別
    /// </summary>
    [TestClass()]
    [TestCategory("Common.Test")]
    public class IDictionaryExtensionTester
    {
        /// <summary>擷取出字典物件中的 Key 集合的測試
        /// </summary>
        [TestMethod()]
        [Description("擷取出字典物件中的 Key 集合的測試")]
        public void FetchKeysTest()
        {
            var dict = new Dictionary<string, string>
            {
                { "a", "1" },
                { "b", "2" },
                { "c", "3" },
                { "d", "4" },
                { "e", "5" }
            };

            var list = new List<string>()
            {
                "a", "b", "c"
            };

            var result = dict.FetchKeys(list).ToList();
            Assert.IsTrue(0 != result.Count);
        }

        /// <summary>擷取出字典物件中對應 Key 的 Values 集合的測試
        /// </summary>
        [TestMethod()]
        [Description("擷取出字典物件中對應 Key 的 Values 集合的測試")]
        public void FetchValuesTest()
        {
            var dict = new Dictionary<string, string>
            {
                { "a", "1" },
                { "b", "2" },
                { "c", "3" },
                { "d", "4" },
                { "e", "5" }
            };

            var list = new List<string>()
            {
                "a", "b", "c"
            };

            var result = dict.FetchValues<string>(list).ToList();
            Assert.IsTrue(0 != result.Count);
        }
    }
}
