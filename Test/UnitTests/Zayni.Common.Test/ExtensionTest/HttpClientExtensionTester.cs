﻿// 2024-03-09 發現目前 Linode 上的 K8S pod 有問題，無法正常運作，因此暫時無法進行測試。
// using FluentAssertions;
// using Microsoft.VisualStudio.TestTools.UnitTesting;
// using Newtonsoft.Json;
// using System;
// using System.Collections.Generic;
// using System.Diagnostics;
// using System.Net;
// using System.Text.Json.Serialization;
// using System.Threading.Tasks;


// namespace ZayniFramework.Common.Test
// {
//     // dotnet test ./Test/UnitTests/Zayni.Common.Test/Zayni.Common.Test.csproj --filter ZayniFramework.Common.Test.HttpClientExtensionTester
//     /// <summary>HttpClientExtension 與 HttpClientFactory 的測試類別
//     /// </summary>
//     [TestClass()]
//     [TestCategory("Common.Test")]
//     public class HttpClientExtensionTester
//     {
//         /// <summary>HttpClientExtension 發送 Http GET 請求測試
//         /// </summary>
//         [TestMethod()]
//         [Description("HttpClientExtension 發送 Http GET 請求測試")]
//         public async Task ExecuteGetAsync_Test()
//         {
//             var httpRetryOption = new RetryOption
//             {
//                 EnableRetry = true,
//                 RetryFrequencies =
//                 [
//                     TimeSpan.FromSeconds( 1 ),
//                     TimeSpan.FromSeconds( 1.5 ),
//                 ],
//             };

//             var baseUrl = ConfigManager.GetAppSettings()["WebAPIUrl"];
//             var httpClient = HttpClientFactory.Create(baseUrl, 5);
//             httpClient.DefaultRequestVersion = new Version(2, 0);
//             Assert.IsNotNull(httpClient);

//             var random = new Random(Guid.NewGuid().GetHashCode());

//             var request = new HttpRequest()
//             {
//                 Path = "values",
//                 QueryStrings =
//                 [
//                     new KeyValuePair<string, string>( "msg", "HttpClientExtensionTester sending http GET." ),
//                     new KeyValuePair<string, string>( "number", random.Next( 500 ) + "" ),
//                 ]
//             };

//             for (var i = 0; i < 5; i++)
//             {
//                 var g = await httpClient.ExecuteGetAsync(request, consoleOutputLog: false, httpRetryOption: httpRetryOption);
//                 Assert.IsTrue(g.Success);
//                 Assert.IsTrue(g.StatusCode == HttpStatusCode.OK);
//                 await Command.StdoutAsync(g.Data);
//                 Debug.Print(g.Data);

//                 dynamic r = JsonConvert.DeserializeObject(g.Data);
//                 Assert.IsNotNull(r);
//                 Assert.IsTrue((bool)r.success);

//                 await Task.Delay(50);
//             }

//             for (var i = 0; i < 3; i++)
//             {
//                 var g = await httpClient.ExecuteHttp2GetAsync<Result<MagicDTO>>(request, consoleOutputLog: false, httpRetryOption: httpRetryOption);
//                 Assert.IsTrue(g.Success);
//                 Assert.IsTrue(g.StatusCode == HttpStatusCode.OK);
//                 await Command.StdoutAsync(g.Data);
//                 Debug.Print(g.Data);

//                 var responseDTO = g.Payload;
//                 Assert.IsNotNull(responseDTO);
//                 Assert.IsTrue(responseDTO.Success);
//                 await Task.Delay(50);
//             }

//             // ===================

//             baseUrl += "values/";
//             httpClient = HttpClientFactory.Create(baseUrl, 5);

//             request = new HttpRequest()
//             {
//                 // 故意測試 baseUrl 已經包含 request.Path 的情境，因此 request.Path 傳入 null 或空字串的測試情境。
//                 Path = "",
//                 QueryStrings =
//                 [
//                     new KeyValuePair<string, string>( "msg", "HttpClientExtensionTester sending http GET." ),
//                     new KeyValuePair<string, string>( "number", random.Next( 500 ) + "" ),
//                 ]
//             };

//             for (var i = 0; i < 5; i++)
//             {
//                 // 故意測試 baseUrl 已經包含 request.Path 的情境，因此 request.Path 傳入 null 或空字串的測試情境。
//                 When.True(3 == i, () => request.Path = null);

//                 var g = await httpClient.ExecuteGetAsync(request, consoleOutputLog: false, httpRetryOption: httpRetryOption);
//                 Assert.IsTrue(g.Success);
//                 Assert.IsTrue(g.StatusCode == HttpStatusCode.OK);
//                 await Command.StdoutAsync(g.Data);
//                 Debug.Print(g.Data);

//                 dynamic r = JsonConvert.DeserializeObject(g.Data);
//                 Assert.IsNotNull(r);
//                 Assert.IsTrue((bool)r.success);

//                 await Task.Delay(50);
//             }
//         }

//         /// <summary>HttpClientExtension 發送 Http GET 請求測試
//         /// </summary>
//         [TestMethod()]
//         [Description("HttpClientExtension 發送 Http GET 請求測試")]
//         public async Task ExecuteGetAsync_RetrieveErrorDetail_Test()
//         {
//             var httpRetryOption = new RetryOption
//             {
//                 EnableRetry = true,
//                 RetryFrequencies =
//                 [
//                     TimeSpan.FromSeconds( 1 ),
//                     TimeSpan.FromSeconds( 1.5 ),
//                 ],
//             };

//             var baseUrl = ConfigManager.GetAppSettings()["WebAPIUrl"];
//             var httpClient = HttpClientFactory.Create(baseUrl, 5);
//             httpClient.DefaultRequestVersion = new Version(2, 0);
//             Assert.IsNotNull(httpClient);

//             var random = new Random(Guid.NewGuid().GetHashCode());
//             var request = new HttpRequest()
//             {
//                 Path = "values123",     // wrong path
//                 QueryStrings =
//                 [
//                     new KeyValuePair<string, string>( "msg", "HttpClientExtensionTester sending http GET." ),
//                     new KeyValuePair<string, string>( "number", random.Next( 500 ) + "" ),
//                 ]
//             };

//             var g = await httpClient.ExecuteGetAsync(request, consoleOutputLog: false, httpRetryOption: httpRetryOption);
//             Assert.IsFalse(g.Success);
//             Assert.IsTrue(g.StatusCode != HttpStatusCode.OK);
//             await Command.StdoutAsync(g.Data);
//             Debug.Print(g.Data);

//             g.HttpResponseMessaage.Should().NotBeNull();
//         }

//         /// <summary>HttpClientExtension 發送 Http POST JSON 請求測試
//         /// </summary>
//         [TestMethod()]
//         [Description("HttpClientExtension 發送 Http POST JSON 請求測試")]
//         public async Task ExecutePostJsonAsync_Test()
//         {
//             var httpRetryOption = new RetryOption
//             {
//                 EnableRetry = true,
//                 RetryFrequencies =
//                 [
//                     TimeSpan.FromSeconds( 1 ),
//                     TimeSpan.FromSeconds( 1.5 ),
//                     TimeSpan.FromSeconds( 1.5 ),
//                 ],
//             };

//             var baseUrl = ConfigManager.GetAppSettings()["WebAPIUrl"];
//             var httpClient = HttpClientFactory.Create(baseUrl, 5);
//             httpClient.DefaultRequestVersion = new Version(2, 0);
//             Assert.IsNotNull(httpClient);

//             var random = new Random(Guid.NewGuid().GetHashCode());

//             var reqDTO = new
//             {
//                 someMsg = "HttpClientExtensionTester sending http POST.",
//                 someDate = DateTime.UtcNow,
//                 magicNumber = random.Next(300),
//                 luckyNumber = random.Next(500),
//                 isSuperXMan = true
//             };

//             var reqJson = JsonConvert.SerializeObject(reqDTO);

//             var request = new HttpRequest()
//             {
//                 Path = "/values/test",
//                 Body = reqJson
//             };

//             for (var i = 0; i < 5; i++)
//             {
//                 var p = await httpClient.ExecuteHttp2PostJsonAsync(request, consoleOutputLog: false, httpRetryOption: httpRetryOption);
//                 await Command.StdoutAsync($"VVV ExecuteHttp2PostJsonAsync_Test{i} ResponseStatusCode: {p.HttpResponseMessaage.StatusCode}");
//                 await Command.StdoutAsync($"VVV ExecuteHttp2PostJsonAsync_Test{i} ResponseReasonPhrase: {p.HttpResponseMessaage.ReasonPhrase}");
//                 await Command.StdoutAsync($"VVV ExecuteHttp2PostJsonAsync_Test{i} ResponseMessage: {p.Message}");

//                 Assert.IsTrue(p.Success);
//                 Assert.IsTrue(p.StatusCode == HttpStatusCode.OK);
//                 await Command.StdoutAsync(p.Data);
//                 Debug.Print(p.Data);

//                 dynamic r = JsonConvert.DeserializeObject(p.Data);
//                 Assert.IsNotNull(r);
//                 Assert.IsTrue((bool)r.success);
//                 await Task.Delay(50);
//             }

//             // =================

//             var requestDTO = new SomethingDTO
//             {
//                 SomeMessage = "HttpClientExtensionTester sending http POST (strong DTO type).",
//                 SomeDate = DateTime.UtcNow,
//                 SomeMagicNumber = random.Next(20),
//                 LuckyNumber = random.Next(50),
//                 IsSuperMan = true
//             };

//             request = new HttpRequest()
//             {
//                 Path = "/values/test"
//             };

//             for (var i = 0; i < 3; i++)
//             {
//                 var p = await httpClient.ExecuteHttp2PostJsonAsync<SomethingDTO, Result<MagicDTO>>(request, requestDTO, consoleOutputLog: false, httpRetryOption: httpRetryOption);
//                 Assert.IsTrue(p.Success);
//                 Assert.IsTrue(p.StatusCode == HttpStatusCode.OK);
//                 await Command.StdoutAsync(p.Data);
//                 Debug.Print(p.Data);

//                 var responseDTO = p.Payload;
//                 Assert.IsNotNull(responseDTO);
//                 Assert.IsNotNull(responseDTO.Data);
//                 Assert.IsTrue(responseDTO.Success);
//                 await Task.Delay(50);
//             }

//             // =================

//             baseUrl += "values/test/";
//             httpClient = HttpClientFactory.Create(baseUrl, 5);

//             request = new HttpRequest()
//             {
//                 // 故意測試 baseUrl 已經包含 request.Path 的情境，因此 request.Path 傳入 null 或空字串的測試情境。
//                 Path = null,
//                 Body = reqJson
//             };

//             for (var i = 0; i < 5; i++)
//             {
//                 // 故意測試 baseUrl 已經包含 request.Path 的情境，因此 request.Path 傳入 null 或空字串的測試情境。
//                 When.True(3 == i, () => request.Path = string.Empty);

//                 var p = await httpClient.ExecutePostJsonAsync(request, consoleOutputLog: false, httpRetryOption: httpRetryOption);
//                 Assert.IsTrue(p.Success);
//                 Assert.IsTrue(p.StatusCode == HttpStatusCode.OK);
//                 await Command.StdoutAsync(p.Data);
//                 Debug.Print(p.Data);

//                 dynamic r = JsonConvert.DeserializeObject(p.Data);
//                 Assert.IsNotNull(r);
//                 Assert.IsTrue((bool)r.success);
//                 await Task.Delay(50);
//             }
//         }

//         /// <summary>HttpClientExtension 發送 Http POST JSON 請求測試
//         /// </summary>
//         [TestMethod()]
//         [Description("HttpClientExtension 發送 Http POST JSON 請求測試")]
//         public async Task ExecuteHttp2PostJsonAsync_Test()
//         {
//             var httpRetryOption = new RetryOption
//             {
//                 EnableRetry = true,
//                 RetryFrequencies =
//                 [
//                     TimeSpan.FromSeconds( 1 ),
//                     TimeSpan.FromSeconds( 1.5 ),
//                     TimeSpan.FromSeconds( 1.5 ),
//                 ],
//             };

//             // 故意測試 baseUrl 傳入一個完整的 API endpoint URL 位置，包含 api domain 與 api path。}"
//             var baseUrl = $"{ConfigManager.GetAppSettings()["WebAPIUrl"]}values/test";
//             var httpClient = HttpClientFactory.Create(baseUrl, 5);
//             httpClient.DefaultRequestVersion = new Version(2, 0);

//             var random = new Random(Guid.NewGuid().GetHashCode());

//             var requestDTO = new SomethingDTO
//             {
//                 SomeMessage = "HttpClientExtensionTester sending http POST (strong DTO type).",
//                 SomeDate = DateTime.UtcNow,
//                 SomeMagicNumber = random.Next(20),
//                 LuckyNumber = random.Next(50),
//                 IsSuperMan = true
//             };

//             var request = new HttpRequest()
//             {
//                 Path = ""   // 當 HttpClient 的 BaseAddress 已經傳入一個完整的 api endpoint URL 位置，這邊的 Path 就要給定空字串。
//             };

//             var p = await httpClient.ExecuteHttp2PostJsonAsync<SomethingDTO, Result<MagicDTO>>(request, requestDTO, withHttp2: true, httpRetryOption: httpRetryOption);
//             await Command.StdoutAsync($"VVV ExecuteHttp2PostJsonAsync_Test ResponseStatusCode: {p.HttpResponseMessaage.StatusCode}");
//             await Command.StdoutAsync($"VVV ExecuteHttp2PostJsonAsync_Test ResponseReasonPhrase: {p.HttpResponseMessaage.ReasonPhrase}");
//             await Command.StdoutAsync($"VVV ExecuteHttp2PostJsonAsync_Test ResponseMessage: {p.Message}");

//             p.Success.Should().BeTrue();
//             p.StatusCode.Should().Be(HttpStatusCode.OK);
//             await Command.StdoutAsync(p.Data);
//             Debug.Print(p.Data);

//             var responseDTO = p.Payload;
//             responseDTO.Should().NotBeNull();
//             responseDTO.Data.Should().NotBeNull();
//             responseDTO.Success.Should().BeTrue();
//         }

//         /// <summary>HttpClientExtension 發送 Http POST JSON 請求測試
//         /// </summary>
//         [TestMethod()]
//         [Description("HttpClientExtension 發送 Http POST JSON 請求測試")]
//         public async Task ExecutePostJsonAsync_RetrieveErrorDetail_Test()
//         {
//             var httpRetryOption = new RetryOption
//             {
//                 EnableRetry = true,
//                 RetryFrequencies =
//                 [
//                     TimeSpan.FromMilliseconds( 100 ),
//                 ],
//             };

//             var baseUrl = ConfigManager.GetAppSettings()["WebAPIUrl"];
//             var httpClient = HttpClientFactory.Create(baseUrl, 5);
//             httpClient.DefaultRequestVersion = new Version(2, 0);
//             Assert.IsNotNull(httpClient);

//             var random = new Random(Guid.NewGuid().GetHashCode());

//             var reqDTO = new
//             {
//                 someMsg = "HttpClientExtensionTester sending http POST.",
//                 someDate = DateTime.UtcNow,
//                 magicNumber = random.Next(300),
//                 luckyNumber = random.Next(500),
//                 isSuperXMan = true
//             };

//             var reqJson = JsonConvert.SerializeObject(reqDTO);

//             var request = new HttpRequest()
//             {
//                 Path = "/values/test123123",
//                 Body = reqJson
//             };

//             var p = await httpClient.ExecuteHttp2PostJsonAsync(request, consoleOutputLog: false, httpRetryOption: httpRetryOption);
//             Assert.IsFalse(p.Success);
//             Assert.IsTrue(p.StatusCode != HttpStatusCode.OK);
//             await Command.StdoutAsync(p.Data);
//             Debug.Print(p.Data);

//             p.HttpResponseMessaage.Should().NotBeNull();
//         }

//         /// <summary>HttpClientExtension 的 Execute 擴充方法測試
//         /// </summary>
//         [TestMethod()]
//         [Description("HttpClientExtension 的 Execute 擴充方法測試")]
//         public async Task HttpClientExtension_Test()
//         {
//             var httpRetryOption = new RetryOption
//             {
//                 EnableRetry = true,
//                 RetryFrequencies =
//                 [
//                     TimeSpan.FromSeconds( 1 ),
//                     TimeSpan.FromSeconds( 1.5 ),
//                     TimeSpan.FromSeconds( 1.5 ),
//                 ],
//             };

//             var baseUrl = ConfigManager.GetAppSettings()["WebAPIUrl"];
//             var httpClient = HttpClientFactory.Create(baseUrl, 5);
//             Assert.IsNotNull(httpClient);

//             var random = new Random(Guid.NewGuid().GetHashCode());

//             #region 新增資料測試 Http POST Test

//             var model1 = new
//             {
//                 name = $"UnitTest-{RandomTextHelper.Create(10)}",
//                 sex = 0,
//                 age = 25,
//                 birthday = new DateTime(1998, 3, 27),
//                 is_vip = true,
//                 is_good = true
//             };

//             var request = new HttpRequest()
//             {
//                 Path = "/users",
//                 Body = JsonConvert.SerializeObject(model1)
//             };

//             var c = await httpClient.ExecuteHttp2PostJsonAsync(request, consoleOutputLog: false, httpRetryOption: httpRetryOption);
//             await Command.StdoutAsync($"VVV HttpClientExtension_Test ResponseStatusCode: {c.HttpResponseMessaage.StatusCode}");
//             await Command.StdoutAsync($"VVV HttpClientExtension_Test ResponseReasonPhrase: {c.HttpResponseMessaage.ReasonPhrase}");
//             await Command.StdoutAsync($"VVV HttpClientExtension_Test ResponseMessage: {c.Message}");

//             Assert.IsTrue(c.Success);
//             Assert.IsTrue(c.StatusCode == HttpStatusCode.OK);
//             await Command.StdoutAsync(c.Data);
//             Debug.Print(c.Data);
//             dynamic u1 = JsonConvert.DeserializeObject(c.Data);
//             await Task.Delay(100);

//             // ===============

//             var model2 = new
//             {
//                 name = $"UnitTest-{RandomTextHelper.Create(10)}",
//                 sex = 1,
//                 age = 30,
//                 birthday = new DateTime(1998, 3, 27),
//                 is_vip = false,
//                 is_good = true
//             };

//             request = new HttpRequest()
//             {
//                 Path = "users",
//                 Body = JsonConvert.SerializeObject(model2)
//             };

//             var c2 = await httpClient.ExecuteHttp2PostJsonAsync(request, withHttp2: true, consoleOutputLog: false, httpRetryOption: httpRetryOption);
//             Assert.IsTrue(c2.Success);
//             Assert.IsTrue(c2.StatusCode == HttpStatusCode.OK);
//             await Command.StdoutAsync(c2.Data);
//             Debug.Print(c2.Data);
//             dynamic u2 = JsonConvert.DeserializeObject(c2.Data);
//             await Task.Delay(100);

//             #endregion 新增資料測試 Http POST Test

//             #region 查詢資料測試 Http GET Test

//             request = new HttpRequest()
//             {
//                 Path = "users",
//                 QueryStrings =
//                 [
//                     new KeyValuePair<string, string>( "sex", "0" ),
//                 ]
//             };

//             var g = await httpClient.ExecuteHttp2GetAsync(request, consoleOutputLog: false, httpRetryOption: httpRetryOption);
//             Assert.IsTrue(g.Success);
//             Assert.IsTrue(g.StatusCode == HttpStatusCode.OK);
//             await Command.StdoutAsync(g.Data);
//             Debug.Print(g.Data);

//             dynamic r = JsonConvert.DeserializeObject(g.Data);
//             Assert.IsNotNull(r);
//             Assert.IsTrue((bool)r.success);

//             await Task.Delay(100);

//             // ========

//             request = new HttpRequest()
//             {
//                 Path = "users",
//                 QueryStrings =
//                 [
//                     new KeyValuePair<string, string>( "sex", "1" ),
//                 ]
//             };

//             var q = await httpClient.ExecuteGetAsync(request, consoleOutputLog: false, httpRetryOption: httpRetryOption);
//             Assert.IsTrue(q.Success);
//             Assert.IsTrue(q.StatusCode == HttpStatusCode.OK);
//             await Command.StdoutAsync(q.Data);
//             Debug.Print(q.Data);

//             dynamic z = JsonConvert.DeserializeObject(q.Data);
//             Assert.IsNotNull(z);
//             Assert.IsTrue((bool)z.success);

//             await Task.Delay(100);

//             // ============================

//             request = new HttpRequest()
//             {
//                 Path = $"/users/{u1.data.account_id}"
//             };

//             var q1 = await httpClient.ExecuteHttp2GetAsync(request, withHttp2: true, consoleOutputLog: false, httpRetryOption: httpRetryOption);
//             Assert.IsTrue(q1.Success);
//             Assert.IsTrue(q1.StatusCode == HttpStatusCode.OK);
//             await Command.StdoutAsync(q1.Data);
//             Debug.Print(q1.Data);

//             request = new HttpRequest()
//             {
//                 Path = $"/users/{u2.data.account_id}"
//             };

//             var q2 = await httpClient.ExecuteHttp2GetAsync(request, withHttp2: true, consoleOutputLog: false, httpRetryOption: httpRetryOption);
//             Assert.IsTrue(q2.Success);
//             Assert.IsTrue(q2.StatusCode == HttpStatusCode.OK);
//             await Command.StdoutAsync(q2.Data);
//             Debug.Print(q2.Data);

//             #endregion 查詢資料測試 Http GET Test

//             #region 更新資料測試 Http PUT Test

//             var m1 = new
//             {
//                 u1.data.account_id,
//                 name = "Emily Joyce",
//                 sex = 0,
//                 age = 22,
//                 birthday = new DateTime(2000, 5, 5)
//             };

//             request = new HttpRequest()
//             {
//                 Path = "/users",
//                 Body = JsonConvert.SerializeObject(m1)
//             };

//             var d1 = await httpClient.ExecuteHttp2PutJsonAsync(request, withHttp2: true, consoleOutputLog: false, httpRetryOption: httpRetryOption);
//             Assert.IsTrue(d1.Success);
//             Assert.IsTrue(d1.StatusCode == HttpStatusCode.OK);
//             await Command.StdoutAsync(d1.Data);
//             Debug.Print(d1.Data);
//             await Task.Delay(100);

//             var m2 = new
//             {
//                 u2.data.account_id,
//                 name = "Nancy Wilson",
//                 sex = 1,
//                 age = 40,
//                 birthday = new DateTime(1980, 6, 6)
//             };

//             request = new HttpRequest()
//             {
//                 Path = "/users",
//                 Body = JsonConvert.SerializeObject(m2)
//             };

//             var d2 = await httpClient.ExecutePutJsonAsync(request, consoleOutputLog: false, httpRetryOption: httpRetryOption);
//             Assert.IsTrue(d2.Success);
//             Assert.IsTrue(d2.StatusCode == HttpStatusCode.OK);
//             await Command.StdoutAsync(d2.Data);
//             Debug.Print(d1.Data);
//             await Task.Delay(100);

//             #endregion 更新資料測試 Http PUT Test

//             #region 資料刪除測試 Http DELETE Test

//             request = new HttpRequest()
//             {
//                 Path = $"/users/{u1.data.account_id}"
//             };

//             var k1 = await httpClient.ExecuteHttp2DeleteAsync(request, withHttp2: true, consoleOutputLog: false, httpRetryOption: httpRetryOption);
//             Assert.IsTrue(k1.Success);
//             Assert.IsTrue(k1.StatusCode == HttpStatusCode.OK);
//             await Command.StdoutAsync(k1.Data);
//             Debug.Print(k1.Data);

//             request = new HttpRequest()
//             {
//                 Path = $"/users/{u2.data.account_id}"
//             };

//             var k2 = await httpClient.ExecuteDeleteAsync(request, consoleOutputLog: false);
//             Assert.IsTrue(k2.Success);
//             Assert.IsTrue(k2.StatusCode == HttpStatusCode.OK);
//             await Command.StdoutAsync(k2.Data);
//             Debug.Print(k2.Data);


//             #endregion 資料刪除測試 Http DELETE Test
//         }

//         /// <summary>請求並且嘗試 retry 的測試
//         /// </summary>
//         [TestMethod()]
//         [Description("請求並且嘗試 retry 的測試")]
//         public async Task ExecuteHttpRequestWithRetry_Test()
//         {
//             var baseUrl = "http://localhost:6662";       // 故意給定一個不存在的 baseUrl 位址。
//             var httpClient = HttpClientFactory.Create(baseUrl, 1);
//             Assert.IsNotNull(httpClient);

//             var httpRetryOption = new RetryOption
//             {
//                 EnableRetry = true,
//                 RetryFrequencies =
//                 [
//                     TimeSpan.FromSeconds( 1 ),
//                     TimeSpan.FromSeconds( 1.5 ),
//                     TimeSpan.FromSeconds( 1.5 ),
//                 ],
//             };

//             var errorCode = "40097";

//             #region Http GET retry 測試

//             var random = new Random(Guid.NewGuid().GetHashCode());

//             var request = new HttpRequest()
//             {
//                 Path = "/error/test",
//                 QueryStrings =
//                 [
//                     new KeyValuePair<string, string>( "msg", "This is http GET retry test." ),
//                     new KeyValuePair<string, string>( "number", random.Next( 500 ) + "" ),
//                 ]
//             };

//             var g = await httpClient.ExecuteHttp2GetAsync(request, httpRetryOption: httpRetryOption);
//             Assert.IsFalse(g.Success);
//             Assert.AreEqual(errorCode, g.Code);
//             Assert.AreNotEqual(HttpStatusCode.OK, g.StatusCode);
//             await Command.StdoutAsync(g.Data);
//             Debug.Print(g.Data);

//             #endregion Http GET retry 測試

//             #region Http POST retry 測試

//             request = new HttpRequest()
//             {
//                 Path = "/fucking/error/path",
//                 Body = "這不重要!"
//             };

//             g = await httpClient.ExecuteHttp2PostJsonAsync(request, httpRetryOption: httpRetryOption);
//             Assert.IsFalse(g.Success);
//             Assert.AreEqual(errorCode, g.Code);
//             Assert.AreNotEqual(HttpStatusCode.OK, g.StatusCode);
//             await Command.StdoutAsync(g.Data);
//             Debug.Print(g.Data);

//             #endregion Http POST retry 測試

//             #region Http PUT retry 測試

//             request = new HttpRequest()
//             {
//                 Path = "/fucking/error/path",
//                 Body = "這不重要!"
//             };

//             g = await httpClient.ExecuteHttp2PutJsonAsync(request, httpRetryOption: httpRetryOption, withHttp2: true);
//             Assert.IsFalse(g.Success);
//             Assert.AreEqual(errorCode, g.Code);
//             Assert.AreNotEqual(HttpStatusCode.OK, g.StatusCode);
//             await Command.StdoutAsync(g.Data);
//             Debug.Print(g.Data);

//             #endregion Http PUT retry 測試

//             #region Http DELETE retry 測試

//             request = new HttpRequest()
//             {
//                 Path = "/fucking/error/path"
//             };

//             g = await httpClient.ExecuteHttp2DeleteAsync(request, httpRetryOption: httpRetryOption, withHttp2: true);
//             Assert.IsFalse(g.Success);
//             Assert.AreEqual(errorCode, g.Code);
//             Assert.AreNotEqual(HttpStatusCode.OK, g.StatusCode);
//             await Command.StdoutAsync(g.Data);
//             Debug.Print(g.Data);

//             #endregion Http DELETE retry 測試
//         }

//         /// <summary>HttpClientExtension 發送 Http GET 請求測試
//         /// </summary>
//         [TestMethod()]
//         [Description("HttpClientExtension 發送 Http GET 請求測試")]
//         public async Task ExecuteGetAsync_BaseUrlTest()
//         {
//             var httpRetryOption = new RetryOption
//             {
//                 EnableRetry = true,
//                 RetryFrequencies =
//                 [
//                     TimeSpan.FromSeconds( 1 ),
//                     TimeSpan.FromSeconds( 1.5 ),
//                     TimeSpan.FromSeconds( 1.5 ),
//                 ],
//             };

//             // 故意測試一個 baseUrl 結尾不是斜線的情況。
//             var baseUrl = "http://zayni-lab.ddns.net:31178/zayni/api";
//             var httpClient = HttpClientFactory.Create(baseUrl, 5);
//             httpClient.DefaultRequestVersion = new Version(2, 0);
//             Assert.IsNotNull(httpClient);

//             var random = new Random(Guid.NewGuid().GetHashCode());

//             var request = new HttpRequest()
//             {
//                 Path = "/values",
//                 QueryStrings =
//                 [
//                     new KeyValuePair<string, string>( "msg", "HttpClientExtensionTester sending http GET." ),
//                     new KeyValuePair<string, string>( "number", random.Next( 500 ) + "" ),
//                 ]
//             };

//             var g = await httpClient.ExecuteHttp2GetAsync<Result<MagicDTO>>(request, consoleOutputLog: false, httpRetryOption: httpRetryOption);
//             Assert.IsTrue(g.Success);
//             Assert.IsTrue(g.StatusCode == HttpStatusCode.OK);
//             await Command.StdoutAsync(g.Data);
//             Debug.Print(g.Data);

//             var responseDTO = g.Payload;
//             Assert.IsNotNull(responseDTO);
//             Assert.IsTrue(responseDTO.Success);


//             var request2 = new HttpRequest()
//             {
//                 Path = "values",
//                 QueryStrings =
//                 [
//                     new KeyValuePair<string, string>( "msg", "HttpClientExtensionTester sending http GET." ),
//                     new KeyValuePair<string, string>( "number", random.Next( 500 ) + "" ),
//                 ]
//             };

//             var g2 = await httpClient.ExecuteHttp2GetAsync<Result<MagicDTO>>(request2, consoleOutputLog: false, httpRetryOption: httpRetryOption);
//             Assert.IsTrue(g2.Success);
//             Assert.IsTrue(g2.StatusCode == HttpStatusCode.OK);
//             await Command.StdoutAsync(g2.Data);
//             Debug.Print(g2.Data);

//             var responseDTO2 = g.Payload;
//             Assert.IsNotNull(responseDTO2);
//             Assert.IsTrue(responseDTO2.Success);

//             await Task.Delay(50);
//         }
//     }

//     /// <summary>測試用的傳輸資料載體
//     /// </summary>
//     public class SomethingDTO
//     {
//         /// <summary>測試訊息
//         /// </summary>
//         [JsonProperty(PropertyName = "someMsg")]
//         [JsonPropertyName("someMsg")]
//         public string SomeMessage { get; set; }

//         /// <summary>測試日期
//         /// </summary>
//         [JsonProperty(PropertyName = "someDate")]
//         [JsonPropertyName("someDate")]
//         public DateTime SomeDate { get; set; }

//         /// <summary>測試浮點數
//         /// </summary>
//         [JsonProperty(PropertyName = "magicNumber")]
//         [JsonPropertyName("magicNumber")]
//         public double SomeMagicNumber { get; set; }

//         /// <summary>測試整數
//         /// </summary>
//         [JsonProperty(PropertyName = "luckyNumber")]
//         [JsonPropertyName("luckyNumber")]
//         public long LuckyNumber { get; set; }

//         /// <summary>測試整布林值
//         /// </summary>
//         [JsonProperty(PropertyName = "isSuperXMan")]
//         [JsonPropertyName("isSuperXMan")]
//         public bool IsSuperMan { get; set; }
//     }

//     /// <summary>測試用資料載體
//     /// </summary>
//     public class MagicDTO
//     {
//         /// <summary>測試用的訊息
//         /// </summary>
//         [JsonProperty(PropertyName = "magicWords")]
//         [JsonPropertyName("magicWords")]
//         public string MagicWords { get; set; }

//         /// <summary>測試日期
//         /// </summary>
//         [JsonProperty(PropertyName = "magicTime")]
//         [JsonPropertyName("magicTime")]
//         public DateTime MagicTime { get; set; }

//         /// <summary>測試用 Double 浮點數
//         /// </summary>
//         [JsonProperty(PropertyName = "testDouble", NullValueHandling = NullValueHandling.Ignore)]
//         [JsonPropertyName("testDouble")]
//         public double? TestDouble { get; set; }
//     }
// }
