using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace ZayniFramework.Common.Test
{
    /// <summary>Float 或 Float? 擴充方法的測試類別
    /// </summary>
    [TestClass()]
    [TestCategory("Common.Test")]
    public class FloatExtensionTester
    {
        /// <summary>檢查 float 是否為一個包含小數位數的 float 數值的測試。<para/>
        /// * SUT: float.IsRealFloat()<para/>
        /// * 給定一個真的包含小數位數的 float 數值。<para/>
        /// * 當檢查 float 是否為一個包含小數位數的 float 數值。<para/>
        /// * 應該得到檢查結果為真。<para/>
        /// </summary>
        [TestMethod()]
        [Description("檢查 float 是否為一個包含小數位數的 float 數值的測試。")]
        public void GivenRealFloatNumber_WhenIsRealFloat_ThenShouldRetriveTrue()
        {
            GivenRealFloatNumber();
            WhenIsRealFloat();
            ThenShouldRetriveTrue();

            float source1;
            float source2;
            float source3;
            float source4;
            void GivenRealFloatNumber()
            {
                source1 = 123.58F;
                source2 = 123.03F;
                source3 = 123.009F;
                source4 = -123.34F;
            }

            bool result1;
            bool result2;
            bool result3;
            bool result4;
            void WhenIsRealFloat()
            {
                result1 = source1.IsRealFloat();
                result2 = source2.IsRealFloat();
                result3 = source3.IsRealFloat();
                result4 = source4.IsRealFloat();
            }

            void ThenShouldRetriveTrue()
            {
                result1.Should().BeTrue();
                result2.Should().BeTrue();
                result3.Should().BeTrue();
                result4.Should().BeTrue();
            }
        }

        /// <summary>檢查 float 是否為一個包含小數位數的 float 數值的測試。<para/>
        /// * SUT: float.IsRealFloat()<para/>
        /// * 給定一個沒有包含小數位數的 float 數值或小數位數為 0 的 decimal。<para/>
        /// * 當檢查 float 是否為一個包含小數位數的 float 數值。<para/>
        /// * 應該得到檢查結果為假。<para/>
        /// </summary>
        [TestMethod()]
        [Description("檢查 float 是否為一個包含小數位數的 float 數值的測試。")]
        public void GivenFloatNumberWithoutFloatPart_WhenIsRealFloat_ThenShouldRetriveFalse()
        {
            GivenFloatNumberWithoutFloatPart();
            WhenIsRealFloat();
            ThenShouldRetriveFalse();

            float source1;
            float source2;
            float source3;
            void GivenFloatNumberWithoutFloatPart()
            {
                source1 = 123F;              // 根本沒有包含小數位數
                source2 = -123F;             // 實際上為一個 Int32 的負整數
                source3 = 123.000F;      // 雖然有包含小數位數，但實際上小數位都是 0 的情況
            }

            bool result1;
            bool result2;
            bool result3;
            void WhenIsRealFloat()
            {
                result1 = source1.IsRealFloat();
                result2 = source2.IsRealFloat();
                result3 = source3.IsRealFloat();
            }

            void ThenShouldRetriveFalse()
            {
                result1.Should().BeFalse();
                result2.Should().BeFalse();
                result3.Should().BeFalse();
            }
        }

        /// <summary>檢查 float? 是否為一個包含小數位數的 float? 數值的測試。<para/>
        /// * SUT: float.IsRealFloat()<para/>
        /// * 給定一個真的包含小數位數的 float? 數值。<para/>
        /// * 當檢查 float? 是否為一個包含小數位數的 float? 數值。<para/>
        /// * 應該得到檢查結果為真。<para/>
        /// </summary>
        [TestMethod()]
        [Description("檢查 float 是否為一個包含小數位數的 float 數值的測試。")]
        public void GivenRealFloatNullableNumber_WhenIsRealFloat_ThenShouldRetriveTrue()
        {
            GivenRealFloatNullableNumber();
            WhenIsRealFloat();
            ThenShouldRetriveTrue();

            float? source1;
            float? source2;
            float? source3;
            float? source4;
            void GivenRealFloatNullableNumber()
            {
                source1 = 123.58F;
                source2 = 123.03F;
                source3 = 123.009F;
                source4 = -123.34F;
            }

            bool result1;
            bool result2;
            bool result3;
            bool result4;
            void WhenIsRealFloat()
            {
                result1 = source1.IsRealFloat();
                result2 = source2.IsRealFloat();
                result3 = source3.IsRealFloat();
                result4 = source4.IsRealFloat();
            }

            void ThenShouldRetriveTrue()
            {
                result1.Should().BeTrue();
                result2.Should().BeTrue();
                result3.Should().BeTrue();
                result4.Should().BeTrue();
            }
        }

        /// <summary>檢查 float? 是否為一個包含小數位數的 float? 數值的測試。<para/>
        /// * SUT: float.IsRealFloat()<para/>
        /// * 給定一個沒有包含小數位數的 float? 數值或小數位數為 0 的 float?。<para/>
        /// * 當檢查 float? 是否為一個包含小數位數的 float? 數值。<para/>
        /// * 應該得到檢查結果為假。<para/>
        /// </summary>
        [TestMethod()]
        [Description("檢查 float 是否為一個包含小數位數的 float 數值的測試。")]
        public void GivenFloatNullableNumberWithoutFloatPart_WhenIsRealFloat_ThenShouldRetriveFalse()
        {
            GivenFloatNullableNumberWithoutFloatPart();
            WhenIsRealFloat();
            ThenShouldRetriveFalse();

            float? source1;
            float? source2;
            float? source3;
            float? source4;
            void GivenFloatNullableNumberWithoutFloatPart()
            {
                source1 = 123F;              // 根本沒有包含小數位數
                source2 = -123F;             // 實際上為一個 Int32 的負整數
                source3 = 123.000F;          // 雖然有包含小數位數，但實際上小數位都是 0 的情況
                source4 = null;              // 根本就是一個 null
            }

            bool result1;
            bool result2;
            bool result3;
            bool result4;
            void WhenIsRealFloat()
            {
                result1 = source1.IsRealFloat();
                result2 = source2.IsRealFloat();
                result3 = source3.IsRealFloat();
                result4 = source4.IsRealFloat();
            }

            void ThenShouldRetriveFalse()
            {
                result1.Should().BeFalse();
                result2.Should().BeFalse();
                result3.Should().BeFalse();
                result4.Should().BeFalse();
            }
        }
    }
}