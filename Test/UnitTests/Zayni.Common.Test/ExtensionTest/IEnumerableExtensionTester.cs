﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Common.Test.TestModel;


namespace ZayniFramework.Common.Test.Extension.Test
{
    /// <summary>IEnumerable LINQ 擴充的測試類別
    /// </summary>
    [TestClass()]
    [TestCategory("Common.Test")]
    public class IEnumerableExtensionTester
    {
        /// <summary>檢查是否有元素重覆的測試
        /// </summary>
        [TestMethod()]
        [Description("檢查是否有元素重覆的測試")]
        public void IEnumerable_HasDuplicates_Test()
        {
            var source1 = new List<string>() { "A", "B", "C", "C" };
            var result1 = source1.HasDuplicates(q => q);
            Assert.IsTrue(result1);

            // ===============================

            var source2 = new List<UserModel>()
            {
                new()
                {
                    Name = "A",
                    Age  = 20,
                    Sex  = "female"
                },
                new()
                {
                    Name = "A",
                    Age  = 20,
                    Sex  = "female"
                },
                new()
                {
                    Name = "B",
                    Age  = 20,
                    Sex  = "female"
                }
            };

            var result2 = source2.HasDuplicates(q => q.Name);
            Assert.IsTrue(result2);

            // ===============================

            var source3 = new int[] { 1, 1, 3, 3, 4 };
            var result3 = source3.HasDuplicates(j => j);
            Assert.IsTrue(result3);

            // ===============================

            var source4 = new string[] { "1", "2", "A", "BBB" };
            var result4 = source4.HasDuplicates(s => s);
            Assert.IsFalse(result4);

            // ===============================

            var source5 = new List<UserModel>()
            {
                new()
                {
                    Name = "Kate",
                    Age  = 20,
                    Sex  = "female"
                },
                new()
                {
                    Name = "Amber",
                    Age  = 20,
                    Sex  = "female"
                },
                new()
                {
                    Name = "Sylvia",
                    Age  = 20,
                    Sex  = "female"
                }
            };

            var result5 = source5.HasDuplicates(m => m.Name);
            Assert.IsFalse(result5);
        }

        /// <summary>對集合物件以指定分格符號串接所有集合內元素的測試 JoinToken的測試
        /// </summary>
        [TestMethod()]
        [Description("對集合物件以指定分格符號串接所有集合內元素的測試 JoinToken的測試")]
        public void IEnumerableJoinTokenTest()
        {
            var models = new List<string>() { "AA", "BB", "CC", "DD" };
            var result = models.JoinToken("-");
            Assert.AreEqual("AA-BB-CC-DD", result);
        }

        /// <summary>檢查 IEnumerable 物件是否為 Null 或空集合的測試
        /// </summary>
        [TestMethod()]
        [Description("檢查 IEnumerable 物件是否為 Null 或空集合的測試")]
        public void IEnumerable_IsNullOrEmptyTest()
        {
            IEnumerable<string> source = null;
            Assert.IsTrue(source.IsNullOrEmpty());
            Assert.IsFalse(source.IsNotNullOrEmpty());

            IEnumerable<string> models = new List<string>();
            Assert.IsTrue(models.IsNullOrEmpty());
            Assert.IsFalse(models.IsNotNullOrEmpty());

            models = new List<string>() { "Test" };
            Assert.IsFalse(models.IsNullOrEmpty());
            Assert.IsTrue(models.IsNotNullOrEmpty());
        }

        /// <summary>檢查 IEnumerable 物件是否為 Null 或空集合的測試
        /// </summary>
        [TestMethod()]
        [Description("檢查 IEnumerable 物件是否為 Null 或空集合的測試")]
        public async Task IEnumerable_IsNullOrEmpty_Test()
        {
            IEnumerable<string> source = null;
            Assert.IsTrue(source.IsNullOrEmpty());
            Assert.IsFalse(source.IsNotNullOrEmpty());

            IEnumerable<string> models = new List<string>();
            Assert.IsTrue(models.IsNullOrEmpty());
            Assert.IsFalse(models.IsNotNullOrEmpty());

            models = new List<string>() { "Test" };
            Assert.IsFalse(models.IsNullOrEmpty());
            Assert.IsTrue(models.IsNotNullOrEmpty());

            // =============

            var isEmpty = false;
            var empty = default(List<UserDataModel>);

            empty.WhenNullOrEmpty(() => isEmpty = true);
            Assert.IsTrue(isEmpty);
            await empty.WhenNullOrEmpty(async () => { await Task.Delay(1); isEmpty = true; });
            Assert.IsTrue(isEmpty);

            // =============

            var isNotEmpty = false;
            var notEmpty = new List<UserDataModel>() { new() { Name = "Judy" }, new() { Name = "Nancy" } };

            notEmpty.WhenNotNullOrEmpty(m => isNotEmpty = true);
            Assert.IsTrue(isNotEmpty);

            await notEmpty.WhenNotNullOrEmpty(async m => { await Task.Delay(1); isNotEmpty = true; });
            Assert.IsTrue(isNotEmpty);

            var bitchModels = notEmpty.WhenNotNullOrEmpty(m => m.Foreach(q => q.Sex = "bitch").Select(z => z));
            Assert.IsTrue(bitchModels.Count() == 2);
            Assert.IsTrue(bitchModels.All(b => b.Sex == "bitch"));

            bitchModels = await notEmpty.WhenNotNullOrEmpty(async m =>
            {
                await Task.Delay(1);
                return m.Foreach(q => q.Sex = "female").Select(z => z);
            });
            Assert.IsTrue(bitchModels.Count() == 2);
            Assert.IsTrue(bitchModels.All(b => b.Sex == "female"));

            // =============

            var isEmpty2 = false;
            var empty2 = default(IEnumerable);

            empty2.WhenNullOrEmpty(() => isEmpty2 = true);
            Assert.IsTrue(isEmpty2);
            await empty2.WhenNullOrEmpty(async () => { await Task.Delay(1); isEmpty2 = true; });
            Assert.IsTrue(isEmpty2);

            // =============

            var isNotEmpty2 = false;
            IEnumerable notEmpty2 = new UserDataModel[] { new() { Name = "Judy" }, new() { Name = "Nancy" } };

            notEmpty2.WhenNotNullOrEmpty(m => isNotEmpty2 = true);
            Assert.IsTrue(isNotEmpty2);

            await notEmpty2.WhenNotNullOrEmpty(async m => { await Task.Delay(1); isNotEmpty2 = true; });
            Assert.IsTrue(isNotEmpty2);

            var userModels = notEmpty2.WhenNotNullOrEmpty(m => m.Cast<UserDataModel>().Foreach(q => q.Sex = "bitch").Select(z => z));
            Assert.IsTrue(userModels.Count() == 2);
            Assert.IsTrue(userModels.Cast<UserDataModel>().All(b => b.Sex == "bitch"));

            userModels = await notEmpty2.WhenNotNullOrEmpty(async m =>
            {
                await Task.Delay(1);
                return m.Cast<UserDataModel>().Foreach(q => q.Sex = "female").Select(z => z);
            });
            Assert.IsTrue(userModels.Count() == 2);
            Assert.IsTrue(userModels.Cast<UserDataModel>().All(b => b.Sex == "female"));
        }

        /// <summary>檢查 IEnumerable 物件是否為 Null 或空集合的測試
        /// </summary>
        [TestMethod()]
        [Description("檢查 IEnumerable 物件是否為 Null 或空集合的測試")]
        public void IEnumerable_IsNullOrEmptyTest2()
        {
            IEnumerable<string> source = null;
            Assert.IsTrue(source.IsNullOrEmpty());
            Assert.IsFalse(source.IsNotNullOrEmpty());

            IEnumerable<string> models = new List<string>();
            Assert.IsTrue(models.IsNullOrEmpty());
            Assert.IsFalse(models.IsNotNullOrEmpty());

            models = new List<string>() { "Test" };
            Assert.IsFalse(models.IsNullOrEmpty());
            Assert.IsTrue(models.IsNotNullOrEmpty());
        }

        /// <summary>對單一欄位的 Distinct 測試
        /// </summary>
        [TestMethod()]
        [Description("對單一欄位的 Distinct 測試")]
        public void IEnumerable_Distinct_SingleFields_Test()
        {
            var models = MakeFakeUsers();
            Assert.AreEqual(12, models.Count());

            // Distinct單一欄位
            var results = models.Distinct(k => k.Name).ToList();
            Assert.IsTrue(results.IsNotNullOrEmpty());
            Assert.AreEqual(2, results.Count);
        }

        /// <summary>對多個欄位的 Distinct 測試
        /// </summary>
        [TestMethod()]
        [Description("對多個欄位的 Distinct 測試")]
        public void IEnumerable_Distinct_MultiplueFields_Test()
        {
            var models = MakeFakeUsers();
            Assert.AreEqual(12, models.Count());

            // Distinct多個欄位
            var results = models.Distinct(k => new { k.Name, k.Age }).ToList();
            Assert.IsTrue(results.IsNotNullOrEmpty());
            Assert.AreEqual(3, results.Count);
        }

        /// <summary>IEnumerable 的 Insert 擴充方法測試
        /// </summary>
        [TestMethod()]
        public void IEnumerable_Insert_Test()
        {
            IEnumerable<UserModel> models = new List<UserModel>()
            {
                new()
                {
                    Name = "Kate",
                    Sex  = "Female",
                    Age  = 19
                }
            };

            var model =
                    models
                        .Insert(new UserModel() { Name = "Sylvia" })
                        .Insert(new UserModel() { Name = "Amber" })
                        .Insert(new UserModel() { Name = "Nancy" })
                        .Where(u => u.Name == "Amber")
                        .FirstOrDefault();

            Assert.AreEqual("Amber", model.Name);
            Assert.IsTrue(models.Any(q => q.Name == "Amber"));
        }

        /// <summary>IEnumerable ForeachAsync 擴充方法的測試
        /// </summary>
        [TestMethod()]
        public async Task IEnumerable_ForeachAsync_Test()
        {
            var models = MakeFakeUsers();
            var results = await models.ForeachAsync(async m =>
            {
                await Task.Delay(10);
                m.Name = "AngelaBaby";
            });

            Assert.IsTrue(models.All(m => m.Name == "AngelaBaby"));
            Assert.IsTrue(results.All(m => m.Name == "AngelaBaby"));
        }

        /// <summary>IEnumerable Foreach 擴充方法的測試
        /// </summary>
        [TestMethod()]
        public async Task IEnumerable_Foreach_Test()
        {
            var models = MakeFakeUsers();
            var results = models.Foreach(m => m.Name = "AngelaBaby");
            Assert.IsTrue(models.All(m => m.Name == "AngelaBaby"));
            Assert.IsTrue(results.All(m => m.Name == "AngelaBaby"));

            // ===================

            var models2 = MakeFakeUsers();
            var results2 = models2.ForeachYield(m => m.Name = "Amber");
            Assert.IsFalse(models2.All(m => m.Name == "Amber"));
            Assert.IsTrue(results2.All(m => m.Name == "Amber"));

            // ===================

            var result3 = MakeFakeUsers()
                .Where(s => "female" == s.Sex)
                .ForeachYield(y => y.Age = 25)
                .ForeachYield(z => z.Name = "hey");

            Assert.IsTrue(result3.Count() == 11);
            Assert.IsTrue(result3.All(m => m.Age == 25));
            Assert.IsTrue(result3.All(m => m.Name == "hey"));

            // ===================

            var result4 = MakeFakeUsers();

            // 只會 Age 大於 30 的資料進行 ForeachAsync
            var woModels = await result4.Where(o => o.Age > 30).ForeachAsync(async q =>
            {
                await Task.Delay(10);
                q.ToyCount = "600";
                q.Age += 10;
            });

            Assert.IsTrue(result4.Count() == 12);
            Assert.IsTrue(result4.Where(h => h.Age > 30).Count() == 10);
            Assert.IsTrue(result4.Where(h => h.Age > 30).All(m => m.ToyCount == "600"));
            Assert.IsTrue(result4.Where(h => h.Age > 30).All(m => m.Age == 43));
            Assert.IsTrue(woModels.Count() == 10);

            var sum = woModels
                .ForeachYield(q => q.Age += 7)
                .Select(s => s.Age)
                .Sum();

            // 500
            Assert.IsTrue(sum == 500);

            var sumAsync = (await woModels
                .ForeachAsync(async y =>
                {
                    await Task.Delay(1);
                    y.Age += 7;
                }))
                .Select(s => s.Age)
                .Sum();

            // 570
            Assert.IsTrue(sumAsync == 570);
        }

        #region Private Methdos

        /// <summary>建立測試用的假資料
        /// </summary>
        /// <returns></returns>
        private static List<UserModel> MakeFakeUsers()
        {
            var result = new List<UserModel>();

            for (var i = 0; i < 10; i++)
            {
                var model = new UserModel()
                {
                    Name = "Sylvia",
                    Sex = "female",
                    Age = 33
                };

                result.Add(model);
            }

            result.Add(new UserModel()
            {
                Name = "Sylvia",
                Sex = "female",
                Age = 30
            });

            result.Add(new UserModel()
            {
                Name = "Pony",
                Sex = "male",
                Age = 28
            });

            return result;
        }

        #endregion Private Methdos
    }
}
