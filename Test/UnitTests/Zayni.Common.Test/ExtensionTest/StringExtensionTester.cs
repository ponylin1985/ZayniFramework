﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Text;
using ZayniFramework.Common.Test.TestModel;


namespace ZayniFramework.Common.Test
{
    /// <summary>字串型別擴充方法的測試類別
    /// </summary>
    [TestClass()]
    [TestCategory("Common.Test")]
    public class StringExtensionTester
    {
        /// <summary>資料庫連線字串遮罩的測試。<para/>
        /// * SUT: ConnectionStringMask.Mask()<para/>
        /// * 給定合法的資料庫連線字串。<para/>
        /// * 當執行資料庫連線字串遮罩處理。<para/>
        /// * 應該得到帳密被遮罩成功的資料庫連線字串。<para/>
        /// </summary>
        [TestMethod()]
        [Description("資料庫連線字串遮罩的測試。")]
        public void GivenDbConnectionStrings_WhenMaskDbConnectionStrings_ThenShouldRetriveMaskedDbConnectionStrings()
        {
            string mysqlConnString;
            string mssqlConnString;
            string pgsqlConnString;
            string mysqlExpected;
            string mssqlExpected;
            string pgsqlExpected;
            string mysqlActual;
            string mssqlActual;
            string pgsqlActual;

            GivenDbConnectionStrings();
            WhenMaskDbConnectionStrings();
            ThenShouldRetriveMaskedDbConnectionStrings();

            void GivenDbConnectionStrings()
            {
                mysqlConnString = $"Server=localhost;Database=zayni;Uid=pony;Password=Admin123;Allow User Variables=True;Charset=utf8;Convert Zero Datetime=True;SslMode=none;";
                mssqlConnString = $"Server=localhost;Database=ZayniFramework;User Id=pony;Password=Admin123;Max Pool Size=20;Min Pool Size=5;Connection Lifetime=10;";
                pgsqlConnString = $"Server=localhost;Port=5432;Database=zayni;User Id=pony;Password=Admin123;MaxPoolSize=20;MinPoolSize=5;";
                mysqlExpected = $"Server=localhost;Database=zayni;Uid=*****;Password=*****;Allow User Variables=True;Charset=utf8;Convert Zero Datetime=True;SslMode=none;";
                mssqlExpected = $"Server=localhost;Database=ZayniFramework;User Id=*****;Password=*****;Max Pool Size=20;Min Pool Size=5;Connection Lifetime=10;";
                pgsqlExpected = $"Server=localhost;Port=5432;Database=zayni;User Id=*****;Password=*****;MaxPoolSize=20;MinPoolSize=5;";
            }

            void WhenMaskDbConnectionStrings()
            {
                mysqlActual = ConnectionStringMask.Mask(mysqlConnString);
                mssqlActual = ConnectionStringMask.Mask(mssqlConnString);
                pgsqlActual = ConnectionStringMask.Mask(pgsqlConnString);
            }

            void ThenShouldRetriveMaskedDbConnectionStrings()
            {
                mysqlActual.Should().Be(mysqlExpected);
                mssqlActual.Should().Be(mssqlExpected);
                pgsqlActual.Should().Be(pgsqlExpected);
            }
        }

        /// <summary>移除字串中的不可視字元。<para/>
        /// * SUT: string.TrimNonVisualCharacter()<para/>
        /// * 給定包含不可視字元與空格字元的字串。<para/>
        /// * 當執行 Trim 掉不可視字元與空格字元的動作。<para/>
        /// * 應該得到所有頭尾不可視字元和空格都被移除的字串。<para/>
        /// </summary>
        [TestMethod()]
        [Description("移除字串中的不可視字元。")]
        public void GivenInvisibleCharactersAndSpeces_WhenTrimNonVisualCharacter_ThenShouldRetriveTrimedText()
        {
            string source;
            string result;

            GivenInvisibleCharactersAndSpeces();
            WhenTrimNonVisualCharacter();
            ThenShouldRetriveTrimedText();

            void GivenInvisibleCharactersAndSpeces()
            {
                // 頭尾包含半形或全形的空格，也包含不可視的控制字元
                source = "  　⊥A B C 我是快樂熊  I am happy bear!！　　　　";
                var g = '\u1011';
                var h = '\u0001';
                var j = '\u0020';
                var k = '\u0007';
                var q = '\u200B';
                var w = '\uFEFF';
                var d = '　';
                source = string.Concat(source, g, h, j, k, q, w, d, d, d);
            }

            void WhenTrimNonVisualCharacter() =>
                result = source.TrimNonVisualCharacter(removeControlCharacter: true);

            void ThenShouldRetriveTrimedText()
            {
                System.Diagnostics.Debug.Print("Source:");
                System.Diagnostics.Debug.Print(Newtonsoft.Json.JsonConvert.SerializeObject(source));
                System.Diagnostics.Debug.Print(System.Text.Json.JsonSerializer.Serialize(source));
                System.Diagnostics.Debug.Print("===========");
                System.Diagnostics.Debug.Print("Result:");
                System.Diagnostics.Debug.Print(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                System.Diagnostics.Debug.Print(System.Text.Json.JsonSerializer.Serialize(result));
                result.Should().Be("⊥A B C 我是快樂熊  I am happy bear!！　　　　ထ");
            }
        }

        /// <summary>字串開頭檢查的測試。<para/>
        /// * SUT: string.StartsLike()<para/>
        /// * 給定特定字串開頭的來源字串。<para/>
        /// * 當執行忽略大小寫檢查來源自串是否已特定字串開頭。<para/>
        /// * 應該得到檢查有確實檢查到有以給定特定字串開頭。<para/>
        /// </summary>
        [TestMethod()]
        [Description("字串開頭檢查的測試。")]
        public void GivenPrefixInCaseInsentiviteText_WhenDoStartsLikeCheck_ThenShouldRetriveTrue()
        {
            string source1;
            string source2;
            string source3;
            bool result1;
            bool result2;
            bool result3;

            GivenPrefixInCaseInsentiviteText();
            WhenDoStartsLikeCheck();
            ThenShouldRetriveTrue();

            void GivenPrefixInCaseInsentiviteText()
            {
                source1 = "BBKKIItt_123_rrr";
                source2 = "BbkkIItT_";
                source3 = "KKKKjdie";
            }

            void WhenDoStartsLikeCheck()
            {
                result1 = source1.StartsLike("bbKkiItT");
                result2 = source2.StartsLike("BbkkIItT_");
                result3 = source3.StartsLike("KKKKjdie");
            }

            void ThenShouldRetriveTrue()
            {
                result1.Should().BeTrue();
                result2.Should().BeTrue();
                result3.Should().BeTrue();
            }
        }

        /// <summary>字串結尾檢查的測試。<para/>
        /// * SUT: string.EndsLike()<para/>
        /// * 給定特定字串結尾的來源字串。<para/>
        /// * 當執行忽略大小寫檢查來源自串是否已特定字串結尾。<para/>
        /// * 應該得到檢查有確實檢查到有以給定特定字串結尾。<para/>
        /// </summary>
        [TestMethod()]
        [Description("字串結尾檢查的測試。")]
        public void GivenPostfixInCaseInsentiviteText_WhenDoEndsLikeCheck_ThenShouldRetriveTrue()
        {
            GivenPostfixInCaseInsentiviteText();
            WhenDoEndsLikeCheck();
            ThenShouldRetriveTrue();

            string source1;
            string source2;
            string source3;

            void GivenPostfixInCaseInsentiviteText()
            {
                source1 = "BBKKIItt_123_HelloHappyBear";
                source2 = "hAPPyBeaR";
                source3 = "Testing";
            }

            bool result1;
            bool result2;
            bool result3;

            void WhenDoEndsLikeCheck()
            {
                result1 = source1.EndsLike("_helloHAPPYbear");
                result2 = source2.EndsLike("beAr");
                result3 = source3.EndsLike("ING");
            }

            void ThenShouldRetriveTrue()
            {
                result1.Should().BeTrue();
                result2.Should().BeTrue();
                result3.Should().BeTrue();
            }
        }

        /// <summary>字串列舉值轉換的測試。<para/>
        /// * SUT: string.ParseEnum<T>()<para/>
        /// * 給定正確格式的來源字串。<para/>
        /// * 當執行轉換成 enum 列舉值時。<para/>
        /// * 應該得到正確得 enum 列舉值。<para/>
        /// </summary>
        [TestMethod()]
        [Description("字串列舉值轉換的測試。")]
        public void GivenValidSourceText_WhenDoParseEnum_ThenShouldRetriveCorrectEnumValue()
        {
            GivenValidSourceText();
            WhenDoParseEnum();
            ThenShouldRetriveCorrectEnumValue();

            string source1;
            string source2;

            void GivenValidSourceText()
            {
                source1 = "Green";
                source2 = "enable";
            }

            ConsoleColor result1;
            State result2;

            void WhenDoParseEnum()
            {
                result1 = source1.ParseEnum<ConsoleColor>();
                result2 = source2.ParseEnum<State>();
            }

            void ThenShouldRetriveCorrectEnumValue()
            {
                result1.Should().Be(ConsoleColor.Green);
                result2.Should().Be(State.Enable);
            }
        }

        /// <summary>字串列舉值轉換的測試。<para/>
        /// * SUT: string.ParseEnum<T>()<para/>
        /// * 給定正確格式的來源字串。<para/>
        /// * 當執行轉換成 nullable 的列舉值時。<para/>
        /// * 應該得到正確得 nullable 的 enum 列舉值。<para/>
        /// </summary>
        [TestMethod()]
        [Description("字串列舉值轉換的測試。")]
        public void GivenValidSourceText_WhenDoParseNullableEnum_ThenShouldRetriveCorrectEnumValue()
        {
            string source;
            ConsoleColor? result;

            GivenValidSourceText();
            WhenDoParseNullableEnum();
            ThenShouldRetriveCorrectEnumValue();

            void GivenValidSourceText() => source = "Green";
            void WhenDoParseNullableEnum() => result = source.ParseEnum<ConsoleColor>();
            void ThenShouldRetriveCorrectEnumValue() => result.Should().Be(ConsoleColor.Green);
        }

        /// <summary>日期時間字串轉換測試。<para/>
        /// * SUT string.TryParseDateTime()<para/>
        /// * 給定合法得日期時間字串。<para/>
        /// * 當執行日期時間字串轉換成 DateTime 物件。<para/>
        /// * 應該得到正確得日期時間 DateTime 物件。<para/>
        /// </summary>
        [TestMethod()]
        [Description("日期時間字串轉換測試。")]
        public void GiveValidDateTimeSourceText_WhenDoTryParseDateTime_ThenShouldRetriveCorrectDateTime()
        {
            string source1;
            string source2;
            string source3;
            bool result1;
            bool result2;
            bool result3;
            DateTime dateTime1;
            DateTime dateTime2;
            DateTime dateTime3;

            GiveValidDateTimeSourceText();
            WhenDoTryParseDateTime();
            ThenShouldRetriveCorrectDateTime();

            void GiveValidDateTimeSourceText()
            {
                source1 = "20160526";
                source2 = "201605";
                source3 = "2016-05/26   14:23~45";
            }

            void WhenDoTryParseDateTime()
            {
                result1 = source1.TryParseDateTime("yyyyMMdd", out dateTime1);
                result2 = source2.TryParseDateTime("yyyyMM", out dateTime2);
                result3 = source3.TryParseDateTime("yyyy-MM/dd   HH:mm~ss", out dateTime3);
            }

            void ThenShouldRetriveCorrectDateTime()
            {
                var expected1 = new DateTime(2016, 5, 26);
                result1.Should().BeTrue();
                dateTime1.Should().Be(expected1);

                var expected2 = new DateTime(2016, 5, 1);
                result2.Should().BeTrue();
                dateTime2.Should().Be(expected2);

                var expected3 = new DateTime(2016, 5, 26, 14, 23, 45);
                result3.Should().BeTrue();
                dateTime3.Should().Be(expected3);
            }
        }

        /// <summary>具名參數字串格式化測試。<para/>
        /// * SUT: string.Format()<para/>
        /// * 給定帶有合法格式的具名參數化字串。<para/>
        /// * 當執行劇名參數格式化處理。<para/>
        /// * 應該得到完整正確的結果字串。<para/>
        /// </summary>
        [TestMethod()]
        [Description("具名參數字串格式化測試。")]
        public void GivenTextWithValidNamedParameters_WhenDoStringFormat_ThenShouldRetriveCorrectText()
        {
            var source1 = default(string);
            var source2 = default(string);
            var result1 = default(string);
            var result2 = default(string);

            GivenTextWithValidNamedParameters();
            WhenDoStringFormat();
            ThenShouldRetriveCorrectText();

            void GivenTextWithValidNamedParameters()
            {
                source1 = "Hi {name}. {message}";
                source2 = "{framework} is very cool.";
            }

            void WhenDoStringFormat()
            {
                result1 = source1.Format(name => "Pony", message => "I am happy bear.");
                result2 = source2.Format(framework => new StringBuilder("Zayni Framework"));
            }

            void ThenShouldRetriveCorrectText()
            {
                result1.Should().Be("Hi Pony. I am happy bear.");
                result2.Should().Be("Zayni Framework is very cool.");
            }
        }

        /// <summary>擷取子字串的單元測試
        /// </summary>
        [TestMethod()]
        [Description("擷取子字串的單元測試")]
        public void FetchSubstringTest()
        {
            var test = "AAkkkBBAAiiiBB";
            var tokens = test.FetchSubstring("AA", "BB").ToArray();

            Assert.IsTrue(tokens.IsNotNullOrEmpty());
            Assert.IsFalse(tokens.HasNullOrEmptyElemants());

            Assert.AreEqual("kkk", tokens[0]);
            Assert.AreEqual("iii", tokens[1]);
        }

        [TestMethod()]
        [TestCategory("Common.StringExtension - 字串擴充方法測試")]
        [Description("IsNullOrEmpty 回呼處理委派的單元測試")]
        public void IsNullOrEmptyCallbackTest1()
        {
            var model = new UserModel()
            {
                Name = "Sylvia"
            };

            var test = "";

            var isNullOrEmpty = test.IsNullOrEmpty(() =>
            {
                test = model.Name;
            });

            Assert.IsTrue(isNullOrEmpty);
            Assert.AreEqual("Sylvia", test);

            //=============================================================================

            string test2 = null;

            var isNullOrEmpty2 = test2.IsNullOrEmpty(() =>
            {
                test2 = model.Name;
            });

            Assert.IsTrue(isNullOrEmpty2);
            Assert.AreEqual("Sylvia", test2);
        }

        [TestMethod()]
        [Description("IsNullOrEmpty 回呼處理委派的反向單元測試")]
        public void IsNullOrEmptyCallbackTest2()
        {
            var model = new UserModel()
            {
                Name = "Sylvia"
            };

            var test = "hello";

            var isNullOrEmpty = test.IsNullOrEmpty(() =>
            {
                test = model.Name;
            });

            Assert.IsFalse(isNullOrEmpty);
            Assert.AreEqual("hello", test);
        }

        [TestMethod()]
        [Description("IsNotNullOrEmpty 回呼處理委派的單元測試")]
        public void IsNotNullOrEmptyCallbackTest1()
        {
            var test = "Sylvia";

            var isNotNullOrEmpty = test.IsNotNullOrEmpty(s =>
            {
                test = s + " loves Pony very much.";
            });

            Assert.IsTrue(isNotNullOrEmpty);
            Assert.AreEqual("Sylvia loves Pony very much.", test);

            var test2 = "aaa";

            var isNotNullOrEmpty2 = test2.IsNotNullOrEmpty(r =>
            {
                r = "bbb";      // Pony Says: 請注意! 這邊對r重新做指派，並沒有辦法改變test指向的物件，因為不是Pass-by-reference
            });

            Assert.IsTrue(isNotNullOrEmpty2);
            Assert.AreEqual("aaa", test2);
        }

        [TestMethod()]
        [Description("IsNotNullOrEmpty 回呼處理委派的反向單元測試")]
        public void IsNotNullOrEmptyCallbackTest2()
        {
            var test = "";

            var isNotNullOrEmpty = test.IsNotNullOrEmpty(s =>
            {
                test = s + " loves Pony very much.";
            });

            Assert.IsFalse(isNotNullOrEmpty);
            Assert.AreNotEqual("Sylvia loves Pony very much.", test);
            Assert.IsTrue(test.IsNullOrEmpty());
        }

        [TestMethod()]
        [Description("IsNullOrEmptyTest擴充方法的單元測試")]
        public void IsNullOrEmptyTest1()
        {
            var test = "";
            Assert.IsTrue(test.IsNullOrEmpty());
        }

        [TestMethod()]
        [Description("IsNullOrEmptyTest擴充方法的單元測試")]
        public void IsNullOrEmptyTest2()
        {
            string test = null;
            Assert.IsTrue(test.IsNullOrEmpty());
        }

        [TestMethod()]
        [Description("IsNullOrEmptyTest擴充方法的單元測試")]
        public void IsNullOrEmptyTest3()
        {
            var test = "   ";
            Assert.IsTrue(test.IsNullOrEmpty());      // 請注意，IsNullOrEmpty預設是會Trim的
        }

        [TestMethod()]
        [Description("IsNullOrEmptyTest擴充方法的單元測試")]
        public void IsNullOrEmptyTest4()
        {
            var test = "   ";
            Assert.IsFalse(test.IsNullOrEmpty(false));      // 強制IsNullOrEmpty先不要Trim再進行檢查
        }

        [TestMethod()]
        [Description("IsNullOrEmptyTest擴充方法的單元測試")]
        public void IsNullOrEmptyTest5()
        {
            var test = "Sylvia";
            Assert.IsFalse(test.IsNullOrEmpty(false));      // 強制IsNullOrEmpty先不要Trim再進行檢查
        }

        [TestMethod()]
        [Description("IsNullOrEmptyString擴充方法的單元測試")]
        public void IsNullOrEmptyStringTest1()
        {
            var test = "";
            var result = test.IsNullOrEmptyString("Sylvia is my lover.");
            Assert.AreEqual("Sylvia is my lover.", result);

            var test2 = "   ";
            var result2 = test2.IsNullOrEmptyString("Sylvia is my lover.");    // 請注意，IsNullOrEmptyString預設是不會Trim的
            Assert.AreNotEqual("Sylvia is my lover.", result2);
            Assert.AreEqual(test2, result2);

            string test3 = null;
            var result3 = test3 ?? "Hello Sylvia";   // 檢查是否為Null的運算子
            Assert.AreEqual("Hello Sylvia", result3);

            object test4 = null;
            var result4 = test4 ?? new User() { Age = 32 };
            Assert.IsTrue(result4 is User);
            Assert.AreEqual(32, ((User)result4).Age);
        }

        [TestMethod()]
        [Description("IsNullOrEmptyString擴充方法的單元測試")]
        public void IsNullOrEmptyStringTest2()
        {
            var test = "Pony loves Sylvia.";
            var result = test.IsNullOrEmptyString("Sylvia is my lover.");
            Assert.AreEqual("Pony loves Sylvia.", result);
        }

        [TestMethod()]
        [Description("IsNullOrEmptyString擴充方法的單元測試")]
        public void IsNullOrEmptyStringTest3()
        {
            var test = "   ";
            var result = test.IsNullOrEmptyString("Sylvia is my lover.", true);    // 強制IsNullOrEmptyString先Trim之後再進行檢查
            Assert.AreEqual("Sylvia is my lover.", result);
        }

        [TestMethod()]
        [Description("IsNullOrEmptyString擴充方法的單元測試")]
        public void IsNullOrEmptyStringTest4()
        {
            string test = null;
            var result = test.IsNullOrEmptyString("Sylvia is my lover.", true);
            Assert.AreEqual("Sylvia is my lover.", result);
            _ = test.IsNullOrEmptyString("Sylvia is my lover.", false);
            Assert.AreEqual("Sylvia is my lover.", result);
        }

        [TestMethod()]
        [Description("覆寫運算子的單元測試")]
        public void IsNullOrEmptyTest222()
        {
            var u = new User
            {
                Age = 9
            };

            var u2 = new User
            {
                Age = 9
            };

            var result = u + u2;

            Assert.AreEqual(18, result.Age);
        }

        [TestMethod()]
        [Description("對StringExtension的ReplaceChar的正向單元測試")]
        public void ReplaceCharTest()
        {
            var name = "林品辰";
            var cpName = "博暉科技股份有限公司";

            var result1 = name.ReplaceChar('X', 1);
            Assert.AreEqual("林XX", result1);

            var result2 = cpName.ReplaceChar('X', 1, 5);
            Assert.AreEqual("博XXXXX有限公司", result2);
        }

        [TestMethod()]
        [Description("對StringExtension的ReplaceChar的單元測試")]
        public void ReplaceCharTest2()
        {
            var name = "林品辰";
            var cpName = "博暉科技股份有限公司";

            var result1 = name.ReplaceChar('X', 1, 65451);          // 故意給定一個一定會爆掉的長度
            Assert.AreEqual("林XX", result1);

            var result2 = cpName.ReplaceChar('X', 1, 6321324);      // 故意給定一個一定會爆掉的長度
            Assert.AreEqual("博XXXXXXXXX", result2);                // 結果應該會得到一個字串，replace到原本字串整個結束掉，算是一種防止爆Exception的機制
        }

        [TestMethod()]
        [Description("對StringExtension的擴充的單元測試")]
        public void StringExtensionTest()
        {
            var test1 = "";
            Assert.IsTrue(test1.IsNullOrEmpty());
            Assert.IsFalse(test1.IsNotNullOrEmpty());

            string test2 = null;
            Assert.IsTrue(test2.IsNullOrEmpty());
            Assert.IsFalse(test2.IsNotNullOrEmpty());

            var test3 = "kkk";
            Assert.IsFalse(test3.IsNullOrEmpty());
            Assert.IsTrue(test3.IsNotNullOrEmpty());
        }

        [TestMethod()]
        [Description("對SplitString字串擴充方法的測試")]
        public void SplitStringExtensionTest()
        {
            var test = "a;;b;;c;;d;;e;;f";
            var tokens = test.SplitString(";;");

            Assert.IsTrue(tokens.IsNotNullOrEmpty());
            Assert.IsTrue(6 == tokens.Length);
            Assert.AreEqual("a", tokens[0]);
            Assert.AreEqual("b", tokens[1]);
            Assert.AreEqual("c", tokens[2]);
            Assert.AreEqual("d", tokens[3]);
            Assert.AreEqual("e", tokens[4]);
            Assert.AreEqual("f", tokens[5]);
        }

        [TestMethod()]
        [Description("對SplitString字串擴充方法的測試")]
        public void StringArrayJoinExtensionTest()
        {
            var separator = ";;";

            var test = "a;;b;;c;;d;;e;;f";
            var tokens = test.SplitString(separator);

            Assert.IsTrue(tokens.IsNotNullOrEmpty());
            Assert.IsTrue(6 == tokens.Length);
            Assert.AreEqual("a", tokens[0]);
            Assert.AreEqual("b", tokens[1]);
            Assert.AreEqual("c", tokens[2]);
            Assert.AreEqual("d", tokens[3]);
            Assert.AreEqual("e", tokens[4]);
            Assert.AreEqual("f", tokens[5]);

            var result = tokens.Join(separator);
            Assert.AreEqual(test, result);
        }

        [TestMethod()]
        [Description("對字串陣列TrimElements擴充方法的測試")]
        public void StringArrayTrimElementsTest()
        {
            string[] test = [" hh ", "g  ", "Sylvia   "];
            test.TrimElements();

            Assert.AreEqual("hh", test[0]);
            Assert.AreEqual("g", test[1]);
            Assert.AreEqual("Sylvia", test[2]);
        }

        [TestMethod()]
        [Description("對字串陣列TrimElements擴充方法的測試2")]
        public void StringArrayTrimElementsTest2()
        {
            string[] test = [];
            test.TrimElements();

            Assert.IsTrue(test.IsNullOrEmpty());
        }

        [TestMethod()]
        [ExpectedException(typeof(NullReferenceException))]
        [Description("對字串陣列TrimElements擴充方法的測試3 - Exception預期測試")]
        public void StringArrayTrimElementsTest3()
        {
            string[] test = null;   // 故意傳一個Null的值
            test.TrimElements();
        }

        [TestMethod()]
        [Description("對字串轉換成Binary二進位資料的測試")]
        public void ToBytesTest()
        {
            var test = "This is a test string.";
            var binary = test.ToUtf8Bytes();
            Assert.IsTrue(binary.IsNotNullOrEmpty());

            var result = System.Text.Encoding.UTF8.GetString(binary);
            Assert.AreEqual(test, result);
        }

        [TestMethod()]
        [Description("對字串進行Base64編碼的測試")]
        public void Base64EncodeTest()
        {
            var test = "This is a test string.";
            var result = test.Base64Encode();
            Assert.IsTrue(result.IsNotNullOrEmpty());
        }

        [TestMethod()]
        [Description("對字串進行Base64解碼的測試")]
        public void Base64DecodeTest()
        {
            var test = "This is a test string.";
            var result = test.Base64Encode();
            Assert.IsTrue(result.IsNotNullOrEmpty());

            var recover = result.Base64Decode();
            Assert.AreEqual(test, recover);
        }

        [TestMethod()]
        [Description("對字串進行GZip壓縮的測試")]
        public void StringCompressionTest()
        {
            var test = "This is a test string. This is a test string. This is a test string. This is a test string. This is a test string. This is a test string. This is a test string.";
            var result = test.GZipCompress();
            Assert.IsTrue(result.IsNotNullOrEmpty());
            Assert.IsTrue(result.Length < test.Length);

            var recorver = result.GZipDecompress();
            Assert.IsTrue(recorver.IsNotNullOrEmpty());
            Assert.AreEqual(test, recorver);
        }

        [TestMethod()]
        [Description("對字串進行GZip壓縮的測試 - 壓縮成二進位資料的測試")]
        public void StringCompressionTest2()
        {
            var test = "This is a test string. This is a test string. This is a test string. This is a test string. This is a test string. This is a test string. This is a test string.";
            var result = test.GZipCompress2Binary();
            Assert.IsTrue(result.IsNotNullOrEmpty());
            Assert.IsTrue(result.Length < test.Length);
        }

        [TestMethod()]
        [Description("對字串特定字元Trim的測試1")]
        public void StringTrimCharsTest()
        {
            var test = "&&Sylvia loves Pony very much.&&&&&";
            var result = test.TrimChars('&');
            Assert.AreEqual("Sylvia loves Pony very much.", result);
        }

        [TestMethod()]
        [Description("對字串特定字元Trim的測試2")]
        public void StringTrimCharsTest2()
        {
            var test = "&&Sylvia loves Pony very much.&&&&&###";
            var result = test.TrimChars('&', '#');
            Assert.AreEqual("Sylvia loves Pony very much.", result);
        }

        [TestMethod()]
        [Description("對字串特定字元Trim的測試3")]
        public void StringTrimCharsTest3()
        {
            var test = "Sylvia-loves-Pony-very-much.";   // 請注意，TrimChars只會去頭去尾
            var result = test.TrimChars('-');
            Assert.AreEqual("Sylvia-loves-Pony-very-much.", result);
        }

        [TestMethod()]
        [Description("對字串特定字元Trim的測試4")]
        public void StringTrimCharsTest4()
        {
            var test = "Sylvia loves Pony very much.";       // 故意傳入一個沒有要目標要trim的字元的字串
            var result = test.TrimChars('&');
            Assert.AreEqual("Sylvia loves Pony very much.", result);
        }

        [TestMethod()]
        [Description("對字串特定字元Trim的測試5")]
        public void StringTrimCharsTest5()
        {
            var test = "&&&Sylvia loves Pony very much.&&&&&&&&&&&&&&";       // 故意傳入一個沒有要目標要trim的字元的字串
            var result = test.TrimChars('&', '*', '#');                     // 故意讓他trim不到 * 和 #
            Assert.AreEqual("Sylvia loves Pony very much.", result);
        }

        [TestMethod()]
        [Description("對字串特定字元Trim的測試6")]
        public void StringTrimCharsTest6()
        {
            var test = "&&###&&&&Sylvia loves Pony very much.&&&&&&&&&&&&&&";     // 故意傳入一個沒有要目標要trim的字元的字串
            var result = test.TrimChars('&', '*', '#');                           // 故意讓他trim不到 *
            Assert.AreEqual("Sylvia loves Pony very much.", result);
        }

        [TestMethod()]
        [Description("檢查字串陣列中是否包含任何Null或空字串的元素的測試")]
        public void HasNullOrEmptyElementsTest()
        {
            string[] arr = ["", "asdf"];
            Assert.IsTrue(arr.HasNullOrEmptyElemants());

            string[] arr2 = [null, "ff"];
            Assert.IsTrue(arr2.HasNullOrEmptyElemants());
        }

        [TestMethod()]
        [Description("檢查字串陣列中是否包含任何Null或空字串的元素的反向測試")]
        public void HasNullOrEmptyElementsTest2()
        {
            string[] arr = ["ddd", "asdf"];
            Assert.IsFalse(arr.HasNullOrEmptyElemants());
        }

        /// <summary>String RemoveFirstAppeared 擴充方法的測試
        /// </summary>
        [TestMethod()]
        [Description("String RemoveFirstAppeared 擴充方法的測試")]
        public void RemoveFirstAppeared_Test()
        {
            // 測試目標字串存在於原始字串中的正常情況
            var source = "AAANN,KKK-333-888+DFJ{22_SuckMyCock_YouFuckingBitch...";
            var result = source.RemoveFirstAppeared("AAANN,KKK-333-");
            Assert.AreEqual("888+DFJ{22_SuckMyCock_YouFuckingBitch...", result);

            // ===============================

            // 測試目標字串不存在於原始字串中的例外情況
            result = source.RemoveFirstAppeared("操妳個騷機八");
            Assert.AreEqual("AAANN,KKK-333-888+DFJ{22_SuckMyCock_YouFuckingBitch...", result);
        }

        /// <summary>String RemoveLastAppeared 擴充方法的測試
        /// </summary>
        [TestMethod()]
        [Description("String RemoveLastAppeared 擴充方法的測試")]
        public void RemoveLastAppeared_Test()
        {
            // 測試目標字串存在於原始字串中的正常情況
            var source = "AAANN,KKK-333-888+DFJ{22_SuckMyCock_YouFuckingBitch...";
            var result = source.RemoveLastAppeared("...");
            Assert.AreEqual("AAANN,KKK-333-888+DFJ{22_SuckMyCock_YouFuckingBitch", result);

            // ===============================

            // 測試目標字串不存在於原始字串中的例外情況
            result = source.RemoveLastAppeared("操妳個騷機八");
            Assert.AreEqual("AAANN,KKK-333-888+DFJ{22_SuckMyCock_YouFuckingBitch...", result);
        }
    }

    /// <summary>單元測試用的 User 使用者類別
    /// </summary>
    public class User
    {
        /// <summary>年齡
        /// </summary>
        /// <value></value>
        public int Age { get; set; }

        /// <summary>運算子 + 方法覆寫
        /// </summary>
        /// <param name="i"></param>
        /// <param name="j"></param>
        /// <returns></returns>
        public static User operator +(User i, User j)
        {
            return new User()
            {
                Age = i.Age + j.Age
            };
        }
    }

    /// <summary>測試用的 Enum 列舉值
    /// </summary>
    public enum State
    {
        /// <summary>啟用
        /// </summary>
        Enable,

        /// <summary>停用
        /// </summary>
        Disable
    }
}
