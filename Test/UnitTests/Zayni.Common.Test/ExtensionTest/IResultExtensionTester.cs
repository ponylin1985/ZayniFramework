using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;
using System.Text;


namespace ZayniFramework.Common.Test
{
    /// <summary>IResult 物件擴充方法測試類別
    /// </summary>
    [TestClass()]
    [TestCategory("Common.Test")]
    public class IResultExtensionTester
    {
        /// <summary>給定合法的 IResult 方法鏈 pipeline 通道，執行此 IResult 方法鏈之後，應該得到正確的執行結果。
        /// </summary>
        [TestMethod()]
        [Description("IResult Pipeline Extension Method Test")]
        public void GivenIResultPipeline_WhenRunPipeline_ThenShouldRetriveCorrectResult()
        {
            var func1 = default(Func<string, IResult<string>>);
            var func2 = default(Func<string, IResult<string>>);
            var func3 = default(Func<string, IResult<string>>);
            var func4 = default(Func<string, IResult<StringBuilder>>);
            var result = default(IResult<StringBuilder>);

            GivenIResultPipeline();
            WhenRunPipeline();
            ThenShouldRetriveCorrectResult();

            void GivenIResultPipeline()
            {
                func1 = new Func<string, IResult<string>>(DoSomething1);
                func2 = new Func<string, IResult<string>>(DoSomething2);
                func3 = new Func<string, IResult<string>>(DoSomething3);
                func4 = new Func<string, IResult<StringBuilder>>(DoSomething4);
            }

            void WhenRunPipeline()
            {
                result =
                    func1("a")
                        .Pipe(b => func2(b))
                        .Pipe(c => func3(c))
                        .Pipe(d => func4(d));
            }

            void ThenShouldRetriveCorrectResult()
            {
                result.Data.ToString().Should().Be("abcd");
            }

            IResult<string> DoSomething1(string msg)
            {
                var value = $"{msg}";
                return Result.Create(true, data: value);
            }

            IResult<string> DoSomething2(string msg)
            {
                var value = $"{msg}b";
                return Result.Create(true, data: value);
            }

            IResult<string> DoSomething3(string msg)
            {
                var value = $"{msg}c";
                return Result.Create(true, data: value);
            }

            IResult<StringBuilder> DoSomething4(string msg)
            {
                var value = $"{msg}d";
                return Result.Create(true, data: new StringBuilder(value));
            }
        }

        /// <summary>
        /// </summary>
        [TestMethod()]
        [Description("IResult Pipeline Extension Method Test")]
        public void GivenExceptionIResultPipeline_WhenRunPipeline_ThenShouldRetriveErrorHandlingResult()
        {
            var func1 = default(Func<string, IResult<string>>);
            var func2 = default(Func<string, IResult<string>>);
            var func3 = default(Func<string, IResult<string>>);
            var result = default(string);

            GivenExceptionIResultPipeline();
            WhenRunPipeline();
            ThenShouldRetriveErrorHandlingResult();

            void GivenExceptionIResultPipeline()
            {
                func1 = new Func<string, IResult<string>>(DoSomething1);
                func2 = new Func<string, IResult<string>>(DoSomething2);
                func3 = new Func<string, IResult<string>>(DoSomething3);
            }

            void WhenRunPipeline()
            {
                var r =
                    func1("a")
                        .Pipe(b => func2(b))
                        .Pipe(c => func3(c))
                        .WhenError(e =>
                        {
                            Debug.Print($"There was an exception. {e}");
                            result = "There was an exception.";
                        });
            }

            void ThenShouldRetriveErrorHandlingResult()
            {
                result.Should().Be("There was an exception.");
            }

            IResult<string> DoSomething1(string msg)
            {
                var value = $"{msg}";
                return Result.Create(true, data: value);
            }

            IResult<string> DoSomething2(string msg)
            {
                throw new Exception("FUCK THIS");
            }

            IResult<string> DoSomething3(string msg)
            {
                throw new Exception($"ERROR");
            }
        }
    }
}