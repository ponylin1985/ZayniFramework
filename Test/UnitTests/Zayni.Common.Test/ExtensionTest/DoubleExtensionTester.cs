using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace ZayniFramework.Common.Test
{
    /// <summary>Double 或 Double? 擴充方法的測試類別
    /// </summary>
    [TestClass()]
    [TestCategory("Common.Test")]
    public class DoubleExtensionTester
    {
        /// <summary>檢查 double 是否為一個包含小數位數的 double 數值的測試。<para/>
        /// * SUT: double.IsRealDouble()<para/>
        /// * 給定一個真的包含小數位數的 double 數值。<para/>
        /// * 當檢查 double 是否為一個包含小數位數的 double 數值。<para/>
        /// * 應該得到檢查結果為真。<para/>
        /// </summary>
        [TestMethod()]
        [Description("檢查 double 是否為一個包含小數位數的 double 數值的測試。")]
        public void GivenRealDoubleNumber_WhenIsRealDouble_ThenShouldRetriveTrue()
        {
            GivenRealDoubleNumber();
            WhenIsRealDouble();
            ThenShouldRetriveTrue();

            double source1;
            double source2;
            double source3;
            double source4;
            void GivenRealDoubleNumber()
            {
                source1 = 123.58;
                source2 = 123.03;
                source3 = 123.0000009;
                source4 = -123.34;
            }

            bool result1;
            bool result2;
            bool result3;
            bool result4;
            void WhenIsRealDouble()
            {
                result1 = source1.IsRealDouble();
                result2 = source2.IsRealDouble();
                result3 = source3.IsRealDouble();
                result4 = source4.IsRealDouble();
            }

            void ThenShouldRetriveTrue()
            {
                result1.Should().BeTrue();
                result2.Should().BeTrue();
                result3.Should().BeTrue();
                result4.Should().BeTrue();
            }
        }

        /// <summary>檢查 double 是否為一個包含小數位數的 double 數值的測試。<para/>
        /// * SUT: double.IsRealDouble()<para/>
        /// * 給定一個沒有包含小數位數的 double 數值或小數位數為 0 的 decimal。<para/>
        /// * 當檢查 double 是否為一個包含小數位數的 double 數值。<para/>
        /// * 應該得到檢查結果為假。<para/>
        /// </summary>
        [TestMethod()]
        [Description("檢查 double 是否為一個包含小數位數的 double 數值的測試。")]
        public void GivenDoubleNumberWithoutDoublePart_WhenIsRealDouble_ThenShouldRetriveFalse()
        {
            GivenDoubleNumberWithoutDoublePart();
            WhenIsRealDouble();
            ThenShouldRetriveFalse();

            double source1;
            double source2;
            double source3;
            void GivenDoubleNumberWithoutDoublePart()
            {
                source1 = 123;              // 根本沒有包含小數位數
                source2 = -123;             // 實際上為一個 Int32 的負整數
                source3 = 123.0000000;      // 雖然有包含小數位數，但實際上小數位都是 0 的情況
            }

            bool result1;
            bool result2;
            bool result3;
            void WhenIsRealDouble()
            {
                result1 = source1.IsRealDouble();
                result2 = source2.IsRealDouble();
                result3 = source3.IsRealDouble();
            }

            void ThenShouldRetriveFalse()
            {
                result1.Should().BeFalse();
                result2.Should().BeFalse();
                result3.Should().BeFalse();
            }
        }

        /// <summary>檢查 double? 是否為一個包含小數位數的 double? 數值的測試。<para/>
        /// * SUT: double.IsRealDouble()<para/>
        /// * 給定一個真的包含小數位數的 double? 數值。<para/>
        /// * 當檢查 double? 是否為一個包含小數位數的 double? 數值。<para/>
        /// * 應該得到檢查結果為真。<para/>
        /// </summary>
        [TestMethod()]
        [Description("檢查 double 是否為一個包含小數位數的 double 數值的測試。")]
        public void GivenRealDoubleNullableNumber_WhenIsRealDouble_ThenShouldRetriveTrue()
        {
            GivenRealDoubleNullableNumber();
            WhenIsRealDouble();
            ThenShouldRetriveTrue();

            double? source1;
            double? source2;
            double? source3;
            double? source4;
            void GivenRealDoubleNullableNumber()
            {
                source1 = 123.58;
                source2 = 123.03;
                source3 = 123.0000009;
                source4 = -123.34;
            }

            bool result1;
            bool result2;
            bool result3;
            bool result4;
            void WhenIsRealDouble()
            {
                result1 = source1.IsRealDouble();
                result2 = source2.IsRealDouble();
                result3 = source3.IsRealDouble();
                result4 = source4.IsRealDouble();
            }

            void ThenShouldRetriveTrue()
            {
                result1.Should().BeTrue();
                result2.Should().BeTrue();
                result3.Should().BeTrue();
                result4.Should().BeTrue();
            }
        }

        /// <summary>檢查 double? 是否為一個包含小數位數的 double? 數值的測試。<para/>
        /// * SUT: double.IsRealDouble()<para/>
        /// * 給定一個沒有包含小數位數的 double? 數值或小數位數為 0 的 double?。<para/>
        /// * 當檢查 double? 是否為一個包含小數位數的 double? 數值。<para/>
        /// * 應該得到檢查結果為假。<para/>
        /// </summary>
        [TestMethod()]
        [Description("檢查 double 是否為一個包含小數位數的 double 數值的測試。")]
        public void GivenDoubleNullableNumberWithoutDoublePart_WhenIsRealDouble_ThenShouldRetriveFalse()
        {
            GivenDoubleNullableNumberWithoutDoublePart();
            WhenIsRealDouble();
            ThenShouldRetriveFalse();

            double? source1;
            double? source2;
            double? source3;
            double? source4;
            void GivenDoubleNullableNumberWithoutDoublePart()
            {
                source1 = 123;              // 根本沒有包含小數位數
                source2 = -123;             // 實際上為一個 Int32 的負整數
                source3 = 123.0000000;      // 雖然有包含小數位數，但實際上小數位都是 0 的情況
                source4 = null;             // 根本就是一個 null
            }

            bool result1;
            bool result2;
            bool result3;
            bool result4;
            void WhenIsRealDouble()
            {
                result1 = source1.IsRealDouble();
                result2 = source2.IsRealDouble();
                result3 = source3.IsRealDouble();
                result4 = source4.IsRealDouble();
            }

            void ThenShouldRetriveFalse()
            {
                result1.Should().BeFalse();
                result2.Should().BeFalse();
                result3.Should().BeFalse();
                result4.Should().BeFalse();
            }
        }
    }
}