﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace ZayniFramework.Common.Test
{
    /// <summary>Decimal 或 Decimal? 擴充方法的測試類別
    /// </summary>
    [TestClass()]
    [TestCategory("Common.Test")]
    public class DecimalExtensionTester
    {
        /// <summary>取得 decimal 數值小數位數完整長度的測試。<para/>
        /// * SUT: Decimal.LengthOfDecimalPlaces<para/>
        /// * 給定 decimal 數值，包括包含有小數位數，或者沒有小數位數的 decimal 數值。<para/>
        /// * 應該得到正確的完整小數位數長度。
        /// </summary>
        [TestMethod()]
        [Description("取得 decimal 數值小數位數完整長度的測試。")]
        public void GivenDecimalNumbers_WhenGetLengthOfDecimalPlaces_ThenShouldRetriveLengthOfDecimalPlaces()
        {
            GivenDecimalNumbers();
            WhenGetLengthOfDecimalPlaces();
            ThenShouldRetriveLengthOfDecimalPlaces();

            decimal source1;
            decimal source2;
            decimal source3;
            decimal source4;
            decimal source5;
            decimal source6;
            decimal? sourceX1;
            decimal? sourceX2;
            decimal? sourceX3;
            decimal? sourceX4;
            decimal? sourceX5;
            decimal? sourceX6;
            decimal? sourceX7;
            void GivenDecimalNumbers()
            {
                source1 = 123.58996542M;  // 具有浮點數，並且小數位數尾數沒有 0
                source2 = 123.03000000M;  // 具有浮點數，並且小數位數尾數有 0
                source3 = 0M;             // 沒有浮點數的 0
                source4 = 0.000000M;      // 具有浮點數的 0，並且小數位數尾數有 0
                source5 = 0.00M;          // 具有浮點數的 0，並且小數位數尾數有 0
                source6 = -222M;          // 整數，沒有小數位數

                sourceX1 = 123.58996542M;  // 具有浮點數，並且小數位數尾數沒有 0
                sourceX2 = 123.03000000M;  // 具有浮點數，並且小數位數尾數有 0
                sourceX3 = 0M;             // 沒有浮點數的 0
                sourceX4 = 0.000000M;      // 具有浮點數的 0，並且小數位數尾數有 0
                sourceX5 = 0.00M;          // 具有浮點數的 0，並且小數位數尾數有 0
                sourceX6 = -222M;          // 整數，沒有小數位數
                sourceX7 = null;           // 指向 null 的 decimal?
            }

            int result1;
            int result2;
            int result3;
            int result4;
            int result5;
            int result6;
            int resultX1;
            int resultX2;
            int resultX3;
            int resultX4;
            int resultX5;
            int resultX6;
            int resultX7;
            void WhenGetLengthOfDecimalPlaces()
            {
                result1 = source1.LengthOfDecimalPlaces();
                result2 = source2.LengthOfDecimalPlaces();
                result3 = source3.LengthOfDecimalPlaces();
                result4 = source4.LengthOfDecimalPlaces();
                result5 = source5.LengthOfDecimalPlaces();
                result6 = source6.LengthOfDecimalPlaces();

                resultX1 = sourceX1.LengthOfDecimalPlaces();
                resultX2 = sourceX2.LengthOfDecimalPlaces();
                resultX3 = sourceX3.LengthOfDecimalPlaces();
                resultX4 = sourceX4.LengthOfDecimalPlaces();
                resultX5 = sourceX5.LengthOfDecimalPlaces();
                resultX6 = sourceX6.LengthOfDecimalPlaces();
                resultX7 = sourceX7.LengthOfDecimalPlaces();
            }

            void ThenShouldRetriveLengthOfDecimalPlaces()
            {
                result1.Should().Be(8);
                result2.Should().Be(8);
                result3.Should().Be(0);
                result4.Should().Be(6);
                result5.Should().Be(2);
                result6.Should().Be(0);

                resultX1.Should().Be(8);
                resultX2.Should().Be(8);
                resultX3.Should().Be(0);
                resultX4.Should().Be(6);
                resultX5.Should().Be(2);
                resultX6.Should().Be(0);
                resultX7.Should().Be(0);
            }
        }

        /// <summary>取得 decimal 數值小數位數完整長度的測試。<para/>
        /// * SUT: Decimal.LengthOfDecimalPlaces<para/>
        /// * 給定 decimal 數值，包括包含有小數位數，或者沒有小數位數的 decimal 數值。<para/>
        /// * 應該得到正確的完整小數位數長度。
        /// </summary>
        [TestMethod()]
        [Description("取得 decimal 數值小數位數完整長度的測試。")]
        public void GivenDecimalNumbers_WhenGetEfftiveLengthOfDecimalPlaces_ThenShouldRetriveEfftiveLengthOfDecimalPlaces()
        {
            GivenDecimalNumbers();
            WhenGetLengthOfDecimalPlaces();
            ThenShouldRetriveLengthOfDecimalPlaces();

            decimal source1;
            decimal source2;
            decimal source3;
            decimal source4;
            decimal source5;
            decimal source6;
            decimal? sourceX1;
            decimal? sourceX2;
            decimal? sourceX3;
            decimal? sourceX4;
            decimal? sourceX5;
            decimal? sourceX6;
            decimal? sourceX7;
            void GivenDecimalNumbers()
            {
                source1 = 123.58996542M;  // 具有浮點數，並且小數位數尾數沒有 0
                source2 = 123.03000000M;  // 具有浮點數，並且小數位數尾數有 0
                source3 = 0M;             // 沒有浮點數的 0
                source4 = 0.000000M;      // 具有浮點數的 0，並且小數位數尾數有 0
                source5 = 0.00M;          // 具有浮點數的 0，並且小數位數尾數有 0
                source6 = -222M;          // 整數，沒有小數位數

                sourceX1 = 123.58996542M;  // 具有浮點數，並且小數位數尾數沒有 0
                sourceX2 = 123.03000000M;  // 具有浮點數，並且小數位數尾數有 0
                sourceX3 = 0M;             // 沒有浮點數的 0
                sourceX4 = 0.000000M;      // 具有浮點數的 0，並且小數位數尾數有 0
                sourceX5 = 0.00M;          // 具有浮點數的 0，並且小數位數尾數有 0
                sourceX6 = -222M;          // 整數，沒有小數位數
                sourceX7 = null;           // 指向 null 的 decimal?
            }

            int result1;
            int result2;
            int result3;
            int result4;
            int result5;
            int result6;
            int resultX1;
            int resultX2;
            int resultX3;
            int resultX4;
            int resultX5;
            int resultX6;
            int resultX7;
            void WhenGetLengthOfDecimalPlaces()
            {
                result1 = source1.LengthOfDecimalPlaces();
                result2 = source2.LengthOfDecimalPlaces();
                result3 = source3.LengthOfDecimalPlaces();
                result4 = source4.LengthOfDecimalPlaces();
                result5 = source5.LengthOfDecimalPlaces();
                result6 = source6.LengthOfDecimalPlaces();

                resultX1 = sourceX1.LengthOfDecimalPlaces();
                resultX2 = sourceX2.LengthOfDecimalPlaces();
                resultX3 = sourceX3.LengthOfDecimalPlaces();
                resultX4 = sourceX4.LengthOfDecimalPlaces();
                resultX5 = sourceX5.LengthOfDecimalPlaces();
                resultX6 = sourceX6.LengthOfDecimalPlaces();
                resultX7 = sourceX7.LengthOfDecimalPlaces();
            }

            void ThenShouldRetriveLengthOfDecimalPlaces()
            {
                result1.Should().Be(8);
                result2.Should().Be(8);
                result3.Should().Be(0);
                result4.Should().Be(6);
                result5.Should().Be(2);
                result6.Should().Be(0);

                resultX1.Should().Be(8);
                resultX2.Should().Be(8);
                resultX3.Should().Be(0);
                resultX4.Should().Be(6);
                resultX5.Should().Be(2);
                resultX6.Should().Be(0);
                resultX7.Should().Be(0);
            }
        }

        /// <summary>檢查 decimal 是否可以正確依照指定的小數位數進行無條件捨去處理。<para/>
        /// * SUT: Decimal.FloorDecimalPlaces<para/>
        /// * 給定 decimal 並且依照指定的小數位數執行無條件捨去。<para/>
        /// * 當執行 FloorDecimalPlaces 處理。<para/>
        /// * 應該得到 decimal 回傳值有依照指定的小數位數進行無條件捨去。
        /// </summary>
        [TestMethod()]
        [Description("檢查 decimal 是否可以正確依照指定的小數位數進行無條件捨去處理。")]
        public void GivenDecimalPlacesLength_WhenFloorDecimalPlaces_ThenShouldRetriveDecimalNumberWithGivenDecimalPlacesLength()
        {
            var lengthOfDecimalPlaces = 4;
            GivenMatchedDecimalPlacesLength();
            WhenFloorDecimalPlaces();
            ThenShouldRetriveDecimalNumberWithGivenDecimalPlacesLength();


            decimal source1;
            decimal source2;
            decimal source3;
            decimal source4;
            decimal source5;
            decimal source6;
            decimal source7;
            decimal? source8;
            decimal? source9;
            decimal? source10;
            decimal? source11;
            decimal? source12;
            decimal? source13;
            decimal? source14;
            decimal? source15;
            void GivenMatchedDecimalPlacesLength()
            {
                source1 = 123.58996542M;  // 具有浮點數，並且小數位數超出指定長度
                source2 = 123.03M;        // 具有浮點數，並且小數位數未超出指定長度
                source3 = 0M;             // 沒有浮點數的 0
                source4 = 0.000000M;      // 具有浮點數的 0，並且小數位數超出指定長度
                source5 = 0.00M;          // 具有浮點數的 0，並且小數位數未超出指定長度
                source6 = 111M;           // 沒有浮點數
                source7 = 111.1234M;      // 具有浮點數，並且小數位數等於指定長度

                source8 = 123.58996542M;  // 具有浮點數，並且小數位數超出指定長度
                source9 = 123.03M;        // 具有浮點數，並且小數位數未超出指定長度
                source10 = 0M;             // 沒有浮點數的 0
                source11 = 0.000000M;      // 具有浮點數的 0，並且小數位數超出指定長度
                source12 = 0.00M;          // 具有浮點數的 0，並且小數位數未超出指定長度
                source13 = 111M;           // 沒有浮點數
                source14 = 111.1234M;      // 具有浮點數，並且小數位數等於指定長度
                source15 = null;           // 指向 null 的 decimal?
            }

            decimal result1;
            decimal result2;
            decimal result3;
            decimal result4;
            decimal result5;
            decimal result6;
            decimal result7;
            decimal result8;
            decimal result9;
            decimal result10;
            decimal result11;
            decimal result12;
            decimal result13;
            decimal result14;
            decimal result15;
            void WhenFloorDecimalPlaces()
            {
                result1 = source1.FloorDecimalPlaces(lengthOfDecimalPlaces);
                result2 = source2.FloorDecimalPlaces(lengthOfDecimalPlaces);
                result3 = source3.FloorDecimalPlaces(lengthOfDecimalPlaces);
                result4 = source4.FloorDecimalPlaces(lengthOfDecimalPlaces);
                result5 = source5.FloorDecimalPlaces(lengthOfDecimalPlaces);
                result6 = source6.FloorDecimalPlaces(lengthOfDecimalPlaces);
                result7 = source7.FloorDecimalPlaces(lengthOfDecimalPlaces);

                result8 = source8.FloorDecimalPlaces(lengthOfDecimalPlaces);
                result9 = source9.FloorDecimalPlaces(lengthOfDecimalPlaces);
                result10 = source10.FloorDecimalPlaces(lengthOfDecimalPlaces);
                result11 = source11.FloorDecimalPlaces(lengthOfDecimalPlaces);
                result12 = source12.FloorDecimalPlaces(lengthOfDecimalPlaces);
                result13 = source13.FloorDecimalPlaces(lengthOfDecimalPlaces);
                result14 = source14.FloorDecimalPlaces(lengthOfDecimalPlaces);
                result15 = source15.FloorDecimalPlaces(lengthOfDecimalPlaces);
            }

            void ThenShouldRetriveDecimalNumberWithGivenDecimalPlacesLength()
            {
                result1.Should().Be(123.5899M);
                result2.Should().Be(123.0300M);
                result3.Should().Be(0.0000M);
                result4.Should().Be(0.0000M);
                result5.Should().Be(0.0000M);
                result6.Should().Be(111.0000M);
                result7.Should().Be(111.1234M);

                result8.Should().Be(123.5899M);
                result9.Should().Be(123.0300M);
                result10.Should().Be(0.0000M);
                result11.Should().Be(0.0000M);
                result12.Should().Be(0.0000M);
                result13.Should().Be(111.0000M);
                result14.Should().Be(111.1234M);
                result15.Should().Be(0.0000M);
            }
        }

        /// <summary>檢查 decimal 是否可以正確依照指定的小數位數進行無條件捨去處理。<para/>
        /// * SUT: Decimal.FloorDecimalPlaces<para/>
        /// * 給定 decimal 並且依照指定的小數位數執行無條件捨去。<para/>
        /// * 當執行 FloorDecimalPlaces 處理。<para/>
        /// * 應該得到 decimal 回傳值有依照指定的小數位數進行無條件捨去。
        /// </summary>
        [TestMethod()]
        [Description("檢查 decimal 是否可以正確依照指定的小數位數進行無條件捨去處理。")]
        public void Given5DecimalPlacesLength_WhenFloorDecimalPlaces_ThenShouldRetriveDecimalNumberWithGivenDecimalPlacesLength()
        {
            var lengthOfDecimalPlaces = 5;
            Given5DecimalPlacesLength();
            WhenFloorDecimalPlaces();
            ThenShouldRetriveDecimalNumberWithGivenDecimalPlacesLength();


            decimal source1;
            decimal source2;
            decimal source3;
            decimal source4;
            decimal source5;
            decimal source6;
            decimal source7;
            decimal? source8;
            decimal? source9;
            decimal? source10;
            decimal? source11;
            decimal? source12;
            decimal? source13;
            decimal? source14;
            decimal? source15;
            void Given5DecimalPlacesLength()
            {
                source1 = 123.58996542M;  // 具有浮點數，並且小數位數超出指定長度
                source2 = 123.03M;        // 具有浮點數，並且小數位數未超出指定長度
                source3 = 0M;             // 沒有浮點數的 0
                source4 = 0.000000M;      // 具有浮點數的 0，並且小數位數超出指定長度
                source5 = 0.00M;          // 具有浮點數的 0，並且小數位數未超出指定長度
                source6 = 111M;           // 沒有浮點數
                source7 = 111.12345M;     // 具有浮點數，並且小數位數等於指定長度

                source8 = 123.58996542M;  // 具有浮點數，並且小數位數超出指定長度
                source9 = 123.03M;        // 具有浮點數，並且小數位數未超出指定長度
                source10 = 0M;             // 沒有浮點數的 0
                source11 = 0.000000M;      // 具有浮點數的 0，並且小數位數超出指定長度
                source12 = 0.00M;          // 具有浮點數的 0，並且小數位數未超出指定長度
                source13 = 111M;           // 沒有浮點數
                source14 = 111.12345M;     // 具有浮點數，並且小數位數等於指定長度
                source15 = null;           // 指向 null 的 decimal?
            }

            decimal result1;
            decimal result2;
            decimal result3;
            decimal result4;
            decimal result5;
            decimal result6;
            decimal result7;
            decimal result8;
            decimal result9;
            decimal result10;
            decimal result11;
            decimal result12;
            decimal result13;
            decimal result14;
            decimal result15;
            void WhenFloorDecimalPlaces()
            {
                result1 = source1.FloorDecimalPlaces(lengthOfDecimalPlaces);
                result2 = source2.FloorDecimalPlaces(lengthOfDecimalPlaces);
                result3 = source3.FloorDecimalPlaces(lengthOfDecimalPlaces);
                result4 = source4.FloorDecimalPlaces(lengthOfDecimalPlaces);
                result5 = source5.FloorDecimalPlaces(lengthOfDecimalPlaces);
                result6 = source6.FloorDecimalPlaces(lengthOfDecimalPlaces);
                result7 = source7.FloorDecimalPlaces(lengthOfDecimalPlaces);

                result8 = source8.FloorDecimalPlaces(lengthOfDecimalPlaces);
                result9 = source9.FloorDecimalPlaces(lengthOfDecimalPlaces);
                result10 = source10.FloorDecimalPlaces(lengthOfDecimalPlaces);
                result11 = source11.FloorDecimalPlaces(lengthOfDecimalPlaces);
                result12 = source12.FloorDecimalPlaces(lengthOfDecimalPlaces);
                result13 = source13.FloorDecimalPlaces(lengthOfDecimalPlaces);
                result14 = source14.FloorDecimalPlaces(lengthOfDecimalPlaces);
                result15 = source15.FloorDecimalPlaces(lengthOfDecimalPlaces);
            }

            void ThenShouldRetriveDecimalNumberWithGivenDecimalPlacesLength()
            {
                result1.Should().Be(123.58996M);
                result2.Should().Be(123.03000M);
                result3.Should().Be(0.00000M);
                result4.Should().Be(0.00000M);
                result5.Should().Be(0.00000M);
                result6.Should().Be(111.00000M);
                result7.Should().Be(111.12345M);

                result8.Should().Be(123.58996M);
                result9.Should().Be(123.03000M);
                result10.Should().Be(0.00000M);
                result11.Should().Be(0.00000M);
                result12.Should().Be(0.00000M);
                result13.Should().Be(111.00000M);
                result14.Should().Be(111.12345M);
                result15.Should().Be(0.00000M);
            }
        }

        /// <summary>檢查 decimal 是否為一個包含小數位數的 decimal 數值的測試。<para/>
        /// * SUT: Decimal.IsRealDecimal()<para/>
        /// * 給定一個真的包含小數位數的 decimal 數值。<para/>
        /// * 當檢查 decimal 是否為一個包含小數位數的 decimal 數值。<para/>
        /// * 應該得到檢查結果為真。<para/>
        /// </summary>
        [TestMethod()]
        [Description("檢查 decimal 是否為一個包含小數位數的 decimal 數值的測試。")]
        public void GivenRealDecimalNumber_WhenIsRealDecimal_ThenShouldRetriveTrue()
        {
            GivenRealDecimalNumber();
            WhenIsRealDecimal();
            ThenShouldRetriveTrue();

            decimal source1;
            decimal source2;
            decimal source3;
            decimal source4;
            void GivenRealDecimalNumber()
            {
                source1 = 123.58M;
                source2 = 123.03M;
                source3 = 123.0000009M;
                source4 = -123.34M;
            }

            bool result1;
            bool result2;
            bool result3;
            bool result4;
            void WhenIsRealDecimal()
            {
                result1 = source1.IsRealDecimal();
                result2 = source2.IsRealDecimal();
                result3 = source3.IsRealDecimal();
                result4 = source4.IsRealDecimal();
            }

            void ThenShouldRetriveTrue()
            {
                result1.Should().BeTrue();
                result2.Should().BeTrue();
                result3.Should().BeTrue();
                result4.Should().BeTrue();
            }
        }

        /// <summary>檢查 decimal 是否為一個包含小數位數的 decimal 數值的測試。<para/>
        /// * SUT: Decimal.IsRealDecimal()<para/>
        /// * 給定一個沒有包含小數位數的 decimal 數值或小數位數為 0 的 decimal。<para/>
        /// * 當檢查 decimal 是否為一個包含小數位數的 decimal 數值。<para/>
        /// * 應該得到檢查結果為假。<para/>
        /// </summary>
        [TestMethod()]
        [Description("檢查 decimal 是否為一個包含小數位數的 decimal 數值的測試。")]
        public void GivenDecimalNumberWithoutDecimalPart_WhenIsRealDecimal_ThenShouldRetriveFalse()
        {
            GivenDecimalNumberWithoutDecimalPart();
            WhenIsRealDecimal();
            ThenShouldRetriveFalse();

            decimal source1;
            decimal source2;
            decimal source3;
            void GivenDecimalNumberWithoutDecimalPart()
            {
                source1 = 123M;             // 根本沒有包含小數位數
                source2 = -123;             // 實際上為一個 Int32 的負整數
                source3 = 123.0000000M;     // 雖然有包含小數位數，但實際上小數位都是 0 的情況
            }

            bool result1;
            bool result2;
            bool result3;
            void WhenIsRealDecimal()
            {
                result1 = source1.IsRealDecimal();
                result2 = source2.IsRealDecimal();
                result3 = source3.IsRealDecimal();
            }

            void ThenShouldRetriveFalse()
            {
                result1.Should().BeFalse();
                result2.Should().BeFalse();
                result3.Should().BeFalse();
            }
        }

        /// <summary>檢查 decimal? 是否為一個包含小數位數的 decimal? 數值的測試。<para/>
        /// * SUT: Decimal.IsRealDecimal()<para/>
        /// * 給定一個真的包含小數位數的 decimal? 數值。<para/>
        /// * 當檢查 decimal? 是否為一個包含小數位數的 decimal? 數值。<para/>
        /// * 應該得到檢查結果為真。<para/>
        /// </summary>
        [TestMethod()]
        [Description("檢查 decimal 是否為一個包含小數位數的 decimal 數值的測試。")]
        public void GivenRealDecimalNullableNumber_WhenIsRealDecimal_ThenShouldRetriveTrue()
        {
            GivenRealDecimalNullableNumber();
            WhenIsRealDecimal();
            ThenShouldRetriveTrue();

            decimal? source1;
            decimal? source2;
            decimal? source3;
            decimal? source4;
            void GivenRealDecimalNullableNumber()
            {
                source1 = 123.58M;
                source2 = 123.03M;
                source3 = 123.0000009M;
                source4 = -123.34M;
            }

            bool result1;
            bool result2;
            bool result3;
            bool result4;
            void WhenIsRealDecimal()
            {
                result1 = source1.IsRealDecimal();
                result2 = source2.IsRealDecimal();
                result3 = source3.IsRealDecimal();
                result4 = source4.IsRealDecimal();
            }

            void ThenShouldRetriveTrue()
            {
                result1.Should().BeTrue();
                result2.Should().BeTrue();
                result3.Should().BeTrue();
                result4.Should().BeTrue();
            }
        }

        /// <summary>檢查 decimal? 是否為一個包含小數位數的 decimal? 數值的測試。<para/>
        /// * SUT: Decimal.IsRealDecimal()<para/>
        /// * 給定一個沒有包含小數位數的 decimal? 數值或小數位數為 0 的 decimal?。<para/>
        /// * 當檢查 decimal? 是否為一個包含小數位數的 decimal? 數值。<para/>
        /// * 應該得到檢查結果為假。<para/>
        /// </summary>
        [TestMethod()]
        [Description("檢查 decimal 是否為一個包含小數位數的 decimal 數值的測試。")]
        public void GivenDecimalNullableNumberWithoutDecimalPart_WhenIsRealDecimal_ThenShouldRetriveFalse()
        {
            GivenDecimalNullableNumberWithoutDecimalPart();
            WhenIsRealDecimal();
            ThenShouldRetriveFalse();

            decimal? source1;
            decimal? source2;
            decimal? source3;
            decimal? source4;
            void GivenDecimalNullableNumberWithoutDecimalPart()
            {
                source1 = 123M;             // 根本沒有包含小數位數
                source2 = -123;             // 實際上為一個 Int32 的負整數
                source3 = 123.0000000M;     // 雖然有包含小數位數，但實際上小數位都是 0 的情況
                source4 = null;             // 根本就是一個 null
            }

            bool result1;
            bool result2;
            bool result3;
            bool result4;
            void WhenIsRealDecimal()
            {
                result1 = source1.IsRealDecimal();
                result2 = source2.IsRealDecimal();
                result3 = source3.IsRealDecimal();
                result4 = source4.IsRealDecimal();
            }

            void ThenShouldRetriveFalse()
            {
                result1.Should().BeFalse();
                result2.Should().BeFalse();
                result3.Should().BeFalse();
                result4.Should().BeFalse();
            }
        }
    }
}
