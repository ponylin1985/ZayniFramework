﻿using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace ZayniFramework.Common.Test
{
    /// <summary>陣列擴充方法的測試類別
    /// </summary>
    [TestClass()]
    [TestCategory("Common.Test")]
    public class ArrayExtensionTester
    {
        /// <summary>對字串陣列以指定分格符號串接所有陣列內元素的測試 JoinToken的測試
        /// </summary>
        [TestMethod()]
        [Description("對字串陣列以指定分格符號串接所有陣列內元素的測試 JoinToken的測試")]
        public void IEnumerableJoinTokenTest()
        {
            var models = new string[] { "AA", "BB", "CC", "DD" };
            var result = models.JoinToken("-");
            Assert.AreEqual("AA-BB-CC-DD", result);
        }

        [TestMethod()]
        [Description("對IsNullOrEmpty擴充方法的正常測試")]
        public void IsNullOrEmptyTest()
        {
            string[] stringArray1 = [];
            Assert.IsTrue(stringArray1.IsNullOrEmpty());

            string[] stringArray2 = null;
            Assert.IsTrue(stringArray2.IsNullOrEmpty());

            string[] stringArray3 = ["k", "u"];
            Assert.IsFalse(stringArray3.IsNullOrEmpty());
        }

        [TestMethod()]
        [Description("對IsNotNullAndEmptyArray擴充方法的正常測試")]
        public void IsNotNullAndEmptyArrayTest()
        {
            string[] stringArray1 = [];
            Assert.IsFalse(stringArray1.IsNotNullOrEmpty());

            string[] stringArray2 = null;
            Assert.IsFalse(stringArray2.IsNotNullOrEmpty());

            string[] stringArray3 = ["k", "u"];
            Assert.IsTrue(stringArray3.IsNotNullOrEmpty());
        }

        [TestMethod()]
        [Description("Binary二進位資料轉換成字串的測試")]
        public void ByteArrayGetStringTest()
        {
            var test = "This is a test string.";

            var binary = test.ToUtf8Bytes();
            Assert.IsTrue(binary.IsNotNullOrEmpty());

            var result = binary.GetStringFromUtf8Bytes();
            Assert.AreEqual(test, result);
        }

        [TestMethod()]
        [Description("Binary二進位資料進行GZip解壓縮成字串的測試")]
        public void GZipDecompress2StringTest()
        {
            var test = "This is a test string. This is a test string. This is a test string. This is a test string. This is a test string. This is a test string. This is a test string.";
            var result = test.GZipCompress2Binary();
            Assert.IsTrue(result.IsNotNullOrEmpty());
            Assert.IsTrue(result.Length < test.Length);

            var recover = result.GZipDecompress2String();
            Assert.AreEqual(test, recover);
        }
    }
}
