﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text.Json;
using ZayniFramework.Common.Test.TestModel;


namespace ZayniFramework.Common.Test
{
    /// <summary>測試用的類別
    /// </summary>
    public class MemberModel
    {
        /// <summary>名稱
        /// </summary>
        /// <value></value>
        public string Name { get; set; }

        /// <summary>測試用的方法
        /// </summary>
        /// <param name="message">訊息</param>
        /// <returns>測試字串</returns>
        public string SayHello(string message) => $"Hello, my name is {Name}. {message}";
    }

    /// <summary>Object 物件擴充方法測試類別
    /// </summary>
    [TestClass()]
    [TestCategory("Common.Test")]
    public class ObjectExtensionTester
    {
        /// <summary>ConvertTo 資料轉換的測試
        /// </summary>
        [TestMethod()]
        [Description("ConvertTo 資料轉換的測試")]
        public void ConvertTo_Test()
        {
            var source = new UserModel()
            {
                Name = "Judy",
                Sex = "female",
                Age = 19,
                Birthday = new DateTime(2002, 2, 14)
            };

            var target = source.ConvertTo<UserModel_ConvertTest>();
            Assert.IsNotNull(target);
            Assert.AreEqual(source.Age + "", target.Age);
            Console.WriteLine($"Target 1: {JsonSerializer.Serialize(target, new JsonSerializerOptions() { WriteIndented = true })}");

            // ====================================

            source = new UserModel()
            {
                Name = "Judy",
                Sex = "female",
                Age = 19,
                Birthday = new DateTime(2002, 2, 14),
                Balance = "5278.7788"
            };

            target = source.ConvertTo<UserModel_ConvertTest>();
            Assert.IsNotNull(target);
            Assert.AreEqual(decimal.Parse(source.Balance), target.Balance);
            Console.WriteLine($"Target 2: {JsonSerializer.Serialize(target, new JsonSerializerOptions() { WriteIndented = true })}");

            // ====================================

            source = new UserModel()
            {
                Name = "Judy",
                Sex = "female",
                Age = 19,
                Birthday = new DateTime(2002, 2, 14),
                Balance = "This is not a decimal value."
            };

            target = source.ConvertTo<UserModel_ConvertTest>();
            Assert.IsNotNull(target);
            Assert.AreEqual(0, target.Balance);
            Console.WriteLine($"Target 3: {JsonSerializer.Serialize(target, new JsonSerializerOptions() { WriteIndented = true })}");

            // ====================================

            source = new UserModel()
            {
                Name = "Judy",
                Sex = "female",
                Age = 19,
                Birthday = new DateTime(2002, 2, 14),
                Balance = "7788.1234",
                ToyCount = "100",
                ExpireDate = "2020-02-04"
            };

            target = source.ConvertTo<UserModel_ConvertTest>();
            Assert.IsNotNull(target);
            Assert.AreEqual(source.Age + "", target.Age);
            Assert.AreEqual(decimal.Parse(source.Balance), target.Balance);
            Assert.AreEqual(long.Parse(source.ToyCount), target.ToyCount);
            Assert.AreEqual(DateTime.Parse(source.ExpireDate), (DateTime)target.ExpireDate);
            Console.WriteLine($"Target 4: {JsonSerializer.Serialize(target, new JsonSerializerOptions() { WriteIndented = true })}");

            // ====================================

            source = new UserModel()
            {
                Name = "Judy",
                Sex = "female",
                Age = 19,
                Birthday = new DateTime(2002, 2, 14),
                Balance = "7788.1234",
                ToyCount = "This is not a valid Int64 number string...",
                ExpireDate = "This is not a valid date time string..."
            };

            target = source.ConvertTo<UserModel_ConvertTest>();
            Assert.IsNotNull(target);
            Assert.AreEqual(source.Age + "", target.Age);
            Assert.AreEqual(decimal.Parse(source.Balance), target.Balance);
            Assert.IsNull(target.ToyCount);
            Assert.IsNull(target.ExpireDate);
            Console.WriteLine($"Target 5: {JsonSerializer.Serialize(target, new JsonSerializerOptions() { WriteIndented = true })}");
        }

        /// <summary>轉換成字典資料集合的測試
        /// </summary>
        [TestMethod()]
        [Description("轉換成字典資料集合的測試")]
        public void ConvertToDictionary_Test()
        {
            var model = new UserModel
            {
                Name = "Amber",
                Age = 23,
                Sex = "female"
            };

            var dict = model.ConvertToDictionary();

            var name = dict["Name"] + "";
            var sex = dict["Sex"] + "";
            var age = int.Parse(dict["Age"] + "");

            Assert.AreEqual(model.Name, name);
            Assert.AreEqual(model.Age, age);
            Assert.AreEqual(model.Sex, sex);

            // =====================================================

            var source = new UserModel
            {
                Name = "Sylvia",
                Age = 26,
                Sex = "F"
            };

            var d = source.ConvertToStringDictionary();

            var name2 = d["Name"];
            var sex2 = d["Sex"];
            var age2 = d["Age"];

            Assert.AreEqual(source.Name, name2);
            Assert.AreEqual(source.Age + "", age2);
            Assert.AreEqual(source.Sex, sex2);

            // =====================================================

            var now = DateTime.Now;

            var obj = new
            {
                Message = "Hello World",
                DoB = now,
                Money = 324.52M,
                Cash = 76.123D,
                Age = 33,
                User = model
            };

            dict = obj.ConvertToDictionary();

            Assert.AreEqual(obj.Message, dict["Message"] + "");
            Assert.AreEqual(obj.Money, decimal.Parse(dict["Money"] + ""));
            Assert.AreEqual(obj.Cash, double.Parse(dict["Cash"] + ""));
            Assert.AreEqual(obj.Age, int.Parse(dict["Age"] + ""));

            var dob = (DateTime)dict["DoB"];
            Assert.AreEqual(obj.DoB.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture), dob.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture));
            Assert.AreEqual(obj.DoB, dob);

            var userModel = dict["User"] as UserModel;
            Assert.AreEqual(model.Name, userModel.Name);
            Assert.AreEqual(model.Age, userModel.Age);
            Assert.AreEqual(model.Sex, userModel.Sex);

        }

        /// <summary>將字典資料集合轉換成動態物件的測試
        /// </summary>
        [TestMethod()]
        [Description("將字典資料集合轉換成動態物件的測試")]
        public void ConvertToDynamicObj_Test()
        {
            var source = new Dictionary<string, object>()
            {
                ["HappyBear"] = "Ya我是Happy Bear",
                ["Birthday"] = DateTime.Now,
                ["MagicNumber"] = 7765,
                ["UserInfo"] = new UserModel() { Name = "Sylvia", Sex = "female", Age = 27 }
            };

            dynamic d = source.ConvertToDynamicObj();
            Assert.AreEqual(source["HappyBear"], d.HappyBear);
            Assert.AreEqual(source["Birthday"], d.Birthday);
            Assert.AreEqual(source["MagicNumber"], d.MagicNumber);

            UserModel uModel = d.UserInfo;
            Assert.AreEqual("Sylvia", uModel.Name);
            Assert.AreEqual("female", uModel.Sex);
            Assert.AreEqual(27, uModel.Age);

            // =====================================================

            var model = new UserModel
            {
                Name = "Amber",
                Age = 23,
                Sex = "female"
            };

            var dict = model.ConvertToDictionary();

            var name = dict["Name"] + "";
            var sex = dict["Sex"] + "";
            var age = int.Parse(dict["Age"] + "");

            Assert.AreEqual(model.Name, name);
            Assert.AreEqual(model.Age, age);
            Assert.AreEqual(model.Sex, sex);

            dynamic dynamicObj = dict.ConvertToDynamicObj();
            Assert.AreEqual(model.Name, dynamicObj.Name);
            Assert.AreEqual(model.Age, dynamicObj.Age);
            Assert.AreEqual(model.Sex, dynamicObj.Sex);

            // =====================================================

            var now = DateTime.Now;

            var obj = new
            {
                Message = "Hello World",
                DoB = now,
                Money = 324.52M,
                Cash = 76.123D,
                Age = 33,
                User = model
            };

            dict = obj.ConvertToDictionary();

            Assert.AreEqual(obj.Message, dict["Message"] + "");
            Assert.AreEqual(obj.Money, decimal.Parse(dict["Money"] + ""));
            Assert.AreEqual(obj.Cash, double.Parse(dict["Cash"] + ""));
            Assert.AreEqual(obj.Age, int.Parse(dict["Age"] + ""));

            var dob = (DateTime)dict["DoB"];
            Assert.AreEqual(obj.DoB.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture), dob.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture));
            Assert.AreEqual(obj.DoB, dob);

            var userModel = dict["User"] as UserModel;
            Assert.AreEqual(model.Name, userModel.Name);
            Assert.AreEqual(model.Age, userModel.Age);
            Assert.AreEqual(model.Sex, userModel.Sex);

            dynamicObj = dict.ConvertToDynamicObj();
            Assert.AreEqual(obj.Message, dynamicObj.Message);
            Assert.AreEqual(obj.Money, dynamicObj.Money);
            Assert.AreEqual(obj.Cash, dynamicObj.Cash);
            Assert.AreEqual(obj.Age, dynamicObj.Age);

            var user = dynamicObj.User as UserModel;
            Assert.AreEqual(model.Name, user.Name);
            Assert.AreEqual(model.Age, user.Age);
            Assert.AreEqual(model.Sex, user.Sex);
        }

        /// <summary>取得物件的屬性值的正向測試
        /// </summary>
        [TestMethod()]
        [Description("取得物件的屬性值的正向測試")]
        public void GetPropertyValueTest()
        {
            var model = new MemberModel()
            {
                Name = "Kate"
            };

            var obj = model.GetPropertyValue("Name");
            Assert.IsNotNull(obj);

            var name = obj + "";
            Assert.AreEqual(model.Name, name);

            name = model.GetPropertyValue<string>("Name");
            Assert.AreEqual(model.Name, name);
        }

        /// <summary>取得物件的屬性值的反向測試
        /// </summary>
        [TestMethod()]
        [Description("取得物件的屬性值的反向測試")]
        public void GetPropertyValue_RevertTest()
        {
            var model = new MemberModel()
            {
                Name = "Kate"
            };

            var obj = model.GetPropertyValue("Age");
            Assert.IsNull(obj);

            var objSome = new
            {
                UserName = "DDD"
            };

            obj = objSome.GetPropertyValue("Name");
            Assert.IsNull(obj);
        }

        /// <summary>反射呼叫物件方法的正向測試
        /// </summary>
        [TestMethod()]
        [Description("反射呼叫物件方法的正向測試")]
        public void MethodInvoke_Test()
        {
            var model = new MemberModel() { Name = "Sylvia" };
            var result = model.MethodInvoke("SayHello", "This is a test.");
            Assert.AreEqual("Hello, my name is Sylvia. This is a test.", result);
        }

        [TestMethod()]
        [Description("IsNull回呼處理的正常測試")]
        public void IsNullCallbackTest1()
        {
            object test = null;     // 目標變數是Null的情況，回呼應該要被執行到
            var str = "";

            test.IsNull(() =>
            {
                // 在這邊可以做一些當test是Null情況下的事情!
                test = "I love Sylvia.";
                str = "I love Sylvia very much.";
            });

            Assert.AreEqual(test + "", "I love Sylvia.");
            Assert.AreEqual(str, "I love Sylvia very much.");

            // ===============================================================================================

            UserModel model = null;     // 目標變數是Null的情況，回呼應該要被執行到

            var isNull = model.IsNull(() =>
            {
                // 在這邊可以做一些當model是Null情況下的事情! 譬如重新初始化Model資料模型的屬性
                model = new UserModel()
                {
                    Name = "Sylvia",
                    Age = 35,
                    Sex = "Female"
                };
            });

            if (null == model)
            {
                model = new UserModel()
                {
                    Name = "Sylvia",
                    Age = 35,
                    Sex = "Female"
                };
            }

            Assert.IsTrue(isNull);
            Assert.AreEqual("Sylvia", model.Name);
            Assert.AreEqual(35, model.Age);
            Assert.AreEqual("Female", model.Sex);
        }

        [TestMethod()]
        [Description("IsNull回呼處理的反向測試")]
        public void IsNullCallbackTest2()
        {
            var model = new UserModel()   // 目標變數不是Null的情況，回呼不應該被執行到
            {
                Name = "Sylvia",
                Age = 35,
                Sex = "Female"
            };

            var isNull = model.IsNull(() =>
            {
                // 在這邊可以做一些當test是Null情況下的事情! 譬如重新初始化Model資料模型的屬性
                model = new UserModel()
                {
                    Name = "Vivian",
                    Age = 29,
                    Sex = "female"
                };
            });

            Assert.IsFalse(isNull);
            Assert.AreEqual("Sylvia", model.Name);
            Assert.AreEqual(35, model.Age);
            Assert.AreEqual("Female", model.Sex);
        }

        [TestMethod()]
        [Description("IsNotNull回呼處理的正常測試")]
        public void IsNotNullCallbackTest1()
        {
            var test = new object();     // 目標變數不是Null的情況，回呼應該要被執行到
            var str = "";

            test.IsNotNull(m =>
            {
                // 在這邊可以做一些當test不是Null情況下的事情!
                test = "I love Sylvia";
                str = "I love Sylvia";
            });

            Assert.AreEqual(test + "", "I love Sylvia");
            Assert.AreEqual(str, "I love Sylvia");

            var model = new UserModel()
            {
                Name = "Sylvia",
                Sex = "Female"
            };

            var g = "";
            var s = "";

            var isNotNull = model.IsNotNull(m =>
            {
                g = m.Name + " HH";     // 這是第一種方式可以拿到目標物件的方式，使用ZayniFramework 框架執行回呼時傳入的dynamic物件
                s = model.Sex;
            });

            Assert.AreEqual(g, "Sylvia HH");
            Assert.AreEqual(s, "Female");
            Assert.IsTrue(isNotNull);
        }

        [TestMethod()]
        [Description("IsNotNull回呼處理的反向測試")]
        public void IsNotNullCallbackTest2()
        {
            object test = null;     // 目標變數是Null的情況，回呼不應該被執行到
            var str = "";

            test.IsNotNull(m =>
            {
                // 在這邊可以做一些當test不是Null情況下的事情!
                test = "I love Sylvia";
                str = "I love Sylvia";
            });

            Assert.IsNull(test);
            Assert.IsTrue(string.IsNullOrWhiteSpace(str));
        }

        [TestMethod()]
        [Description("IsNull檢查是否為Null值的向單元測試")]
        public void IsNullTest()
        {
            object test = null;
            dynamic result = test.IsNull(new { Message = "我愛Sylvia姊姊!" });
            Assert.AreEqual("我愛Sylvia姊姊!", result.Message);
        }

        [TestMethod()]
        [Description("IsNull檢查是否為Null值的向單元測試")]
        public void IsNullTest2()
        {
            var test = "Sylvia姊姊也很愛Pony!!!";
            var result = test.IsNull("我愛Sylvia姊姊!") + "";
            Assert.AreNotEqual("我愛Sylvia姊姊!", result);
            Assert.AreEqual("Sylvia姊姊也很愛Pony!!!", result);
        }

        [TestMethod()]
        [Description("IsNull檢查是否為Null值的向單元測試")]
        public void IsNullTest3()
        {
            string test = null;
            var result = test.IsNull();
            Assert.IsTrue(result);
        }

        [TestMethod()]
        [Description("IsNull檢查是否為Null值的向單元測試")]
        public void IsNullTest4()
        {
            var test = "Sylvia姊姊也很愛Pony!!!";
            var result = test.IsNull();
            Assert.IsFalse(result);
        }

        [TestMethod()]
        [Description("IsNotNull的向單元測試")]
        public void IsNotNullTest()
        {
            var test = "Sylvia姊姊也很愛Pony!!!";
            var result = test.IsNotNull();
            Assert.IsTrue(result);
        }

        [TestMethod()]
        [Description("IsNotNull的向單元測試")]
        public void IsNotNullTest2()
        {
            string test = null;
            var result = test.IsNotNull();
            Assert.IsFalse(result);
        }

        [TestMethod()]
        [Description("對ObjectExtension的IsNull和IsNotNull的正向單元測試")]
        public void ObjectIsNullTest()
        {
            var list = new List<string>();
            Assert.IsFalse(list.IsNull());
            Assert.IsTrue(list.IsNotNull());

            MemberModel m = null;
            Assert.IsFalse(m.IsNotNull());
            Assert.IsTrue(m.IsNull());
        }

        [TestMethod()]
        [Description("對ObjectExtension的ToList和ToList的正向單元測試")]
        public void ObjectArrayToListTest()
        {
            var array = new object[] { new MemberModel { Name = "Jack" }, new MemberModel { Name = "Jack2" }, new MemberModel { Name = "Jack3" } };
            var result = array.ToList<MemberModel>();

            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Count);
            Assert.IsTrue(result[0] is not null);
            Assert.AreEqual("Jack", result[0].Name);
            Assert.AreEqual("Jack2", result[1].Name);
            Assert.AreEqual("Jack3", result[2].Name);
        }

        [TestMethod()]
        [Description("檢查目標物件是否為指定父型別的子型態物件的測試")]
        public void IsTypeofTest1()
        {
            var type = Type.GetType("System.Exception");
            var ex = new DataAccessException();
            Assert.IsTrue(ex.IsTypeof(type));

            Assert.IsTrue(ex.IsTypeof(typeof(DataAccessException)));

            var str = "test";
            Assert.IsTrue(str.IsTypeof(typeof(string)));
        }

        [TestMethod()]
        [Description("檢查目標物件是否為指定父型別的子型態物件的測試 - 反向測試")]
        public void IsTypeofTest2()
        {
            var type = Type.GetType("System.Exception");
            var str = "test";
            Assert.IsFalse(str.IsTypeof(type));
        }

        [TestMethod()]
        [Description("檢查目標物件是否為指定父型別的子型態物件的測試")]
        public void IsTypeofTest4()
        {
            var model = new UserModel();
            var isOk = model.IsTypeof(typeof(IUser));
            Assert.IsTrue(isOk);

            var model2 = new UserModel();
            var isOk2 = model2.IsTypeof(typeof(IUser));
            Assert.IsTrue(isOk2);
        }

        [TestMethod()]
        [Description("檢查物件陣列中是否有包含任何Null的元素的測試")]
        public void HasNullElementTest()
        {
            object[] arr = [null, new()];
            var has = arr.HasNullElements();
            Assert.IsTrue(has);
        }

        [TestMethod()]
        [Description("檢查物件陣列中是否有包含任何Null的元素的反向測試")]
        public void HasNullElementTest2()
        {
            object[] arr = [new(), new(), new()];
            var has = arr.HasNullElements();
            Assert.IsFalse(has);
        }
    }
}
