﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Reflection;


namespace ZayniFramework.Common.Test
{
    /// <summary>
    /// </summary>
    [TestClass()]
    [TestCategory("Common.Test")]
    public class AppSettingsLoaderTester
    {
        #region 測試組件初始化

        /// <summary>初始化整個 Test project
        /// </summary>
        /// <param name="testcontext"></param>
        [AssemblyInitialize()]
        public static void Init(TestContext testcontext)
        {
            // 在 .NET Core 版本的 MSTest，一定要自行指定 .config 的完整絕對路徑到 ConfigManagement.ConfigFullPath 中，整個 ZayniFramework 舊有在 XML Config 的機制才會正常運作！
            var path = $"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}/zayni.json";
            ConfigManager.Init(path);
        }

        #endregion 測試組件初始化
    }
}
