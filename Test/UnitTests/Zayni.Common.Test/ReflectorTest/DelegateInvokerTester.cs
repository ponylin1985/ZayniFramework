﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace ZayniFramework.Common.Test
{
    /// <summary>DelegateInvoker 通用委派執行器的測試類別
    /// </summary>
    [TestClass()]
    [TestCategory("Common.Test")]
    public class DelegateInvokerTester
    {
        /// <summary>執行次數
        /// </summary>
        private static int _count = 3000000;

        /// <summary>測試類別初始化
        /// </summary>
        /// <param name="context"></param>
        public static void Init(TestContext context)
        {
#if DEBUG
            _count = 1000;
#endif
        }

        // Duration: 420 ms ~ 486 ms
        /// <summary>直接呼叫物件方法的效能測試<para/>
        /// 在 .NET Framewokr 4.5 下，這居然反而是效能第二好的呼叫方式... XDDDDDDDDD
        /// </summary>
        [TestMethod()]
        [Description("直接呼叫物件方法的效能測試")]
        public void DirectCall_Test()
        {
            _ = new Something();

            for (var i = 0; i < _count; i++)
            {
                var model = new Something().GetUserData("Amy", "female");
                Assert.IsNotNull(model);
                Assert.AreEqual("UnitTest Amy", model.Name);
                Assert.AreEqual("female", model.Sex);
            }
        }

        // Duration: 420 ms ~ 452 ms
        /// <summary>採用強型別 Delegate 委派直些呼叫的效能測試<para/>
        /// 目前測試下，使用這種呼叫方式，在 .NET Framework 4.5 下這是呼叫方法最好的效能! Oh, my GODDDDDDDD!!!
        /// </summary>
        [TestMethod()]
        [Description("採用強型別 Delegate 委派直些呼叫的效能測試")]
        public void DelegateDirectInvoke_Test()
        {
            Func<string, string, UserDataModel> func = new Something().GetUserData;

            for (var i = 0; i < _count; i++)
            {
                var model = func.Invoke("Amy", "female");
                Assert.IsNotNull(model);
                Assert.AreEqual("UnitTest Amy", model.Name);
                Assert.AreEqual("female", model.Sex);
            }
        }

        // Duration: 1 秒
        /// <summary>採用反射 MethodInfo.Invoke 的效能測試 (理論上這樣效能較好，只是比較麻煩)
        /// </summary>
        [TestMethod()]
        [Description("採用反射 MethodInfo.Invoke 的效能測試")]
        public void ReflectMethodInfoInvoke_Test()
        {
            var obj = new Something();
            var method = obj.GetMethod(nameof(obj.GetUserData));
            var parameters = new object[] { "Amy", "female" };

            for (var i = 0; i < _count; i++)
            {
                var model = (UserDataModel)method.Invoke(obj, parameters);
                Assert.IsNotNull(model);
                Assert.AreEqual("UnitTest Amy", model.Name);
                Assert.AreEqual("female", model.Sex);
            }
        }

        // Duration: 1 秒
        /// <summary>採用反射 MethodInfo.Invoke 的效能測試 (效能應該較差，但程式碼較為簡便)
        /// </summary>
        [TestMethod()]
        [Description("採用反射 MethodInfo.Invoke 的效能測試")]
        public void ReflectMethodInfoInvoke_Test2()
        {
            var obj = new Something();
            var methodName = nameof(obj.GetUserData);

            for (var i = 0; i < _count; i++)
            {
                var model = (UserDataModel)obj.MethodInvoke(methodName, "Amy", "female");
                Assert.IsNotNull(model);
                Assert.AreEqual("UnitTest Amy", model.Name);
                Assert.AreEqual("female", model.Sex);
            }
        }

        // Duration: 1 秒
        /// <summary>採用 DelegateInvoker 的效能測試<para/>
        /// 如果情境為只能得到一個通用的 Delegate 基底型別，目前在 DelegateInvoker 中的實作，應該是最好的效能作法。<para/>
        /// 基本上，效能應該跟用 MethodInfo.Invoke() 的作法會差不多。
        /// </summary>
        [TestMethod()]
        [Description("採用 DelegateInvoker 的效能測試")]
        public void DelegateInvoker_Test()
        {
            var obj = new Something();
            Delegate action = (Func<string, string, UserDataModel>)new Something().GetUserData;

            for (var i = 0; i < _count; i++)
            {
                var model = DelegateInvoker.ExecResult<UserDataModel>(action, "Amy", "female");
                Assert.IsNotNull(model);
                Assert.AreEqual("UnitTest Amy", model.Name);
                Assert.AreEqual("female", model.Sex);
            }
        }

        // Duration: 1 秒
        /// <summary>採用 DelegateInvoker 的效能測試 2
        /// </summary>
        [TestMethod()]
        [Description("採用 DelegateInvoker 的效能測試 2")]
        public void DelegateInvoker_Test2()
        {
            var obj = new Something();
            var action = Delegate.CreateDelegate(typeof(Func<string, string, UserDataModel>), obj, nameof(obj.GetUserData));

            for (var i = 0; i < _count; i++)
            {
                var model = DelegateInvoker.ExecResult<UserDataModel>(action, "Amy", "female");
                Assert.IsNotNull(model);
                Assert.AreEqual("UnitTest Amy", model.Name);
                Assert.AreEqual("female", model.Sex);
            }
        }

        // Duration: 1 秒
        /// <summary>採用 DelegateInvoker 的效能測試<para/>
        /// 如果情境為只能得到一個通用的 Delegate 基底型別，目前在 DelegateInvoker 中的實作，應該是最好的效能作法。<para/>
        /// 基本上，效能應該跟用 MethodInfo.Invoke() 的作法會差不多。
        /// </summary>
        [TestMethod()]
        [Description("採用 DelegateInvoker 的效能測試")]
        public void DelegateInvoker_Static_Test()
        {
            var obj = new Something();
            var action = (Func<string, string, UserDataModel>)new Something().GetUser;

            for (var i = 0; i < _count; i++)
            {
                var model = DelegateInvoker.ExecResult<UserDataModel>(action, "Amy", "female");
                Assert.IsNotNull(model);
                Assert.AreEqual("UnitTest Amy", model.Name);
                Assert.AreEqual("female", model.Sex);
            }
        }

        // Duration: 3 秒
        /// <summary>採用 Delegate.DynamicInvoke 的效能測試，這反而比較最慢。
        /// </summary>
        [TestMethod()]
        [Description("採用 Delegate.DynamicInvoke 的效能測試")]
        public void DelegateDynamicInvoke_Test()
        {
            _ = new Something();
            var action = (Func<string, string, UserDataModel>)new Something().GetUserData;

            for (var i = 0; i < _count; i++)
            {
                var model = (UserDataModel)action.DynamicInvoke("Amy", "female");
                Assert.IsNotNull(model);
                Assert.AreEqual("UnitTest Amy", model.Name);
                Assert.AreEqual("female", model.Sex);
            }
        }
    }

    /// <summary>測試用類別
    /// </summary>
    public class Something
    {
        /// <summary>測試用的目標 Instance 方法
        /// </summary>
        /// <param name="name"></param>
        /// <param name="sex"></param>
        /// <returns></returns>
        public UserDataModel GetUserData(string name, string sex)
        {
            return new UserDataModel() { Name = $"UnitTest {name}", Sex = sex };
        }

        /// <summary>測試用的目標 Static 方法
        /// </summary>
        /// <param name="name"></param>
        /// <param name="sex"></param>
        /// <returns></returns>
        public UserDataModel GetUser(string name, string sex)
        {
            return new() { Name = $"UnitTest {name}", Sex = sex };
        }
    }
}
