using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;
using System.Text.Json;
using ZayniFramework.Common.Test.TestModel;


namespace ZayniFramework.Common.Test
{
    /// <summary>ObjectCreator 的測試類別
    /// </summary>
    [TestClass()]
    public class ObjectCreatorTester
    {
        /// <summary>建立物件 CreateInstance 的測試
        /// </summary>
        [TestMethod()]
        [Description("建立物件 CreateInstance 的測試")]
        [TestCategory("Common.Test")]
        public void CreateInstance_Test()
        {
            var birthday = new DateTime(2001, 2, 21).ToUtcKind();

            // ==========================

            var model = ObjectCreator<UserModel>.CreateInstance();
            Assert.IsNotNull(model);

            model.Name = "Kate";
            model.Sex = "Female";
            model.Age = 21;
            model.Birthday = birthday;
            Assert.AreEqual("Kate", model.Name);
            Assert.AreEqual("Female", model.Sex);
            Assert.AreEqual(21, model.Age);
            Assert.AreEqual(birthday, model.Birthday);

            var json = JsonSerializer.Serialize(model);
            Console.WriteLine($"Use generic object creator.");
            Console.WriteLine(json);
            Debug.Print(json);

            // ==========================

            var creator = new ObjectCreator(typeof(UserModel));
            var obj = creator.CreateInstance();
            Assert.IsNotNull(obj);

            var userModel = obj as UserModel;
            Assert.IsNotNull(userModel);

            userModel.Name = "Amber";
            userModel.Sex = "Slut";
            userModel.Age = 23;
            userModel.Birthday = birthday;
            Assert.AreEqual("Amber", userModel.Name);
            Assert.AreEqual("Slut", userModel.Sex);
            Assert.AreEqual(23, userModel.Age);
            Assert.AreEqual(birthday, userModel.Birthday);

            json = JsonSerializer.Serialize(userModel, new JsonSerializerOptions() { WriteIndented = true });
            Console.WriteLine($"Use non-generic object creator.");
            Console.WriteLine(json);
            Debug.Print(json);
        }

        /// <summary>建立物件 CreateInstance 的效能測試
        /// </summary>
        [TestMethod()]
        [Description("建立物件 CreateInstance 的效能測試")]
        public void CreateInstance_Performance_Test()
        {
            var begin = DateTime.Now;

            for (var i = 0; i < 10000000; i++)
            {
                var model = new UserModel();
                Assert.IsNotNull(model);
            }

            var end = DateTime.Now;
            var duration = (end - begin).TotalMilliseconds;

            Debug.Print($"Use new operator duration: {duration} ms.");
            Console.WriteLine($"Use new operator duration: {duration} ms.");

            // ========================================

            begin = DateTime.Now;

            // 注意，如果要使用 non-generic 的 ObjectCreator 要重覆產生物件，需要先 Initialize 一次，然後再進行 CreateInstance 方法呼叫。
            var creator = new ObjectCreator(typeof(UserModel));

            for (var i = 0; i < 10000000; i++)
            {
                // var model = ObjectCreator<UserModel>.CreateInstance();
                // Assert.IsNotNull( model );

                var model = creator.CreateInstance();
                Assert.IsNotNull(model);
            }

            end = DateTime.Now;
            duration = (end - begin).TotalMilliseconds;

            Debug.Print($"Use ZayniFramework ObjectCreator duration: {duration} ms.");
            Console.WriteLine($"Use ZayniFramework ObjectCreator.CreateInstance duration: {duration} ms.");

            // ========================================

            begin = DateTime.Now;

            for (var i = 0; i < 10000000; i++)
            {
                var model = Activator.CreateInstance<UserModel>();
                Assert.IsNotNull(model);
            }

            end = DateTime.Now;
            duration = (end - begin).TotalMilliseconds;

            Debug.Print($"Use Activator.CreateInstance duration: {duration} ms.");
            Console.WriteLine($"Use Activator.CreateInstance duration: {duration} ms.");
        }
    }
}