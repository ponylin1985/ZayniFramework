﻿using ZayniFramework.Common.Dynamic;


namespace ZayniFramework.Common.Test
{
    /// <summary>執行後置攔截的動作
    /// </summary>
    public class AfterInterceptor : AfterMethodInterceptor
    {
        /// <summary>執行後置攔截的動作
        /// </summary>
        /// <param name="result"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public override object AspectExecute(object result, dynamic parameters = null)
        {
            _ = DynamicHelper.HasProperty(parameters, "Name") ? parameters.Name : "";
            var ret = result + "";
            return ret + " After ";
        }
    }
}
