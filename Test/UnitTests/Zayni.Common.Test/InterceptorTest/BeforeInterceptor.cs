﻿using System.Linq;
using ZayniFramework.Common.Dynamic;


namespace ZayniFramework.Common.Test
{
    /// <summary>前置攔截器
    /// </summary>
    public class BeforeInterceptor : BeforeMethodInterceptor
    {
        /// <summary>執行前置攔截的動作
        /// </summary>
        /// <param name="aspectParameters"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public override object[] AspectExecute(dynamic aspectParameters = null, params object[] parameters)
        {
            _ = DynamicHelper.HasProperty(aspectParameters, "Name") ? aspectParameters.Name : "";
            var target = parameters.FirstOrDefault() + "";
            target += " Before ";
            return [target];
        }
    }
}
