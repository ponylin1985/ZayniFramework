﻿namespace ZayniFramework.Common.Test
{
    /// <summary>測試用的 Controller 控制器
    /// </summary>
    public class HomeController
    {
        /// <summary>測試方法 1
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        [BeforeInterceptor()]
        [AfterInterceptor()]
        public string GetSomething(string str) => str + " ABCD";

        /// <summary>測試方法 2
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        [BeforeInterceptor()]
        public string GetSomething2(string str) => str + " ABCD";

        /// <summary>測試方法 3
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        [AfterInterceptor()]
        public string GetSomething3(string str) => str + " ABCD";

        /// <summary>測試方法
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public string GetSomethingClear(string str) => str + " ABCD";

        /// <summary>測試方法
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        [BeforeInterceptor(Order = "1")]
        [BeforeInterceptor(Order = "2")]
        [BeforeFilter(Order = "3")]
        public string GetString(string str) => str + " ABCD";

        /// <summary>測試方法
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        [BeforeInterceptor(Order = "1")]
        [BeforeInterceptor(Order = "2")]
        [BeforeFilter(Order = "0")]
        public string GetString2(string str) => str + " ABCD";
    }
}
