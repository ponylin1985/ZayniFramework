﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZayniFramework.Common.Dynamic;


namespace ZayniFramework.Common.Test
{
    /// <summary>
    /// </summary>
    [TestClass()]
    [TestCategory("Common.Test")]
    public class AopTester
    {
        /// <summary>
        /// </summary>
        [TestMethod()]
        [Description("ZayniFramework 框架AOP攔截器測試 - Before 與 After 都要攔截得情況")]
        public void AopAspectInvokeTest()
        {
            var controller = new HomeController();
            var obj = controller.AspectInvoke("GetSomething", methodParameters: "Testing");

            Assert.IsNotNull(obj);
            Assert.AreEqual("Testing Before  ABCD After ", obj);

            var result = AspectInvoker.Invoke("GetSomething", controller, methodParameters: "Testing");
            Assert.IsNotNull(result.ReturnValue);
            Assert.AreEqual("Testing Before  ABCD After ", result.ReturnValue);
        }

        /// <summary>
        /// </summary>
        [TestMethod()]
        [Description("ZayniFramework 框架AOP攔截器測試 - 只有攔截 Before")]
        public void AopAspectInvokeTest2()
        {
            var controller = new HomeController();
            dynamic interceptorParameters = DynamicHelper.CreateDynamicObject();
            interceptorParameters.Name = "XXXTTT";
            var obj = controller.AspectInvoke("GetSomething2", methodParameters: "Testing");

            Assert.IsNotNull(obj);
            Assert.AreEqual("Testing Before  ABCD", obj);

            InterceptResult result = AspectInvoker.Invoke("GetSomething2", controller, interceptorParameters: interceptorParameters, methodParameters: "Testing");
            Assert.IsNotNull(result.ReturnValue);
            Assert.AreEqual("Testing Before  ABCD", result.ReturnValue);
        }

        /// <summary>
        /// </summary>
        [TestMethod()]
        [Description("ZayniFramework 框架AOP攔截器測試 - 只有攔截 After")]
        public void AopAspectInvokeTest3()
        {
            var controller = new HomeController();
            dynamic interceptorParameters = DynamicHelper.CreateDynamicObject();
            interceptorParameters.Name = "XXXTTT";

            var obj = controller.AspectInvoke("GetSomething3", "Testing");

            Assert.IsNotNull(obj);
            Assert.AreEqual("Testing ABCD After ", obj);

            InterceptResult result = AspectInvoker.Invoke("GetSomething3", controller, interceptorParameters: interceptorParameters, methodParameters: "Testing");
            Assert.IsNotNull(result.ReturnValue);
            Assert.AreEqual("Testing ABCD After ", result.ReturnValue);
        }

        /// <summary>
        /// </summary>
        [TestMethod()]
        [Description("ZayniFramework 框架AOP攔截器測試 - 都沒有攔截")]
        public void AopAspectInvokeTest4()
        {
            var controller = new HomeController();
            var obj = controller.AspectInvoke("GetSomethingClear", methodParameters: "Testing");

            Assert.IsNotNull(obj);
            Assert.AreEqual("Testing ABCD", obj);

            var result = AspectInvoker.Invoke("GetSomethingClear", controller, methodParameters: "Testing");
            Assert.IsNotNull(result.ReturnValue);
            Assert.AreEqual("Testing ABCD", result.ReturnValue);
        }

        /// <summary>
        /// </summary>
        [TestMethod()]
        [Description("ZayniFramework 框架AOP攔截器測試 - 多個前置攔截器")]
        public void AopAspectInvokeTest5()
        {
            var controller = new HomeController();
            var obj = controller.AspectInvoke("GetString", methodParameters: "Testing");

            Assert.IsNotNull(obj);
            Assert.AreEqual("Testing Before  Before  B_Filter  ABCD", obj);

            var result = AspectInvoker.Invoke("GetString", controller, methodParameters: "Testing");
            Assert.IsNotNull(result.ReturnValue);
            Assert.AreEqual("Testing Before  Before  B_Filter  ABCD", result.ReturnValue);
        }

        /// <summary>
        /// </summary>
        [TestMethod()]
        [Description("ZayniFramework 框架AOP攔截器測試 - 多個前置攔截器，改變攔截器執行順序的測試")]
        public void AopAspectInvokeTest6()
        {
            var controller = new HomeController();
            var obj = controller.AspectInvoke("GetString2", methodParameters: "Testing");

            Assert.IsNotNull(obj);
            Assert.AreEqual("Testing B_Filter  Before  Before  ABCD", obj);

            var result = AspectInvoker.Invoke("GetString2", controller, methodParameters: "Testing");
            Assert.IsNotNull(result.ReturnValue);
            Assert.AreEqual("Testing B_Filter  Before  Before  ABCD", result.ReturnValue);
        }
    }
}
