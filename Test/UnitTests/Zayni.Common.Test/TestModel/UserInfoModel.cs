﻿namespace ZayniFramework.Common.Test
{
    /// <summary>測試用資料模型
    /// </summary>
    public class UserInfoModel
    {
        /// <summary>使用者代碼
        /// </summary>
        public string UserId { get; set; }

        /// <summary>年紀
        /// </summary>
        public int Age { get; set; }

        /// <summary>描述
        /// </summary>
        /// <value></value>
        public string Description { get; set; }
    }
}
