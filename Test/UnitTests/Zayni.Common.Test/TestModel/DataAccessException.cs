﻿using System;


namespace ZayniFramework.Common.Test.TestModel
{
    /// <summary>測試用的例外異常
    /// </summary>
    public class DataAccessException : Exception
    {
    }
}
