﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using ZayniFramework.Common;
using ZayniFramework.Common.Test.TestModel;


namespace ZayniFramework.Common.Test
{
    /// <summary>分頁 PagingMaker 的測試類別
    /// </summary>
    [TestClass()]
    [TestCategory("Common.Test")]
    public class PagingMakerTester
    {
        #region Private Methods

        /// <summary>準備測試假資料
        /// </summary>
        /// <returns></returns>
        private static IEnumerable<object> PrepareData()
        {
            _ = new List<object>();

            for (var i = 0; i < 500; i++)
            {
                yield return new UserModel()
                {
                    Age = i,
                    Name = "aaa",
                    Sex = "bbb"
                };
            }
        }

        #endregion Private Methods

        #region Test Methods

        /// <summary>分頁處理測試
        /// </summary>
        [TestMethod()]
        [TestCategory("Common.PagingMaker Test - 分頁處理測試")]
        public void PagingMaker_Paging_Test()
        {
            var data = PrepareData().ToList();
            var p = PagingMaker.Paging(data, 1, 20).ToList();
            Assert.IsTrue(0 != p.Count);
            Assert.IsTrue(20 == p.Count);

            p = PagingMaker.Paging(data, 2, 20).ToList();
            Assert.IsTrue(0 != p.Count);
            Assert.IsTrue(20 == p.Count);
        }

        /// <summary>分頁處理測試
        /// </summary>
        [TestMethod()]
        [TestCategory("Common.PagingMaker Test - 分頁處理測試")]
        public void PagingMaker_Paging_Test2()
        {
            var data = PrepareData();
            var p = PagingMaker.Paging(data, 1, 5);
            Assert.IsTrue(0 != p.Count());
            Assert.IsTrue(5 == p.Count());

            p = PagingMaker.Paging(data, 2, 5);
            Assert.IsTrue(0 != p.Count());
            Assert.IsTrue(5 == p.Count());
        }

        #endregion Test Methods
    }
}
