﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;


namespace ZayniFramework.Common.Test
{
    /// <summary>PathStringMaker 的測試類別
    /// </summary>
    [TestClass()]
    [TestCategory("Common.Test")]
    public class FileHelperTester
    {
        /// <summary>PathStringMaker.TrimLastSlash 的測試
        /// </summary>
        [TestMethod()]
        [Description("PathStringMaker.TrimLastSlash 的測試")]
        public void PathStringMaker_TrimLastSlash_Test()
        {
            var separator = Path.DirectorySeparatorChar;

            var path = $@"{separator}ABC{separator}DEF{separator}";
            var except = $@"{separator}ABC{separator}DEF";

            var p = FileHelper.TrimLastSlash(path);
            Assert.AreEqual(except, p);

            path = $@"{separator}ABC{separator}DEF";
            p = FileHelper.TrimLastSlash(path);
            Assert.AreEqual(except, p);
        }
    }
}
