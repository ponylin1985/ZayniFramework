﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;


namespace ZayniFramework.Common.Test
{
    /// <summary>檔案搜尋器 FilePathFinder 的測試類別
    /// </summary>
    [TestClass()]
    public class FilePathFinderTester
    {
        /// <summary>FilePathFinder 檔案搜尋的測試
        /// </summary>
        [TestMethod()]
        [Description("FilePathFinder 檔案搜尋的測試")]
        public void SearchFile_Test()
        {
            //var root = new DirectoryInfo( Path.Combine( AppDomain.CurrentDomain.BaseDirectory, "FileIOTest" ) );
            //FileInfo[] files = FileHelper.SearchFile( "Something.txt", root );
            //Assert.IsTrue( files.IsNotNullAndEmptyArray() );
            //Assert.IsNotNull( files.FirstOrDefault() );

            //Result<FileInfo[]> r = FileHelper.SearchFile( "Something.txt", Path.Combine( AppDomain.CurrentDomain.BaseDirectory, "FileIOTest" ) );
            //Assert.IsTrue( r.Success );
            //Assert.IsNotNull( r.Data.FirstOrDefault() );
        }

        /// <summary>FilePathFinder 檔案搜尋的反向測試
        /// </summary>
        [TestMethod()]
        [Description("FilePathFinder 檔案搜尋的反向測試 - 不存在的檔案")]
        public void SeacherFileResultTest_NegativeTest_NotExistFile()
        {
            // 故意搜尋一個錯誤根本不存在的檔案!
            var r = FileHelper.SearchFile("ggg.txt", Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FileIOTest"));
            Assert.IsFalse(r.Success);
            Assert.IsNull(r.Data);
        }

        /// <summary>FilePathFinder 檔案搜尋的反向測試 - 從不存的根目錄搜尋
        /// </summary>
        [TestMethod()]
        [Description("FilePathFinder 檔案搜尋的反向測試 - 從不存的根目錄搜尋")]
        public void SeacherFileResultTest_NegativeTest_NotExistRootDir()
        {
            // 故意從不存的根目錄搜尋!
            var r = FileHelper.SearchFile("asdf.dll", @"C:\Program Files GGUU JIJ66\");
            Assert.IsFalse(r.Success);
            Assert.IsNull(r.Data);
        }

        /// <summary>FilePathFinder 檔案搜尋的反向測試 - 故意傳入不合法的參數
        /// </summary>
        [TestMethod()]
        [Description("FilePathFinder 檔案搜尋的反向測試 - 故意傳入不合法的參數")]
        public void SeacherFileResultTest_NegativeTest_WrongParameters()
        {
            // 目標檔案名稱傳入空字串
            var r = FileHelper.SearchFile("", @"C:\Program Files GGUU JIJ66\");
            Assert.IsFalse(r.Success);
            Assert.IsNull(r.Data);

            // 目標根目錄路徑傳入空字串
            r = FileHelper.SearchFile("Something.txt", "");
            Assert.IsFalse(r.Success);
            Assert.IsNull(r.Data);
        }
    }
}
