﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;

namespace ZayniFramework.Common.Test
{
    /// <summary>迭代器物件的測試類別
    /// </summary>
    [TestClass()]
    [TestCategory("Common.Test")]
    public class IteratorTester
    {
        /// <summary>For 迭代器的測試
        /// </summary>
        [TestMethod()]
        public void ForTest()
        {
            For.Reset();
            For.Do(5, () => Debug.Print(For.Count + ""));
            For.Reset();
            Console.WriteLine();
            For.Do(10, () => Debug.Print((For.Count + 1) + ""));
        }

        /// <summary>ForEach 迭代器的測試
        /// </summary>
        [TestMethod()]
        public void ForEachTest()
        {
            var a = new string[] { "a", "b", "c", "d", "e" };
            var f = new ForEach(a);
            f.Do(() => Console.WriteLine("AAA"));
        }
    }
}
