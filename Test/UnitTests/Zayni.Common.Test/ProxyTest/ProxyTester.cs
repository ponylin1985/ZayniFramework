using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using ZayniFramework.Logging;


namespace ZayniFramework.Common.Test
{
    /// <summary>代理人呼叫測試類別
    /// </summary>
    [TestClass()]
    [TestCategory("Common.Test")]
    public class ProxyTester
    {
        /// <summary>代理人呼叫測試<para/>
        /// * 有 before 和 after handler 的測試。<para/>
        /// * 有 retry 的測試。<para/>
        /// * non-async method 的 invoke 測試。<para/>
        /// * Targat Method 沒有回傳值的測試。
        /// </summary>
        [TestMethod()]
        [Description("代理人呼叫測試")]
        public void GivenTargetMethodWithNoReturnValue_WhenProxyInvoke_ThenShouldExecuteSuccessfully()
        {
            var service = new UserService();
            var before = default(IResult<UserInfoModel>);

            Proxy.Invoke(
                action: () => service.Reset(),
                beforeAction: () => before = UserService.GetUser("HappyBear1"),
                afterAction: () => service.Users.Insert(new UserInfoModel { UserId = "HappyBear100", Age = 15, Description = "Nice" }),
                retryOption: new RetryOption { EnableRetry = true, RetryFrequencies = [TimeSpan.FromMilliseconds(20), TimeSpan.FromMilliseconds(20),] }
            );

            Assert.AreEqual("HappyBear1", before.Data.UserId);
            Assert.AreEqual(18, before.Data.Age);
            Assert.AreEqual("AAA", before.Data.Description);

            Assert.IsTrue(service.Users.IsNotNullOrEmpty());
            Assert.AreEqual(2, service.Users.Count());

            var m = service.Users.Where(q => q.UserId == "HappyBear1")?.FirstOrDefault();
            Assert.IsNotNull(m);
            Assert.AreEqual("HappyBear1", m.UserId);
            Assert.AreEqual(18, m.Age);
            Assert.AreEqual("AAA", m.Description);

            var n = service.Users.Where(q => q.UserId == "HappyBear100")?.FirstOrDefault();
            Assert.IsNotNull(n);
            Assert.AreEqual("HappyBear100", n.UserId);
            Assert.AreEqual(15, n.Age);
            Assert.AreEqual("Nice", n.Description);
        }

        /// <summary>代理人呼叫測試<para/>
        /// * 有 before 和 after handler 的測試。<para/>
        /// * 有 retry 的測試。<para/>
        /// * async method 的 invoke 測試。<para/>
        /// * Targat Method 沒有回傳值的測試。
        /// </summary>
        [TestMethod()]
        [Description("代理人呼叫測試")]
        public async Task GivenAsyncTargetMethodWithNoReturnValue_WhenProxyInvokeAsync_ThenShouldExecuteSuccessfully()
        {
            var service = new UserService();
            var before = default(IResult<UserInfoModel>);

            await Proxy.InvokeAsync(
                asyncFunc: async () => await service.ResetAsync(),
                beforeFunc: async () => { await Task.Yield(); before = UserService.GetUser("HappyBear1"); },
                afterFunc: async () => { await Task.Yield(); service.Users.Insert(new UserInfoModel { UserId = "HappyBear100", Age = 15, Description = "Nice" }); },
                retryOption: new RetryOption { EnableRetry = true, RetryFrequencies = [TimeSpan.FromMilliseconds(20), TimeSpan.FromMilliseconds(20),] }
            );


            Assert.AreEqual("HappyBear1", before.Data.UserId);
            Assert.AreEqual(18, before.Data.Age);
            Assert.AreEqual("AAA", before.Data.Description);

            Assert.IsTrue(service.Users.IsNotNullOrEmpty());
            Assert.AreEqual(2, service.Users.Count());

            var m = service.Users.Where(q => q.UserId == "HappyBear1")?.FirstOrDefault();
            Assert.IsNotNull(m);
            Assert.AreEqual("HappyBear1", m.UserId);
            Assert.AreEqual(18, m.Age);
            Assert.AreEqual("AAA", m.Description);

            var n = service.Users.Where(q => q.UserId == "HappyBear100")?.FirstOrDefault();
            Assert.IsNotNull(n);
            Assert.AreEqual("HappyBear100", n.UserId);
            Assert.AreEqual(15, n.Age);
            Assert.AreEqual("Nice", n.Description);
        }

        /// <summary>代理人呼叫測試<para/>
        /// * 有 before 和 after handler 的測試。<para/>
        /// * 有 retry 的測試。<para/>
        /// * non-async method 的 invoke 測試。<para/>
        /// * Targat Method 有回傳值的測試。
        /// </summary>
        [TestMethod()]
        [Description("代理人呼叫測試")]
        public void GivenTargetMethodWithReturnValue_WhenProxyInvoke_ThenShouldExecuteSuccessfully()
        {
            var service = new UserService();
            var userId = "HappyBear";

            var r = Proxy.Invoke(
                func: () => UserService.GetUser(userId),
                beforeAction: () => userId = $"{userId}1",
                afterFunc: r => r.When(g => g.Data.IsNotNull(), u => { u.Data.Description = $"{u.Data.Description}-Good"; return u; }),
                retryOption: new RetryOption { EnableRetry = true, RetryFrequencies = [TimeSpan.FromMilliseconds(20), TimeSpan.FromMilliseconds(20),] }
            );

            Assert.IsTrue(r.Success);
            Assert.AreEqual("HappyBear1", r.Data.UserId);
            Assert.AreEqual(18, r.Data.Age);
            Assert.AreEqual("AAA-Good", r.Data.Description);
        }

        /// <summary>代理人呼叫測試<para/>
        /// * 有 before 和 after handler 的測試。<para/>
        /// * 有 retry 的測試。<para/>
        /// * non-async method 的 invoke 測試。<para/>
        /// * Targat Method 有回傳值的測試。
        /// </summary>
        [TestMethod()]
        [Description("代理人呼叫測試")]
        public void GivenExceptionOccured_WhenProxyInvoke_ThenShouldRetryAndFailover()
        {
            var service = new UserService();
            var userId = "NotExistedUserId";

            var r = Proxy.Invoke(
                func: () => UserService.GetUser(userId),
                beforeAction: () => userId = $"{userId}1",
                afterFunc: r => r.When(g => g.Data.IsNotNull(), u => { u.Data.Description = $"{u.Data.Description}-Good"; return u; }),
                retryOption: new RetryOption { EnableRetry = true, RetryFrequencies = [TimeSpan.FromMilliseconds(20), TimeSpan.FromMilliseconds(20),] },
                returnWhenRetryFail: f => Result.Create(true, new UserInfoModel
                {
                    UserId = userId,
                    Age = 25,
                    Description = "This is failover."
                })
            );

            Assert.IsTrue(r.Success);
            Assert.AreEqual(userId, r.Data.UserId);
            Assert.AreEqual(25, r.Data.Age);
            Assert.AreEqual("This is failover.", r.Data.Description);
        }

        /// <summary>代理人呼叫測試<para/>
        /// * 有 before 和 after handler 的測試。<para/>
        /// * 有 retry 的測試。<para/>
        /// * async method 的 invoke 測試。<para/>
        /// * Targat Method 有回傳值的測試。
        /// </summary>
        [TestMethod()]
        [Description("代理人呼叫測試")]
        public async Task GivenAsyncTargetMethodWithReturnValue_WhenProxyInvokeAsync_ThenShouldExecuteSuccessfully()
        {
            var service = new UserService();
            var userId = "HappyBear";

            var r = await Proxy.InvokeAsync(
                asyncFunc: async () => await UserService.GetUserAsync(userId),
                beforeFunc: async () => { userId = $"{userId}1"; await Task.FromResult(0); },
                afterFunc: async r => await Task.FromResult(r.When(g => g.Data.IsNotNull(), u => { u.Data.Description = $"Good"; return u; })),
                retryOption: new RetryOption { EnableRetry = true, RetryFrequencies = [TimeSpan.FromMilliseconds(20), TimeSpan.FromMilliseconds(20),] }
            );

            Assert.IsTrue(r.Success);
            Assert.AreEqual("HappyBear1", r.Data.UserId);
            Assert.AreEqual(18, r.Data.Age);
            Assert.AreEqual("Good", r.Data.Description);
        }

        /// <summary>代理人呼叫測試<para/>
        /// * 有 before 和 after handler 的測試。<para/>
        /// * 有 retry 的測試。<para/>
        /// * async method 的 invoke 測試。<para/>
        /// * Targat Method 有回傳值的測試。
        /// </summary>
        [TestMethod()]
        [Description("代理人呼叫測試")]
        public async Task GivenExceptionOccured_WhenProxyInvokeAsync_ThenShouldRetryAndFailover()
        {
            var service = new UserService();
            var userId = "NotExistedUserId";

            var r = await Proxy.InvokeAsync(
                asyncFunc: async () => await UserService.GetUserAsync(userId),
                beforeFunc: async () => { userId = $"{userId}1"; await Task.FromResult(0); },
                afterFunc: async r => await Task.FromResult(r.When(g => g.Data.IsNotNull(), u => { u.Data.Description = $"Good"; return u; })),
                retryOption: new RetryOption { EnableRetry = true, RetryFrequencies = [TimeSpan.FromMilliseconds(20), TimeSpan.FromMilliseconds(20),] },
                returnWhenRetryFail: f => Result.Create(true, new UserInfoModel
                {
                    UserId = userId,
                    Age = 25,
                    Description = "This is failover."
                })
            );

            r.Success.Should().BeTrue();
            r.Data.UserId.Should().Be(userId);
            r.Data.Age.Should().Be(25);
            r.Data.Description.Should().Be("This is failover.");
        }

        /// <summary>代理人呼叫測試
        /// </summary>
        [TestMethod()]
        [Description("代理人呼叫測試")]
        public void ProxyInvoke_Test()
        {
            var service = new SomeService();

            Proxy.Invoke(
                service.DoSomething,
                beforeAction: () => Debug.Print($"This is before action"),
                afterAction: () => Debug.Print($"This is after action"));

            // ==============

            var model = new UserInfoModel
            {
                UserId = "Judy",
                Age = 19
            };

            Proxy.Invoke(
                () => service.SetMyFavoriteUser(model),
                beforeAction: () => model.When(m => "Judy" == m.UserId, u => u.Description = "My favorite slut!"),
                afterAction: () => Debug.Print($"This is after action")
            );

            Assert.AreEqual("My favorite slut!", model.Description);

            // ==============

            var userId = "";

            var g = Proxy.Invoke(
                () => SomeService.GetUser(userId),
                beforeAction: () => userId.When(s => s.IsNullOrEmpty(), z => userId = "Judy"),
                afterFunc: r => r.When(f => f.Success && "Judy" == f.Data.UserId, q => { q.Data.Description = "What a slut!"; return q; })
            );

            Assert.IsTrue(g.Success);
            Assert.IsNotNull(g.Data);
            Assert.AreEqual("Judy", g.Data.UserId);
            Assert.AreEqual(19, g.Data.Age);
            Assert.AreEqual("What a slut!", g.Data.Description);
        }

        /// <summary>代理人呼叫測試，沒有參數，沒有回傳值的代理呼叫測試
        /// </summary>
        [TestMethod()]
        [Description("代理人呼叫測試")]
        public void ProxyInvoke_NoParameters_NoReturn_Test()
        {
            var service = new SomeService();

            var proxy = Proxy.Create(
                new Action(service.DoSomething),
                parameters =>
                {
                    var msg = "Judy loves me.";
                    Debug.Print(msg);
                    ConsoleLogger.WriteLine(msg);
                    return parameters;
                });

            proxy.Invoke();
        }

        /// <summary>代理人呼叫測試，有參數，沒有回傳值的代理呼叫測試
        /// </summary>
        [TestMethod()]
        [Description("代理人呼叫測試")]
        public void ProxyInvoke_Parameters_NoReturn_Test()
        {
            var service = new SomeService();

            var proxy = Proxy.Create(
                new Action<UserInfoModel>(model => service.SetMyFavoriteUser(model)),
                parameters =>
                {
                    var model = (UserInfoModel)parameters[0];

                    if (model.UserId == "Judy")
                    {
                        model.UserId = "Judy Bitch";
                    }

                    return parameters;
                });

            proxy.Invoke(new UserInfoModel() { UserId = "Judy", Age = 19 });

            var r = service.GetMyFavoriteUser();
            Assert.IsTrue(r.Success);
            Assert.AreEqual(r.Data.UserId, "Judy Bitch");
            Assert.AreEqual(r.Data.Age, 19);
        }

        /// <summary>代理人呼叫測試，沒有參數，有回傳值的代理呼叫測試
        /// </summary>
        [TestMethod()]
        [Description("代理人呼叫測試")]
        public void ProxyInvoke_NoParameters_Return_Test()
        {
            var service = new SomeService();
            service.SetMyFavoriteUser(new UserInfoModel() { UserId = "Kate", Age = 18 });

            var proxy = Proxy.Create<IResult<UserInfoModel>>(
                new Func<IResult<UserInfoModel>>(service.GetMyFavoriteUser),
                afterInvoke: (parameters, result) =>
                {
                    result.Message = "I stil l love fuck Judy's ass and throat. Fuck Yeah~~~";
                    return result;
                });

            var r = proxy.Invoke();
            Assert.IsTrue(r.Success);
            Assert.AreEqual("Kate", r.Data.UserId);
            Assert.AreEqual(18, r.Data.Age);
            Assert.AreEqual("I stil l love fuck Judy's ass and throat. Fuck Yeah~~~", r.Message);
        }

        /// <summary>代理人呼叫測試，有參數，有回傳值的代理呼叫測試
        /// </summary>
        [TestMethod()]
        [Description("代理人呼叫測試")]
        public void ProxyInvoke_Parameters_ReturnValue_Test()
        {
            var service = new SomeService();

            var proxy = Proxy.Create<IResult<UserInfoModel>>(
                new Func<string, IResult<UserInfoModel>>(userId => SomeService.GetUser(userId)),
                parameters =>
                {
                    var userId = parameters[0] + "";
                    parameters[0] = userId != "Judy" ? "Sylvia" : userId;
                    return parameters;
                },
                (parameters, result) =>
                {
                    if (!result.Success)
                    {
                        result.Message = "I love fuck Sylvia. Oh yeah!!!";
                        return result;
                    }

                    result.Message = "I love fuck Judy. Oh yeah!!!";
                    return result;
                });

            var r = proxy.Invoke("Judy");
            Assert.IsTrue(r.Success);
            Assert.AreEqual("I love fuck Judy. Oh yeah!!!", r.Message);

            var z = proxy.Invoke("Kelly");
            Assert.IsFalse(z.Success);
            Assert.AreEqual("I love fuck Sylvia. Oh yeah!!!", z.Message);
        }
    }

    /// <summary>某個服務類別，單元測試使用
    /// </summary>
    internal class SomeService
    {
        /// <summary>測試訊息
        /// </summary>
        /// <value></value>
        public string SomeMessage { get; set; }

        /// <summary>私有測試欄位
        /// </summary>
        private UserInfoModel _myFavoriteUser;

        /// <summary>某個測試方法，沒有參數，沒有回傳值
        /// </summary>
        public void DoSomething()
        {
            SomeMessage = "I love fuck Jody's ass hole.";
        }

        /// <summary>某個測試方法，沒有參數，但有回傳值
        /// </summary>
        /// <returns></returns>
        public IResult<UserInfoModel> GetMyFavoriteUser()
        {
            return Result.Create<UserInfoModel>(true, _myFavoriteUser);
        }

        /// <summary>某個測試方法，沒有參數，但有回傳值
        /// </summary>
        /// <returns></returns>
        public void SetMyFavoriteUser(UserInfoModel model)
        {
            _myFavoriteUser = model;
        }

        /// <summary>某個查詢之類的測試方法，有需要參數，有回傳值
        /// </summary>
        /// <param name="userId">使用者帳號</param>
        /// <returns>查詢結果</returns>
        public static IResult<UserInfoModel> GetUser(string userId)
        {
            if (userId != "Judy")
            {
                return Result.Create<UserInfoModel>();
            }

            var model = new UserInfoModel()
            {
                UserId = "Judy",
                Age = 19
            };

            return Result.Create<UserInfoModel>(true, model);
        }
    }

    /// <summary>User service for unit test.
    /// </summary>
    internal class UserService
    {
        /// <summary>Fake user profiles for tests.
        /// </summary>
        private static IEnumerable<UserInfoModel> _fakeModels;

        /// <summary>Some property for tests.
        /// </summary>
        /// <value></value>
        public IEnumerable<UserInfoModel> Users { get; set; }

        /// <summary>Static Constructor
        /// </summary>
        static UserService() => MakeFakeData();

        /// <summary>Some reset function.
        /// </summary>
        /// <returns></returns>
        public void Reset() => Users = _fakeModels.Where(m => m.Age <= 18).ToList();

        /// <summary>Some reset function. Fake async function.
        /// </summary>
        /// <returns></returns>
        public async Task ResetAsync()
        {
            await Task.Yield();
            Users = _fakeModels.Where(m => m.Age <= 18).ToList();
        }

        /// <summary>Enquiry for user profile.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static IResult<UserInfoModel> GetUser(string userId)
        {
            var model = _fakeModels.Where(m => m.UserId == userId)?.FirstOrDefault();
            return Result.Create(true, model);
        }

        /// <summary>Some broken function for test.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static IResult<UserInfoModel> GetUserWithError(string userId)
        {
            var model = _fakeModels.Where(m => m.UserId == userId)?.FirstOrDefault();
            return Result.Create(true, model);
        }

        /// <summary>Enquiry for user profiles.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static async Task<IResult<UserInfoModel>> GetUserAsync(string userId)
        {
            await Task.Yield();
            var model = _fakeModels.Where(m => m.UserId == userId)?.FirstOrDefault();
            return await Task.FromResult(Result.Create(true, model));
        }

        /// <summary>Make some fake data for tests.
        /// </summary>
        private static void MakeFakeData()
        {
            _fakeModels = new List<UserInfoModel>
            {
                new() {
                    UserId      = "HappyBear1",
                    Age         = 18,
                    Description = "AAA"
                },
                new() {
                    UserId      = "HappyBear2",
                    Age         = 20,
                    Description = "BBB"
                },
                new() {
                    UserId      = "HappyBear3",
                    Age         = 21,
                    Description = "CCC"
                },
            };
        }
    }
}
