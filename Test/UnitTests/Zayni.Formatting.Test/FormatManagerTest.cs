﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using ZayniFramework.Formatting;


namespace Formatting.Test
{
    // dotnet test ./Test/UnitTests/Zayni.Formatting.Test/Zayni.Formatting.Test.csproj --no-build --filter ClassName=Formatting.Test.FormatManagerTest
    /// <summary>
    ///這是 FormatManagerTest 的測試類別，應該包含
    ///所有 FormatManagerTest 單元測試
    ///</summary>
    [TestClass()]
    public class FormatManagerTest
    {
        #region ToCurrencyString( decimal, int) 單元測試

        [Description("對ToCurrencyString的正常測試，小數位數一位")]
        [TestMethod()]
        public void ToCurrencyStringTest()
        {
            var target = 255574448.84233M;
            var decLength = 1;

            var r = FormatManager.ToCurrencyString(target, decLength);
            Assert.IsTrue(r.Success);

            var expected = "255,574,448.8";
            Assert.AreEqual(expected, r.Result, "格式化結果錯誤");
        }

        [Description("對ToCurrencyString的正常測試，小數位數三位")]
        [TestMethod()]
        public void ToCurrencyStringTest2()
        {
            var target = 255574448.84233M;
            var decLength = 3;

            var r = FormatManager.ToCurrencyString(target, decLength);
            Assert.IsTrue(r.Success);

            var expected = "255,574,448.842";
            Assert.AreEqual(expected, r.Result, "格式化結果錯誤");
        }

        [Description("對ToCurrencyString的反向測試，輸入錯誤小數位數長度")]
        [TestMethod()]
        public void ToCurrencyStringOppositeTest()
        {
            var target = 255574448.84233M;
            var decLength = -3;

            var r = FormatManager.ToCurrencyString(target, decLength);
            Assert.IsTrue(r.Success);

            var expected = "255,574,448.842";
            Assert.AreNotEqual(expected, r.Result, "格式化結果錯誤");
        }

        #endregion ToCurrencyString( decimal, int) 單元測試

        #region ToCurrencyString(decimal, decimal=0M) 單元測試

        [Description("對ToCurrencyString的正常測試，無輸入小數點位數長度")]
        [TestMethod()]
        public void ToCurrencyString_DecimalDotLengthTest()
        {
            var target = 255574448.84233M;

            var r = FormatManager.ToCurrencyString(target);
            Assert.IsTrue(r.Success);

            var expected = "255,574,449";
            Assert.AreEqual(expected, r.Result, "格式化結果錯誤");
        }

        [Description("對ToCurrencyString的正常測試，小數位數三位")]
        [TestMethod()]
        public void ToCurrencyString_DecimalDotLengthTest2()
        {
            var target = 255574448.84233M;
            var decLength = 3M;

            var r = FormatManager.ToCurrencyString(target, decLength);
            Assert.IsTrue(r.Success);

            var expected = "255,574,448.842";
            Assert.AreEqual(expected, r.Result, "格式化結果錯誤");
        }

        [Description("對ToCurrencyString的反向測試，輸入錯誤小數位數長度")]
        [TestMethod()]
        public void ToCurrencyString_DecimalDotLengthOppositeTest()
        {
            var target = 255574448.84233M;
            var decLength = 0.3M;   // 錯誤的小數位數長度

            var r = FormatManager.ToCurrencyString(target, decLength);
            Assert.IsTrue(r.Success);

            var expected = "255,574,448.842";
            Assert.AreNotEqual(expected, r.Result, "格式化結果錯誤");
        }

        #endregion ToCurrencyString(decimal, decimal=0M) 單元測試

        #region ToNumber( decimal,int=0 ) 單元測試

        [Description("對ToNumber的測試，無小數")]
        [TestMethod()]
        public void ToNumber_DecimalTargetTest()
        {
            var target = 255574448.84233M;
            var decLength = 0;

            var r = FormatManager.ToNumber(target, decLength);
            Assert.IsTrue(r.Success);

            var expected = "255,574,449";
            Assert.AreEqual(expected, r.Result, "格式化結果錯誤");
        }

        [Description("對ToNumber的測試，小數位數二位")]
        [TestMethod()]
        public void ToNumber_DecimalTargetTest2()
        {
            var target = 255574448.84233M;
            var decLength = 2;

            var r = FormatManager.ToNumber(target, decLength);
            Assert.IsTrue(r.Success);

            var expected = "255,574,448.84";
            Assert.AreEqual(expected, r.Result, "格式化結果錯誤");
        }

        [Description("對ToNumber的反向測試，輸入錯誤小數位數長度")]
        [TestMethod()]
        public void ToNumber_DecimalTargetOppositeTest()
        {
            var target = 255574448.84233M;
            var decLength = -2; // 小數位數資料為負Int值.

            var r = FormatManager.ToNumber(target, decLength);
            Assert.IsTrue(r.Success);

            var expected = "255,574,448.84";
            Assert.AreNotEqual(expected, r.Result, "格式化結果錯誤");
        }

        #endregion ToNumber( decimal,int=0 )  單元測試

        #region ToNumber( string, int=0 ) 單元測試
        [Description("對ToNumber的測試，無小數")]
        [TestMethod()]
        public void ToNumber_StringTargetTest()
        {
            var target = "255574448.84233";
            var decLength = 0;

            var r = FormatManager.ToNumber(target, decLength);
            Assert.IsTrue(r.Success);

            var expected = "255,574,449";
            Assert.AreEqual(expected, r.Result, "格式化結果錯誤");
        }

        [Description("對ToNumber的測試，小數位數二位")]
        [TestMethod()]
        public void ToNumber_StringTargetTest2()
        {
            var target = "255574448.84233";
            var decLength = 2;

            var r = FormatManager.ToNumber(target, decLength);
            Assert.IsTrue(r.Success);

            var expected = "255,574,448.84";
            Assert.AreEqual(expected, r.Result, "格式化結果錯誤");
        }

        [Description("對ToNumber的反向測試，輸入錯誤小數位數長度")]
        [TestMethod()]
        public void ToNumber_StringTargetOppositeTest()
        {
            var target = "255574448.84233";
            var decLength = -2; // 小數位數資料為負Int值.

            var r = FormatManager.ToNumber(target, decLength);
            Assert.IsTrue(r.Success);

            var expected = "255,574,448.84";
            Assert.AreNotEqual(expected, r.Result, "格式化結果錯誤");
        }

        [Description("對ToNumber的例外測試，格式化目標為Null")]
        [TestMethod()]
        public void ToNumber_StringTargetExceptionTest()
        {
            string target = null;   // 格式化目標為Null
            var decLength = 2;

            var r = FormatManager.ToNumber(target, decLength);
            Assert.IsFalse(r.Success);
        }

        [Description("對ToNumber的例外測試，格式化目標為空字串")]
        [TestMethod()]
        public void ToNumber_StringTargetExceptionTest2()
        {
            var target = "";   // 格式化目標為空字串
            var decLength = 2;

            var r = FormatManager.ToNumber(target, decLength);
            Assert.IsFalse(r.Success);
        }

        #endregion ToNumber( string, int=0 )  單元測試

        #region ToMoney( Decimal, int=0 ) 單元測試

        [Description("對ToMoney的測試，無小數")]
        [TestMethod()]
        public void ToMoney_DecimalTargetTest()
        {
            var target = 255574448.84233M;
            var decLength = 0;

            var r = FormatManager.ToMoney(target, decLength);
            Assert.IsTrue(r.Success);

            var expected = "$255,574,449";
            Assert.AreEqual(expected, r.Result, "格式化結果錯誤");
        }

        [Description("對ToMoney的測試，小數位數二位")]
        [TestMethod()]
        public void ToMoney_DecimalTargetTest2()
        {
            var target = 255574448.84233M;
            var decLength = 2;

            var r = FormatManager.ToMoney(target, decLength);
            Assert.IsTrue(r.Success);

            var expected = "$255,574,448.84";
            Assert.AreEqual(expected, r.Result, "格式化結果錯誤");
        }

        [Description("對ToMoney的反向測試，輸入錯誤小數位數長度")]
        [TestMethod()]
        public void ToMoney_DecimalTargetOppositeTest()
        {
            var target = 255574448.84233M;
            var decLength = -2; // 小數位數資料為負Int值.

            var r = FormatManager.ToMoney(target, decLength);
            Assert.IsTrue(r.Success);

            var expected = "$255,574,448.84";
            Assert.AreNotEqual(expected, r.Result, "格式化結果錯誤");
        }

        #endregion ToMoney( Decimal, int=0 ) 單元測試

        #region ToMoney( string, int=0 ) 單元測試
        [Description("對ToMoney的測試，無小數")]
        [TestMethod()]
        public void ToMoney_StringTargetTest()
        {
            var target = "255574448.84233";
            var decLength = 0;

            var r = FormatManager.ToMoney(target, decLength);
            Assert.IsTrue(r.Success);

            var expected = "$255,574,449";
            Assert.AreEqual(expected, r.Result, "格式化結果錯誤");
        }

        [Description("對ToMoney的測試，小數位數二位")]
        [TestMethod()]
        public void ToMoney_StringTargetTest2()
        {
            var target = "255574448.84233";
            var decLength = 2;

            var r = FormatManager.ToMoney(target, decLength);
            Assert.IsTrue(r.Success);

            var expected = "$255,574,448.84";
            Assert.AreEqual(expected, r.Result, "格式化結果錯誤");
        }

        [Description("對ToMoney的反向測試，輸入錯誤小數位數長度")]
        [TestMethod()]
        public void ToMoney_StringTargetOppositeTest()
        {
            var target = "255574448.84233";
            var decLength = -2; // 小數位數資料為負Int值.

            var r = FormatManager.ToMoney(target, decLength);
            Assert.IsTrue(r.Success);

            var expected = "$255,574,448.84";
            Assert.AreNotEqual(expected, r.Result, "格式化結果錯誤");
        }

        [Description("對ToMoney的例外測試，輸入格式化字串為Null")]
        [TestMethod()]
        public void ToMoney_StringTargetExceptionTest()
        {
            string target = null;   // 格式化目標為Null
            var decLength = 2;

            var r = FormatManager.ToMoney(target, decLength);
            Assert.IsFalse(r.Success);
        }

        [Description("對ToMoney的例外測試，輸入格式化字串為空字串")]
        [TestMethod()]
        public void ToMoney_StringTargetExceptionTest2()
        {
            var target = ""; // 格式化目標為空字串
            var decLength = 2;

            var r = FormatManager.ToMoney(target, decLength);
            Assert.IsFalse(r.Success);
        }

        #endregion ToMoney( string, int=0 ) 單元測試

        #region Format 單元測試

        [Description("對Format的正常測試，格式化目標為DateTime")]
        [TestMethod()]
        public void FormatTest()
        {
            var target = DateTime.Now;
            var format = "yyyy-MM-dd HH:mm:ss";
            var type = "DateTime";

            var r = FormatManager.Format(target, format, type);
            Assert.IsTrue(r.Success);

            var expected = target.ToString(format);
            Assert.AreEqual(expected, r.Result, "格式化結果錯誤");
        }

        [Description("對Format的正常測試，格式化目標為Decimal")]
        [TestMethod()]
        public void FormatTest2()
        {
            var target = 255574448.84233M; ;
            var format = "{0:N2}";
            var type = "Decimal";

            var r = FormatManager.Format(target, format, type);
            Assert.IsTrue(r.Success);

            var expected = "255,574,448.84";
            Assert.AreEqual(expected, r.Result, "格式化結果錯誤");
        }

        [Description("對Format的反向測試，輸入錯誤TypeName")]
        [TestMethod()]
        public void FormatOppositeTest()
        {
            var target = 255574448.84233M; ;
            var format = "{0:N2}";
            var type = "fdsfds";   // 輸入錯誤TypeName

            var r = FormatManager.Format(target, format, type);
            Assert.IsFalse(r.Success);
        }

        [Description("對Format的反向測試，輸入目標型態為double")]
        [TestMethod()]
        public void FormatOppositeTest2()
        {
            var target = 255574448.84233; // 輸入錯誤的格式化目標型態
            var format = "{0:N2}";
            var type = "Decimal";

            var r = FormatManager.Format(target, format, type);
            Assert.IsTrue(r.Success);

            var expected = "255,574,448.84";
            Assert.AreEqual(expected, r.Result, "格式化結果錯誤");
        }

        [Description("對Format的反向測試，輸入錯誤的格式化字串")]
        [TestMethod()]
        public void FormatOppositeTest3()
        {
            var target = 255574448.84233M;
            var format = "jjjhhdss"; // 輸入錯誤的格式化字串
            var type = "Decimal";

            var r = FormatManager.Format(target, format, type);
            Assert.IsTrue(r.Success);

            var expected = "255574448.84";
            Assert.AreNotEqual(expected, r.Result, "格式化結果錯誤");
        }

        [Description("對Format的列外測試，輸入格式化目標為Null")]
        [TestMethod()]
        public void FormatExceptionTest()
        {
            object target = null; ;
            var format = "{0:N2}";
            var type = "Decimal";

            var r = FormatManager.Format(target, format, type);
            Assert.IsFalse(r.Success);
        }

        [Description("對Format的列外測試，輸入的格式化字串為Null")]
        [TestMethod()]
        public void FormatExceptionTest2()
        {
            var target = 255574448.84233M;
            string format = null;
            var type = "Decimal";

            var r = FormatManager.Format(target, format, type);
            Assert.IsFalse(r.Success);
        }

        [Description("對Format的列外測試，輸入的TypeName為Null")]
        [TestMethod()]
        public void FormatExceptionTest3()
        {
            var target = 255574448.84233M;
            var format = "{0:N2}";
            string type = null;

            var r = FormatManager.Format(target, format, type);
            Assert.IsFalse(r.Success);
        }

        #endregion Format 單元測試
    }
}
