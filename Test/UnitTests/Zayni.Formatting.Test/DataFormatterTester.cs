﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using ZayniFramework.Common;
using ZayniFramework.Formatting;


namespace Formatting.Test
{
    // dotnet test ./Test/UnitTests/Zayni.Formatting.Test/Zayni.Formatting.Test.csproj --no-build --filter ClassName=Formatting.Test.DataFormatterTest
    /// <summary>DataFormatter 的測試類別
    ///</summary>
    [TestClass()]
    public class DataFormatterTest
    {
        #region 測試組件初始化

        /// <summary>初始化整個 Test project
        /// </summary>
        /// <param name="testcontext"></param>
        [AssemblyInitialize()]
        public static void Init(TestContext testcontext)
        {
            // 在 .NET Core 版本的 MSTest，一定要自行指定 .config 的完整絕對路徑到 ConfigManagement.ConfigFullPath 中，整個 ZayniFramework 舊有在 XML Config 的機制才會正常運作！
            var path = $"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}/zayni.json";
            ConfigManager.Init(path);
        }

        #endregion 測試組件初始化


        #region DoModelSimpleFormat( object model )的單元測試

        // 20150731 Edited by Pony: DateTime 日期時間支援多國語系格式化的測試
        /// <summary>日期多國語系格式化的測試
        /// </summary>
        [Description("DoModelSimpleFormatToViewDataTest測試，輸入純基本型別的目標Model")]
        [TestMethod()]
        public void DoModelSimpleFormatToViewDataTest()
        {
            var model = new TestModel()
            {
                Money = 2000M,
                Date = new DateTime(2013, 3, 5)
            };

            var formatter = new DataFormatter();

            var resultModel = formatter.DoModelSimpleFormat(model);
            Assert.IsNotNull(resultModel);

            var tModel = (TestModel)resultModel;
            Assert.AreEqual("2,000.00", tModel["Money"]);
            Assert.AreEqual("2013/03/05", tModel["Date"]);

            //================================================================================

            var r = formatter.DoModelSimpleFormat(model, language: "en-US");
            Assert.IsNotNull(r);
            var m = (TestModel)r;
            Assert.AreEqual("2,000.00", m["Money"]);
            Assert.AreEqual("03-05-2013", m["Date"]);

            //================================================================================

            r = formatter.DoModelSimpleFormat(model, typeof(TestVModel), language: "en-US");
            Assert.IsNotNull(r);
            var m2 = (TestModel)r;
            Assert.AreEqual("2,000.00", m2.ViewModel.Money);
            Assert.AreEqual("03-05-2013", m2.ViewModel.Date);

            //================================================================================

            r = formatter.DoModelDeepFormat(model, language: "en-US");
            Assert.IsNotNull(r);
            var m3 = (TestModel)r;
            Assert.AreEqual("2,000.00", m3["Money"]);
            Assert.AreEqual("03-05-2013", m3["Date"]);

            //================================================================================

            r = formatter.DoModelDeepFormat(model, typeof(TestVModel), language: "en-US");
            Assert.IsNotNull(r);
            var m4 = (TestModel)r;
            Assert.AreEqual("2,000.00", m4.ViewModel.Money);
            Assert.AreEqual("03-05-2013", m4.ViewModel.Date);
        }

        [Description("DoModelSimpleFormatToViewDataTest測試，輸入全部屬性接沒標示Attribute的目標Model")]
        [TestMethod()]
        public void DoModelSimpleFormatToViewDataTest2()
        {
            var model = new TestModel3()
            {
                Money = 2000M,
                Date = new DateTime(2013, 3, 5)
            };

            var formatter = new DataFormatter();

            var resultModel = formatter.DoModelSimpleFormat(model);
            Assert.IsNotNull(resultModel);

            var tModel = (TestModel3)resultModel;
            Assert.AreEqual("", tModel["Money"]);
            Assert.AreEqual("", tModel["Date"]);
        }

        [Description("DoModelSimpleFormatToViewDataTest測試，輸入含有List屬性的目標Model")]
        [TestMethod()]
        public void DoModelSimpleFormatToViewDataTest3()
        {
            var listMoney = new List<decimal>
            {
                100.2M,
                200.55M
            };

            var model = new TestListModel()
            {
                Moneys = listMoney,
                Date = new DateTime(2013, 3, 5)
            };

            var formatter = new DataFormatter();

            var resultModel = formatter.DoModelSimpleFormat(model);
            Assert.IsNotNull(resultModel);

            var tModel = (TestListModel)resultModel;
            Assert.AreEqual("", tModel["Money"]);
            Assert.AreEqual("", tModel["Date"]);
        }

        [Description("DoModelSimpleFormatToViewDataTest測試，輸入含有屬性的Attribute標示錯誤的目標Model")]
        [TestMethod()]
        public void DoModelSimpleFormatToViewDataTest4()
        {
            var model = new TestErrorAttriModel()
            {
                Money = 2000M,
                Date = new DateTime(2013, 3, 5)
            };

            var formatter = new DataFormatter();

            var resultModel = formatter.DoModelSimpleFormat(model);
            Assert.IsNotNull(resultModel);

            var tModel = (TestErrorAttriModel)resultModel;
            Assert.AreEqual("", tModel["Money"]);
            Assert.AreEqual("", tModel["Date"]);
        }

        [Description("DoModelSampleFormat例列外測試，輸入目標Model為Null")]
        [TestMethod()]
        public void DoModelSimpleFormatToViewDataExceptionTest()
        {
            object model = null;     // 輸入Null
            var formatter = new DataFormatter();
            var resultModel = formatter.DoModelSimpleFormat(model);
            Assert.IsNull(resultModel);
        }

        [Description("測試隱碼屬性")]
        [TestMethod]
        public void DoModelSimpleFormatToMaskTest()
        {
            var model = new TestMaskFormatAttrModel
            {
                Name = "AAAAAAAAA",
                Name2 = "AAAAAAAAA",
                Name3 = "AAAAAAAAA",
                Age = 18,
                Time = new DateTime(2013, 12, 13)
            };

            var formatter = new DataFormatter();
            var result = formatter.DoModelSimpleFormat(model);

            Assert.IsNotNull(model);

            var tmodel = (TestMaskFormatAttrModel)result;
            Assert.AreEqual("AXXXXXXXX", tmodel["Name"]);
            Assert.AreEqual("AAXXXAAAA", tmodel["Name2"]);
            Assert.AreEqual("AA!!!AAAA", tmodel["Name3"]);
            Assert.AreEqual("1!", tmodel["Age"]);
            Assert.AreEqual("20!!!12/13", tmodel["Time"]);
        }

        #endregion DoModelSimpleFormat( object model )的單元測試


        #region DoModelSimpleFormat( object model, Type viewModelType )的單元測試

        /// <summary>
        ///DoModelSimpleFormat 的測試
        ///</summary>
        [Description("DoModelSampleFormat測試，輸入純基本型別的目標Model")]
        [TestMethod()]
        public void DoModelSimpleFormatTest()
        {
            var model = new TestModel
            {
                Money = 2000M,
                Date = new DateTime(2013, 3, 5)
            };

            var viewModelType = typeof(TestVModel);
            var formatter = new DataFormatter();

            var resultModel = formatter.DoModelSimpleFormat(model, viewModelType);
            Assert.IsNotNull(resultModel);

            var tModel = (TestModel)resultModel;
            Assert.AreEqual("2,000.00", tModel["Money"]);
            Assert.AreEqual("2013/03/05", tModel["Date"]);

            var vModel = tModel.ViewModel;
            Assert.IsNotNull(vModel);
            Assert.AreEqual("2,000.00", vModel.Money);
            Assert.AreEqual("2013/03/05", vModel.Date);
        }

        [Description("DoModelSimpleFormat測試，輸入DateTime屬性沒標示Attribute的目標Model")]
        [TestMethod()]
        public void DoModelSimpleFormatTest2()
        {
            var model = new TestModel2()
            {
                Money = 2000M,
                Date = new DateTime(2013, 3, 5)
            };

            var viewModelType = typeof(TestVModel2);
            var formatter = new DataFormatter();

            var resultModel = formatter.DoModelSimpleFormat(model, viewModelType);
            Assert.IsNotNull(resultModel);

            var tModel = (TestModel2)resultModel;
            Assert.AreEqual("2,000.00", tModel["Money"]);
            Assert.AreEqual("", tModel["Date"]);

            var vModel = tModel.ViewModel;
            Assert.IsNotNull(vModel);
            Assert.IsNotNull(vModel.Money);
            // 20130316 Pony Comment: 這邊幾天修正為ViewModel中的屬性值都會初始化為空字串，不會為Null值
            //Assert.IsNull( vModel.Date );
            Assert.AreEqual("2,000.00", vModel.Money);
        }

        [Description("DoModelSampleFormat測試，輸入全部屬性接沒標示Attribute的目標Model")]
        [TestMethod()]
        public void DoModelSimpleFormatTest3()
        {
            var model = new TestModel3()
            {
                Money = 2000M,
                Date = new DateTime(2013, 3, 5)
            };

            var viewModelType = typeof(TestVModel3);
            var formatter = new DataFormatter();

            var resultModel = formatter.DoModelSimpleFormat(model, viewModelType);
            Assert.IsNotNull(resultModel);

            var tModel = (TestModel3)resultModel;
            Assert.AreEqual("", tModel["Money"]);
            Assert.AreEqual("", tModel["Date"]);

            var vModel = tModel.ViewModel;
            Assert.IsNotNull(vModel);

            // 20130316 Pony Comment: 這邊幾天修正為ViewModel中的屬性值都會初始化為空字串，不會為Null值
            //Assert.IsNull( vModel.Money );
            //Assert.IsNull( vModel.Date );
        }

        [Description("DoModelSampleFormat測試，輸入含有List屬性的目標Model")]
        [TestMethod()]
        public void DoModelSimpleFormatTest4()
        {
            var listMoney = new List<decimal>
            {
                100.2M,
                200.55M
            };

            var model = new TestListModel()
            {
                Moneys = listMoney,
                Date = new DateTime(2013, 3, 5)
            };

            var viewModelType = typeof(TestListVModel);
            var formatter = new DataFormatter();

            var resultModel = formatter.DoModelSimpleFormat(model, viewModelType);
            Assert.IsNotNull(resultModel);

            var tModel = (TestListModel)resultModel;
            Assert.AreEqual("", tModel["Money"]);
            Assert.AreEqual("", tModel["Date"]);

            var vModel = tModel.ViewModel;
            Assert.IsNull(vModel);
        }

        [Description("DoModelSampleFormat測試，輸入含有屬性的Attribute標示錯誤的目標Model")]
        [TestMethod()]
        public void DoModelSimpleFormatTest5()
        {
            var model = new TestErrorAttriModel()
            {
                Money = 2000M,
                Date = new DateTime(2013, 3, 5)
            };

            var viewModelType = typeof(TestErrorAttriVModel);
            var formatter = new DataFormatter();

            var resultModel = formatter.DoModelSimpleFormat(model, viewModelType);
            Assert.IsNotNull(resultModel);

            var tModel = (TestErrorAttriModel)resultModel;
            Assert.AreEqual("", tModel["Money"]);
            Assert.AreEqual("", tModel["Date"]);

            var vModel = tModel.ViewModel;
            Assert.IsNull(vModel);
        }

        [TestMethod]
        [Description("測試資料隱碼深度格式化")]
        public void DoModelDeepFormatToMaskTest()
        {
            var model = new TestMaskModel
            {
                Name = "AAAAAAAAA",
                Name2 = "AAAAAAAAA",
                Name3 = "AAAAAAAAA",
                Age = 18,
                Time = new DateTime(2013, 12, 13),
                DeepData = new VeryDeepModel() { DeepName = "AAAAAAAAA" }
            };

            var formatter = new DataFormatter();
            var result = formatter.DoModelDeepFormat(model, typeof(TestMaskVModel));

            Assert.IsNotNull(model);

            var resultModel = (TestMaskModel)result;

            Assert.AreEqual("AXXXXXXXX", resultModel.ViewModel.Name);
            Assert.AreEqual("AAXXXAAAA", resultModel.ViewModel.Name2);
            Assert.AreEqual("AA!!!AAAA", resultModel.ViewModel.Name3);
            Assert.AreEqual("1!", resultModel.ViewModel.Age);
            Assert.AreEqual("20!!!12/13", resultModel.ViewModel.Time);
            Assert.AreEqual("AAXXXAAAA", resultModel.DeepData.ViewModel.DeepName);
        }

        // 20130318 Added by Pony
        [Description("DoModelDeepFormatTest測試，完整的深度格式化正常測試")]
        [TestMethod()]
        public void DoModelDeepFormatTest()
        {
            #region 準備測試資料

            var model = new MasterModel
            {
                Name = "John",
                Birthday = new DateTime(1988, 1, 1),
                Age = 16,
                DetailInfo = new DetailInfoModel()
                {
                    InfoName = "JJJ",
                    NAVDate = new DateTime(2013, 5, 7)
                },
                Details =
            [
                new DetailModel
                {
                    DetailName       = "IUIU",
                    Length          = 3M,
                    Pay             = 45673.52434M,
                    NavDate         = new DateTime( 2013, 3, 5 ),
                    Name            = "Joe",
                    MaskStartIndex  = 1,
                    MaskLength      = 1,
                    Name2           = "Jimmy"
                },
                new DetailModel
                {
                    DetailName      = "Jack",
                    Length          = 2M,
                    Pay             = 785.5497M,
                    NavDate         = new DateTime( 2013, 7, 5 ),
                    Name            = "Kindding",
                    MaskStartIndex  = 2,
                    MaskLength      = 3,
                    Name2           = "Ruby"
                }
            ]
            };

            #endregion 準備測試資料

            #region 呼叫目標方法

            var viewModelType = typeof(MasterVModel);
            var formatter = new DataFormatter();

            var result = formatter.DoModelDeepFormat(model, viewModelType);
            Assert.IsNotNull(result);

            #endregion 呼叫目標方法

            #region 驗證格式化結果

            var resultModel = (MasterModel)result;
            var resultVModel = resultModel.ViewModel;

            Assert.IsNotNull(resultModel);
            Assert.IsNotNull(resultVModel);
            Assert.IsNotNull(resultModel.DetailInfo.ViewModel);

            Assert.AreEqual("1988/01/01", resultVModel.Birthday);
            Assert.AreEqual("16.00", resultVModel.Age);
            Assert.AreEqual("2013/05/07", resultModel.DetailInfo.ViewModel.NAVDate);
            Assert.AreEqual("", resultModel.DetailInfo.ViewModel.InfoName);

            Assert.AreEqual("1988/01/01", resultModel["Birthday"]);
            Assert.AreEqual("16.00", resultModel["Age"]);
            Assert.AreEqual("2013/05/07", resultModel.DetailInfo["NAVDate"]);
            Assert.AreEqual("", resultModel.DetailInfo["InfoName"]);

            foreach (var item in resultModel.Details)
            {
                Assert.IsNotNull(item.ViewModel);
            }

            var vModel1 = resultModel.Details[0].ViewModel;
            var model1 = resultModel.Details[0];

            Assert.AreEqual("", vModel1.DetailName);
            Assert.AreEqual("45,673.524", vModel1.Pay);
            Assert.AreEqual("2013/03/05", vModel1.NavDate);
            Assert.AreEqual("JXe", vModel1.Name);
            Assert.AreEqual("XXmmy", vModel1.Name2);

            Assert.AreEqual("", model1["DetailName"]);
            Assert.AreEqual("45,673.524", model1["Pay"]);
            Assert.AreEqual("2013/03/05", model1["NavDate"]);
            Assert.AreEqual("JXe", model1["Name"]);
            Assert.AreEqual("XXmmy", model1["Name2"]);

            var vModel2 = resultModel.Details[1].ViewModel;
            var model2 = resultModel.Details[1];

            Assert.AreEqual("", vModel2.DetailName);
            Assert.AreEqual("785.55", vModel2.Pay);
            Assert.AreEqual("2013/07/05", vModel2.NavDate);
            Assert.AreEqual("KiXXXing", vModel2.Name);
            Assert.AreEqual("XXby", vModel2.Name2);

            Assert.AreEqual("", model2["DetailName"]);
            Assert.AreEqual("785.55", model2["Pay"]);
            Assert.AreEqual("2013/07/05", model2["NavDate"]);
            Assert.AreEqual("KiXXXing", model2["Name"]);
            Assert.AreEqual("XXby", model2["Name2"]);

            #endregion 驗證格式化結果
        }

        // 20150730 Added by Pony: DateTime 日期時間支援多國語系的格式化
        [Description("DoModelDeepFormatTest測試，完整的深度格式化正常測試")]
        [TestMethod()]
        public void DoModelDeepFormatTest2()
        {
            #region 準備測試資料

            var model = new MasterModel
            {
                Name = "John",
                Birthday = new DateTime(1988, 1, 1),
                Age = 16,

                DetailInfo = new DetailInfoModel()
                {
                    InfoName = "JJJ",
                    NAVDate = new DateTime(2013, 5, 7)
                },

                Details =
                [
                    new DetailModel
                    {
                        DetailName       = "IUIU",
                        Length          = 3M,
                        Pay             = 45673.52434M,
                        NavDate         = new DateTime( 2013, 3, 5 ),
                        Name            = "Joe",
                        MaskStartIndex  = 1,
                        MaskLength      = 1,
                        Name2           = "Jimmy"
                    },
                    new DetailModel
                    {
                        DetailName      = "Jack",
                        Length          = 2M,
                        Pay             = 785.5497M,
                        NavDate         = new DateTime( 2013, 7, 5 ),
                        Name            = "Kindding",
                        MaskStartIndex  = 2,
                        MaskLength      = 3,
                        Name2           = "Ruby"
                    }
                ]
            };

            #endregion 準備測試資料

            #region 呼叫目標方法

            var viewModelType = typeof(MasterVModel);
            var formatter = new DataFormatter();

            var result = formatter.DoModelDeepFormat(model, viewModelType, language: "en-US");
            Assert.IsNotNull(result);

            #endregion 呼叫目標方法

            #region 驗證格式化結果

            var resultModel = (MasterModel)result;
            var resultVModel = resultModel.ViewModel;

            Assert.IsNotNull(resultModel);
            Assert.IsNotNull(resultVModel);
            Assert.IsNotNull(resultModel.DetailInfo.ViewModel);

            Assert.AreEqual("01-01-1988", resultVModel.Birthday);
            Assert.AreEqual("16.00", resultVModel.Age);
            Assert.AreEqual("05-07-2013", resultModel.DetailInfo.ViewModel.NAVDate);
            Assert.AreEqual("", resultModel.DetailInfo.ViewModel.InfoName);

            Assert.AreEqual("01-01-1988", resultModel["Birthday"]);
            Assert.AreEqual("16.00", resultModel["Age"]);
            Assert.AreEqual("05-07-2013", resultModel.DetailInfo["NAVDate"]);
            Assert.AreEqual("", resultModel.DetailInfo["InfoName"]);

            foreach (var item in resultModel.Details)
            {
                Assert.IsNotNull(item.ViewModel);
            }

            var vModel1 = resultModel.Details[0].ViewModel;
            var model1 = resultModel.Details[0];

            Assert.AreEqual("", vModel1.DetailName);
            Assert.AreEqual("45,673.524", vModel1.Pay);
            Assert.AreEqual("03-05-2013", vModel1.NavDate);
            Assert.AreEqual("JXe", vModel1.Name);
            Assert.AreEqual("XXmmy", vModel1.Name2);

            Assert.AreEqual("", model1["DetailName"]);
            Assert.AreEqual("45,673.524", model1["Pay"]);
            Assert.AreEqual("03-05-2013", model1["NavDate"]);
            Assert.AreEqual("JXe", model1["Name"]);
            Assert.AreEqual("XXmmy", model1["Name2"]);

            var vModel2 = resultModel.Details[1].ViewModel;
            var model2 = resultModel.Details[1];

            Assert.AreEqual("", vModel2.DetailName);
            Assert.AreEqual("785.55", vModel2.Pay);
            Assert.AreEqual("07-05-2013", vModel2.NavDate);
            Assert.AreEqual("KiXXXing", vModel2.Name);
            Assert.AreEqual("XXby", vModel2.Name2);

            Assert.AreEqual("", model2["DetailName"]);
            Assert.AreEqual("785.55", model2["Pay"]);
            Assert.AreEqual("07-05-2013", model2["NavDate"]);
            Assert.AreEqual("KiXXXing", model2["Name"]);
            Assert.AreEqual("XXby", model2["Name2"]);

            #endregion 驗證格式化結果
        }

        // 20131219 Added by Pony
        [Description("FormatDynamicModel測試，Formatting格式化模組利用 ZayniFramework Dynamic API來進行深度格式化的測試")]
        [TestMethod()]
        public void FormatDynamicModelTest()
        {
            #region 準備測試資料

            var model = new MasterModel2
            {
                Name = "John",
                Birthday = new DateTime(1988, 1, 1),
                Age = 16,
                DetailInfo = new DetailInfoModel2()
                {
                    InfoName = "JJJ",
                    NAVDate = new DateTime(2013, 5, 7)
                },
                Details =
                [
                    new DetailModel2
                    {
                        DetailName = "IUIU",
                        Length     = 3M,
                        Pay        = 45673.52434M,
                        NavDate    = new DateTime( 2013, 3, 5 )
                    },
                    new DetailModel2
                    {
                        DetailName = "Jack",
                        Length     = 2M,
                        Pay        = 785.5497M,
                        NavDate    = new DateTime( 2013, 7, 5 )
                    }
                ]
            };

            #endregion 準備測試資料

            #region 呼叫目標方法

            var formatter = new DataFormatter();

            var result = formatter.FormatDynamicModel(model);
            Assert.IsNotNull(result);

            #endregion 呼叫目標方法

            #region 驗證格式化結果

            var resultModel = (MasterModel2)result;
            dynamic resultVModel = resultModel.ViewModel;

            Assert.IsNotNull(resultModel);
            Assert.IsNotNull(resultVModel);
            Assert.IsNotNull(resultModel.DetailInfo.ViewModel);

            Assert.AreEqual("1988/01/01", resultVModel.Birthday);
            Assert.AreEqual("16.00", resultVModel.Age);
            Assert.AreEqual("2013/05/07", resultModel.DetailInfo.ViewModel.NAVDate);
            //Assert.AreEqual( "",            resultModel.DetailInfo.ViewModel.InfoName );    // 沒有標記FormatAttribute的屬性，不會動態的產生在ViewModel動態物件中

            Assert.AreEqual("1988/01/01", resultModel["Birthday"]);
            Assert.AreEqual("16.00", resultModel["Age"]);
            Assert.AreEqual("2013/05/07", resultModel.DetailInfo["NAVDate"]);
            Assert.AreEqual("", resultModel.DetailInfo["InfoName"]);

            foreach (var item in resultModel.Details)
            {
                Assert.IsNotNull(item.ViewModel);
            }

            dynamic vModel1 = resultModel.Details[0].ViewModel;
            var model1 = resultModel.Details[0];

            //Assert.AreEqual( "",            vModel1.DetailName );     // 沒有標記FormatAttribute的屬性，不會動態的產生在ViewModel動態物件中
            Assert.AreEqual("45,673.524", vModel1.Pay);
            Assert.AreEqual("2013/03/05", vModel1.NavDate);

            Assert.AreEqual("", model1["DetailName"]);
            Assert.AreEqual("45,673.524", model1["Pay"]);
            Assert.AreEqual("2013/03/05", model1["NavDate"]);

            dynamic vModel2 = resultModel.Details[1].ViewModel;
            var model2 = resultModel.Details[1];

            //Assert.AreEqual( "",            vModel2.DetailName );     // 沒有標記FormatAttribute的屬性，不會動態的產生在ViewModel動態物件中
            Assert.AreEqual("785.55", vModel2.Pay);
            Assert.AreEqual("2013/07/05", vModel2.NavDate);

            Assert.AreEqual("", model2["DetailName"]);
            Assert.AreEqual("785.55", model2["Pay"]);
            Assert.AreEqual("2013/07/05", model2["NavDate"]);

            #endregion 驗證格式化結果
        }

        // 20150730 Added by Pony: DateTime 日期時間支援多國語系的格式化
        [Description("FormatDynamicModel測試，Formatting格式化模組利用 ZayniFramework Dynamic API來進行深度格式化的測試")]
        [TestMethod()]
        public void FormatDynamicModelTest2()
        {
            #region 準備測試資料

            var model = new MasterModel2
            {
                Name = "John",
                Birthday = new DateTime(1988, 1, 1),
                Age = 16,
                DetailInfo = new DetailInfoModel2()
                {
                    InfoName = "JJJ",
                    NAVDate = new DateTime(2013, 5, 7)
                },
                Details =
                [
                    new() {
                        DetailName = "IUIU",
                        Length     = 3M,
                        Pay        = 45673.52434M,
                        NavDate    = new DateTime( 2013, 3, 5 )
                    },
                    new() {
                        DetailName = "Jack",
                        Length     = 2M,
                        Pay        = 785.5497M,
                        NavDate    = new DateTime( 2013, 7, 5 )
                    }
                ]
            };

            #endregion 準備測試資料

            #region 呼叫目標方法

            var formatter = new DataFormatter();

            var result = formatter.FormatDynamicModel(model, language: "en-US");
            Assert.IsNotNull(result);

            #endregion 呼叫目標方法

            #region 驗證格式化結果

            var resultModel = (MasterModel2)result;
            dynamic resultVModel = resultModel.ViewModel;

            Assert.IsNotNull(resultModel);
            Assert.IsNotNull(resultVModel);
            Assert.IsNotNull(resultModel.DetailInfo.ViewModel);

            Assert.AreEqual("01-01-1988", resultVModel.Birthday);
            Assert.AreEqual("16.00", resultVModel.Age);
            Assert.AreEqual("05-07-2013", resultModel.DetailInfo.ViewModel.NAVDate);
            //Assert.AreEqual( "",            resultModel.DetailInfo.ViewModel.InfoName );    // 沒有標記FormatAttribute的屬性，不會動態的產生在ViewModel動態物件中

            Assert.AreEqual("01-01-1988", resultModel["Birthday"]);
            Assert.AreEqual("16.00", resultModel["Age"]);
            Assert.AreEqual("05-07-2013", resultModel.DetailInfo["NAVDate"]);
            Assert.AreEqual("", resultModel.DetailInfo["InfoName"]);

            foreach (var item in resultModel.Details)
            {
                Assert.IsNotNull(item.ViewModel);
            }

            dynamic vModel1 = resultModel.Details[0].ViewModel;
            var model1 = resultModel.Details[0];

            Assert.AreEqual("45,673.524", vModel1.Pay);
            Assert.AreEqual("03-05-2013", vModel1.NavDate);

            Assert.AreEqual("", model1["DetailName"]);
            Assert.AreEqual("45,673.524", model1["Pay"]);
            Assert.AreEqual("03-05-2013", model1["NavDate"]);

            dynamic vModel2 = resultModel.Details[1].ViewModel;
            var model2 = resultModel.Details[1];

            //Assert.AreEqual( "",            vModel2.DetailName );     // 沒有標記FormatAttribute的屬性，不會動態的產生在ViewModel動態物件中
            Assert.AreEqual("785.55", vModel2.Pay);
            Assert.AreEqual("07-05-2013", vModel2.NavDate);

            Assert.AreEqual("", model2["DetailName"]);
            Assert.AreEqual("785.55", model2["Pay"]);
            Assert.AreEqual("07-05-2013", model2["NavDate"]);

            #endregion 驗證格式化結果
        }

        [Description("DoModelSampleFormat例列外測試，輸入目標Model為Null")]
        [TestMethod()]
        public void DoModelSimpleFormatExceptionTest()
        {
            object model = null;     // 輸入Null

            var viewModelType = typeof(TestVModel);
            var formatter = new DataFormatter();

            var resultModel = formatter.DoModelSimpleFormat(model, viewModelType);
            Assert.IsNull(resultModel);
        }

        [Description("DoModelSampleFormat例列外測試，輸入viewModelType為Null")]
        [TestMethod()]
        public void DoModelSimpleFormatExceptionTest2()
        {
            var model = new TestModel()
            {
                Money = 2000M,
                Date = new DateTime(2013, 3, 5)
            };

            Type viewModelType = null; // 輸入Null
            var formatter = new DataFormatter();

            var resultModel = formatter.DoModelSimpleFormat(model, viewModelType);
            Assert.IsNotNull(resultModel);

            var tModel = (TestModel)resultModel;
            Assert.AreEqual("", tModel["Money"]);
            Assert.AreEqual("", tModel["Date"]);

            var vModel = tModel.ViewModel;
            Assert.IsNull(vModel);
        }

        #endregion DoModelSimpleFormat( object model, Type viewModelType )的單元測試


        #region DoModelDeepFormat( object model )單元測試

        [Description("DoModelDeepFormat 字典集合資料測試，輸入正確資料")]
        [TestMethod()]
        public void DoModelDeepFormatToViewDataTest()
        {
            var model = new TestDeepModel()
            {
                Money = 2000M,
                Date = new DateTime(2013, 3, 5),
                Numbers = []
            };

            for (var i = 0; i < 3; i++)
            {
                var d = new TestDataModel()
                {
                    Price = 123456.25631M,
                    Length = 3
                };

                model.Numbers.Add(d);
            }

            var formatter = new DataFormatter();

            var resultModel = formatter.DoModelDeepFormat(model);
            Assert.IsNotNull(resultModel);

            var tModel = (TestDeepModel)resultModel;
            Assert.AreEqual("2,000.00", tModel["Money"]);
            Assert.AreEqual("2013/03/05", tModel["Date"]);

            // Model List格式化後的資料比對
            foreach (var d in tModel.Numbers)
            {
                Assert.AreEqual("123,456.256", d["Price"]);
            }
        }

        [Description("DoModelDeepFormat 字典集合資料反向測試，輸入Model集合資料未建立")]
        [TestMethod()]
        public void DoModelDeepFormatToViewDataOppositeTest()
        {
            var model = new TestDeepModel()
            {
                Money = 2000M,
                Date = new DateTime(2013, 3, 5),
                Numbers = []
            };

            var formatter = new DataFormatter();

            var resultModel = formatter.DoModelDeepFormat(model);
            Assert.IsNotNull(resultModel);

            var tModel = (TestDeepModel)resultModel;
            Assert.AreEqual("2,000.00", tModel["Money"]);
            Assert.AreEqual("2013/03/05", tModel["Date"]);

            // Model List格式化後的資料比對
            foreach (var d in tModel.Numbers)
            {
                Assert.AreEqual("", d["Price"]);
            }
        }

        [Description("DoModelDeepFormat 字典集合資料例外測試，輸入Model為Null")]
        [TestMethod()]
        public void DoModelDeepFormatToViewDataExceptionTest()
        {
            object model = null;      // 目標Model為Null

            var formatter = new DataFormatter();
            var resultModel = formatter.DoModelDeepFormat(model);
            Assert.IsNull(resultModel);
        }

        [Description("DoModelDeepFormat 字典集合資料反向測試，輸入Model的屬性值都為Null的情況")]
        [TestMethod()]
        public void DoModelDeepFormatOppsiteTest()
        {
            var model = new UserTestModel();
            var formatter = new DataFormatter();

            var resultModel = formatter.DoModelDeepFormat(model, typeof(UserTestVModel));
            Assert.IsNotNull(resultModel);

            var result = (UserTestModel)resultModel;
            var resultVModel = result.ViewModel;
            Assert.IsNotNull(resultVModel);

            Assert.IsNotNull(resultVModel.UserBDay);
            Assert.AreEqual(string.Empty, resultVModel.UserName);
            Assert.AreEqual("0", resultVModel.UserSalary);

            Assert.IsNotNull(result.Members);
            Assert.AreEqual(0, result.Members.Count);
        }

        [Description("DoModelDeepFormat 字典集合資料反向測試，輸入Model的屬性值都為Null的情況")]
        [TestMethod()]
        public void DoModelDeepFormatOppsiteTest2()
        {
            var model = new UserTestModel
            {
                Members =
                [
                    new MemberModel(),
                    new MemberModel(),
                    new MemberModel(),
                ]
            };

            var formatter = new DataFormatter();

            var resultModel = formatter.DoModelDeepFormat(model, typeof(UserTestVModel));
            Assert.IsNotNull(resultModel);

            var result = (UserTestModel)resultModel;
            var resultVModel = result.ViewModel;
            Assert.IsNotNull(resultVModel);

            Assert.IsNotNull(resultVModel.UserBDay);
            Assert.IsNotNull(result.Member);
            Assert.IsNotNull(result.Member.ViewModel);
            Assert.AreEqual(string.Empty, resultVModel.UserName);
            Assert.AreEqual("0", resultVModel.UserSalary);

            Assert.IsNotNull(result.Members);
            Assert.AreNotEqual(0, result.Members.Count);

            foreach (var m in result.Members)
            {
                Assert.IsNotNull(m.ViewModel);
            }
        }

        [Description("DoModelDeepFormat 字典集合資料例外測試，輸入Model集合資料未建立")]
        [TestMethod()]
        public void DoModelDeepFormatToViewDataExceptionTest2()
        {
            var model = new TestDeepModel()
            {
                Money = 2000M,
                Date = new DateTime(2013, 3, 5),
                Numbers = null      // List為Null
            };

            var formatter = new DataFormatter();

            var resultModel = formatter.DoModelDeepFormat(model);
            Assert.IsNotNull(resultModel);

            var tModel = (TestDeepModel)resultModel;
            Assert.AreEqual("2,000.00", tModel["Money"]);
            Assert.AreEqual("2013/03/05", tModel["Date"]);
            Assert.IsNull(tModel.Numbers);
        }

        [Description("測試格式化ModelFormatter 深度格式化API如果在標記FormatAttribute錯誤的的情況，ViewModel防呆機制是否正常")]
        [TestMethod()]
        public void DoModelDeepFormatStupidTest()
        {
            var userModel = new UserModel
            {
                UserName = "ViVi",
                UserBDay = new DateTime(1990, 1, 1),
                UserSalary = 579521.5354M
            };

            var formatter = new DataFormatter();

            var model = formatter.DoModelDeepFormat(userModel, typeof(UserVModel));
            var resultModel = (UserModel)model;
            var resultVModel = resultModel.ViewModel;

            Assert.IsNotNull(resultModel);
            Assert.IsNotNull(resultVModel);

            Assert.AreEqual(string.Empty, resultVModel.UserName);
            Assert.AreEqual(string.Empty, resultVModel.UserBDay);
            Assert.AreEqual(string.Empty, resultVModel.UserSalary);
        }

        [Description("測試格式化ModelFormatter 深度格式化API如果在標記FormatAttribute錯誤的的情況，ViewModel防呆機制是否正常")]
        [TestMethod()]
        public void DoModelSimpleFormatStupidTest()
        {
            var userModel = new UserModel
            {
                UserName = "ViVi",
                UserBDay = new DateTime(1990, 1, 1),
                UserSalary = 579521.5354M
            };

            var formatter = new DataFormatter();

            var model = formatter.DoModelSimpleFormat(userModel, typeof(UserVModel));
            var resultModel = (UserModel)model;
            var resultVModel = resultModel.ViewModel;

            Assert.IsNotNull(resultModel);
            Assert.IsNotNull(resultVModel);

            Assert.AreEqual(string.Empty, resultVModel.UserName);
            Assert.AreEqual(string.Empty, resultVModel.UserBDay);
            Assert.AreEqual(string.Empty, resultVModel.UserSalary);
        }

        // 20130318 Added by Pony
        [TestMethod()]
        [Description("測試格式化ModelFormatter 深度格式化傳入的目標Model的屬性都為Null值，第一層ViewModel防呆機制是否正常")]
        public void DoModelDeepFormatStupidTest2()
        {
            var model = new UserTestModel();
            var member = new MemberModel();
            model.Member = member;
            var formatter = new DataFormatter();

            var resultModel = formatter.DoModelDeepFormat(model, typeof(UserTestVModel), checkDefaultValue: true);
            Assert.IsNotNull(resultModel);

            var result = (UserTestModel)resultModel;
            var resultVModel = result.ViewModel;
            Assert.IsNotNull(resultVModel);

            Assert.AreEqual(string.Empty, resultVModel.UserName);
            Assert.AreEqual(string.Empty, resultVModel.UserSalary);
            Assert.AreEqual(string.Empty, resultVModel.UserBDay);

            Assert.IsNotNull(result.Members);
            Assert.IsNotNull(result.Member);

            Assert.IsNotNull(result.Member.ViewModel);
            Assert.AreEqual(string.Empty, result.Member.ViewModel.MemberDate);
        }

        // 20130318 Added by Pony
        [TestMethod()]
        [Description("測試格式化ModelFormatter 深度格式化傳入的目標Model的屬性都為Null值，第一層ViewModel防呆機制是否正常")]
        public void DoModelDeepFormatStupidTest3()
        {
            var model = new UserTestModel();
            var member = new MemberModel();
            model.Member = member;

            model.Members =
            [
                new MemberModel()
            ];

            var formatter = new DataFormatter();

            var resultModel = formatter.DoModelDeepFormat(model, typeof(UserTestVModel), checkDefaultValue: true);
            Assert.IsNotNull(resultModel);

            var result = (UserTestModel)resultModel;
            var resultVModel = result.ViewModel;
            Assert.IsNotNull(resultVModel);

            Assert.AreEqual(string.Empty, resultVModel.UserName);
            Assert.AreEqual(string.Empty, resultVModel.UserSalary);
            Assert.AreEqual(string.Empty, resultVModel.UserBDay);

            Assert.IsNotNull(result.Members);
            Assert.IsNotNull(result.Member);

            Assert.IsNotNull(result.Member.ViewModel);
            Assert.AreEqual(string.Empty, result.Member.ViewModel.MemberDate);

            var actual = result.Members[0].ViewModel.MemberDate;
            Assert.AreEqual(string.Empty, actual);

        }

        #endregion DoModelDeepFormat( object model )的單元測試
    }
}
