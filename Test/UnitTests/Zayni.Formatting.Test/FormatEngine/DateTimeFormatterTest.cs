﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Globalization;
using ZayniFramework.Formatting;


namespace Formatting.Test
{
    /// <summary>
    ///這是 DateTimeFormatterTest 的測試類別，應該包含
    ///所有 DateTimeFormatterTest 單元測試
    ///</summary>
    [TestClass()]
    public class DateTimeFormatterTest
    {
        private TestContext testContextInstance;

        /// <summary>
        ///取得或設定提供目前測試回合的相關資訊與功能
        ///的測試內容。
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region 其他測試屬性
        // 
        //您可以在撰寫測試時，使用下列的其他屬性:
        //
        //在執行類別中的第一項測試之前，先使用 ClassInitialize 執行程式碼
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //在執行類別中的所有測試之後，使用 ClassCleanup 執行程式碼
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //在執行每一項測試之前，先使用 TestInitialize 執行程式碼
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //在執行每一項測試之後，使用 TestCleanup 執行程式碼
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        [Description("對DateTimeFormatter的正常測試，yyyy/MM/dd HH:mm:ss")]
        [TestMethod()]
        public void TryFormatTest()
        {
            var target = new DateTime(2013, 3, 1, 23, 20, 10);
            var formatter = new DateTimeFormatter();

            var info = new FormatInfo
            {
                TypeName = "DateTime",
                FormatString = "yyyy/MM/dd HH:mm:ss"
            };
            var actual = formatter.TryFormat(target, info, out var formatted, out _);
            Assert.IsTrue(actual);

            var expected = target.ToString(info.FormatString, CultureInfo.InvariantCulture);
            Assert.AreEqual(expected, formatted, "DateTime格式化結果錯誤");
        }

        [Description("對DateTimeFormatter的正常測試，yyyy-MM-dd")]
        [TestMethod()]
        public void TryFormatTest2()
        {
            var target = new DateTime(2013, 3, 1);
            var formatter = new DateTimeFormatter();

            var info = new FormatInfo
            {
                TypeName = "DateTime",
                FormatString = "yyyy-MM-dd"
            };
            var actual = formatter.TryFormat(target, info, out var formatted, out _);
            Assert.IsTrue(actual);

            var expected = target.ToString(info.FormatString, CultureInfo.InvariantCulture);
            Assert.AreEqual(expected, formatted, "DateTime格式化結果錯誤");
        }

        [Description("對DateTimeFormatter的反向測試，輸入錯誤型態")]
        [TestMethod()]
        public void TryFormatOppositeTest()
        {
            var target = 20130202;     // 型別錯誤
            var formatter = new DateTimeFormatter();

            var info = new FormatInfo
            {
                TypeName = "DateTime",
                FormatString = "yyyy-MM-dd"
            };
            var actual = formatter.TryFormat(target, info, out _, out _);
            Assert.IsFalse(actual);
        }

        [Description("對DateTimeFormatter的反向測試，錯誤的格式化字串")]
        [TestMethod()]
        public void TryFormatOppositeTest2()
        {
            var target = new DateTime(2013, 3, 1);
            var formatter = new DateTimeFormatter();

            var info = new FormatInfo
            {
                TypeName = "DateTime",
                FormatString = "125648pp"   // 錯誤格式化字串
            };
            var actual = formatter.TryFormat(target, info, out var formatted, out _);
            Assert.IsTrue(actual);

            var expected = target.ToString("yyyy-MM-dd");
            Assert.AreNotEqual(expected, formatted, "DateTime格式化結果錯誤");
        }

        [Description("對DateTimeFormatter的例外測試，欲格式化的target為Null")]
        [TestMethod()]
        public void TryFormatExceptionTest()
        {
            object target = null;   // 輸入為Null

            var formatter = new DateTimeFormatter();

            var info = new FormatInfo
            {
                TypeName = "DateTime",
                FormatString = "yyyy-MM-dd"
            };
            var actual = formatter.TryFormat(target, info, out _, out _);
            Assert.IsFalse(actual);
        }

        [Description("對DateTimeFormatter的例外測試，格式化字串為null")]
        [TestMethod()]
        public void TryFormatExceptionTest2()
        {
            var target = new DateTime(2013, 3, 1);
            var formatter = new DateTimeFormatter();

            var info = new FormatInfo
            {
                TypeName = "DateTime",
                FormatString = null         // 輸入為Null
            };
            var actual = formatter.TryFormat(target, info, out var formatted, out _);
            Assert.IsTrue(actual);

            var expected = target.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            Assert.AreNotEqual(expected, formatted, "DateTime格式化結果錯誤");
        }
    }
}
