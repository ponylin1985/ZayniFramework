﻿using System;
using System.Collections.Generic;
using ZayniFramework.Formatting;


namespace Formatting.Test;


/// <summary>簡單資料型別的模型
/// </summary>
public class TestModel : BasicModel<TestVModel>
{
    /// <summary>
    /// </summary>
    /// <value></value>
    [NumberFormat(Length = 2)]
    public decimal Money { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    [DateFormat()]
    public DateTime Date { get; set; }
}

/// <summary>
/// </summary>
public class TestVModel : BasicViewModel
{
    /// <summary>
    /// </summary>
    /// <value></value>
    public string Money { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string Date { get; set; }
}

// 正常基本型別的Model.
/// <summary>
/// </summary>
public class TestModel2 : BasicModel<TestVModel2>
{
    /// <summary>
    /// </summary>
    /// <value></value>
    [NumberFormat(Length = 2)]
    public decimal Money { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public DateTime Date { get; set; }
}

/// <summary>
/// </summary>
public class TestVModel2 : BasicViewModel
{
    /// <summary>
    /// </summary>
    /// <value></value>
    public string Money { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string Date { get; set; }
}

// 正常基本型別的Model.
/// <summary>
/// </summary>
public class TestModel3 : BasicModel<TestVModel3>
{
    /// <summary>
    /// </summary>
    /// <value></value>
    public decimal Money { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public DateTime Date { get; set; }
}

/// <summary>
/// </summary>
public class TestVModel3 : BasicViewModel
{
    /// <summary>
    /// </summary>
    /// <value></value>
    public string Money { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string Date { get; set; }
}

// 含有List屬性的Model.
/// <summary>
/// </summary>
public class TestListModel : BasicModel<TestListVModel>
{
    /// <summary>
    /// </summary>
    /// <value></value>
    [CollectionFormat()]
    public List<decimal> Moneys { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    [DateFormat()]
    public DateTime Date { get; set; }
}

/// <summary>
/// </summary>
public class TestListVModel : BasicViewModel
{
    // 20130316 Pony Comment: Stephen 還是搞不清楚狀況，ViewModel 中不允許有這種型別的屬性
    //public List< string > Moneys { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string Date { get; set; }
}

// attribute 標示錯誤的Model
/// <summary>
/// </summary>
public class TestErrorAttriModel : BasicModel<TestErrorAttriVModel>
{
    /// <summary>
    /// </summary>
    /// <value></value>
    [CollectionFormat()]
    public decimal Money { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    [DateFormat()]
    public DateTime Date { get; set; }
}

/// <summary>
/// </summary>
public class TestErrorAttriVModel : BasicViewModel
{
    /// <summary>
    /// </summary>
    /// <value></value>
    public string Money { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string Date { get; set; }
}

/// <summary>
/// </summary>
public class TestMaskFormatAttrModel : BasicModel
{
    /// <summary>
    /// </summary>
    /// <value></value>
    [MaskFormat(StartIndex = 1)]
    public string Name { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    [MaskFormat(StartIndex = 2, Length = 3)]
    public string Name2 { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    [MaskFormat(StartIndex = 2, Length = 3, ReplaceChar = '!')]
    public string Name3 { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    [MaskFormat(StartIndex = 1, Length = 1, ReplaceChar = '!')]
    public int Age { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    [MaskFormat(StartIndex = 2, Length = 3, ReplaceChar = '!')]
    public DateTime Time { get; set; }
}

/// <summary>
/// </summary>
public class TestMaskModel : BasicModel<TestMaskVModel>
{
    /// <summary>
    /// </summary>
    /// <value></value>
    [MaskFormat(StartIndex = 1)]
    public string Name { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    [MaskFormat(StartIndex = 2, Length = 3)]
    public string Name2 { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    [MaskFormat(StartIndex = 2, Length = 3, ReplaceChar = '!')]
    public string Name3 { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    [MaskFormat(StartIndex = 1, Length = 1, ReplaceChar = '!')]
    public int Age { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    [MaskFormat(StartIndex = 2, Length = 3, ReplaceChar = '!')]
    public DateTime Time { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    [CollectionFormat()]
    public VeryDeepModel DeepData { get; set; }
}

/// <summary>
/// </summary>
public class VeryDeepModel : BasicModel<VeryDeepVModel>
{
    /// <summary>
    /// </summary>
    /// <value></value>
    [MaskFormat(StartIndex = 2, Length = 3)]
    public string DeepName { get; set; }
}

/// <summary>
/// </summary>
public class TestMaskVModel : BasicViewModel
{
    /// <summary>
    /// </summary>
    /// <value></value>
    public string Name { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string Name2 { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string Name3 { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string Age { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string Time { get; set; }
}

/// <summary>
/// </summary>
public class VeryDeepVModel : BasicViewModel
{
    /// <summary>
    /// </summary>
    /// <value></value>
    public string DeepName { get; set; }
}
