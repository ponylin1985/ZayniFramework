﻿using System;
using ZayniFramework.Formatting;


namespace Formatting.Test;

/// <summary>
/// </summary>
public class DetailModel : BasicModel<DetailVModel>
{
    /// <summary>
    /// </summary>
    /// <value></value>
    public string DetailName
    {
        get;
        set;
    }

    /// <summary>
    /// </summary>
    /// <value></value>
    public decimal Length
    {
        get;
        set;
    }

    /// <summary>
    /// </summary>
    /// <value></value>
    [NumberFormat(LengthPropertyName = "Length")]
    public decimal Pay
    {
        get;
        set;
    }

    /// <summary>
    /// </summary>
    /// <value></value>
    [DateFormat()]
    public DateTime NavDate
    {
        get;
        set;
    }

    /// <summary>
    /// </summary>
    /// <value></value>
    [MaskFormat(StartIndexPropertyName = "MaskStartIndex", LengthPropertyName = "MaskLength")]
    public string Name
    {
        get;
        set;
    }

    /// <summary>
    /// </summary>
    /// <value></value>
    public int MaskStartIndex
    {
        get;
        set;
    }

    /// <summary>
    /// </summary>
    /// <value></value>
    public int MaskLength
    {
        get;
        set;
    }

    /// <summary>
    /// </summary>
    /// <value></value>
    [MaskFormat(Length = 2)]
    public string Name2
    {
        get;
        set;
    }
}

/// <summary>
/// </summary>
public class DetailModel2 : BasicDynamicModel
{
    /// <summary>
    /// </summary>
    /// <value></value>
    public string DetailName
    {
        get;
        set;
    }

    /// <summary>
    /// </summary>
    /// <value></value>
    public decimal Length
    {
        get;
        set;
    }

    /// <summary>
    /// </summary>
    /// <value></value>
    [NumberFormat(LengthPropertyName = "Length")]
    public decimal Pay
    {
        get;
        set;
    }

    /// <summary>
    /// </summary>
    /// <value></value>
    [DateFormat()]
    public DateTime NavDate
    {
        get;
        set;
    }
}

/// <summary>
/// </summary>
public class DetailVModel : BasicViewModel
{
    /// <summary>
    /// </summary>
    /// <value></value>
    public string DetailName
    {
        get;
        set;
    }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string Length
    {
        get;
        set;
    }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string Pay
    {
        get;
        set;
    }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string NavDate
    {
        get;
        set;
    }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string Name
    {
        get;
        set;
    }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string Name2
    {
        get;
        set;
    }
}

