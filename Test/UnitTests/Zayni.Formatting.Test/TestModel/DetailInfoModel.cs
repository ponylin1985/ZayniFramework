﻿using System;
using ZayniFramework.Formatting;


namespace Formatting.Test;

/// <summary>
/// </summary>
public class DetailInfoModel : BasicModel<DetailInfoVModel>
{
    /// <summary>
    /// </summary>
    /// <value></value>
    public string InfoName
    {
        get;
        set;
    }

    /// <summary>
    /// </summary>
    /// <value></value>
    [DateFormat()]
    public DateTime NAVDate
    {
        get;
        set;
    }
}

/// <summary>
/// </summary>
public class DetailInfoModel2 : BasicDynamicModel
{
    /// <summary>
    /// </summary>
    /// <value></value>
    public string InfoName
    {
        get;
        set;
    }

    /// <summary>
    /// </summary>
    /// <value></value>
    [DateFormat()]
    public DateTime NAVDate
    {
        get;
        set;
    }
}

/// <summary>
/// </summary>
public class DetailInfoVModel : BasicViewModel
{
    /// <summary>
    /// </summary>
    /// <value></value>
    public string InfoName
    {
        get;
        set;
    }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string NAVDate
    {
        get;
        set;
    }
}

