﻿using System;
using System.Collections.Generic;
using ZayniFramework.Formatting;


namespace Formatting.Test;


/// <summary>
/// </summary>
public class MasterModel : BasicModel<MasterVModel>
{
    /// <summary>
    /// </summary>
    /// <value></value>
    [CollectionFormat()]
    public List<DetailModel> Details
    {
        get;
        set;
    }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string Name
    {
        get;
        set;
    }

    /// <summary>
    /// </summary>
    /// <value></value>
    [DateFormat()]
    public DateTime Birthday
    {
        get;
        set;
    }

    /// <summary>
    /// </summary>
    /// <value></value>
    [NumberFormat(Length = 2)]
    public decimal Age
    {
        get;
        set;
    }

    /// <summary>
    /// </summary>
    /// <value></value>
    [CollectionFormat()]
    public DetailInfoModel DetailInfo
    {
        get;
        set;
    }
}

/// <summary>
/// </summary>
public class MasterModel2 : BasicDynamicModel
{
    /// <summary>
    /// </summary>
    /// <value></value>
    [CollectionFormat()]
    public List<DetailModel2> Details
    {
        get;
        set;
    }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string Name
    {
        get;
        set;
    }

    /// <summary>
    /// </summary>
    /// <value></value>
    [DateFormat()]
    public DateTime Birthday
    {
        get;
        set;
    }

    /// <summary>
    /// </summary>
    /// <value></value>
    [NumberFormat(Length = 2)]
    public decimal Age
    {
        get;
        set;
    }

    /// <summary>
    /// </summary>
    /// <value></value>
    [CollectionFormat()]
    public DetailInfoModel2 DetailInfo
    {
        get;
        set;
    }
}

/// <summary>
/// </summary>
public class MasterVModel : BasicViewModel
{
    /// <summary>
    /// </summary>
    /// <value></value>
    public string Name
    {
        get;
        set;
    }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string Birthday
    {
        get;
        set;
    }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string Age
    {
        get;
        set;
    }

    // ViewModel中不應該有String以外型別的屬性
    //public DetailInfoModel DetailInfo
    //{
    //    get;
    //    set;
    //}
}

