﻿using System;
using System.Collections.Generic;
using ZayniFramework.Formatting;


namespace Formatting.Test;

// List 資料型別的模型
/// <summary>
/// </summary>
public class TestDataModel : BasicModel
{
    /// <summary>
    /// </summary>
    /// <value></value>
    [NumberFormat(LengthPropertyName = "Length")]
    public decimal Price { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public int Length { get; set; }
}

/// <summary>
/// </summary>
public class TestDeepModel : BasicModel
{
    /// <summary>
    /// </summary>
    /// <value></value>
    [NumberFormat(Length = 2)]
    public decimal Money { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    [DateFormat()]
    public DateTime Date { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    [CollectionFormat()]
    public List<TestDataModel> Numbers { get; set; }
}

/// <summary>
/// </summary>
public class UserTestModel : BasicModel<UserTestVModel>
{
    /// <summary>
    /// </summary>
    /// <value></value>
    public string UserName { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    [DateTimeFormat()]
    public DateTime UserBDay { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    [NumberFormat(LengthPropertyName = "SalaryLength")]
    public decimal UserSalary { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public int SalaryLength { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    [CollectionFormat()]
    public List<MemberModel> Members { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    [CollectionFormat()]
    public MemberModel Member { get; set; }
}

/// <summary>
/// </summary>
public class MemberModel : BasicModel<MemberVModel>
{
    /// <summary>
    /// </summary>
    /// <value></value>
    [DateTimeFormat()]
    public DateTime MemberDate { get; set; }
}

/// <summary>
/// </summary>
public class MemberVModel : BasicViewModel
{
    /// <summary>
    /// </summary>
    /// <value></value>
    public string MemberDate { get; set; }
}

/// <summary>
/// </summary>
public class UserTestVModel : BasicViewModel
{
    /// <summary>
    /// </summary>
    /// <value></value>
    public string UserName { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string UserBDay { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string UserSalary { get; set; }

    // 在ViewModel中不會放string以外型別的屬性
    //public List<MemberModel> Members { get; set; }
}

/// <summary>
/// </summary>
public class UserModel : BasicModel<UserVModel>
{
    // 20130313 Comment by Pony: 故意什麼FormatAttribute都沒標記

    /// <summary>
    /// </summary>
    /// <value></value>
    public string UserName { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public DateTime UserBDay { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public decimal UserSalary { get; set; }
}

/// <summary>
/// </summary>
public class UserVModel : BasicViewModel
{
    /// <summary>
    /// </summary>
    /// <value></value>
    public string UserName { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string UserBDay { get; set; }

    /// <summary>
    /// </summary>
    /// <value></value>
    public string UserSalary { get; set; }
}

