﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Reflection;
using ZayniFramework.Common;
using ZayniFramework.Cryptography;


namespace Cryptography.Test
{
    /// <summary>AES對稱式加解密元件的測試類別
    /// </summary>
    [TestClass]
    public class AesEncryptorTester
    {
        #region 測試組件初始化

        /// <summary>初始化整個 Test project
        /// </summary>
        /// <param name="testcontext"></param>
        [AssemblyInitialize()]
        public static void Init(TestContext testcontext)
        {
            // 在 .NET Core 版本的 MSTest，一定要自行指定 .config 的完整絕對路徑到 ConfigManagement.ConfigFullPath 中，整個 ZayniFramework 舊有在 XML Config 的機制才會正常運作！
            var path = $"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}/zayni.json";
            ConfigManager.Init(path);
        }

        #endregion 測試組件初始化


        #region 宣告私有的欄位

        /// <summary>對稱式加密演算法
        /// </summary>
        private static string _encryptMode = "aes";

        #endregion 宣告私有的欄位


        #region 宣告測式框架的事件處理

        /// <summary>初始化測試資料
        /// </summary>
        /// <param name="context">測試環境上下文</param>
        [ClassInitialize()]
        public static void Initialize(TestContext context)
        {
            _encryptMode = "aes";
        }

        #endregion 宣告測式框架的事件處理


        #region 宣告測試方法

        /// <summary>資料對稱式加密測式
        /// </summary>
        [TestMethod()]
        [TestCategory("Cryptography Test")]
        [Description("資料對稱式加密測式")]
        public void EncryptionTest()
        {
            var source = "ABCDEFG_HIJKLMN_OPQRST_UVW_XYZ";

            var encryptor = SymmetricEncryptorFactory.Create(_encryptMode);
            var result = encryptor.Encrypt(source);
            var plain = encryptor.Decrypt(result);

            Assert.IsTrue(result.IsNotNullOrEmpty());
            Assert.AreEqual(plain, source);

            // ===========================================================

            source = "喔，Yeah，這是一個UTF-8的加密測式啊!!! $%::+=- nn ●€ 還有奇怪的字元喔... 』┴◎ おはようございます %``<<||좋은 아침§【这才是测试】....。。。。^54Cgs";
            result = encryptor.Encrypt(source);
            plain = encryptor.Decrypt(result);

            Assert.IsTrue(result.IsNotNullOrEmpty());
            Assert.AreEqual(plain, source);
        }

        /// <summary>對 AES 加解密元件的測試
        /// </summary>
        [TestMethod()]
        [TestCategory("Cryptography AES Encryption Test")]
        [Description("對 AES 加解密元件的測試")]
        public void AesEncryptionTest()
        {
            var encryptor = new AesEncryptor();
            var text = "abcdefghijklmnopqrspuvwxyz1abcdefghijklmnopqrspuvwzyz1abcdefghijklmnopqrspuvwzyz1abcdefghijklmnopqrspuvwzyz1abcdefghijklmnopqrspuvwzyz1";

            var encryptString = encryptor.Encrypt(text);
            Assert.IsTrue(encryptString.IsNotNullOrEmpty());

            var decryptionString = encryptor.Decrypt(encryptString);
            Assert.AreEqual(text, decryptionString);
        }

        /// <summary>對 AES 加解密元件的測試
        /// </summary>
        [TestMethod()]
        [TestCategory("Cryptography AES Encryption Test")]
        [Description("對 AES 加解密元件的測試")]
        public void AesEncryptionTest_ChineseTextTest()
        {
            var encryptor = new AesEncryptor();
            var text = "你好嗎?! 我很好喔++//~`";

            var encryptString = encryptor.Encrypt(text);
            Assert.IsTrue(encryptString.IsNotNullOrEmpty());

            var decryptionString = encryptor.Decrypt(encryptString);
            Assert.AreEqual(text, decryptionString);
        }

        /// <summary>對 AES 加解密元件的測試 2
        /// </summary>
        [TestMethod()]
        [TestCategory("Cryptography AES Encryption Test")]
        [Description("對 AES 加解密元件的測試 2")]
        public void AesEncryptionTest2()
        {
            var encryptor = new AesEncryptor();
            var text = "12345";

            var encryptString = encryptor.Encrypt(text);
            Assert.IsTrue(encryptString.IsNotNullOrEmpty());

            var encyptData = encryptString.ToUtf8Bytes();
            Assert.IsTrue(encyptData.IsNotNullOrEmpty());

            var encryedStr = encyptData.GetStringFromUtf8Bytes();
            Assert.AreEqual(encryptString, encryedStr);

            var decryptionString = encryptor.Decrypt(encryptString);
            Assert.AreEqual(text, decryptionString);
        }

        /// <summary>對 AES 加解密元件的測試，沒有傳入密碼的測試
        /// </summary>
        [TestMethod()]
        [TestCategory("Cryptography AES Encryption Test")]
        [Description("對 AES 加解密元件的測試，沒有傳入密碼的測試")]
        public void AesEncryptionTest_NoPassword()
        {
            var encryptor = new AesEncryptor();

            var text = "abcdefghijklmnopqrspuvwxyz1abcdefghijklmnopqrspuvwzyz1abcdefghijklmnopqrspuvwzyz1abcdefghijklmnopqrspuvwzyz1abcdefghijklmnopqrspuvwzyz1";

            var encryptString = encryptor.Encrypt(text);
            Assert.IsTrue(encryptString.IsNotNullOrEmpty());

            var decryptionString = encryptor.Decrypt(encryptString);
            Assert.AreEqual(text, decryptionString);
        }

        /// <summary>對 AES 加解密元件的測試，沒有傳入密碼的測試 2
        /// </summary>
        [TestMethod()]
        [TestCategory("Cryptography AES Encryption Test")]
        [Description("對 AES 加解密元件的測試，沒有傳入密碼的測試 2")]
        public void AesEncryptionTest_NoPassword2()
        {
            var encryptor = new AesEncryptor();

            var text = "12345";

            var encryptString = encryptor.Encrypt(text);
            Assert.IsTrue(encryptString.IsNotNullOrEmpty());

            var encyptData = encryptString.ToUtf8Bytes();
            Assert.IsTrue(encyptData.IsNotNullOrEmpty());

            var encryedStr = encyptData.GetStringFromUtf8Bytes();
            Assert.AreEqual(encryptString, encryedStr);

            var decryptionString = encryptor.Decrypt(encryptString);
            Assert.AreEqual(text, decryptionString);
        }

        #endregion 宣告測試方法
    }
}
