﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StackExchange.Redis;
using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using ZayniFramework.Caching.RedisClientComponent;
using ZayniFramework.Common;
using ZayniFramework.Serialization;


namespace Caching.RedisClientComponent.Test
{
    // dotnet test ./Test/UnitTests/Zayni.Caching.RedisClient.Test/Zayni.Caching.RedisClient.Test.csproj --filter ClassName=Caching.RedisClientComponent.Test.RedisClientTester
    /// <summary>RedisClient 客戶端的測試類別
    /// </summary>
    [TestClass()]
    public class RedisClientTester
    {
        #region 測試組件初始化

        /// <summary>初始化整個 Test project
        /// </summary>
        /// <param name="testContext"></param>
        [AssemblyInitialize()]
        public static void Init(TestContext testContext)
        {
            // 在 .NET Core 版本的 MSTest，一定要自行指定 .config 的完整絕對路徑到 ConfigManagement.ConfigFullPath 中，整個 ZayniFramework 舊有在 XML Config 的機制才會正常運作！
            var path = $"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}/zayni.json";
            ConfigManager.Init(path);
        }

        #endregion 測試組件初始化


        /// <summary>RedisClient 直接使用 IDatabase 執行指令的測試
        /// </summary>
        [TestMethod()]
        [Description("RedisClient 直接使用 IDatabase 執行指令的測試")]
        public void RedisClient_IDatabase_Test()
        {
            var redisClient = RedisClientContainer.Get("Local-Dev");
            var isSuccess = redisClient.Db.StringSet("ZayniTest1", "AAA", when: StackExchange.Redis.When.Always);
            Assert.IsTrue(isSuccess);
            string result = redisClient.Db.StringGet("ZayniTest1");
            Assert.AreEqual("AAA", result);

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, TimeSpan.FromSeconds(1));
#endif
        }

        /// <summary>RedisClient 直接使用 IDatabase 執行非同步指令的測試
        /// </summary>
        [TestMethod()]
        [Description("RedisClient 直接使用 IDatabase 執行非同步指令的測試")]
        public async Task RedisClient_IDatabase_TestAsync()
        {
            var redisClient = RedisClientContainer.Get("Local-Dev");

            var key = "ZayniTest1";
            var value = "AAA";

            var isSuccess = await redisClient.Db.StringSetAsync(key, value, when: StackExchange.Redis.When.Always);
            Assert.IsTrue(isSuccess);

            string result = await redisClient.Db.StringGetAsync(key);
            Assert.AreEqual(value, result);

            var success = await redisClient.Db.KeyDeleteAsync(key);
            Assert.IsTrue(success);

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, TimeSpan.FromSeconds(1));
#endif
        }

        /// <summary>RedisClient 使用 ExecResult 方法執行 StringSet、StringGet 指令的測試
        /// </summary>
        [TestMethod()]
        [Description("RedisClient 使用 ExecResult 方法執行指令的測試")]
        public void RedisClient_ExecResult_Test()
        {
            var redisClient = RedisClientContainer.Get();

            Delegate stringSet = (Func<string, string, StackExchange.Redis.When, bool>)((key, value, when) => redisClient.Db.StringSet(key, value, when: when));
            var r1 = redisClient.ExecResult<bool>(stringSet, "ZayniTest2", "BBB", StackExchange.Redis.When.Always);
            Assert.IsTrue(r1.Success);
            Assert.IsTrue(r1.Data);

            Delegate stringGet = (Func<string, string>)(key => redisClient.Db.StringGet(key));
            var r2 = redisClient.ExecResult<string>(stringGet, "ZayniTest2");
            Assert.IsTrue(r2.Success);
            Assert.AreEqual("BBB", r2.Data);

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, TimeSpan.FromSeconds(1));
#endif
        }

        /// <summary>RedisClient 使用 ExecResult 方法執行 StringSet、StringGet 指令的測試 2 (Value 存放 JSON 資料)
        /// </summary>
        [TestMethod()]
        [Description("RedisClient 使用 ExecResult 方法執行指令的測試")]
        public void RedisClient_ExecResult_Test2()
        {
            var redisClient = RedisClientContainer.Get();

            var source = new
            {
                UserName = "Nancy",
                UserSex = "female",
                UserBirthday = new DateTime(1992, 4, 16),
                UserLevel = 5,
                Balance = 66875654.45,
                Currency = "TWD",
                IsSuperUser = true,
                CreateTime = DateTime.Now
            };

            var json = JsonSerialize.SerializeObject(source);

            Delegate stringSet = (Func<string, string, StackExchange.Redis.When, bool>)((key, value, when) => redisClient.Db.StringSet(key, value, when: when));
            var r1 = redisClient.ExecResult<bool>(stringSet, "ZayniTest5", json, StackExchange.Redis.When.Always);
            Assert.IsTrue(r1.Success);
            Assert.IsTrue(r1.Data);

            Delegate stringGet = (Func<string, string>)(key => redisClient.Db.StringGet(key));
            var r2 = redisClient.ExecResult<string>(stringGet, "ZayniTest5");
            Assert.IsTrue(r2.Success);
            Assert.IsTrue(r2.Data.IsNotNullOrEmpty());

            // 這邊要注意一下，如果當初序列化的物件，為一種匿名型別的物件，使用 Json.NET 反序列化後使用 dynamic 接的時候，要用 Value 屬性才能取得到真正的資料值。
            dynamic model = NewtonsoftJsonConvert.Deserialize(json);
            Assert.AreEqual(source.UserName, model.UserName.Value);
            Assert.AreEqual(source.UserSex, model.UserSex.Value);
            Assert.AreEqual(source.UserBirthday, model.UserBirthday.Value);
            Assert.AreEqual(source.UserLevel, model.UserLevel.Value);
            Assert.AreEqual(source.Currency, model.Currency.Value);
            Assert.AreEqual(source.IsSuperUser, model.IsSuperUser.Value);
            Assert.AreEqual(source.CreateTime, model.CreateTime.Value);

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, TimeSpan.FromSeconds(1));
#endif
        }

        /// <summary>RedisClient 使用 ExecResult 方法執行 HashSet、HashGet 指令的測試
        /// </summary>
        [TestMethod()]
        [Description("RedisClient 使用 ExecResult 方法執行指令的測試")]
        public void RedisClient_ExecResult_HashTest()
        {
            var redisClient = RedisClientContainer.Get("Local-Dev");

            var hashName = new HashEntry("name", "Pony Lin");
            var hashAge = new HashEntry("age", 32);
            var hashSex = new HashEntry("sex", 1);
            var hashDoB = new HashEntry("dob", "1985-05-25");

            var hashSet = (Action<string, HashEntry[]>)((key, hashFields) => redisClient.Db.HashSet(key, hashFields));
            var r = redisClient.Exec(hashSet, "ZayniTest3", new HashEntry[] { hashName, hashAge, hashSex, hashDoB });
            Assert.IsTrue(r.Success);

            var hashGet = (Func<string, string, string>)((key, hashField) => redisClient.Db.HashGet(key, hashField));
            var name = redisClient.ExecResult<string>(hashGet, "ZayniTest3", "name").Data;
            var age = redisClient.ExecResult<string>(hashGet, "ZayniTest3", "age").Data;
            var sex = redisClient.ExecResult<string>(hashGet, "ZayniTest3", "sex").Data;
            var dob = redisClient.ExecResult<string>(hashGet, "ZayniTest3", "dob").Data;
            var yyy = redisClient.ExecResult<string>(hashGet, "ZayniTest3", "yyy").Data;   // 故意嘗試去得一個不存在的 hash field

            Assert.AreEqual("Pony Lin", name);
            Assert.AreEqual("32", age);
            Assert.AreEqual("1", sex);
            Assert.AreEqual("1985-05-25", dob);
            Assert.IsNull(yyy);

            // ======================

            hashName = new HashEntry("name", "Amber");
            hashAge = new HashEntry("age", 24);
            hashSex = new HashEntry("sex", 0);
            hashDoB = new HashEntry("dob", "1993-04-16");

            hashSet = (Action<string, HashEntry[]>)((key, hashFields) => redisClient.Db.HashSet(key, hashFields));
            r = redisClient.Exec(hashSet, "ZayniTest4", new HashEntry[] { hashName, hashAge, hashSex, hashDoB });
            Assert.IsTrue(r.Success);

            hashGet = (Func<string, string, string>)((key, hashField) => redisClient.Db.HashGet(key, hashField));
            name = redisClient.ExecResult<string>(hashGet, "ZayniTest4", "name").Data;
            age = redisClient.ExecResult<string>(hashGet, "ZayniTest4", "age").Data;
            sex = redisClient.ExecResult<string>(hashGet, "ZayniTest4", "sex").Data;
            dob = redisClient.ExecResult<string>(hashGet, "ZayniTest4", "dob").Data;
            yyy = redisClient.ExecResult<string>(hashGet, "ZayniTest4", "xxx").Data;   // 故意嘗試去得一個不存在的 hash field

            Assert.AreEqual("Amber", name);
            Assert.AreEqual("24", age);
            Assert.AreEqual("0", sex);
            Assert.AreEqual("1993-04-16", dob);
            Assert.IsNull(yyy);

            // ======================

            var hashAge2 = new HashEntry("age", 25);
            var hashIsBitch = new HashEntry("isBitch", true);
            var hashComment = new HashEntry("comment", "Amber is a slut.");

            hashSet = (Action<string, HashEntry[]>)((key, hashFields) => redisClient.Db.HashSet(key, hashFields));
            redisClient.Exec(hashSet, "ZayniTest4", new HashEntry[] { hashAge2, hashIsBitch, hashComment });

            hashGet = (Func<string, string, string>)((key, hashField) => redisClient.Db.HashGet(key, hashField));
            age = redisClient.ExecResult<string>(hashGet, "ZayniTest4", "age").Data;
            var isBitch = redisClient.ExecResult<string>(hashGet, "ZayniTest4", "isBitch").Data;
            var comment = redisClient.ExecResult<string>(hashGet, "ZayniTest4", "comment").Data;

            Assert.AreEqual("25", age);
            Assert.IsTrue("1" == isBitch);    // 這邊要注意，如果直接塞一個 boolean 值到 Redis Server，重新取得的 Value 值，會是 1 或 0。
            Assert.AreEqual("Amber is a slut.", comment);

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, TimeSpan.FromSeconds(1));
#endif
        }

        /// <summary>RedisClient PutHashObject 和 GetHashObject 的測試
        /// </summary>
        [TestMethod()]
        [Description("RedisClient PutHashObject 和 GetHashObject 的測試")]
        public void RedisClient_PutHashObject_Test()
        {
            var source = new UserModel()
            {
                Name = "Nancy",
                Age = 26,
                Birthday = new DateTime(1993, 2, 24),
                CashBalance = 3000,
                TotalAssets = 4100
            };

            var redisClient = RedisClientContainer.Get("Local-Dev");

            var key = $"ZayniTest123:{source.Name}";

            var p = redisClient.PutHashObject(key, source, TimeSpan.FromSeconds(15));
            Assert.IsTrue(p.Success);

            var g = redisClient.GetHashObject<UserModel>(key);
            Assert.IsTrue(g.Success);
            Assert.AreEqual(source.Name, g.Data.Name);
            Assert.AreEqual(source.Age, g.Data.Age);
            Assert.AreEqual(source.Birthday, g.Data.Birthday);
            Assert.AreEqual(source.CashBalance, g.Data.CashBalance);
            Assert.AreEqual(source.TotalAssets, g.Data.TotalAssets);

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, TimeSpan.FromSeconds(1));
#endif
        }
    }
}
