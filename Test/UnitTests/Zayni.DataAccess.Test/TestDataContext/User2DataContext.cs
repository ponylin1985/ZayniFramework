﻿using DataAccess.Test.TestModel;
using ZayniFramework.DataAccess.Lightweight;


namespace DataAccess.Test
{
    /// <summary>單元測試用使用者資料來源類別，測試 DataContext 類別
    /// </summary>
    internal class User2DataContext : DataContext<UserModel2, UserModel2>
    {
        #region 宣告建構子

        /// <summary>預設建構子
        /// </summary>
        internal User2DataContext() : base("ZayniUnitTest")
        {
        }

        #endregion 宣告建構子
    }
}
