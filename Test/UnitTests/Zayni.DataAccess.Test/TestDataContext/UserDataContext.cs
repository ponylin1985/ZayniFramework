﻿using DataAccess.Test.TestModel;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.DataAccess.Lightweight;


namespace DataAccess.Test
{
    /// <summary>單元測試用使用者資料來源類別，測試 DataContext 類別
    /// </summary>
    internal class UserDataContext : DataContext<UserReqArgs, UserModel>
    {
        #region 宣告建構子

        /// <summary>預設建構子
        /// </summary>
        internal UserDataContext() : base("ZayniUnitTest")
        {
        }

        #endregion 宣告建構子


        #region 宣告內部的方法

        /// <summary>依照 PKey 帳號進行查詢
        /// </summary>
        /// <param name="account">帳號</param>
        /// <returns>查詢結果</returns>
        internal async Task<IResult<UserModel>> SelectByAccountAsync(string account)
        {
            var result = Result.Create<UserModel>();

            var reqArgs = new UserModel()
            {
                Account = account
            };

            var q = await SqlEngine.SelectAsync<UserModel>(reqArgs);

            if (!q.Success)
            {
                result.Code = q.Code;
                result.Message = q.Message;
                return result;
            }

            var models = q.Data;

            if (models.IsNullOrEmpty())
            {
                result.Message = $"Data not found.";
                return result;
            }

            var model = models.FirstOrDefault();

            result.Data = model;
            result.Success = true;
            return result;
        }

        /// <summary>依照 PKey 帳號進行查詢
        /// </summary>
        /// <param name="account">帳號</param>
        /// <returns>查詢結果</returns>
        internal IResult<UserModel> SelectByAccount(string account)
        {
            var result = Result.Create<UserModel>();

            var reqArgs = new UserModel()
            {
                Account = account
            };

            var q = SqlEngine.Select<UserModel>(reqArgs);

            if (!q.Success)
            {
                result.Code = q.Code;
                result.Message = q.Message;
                return result;
            }

            var models = q.Data;

            if (models.IsNullOrEmpty())
            {
                result.Message = $"Data not found.";
                return result;
            }

            var model = models.FirstOrDefault();

            result.Data = model;
            result.Success = true;
            return result;
        }

        /// <summary>依照指定欄位進行查詢
        /// </summary>
        /// <param name="name">使用者姓名</param>
        /// <returns>查詢結果</returns>
        internal async Task<IResult<UserModel>> SelectByNameAsync(string name)
        {
            var result = Result.Create<UserModel>();

            var reqArgs = new UserModel()
            {
                Name = name
            };

            var q = await SqlEngine.WhereByAsync<UserModel>(reqArgs, ["NAME"]);

            if (!q.Success)
            {
                result.Code = q.Code;
                result.Message = q.Message;
                return result;
            }

            var models = q.Data;

            if (models.IsNullOrEmpty())
            {
                result.Message = $"Data not found. Name: {name}";
                return result;
            }

            var model = models.FirstOrDefault();

            result.Data = model;
            result.Success = true;
            return result;
        }

        /// <summary>依照指定欄位進行查詢
        /// </summary>
        /// <param name="name">使用者姓名</param>
        /// <returns>查詢結果</returns>
        internal IResult<UserModel> SelectByName(string name)
        {
            var result = Result.Create<UserModel>();

            var reqArgs = new UserModel()
            {
                Name = name
            };

            var q = SqlEngine.WhereBy<UserModel>(reqArgs, ["NAME"]);

            if (!q.Success)
            {
                result.Code = q.Code;
                result.Message = q.Message;
                return result;
            }

            var models = q.Data;

            if (models.IsNullOrEmpty())
            {
                result.Message = $"Data not found. Name: {name}";
                return result;
            }

            var model = models.FirstOrDefault();

            result.Data = model;
            result.Success = true;
            return result;
        }

        /// <summary>依照 PKey 和指定的資料庫連線、交易進行資料模型更新
        /// </summary>
        /// <param name="model">使用者資料模型</param>
        /// <returns>更新結果</returns>
        internal async Task<IResult<UserModel>> UpdateAsync(UserModel model, string[] ignoreColumns, DbConnection conn, DbTransaction trans)
        {
            var result = Result.Create<UserModel>();

            var r = await SqlEngine.UpdateAsync(model, connection: conn, transaction: trans, ignoreColumns: ignoreColumns);

            if (!r.Success)
            {
                result.Code = r.Code;
                result.Message = r.Message;
                return result;
            }

            var dModel = r.Data;

            result.Data = dModel;
            result.Success = true;
            return result;
        }

        /// <summary>依照 PKey 和指定的資料庫連線、交易進行資料模型更新
        /// </summary>
        /// <param name="model">使用者資料模型</param>
        /// <returns>更新結果</returns>
        internal IResult<UserModel> Update(UserModel model, string[] ignoreColumns, DbConnection conn, DbTransaction trans)
        {
            var result = Result.Create<UserModel>();

            var r = SqlEngine.Update(model, connection: conn, transaction: trans, ignoreColumns: ignoreColumns);

            if (!r.Success)
            {
                result.Code = r.Code;
                result.Message = r.Message;
                return result;
            }

            var dModel = r.Data;

            result.Data = dModel;
            result.Success = true;
            return result;
        }

        #endregion 宣告內部的方法
    }

    /// <summary>單元測試用使用者資料來源類別，測試 DataContext 類別<para/>
    /// </summary>
    internal class UserModelDataContext : DataContext<UserModel, UserModel>
    {
        #region 宣告建構子

        /// <summary>預設建構子
        /// </summary>
        internal UserModelDataContext() : base("ZayniUnitTest")
        {
        }

        #endregion 宣告建構子
    }
}
