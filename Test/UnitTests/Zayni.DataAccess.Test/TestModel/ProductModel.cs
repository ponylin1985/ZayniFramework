using System;
using ZayniFramework.Common.ORM;


namespace DataAccess.Test.TestModel
{
    /// <summary>產品資料模型
    /// </summary>
    [MappingTable(TableName = "FS_UNIT_TEST_PRODUCT")]
    internal class ProductModel
    {
        /// <summary>產品編號
        /// </summary>
        /// <value></value>
        [TableColumn(ColumnName = "PRODUCT_ID")]
        public Guid ProductId { get; set; }

        /// <summary>產品名稱
        /// </summary>
        /// <value></value>
        [TableColumn(ColumnName = "PRODUCT_NAME")]
        public string ProductName { get; set; }

        /// <summary>產品類型
        /// </summary>
        /// <value></value>
        [TableColumn(ColumnName = "PRODUCT_TYPE")]
        public byte ProductType { get; set; }

        /// <summary>產品建造時間
        /// </summary>
        /// <value></value>
        [TableColumn(ColumnName = "BUILD_TIME")]
        public DateTimeOffset BuildTime { get; set; }

        /// <summary>資料異動時間戳記。<para/>
        /// * IsTimeStamp = true 的屬性型別必須要為 long、long?、Int64 或 Int64?<para/>
        /// </summary>
        [TableColumn(ColumnName = "DATA_FLAG", IsTimeStamp = true)]
        public long? DataFlag { get; set; }


        #region Internal Methods (For Enum Properties)

        /// <summary>取得產品類型
        /// </summary>
        /// <typeparam name="ProductType"></typeparam>
        /// <returns></returns>
        internal ProductType GetProductType() => Enum.Parse<ProductType>(ProductType + "");

        #endregion Internal Methods (For Enum Properties)
    }

    /// <summary>產品類型
    /// </summary>
    internal enum ProductType : byte
    {
        /// <summary>玩具
        /// </summary>
        Toy = 1,

        /// <summary>電腦
        /// </summary>
        Computer = 2,

        /// <summary>書籍
        /// </summary>
        Book = 3,
    }
}