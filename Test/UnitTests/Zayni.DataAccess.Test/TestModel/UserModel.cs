﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using ZayniFramework.Common.ORM;
using ZayniFramework.DataAccess;


namespace DataAccess.Test.TestModel
{
    // 20170929 Pony Says: 目前以此資料模型的宣告，確定 MSSQL 和 MySQL 的 DataContext 的基本 CRUD 是可以正常運作的。
    // 20180304 Edited by Pony: 標記為資料異動時間戳記 (IsTimestamp = true) 的 Property，
    // 必須要將 Property 的型別宣告為 public 的 long、long?、Int64 或 Int64?
    // 只要 Timestamp 屬性有正確標記，而且型別宣告正確，DataContext 即可以自動處理。
    /// <summary>單元測試用使用者資料模型
    /// </summary>
    [MappingTable(TableName = "FS_UNIT_TEST_USER")]
    internal class UserModel
    {
        /// <summary>帳號
        /// </summary>
        [JsonProperty(PropertyName = "ACCOUNT_ID")]
        [TableColumn(ColumnName = "ACCOUNT_ID", IsPrimaryKey = true)]
        public string Account { get; set; }

        /// <summary>姓名
        /// </summary>
        [JsonProperty(PropertyName = "NAME")]
        [TableColumn(ColumnName = "NAME")]
        public string Name { get; set; }

        /// <summary>年齡
        /// </summary>
        [JsonProperty(PropertyName = "AGE")]
        [TableColumn(ColumnName = "AGE")]
        public int Age { get; set; }

        /// <summary>性別
        /// </summary>
        [JsonProperty(PropertyName = "SEX")]
        [TableColumn(ColumnName = "SEX", IsAllowNull = true, DefaultValue = "1")]
        public int Sex { get; set; }

        /// <summary>生日
        /// </summary>
        [JsonProperty(PropertyName = "BIRTHDAY")]
        [TableColumn(ColumnName = "BIRTHDAY", IsAllowNull = true, DefaultValue = null)]
        public DateTime? DOB { get; set; }

        /// <summary>是否為VIP會員
        /// </summary>
        /// <remarks>資料庫中 Boolean 布林值型別欄位:
        /// MySQL資料庫中: 
        /// 1. MySQL資料庫可以開成 Bit(1) 或 TinyInt(1)。
        /// 2. 程式中 MySqlDbType.Bit 可以直接對應。使用 MySqlDbType.Int32 也是可以，但比較不建議。
        /// 3. 程式中資料值可以直接為 bool 型別，或是將 bool 依照 0 = false，1 = true 的規則，轉換成 Int32 之後都可以。
        /// 4. 查詢使用者DataAdapter.Fill之後取得DataTable之後，直接使用 Newtonsoft.Json 進行ORM轉換，可以直接轉換成正確的 bool 型別。
        /// </remarks>
        [JsonProperty(PropertyName = "IS_VIP")]
        [TableColumn(ColumnName = "IS_VIP", IsAllowNull = true)]
        public bool? IsVip { get; set; }

        /// <summary>只有非 Nullable 的型別，才會支援查詢 DefaultValue 預設值機制。
        /// </summary>
        [JsonProperty(PropertyName = "IS_GOOD")]
        [TableColumn(ColumnName = "IS_GOOD", IsAllowNull = true, DefaultValue = "true")]
        public bool IsGood { get; set; }

        /// <summary>資料異動時間戳記。<para/>
        /// 1. IsTimeStamp = true 的屬性型別必須要為 long、long?、Int64 或 Int64?<para/>
        /// 2. 如果使給 BaseMySqlDal 使用的 Timestampe 屬性，則需要標記 Json.NET 的 [JsonConverter( typeof ( MySqlTimestampConverter ) )] Attribute。
        /// </summary>
        [JsonProperty(PropertyName = "DATA_FLAG")]
        [TableColumn(ColumnName = "DATA_FLAG", IsTimeStamp = true)]
        [JsonConverter(typeof(MySqlTimestampConverter))]
        public long? DataFlag { get; set; }

        /// <summary>連絡電話資料集合
        /// </summary>
        public List<UserPhoneDetailModel> PhoneDetails { get; set; }
    }

    /// <summary>使用者資料查詢請求
    /// </summary>
    [MappingTable(TableName = "FS_UNIT_TEST_USER")]
    internal class UserReqArgs : UserModel
    {
        // 因為這個資料模型作為查詢參數使用，因此，PKey 欄位指定 OrderBy、OrderByDesc 和 SqlOperator，
        // 並且使用 new 關鍵字強制覆寫掉 UserModel 父型別中的 Account PKey 屬性。
        /// <summary>帳號
        /// </summary>
        [JsonProperty(PropertyName = "ACCOUNT_ID")]
        [TableColumn(ColumnName = "ACCOUNT_ID", IsPrimaryKey = true, OrderBy = true, OrderByDesc = true, SqlOperator = "LIKE")]
        public new string Account { get; set; }
    }
}
