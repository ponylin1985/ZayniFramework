﻿using DataAccess.Test.TestDao;
using DataAccess.Test.TestData;
using DataAccess.Test.TestModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Serialization;


namespace DataAccess.Test
{
    // dotnet test ./Test/UnitTests/Zayni.DataAccess.Test/Zayni.DataAccess.Test.csproj --filter ClassName=DataAccess.Test.BaseDataAccessTest
    /// <summary>BaseDataAccess 的測試類別
    /// </summary>
    [TestClass()]
    public class BaseDataAccessTest
    {
        #region 測試組件初始化

        /// <summary>初始化整個 Test project
        /// </summary>
        /// <param name="testcontext"></param>
        [AssemblyInitialize()]
        public static void Init(TestContext testcontext)
        {
            // 在 .NET Core 版本的 MSTest，一定要自行指定 .config 的完整絕對路徑到 ConfigManagement.ConfigFullPath 中，整個 ZayniFramework 舊有在 XML Config 的機制才會正常運作！
            var path = $"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}/zayni.json";
            ConfigManager.Init(path);
        }

        #endregion 測試組件初始化


        #region 宣告測試初始化事件

        /// <summary>測試方法初始化
        /// </summary>
        [TestInitialize()]
        public async Task TestInitializeAsync() => await TestDataMaker.ClearTestDataAsync();

        /// <summary>重置測試資料
        /// </summary>
        [TestCleanup()]
        public async Task TestCleanupAsync() => await TestDataMaker.ClearTestDataAsync();

        #endregion 宣告測試事件


        #region 宣告測試方法

        /// <summary>MSSQL 資料庫對於 GUID、DateTimeOffset 和 TinyInt 型別的測試。
        /// </summary>
        [TestMethod()]
        [TestCategory("MSSQL 資料庫對於 GUID、DateTimeOffset 和 TinyInt 型別的測試。")]
        [Description("MSSQL 資料庫對於 GUID、DateTimeOffset 和 TinyInt 型別的測試。")]
        public async Task MSSql_Guid_DateTimeOffset_CRUD_Test()
        {
            _ = await Task.FromResult(0);

            // var model = new ProductModel()
            // {
            //     ProductId   = Guid.NewGuid(),
            //     ProductName = "MacBook Pro 2017 13'",
            //     ProductType = (byte)ProductType.Computer,
            //     BuildTime   = new DateTimeOffset( new DateTime( 2018, 12, 24 ), TimeSpan.FromHours( 0 ) )
            // };

            // var dao = new ProductDao();

            // var c = await dao.InsertProductAsync( model );
            // Assert.IsTrue( c.Success );

            // var r = await dao.GetProductAsync( model.ProductId );
            // Assert.IsTrue( r.Success );
            // Assert.IsNotNull( r.Data );
            // Assert.AreEqual( model.ProductId, r.Data.ProductId );
            // Assert.AreEqual( model.ProductName, r.Data.ProductName );
            // Assert.AreEqual( model.ProductType, r.Data.ProductType );
            // Assert.AreEqual( ProductType.Computer, r.Data.GetProductType() );
            // Assert.AreEqual( model.BuildTime, r.Data.BuildTime );

            // model.ProductName = "Happy Bear";
            // model.ProductType = (byte)ProductType.Toy;
            // model.BuildTime   = new DateTimeOffset( new DateTime( 2020, 12, 24 ), TimeSpan.FromHours( 0 ) );
            // var u = await dao.UpdateProductAsync( model );
            // Assert.IsTrue( u.Success );

            // r = await dao.GetProductAsync( model.ProductId );
            // Assert.IsTrue( r.Success );
            // Assert.IsNotNull( r.Data );
            // Assert.AreEqual( model.ProductId, r.Data.ProductId );
            // Assert.AreEqual( model.ProductName, r.Data.ProductName );
            // Assert.AreEqual( model.ProductType, r.Data.ProductType );
            // Assert.AreEqual( ProductType.Toy, r.Data.GetProductType() );
            // Assert.AreEqual( model.BuildTime, r.Data.BuildTime );

            // var d = await dao.DeleteProductAsync( model );
            // Assert.IsTrue( d.Success );

            // #if DEBUG
            // await Task.Delay( 1000 * 1 );
            // #endif
        }

        /// <summary>ExecuteNonQuery 新增方法測試
        /// </summary>
        [TestMethod()]
        [TestCategory("DataAccess.BaseDataAccess - 執行SQL指令測試")]
        [Description("ExecuteNonQuery新增方法測試")]
        public async Task Insert_ExecuteNonQueryAsync_Test()
        {
            var model = new UserModel()
            {
                Account = RandomTextHelper.Create(15),
                Name = "Test_001",
                Age = 27,
                Sex = 1,
                DOB = new DateTime(1986, 12, 23)
            };

            var dao = new UserDao();

            var r = await dao.Insert_ExecuteNonQueryAsync(model);
            Assert.IsTrue(r.Success);

#if DEBUG
            await Task.Delay(1000 * 1);
#endif
        }

        /// <summary>ExecuteNonQuery 新增方法測試
        /// </summary>
        [TestMethod()]
        [TestCategory("DataAccess.BaseDataAccess - 執行SQL指令測試")]
        [Description("ExecuteNonQuery新增方法測試")]
        public void Insert_ExecuteNonQuery_Test()
        {
            var model = new UserModel()
            {
                Account = RandomTextHelper.Create(15),
                Name = "Test_001",
                Age = 27,
                Sex = 1,
                DOB = new DateTime(1986, 12, 23)
            };

            var dao = new UserDao();

            var r = dao.Insert_ExecuteNonQuery(model);
            Assert.IsTrue(r.Success);

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, TimeSpan.FromSeconds(1));
#endif
        }

        /// <summary>ExecuteNonQuery 更新方法測試
        /// </summary>
        [TestMethod()]
        [TestCategory("DataAccess.BaseDataAccess - 執行SQL指令測試")]
        [Description("ExecuteNonQuery更新方法測試")]
        public async Task Update_ExecuteNonQueryAsync_Test()
        {
            var model = new UserModel()
            {
                Account = RandomTextHelper.Create(15),
                Name = "Test_003",
                Age = 35,
                Sex = 0,
                DOB = new DateTime(1986, 4, 13)
            };

            var dao = new UserDao();

            var r = await dao.Insert_ExecuteNonQueryAsync(model);
            Assert.IsTrue(r.Success);

            model.Age = 20;
            model.DOB = new DateTime(2003, 6, 22);

            var d = await dao.Update_ExecuteNonQueryAsync(model);
            Assert.IsTrue(d.Success);

#if DEBUG
            await Task.Delay(1000 * 1);
#endif
        }

        /// <summary>ExecuteNonQuery 更新方法測試
        /// </summary>
        [TestMethod()]
        [TestCategory("DataAccess.BaseDataAccess - 執行SQL指令測試")]
        [Description("ExecuteNonQuery更新方法測試")]
        public void Update_ExecuteNonQuery_Test()
        {
            var model = new UserModel()
            {
                Account = RandomTextHelper.Create(15),
                Name = "Test_003",
                Age = 35,
                Sex = 0,
                DOB = new DateTime(1986, 4, 13)
            };

            var dao = new UserDao();

            var r = dao.Insert_ExecuteNonQuery(model);
            Assert.IsTrue(r.Success);

            model.Age = 20;
            model.DOB = new DateTime(2003, 6, 22);

            var d = dao.Update_ExecuteNonQuery(model);
            Assert.IsTrue(d.Success);

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, TimeSpan.FromSeconds(1));
#endif
        }

        /// <summary>ExecuteNonQuery刪除方法測試
        /// </summary>
        [TestMethod()]
        [TestCategory("DataAccess.BaseDataAccess - 執行SQL指令測試")]
        [Description("ExecuteNonQuery刪除方法測試")]
        public async Task Delete_ExecuteNonQueryAsync_Test()
        {
            var model = new UserModel()
            {
                Account = RandomTextHelper.Create(15),
                Name = "Test_004",
                Age = 11,
                Sex = 0,
                DOB = new DateTime(2006, 2, 11)
            };

            var dao = new UserDao();

            var r = await dao.Insert_ExecuteNonQueryAsync(model);
            Assert.IsTrue(r.Success);

            var d = await dao.Delete_ExecuteNonQueryAsync(model);
            Assert.IsTrue(d.Success);

#if DEBUG
            await Task.Delay(1000 * 1);
#endif
        }

        /// <summary>ExecuteNonQuery刪除方法測試
        /// </summary>
        [TestMethod()]
        [TestCategory("DataAccess.BaseDataAccess - 執行SQL指令測試")]
        [Description("ExecuteNonQuery刪除方法測試")]
        public void Delete_ExecuteNonQuery_Test()
        {
            var model = new UserModel()
            {
                Account = RandomTextHelper.Create(15),
                Name = "Test_004",
                Age = 11,
                Sex = 0,
                DOB = new DateTime(2006, 2, 11)
            };

            var dao = new UserDao();

            var r = dao.Insert_ExecuteNonQuery(model);
            Assert.IsTrue(r.Success);

            var d = dao.Delete_ExecuteNonQuery(model);
            Assert.IsTrue(d.Success);

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, TimeSpan.FromSeconds(1));
#endif
        }

        /// <summary>LoadDataToModel 查詢方法測試
        /// </summary>
        [TestMethod()]
        [TestCategory("DataAccess.BaseDataAccess - 執行SQL指令測試")]
        [Description("LoadDataToModel查詢方法測試")]
        public async Task Select_LoadDataToModelAsync_Test()
        {
            #region Arrange

            var dao = new UserDao();

            var model1 = new UserModel()
            {
                Account = RandomTextHelper.Create(15),
                Name = "Test_005",
                Age = 11,
                Sex = 1,
                DOB = new DateTime(2006, 2, 11)
            };

            var f1 = await dao.Insert_ExecuteNonQueryAsync(model1);

            var model2 = new UserModel()
            {
                Account = RandomTextHelper.Create(15),
                Name = "Test_005",
                Age = 11,
                Sex = 1,
                DOB = new DateTime(2006, 9, 17)
            };

            var f2 = await dao.Insert_ExecuteNonQueryAsync(model2);

            var model3 = new UserModel()
            {
                Account = RandomTextHelper.Create(15),
                Name = "Test_005",
                Age = 11,
                Sex = 1,
                DOB = new DateTime(2006, 6, 23)
            };

            var f3 = await dao.Insert_ExecuteNonQueryAsync(model3);

            if (new IResult[] { f1, f2, f3 }.Any(f => !f.Success))
            {
                throw new Exception($"Make fake data fail.");
            }

            #endregion Arrange

            #region Act & Assert

            var query = new UserModel() { Sex = 1 };
            var r = await dao.Select_LoadDataToModelAsync(query);
            Assert.IsTrue(r.Success);

            var models = r.Data;
            Assert.IsTrue(models.IsNotNullOrEmpty());

            #endregion Act & Assert

#if DEBUG
            await Task.Delay(1000 * 1);
#endif
        }

        /// <summary>LoadDataToModel 查詢方法測試
        /// </summary>
        [TestMethod()]
        [TestCategory("DataAccess.BaseDataAccess - 執行SQL指令測試")]
        [Description("LoadDataToModel查詢方法測試")]
        public void Select_LoadDataToModel_Test()
        {
            #region Arrange

            var dao = new UserDao();

            var model1 = new UserModel()
            {
                Account = RandomTextHelper.Create(15),
                Name = "Test_005",
                Age = 11,
                Sex = 1,
                DOB = new DateTime(2006, 2, 11)
            };

            var f1 = dao.Insert_ExecuteNonQuery(model1);

            var model2 = new UserModel()
            {
                Account = RandomTextHelper.Create(15),
                Name = "Test_005",
                Age = 11,
                Sex = 1,
                DOB = new DateTime(2006, 9, 17)
            };

            var f2 = dao.Insert_ExecuteNonQuery(model2);

            var model3 = new UserModel()
            {
                Account = RandomTextHelper.Create(15),
                Name = "Test_005",
                Age = 11,
                Sex = 1,
                DOB = new DateTime(2006, 6, 23)
            };

            var f3 = dao.Insert_ExecuteNonQuery(model3);

            if (new IResult[] { f1, f2, f3 }.Any(f => !f.Success))
            {
                throw new Exception($"Make fake data fail.");
            }

            #endregion Arrange

            #region Act & Assert

            var query = new UserModel() { Sex = 1 };
            var r = dao.Select_LoadDataToModel(query);
            Assert.IsTrue(r.Success);

            var models = r.Data;
            Assert.IsTrue(models.IsNotNullOrEmpty());

            #endregion Act & Assert

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, TimeSpan.FromSeconds(1));
#endif
        }

        /// <summary>LoadDataToModels 查詢方法測試
        /// </summary>
        [TestMethod()]
        [TestCategory("DataAccess.BaseDataAccess - 執行SQL指令測試")]
        [Description("LoadDataToModels 查詢方法測試")]
        public async Task Select_LoadDataToModelsAsync_Test()
        {
            #region Arrange

            var dao = new UserDao();

            var model1 = new UserModel()
            {
                Account = RandomTextHelper.Create(15),
                Name = "Test_006_1",
                Age = 11,
                Sex = 1,
                IsVip = true,
                DOB = new DateTime(2006, 2, 11)
            };

            var f1 = await dao.Insert_ExecuteNonQueryAsync(model1);

            var model2 = new UserModel()
            {
                Account = RandomTextHelper.Create(15),
                Name = "Test_006",
                Age = 12,
                Sex = 1,
                IsVip = true,
                DOB = new DateTime(2006, 9, 17)
            };

            var f2 = await dao.Insert_ExecuteNonQueryAsync(model2);

            var model3 = new UserModel()
            {
                Account = RandomTextHelper.Create(15),
                Name = "Test_006",
                Age = 13,
                Sex = 1,
                IsVip = true,
                DOB = new DateTime(2006, 6, 23)
            };

            var f3 = await dao.Insert_ExecuteNonQueryAsync(model3);

            if (new IResult[] { f1, f2, f3 }.Any(f => !f.Success))
            {
                throw new Exception($"Make fake data fail.");
            }

            #endregion Arrange

            #region Act & Assert

            var query = new UserModel() { Sex = 1 };
            dynamic r = await dao.Select_LoadDataToModelsAsync(query);

            var json1 = (string)NewtonsoftJsonConvert.Serialize(r.Rst1) + "";
            var json2 = (string)NewtonsoftJsonConvert.Serialize(r.Rst2) + "";

            await Console.Out.WriteLineAsync($"Query Result1:");
            await Console.Out.WriteLineAsync(json1);
            await Console.Out.WriteLineAsync();
            await Console.Out.WriteLineAsync();
            await Console.Out.WriteLineAsync($"Query Result2:");
            await Console.Out.WriteLineAsync(json2);

            Debug.Print($"Query Result1:");
            Debug.Print(json1);
            Debug.Print($"{Environment.NewLine}{Environment.NewLine}");
            Debug.Print(json2);
            Assert.IsTrue(r.Success);

            List<UserModel> models = r.Rst1;
            Assert.IsTrue(models.IsNotNullOrEmpty());

            List<UserModel> models2 = r.Rst2;
            Assert.IsTrue(models2.IsNotNullOrEmpty());

            #endregion Act & Assert

#if DEBUG
            await Task.Delay(1000 * 1);
#endif
        }

        /// <summary>LoadDataToModels 查詢方法測試
        /// </summary>
        [TestMethod()]
        [TestCategory("DataAccess.BaseDataAccess - 執行SQL指令測試")]
        [Description("LoadDataToModels 查詢方法測試")]
        public void Select_LoadDataToModels_Test()
        {
            #region Arrange

            var dao = new UserDao();

            var model1 = new UserModel()
            {
                Account = RandomTextHelper.Create(15),
                Name = "Test_016",
                Age = 20,
                Sex = 1,
                DOB = new DateTime(2006, 2, 11)
            };

            var f1 = dao.Insert_ExecuteNonQuery(model1);

            var model2 = new UserModel()
            {
                Account = RandomTextHelper.Create(15),
                Name = "Test_016",
                Age = 21,
                Sex = 1,
                DOB = new DateTime(2006, 9, 17)
            };

            var f2 = dao.Insert_ExecuteNonQuery(model2);

            var model3 = new UserModel()
            {
                Account = RandomTextHelper.Create(15),
                Name = "Test_016",
                Age = 22,
                Sex = 1,
                DOB = new DateTime(2006, 6, 23)
            };

            var f3 = dao.Insert_ExecuteNonQuery(model3);

            if (new IResult[] { f1, f2, f3 }.Any(f => !f.Success))
            {
                throw new Exception($"Make fake data fail.");
            }

            #endregion Arrange

            #region Act & Assert

            var query = new UserModel() { Sex = 1 };
            dynamic r = dao.Select_LoadDataToModels(query);
            Assert.IsTrue(r.Success);

            List<UserModel> models = r.Rst1;
            Assert.IsTrue(models.IsNotNullOrEmpty());

            List<UserModel> models2 = r.Rst2;
            Assert.IsTrue(models2.IsNotNullOrEmpty());

            #endregion Act & Assert

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, TimeSpan.FromSeconds(1));
#endif
        }

        /// <summary>LoadDataToDynaicModel 查詢方法測試
        /// </summary>
        [TestMethod()]
        [TestCategory("DataAccess.BaseDataAccess - 執行SQL指令測試")]
        [Description("LoadDataToDynamicModel查詢方法測試")]
        public async Task Select_LoadDataToDynamicModelAsync_Test()
        {
            #region Arrange

            var model = new UserModel()
            {
                Account = RandomTextHelper.Create(15),
                Name = "Test_007",
                Age = 30,
                Sex = 1,
                DOB = new DateTime(1994, 12, 23, 0, 0, 0, DateTimeKind.Utc)
            };

            var dao = new UserDao();
            var j = await dao.Insert_ExecuteNonQueryAsync(model);

            if (!j.Success)
            {
                throw new Exception($"Make fake data fail.");
            }

            #endregion Arrange

            #region Act & Assert

            var query = new UserModel() { Sex = 1 };
            var r = await dao.Select_LoadDataToDynamicModelAsync(query);
            Assert.IsTrue(r.Success);

            var models = r.Data;
            Assert.IsTrue(models.IsNotNullOrEmpty());

            // Postgres 回傳的 DateTime 的結果:
            // dynamic userModel = models.Where( m => m.Account == model.Account && new DateTime( m.DoB.Ticks ).ToUniversalTime() == model.DOB )?.FirstOrDefault();
            // MySQL 和 MSSQL 回傳 DateTime 的結果:
            // dynamic userModel = models.Where( m => m.Account == model.Account && m.DoB == model.DOB )?.FirstOrDefault();

            dynamic userModel = models.Where(
                m => m.Account == model.Account &&
                new DateTime(m.DoB.Ticks).Year == model.DOB?.Year &&
                new DateTime(m.DoB.Ticks).Month == model.DOB?.Month &&
                new DateTime(m.DoB.Ticks).Day == model.DOB?.Day)?.FirstOrDefault();

            Assert.IsNotNull(userModel);

            #endregion Act & Assert

#if DEBUG
            await Task.Delay(1000 * 1);
#endif
        }

        /// <summary>LoadDataToDynaicModel 查詢方法測試
        /// </summary>
        [TestMethod()]
        [TestCategory("DataAccess.BaseDataAccess - 執行SQL指令測試")]
        [Description("LoadDataToDynamicModel查詢方法測試")]
        public void Select_LoadDataToDynamicModel_Test()
        {
            #region Arrange

            var model = new UserModel()
            {
                Account = RandomTextHelper.Create(15),
                Name = "Test_007",
                Age = 30,
                Sex = 1,
                DOB = new DateTime(1994, 12, 23, 0, 0, 0, DateTimeKind.Utc)
            };

            var dao = new UserDao();
            var j = dao.Insert_ExecuteNonQuery(model);


            if (!j.Success)
            {
                throw new Exception($"Make fake data fail.");
            }

            #endregion Arrange

            #region Act & Assert

            var query = new UserModel() { Sex = 1 };
            var r = dao.Select_LoadDataToDynamicModel(query);
            Assert.IsTrue(r.Success);

            var models = r.Data;
            Assert.IsTrue(models.IsNotNullOrEmpty());

            // Postgres 回傳的 DateTime 的結果:
            // dynamic userModel = models.Where( m => m.Account == model.Account && new DateTime( m.DoB.Ticks ).ToUniversalTime() == model.DOB ).FirstOrDefault();
            // MySQL 和 MSSQL 回傳 DateTime 的結果:
            // dynamic userModel = models.Where( m => m.Account == model.Account && m.DoB == model.DOB ).FirstOrDefault();

            dynamic userModel = models.Where(
                m => m.Account == model.Account &&
                new DateTime(m.DoB.Ticks).Year == model.DOB?.Year &&
                new DateTime(m.DoB.Ticks).Month == model.DOB?.Month &&
                new DateTime(m.DoB.Ticks).Day == model.DOB?.Day).FirstOrDefault();

            Assert.IsNotNull(userModel);

            #endregion Act & Assert

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, TimeSpan.FromSeconds(1));
#endif
        }

        /// <summary>LoadDataToDynamicCollection 查詢方法測試
        /// </summary>
        [TestMethod()]
        [TestCategory("DataAccess.BaseDataAccess - 執行SQL指令測試")]
        [Description("LoadDataToDynamicCollection 查詢方法測試")]
        public async Task Select_LoadDataToDynamicCollectionAsync_Test()
        {
            #region Arrange

            var dao = new UserDao();

            var model = new UserModel()
            {
                Account = RandomTextHelper.Create(15),
                Name = "Test_008",
                Age = 21,
                Sex = 0,
                DOB = new DateTime(1997, 12, 23)
            };

            var j = await dao.Insert_ExecuteNonQueryAsync(model);

            if (!j.Success)
            {
                throw new Exception($"Make fake data fail.");
            }

            #endregion Arrange

            #region Act & Assert

            var query = new UserModel() { Sex = 0 };
            var r = await dao.Select_LoadDataToDynamicCollectionAsync(query);
            Assert.IsTrue(r.Success);

            #endregion Act & Assert

            #region Assert

            var collect = r.Data;
            Assert.IsTrue(collect.IsNotNullOrEmpty());
            Assert.IsTrue(collect.ContainsKey("QueryNo1"));
            Assert.IsTrue(collect.ContainsKey("QueryNo2"));

            var models1 = collect["QueryNo1"];
            Assert.IsTrue(models1.IsNotNullOrEmpty());

            var models2 = collect["QueryNo2"];
            Assert.IsTrue(models2.IsNotNullOrEmpty());

            #endregion Assert

#if DEBUG
            await Task.Delay(1000 * 1);
#endif
        }

        /// <summary>LoadDataToDynamicCollection 查詢方法測試
        /// </summary>
        [TestMethod()]
        [TestCategory("DataAccess.BaseDataAccess - 執行SQL指令測試")]
        [Description("LoadDataToDynamicCollection 查詢方法測試")]
        public void Select_LoadDataToDynamicCollection_Test()
        {
            #region Arrange

            var dao = new UserDao();

            var model = new UserModel()
            {
                Account = RandomTextHelper.Create(15),
                Name = "Test_008",
                Age = 21,
                Sex = 0,
                DOB = new DateTime(1997, 12, 23)
            };

            var j = dao.Insert_ExecuteNonQuery(model);

            if (!j.Success)
            {
                throw new Exception($"Make fake data fail.");
            }

            #endregion Arrange

            #region Act & Assert

            var query = new UserModel() { Sex = 0 };
            var r = dao.Select_LoadDataToDynamicCollection(query);
            Assert.IsTrue(r.Success);

            #endregion Act & Assert

            #region Assert

            var collect = r.Data;
            Assert.IsTrue(collect.IsNotNullOrEmpty());
            Assert.IsTrue(collect.ContainsKey("QueryNo1"));
            Assert.IsTrue(collect.ContainsKey("QueryNo2"));

            var models1 = collect["QueryNo1"];
            Assert.IsTrue(models1.IsNotNullOrEmpty());

            var models2 = collect["QueryNo2"];
            Assert.IsTrue(models2.IsNotNullOrEmpty());

            #endregion Assert

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, TimeSpan.FromSeconds(1));
#endif
        }

        /// <summary>ExecuteScalar查詢純量方法測試
        /// </summary>
        [TestMethod()]
        [TestCategory("DataAccess.BaseDataAccess - 執行SQL指令測試")]
        [Description("ExecuteScalar查詢純量方法測試")]
        public async Task Select_ExecuteScalarAsync_Test()
        {
            #region Arrange

            var dao = new UserDao();

            var model1 = new UserModel()
            {
                Account = RandomTextHelper.Create(15),
                Name = "Test_009",
                Age = 24,
                Sex = 0,
                DOB = new DateTime(2006, 2, 11)
            };

            var f1 = await dao.Insert_ExecuteNonQueryAsync(model1);

            var model2 = new UserModel()
            {
                Account = RandomTextHelper.Create(15),
                Name = "Test_009",
                Age = 18,
                Sex = 0,
                DOB = new DateTime(2006, 9, 17)
            };

            var f2 = await dao.Insert_ExecuteNonQueryAsync(model2);

            var model3 = new UserModel()
            {
                Account = RandomTextHelper.Create(15),
                Name = "Test_009",
                Age = 11,
                Sex = 1,
                DOB = new DateTime(2006, 6, 23)
            };

            var f3 = await dao.Insert_ExecuteNonQueryAsync(model3);

            if (new IResult[] { f1, f2, f3 }.Any(f => !f.Success))
            {
                throw new Exception($"Make fake data fail.");
            }

            #endregion Arrange

            #region Act & Assert

            var r = dao.SelectCount_ExecuteScalar();
            Assert.IsTrue(r.Success);

            var count = r.Data;
            Assert.IsTrue(count > 0);

            #endregion Act & Assert

#if DEBUG
            await Task.Delay(1000 * 1);
#endif
        }

        /// <summary>ExecuteScalar查詢純量方法測試
        /// </summary>
        [TestMethod()]
        [TestCategory("DataAccess.BaseDataAccess - 執行SQL指令測試")]
        [Description("ExecuteScalar查詢純量方法測試")]
        public void Select_ExecuteScalar_Test()
        {
            #region Arrange

            var dao = new UserDao();

            var model1 = new UserModel()
            {
                Account = RandomTextHelper.Create(15),
                Name = "Test_009",
                Age = 24,
                Sex = 0,
                DOB = new DateTime(2006, 2, 11)
            };

            var f1 = dao.Insert_ExecuteNonQuery(model1);

            var model2 = new UserModel()
            {
                Account = RandomTextHelper.Create(15),
                Name = "Test_009",
                Age = 18,
                Sex = 0,
                DOB = new DateTime(2006, 9, 17)
            };

            var f2 = dao.Insert_ExecuteNonQuery(model2);

            var model3 = new UserModel()
            {
                Account = RandomTextHelper.Create(15),
                Name = "Test_009",
                Age = 11,
                Sex = 1,
                DOB = new DateTime(2006, 6, 23)
            };

            var f3 = dao.Insert_ExecuteNonQuery(model3);

            if (new IResult[] { f1, f2, f3 }.Any(f => !f.Success))
            {
                throw new Exception($"Make fake data fail.");
            }

            #endregion Arrange

            #region Act & Assert

            var r = dao.SelectCount_ExecuteScalar();
            Assert.IsTrue(r.Success);

            var count = r.Data;
            Assert.IsTrue(count > 0);

            #endregion Act & Assert

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, TimeSpan.FromSeconds(1));
#endif
        }

        #endregion 宣告測試方法
    }
}
