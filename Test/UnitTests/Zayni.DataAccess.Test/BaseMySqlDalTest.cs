﻿using DataAccess.Test.TestDao;
using DataAccess.Test.TestData;
using DataAccess.Test.TestModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using ZayniFramework.Common;


namespace DataAccess.Test
{
    /// <summary>BaseMySqlDal 的測試類別
    /// </summary>
    [TestClass()]
    public class BaseMySqlDalTest
    {
        #region 宣告私有的欄位

        /// <summary>測試資料建立者
        /// </summary>
        private static readonly TestDataMaker _dataMaker = new();

        #endregion 宣告私有的欄位


        #region 宣告測試初始化事件

        /// <summary>初始化測試資料
        /// </summary>
        /// <param name="context">測試環境上下文</param>
        [ClassInitialize()]
        public static void Initialize(TestContext context) => _dataMaker.Initialize();

        /// <summary>清空測試資料
        /// </summary>
        [ClassCleanup()]
        public static void CleanupClass()
        {
            var dao = new UserDao();
            dao.Clear();
        }

        #endregion 宣告測試事件


        #region 宣告測試方法

        /// <summary>ExecuteNonQuery新增方法測試
        /// </summary>
        [TestMethod()]
        [TestCategory("DataAccess.BaseMySqlDal - 執行SQL指令測試")]
        [Description("ExecuteNonQuery新增方法測試")]
        public void Insert_ExecuteNonQuery_Test()
        {
            var model = new UserModel()
            {
                Account = RandomTextHelper.Create(10),
                Name = "楊謹華",
                Age = 37,
                Sex = 0,
                DOB = new DateTime(1976, 4, 25),
                IsVip = true
            };

            var dal = new UserMySqlDal();

            var r = dal.Insert_ExecuteNonQuery(model);
            Assert.IsTrue(r.Success);

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, TimeSpan.FromSeconds(1));
#endif
        }

        /// <summary>LoadData 查詢方法測試
        /// </summary>
        [TestMethod()]
        [TestCategory("DataAccess.BaseMySqlDal - 執行SQL指令測試")]
        [Description("LoadData查詢方法測試")]
        public void Select_LoadData_Test()
        {
            var query = new UserModel() { Sex = 1 };
            var dal = new UserMySqlDal();

            var r = dal.Select_LoadData(query);
            Assert.IsTrue(r.Success);

            var models = r.Data;
            Assert.IsTrue(models.IsNotNullOrEmpty());

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, TimeSpan.FromSeconds(1));
#endif
        }

        /// <summary>LoadMasterDetail 查詢方法測試
        /// </summary>
        [TestMethod()]
        [TestCategory("DataAccess.BaseMySqlDal - 執行SQL指令測試")]
        [Description("LoadMasterDetail查詢方法測試")]
        public void Select_LoadMasterDetail_Test()
        {
            var query = new UserModel() { Account = "Test001" };
            var dal = new UserMySqlDal();

            var r = dal.Select_MasterDetail(query);
            Assert.IsTrue(r.Success);
            Assert.IsNotNull(r.Data);

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, TimeSpan.FromSeconds(1));
#endif
        }

        /// <summary>LoadDataDynamic 查詢方法測試
        /// </summary>
        [TestMethod()]
        [TestCategory("DataAccess.BaseMySqlDal - 執行SQL指令測試")]
        [Description("LoadDataDynamic查詢方法測試")]
        public void Select_LoadDataDynamic_Test()
        {
            var query = new UserModel() { Account = "Test001" };
            var dal = new UserMySqlDal();

            var r = dal.Select_LoadDataDynamic(query);
            Assert.IsTrue(r.Success);
            Assert.IsNotNull(r.Data);

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, TimeSpan.FromSeconds(1));
#endif
        }

        /// <summary>LoadDataSet 查詢方法測試
        /// </summary>
        [TestMethod()]
        [TestCategory("DataAccess.BaseMySqlDal - 執行SQL指令測試")]
        [Description("LoadDataSet查詢方法測試")]
        public void Select_LoadDataSet_Test()
        {
            var query = new UserModel() { Account = "Test001" };
            var dal = new UserMySqlDal();

            var r = dal.Select_LoadDataSet(query);
            Assert.IsTrue(r.Success);
            Assert.IsNotNull(r.Data);

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, TimeSpan.FromSeconds(1));
#endif
        }

        /// <summary>ExecuteScalar查詢純量方法測試
        /// </summary>
        [TestMethod()]
        [TestCategory("DataAccess.BaseMySqlDal - 執行SQL指令測試")]
        [Description("ExecuteScalar查詢純量方法測試")]
        public void Select_ExecuteScalar_Test()
        {
            var dal = new UserMySqlDal();

            var r = dal.SelectCount_ExecuteScalar();
            Assert.IsTrue(r.Success);

            var count = r.Data;
            Assert.IsTrue(count > 0);

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, TimeSpan.FromSeconds(1));
#endif
        }

        #endregion 宣告測試方法
    }
}
