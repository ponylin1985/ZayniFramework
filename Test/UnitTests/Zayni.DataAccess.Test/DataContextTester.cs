﻿using DataAccess.Test.TestData;
using DataAccess.Test.TestModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.DataAccess;
using ZayniFramework.DataAccess.Lightweight;


namespace DataAccess.Test
{
    // dotnet test ./Test/UnitTests/Zayni.DataAccess.Test/Zayni.DataAccess.Test.csproj --filter ClassName=DataAccess.Test.DataContextTester
    /// <summary>Lightweight.ORM DataContext 的測試類別
    /// </summary>
    [TestClass()]
    public class DataContextTester
    {
        #region 宣告測試初始化事件

        /// <summary>測試方法初始化
        /// </summary>
        [TestInitialize()]
        public async Task TestInitializeAsync()
        {
            // var dcUser = new UserDataContext();
            // dcUser.Clear();

            // var dcUser2 = new User2DataContext();
            // dcUser2.Clear();

            await TestDataMaker.ClearTestDataAsync();
        }

        /// <summary>重置測試資料
        /// </summary>
        [TestCleanup()]
        public async Task TestCleanupAsync()
        {
            // var dcUser = new UserDataContext();
            // dcUser.Clear();

            // var dcUser2 = new User2DataContext();
            // dcUser2.Clear();

            await TestDataMaker.ClearTestDataAsync();
        }

        #endregion 宣告測試事件


        #region 宣告測試方法

        /// <summary>單筆單檔資料 Insert 測試
        /// </summary>
        [TestMethod()]
        [Description("單筆單檔資料 Insert 測試")]
        public async Task SingleTable_DataContext_InsertAsync_Test()
        {
            #region Arrange

            var account = RandomTextHelper.Create(15);

            var model = new UserModel()
            {
                Account = account,
                Name = "DataContext_Test_001",
                Age = 22,
                DOB = new DateTime(1993, 2, 17),
                Sex = 0,
                IsVip = true
            };

            #endregion Arrange

            #region Act & Assert (使用框架內部自動開啟連線與交易)

            var dataContext = new UserDataContext();
            var c = await dataContext.InsertAsync(model);
            Assert.IsTrue(c.Success);

            #endregion Act & Assert (使用框架內部自動開啟連線與交易)

            #region Act & Assert (自行建立連線和開啟資料庫交易)

            model.Account = RandomTextHelper.Create(15);

            using var conn = await dataContext.CreateConnectionAsync();
            using var trans = conn.BeginTransaction();
            var j = await dataContext.InsertAsync(model, conn, trans);
            Assert.IsTrue(j.Success);

            var wheres = new DbParameterEntry[]
            {
                    new() { ColumnName = "ACCOUNT_ID", Name = "Account",  DbType = DbColumnType.String, Value = model.Account },
                    new() { ColumnName = "NAME",       Name = "UserName", DbType = DbColumnType.String, Value = model.Name }
            };

            model.Age = 18;
            model.Name = "Judy";
            model.DOB = new DateTime(2001, 5, 17);
            model.IsVip = false;

            var u = await dataContext.UpdateAsync(model, wheres: wheres, ignoreColumns: ["IS_VIP"], connection: conn, transaction: trans);
            Assert.IsTrue(u.Success);

            var commit = dataContext.Commit(trans, out var errorMsg);
            Assert.IsTrue(commit);

            await conn.CloseAsync();

            #endregion Act & Assert (自行建立連線和開啟資料庫交易)
        }

        /// <summary>單筆單檔資料 Insert 測試
        /// </summary>
        [TestMethod()]
        [Description("單筆單檔資料 Insert 測試")]
        public void SingleTable_DataContext_Insert_Test()
        {
            #region Arrange

            var account = RandomTextHelper.Create(15);

            var model = new UserModel()
            {
                Account = account,
                Name = "DataContext_Test_001",
                Age = 22,
                DOB = new DateTime(1993, 2, 17),
                Sex = 0,
                IsVip = true
            };

            #endregion Arrange

            #region Act & Assert (使用框架內部自動開啟連線與交易)

            var dataContext = new UserDataContext();
            var c = dataContext.Insert(model);
            Assert.IsTrue(c.Success);

            #endregion Act & Assert (使用框架內部自動開啟連線與交易)

            #region Act & Assert (自行建立連線和開啟資料庫交易)

            model.Account = RandomTextHelper.Create(15);

            using var conn = dataContext.CreateConnection();
            using var trans = conn.BeginTransaction();
            var j = dataContext.Insert(model, conn, trans);
            Assert.IsTrue(j.Success);

            var wheres = new DbParameterEntry[]
            {
                new() { ColumnName = "ACCOUNT_ID", Name = "Account",  DbType = DbColumnType.String, Value = model.Account },
                new() { ColumnName = "NAME",       Name = "UserName", DbType = DbColumnType.String, Value = model.Name }
            };

            model.Age = 18;
            model.Name = "Judy";
            model.DOB = new DateTime(2001, 5, 17);
            model.IsVip = false;

            var u = dataContext.Update(model, wheres: wheres, ignoreColumns: ["IS_VIP"], connection: conn, transaction: trans);
            Assert.IsTrue(u.Success);

            var commit = dataContext.Commit(trans, out var errorMsg);
            Assert.IsTrue(commit);

            #endregion Act & Assert (自行建立連線和開啟資料庫交易)
        }

        /// <summary>單檔資料 CRUD 測試
        /// </summary>
        [TestMethod()]
        [Description("單檔資料 CRUD 測試")]
        public async Task SingleTable_DataContext_CRUD_AsyncTest()
        {
            #region 準備測試資料模型

            var account = "Amber001";

            var model = new UserModel()
            {
                Account = account,
                Name = "Amber",
                Age = 22,
                DOB = new DateTime(1993, 2, 17),
                Sex = 0,
                IsVip = true
            };

            var reqArgs = new UserReqArgs()
            {
                Account = model.Account
            };

            var dataContext = new UserDataContext();

            #endregion 準備測試資料模型

            #region 查詢測試資料

            var r = await dataContext.SelectAsync(new SelectInfo<UserReqArgs>() { DataContent = reqArgs });
            Assert.IsTrue(r.Success);
            When.True(r.Data.IsNotNullOrEmpty(), async () => await dataContext.DeleteAsync(model));

            #endregion 查詢測試資料

            #region 新增測試資料

            var c = await dataContext.InsertAsync(model);
            Assert.IsTrue(c.Success);
            await Task.Delay(80);

            #endregion 新增測試資料

            #region 查詢新增成功的測試資料

            r = await dataContext.SelectAsync(new SelectInfo<UserReqArgs>() { DataContent = reqArgs });
            Assert.IsTrue(r.Success);
            Assert.IsTrue(r.Data.IsNotNullOrEmpty());
            Assert.IsNotNull(r.Data.FirstOrDefault());
            Assert.AreEqual(account, r.Data.FirstOrDefault().Account);

            #endregion 查詢新增成功的測試資料

            #region 更新測試資料 Update by Primary Key

            model.Sex = 1;
            model.Name = "Sylvia";
            model.IsVip = false;
            model.DataFlag = r.Data.FirstOrDefault().DataFlag;

            // 強制 BIRTHDAY 和 IS_VIP 欄位要忽略更新! 即便 IsVip 屬性確實有給值，還是會忽略更新!
            var u = await dataContext.UpdateAsync(model, ignoreColumns: _ignoreColumnsArray);
            Assert.IsTrue(u.Success);

            #endregion 更新測試資料 Update by Primary Key

            #region 重新查詢更新成功的測試資料

            r = await dataContext.SelectAsync(new SelectInfo<UserReqArgs>() { DataContent = reqArgs });
            Assert.IsTrue(r.Success);
            Assert.IsTrue(r.Success);
            Assert.IsTrue(r.Data.IsNotNullOrEmpty());
            Assert.IsNotNull(r.Data.FirstOrDefault());
            Assert.AreEqual(account, r.Data.FirstOrDefault().Account);
            Assert.AreEqual("Sylvia", r.Data.FirstOrDefault().Name);

            #endregion 重新查詢更新成功的測試資料

            #region 刪除測試資料

            var d = await dataContext.DeleteAsync(model);
            Assert.IsTrue(d.Success);

            #endregion 刪除測試資料

#if DEBUG
            await Task.Delay(100);
#endif
        }

        private static readonly string[] _ignoreColumns = ["BIRTHDAY", "IS_VIP"];
        private static readonly string[] _ignoreColumnsArray = ["BIRTHDAY", "IS_VIP"];

        /// <summary>單檔資料 CRUD 測試
        /// </summary>
        [TestMethod()]
        [Description("單檔資料 CRUD 測試")]
        public void SingleTable_DataContext_CRUD_Test()
        {
            #region 準備測試資料模型

            var account = "Amber001";

            var model = new UserModel()
            {
                Account = account,
                Name = "Amber",
                Age = 22,
                DOB = new DateTime(1993, 2, 17),
                Sex = 0,
                IsVip = true
            };

            var reqArgs = new UserReqArgs()
            {
                Account = model.Account
            };

            var dataContext = new UserDataContext();

            #endregion 準備測試資料模型

            #region 查詢測試資料

            var r = dataContext.Select(new SelectInfo<UserReqArgs>() { DataContent = reqArgs });
            Assert.IsTrue(r.Success);
            When.True(r.Data.IsNotNullOrEmpty(), () => dataContext.Delete(model));

            #endregion 查詢測試資料

            #region 新增測試資料

            var c = dataContext.Insert(model);
            Assert.IsTrue(c.Success);
            System.Threading.SpinWait.SpinUntil(() => false, 80);

            #endregion 新增測試資料

            #region 查詢新增成功的測試資料

            r = dataContext.Select(new SelectInfo<UserReqArgs>() { DataContent = reqArgs });
            Assert.IsTrue(r.Success);
            Assert.IsTrue(r.Data.IsNotNullOrEmpty());
            Assert.IsNotNull(r.Data.FirstOrDefault());
            Assert.AreEqual(account, r.Data.FirstOrDefault().Account);

            #endregion 查詢新增成功的測試資料

            #region 更新測試資料 Update by Primary Key

            model.Sex = 1;
            model.Name = "Sylvia";
            model.IsVip = false;
            model.DataFlag = r.Data.FirstOrDefault().DataFlag;

            // 強制 BIRTHDAY 和 IS_VIP 欄位要忽略更新! 即便 IsVip 屬性確實有給值，還是會忽略更新!
            var u = dataContext.Update(model, ignoreColumns: _ignoreColumns);
            Assert.IsTrue(u.Success);

            #endregion 更新測試資料 Update by Primary Key

            #region 重新查詢更新成功的測試資料

            r = dataContext.Select(new SelectInfo<UserReqArgs>() { DataContent = reqArgs });
            Assert.IsTrue(r.Success);
            Assert.IsTrue(r.Success);
            Assert.IsTrue(r.Data.IsNotNullOrEmpty());
            Assert.IsNotNull(r.Data.FirstOrDefault());
            Assert.AreEqual(account, r.Data.FirstOrDefault().Account);
            Assert.AreEqual("Sylvia", r.Data.FirstOrDefault().Name);

            #endregion 重新查詢更新成功的測試資料

            #region 刪除測試資料

            var d = dataContext.Delete(model);
            Assert.IsTrue(d.Success);

            #endregion 刪除測試資料

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, TimeSpan.FromMilliseconds(150));
#endif
        }

        /// <summary>單檔資料 CRUD 測試，Update by Where 的測試
        /// </summary>
        [TestMethod()]
        [Description("單檔資料 CRUD 測試，Update by Where 的測試")]
        public async Task SingleTable_DataContext_CRUD_UpdateByWhere_AsyncTest()
        {
            #region Arragne 準備測試資料模型

            var account = "Amber Bitch";

            var model = new UserModel()
            {
                Account = account,
                Name = "Slut",
                Age = 29,
                DOB = new DateTime(1993, 2, 17),
                Sex = 0,
                IsVip = true
            };

            var reqArgs = new UserReqArgs()
            {
                Account = model.Account
            };

            #endregion Arragne 準備測試資料模型

            #region Act & Assert 查詢測試資料

            var dataContext = new UserDataContext();

            var r = await dataContext.SelectAsync(new SelectInfo<UserReqArgs>() { DataContent = reqArgs });
            Assert.IsTrue(r.Success);
            When.True(r.Data.IsNotNullOrEmpty(), async () => await dataContext.DeleteAsync(model));

            #endregion Act & Assert 查詢測試資料

            #region Act & Assert 新增測試資料

            var c = await dataContext.InsertAsync(model);
            Assert.IsTrue(c.Success);
            await Task.Delay(80);

            #endregion Act & Assert 新增測試資料

            #region Act & Assert 重新查詢新增成功的測試資料

            r = await dataContext.SelectAsync(new SelectInfo<UserReqArgs>() { DataContent = reqArgs });
            Assert.IsTrue(r.Success);
            Assert.IsTrue(r.Data.IsNotNullOrEmpty());
            Assert.IsNotNull(r.Data.FirstOrDefault());
            Assert.AreEqual(account, r.Data.FirstOrDefault().Account);

            #endregion Act & Assert 重新查詢新增成功的測試資料

            #region Act & Assert 更新測試資料 Update by where 條件的測試

            var wheres = new DbParameterEntry[]
            {
                new() { ColumnName = "ACCOUNT_ID", Name = "Account",  DbType = DbColumnType.String, Value = account },
                new() { ColumnName = "NAME",       Name = "UserName", DbType = DbColumnType.String, Value = model.Name }
            };

            model.Sex = 1;
            model.Name = "Sylvia";
            model.IsVip = false;
            model.DataFlag = r.Data.FirstOrDefault().DataFlag;

            // 強制 BIRTHDAY 和 IS_VIP 欄位要忽略更新! 即便 IsVip 屬性確實有給值，還是會忽略更新!
            var u = await dataContext.UpdateAsync(model, wheres: wheres, ignoreColumns: ["BIRTHDAY", "IS_VIP"]);
            Assert.IsTrue(u.Success);

            #endregion Act & Assert 更新測試資料 Update by where 條件的測試

            #region Act & Assert 重新查詢更新成功的測試資料

            r = await dataContext.SelectAsync(new SelectInfo<UserReqArgs>() { DataContent = reqArgs });
            Assert.IsTrue(r.Success);
            Assert.IsTrue(r.Success);
            Assert.IsTrue(r.Data.IsNotNullOrEmpty());
            Assert.IsNotNull(r.Data.FirstOrDefault());
            Assert.AreEqual(account, r.Data.FirstOrDefault().Account);
            Assert.AreEqual("Sylvia", r.Data.FirstOrDefault().Name);

            #endregion Act & Assert 重新查詢更新成功的測試資料

            #region Act & Assert 刪除測試資料

            var d = await dataContext.DeleteAsync(model);
            Assert.IsTrue(d.Success);

            #endregion Act & Assert 刪除測試資料

#if DEBUG
            await Task.Delay(100);
#endif
        }

        /// <summary>單檔資料 CRUD 測試，Update by Where 的測試
        /// </summary>
        [TestMethod()]
        [Description("單檔資料 CRUD 測試，Update by Where 的測試")]
        public void SingleTable_DataContext_CRUD_UpdateByWhere_Test()
        {
            #region Arragne 準備測試資料模型

            var account = "Amber Bitch";

            var model = new UserModel()
            {
                Account = account,
                Name = "Slut",
                Age = 29,
                DOB = new DateTime(1993, 2, 17),
                Sex = 0,
                IsVip = true
            };

            var reqArgs = new UserReqArgs()
            {
                Account = model.Account
            };

            #endregion Arragne 準備測試資料模型

            #region Act & Assert 查詢測試資料

            var dataContext = new UserDataContext();

            var r = dataContext.Select(new SelectInfo<UserReqArgs>() { DataContent = reqArgs });
            Assert.IsTrue(r.Success);
            When.True(r.Data.IsNotNullOrEmpty(), () => dataContext.Delete(model));

            #endregion Act & Assert 查詢測試資料

            #region Act & Assert 新增測試資料

            var c = dataContext.Insert(model);
            Assert.IsTrue(c.Success);
            System.Threading.SpinWait.SpinUntil(() => false, 80);

            #endregion Act & Assert 新增測試資料

            #region Act & Assert 重新查詢新增成功的測試資料

            r = dataContext.Select(new SelectInfo<UserReqArgs>() { DataContent = reqArgs });
            Assert.IsTrue(r.Success);
            Assert.IsTrue(r.Data.IsNotNullOrEmpty());
            Assert.IsNotNull(r.Data.FirstOrDefault());
            Assert.AreEqual(account, r.Data.FirstOrDefault().Account);

            #endregion Act & Assert 重新查詢新增成功的測試資料

            #region Act & Assert 更新測試資料 Update by where 條件的測試

            var wheres = new DbParameterEntry[]
            {
                new() { ColumnName = "ACCOUNT_ID", Name = "Account",  DbType = DbColumnType.String, Value = account },
                new() { ColumnName = "NAME",       Name = "UserName", DbType = DbColumnType.String, Value = model.Name },
            };

            model.Sex = 1;
            model.Name = "Sylvia";
            model.IsVip = false;
            model.DataFlag = r.Data.FirstOrDefault().DataFlag;

            // 強制 BIRTHDAY 和 IS_VIP 欄位要忽略更新! 即便 IsVip 屬性確實有給值，還是會忽略更新!
            var u = dataContext.Update(model, wheres: wheres, ignoreColumns: _ignoreColumns);
            Assert.IsTrue(u.Success);

            #endregion Act & Assert 更新測試資料 Update by where 條件的測試

            #region Act & Assert 重新查詢更新成功的測試資料

            r = dataContext.Select(new SelectInfo<UserReqArgs>() { DataContent = reqArgs });
            Assert.IsTrue(r.Success);
            Assert.IsTrue(r.Success);
            Assert.IsTrue(r.Data.IsNotNullOrEmpty());
            Assert.IsNotNull(r.Data.FirstOrDefault());
            Assert.AreEqual(account, r.Data.FirstOrDefault().Account);
            Assert.AreEqual("Sylvia", r.Data.FirstOrDefault().Name);

            #endregion Act & Assert 重新查詢更新成功的測試資料

            #region Act & Assert 刪除測試資料

            var d = dataContext.Delete(model);
            Assert.IsTrue(d.Success);

            #endregion Act & Assert 刪除測試資料

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, TimeSpan.FromMilliseconds(150));
#endif
        }

        /// <summary>單檔資料 CRUD 與獨立資料庫連線測試<para/>
        /// 1. 此為測試單檔資料 CRUD。<para/>
        /// 2. Zayni Frameowrk 外部會使用自己獨立的 DbConnection 和 DbTransaction 的測試。
        /// </summary>
        [TestMethod()]
        [Description("單檔資料 CRUD 與獨立資料庫連線測試")]
        public async Task SingleTable_DataContext_CRUD_Connection_Transaction_AsyncTest()
        {
            var account = RandomTextHelper.Create(10);

            var model = new UserModel()
            {
                Account = account,
                Name = "Nancy",
                Age = 27,
                DOB = new DateTime(1993, 2, 17),
                Sex = 0,
                IsVip = true
            };

            var reqArgs = new UserReqArgs()
            {
                Account = model.Account
            };

            var dataContext = new UserDataContext();
            var r = await dataContext.SelectAsync(new SelectInfo<UserReqArgs>() { DataContent = reqArgs });
            Assert.IsTrue(r.Success);

            if (r.Data.IsNotNullOrEmpty())
            {
                await dataContext.DeleteAsync(model);
            }

            // ========================

            var conn = await dataContext.CreateConnectionAsync();
            var trans = dataContext.BeginTransaction(conn);

            // ========================

            var c = await dataContext.InsertAsync(model, conn, trans);
            Assert.IsTrue(c.Success);
            await Task.Delay(80);

            // ========================

            // 看來連 Select 也需要可以吃 DbConnection 和 DbTransaction 的參數了... 否則也是過不了...
            r = await dataContext.SelectAsync(new SelectInfo<UserReqArgs>() { DataContent = reqArgs }, conn, trans);
            Assert.IsTrue(r.Success);
            Assert.IsTrue(r.Data.IsNotNullOrEmpty());
            Assert.IsNotNull(r.Data.FirstOrDefault());
            Assert.AreEqual(account, r.Data.FirstOrDefault().Account);

            model.Name = "Kelly Wells";
            model.Age = 28;
            model.DataFlag = r.Data.FirstOrDefault().DataFlag;

            // 強制 BIRTHDAY 和 IS_VIP 欄位要忽略更新! 即便 IsVip 屬性確實有給值，還是會忽略更新!
            var z = await dataContext.UpdateAsync(model, ignoreColumns: ["BIRTHDAY", "IS_VIP"], connection: conn, transaction: trans);
            Assert.IsTrue(z.Success);

            // ========================

            var commit = dataContext.Commit(trans, out var errorMsg);
            Assert.IsTrue(commit);

            // ========================

            var trans2 = dataContext.BeginTransaction(conn, IsolationLevel.ReadCommitted);

            var d = await dataContext.DeleteAsync(model, conn, trans2);
            Assert.IsTrue(d.Success);

            commit = dataContext.Commit(trans2, out errorMsg);
            Assert.IsTrue(commit);

            await conn.CloseAsync();
            conn.Dispose();

#if DEBUG
            await Task.Delay(100);
#endif
        }

        /// <summary>單檔資料 CRUD 與獨立資料庫連線測試<para/>
        /// 1. 此為測試單檔資料 CRUD。<para/>
        /// 2. Zayni Frameowrk 外部會使用自己獨立的 DbConnection 和 DbTransaction 的測試。
        /// </summary>
        [TestMethod()]
        [Description("單檔資料 CRUD 與獨立資料庫連線測試")]
        public void SingleTable_DataContext_CRUD_Connection_Transaction_Test()
        {
            var account = RandomTextHelper.Create(10);

            var model = new UserModel()
            {
                Account = account,
                Name = "Nancy",
                Age = 27,
                DOB = new DateTime(1993, 2, 17),
                Sex = 0,
                IsVip = true
            };

            var reqArgs = new UserReqArgs()
            {
                Account = model.Account
            };

            var dataContext = new UserDataContext();
            var r = dataContext.Select(new SelectInfo<UserReqArgs>() { DataContent = reqArgs });
            Assert.IsTrue(r.Success);

            if (r.Data.IsNotNullOrEmpty())
            {
                dataContext.Delete(model);
            }

            // ========================

            var conn = dataContext.CreateConnection();
            var trans = dataContext.BeginTransaction(conn);

            // ========================

            var c = dataContext.Insert(model, conn, trans);
            Assert.IsTrue(c.Success);
            System.Threading.SpinWait.SpinUntil(() => false, 80);

            // ========================

            // 看來連 Select 也需要可以吃 DbConnection 和 DbTransaction 的參數了... 否則也是過不了...
            r = dataContext.Select(new SelectInfo<UserReqArgs>() { DataContent = reqArgs }, conn, trans);
            Assert.IsTrue(r.Success);
            Assert.IsTrue(r.Data.IsNotNullOrEmpty());
            Assert.IsNotNull(r.Data.FirstOrDefault());
            Assert.AreEqual(account, r.Data.FirstOrDefault().Account);

            model.Name = "Kelly Wells";
            model.Age = 28;
            model.DataFlag = r.Data.FirstOrDefault().DataFlag;

            // 強制 BIRTHDAY 和 IS_VIP 欄位要忽略更新! 即便 IsVip 屬性確實有給值，還是會忽略更新!
            var z = dataContext.Update(model, ignoreColumns: _ignoreColumns, connection: conn, transaction: trans);
            Assert.IsTrue(z.Success);

            // ========================

            var commit = dataContext.Commit(trans, out var errorMsg);
            Assert.IsTrue(commit);

            // ========================

            var trans2 = dataContext.BeginTransaction(conn, IsolationLevel.ReadCommitted);

            var d = dataContext.Delete(model, conn, trans2);
            Assert.IsTrue(d.Success);

            commit = dataContext.Commit(trans2, out errorMsg);
            Assert.IsTrue(commit);

            conn.Close();
            conn.Dispose();

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, TimeSpan.FromMilliseconds(150));
#endif
        }

        /// <summary>單檔資料 CRUD 與資料庫交易測試
        /// </summary>
        [TestMethod()]
        [Description("單檔資料 CRUD 與資料庫交易測試")]
        public async Task SingleTable_DataContext_CRUD_Transaction_AsyncTest()
        {
            var account = "Katie";

            var model = new UserModel()
            {
                Account = account,
                Name = "Amy",
                Age = 26,
                DOB = new DateTime(1993, 2, 17),
                Sex = 0,
                IsVip = true
            };

            var reqArgs = new UserReqArgs()
            {
                Account = model.Account
            };

            var dataContext = new UserDataContext();

            var r = await dataContext.SelectAsync(new SelectInfo<UserReqArgs>() { DataContent = reqArgs });
            Assert.IsTrue(r.Success);

            if (r.Data.IsNotNullOrEmpty())
            {
                await dataContext.DeleteAsync(model);
            }

            var conn = await dataContext.CreateConnectionAsync();
            var trans = dataContext.BeginTransaction(conn);

            var c = await dataContext.InsertAsync(model, conn, trans);
            Assert.IsTrue(c.Success);
            await Task.Delay(80);

            var commit = dataContext.Commit(trans, out var errorMsg);
            Assert.IsTrue(commit);

            r = await dataContext.SelectAsync(new SelectInfo<UserReqArgs>() { DataContent = reqArgs });
            Assert.IsTrue(r.Success);
            Assert.IsTrue(r.Data.IsNotNullOrEmpty());
            Assert.IsNotNull(r.Data.FirstOrDefault());
            Assert.AreEqual(account, r.Data.FirstOrDefault().Account);

            model.Sex = 1;
            model.Name = "Sylvia";
            model.IsVip = false;
            model.DataFlag = r.Data.FirstOrDefault().DataFlag;

            var trans2 = dataContext.BeginTransaction(conn);

            // 強制 BIRTHDAY 和 IS_VIP 欄位要忽略更新! 即便 IsVip 屬性確實有給值，還是會忽略更新!
            var u = await dataContext.UpdateAsync(model, conn, trans2, ignoreColumns: ["BIRTHDAY", "IS_VIP"]);
            Assert.IsTrue(u.Success);

            commit = dataContext.Commit(trans2, out errorMsg);
            Assert.IsTrue(commit);

            r = await dataContext.SelectAsync(new SelectInfo<UserReqArgs>() { DataContent = reqArgs });
            Assert.IsTrue(r.Success);
            Assert.IsTrue(r.Success);
            Assert.IsTrue(r.Data.IsNotNullOrEmpty());
            Assert.IsNotNull(r.Data.FirstOrDefault());
            Assert.AreEqual(account, r.Data.FirstOrDefault().Account);
            Assert.AreEqual("Sylvia", r.Data.FirstOrDefault().Name);

            var trans3 = dataContext.BeginTransaction(conn);

            var d = await dataContext.DeleteAsync(model, conn, trans3);
            Assert.IsTrue(d.Success);

            commit = dataContext.Commit(trans3, out errorMsg);
            Assert.IsTrue(commit);

            await conn.CloseAsync();
            conn.Dispose();

#if DEBUG
            await Task.Delay(100);
#endif
        }

        /// <summary>單檔資料 CRUD 與資料庫交易測試
        /// </summary>
        [TestMethod()]
        [Description("單檔資料 CRUD 與資料庫交易測試")]
        public void SingleTable_DataContext_CRUD_Transaction_Test()
        {
            var account = "Katie";

            var model = new UserModel()
            {
                Account = account,
                Name = "Amy",
                Age = 26,
                DOB = new DateTime(1993, 2, 17),
                Sex = 0,
                IsVip = true
            };

            var reqArgs = new UserReqArgs()
            {
                Account = model.Account
            };

            var dataContext = new UserDataContext();

            var r = dataContext.Select(new SelectInfo<UserReqArgs>() { DataContent = reqArgs });
            Assert.IsTrue(r.Success);

            if (r.Data.IsNotNullOrEmpty())
            {
                dataContext.Delete(model);
            }

            var conn = dataContext.CreateConnection();
            var trans = dataContext.BeginTransaction(conn);

            var c = dataContext.Insert(model, conn, trans);
            Assert.IsTrue(c.Success);
            System.Threading.SpinWait.SpinUntil(() => false, 80);

            var commit = dataContext.Commit(trans, out var errorMsg);
            Assert.IsTrue(commit);

            r = dataContext.Select(new SelectInfo<UserReqArgs>() { DataContent = reqArgs });
            Assert.IsTrue(r.Success);
            Assert.IsTrue(r.Data.IsNotNullOrEmpty());
            Assert.IsNotNull(r.Data.FirstOrDefault());
            Assert.AreEqual(account, r.Data.FirstOrDefault().Account);

            model.Sex = 1;
            model.Name = "Sylvia";
            model.IsVip = false;
            model.DataFlag = r.Data.FirstOrDefault().DataFlag;

            var trans2 = dataContext.BeginTransaction(conn);

            // 強制 BIRTHDAY 和 IS_VIP 欄位要忽略更新! 即便 IsVip 屬性確實有給值，還是會忽略更新!
            var u = dataContext.Update(model, conn, trans2, ignoreColumns: ["BIRTHDAY", "IS_VIP"]);
            Assert.IsTrue(u.Success);

            commit = dataContext.Commit(trans2, out errorMsg);
            Assert.IsTrue(commit);

            r = dataContext.Select(new SelectInfo<UserReqArgs>() { DataContent = reqArgs });
            Assert.IsTrue(r.Success);
            Assert.IsTrue(r.Success);
            Assert.IsTrue(r.Data.IsNotNullOrEmpty());
            Assert.IsNotNull(r.Data.FirstOrDefault());
            Assert.AreEqual(account, r.Data.FirstOrDefault().Account);
            Assert.AreEqual("Sylvia", r.Data.FirstOrDefault().Name);

            var trans3 = dataContext.BeginTransaction(conn);

            var d = dataContext.Delete(model, conn, trans3);
            Assert.IsTrue(d.Success);

            commit = dataContext.Commit(trans3, out errorMsg);
            Assert.IsTrue(commit);

            conn.Close();
            conn.Dispose();

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, TimeSpan.FromMilliseconds(150));
#endif
        }

        /// <summary>單檔資料 CRUD 測試 (主索引鍵為組合鍵的測試)
        /// </summary>
        [TestMethod()]
        [Description("單檔資料 CRUD 測試 (主索引鍵為組合鍵的測試)")]
        public async Task SingleTable_MultiplePKey_DataContext_CRUD_AsyncTest()
        {
            var account = "multiple-001";
            var userId = "test-user-001";

            var model = new UserModel2()
            {
                Account = account,
                UserId = userId,
                UserName = "Amber",
                Sex = 0,
                Description = "AAA"
            };

            var dataContext = new User2DataContext();

            var c = await dataContext.InsertAsync(model);
            Assert.IsTrue(c.Success);

            var r = await dataContext.SelectAsync(new SelectInfo<UserModel2>() { DataContent = model });
            Assert.IsTrue(r.Success);
            Assert.IsTrue(r.Data.IsNotNullOrEmpty());
            Assert.IsNotNull(r.Data.FirstOrDefault());
            Assert.AreEqual(account, r.Data.FirstOrDefault().Account);

            model.UserName = null;
            model.Birthday = new DateTime(1993, 3, 27);
            model.Description = "BBB";
            model.UserPassword = "BBB";
            var u = await dataContext.UpdateAsync(model);
            Assert.IsTrue(u.Success);

            model.UserName = null;      // 資料庫中 UserName 為不允許 DBNull 的欄位 (IsAllowNull = false)，因此應該要略過更新 UserName 欄位
            model.Description = "";        // 強制更新資料中 Description 欄位為空字串，不是 DBNull
            model.UserPassword = "CCC";     // 強制更新資料中 UserPassword 欄位為 CCC 字串
            model.Money = null;      // 資料庫中 Money 為允許 DBNull 的欄位 (IsAllowNull = true)，因此這邊會確實強制把 Money 欄位更新為 DBNull。
            model.Birthday = null;      // 資料庫中 Birthday 為允許 DBNull 的欄位 (IsAllowNull = true)，因此這邊會確實強制把 Birthday 欄位更新為 DBNull。
            u = await dataContext.UpdateAsync(model);
            Assert.IsTrue(u.Success);

            model.UserId = null;   // 故意測試其中一個 PKey 沒有當作查詢 where 條件的情況!
            r = await dataContext.SelectAsync(new SelectInfo<UserModel2>() { DataContent = model });
            Assert.IsTrue(r.Success);
            Assert.IsTrue(r.Success);
            Assert.IsTrue(r.Data.IsNotNullOrEmpty());
            Assert.IsNotNull(r.Data.FirstOrDefault());
            Assert.AreEqual(account, r.Data.FirstOrDefault().Account);
            Assert.AreEqual("", r.Data.FirstOrDefault().Description);
            Assert.AreEqual("CCC", r.Data.FirstOrDefault().UserPassword);
            Assert.IsNull(r.Data.FirstOrDefault().Money);
            Assert.IsNull(r.Data.FirstOrDefault().Birthday);

            model.Account = account;
            model.UserId = userId;
            var d = await dataContext.DeleteAsync(model);
            Assert.IsTrue(d.Success);

#if DEBUG
            await Task.Delay(100);
#endif
        }

        /// <summary>單檔資料 CRUD 測試 (主索引鍵為組合鍵的測試)
        /// </summary>
        [TestMethod()]
        [Description("單檔資料 CRUD 測試 (主索引鍵為組合鍵的測試)")]
        public void SingleTable_MultiplePKey_DataContext_CRUD_Test()
        {
            var account = "multiple-001";
            var userId = "test-user-001";

            var model = new UserModel2()
            {
                Account = account,
                UserId = userId,
                UserName = "Amber",
                Sex = 0,
                Description = "AAA"
            };

            var dataContext = new User2DataContext();

            var c = dataContext.Insert(model);
            Assert.IsTrue(c.Success);

            var r = dataContext.Select(new SelectInfo<UserModel2>() { DataContent = model });
            Assert.IsTrue(r.Success);
            Assert.IsTrue(r.Data.IsNotNullOrEmpty());
            Assert.IsNotNull(r.Data.FirstOrDefault());
            Assert.AreEqual(account, r.Data.FirstOrDefault().Account);

            model.UserName = null;
            model.Birthday = new DateTime(1993, 3, 27);
            model.Description = "BBB";
            model.UserPassword = "BBB";
            var u = dataContext.Update(model);
            Assert.IsTrue(u.Success);

            model.UserName = null;      // 資料庫中 UserName 為不允許 DBNull 的欄位 (IsAllowNull = false)，因此應該要略過更新 UserName 欄位
            model.Description = "";        // 強制更新資料中 Description 欄位為空字串，不是 DBNull
            model.UserPassword = "CCC";     // 強制更新資料中 UserPassword 欄位為 CCC 字串
            model.Money = null;      // 資料庫中 Money 為允許 DBNull 的欄位 (IsAllowNull = true)，因此這邊會確實強制把 Money 欄位更新為 DBNull。
            model.Birthday = null;      // 資料庫中 Birthday 為允許 DBNull 的欄位 (IsAllowNull = true)，因此這邊會確實強制把 Birthday 欄位更新為 DBNull。
            u = dataContext.Update(model);
            Assert.IsTrue(u.Success);

            model.UserId = null;   // 故意測試其中一個 PKey 沒有當作查詢 where 條件的情況!
            r = dataContext.Select(new SelectInfo<UserModel2>() { DataContent = model });
            Assert.IsTrue(r.Success);
            Assert.IsTrue(r.Success);
            Assert.IsTrue(r.Data.IsNotNullOrEmpty());
            Assert.IsNotNull(r.Data.FirstOrDefault());
            Assert.AreEqual(account, r.Data.FirstOrDefault().Account);
            Assert.AreEqual("", r.Data.FirstOrDefault().Description);
            Assert.AreEqual("CCC", r.Data.FirstOrDefault().UserPassword);
            Assert.IsNull(r.Data.FirstOrDefault().Money);
            Assert.IsNull(r.Data.FirstOrDefault().Birthday);

            model.Account = account;
            model.UserId = userId;
            var d = dataContext.Delete(model);
            Assert.IsTrue(d.Success);

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, TimeSpan.FromMilliseconds(150));
#endif
        }

        /// <summary>單檔資料 CRUD 測試，並且更新時有 DATA_FLAG_ERROR 錯誤的情境測試
        /// </summary>
        [TestMethod()]
        [Description("單檔資料 CRUD 測試，並且更新時有 DATA_FLAG_ERROR 錯誤的情境測試")]
        public async Task SingleTable_DataFlagError_DataContext_CRUD_AsyncTest()
        {
            var account = "RileyReyes_001";

            var model = new UserModel()
            {
                Account = account,
                Name = "Riley Reyes",
                Age = 27,
                DOB = new DateTime(1997, 4, 21),
                Sex = 0,
                IsVip = true
            };

            var reqArgs = new UserReqArgs()
            {
                Account = model.Account
            };

            var context = new UserDataContext();

            var q = await context.SelectAsync(new SelectInfo<UserReqArgs>() { DataContent = reqArgs });
            Assert.IsTrue(q.Success);

            if (q.Data.IsNullOrEmpty())
            {
                var c = await context.InsertAsync(model);
                Assert.IsTrue(c.Success);
                await Task.Delay(80);
            }

            var r = await context.SelectAsync(new SelectInfo<UserReqArgs>() { DataContent = reqArgs });
            Assert.IsTrue(r.Success);
            Assert.IsTrue(r.Data.IsNotNullOrEmpty());
            Assert.IsNotNull(r.Data.FirstOrDefault());
            Assert.IsNotNull(r.Data.FirstOrDefault().DataFlag);
            Assert.AreEqual(account, r.Data.FirstOrDefault().Account);

            model.Name = "Test DataFlag";
            model.DataFlag = r.Data.FirstOrDefault().DataFlag;
            var u = await context.UpdateAsync(model);
            Assert.IsTrue(u.Success);

            r = await context.SelectAsync(new SelectInfo<UserReqArgs>() { DataContent = reqArgs });
            Assert.IsTrue(r.Success);
            Assert.IsTrue(r.Success);
            Assert.IsTrue(r.Data.IsNotNullOrEmpty());
            Assert.IsNotNull(r.Data.FirstOrDefault());
            Assert.AreEqual(account, r.Data.FirstOrDefault().Account);
            Assert.AreEqual(account, r.Data.FirstOrDefault().Account);
            Assert.AreEqual("Test DataFlag", r.Data.FirstOrDefault().Name);

            model.Name = "Test WrongDataFlag";
            // 故意沒有重新指定 DataFlag，也就是傳入錯誤的 DataFlag 參數並且嘗試更新
            u = await context.UpdateAsync(model);
            Assert.IsFalse(u.Success);
            Assert.AreEqual(StatusConstant.DATA_FLAG_ERROR, u.Code);

            var d = await context.DeleteAsync(model);
            Assert.IsTrue(d.Success);

#if DEBUG
            await Task.Delay(100);
#endif
        }

        /// <summary>單檔資料 CRUD 測試，並且更新時有 DATA_FLAG_ERROR 錯誤的情境測試
        /// </summary>
        [TestMethod()]
        [Description("單檔資料 CRUD 測試，並且更新時有 DATA_FLAG_ERROR 錯誤的情境測試")]
        public void SingleTable_DataFlagError_DataContext_CRUD_Test()
        {
            var account = "RileyReyes_001";

            var model = new UserModel()
            {
                Account = account,
                Name = "Riley Reyes",
                Age = 27,
                DOB = new DateTime(1997, 4, 21),
                Sex = 0,
                IsVip = true
            };

            var reqArgs = new UserReqArgs()
            {
                Account = model.Account
            };

            var context = new UserDataContext();

            var q = context.Select(new SelectInfo<UserReqArgs>() { DataContent = reqArgs });
            Assert.IsTrue(q.Success);

            When.True(q.Data.IsNullOrEmpty(), () =>
            {
                var c = context.Insert(model);
                Assert.IsTrue(c.Success);
                System.Threading.SpinWait.SpinUntil(() => false, 80);
            });

            var r = context.Select(new SelectInfo<UserReqArgs>() { DataContent = reqArgs });
            Assert.IsTrue(r.Success);
            Assert.IsTrue(r.Data.IsNotNullOrEmpty());
            Assert.IsNotNull(r.Data.FirstOrDefault());
            Assert.IsNotNull(r.Data.FirstOrDefault().DataFlag);
            Assert.AreEqual(account, r.Data.FirstOrDefault().Account);

            model.Name = "Test DataFlag";
            model.DataFlag = r.Data.FirstOrDefault().DataFlag;
            var u = context.Update(model);
            Assert.IsTrue(u.Success);

            r = context.Select(new SelectInfo<UserReqArgs>() { DataContent = reqArgs });
            Assert.IsTrue(r.Success);
            Assert.IsTrue(r.Success);
            Assert.IsTrue(r.Data.IsNotNullOrEmpty());
            Assert.IsNotNull(r.Data.FirstOrDefault());
            Assert.AreEqual(account, r.Data.FirstOrDefault().Account);
            Assert.AreEqual(account, r.Data.FirstOrDefault().Account);
            Assert.AreEqual("Test DataFlag", r.Data.FirstOrDefault().Name);

            model.Name = "Test WrongDataFlag";
            // 故意沒有重新指定 DataFlag，也就是傳入錯誤的 DataFlag 參數並且嘗試更新
            u = context.Update(model);
            Assert.IsFalse(u.Success);
            Assert.AreEqual(StatusConstant.DATA_FLAG_ERROR, u.Code);

            var d = context.Delete(model);
            Assert.IsTrue(d.Success);

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, TimeSpan.FromMilliseconds(150));
#endif
        }

        /// <summary>單檔資料依照 Where 條件進行資料刪除的測試
        /// </summary>
        [TestMethod()]
        [Description("單檔資料依照 Where 條件進行資料刪除的測試")]
        public async Task SingleTable_DataContext_DeleteByWhere_AsyncTest()
        {
            var account = "AsukaKirara";

            var model = new UserModel()
            {
                Account = account,
                Name = "Asuka Kirara 明日花羅錡",
                Age = 20,
                DOB = new DateTime(1993, 2, 17),
                Sex = 0,
                IsVip = true
            };

            var reqArgs = new UserReqArgs()
            {
                Account = model.Account
            };

            var dataContext = new UserDataContext();

            var r = await dataContext.SelectAsync(new SelectInfo<UserReqArgs>() { DataContent = reqArgs });
            Assert.IsTrue(r.Success);

            if (r.Data.IsNotNullOrEmpty())
            {
                await dataContext.DeleteAsync(model);
            }

            var c = await dataContext.InsertAsync(model);
            Assert.IsTrue(c.Success);
            await Task.Delay(80);

            var d = await dataContext.DeleteAsync(model, wheres: ["NAME", "IS_VIP"]);
            Assert.IsTrue(d.Success);

#if DEBUG
            await Task.Delay(100);
#endif
        }

        /// <summary>單檔資料依照 Where 條件進行資料刪除的測試
        /// </summary>
        [TestMethod()]
        [Description("單檔資料依照 Where 條件進行資料刪除的測試")]
        public void SingleTable_DataContext_DeleteByWhere_Test()
        {
            var account = "AsukaKirara";

            var model = new UserModel()
            {
                Account = account,
                Name = "Asuka Kirara 明日花羅錡",
                Age = 20,
                DOB = new DateTime(1993, 2, 17),
                Sex = 0,
                IsVip = true
            };

            var reqArgs = new UserReqArgs()
            {
                Account = model.Account
            };

            var dataContext = new UserDataContext();

            var r = dataContext.Select(new SelectInfo<UserReqArgs>() { DataContent = reqArgs });
            Assert.IsTrue(r.Success);

            if (r.Data.IsNotNullOrEmpty())
            {
                dataContext.Delete(model);
            }

            var c = dataContext.Insert(model);
            Assert.IsTrue(c.Success);
            System.Threading.SpinWait.SpinUntil(() => false, 80);

            var d = dataContext.Delete(model, wheres: ["NAME", "IS_VIP"]);
            Assert.IsTrue(d.Success);

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, TimeSpan.FromMilliseconds(150));
#endif
        }

        /// <summary>單檔資料更新的 DataFlag 檢查的測試
        /// </summary>
        [TestMethod()]
        [Description("單檔資料更新的 DataFlag 檢查的測試")]
        public async Task DataFlagCheck_DataContext_AsyncTest()
        {
            var account = $"Amber002-{RandomTextHelper.Create(10)}";

            var model = new UserModel()
            {
                Account = account,
                Name = "Katie",
                Age = 26,
                DOB = new DateTime(1988, 2, 17),
                Sex = 0,
                IsVip = true
            };

            var reqArgs = new UserModel()
            {
                Account = model.Account
            };

            var dataContext = new UserModelDataContext();

            var q = await dataContext.SelectAsync(new SelectInfo<UserModel>() { DataContent = reqArgs });
            Assert.IsTrue(q.Success);

            When.True(q.Data.IsNullOrEmpty(), async () =>
            {
                var c = await dataContext.InsertAsync(model);
                Assert.IsTrue(c.Success);
            });

            await Task.Delay(100);

            var r = await dataContext.SelectAsync(new SelectInfo<UserModel>() { DataContent = reqArgs });
            Assert.IsTrue(r.Success);
            Assert.IsTrue(r.Data.IsNotNullOrEmpty());
            Assert.IsNotNull(r.Data.FirstOrDefault());
            Assert.AreEqual(account, r.Data.FirstOrDefault().Account);

            model.Name = "Katie Waton";
            model.DataFlag = r.Data.FirstOrDefault().DataFlag;
            var u = await dataContext.UpdateAsync(model);
            Assert.IsTrue(u.Success);

            var d = await dataContext.DeleteAsync(model);
            Assert.IsTrue(d.Success);

#if DEBUG
            await Task.Delay(100);
#endif
        }

        /// <summary>單檔資料更新的 DataFlag 檢查的測試
        /// </summary>
        [TestMethod()]
        [Description("單檔資料更新的 DataFlag 檢查的測試")]
        public void DataFlagCheck_DataContext_Test()
        {
            var account = $"Amber002-{RandomTextHelper.Create(10)}";

            var model = new UserModel()
            {
                Account = account,
                Name = "Katie",
                Age = 26,
                DOB = new DateTime(1988, 2, 17),
                Sex = 0,
                IsVip = true
            };

            var reqArgs = new UserModel()
            {
                Account = model.Account
            };

            var dataContext = new UserModelDataContext();

            var q = dataContext.Select(new SelectInfo<UserModel>() { DataContent = reqArgs });
            Assert.IsTrue(q.Success);

            When.True(q.Data.IsNullOrEmpty(), () =>
            {
                var c = dataContext.Insert(model);
                Assert.IsTrue(c.Success);
            });

            System.Threading.SpinWait.SpinUntil(() => false, TimeSpan.FromMilliseconds(100));

            var r = dataContext.Select(new SelectInfo<UserModel>() { DataContent = reqArgs });
            Assert.IsTrue(r.Success);
            Assert.IsTrue(r.Data.IsNotNullOrEmpty());
            Assert.IsNotNull(r.Data.FirstOrDefault());
            Assert.AreEqual(account, r.Data.FirstOrDefault().Account);

            model.Name = "Katie Waton";
            model.DataFlag = r.Data.FirstOrDefault().DataFlag;
            var u = dataContext.Update(model);
            Assert.IsTrue(u.Success);

            var d = dataContext.Delete(model);
            Assert.IsTrue(d.Success);

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, TimeSpan.FromMilliseconds(150));
#endif
        }

        /// <summary>查詢全部資料的測試，沒有 PrimaryKey 當作 where 條件的查詢測試
        /// </summary>
        [TestMethod()]
        [Description("查詢全部資料的測試，沒有 PrimaryKey 當作 where 條件的查詢測試")]
        public async Task SelectAll_DataContext_AsyncTest()
        {
            var dataContext = new UserDataContext();
            var q = await dataContext.SelectAsync(new SelectInfo<UserReqArgs>());
            Assert.IsTrue(q.Success);

#if DEBUG
            await Task.Delay(100);
#endif
        }

        /// <summary>查詢全部資料的測試，沒有 PrimaryKey 當作 where 條件的查詢測試
        /// </summary>
        [TestMethod()]
        [Description("查詢全部資料的測試，沒有 PrimaryKey 當作 where 條件的查詢測試")]
        public void SelectAll_DataContext_Test()
        {
            var dataContext = new UserDataContext();
            var q = dataContext.Select(new SelectInfo<UserReqArgs>());
            Assert.IsTrue(q.Success);

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, TimeSpan.FromMilliseconds(150));
#endif
        }

        /// <summary>查詢前 N 筆資料的測試 (SELECT TOP 的測試)
        /// </summary>
        [TestMethod()]
        [Description("查詢前 N 筆資料的測試 (SELECT TOP)")]
        public async Task SelectTop_DataContext_AsyncTest()
        {
            var dataContext = new UserDataContext();

            // 忽略查詢出的欄位包含 BIRTHDAY 和 IS_VIP
            var q = await dataContext.SelectAsync(new SelectInfo<UserReqArgs>() { Top = 2, IgnoreColumns = ["BIRTHDAY", "IS_VIP"] });
            Assert.IsTrue(q.Success);

#if DEBUG
            await Task.Delay(100);
#endif
        }

        /// <summary>查詢前 N 筆資料的測試 (SELECT TOP 的測試)
        /// </summary>
        [TestMethod()]
        [Description("查詢前 N 筆資料的測試 (SELECT TOP)")]
        public void SelectTop_DataContext_Test()
        {
            var dataContext = new UserDataContext();

            // 忽略查詢出的欄位包含 BIRTHDAY 和 IS_VIP
            var q = dataContext.Select(new SelectInfo<UserReqArgs>() { Top = 2, IgnoreColumns = ["BIRTHDAY", "IS_VIP"] });
            Assert.IsTrue(q.Success);

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, TimeSpan.FromMilliseconds(150));
#endif
        }

        /// <summary>模糊比對查詢的測試
        /// </summary>
        [TestMethod()]
        [Description("模糊比對查詢的測試")]
        public async Task SelectLike_DataContext_AsyncTest()
        {
            var reqArgs = new UserReqArgs() { Account = "Test" };
            var dataContext = new UserDataContext();
            var q = await dataContext.SelectAsync(new SelectInfo<UserReqArgs>() { DataContent = reqArgs });
            Assert.IsTrue(q.Success);

#if DEBUG
            await Task.Delay(100);
#endif
        }

        /// <summary>模糊比對查詢的測試
        /// </summary>
        [TestMethod()]
        [Description("模糊比對查詢的測試")]
        public void SelectLike_DataContext_Test()
        {
            var reqArgs = new UserReqArgs() { Account = "Test" };
            var dataContext = new UserDataContext();
            var q = dataContext.Select(new SelectInfo<UserReqArgs>() { DataContent = reqArgs });
            Assert.IsTrue(q.Success);

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, TimeSpan.FromMilliseconds(150));
#endif
        }

        /// <summary>模糊比對查詢的測試
        /// </summary>
        [TestMethod()]
        [Description("模糊比對查詢的測試")]
        public async Task SelectByPKey_DataContext_AsyncTest()
        {
            #region Arrange

            var account = "ZayniTest001";

            var model = new UserModel()
            {
                Account = account,
                Name = "ZayniTest001",
                Age = 25,
                DOB = new DateTime(2004, 2, 17),
                Sex = 0,
                IsVip = true
            };

            var reqArgs = new UserReqArgs()
            {
                Account = model.Account
            };

            var dc = new UserDataContext();

            var r = await dc.SelectAsync(new SelectInfo<UserReqArgs>() { DataContent = reqArgs });
            Assert.IsTrue(r.Success);

            if (r.Data.IsNullOrEmpty())
            {
                var c = await dc.InsertAsync(model);
                Assert.IsTrue(c.Success);
                await Task.Delay(80);
            }

            #endregion Arrange

            #region Act & Assert

            var dataContext = new UserDataContext();
            var q = await dataContext.SelectByAccountAsync(account);
            Assert.IsTrue(q.Success);

            #endregion Act & Assert

#if DEBUG
            await Task.Delay(100);
#endif
        }

        /// <summary>模糊比對查詢的測試
        /// </summary>
        [TestMethod()]
        [Description("模糊比對查詢的測試")]
        public void SelectByPKey_DataContext_Test()
        {
            #region Arrange

            var account = "ZayniTest001";

            var model = new UserModel()
            {
                Account = account,
                Name = "ZayniTest001",
                Age = 25,
                DOB = new DateTime(2004, 2, 17),
                Sex = 0,
                IsVip = true
            };

            var reqArgs = new UserReqArgs()
            {
                Account = model.Account
            };

            var dc = new UserDataContext();

            var r = dc.Select(new SelectInfo<UserReqArgs>() { DataContent = reqArgs });
            Assert.IsTrue(r.Success);

            When.True(r.Data.IsNullOrEmpty(), () =>
            {
                var c = dc.Insert(model);
                Assert.IsTrue(c.Success);
                System.Threading.SpinWait.SpinUntil(() => false, 80);
            });

            #endregion Arrange

            #region Act & Assert

            var dataContext = new UserDataContext();
            var q = dataContext.SelectByAccount(account);
            Assert.IsTrue(q.Success);

            #endregion Act & Assert

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, TimeSpan.FromMilliseconds(150));
#endif
        }

        /// <summary>使用指定欄位當作 where 條件查詢的測試
        /// </summary>
        [TestMethod()]
        [Description("使用指定欄位當作 where 條件查詢的測試")]
        public async Task SelectByWhere_AsyncTest()
        {
            var dataContext = new UserDataContext();

            var model = new UserModel()
            {
                Account = "Katie5278",
                Name = "Amy",
                Age = 25,
                DOB = new DateTime(1993, 2, 17),
                Sex = 0,
                IsVip = true
            };

            var r = await dataContext.SelectAsync(new SelectInfo<UserReqArgs>() { DataContent = new UserReqArgs() { Account = model.Account } });
            Assert.IsTrue(r.Success);

            if (r.Data.IsNullOrEmpty())
            {
                await dataContext.InsertAsync(model);
            }

            var q = await dataContext.SelectByNameAsync("Amy");
            Assert.IsTrue(q.Success);

#if DEBUG
            await Task.Delay(100);
#endif
        }

        /// <summary>使用指定欄位當作 where 條件查詢的測試
        /// </summary>
        [TestMethod()]
        [Description("使用指定欄位當作 where 條件查詢的測試")]
        public void SelectByWhere_Test()
        {
            var dataContext = new UserDataContext();

            var model = new UserModel()
            {
                Account = "Katie5278",
                Name = "Amy",
                Age = 25,
                DOB = new DateTime(1993, 2, 17),
                Sex = 0,
                IsVip = true
            };

            var r = dataContext.Select(new SelectInfo<UserReqArgs>() { DataContent = new UserReqArgs() { Account = model.Account } });
            Assert.IsTrue(r.Success);
            When.True(r.Data.IsNullOrEmpty(), () => dataContext.Insert(model));

            var q = dataContext.SelectByName("Amy");
            Assert.IsTrue(q.Success);

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, TimeSpan.FromMilliseconds(300));
#endif
        }

        /// <summary>單筆單檔資料 Insert 失敗後 Rollback 的測試
        /// </summary>
        [TestMethod()]
        [Description("單筆單檔資料 Insert 失敗後 Rollback 的測試")]
        public async Task SingleTable_DataContext_InsertFailRollback_AsyncTest()
        {
            #region Arrange

            var account = RandomTextHelper.Create(15);

            var model = new UserModel()
            {
                Account = account,
                Name = "DataContext_Test_501",
                Age = 21,
                DOB = new DateTime(1999, 2, 14),
                Sex = 0,
                IsVip = true
            };

            #endregion Arrange

            #region Act & Assert (使用框架內部自動開啟連線與交易)

            var dataContext = new UserDataContext();

            // 先 Insert 一筆資料到 FS_UNIT_TEST_USER 資料表
            var c = await dataContext.InsertAsync(model);
            Assert.IsTrue(c.Success);

            // 同樣的資料再次 Insert 會造成 Primary Key 重覆，新增資料失敗
            var cf = await dataContext.InsertAsync(model);
            Assert.IsFalse(cf.Success);

            // 嘗試查詢 FS_UNIT_TEST_USER 資料表中所有資料，測試 DbTransaction 交易是否有 Rollback 掉，沒有 lock 住原本的 table
            var qu = await dataContext.SelectAsync(new SelectInfo<UserReqArgs>());
            Assert.IsTrue(qu.Success);

            // 嘗試把 FS_UNIT_TEST_USER 資料表中原本新增成功的資料刪除掉，測試 DbTransaction 交易是否有 Rollback 掉，沒有 lock 住原本的 table
            var de = await dataContext.DeleteAsync(model);
            Assert.IsTrue(de.Success);

            #endregion Act & Assert (使用框架內部自動開啟連線與交易)

            #region Act & Assert (自行建立連線和開啟資料庫交易)

            model.Account = RandomTextHelper.Create(15);

            using (var conn = await dataContext.CreateConnectionAsync())
            using (var trans = conn.BeginTransaction())
            {
                // 先 Insert 一筆資料到 FS_UNIT_TEST_USER 資料表
                var j = await dataContext.InsertAsync(model, conn, trans);
                Assert.IsTrue(j.Success);

                // 確認送出新增動作
                var commit = dataContext.Commit(trans, out var errorMsg);
                Assert.IsTrue(commit, errorMsg);
            }

            using (var conn = await dataContext.CreateConnectionAsync())
            using (var trans2 = conn.BeginTransaction())
            {
                // 同樣的資料再次 Insert 會造成 Primary Key 重覆，新增資料失敗
                var ir = await dataContext.InsertAsync(model, conn, trans2);
                Assert.IsFalse(ir.Success);

                // 取消資料庫交易
                var bollback = dataContext.Rollback(trans2, out var errorMsg);
                Assert.IsTrue(bollback, errorMsg);
            }

            // 嘗試查詢 FS_UNIT_TEST_USER 資料表中所有資料，測試 DbTransaction 交易是否有 Rollback 掉，沒有 lock 住原本的 table
            var st = await dataContext.SelectAsync(new SelectInfo<UserReqArgs>());
            Assert.IsTrue(st.Success);

            // 嘗試把 FS_UNIT_TEST_USER 資料表中原本新增成功的資料刪除掉，測試 DbTransaction 交易是否有 Rollback 掉，沒有 lock 住原本的 table
            var d = await dataContext.DeleteAsync(model);
            Assert.IsTrue(d.Success);

            #endregion Act & Assert (自行建立連線和開啟資料庫交易)

#if DEBUG
            await Task.Delay(100);
#endif
        }

        /// <summary>單筆單檔資料 Insert 失敗後 Rollback 的測試
        /// </summary>
        [TestMethod()]
        [Description("單筆單檔資料 Insert 失敗後 Rollback 的測試")]
        public void SingleTable_DataContext_InsertFailRollback_Test()
        {
            #region Arrange

            var account = RandomTextHelper.Create(15);

            var model = new UserModel()
            {
                Account = account,
                Name = "DataContext_Test_501",
                Age = 21,
                DOB = new DateTime(1999, 2, 14),
                Sex = 0,
                IsVip = true
            };

            #endregion Arrange

            #region Act & Assert (使用框架內部自動開啟連線與交易)

            var dataContext = new UserDataContext();

            // 先 Insert 一筆資料到 FS_UNIT_TEST_USER 資料表
            var c = dataContext.Insert(model);
            Assert.IsTrue(c.Success);

            // 同樣的資料再次 Insert 會造成 Primary Key 重覆，新增資料失敗
            var cf = dataContext.Insert(model);
            Assert.IsFalse(cf.Success);

            // 嘗試查詢 FS_UNIT_TEST_USER 資料表中所有資料，測試 DbTransaction 交易是否有 Rollback 掉，沒有 lock 住原本的 table
            var qu = dataContext.Select(new SelectInfo<UserReqArgs>());
            Assert.IsTrue(qu.Success);

            // 嘗試把 FS_UNIT_TEST_USER 資料表中原本新增成功的資料刪除掉，測試 DbTransaction 交易是否有 Rollback 掉，沒有 lock 住原本的 table
            var de = dataContext.Delete(model);
            Assert.IsTrue(de.Success);

            #endregion Act & Assert (使用框架內部自動開啟連線與交易)

            #region Act & Assert (自行建立連線和開啟資料庫交易)

            model.Account = RandomTextHelper.Create(15);

            using (var conn = dataContext.CreateConnection())
            using (var trans = conn.BeginTransaction())
            {
                // 先 Insert 一筆資料到 FS_UNIT_TEST_USER 資料表
                var j = dataContext.Insert(model, conn, trans);
                Assert.IsTrue(j.Success);

                // 確認送出新增動作
                var commit = dataContext.Commit(trans, out var errorMsg);
                Assert.IsTrue(commit, errorMsg);
            }

            using (var conn = dataContext.CreateConnection())
            using (var trans2 = conn.BeginTransaction())
            {
                // 同樣的資料再次 Insert 會造成 Primary Key 重覆，新增資料失敗
                var ir = dataContext.Insert(model, conn, trans2);
                Assert.IsFalse(ir.Success);

                // 取消資料庫交易
                var bollback = dataContext.Rollback(trans2, out var errorMsg);
                Assert.IsTrue(bollback, errorMsg);
            }

            // 嘗試查詢 FS_UNIT_TEST_USER 資料表中所有資料，測試 DbTransaction 交易是否有 Rollback 掉，沒有 lock 住原本的 table
            var st = dataContext.Select(new SelectInfo<UserReqArgs>());
            Assert.IsTrue(st.Success);

            // 嘗試把 FS_UNIT_TEST_USER 資料表中原本新增成功的資料刪除掉，測試 DbTransaction 交易是否有 Rollback 掉，沒有 lock 住原本的 table
            var d = dataContext.Delete(model);
            Assert.IsTrue(d.Success);

            #endregion Act & Assert (自行建立連線和開啟資料庫交易)

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, TimeSpan.FromMilliseconds(150));
#endif
        }

        /// <summary>單筆單檔資料 Clear 測試
        /// </summary>
        [TestMethod()]
        [Description("單筆單檔資料 Insert 測試")]
        public async Task SingleTable_DataContext_Clear_AsyncTest()
        {
            #region Arrange

            var account = RandomTextHelper.Create(15);

            var model = new UserModel()
            {
                Account = account,
                Name = "DataContext_Test_045",
                Age = 22,
                DOB = new DateTime(1993, 2, 17),
                Sex = 0,
                IsVip = true
            };

            var dataContext = new UserDataContext();
            var c = await dataContext.InsertAsync(model);
            Assert.IsTrue(c.Success);

            account = RandomTextHelper.Create(15);

            model = new UserModel()
            {
                Account = account,
                Name = "DataContext_Test_045",
                Age = 22,
                DOB = new DateTime(2000, 2, 15),
                Sex = 0,
                IsVip = true
            };

            c = await dataContext.InsertAsync(model);
            Assert.IsTrue(c.Success);

            account = RandomTextHelper.Create(15);

            model = new UserModel()
            {
                Account = account,
                Name = "DataContext_Test_045",
                Age = 22,
                DOB = new DateTime(2001, 2, 14),
                Sex = 0,
                IsVip = true
            };

            c = await dataContext.InsertAsync(model);
            Assert.IsTrue(c.Success);

            #endregion Arrange

            #region Act & Assert

            var r = await dataContext.ClearAsync();
            Assert.IsTrue(r.Success);

            #endregion Act & Assert

#if DEBUG
            await Task.Delay(100);
#endif
        }

        /// <summary>單筆單檔資料 Clear 測試
        /// </summary>
        [TestMethod()]
        [Description("單筆單檔資料 Insert 測試")]
        public void SingleTable_DataContext_Clear_Test()
        {
            #region Arrange

            var account = RandomTextHelper.Create(15);

            var model = new UserModel()
            {
                Account = account,
                Name = "DataContext_Test_045",
                Age = 22,
                DOB = new DateTime(1993, 2, 17),
                Sex = 0,
                IsVip = true
            };

            var dataContext = new UserDataContext();
            var c = dataContext.Insert(model);
            Assert.IsTrue(c.Success);

            account = RandomTextHelper.Create(15);

            model = new UserModel()
            {
                Account = account,
                Name = "DataContext_Test_045",
                Age = 22,
                DOB = new DateTime(2000, 2, 15),
                Sex = 0,
                IsVip = true
            };

            c = dataContext.Insert(model);
            Assert.IsTrue(c.Success);

            account = RandomTextHelper.Create(15);

            model = new UserModel()
            {
                Account = account,
                Name = "DataContext_Test_045",
                Age = 22,
                DOB = new DateTime(2001, 2, 14),
                Sex = 0,
                IsVip = true
            };

            c = dataContext.Insert(model);
            Assert.IsTrue(c.Success);

            #endregion Arrange

            #region Act & Assert

            var r = dataContext.Clear();
            Assert.IsTrue(r.Success);

            #endregion Act & Assert

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, TimeSpan.FromMilliseconds(150));
#endif
        }

        #endregion 宣告測試方法
    }
}
