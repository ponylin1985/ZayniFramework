﻿using System;
using System.Data.Common;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.DataAccess;


namespace DataAccess.Test.TestData
{
    /// <summary>測試資料的資料存取類別
    /// </summary>
    internal class TestDataDao : BaseDataAccess
    {
        #region 宣告建構子

        /// <summary>預設建構子
        /// </summary>
        public TestDataDao() : base("ZayniUnitTest")
        {
        }

        #endregion 宣告建構子


        #region 宣告內部的方法

        /// <summary>建立資料庫測試資料
        /// </summary>
        /// <param name="sqlUser">使用者測試資料SQL</param>
        /// <param name="sqlPhone">使用者連絡電話測試資料SQL</param>
        internal async Task MakeDataAsync(string sqlUser, string sqlPhone)
        {
            try
            {
                using var conn = await base.CreateConnectionAsync();
                using var trans = base.BeginTransaction(conn);
                var cmdUser = base.GetSqlStringCommand(sqlUser, conn, trans);

                if (!await ExecuteAsync(cmdUser))
                {
                    base.Rollback(trans);
                    await conn.CloseAsync();
                    return;
                }

                var cmdPhone = base.GetSqlStringCommand(sqlPhone, conn, trans);

                if (!await ExecuteAsync(cmdPhone))
                {
                    base.Rollback(trans);
                    await conn.CloseAsync();
                    return;
                }

                if (!base.Commit(trans))
                {
                    await conn.CloseAsync();
                    return;
                }

                await conn.CloseAsync();
            }
            catch (Exception ex)
            {
                Command.StdoutErr(ex.ToString());
                throw;
            }
        }

        /// <summary>建立資料庫測試資料
        /// </summary>
        /// <param name="sqlUser">使用者測試資料SQL</param>
        /// <param name="sqlPhone">使用者連絡電話測試資料SQL</param>
        internal void MakeData(string sqlUser, string sqlPhone)
        {
            try
            {
                using var conn = base.CreateConnection();
                using var trans = base.BeginTransaction(conn);
                var cmdUser = base.GetSqlStringCommand(sqlUser, conn, trans);

                if (!Execute(cmdUser))
                {
                    base.Rollback(trans);
                    conn.Close();
                    return;
                }

                var cmdPhone = base.GetSqlStringCommand(sqlPhone, conn, trans);

                if (!Execute(cmdPhone))
                {
                    base.Rollback(trans);
                    conn.Close();
                    return;
                }

                if (!base.Commit(trans))
                {
                    conn.Close();
                    return;
                }

                conn.Close();
            }
            catch (Exception ex)
            {
                Command.StdoutErr(ex.ToString());
                throw;
            }
        }

        /// <summary>清除測試資料
        /// </summary>
        internal async Task ClearDataAsync()
        {
            var sqlDelete1 = @"DELETE FROM `FS_UNIT_TEST_USER`;";
            var sqlDelete2 = @"DELETE FROM `FS_UNIT_TEST_USER2`;";
            var sqlDelete3 = @"DELETE FROM `FS_UNIT_TEST_USER_PHONE_DETAIL`;";

            using var conn = await base.CreateConnectionAsync();
            using var trans = base.BeginTransaction(conn);
            var cmd1 = base.GetSqlStringCommand(sqlDelete1, conn, trans);

            if (!await ExecuteAsync(cmd1, false))
            {
                base.Rollback(trans);
                await conn.CloseAsync();
                return;
            }

            var cmd2 = base.GetSqlStringCommand(sqlDelete2, conn, trans);

            if (!await ExecuteAsync(cmd2, false))
            {
                base.Rollback(trans);
                await conn.CloseAsync();
                return;
            }

            var cmd3 = base.GetSqlStringCommand(sqlDelete3, conn, trans);

            if (!await ExecuteAsync(cmd3, false))
            {
                base.Rollback(trans);
                await conn.CloseAsync();
                return;
            }

            if (!base.Commit(trans))
            {
                await conn.CloseAsync();
                return;
            }

            await conn.CloseAsync();
        }

        /// <summary>清除測試資料
        /// </summary>
        internal void ClearData()
        {
            var sql = @"
                DELETE FROM `FS_UNIT_TEST_USER`;
                DELETE FROM `FS_UNIT_TEST_USER2`;
                DELETE FROM `FS_UNIT_TEST_USER_PHONE_DETAIL`; ";

            using var conn = base.CreateConnection();
            using var trans = base.BeginTransaction(conn);
            var cmd = base.GetSqlStringCommand(sql, conn, trans);

            if (!Execute(cmd))
            {
                base.Rollback(trans);
                conn.Close();
                return;
            }

            if (!base.Commit(trans))
            {
                conn.Close();
                return;
            }

            conn.Close();
        }

        #endregion 宣告內部的方法


        #region 宣告私有的方法

        /// <summary>執行 SQL 指令
        /// </summary>
        /// <param name="cmd">資料庫 SQL 指令</param>
        /// <param name="check">是否檢查執行結果的受影響資料筆數，預設會檢查</param>
        /// <returns>執行是否成功</returns>
        private async Task<bool> ExecuteAsync(DbCommand cmd, bool check = true)
        {
            using (cmd)
            {
                if (!await base.ExecuteNonQueryAsync(cmd))
                {
                    return false;
                }

                if (check)
                {
                    if (0 == DataCount)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>執行 SQL 指令
        /// </summary>
        /// <param name="cmd">資料庫 SQL 指令</param>
        /// <returns>執行是否成功</returns>
        private bool Execute(DbCommand cmd)
        {
            using (cmd)
            {
                if (!base.ExecuteNonQuery(cmd))
                {
                    return false;
                }

                if (0 == DataCount)
                {
                    return false;
                }
            }

            return true;
        }

        #endregion 宣告私有的方法
    }
}
