﻿using DataAccess.Test.TestModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.DataAccess;


namespace DataAccess.Test.TestDao
{
    /// <summary>單元測試用 BaseDataAccess 測試 Dao 類別
    /// </summary>
    internal class UserDao : BaseDataAccess
    {
        #region 宣告建構子

        /// <summary>預設建構子
        /// </summary>
        internal UserDao() : base("ZayniUnitTest")
        {
        }

        #endregion 宣告建構子


        #region 宣告測試方法

        /// <summary>新增資料 ExecuteNoeQuery 測試
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <returns>新增結果</returns>
        internal async Task<IResult> Insert_ExecuteNonQueryAsync(UserModel model)
        {
            var result = Result.Create();

            var sql = @" 
                INSERT INTO `FS_UNIT_TEST_USER` (
                      `ACCOUNT_ID`
                    , `NAME`
                    , `AGE`
                    , `SEX`
                    {sqlIsVip}
                    , `BIRTHDAY`
                ) VALUES (
                      @AccountId
                    , @Name
                    , @Age
                    , @Sex
                    {paramIsVip}
                    , @Birthday
                ); ";

            if (model.IsVip.IsNotNull() && (bool)model.IsVip)
            {
                sql = sql.Format(sqlIsVip => ", `IS_VIP`", paramIsVip => ", @IsVip");
            }
            else
            {
                sql = sql.Format(sqlIsVip => "", paramIsVip => "");
            }

            using (var conn = await base.CreateConnectionAsync())
            using (var trans = base.BeginTransaction(conn))
            {
                var cmd = GetSqlStringCommand(sql, conn, trans);
                base.AddInParameter(cmd, "@AccountId", DbColumnType.String, model.Account);
                base.AddInParameter(cmd, "@Name", DbColumnType.String, model.Name);
                base.AddInParameter(cmd, "@Age", DbColumnType.Int32, model.Age);
                base.AddInParameter(cmd, "@Sex", DbColumnType.Int32, model.Sex);
                base.AddInParameter(cmd, "@Birthday", DbColumnType.DateTime, model.DOB);

                if (model.IsVip.IsNotNull() && (bool)model.IsVip)
                {
                    base.AddInParameter(cmd, "@IsVip", DbColumnType.Boolean, model.IsVip);
                }

                if (!await base.ExecuteNonQueryAsync(cmd))
                {
                    base.Rollback(trans);
                    await conn.CloseAsync();
                    result.Message = base.Message;
                    return result;
                }

                if (1 != DataCount)
                {
                    base.Rollback(trans);
                    await conn.CloseAsync();
                    result.Message = $"Insert into test data to FS_UNIT_TEST_USER fail.";
                    return result;
                }

                if (!base.Commit(trans))
                {
                    await conn.CloseAsync();
                    result.Message = base.Message;
                    return result;
                }

                await conn.CloseAsync();
            }

            result.Success = true;
            return result;
        }

        /// <summary>新增資料 ExecuteNoeQuery 測試
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <returns>新增結果</returns>
        internal IResult Insert_ExecuteNonQuery(UserModel model)
        {
            var result = Result.Create();

            var sql = @" 
                INSERT INTO `FS_UNIT_TEST_USER` (
                      `ACCOUNT_ID`
                    , `NAME`
                    , `AGE`
                    , `SEX`
                    , `BIRTHDAY`
                ) VALUES (
                      @AccountId
                    , @Name
                    , @Age
                    , @Sex
                    , @Birthday
                ); ";

            using (var conn = base.CreateConnection())
            using (var trans = base.BeginTransaction(conn))
            {
                var cmd = GetSqlStringCommand(sql, conn, trans);
                base.AddInParameter(cmd, "@AccountId", DbColumnType.String, model.Account);
                base.AddInParameter(cmd, "@Name", DbColumnType.String, model.Name);
                base.AddInParameter(cmd, "@Age", DbColumnType.Int32, model.Age);
                base.AddInParameter(cmd, "@Sex", DbColumnType.Int32, model.Sex);
                base.AddInParameter(cmd, "@Birthday", DbColumnType.DateTime, model.DOB);

                if (!base.ExecuteNonQuery(cmd))
                {
                    base.Rollback(trans);
                    conn.Close();
                    result.Message = base.Message;
                    return result;
                }

                if (1 != DataCount)
                {
                    base.Rollback(trans);
                    conn.Close();
                    result.Message = $"Insert into test data to FS_UNIT_TEST_USER fail.";
                    return result;
                }

                if (!base.Commit(trans))
                {
                    conn.Close();
                    result.Message = base.Message;
                    return result;
                }

                conn.Close();
            }

            result.Success = true;
            return result;
        }

        /// <summary>更新資料 ExecuteNoeQuery 測試
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <returns>更新結果</returns>
        internal async Task<IResult> Update_ExecuteNonQueryAsync(UserModel model)
        {
            var result = Result.Create();

            var sql = @" 
                UPDATE `FS_UNIT_TEST_USER`
                   SET `AGE`          = @Age, 
                       `BIRTHDAY`     = @Birthday
                 WHERE `NAME`         = @Name  
                   AND `ACCOUNT_ID`   = @AccountId; ";

            using (var conn = await base.CreateConnectionAsync())
            using (var trans = base.BeginTransaction(conn))
            {
                var cmd = GetSqlStringCommand(sql, conn, trans);
                base.AddInParameter(cmd, "@Age", DbColumnType.Int32, model.Age);
                base.AddInParameter(cmd, "@Birthday", DbColumnType.DateTime, model.DOB);
                base.AddInParameter(cmd, "@Name", DbColumnType.String, model.Name);
                base.AddInParameter(cmd, "@AccountId", DbColumnType.String, model.Account);

                if (!await base.ExecuteNonQueryAsync(cmd))
                {
                    base.Rollback(trans);
                    conn.Close();
                    result.Message = base.Message;
                    return result;
                }

                if (1 != DataCount)
                {
                    base.Rollback(trans);
                    conn.Close();
                    result.Message = $"Update test data to FS_UNIT_TEST_USER fail.";
                    return result;
                }

                if (!base.Commit(trans))
                {
                    conn.Close();
                    result.Message = base.Message;
                    return result;
                }

                await conn.CloseAsync();
            }

            result.Success = true;
            return result;
        }

        /// <summary>更新資料 ExecuteNoeQuery 測試
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <returns>更新結果</returns>
        internal IResult Update_ExecuteNonQuery(UserModel model)
        {
            var result = Result.Create();

            var sql = @" 
                UPDATE `FS_UNIT_TEST_USER`
                   SET `AGE`          = @Age, 
                       `BIRTHDAY`     = @Birthday
                 WHERE `NAME`         = @Name  
                   AND `ACCOUNT_ID`   = @AccountId; ";

            using (var conn = base.CreateConnection())
            using (var trans = base.BeginTransaction(conn))
            {
                var cmd = GetSqlStringCommand(sql, conn, trans);
                base.AddInParameter(cmd, "@Age", DbColumnType.Int32, model.Age);
                base.AddInParameter(cmd, "@Birthday", DbColumnType.DateTime, model.DOB);
                base.AddInParameter(cmd, "@Name", DbColumnType.String, model.Name);
                base.AddInParameter(cmd, "@AccountId", DbColumnType.String, model.Account);

                if (!base.ExecuteNonQuery(cmd))
                {
                    base.Rollback(trans);
                    conn.Close();
                    result.Message = base.Message;
                    return result;
                }

                if (1 != DataCount)
                {
                    base.Rollback(trans);
                    conn.Close();
                    result.Message = $"Update test data to FS_UNIT_TEST_USER fail.";
                    return result;
                }

                if (!base.Commit(trans))
                {
                    conn.Close();
                    result.Message = base.Message;
                    return result;
                }

                conn.Close();
            }

            result.Success = true;
            return result;
        }

        /// <summary>刪除資料 ExecuteNoeQuery 測試
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <returns>刪除結果</returns>
        internal async Task<IResult> Delete_ExecuteNonQueryAsync(UserModel model)
        {
            var result = Result.Create();

            var sql = @" DELETE FROM `FS_UNIT_TEST_USER`
                             WHERE `ACCOUNT_ID` = @AccountId; ";

            using (var conn = await base.CreateConnectionAsync())
            using (var trans = base.BeginTransaction(conn))
            {
                var cmd = GetSqlStringCommand(sql, conn, trans);
                base.AddInParameter(cmd, "@AccountId", DbColumnType.String, model.Account);

                if (!await base.ExecuteNonQueryAsync(cmd))
                {
                    base.Rollback(trans);
                    await conn.CloseAsync();
                    result.Message = base.Message;
                    return result;
                }

                if (1 != DataCount)
                {
                    base.Rollback(trans);
                    await conn.CloseAsync();
                    result.Message = $"Delete test data from FS_UNIT_TEST_USER fail.";
                    return result;
                }

                if (!base.Commit(trans))
                {
                    await conn.CloseAsync();
                    result.Message = base.Message;
                    return result;
                }

                await conn.CloseAsync();
            }

            result.Success = true;
            return result;
        }

        /// <summary>刪除資料 ExecuteNoeQuery 測試
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <returns>刪除結果</returns>
        internal IResult Delete_ExecuteNonQuery(UserModel model)
        {
            var result = Result.Create();

            var sql = @" DELETE FROM `FS_UNIT_TEST_USER`
                             WHERE `ACCOUNT_ID` = @AccountId; ";

            using (var conn = base.CreateConnection())
            using (var trans = base.BeginTransaction(conn))
            {
                var cmd = GetSqlStringCommand(sql, conn, trans);
                base.AddInParameter(cmd, "@AccountId", DbColumnType.String, model.Account);

                if (!base.ExecuteNonQuery(cmd))
                {
                    base.Rollback(trans);
                    conn.Close();
                    result.Message = base.Message;
                    return result;
                }

                if (1 != DataCount)
                {
                    base.Rollback(trans);
                    conn.Close();
                    result.Message = $"Delete test data from FS_UNIT_TEST_USER fail.";
                    return result;
                }

                if (!base.Commit(trans))
                {
                    conn.Close();
                    result.Message = base.Message;
                    return result;
                }

                conn.Close();
            }

            result.Success = true;
            return result;
        }

        /// <summary>查詢資料 LoadDataToModel 測試
        /// </summary>
        /// <param name="query">查詢條件</param>
        /// <returns>查詢結果</returns>
        internal async Task<IResult<IEnumerable<UserModel>>> Select_LoadDataToModelAsync(UserModel query)
        {
            var result = Result.Create<IEnumerable<UserModel>>();

            var sql = @"
                SELECT 
                    `ACCOUNT_ID`
                  , `NAME`
                  , `AGE`
                  , `SEX`
                  , `BIRTHDAY`

                  , `IS_VIP`
                  , `IS_GOOD`
                  , `DATA_FLAG`
                 FROM `FS_UNIT_TEST_USER`
                WHERE `SEX` = @Sex; ";

            using var conn = await base.CreateConnectionAsync();
            var cmd = base.GetSqlStringCommand(sql, conn);
            base.AddInParameter(cmd, "@Sex", DbColumnType.Int32, query.Sex);

            var r = await base.LoadDataToModelAsync<UserModel>(cmd);

            if (!r.Success)
            {
                await conn.CloseAsync();
                result.Message = base.Message;
                return result;
            }

            await conn.CloseAsync();

            var models = r.Data;

            result.Data = models;
            result.Success = true;
            return result;
        }

        /// <summary>查詢資料 LoadDataToModel 測試
        /// </summary>
        /// <param name="query">查詢條件</param>
        /// <returns>查詢結果</returns>
        internal IResult<IEnumerable<UserModel>> Select_LoadDataToModel(UserModel query)
        {
            var result = Result.Create<IEnumerable<UserModel>>();

            var sql = @"
                SELECT 
                    `ACCOUNT_ID`
                  , `NAME`
                  , `AGE`
                  , `SEX`
                  , `BIRTHDAY`

                  , `IS_VIP`
                  , `IS_GOOD`
                  , `DATA_FLAG`
                 FROM `FS_UNIT_TEST_USER`
                WHERE `SEX` = @Sex; ";

            using var conn = base.CreateConnection();
            var cmd = base.GetSqlStringCommand(sql, conn);
            base.AddInParameter(cmd, "@Sex", DbColumnType.Int32, query.Sex);

            if (!base.LoadDataToModel(cmd, out IEnumerable<UserModel> models))
            {
                conn.Close();
                result.Message = base.Message;
                return result;
            }

            conn.Close();

            result.Data = models;
            result.Success = true;
            return result;
        }

        /// <summary>查詢資料 LoadDataToModels 測試
        /// </summary>
        /// <param name="query">查詢條件</param>
        /// <returns>查詢結果</returns>
        internal async Task<IResult> Select_LoadDataToModelsAsync(UserModel query)
        {
            dynamic result = Result.CreateDynamicResult();

            var sql = @"
                -- QueryNo1
                SELECT 
                    `ACCOUNT_ID` AS |ACCOUNT_ID|
                  , `NAME`
                  , `AGE`
                  , `SEX`
                  , `BIRTHDAY` AS |BIRTHDAY|
  
                  , `IS_VIP`
                  FROM `FS_UNIT_TEST_USER`
                 WHERE `SEX` = @Sex;

                -- QueryNo2
                SELECT 
                    `ACCOUNT_ID` AS |Account|
                  , `NAME`
                  , `AGE`
                  , `SEX`
                  , `BIRTHDAY` AS |DoB|
                  FROM `FS_UNIT_TEST_USER`
                 WHERE `SEX` = @Sex; ";

            using var conn = await base.CreateConnectionAsync();
            var cmd = base.GetSqlStringCommand(sql, conn);
            base.AddInParameter(cmd, "@Sex", DbColumnType.Int32, query.Sex);

            var types = new Type[] { typeof(UserModel), typeof(UserModel) };
            var r = await base.LoadDataToModelsAsync(cmd, types);

            if (!r.Success)
            {
                await conn.CloseAsync();
                return result;
            }

            await conn.CloseAsync();

            var data = r.Data;
            var queryResult1 = data.FirstOrDefault().ToList<UserModel>();
            var queryResult2 = data.LastOrDefault().ToList<UserModel>();

            result.Rst1 = queryResult1;
            result.Rst2 = queryResult2;
            result.Success = true;
            return result;
        }

        /// <summary>查詢資料 LoadDataToModels 測試
        /// </summary>
        /// <param name="query">查詢條件</param>
        /// <returns>查詢結果</returns>
        internal IResult Select_LoadDataToModels(UserModel query)
        {
            dynamic result = Result.CreateDynamicResult();

            var sql = @"
                -- QueryNo1
                SELECT 
                    `ACCOUNT_ID` AS |ACCOUNT_ID|
                  , `NAME`
                  , `AGE`
                  , `SEX`
                  , `BIRTHDAY` AS |BIRTHDAY|
  
                  , `IS_VIP`
                  FROM `FS_UNIT_TEST_USER`
                 WHERE `SEX` = @Sex;

                -- QueryNo2
                SELECT 
                    `ACCOUNT_ID` AS |ACCOUNT_ID|
                  , `NAME`
                  , `AGE`
                  , `SEX`
                  , `BIRTHDAY` AS |BIRTHDAY|
                  FROM `FS_UNIT_TEST_USER`
                 WHERE `SEX` = @Sex; ";

            using var conn = base.CreateConnection();
            var cmd = base.GetSqlStringCommand(sql, conn);
            base.AddInParameter(cmd, "@Sex", DbColumnType.Int32, query.Sex);

            var types = new Type[] { typeof(UserModel), typeof(UserModel) };
            if (!base.LoadDataToModels(cmd, types, out var datas))
            {
                conn.Close();
                return result;
            }

            conn.Close();

            var queryResult1 = datas.FirstOrDefault().ToList<UserModel>();
            var queryResult2 = datas.LastOrDefault().ToList<UserModel>();

            result.Rst1 = queryResult1;
            result.Rst2 = queryResult2;
            result.Success = true;
            return result;
        }

        /// <summary>查詢資料，動態 ORM 繫結 LoadDataToDynamicModel 的測試
        /// </summary>
        /// <param name="query">查詢條件</param>
        /// <returns>查詢結果</returns>
        internal async Task<IResult<IEnumerable<dynamic>>> Select_LoadDataToDynamicModelAsync(UserModel query)
        {
            var result = Result.Create<IEnumerable<dynamic>>();

            var sql = @"
                SELECT 
                    `ACCOUNT_ID` AS |Account|
                  , `NAME`
                  , `AGE`
                  , `SEX`
                  , `BIRTHDAY` AS |DoB|
                  , `IS_VIP`
                  FROM `FS_UNIT_TEST_USER`
                 WHERE `SEX` = @Sex ";

            using var conn = await base.CreateConnectionAsync();
            var cmd = base.GetSqlStringCommand(sql, conn);
            base.AddInParameter(cmd, "@Sex", DbColumnType.Int32, query.Sex);

            var r = await base.LoadDataToDynamicModelAsync(cmd);

            if (!r.Success)
            {
                await conn.CloseAsync();
                result.Message = base.Message;
                return result;
            }

            await conn.CloseAsync();

            var models = r.Data;

            result.Data = models;
            result.Success = true;
            return result;
        }

        /// <summary>查詢資料，動態 ORM 繫結 LoadDataToDynamicModel 的測試
        /// </summary>
        /// <param name="query">查詢條件</param>
        /// <returns>查詢結果</returns>
        internal IResult<IEnumerable<dynamic>> Select_LoadDataToDynamicModel(UserModel query)
        {
            var result = Result.Create<IEnumerable<dynamic>>();

            var sql = @"
                SELECT 
                    `ACCOUNT_ID` AS |Account|
                  , `NAME`
                  , `AGE`
                  , `SEX`
                  , `BIRTHDAY` AS |DoB|
                  , `IS_VIP`
                  FROM `FS_UNIT_TEST_USER`
                 WHERE `SEX` = @Sex ";

            using var conn = base.CreateConnection();
            var cmd = base.GetSqlStringCommand(sql, conn);
            base.AddInParameter(cmd, "@Sex", DbColumnType.Int32, query.Sex);

            if (!base.LoadDataToDynamicModel(cmd, out var models))
            {
                conn.Close();
                result.Message = base.Message;
                return result;
            }

            conn.Close();

            result.Data = models;
            result.Success = true;
            return result;
        }

        /// <summary>查詢資料，動態 ORM 繫結多個查詢到 LoadDataToDynamicCollection 的測試
        /// </summary>
        /// <param name="query">查詢條件</param>
        /// <returns>查詢結果</returns>
        internal async Task<IResult<Dictionary<string, IEnumerable<dynamic>>>> Select_LoadDataToDynamicCollectionAsync(UserModel query)
        {
            var result = Result.Create<Dictionary<string, IEnumerable<dynamic>>>();

            var sql = @" -- QueryNo1
                            SELECT `ACCOUNT_ID` AS |Account|
                                 , `NAME`
                                 , `AGE`
                                 , `SEX`
                                 , `BIRTHDAY` AS |DoB|
                                 , `IS_VIP`
                            FROM `FS_UNIT_TEST_USER`
                                WHERE `SEX` = @Sex;

                            -- QueryNo2
                            SELECT `ACCOUNT_ID` AS |Account|
                                 , `NAME`
                                 , `AGE`
                                 , `SEX`
                                 , `BIRTHDAY` AS |DoB|
                            FROM `FS_UNIT_TEST_USER`
                                WHERE `SEX` = @Sex; ";

            using var conn = await base.CreateConnectionAsync();
            var cmd = base.GetSqlStringCommand(sql, conn);
            base.AddInParameter(cmd, "@Sex", DbColumnType.Int32, query.Sex);

            var r = await base.LoadDataToDynamicCollectionAsync(cmd, "QueryNo1", "QueryNo2");

            if (!r.Success)
            {
                await conn.CloseAsync();
                result.Message = base.Message;
                return result;
            }

            await conn.CloseAsync();

            var collects = r.Data;

            result.Data = collects;
            result.Success = true;
            return result;
        }

        /// <summary>查詢資料，動態 ORM 繫結多個查詢到 LoadDataToDynamicCollection 的測試
        /// </summary>
        /// <param name="query">查詢條件</param>
        /// <returns>查詢結果</returns>
        internal IResult<Dictionary<string, IEnumerable<dynamic>>> Select_LoadDataToDynamicCollection(UserModel query)
        {
            var result = Result.Create<Dictionary<string, IEnumerable<dynamic>>>();

            var sql = @" -- QueryNo1
                            SELECT `ACCOUNT_ID` AS |Account|
                                 , `NAME`
                                 , `AGE`
                                 , `SEX`
                                 , `BIRTHDAY` AS |DoB|
                                 , `IS_VIP`
                            FROM `FS_UNIT_TEST_USER`
                                WHERE `SEX` = @Sex;

                            -- QueryNo2
                            SELECT `ACCOUNT_ID` AS |Account|
                                 , `NAME`
                                 , `AGE`
                                 , `SEX`
                                 , `BIRTHDAY` AS |DoB|
                            FROM `FS_UNIT_TEST_USER`
                                WHERE `SEX` = @Sex; ";

            using var conn = base.CreateConnection();
            var cmd = base.GetSqlStringCommand(sql, conn);
            base.AddInParameter(cmd, "@Sex", DbColumnType.Int32, query.Sex);

            if (!base.LoadDataToDynamicCollection(cmd, out var collects, "QueryNo1", "QueryNo2"))
            {
                conn.Close();
                result.Message = base.Message;
                return result;
            }

            conn.Close();

            result.Data = collects;
            result.Success = true;
            return result;
        }

        /// <summary>查詢純量資料 ExecuteScalar 測試
        /// </summary>
        /// <returns>查詢結果</returns>
        internal async Task<IResult<int>> SelectCount_ExecuteScalarAsync()
        {
            var result = Result.Create<int>();

            var sql = @" SELECT COUNT(*) FROM `FS_UNIT_TEST_USER`; ";

            using var conn = await base.CreateConnectionAsync();
            var cmd = base.GetSqlStringCommand(sql, conn);

            var r = await base.ExecuteScalarAsync(cmd);

            if (!r.Success)
            {
                await conn.CloseAsync();
                result.Message = base.Message;
                return result;
            }

            await conn.CloseAsync();

            var count = int.Parse(r.Data + "");

            result.Data = count;
            result.Success = true;
            return result;
        }

        /// <summary>查詢純量資料 ExecuteScalar 測試
        /// </summary>
        /// <returns>查詢結果</returns>
        internal IResult<int> SelectCount_ExecuteScalar()
        {
            var result = Result.Create<int>();
            var sql = @" SELECT COUNT(*) FROM `FS_UNIT_TEST_USER`; ";

            using var conn = base.CreateConnection();
            var cmd = base.GetSqlStringCommand(sql, conn);

            if (!base.ExecuteScalar(cmd, out var obj))
            {
                conn.Close();
                result.Message = base.Message;
                return result;
            }

            conn.Close();
            var count = int.Parse(obj + "");

            result.Data = count;
            result.Success = true;
            return result;
        }

        /// <summary>清空所有測試資料
        /// </summary>
        /// <returns></returns>
        internal async Task<IResult> ClearAsync()
        {
            var result = Result.Create();
            var sql = @" DELETE FROM `{tableName}`; ";

            using var conn = await base.CreateConnectionAsync();
            var cmd = base.GetSqlStringCommand(sql.Format(tableName => "FS_UNIT_TEST_USER"), conn);

            if (!await base.ExecuteNonQueryAsync(cmd))
            {
                await conn.CloseAsync();
                result.Message = $"Clear FS_UNIT_TEST_USER table fail.";
                return result;
            }

            cmd = base.GetSqlStringCommand(sql.Format(tableName => "FS_UNIT_TEST_USER_PHONE_DETAIL"), conn);

            if (!await base.ExecuteNonQueryAsync(cmd))
            {
                await conn.CloseAsync();
                result.Message = $"Clear FS_UNIT_TEST_USER_PHONE_DETAIL table fail.";
                return result;
            }

            await conn.CloseAsync();

            result.Success = true;
            return result;
        }

        /// <summary>清空所有測試資料
        /// </summary>
        /// <returns></returns>
        internal IResult Clear()
        {
            var result = Result.Create();

            var sql = @" DELETE FROM `{tableName}`; ";

            using var conn = base.CreateConnection();
            var cmd = base.GetSqlStringCommand(sql.Format(tableName => "FS_UNIT_TEST_USER"), conn);

            if (!base.ExecuteNonQuery(cmd))
            {
                conn.Close();
                result.Message = $"Clear FS_UNIT_TEST_USER table fail.";
                return result;
            }

            cmd = base.GetSqlStringCommand(sql.Format(tableName => "FS_UNIT_TEST_USER_PHONE_DETAIL"), conn);

            if (!base.ExecuteNonQuery(cmd))
            {
                conn.Close();
                result.Message = $"Clear FS_UNIT_TEST_USER_PHONE_DETAIL table fail.";
                return result;
            }

            conn.Close();

            result.Success = true;
            return result;
        }

        #endregion 宣告測試方法
    }
}
