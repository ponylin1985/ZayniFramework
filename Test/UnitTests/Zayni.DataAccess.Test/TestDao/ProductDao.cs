using DataAccess.Test.TestModel;
using System;
using System.Linq;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.DataAccess;


namespace DataAccess.Test.TestDao
{
    /// <summary>測試用的產品資料存取類別
    /// </summary>
    internal class ProductDao : BaseDataAccess
    {
        #region Constructors

        /// <summary>預設建構子
        /// </summary>
        internal ProductDao() : base("ZayniUnitTest")
        {
        }

        #endregion Constructors


        #region Internal Methods


        /// <summary>新增產品
        /// </summary>
        /// <param name="model">產品資料模型</param>
        /// <returns>執行結果</returns>
        internal async Task<IResult> InsertProductAsync(ProductModel model)
        {
            var result = Result.Create();

            var sql = @"
                INSERT INTO `FS_UNIT_TEST_PRODUCT` (
                      `PRODUCT_ID`
                    , `PRODUCT_NAME`
                    , `PRODUCT_TYPE`
                    , `BUILD_TIME`
                ) VALUES (
                      @ProductId
                    , @ProductName
                    , @ProductType
                    , @BuildTime
                ); ";

            using (var conn = await base.CreateConnectionAsync())
            using (var trans = base.BeginTransaction(conn))
            {
                var cmd = GetSqlStringCommand(sql, conn, trans);
                base.AddInParameter(cmd, "@ProductId", DbColumnType.Guid, model.ProductId);
                base.AddInParameter(cmd, "@ProductName", DbColumnType.String, model.ProductName);
                base.AddInParameter(cmd, "@ProductType", DbColumnType.Byte, model.ProductType);
                base.AddInParameter(cmd, "@BuildTime", DbColumnType.DateTimeOffset, model.BuildTime);

                if (!await base.ExecuteNonQueryAsync(cmd))
                {
                    base.Rollback(trans);
                    await conn.CloseAsync();
                    result.Message = base.Message;
                    return result;
                }

                if (1 != DataCount)
                {
                    base.Rollback(trans);
                    await conn.CloseAsync();
                    result.Message = $"Insert into test data to FS_UNIT_TEST_PRODUCT fail.";
                    return result;
                }

                if (!base.Commit(trans))
                {
                    await conn.CloseAsync();
                    result.Message = base.Message;
                    return result;
                }

                await conn.CloseAsync();
            }

            result.Success = true;
            return result;
        }

        /// <summary>更新產品資料
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <returns>更新結果</returns>
        internal async Task<IResult> UpdateProductAsync(ProductModel model)
        {
            var result = Result.Create();

            var sql = @"
                UPDATE `FS_UNIT_TEST_PRODUCT`
                   SET `PRODUCT_NAME` = @ProductName,
                       `PRODUCT_TYPE` = @ProductType,
                       `BUILD_TIME`   = @BuildTime
                 WHERE `PRODUCT_ID`   = @ProductId; ";

            using (var conn = await base.CreateConnectionAsync())
            using (var trans = base.BeginTransaction(conn))
            {
                var cmd = GetSqlStringCommand(sql, conn, trans);
                base.AddInParameter(cmd, "@ProductId", DbColumnType.Guid, model.ProductId);
                base.AddInParameter(cmd, "@ProductName", DbColumnType.String, model.ProductName);
                base.AddInParameter(cmd, "@ProductType", DbColumnType.Byte, model.ProductType);
                base.AddInParameter(cmd, "@BuildTime", DbColumnType.DateTimeOffset, model.BuildTime);

                if (!await base.ExecuteNonQueryAsync(cmd))
                {
                    base.Rollback(trans);
                    conn.Close();
                    result.Message = base.Message;
                    return result;
                }

                if (1 != DataCount)
                {
                    base.Rollback(trans);
                    conn.Close();
                    result.Message = $"Update test data to FS_UNIT_TEST_PRODUCT fail.";
                    return result;
                }

                if (!base.Commit(trans))
                {
                    conn.Close();
                    result.Message = base.Message;
                    return result;
                }

                await conn.CloseAsync();
            }

            result.Success = true;
            return result;
        }

        /// <summary>刪除產品資料
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <returns>更新結果</returns>
        internal async Task<IResult> DeleteProductAsync(ProductModel model)
        {
            var result = Result.Create();

            var sql = @"
                DELETE `FS_UNIT_TEST_PRODUCT`
                 WHERE `PRODUCT_ID` = @ProductId; ";

            using (var conn = await base.CreateConnectionAsync())
            using (var trans = base.BeginTransaction(conn))
            {
                var cmd = GetSqlStringCommand(sql, conn, trans);
                base.AddInParameter(cmd, "@ProductId", DbColumnType.Guid, model.ProductId);

                if (!await base.ExecuteNonQueryAsync(cmd))
                {
                    base.Rollback(trans);
                    conn.Close();
                    result.Message = base.Message;
                    return result;
                }

                if (1 != DataCount)
                {
                    base.Rollback(trans);
                    conn.Close();
                    result.Message = $"Delete test data to FS_UNIT_TEST_PRODUCT fail.";
                    return result;
                }

                if (!base.Commit(trans))
                {
                    conn.Close();
                    result.Message = base.Message;
                    return result;
                }

                await conn.CloseAsync();
            }

            result.Success = true;
            return result;
        }

        /// <summary>查詢產品
        /// </summary>
        /// <param name="query">產品編號</param>
        /// <returns>查詢結果</returns>
        internal async Task<IResult<ProductModel>> GetProductAsync(Guid productId)
        {
            var result = Result.Create<ProductModel>();

            var sql = @"
                SELECT
                    `PRODUCT_ID`   |PRODUCT_ID|
                  , `PRODUCT_NAME` |PRODUCT_NAME|
                  , `PRODUCT_TYPE` |PRODUCT_TYPE|
                  , `BUILD_TIME`   |BUILD_TIME|
                  , `DATA_FLAG`    |DATA_FLAG|
                 FROM `FS_UNIT_TEST_PRODUCT`
                WHERE `PRODUCT_ID` = @ProductId; ";

            using var conn = await base.CreateConnectionAsync();
            var cmd = base.GetSqlStringCommand(sql, conn);
            base.AddInParameter(cmd, "@ProductId", DbColumnType.Guid, productId);

            var r = await base.LoadDataToModelAsync<ProductModel>(cmd);

            if (!r.Success)
            {
                await conn.CloseAsync();
                result.Message = base.Message;
                return result;
            }

            await conn.CloseAsync();
            var models = r.Data;

            result.Data = models.FirstOrDefault();
            result.Success = true;
            return result;
        }

        #endregion Internal Methods
    }
}