﻿using System;
using ZayniFramework.Caching;


namespace Caching.Test
{
    /// <summary>測試用的使用者資料模型
    /// </summary>
    internal class UserTestModel
    {
        /// <summary>姓名
        /// </summary>
        [HashProperty(Subkey = "name")]
        public string Name { get; set; }

        /// <summary>年齡
        /// </summary>
        [HashProperty(Subkey = "age")]
        public int Age { get; set; }

        /// <summary>性別
        /// </summary>
        [HashProperty(Subkey = "sex")]
        public string Sex { get; set; }

        /// <summary>帳戶餘額
        /// </summary>
        [HashProperty(Subkey = "balance", DefaultValue = 0)]
        public double Balance { get; set; }

        /// <summary>帳戶損益
        /// </summary>
        [HashProperty(Subkey = "pnl")]
        public decimal? PnL { get; set; }

        /// <summary>生日
        /// </summary>
        [HashProperty(Subkey = "dob", DefaultValue = null)]
        public DateTime? Birthday { get; set; }

        /// <summary>某個欄位
        /// </summary>
        public int Something { get; set; }
    }
}
