﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using ZayniFramework.Caching;
using ZayniFramework.Common;


namespace Caching.Test
{
    // dotnet test ./Test/UnitTests/Zayni.Caching.Test/Zayni.Caching.Test.csproj --filter ClassName=Caching.Test.MySqlMemoryCacheTester
    /// <summary>MySQLMemoryCache 快取的測試類別
    /// </summary>
    [TestClass()]
    public class MySqlMemoryCacheTester
    {
        #region 宣告私有的欄位

        /// <summary>MySqlMemory 記憶體快取
        /// </summary>
        private static MySqlMemoryCache _mysqlMemoryCache;

        #endregion 宣告私有的欄位


        #region 宣告測試初始化事件

        /// <summary>初始化測試資料
        /// </summary>
        /// <param name="context">測試環境上下文</param>
        [ClassInitialize()]
        public static void Initialize(TestContext context)
        {
            _mysqlMemoryCache = new MySqlMemoryCache(ConfigReader.GetDefaultMySqlMemoryCache());
            _mysqlMemoryCache.Clear();
        }

        /// <summary>清除測試資料
        /// </summary>
        [TestCleanup()]
        public void CleanUp() => _mysqlMemoryCache.Clear();

        #endregion 宣告測試事件


        #region 宣告測試方法

        /// <summary>MySqlMemoryCache 的資料寫入和讀取的測試
        /// </summary>
        [TestMethod()]
        [Description("MySqlMemoryCache 的資料寫入和讀取的測試")]
        public void MySqlCacheTest1()
        {
            var cacheId = "hello";

            var model = new UserTestModel
            {
                Name = "Amber",
                Age = 23,
                Sex = "Bitch"
            };

            var cacheProp = new CacheProperty()
            {
                CacheId = cacheId,
                Data = model
            };

            var success = _mysqlMemoryCache.Put(cacheProp);
            Assert.IsTrue(success);

            var g = _mysqlMemoryCache.Get<UserTestModel>(cacheId);
            Assert.IsTrue(g.Success);
            Assert.IsNotNull(g.CacheData);
            Assert.AreEqual(model.Name, g.CacheData.Name);
            Assert.AreEqual(model.Age, g.CacheData.Age);
            Assert.AreEqual(model.Sex, g.CacheData.Sex);

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, 250);
#endif
        }

        /// <summary>MySqlMemoryCache 的資料寫入和讀取的測試 2
        /// </summary>
        [TestMethod()]
        [Description("MySqlMemoryCache 的資料寫入和讀取的測試 2")]
        public void MySqlCacheTest2()
        {
            var cacheId = "hello2";

            var model = new UserTestModel
            {
                Name = "Amber",
                Age = 23,
                Sex = "Bitch"
            };

            var cacheProp = new CacheProperty()
            {
                CacheId = cacheId,
                Data = model
            };

            var success = _mysqlMemoryCache.Put(cacheProp);
            Assert.IsTrue(success);

            var g = _mysqlMemoryCache.Get<UserTestModel>(cacheId);
            Assert.IsTrue(g.Success);
            Assert.IsNotNull(g.CacheData);
            Assert.AreEqual(model.Name, g.CacheData.Name);
            Assert.AreEqual(model.Age, g.CacheData.Age);
            Assert.AreEqual(model.Sex, g.CacheData.Sex);

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, 250);
#endif
        }

        /// <summary>MySqlMemoryCache 的資料寫入和讀取的測試 3
        /// </summary>
        [TestMethod()]
        [Description("MySqlMemoryCache 的資料寫入和讀取的測試 3")]
        public void MySqlCacheTest3()
        {
            var cacheId = "hello3";

            var model = new UserTestModel
            {
                Name = "Amber",
                Age = 23,
                Sex = "Bitch"
            };

            var cacheProp = new CacheProperty()
            {
                CacheId = cacheId,
                Data = model
            };

            var success = _mysqlMemoryCache.Put(cacheProp);
            Assert.IsTrue(success);

            var g = _mysqlMemoryCache.Get<UserTestModel>(cacheId);
            Assert.IsTrue(g.Success);
            Assert.IsNotNull(g.CacheData);
            Assert.AreEqual(model.Name, g.CacheData.Name);
            Assert.AreEqual(model.Age, g.CacheData.Age);
            Assert.AreEqual(model.Sex, g.CacheData.Sex);

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, 250);
#endif
        }

        /// <summary>MySqlMemoryCache 的資料寫入和讀取的測試 4
        /// </summary>
        [TestMethod()]
        [Description("MySqlMemoryCache 的資料寫入和讀取的測試 4")]
        public void MySqlCacheTest4()
        {
            var cacheId = "hello4";

            var model = new UserTestModel
            {
                Name = "Amber",
                Age = 23,
                Sex = "Bitch"
            };

            var cacheProp = new CacheProperty()
            {
                CacheId = cacheId,
                Data = model
            };

            var success = _mysqlMemoryCache.Put(cacheProp);
            Assert.IsTrue(success);

            var g = _mysqlMemoryCache.Get<UserTestModel>(cacheId);
            Assert.IsTrue(g.Success);
            Assert.IsNotNull(g.CacheData);
            Assert.AreEqual(model.Name, g.CacheData.Name);
            Assert.AreEqual(model.Age, g.CacheData.Age);
            Assert.AreEqual(model.Sex, g.CacheData.Sex);

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, 250);
#endif
        }

        /// <summary>MySqlMemoryCache 的資料寫入和讀取的測試 5
        /// </summary>
        [TestMethod()]
        [Description("MySqlMemoryCache 的資料寫入和讀取的測試 5")]
        public void MySqlCacheTest5()
        {
            var cacheId = "hello5";

            var model = new UserTestModel
            {
                Name = "Amber",
                Age = 23,
                Sex = "Bitch"
            };

            var cacheProp = new CacheProperty()
            {
                CacheId = cacheId,
                Data = model
            };

            var success = _mysqlMemoryCache.Put(cacheProp);
            Assert.IsTrue(success);

            var g = _mysqlMemoryCache.Get<UserTestModel>(cacheId);
            Assert.IsTrue(g.Success);
            Assert.IsNotNull(g.CacheData);
            Assert.AreEqual(model.Name, g.CacheData.Name);
            Assert.AreEqual(model.Age, g.CacheData.Age);
            Assert.AreEqual(model.Sex, g.CacheData.Sex);

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, 250);
#endif
        }

        /// <summary>MySqlMemoryCache GetHashProperty 的測試
        /// </summary>
        [TestMethod()]
        [Description("MySqlMemoryCache GetHashProperty 的測試")]
        public void MySqlCache_GetHashProperty_Test()
        {
            MySqlCache_PutHashProperty_Test();

            var cacheId = "zayni.mysql.hash1";

            var r1 = _mysqlMemoryCache.GetHashProperty(cacheId, "a1");
            Assert.IsTrue(r1.Success);
            Assert.AreEqual("Fiona 費媽最好了! 人又正!", r1.Data);

            var r2 = _mysqlMemoryCache.GetHashProperty(cacheId, "a2");
            Assert.IsTrue(r2.Success);
            Assert.AreEqual(true, Convert.ToBoolean(r2.Data + ""));

            var r3 = _mysqlMemoryCache.GetHashProperty(cacheId, "a3");
            Assert.IsTrue(r3.Success);
            Assert.AreEqual(new DateTime(2018, 2, 7), Convert.ToDateTime(r3.Data + ""));

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, 250);
#endif
        }

        /// <summary>MySqlMemoryCache GetHashProperties 的測試
        /// </summary>
        [TestMethod()]
        [Description("MySqlMemoryCache GetHashProperties 的測試")]
        public void MySqlCache_GetHashProperties_Test()
        {
            MySqlCache_PutHashProperty_Test();

            var cacheId = "zayni.mysql.hash1";
            var g = _mysqlMemoryCache.GetHashProperties(cacheId);
            Assert.IsTrue(g.Success);
            Assert.IsTrue(g.Data.IsNotNullOrEmpty());

            var hashProperties = g.Data;

            var v4 = hashProperties.Where(h => h.Subkey == "a4")?.SingleOrDefault()?.Value + "";
            Assert.AreEqual(45.26D, Convert.ToDouble(v4));

            var v5 = hashProperties.Where(h => h.Subkey == "a5")?.SingleOrDefault()?.Value + "";
            Assert.AreEqual(500, Convert.ToDouble(v5));

            var v6 = hashProperties.Where(h => h.Subkey == "a6")?.SingleOrDefault()?.Value + "";
            Assert.AreEqual(3333.77M, Convert.ToDecimal(v6));

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, 250);
#endif
        }

        /// <summary>MySqlMemoryCache PutHashProperty 的測試
        /// </summary>
        [TestMethod()]
        [Description("MySqlMemoryCache PutHashProperty 的測試")]
        public void MySqlCache_PutHashProperty_Test()
        {
            var cacheId = "zayni.mysql.hash1";

            var hashs = new HashProperty[]
            {
                new( "a1", "Fiona 費媽最好了! 人又正!" ),
                new( "a2", true ),
                new( "a3", new DateTime( 2018, 2, 7 ) ),
                new( "a4", 45.26 ),
                new( "a5", 500 ),
                new( "a6", 3333.77M ),
            };

            var p1 = _mysqlMemoryCache.PutHashProperty(cacheId, hashs);
            Assert.IsTrue(p1);

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, 250);
#endif
        }

        /// <summary>MySqlMemoryCache RemoveHashProperty 的測試
        /// </summary>
        [TestMethod()]
        [Description("MySqlMemoryCache RemoveHashProperty 的測試")]
        public void MySqlCache_RemoveHashProperty_Test()
        {
            var cacheId = "zayni.mysql.hash2";

            var hashs = new HashProperty[]
            {
                new("a1", "Fiona 費媽最好了! 人又正!"),
                new("a2", true),
                new("a3", new DateTime( 2018, 2, 7 )),
                new("a4", 45.26),
                new("a5", 500),
                new("a6", 3333.77M),
            };

            var p1 = _mysqlMemoryCache.PutHashProperty(cacheId, hashs);
            Assert.IsTrue(p1);

            var exists = _mysqlMemoryCache.Contains(cacheId);
            Assert.IsTrue(exists);

            var r1 = _mysqlMemoryCache.GetHashProperty(cacheId, "a1");
            Assert.IsTrue(r1.Success);
            Assert.AreEqual("Fiona 費媽最好了! 人又正!", r1.Data);

            var r2 = _mysqlMemoryCache.GetHashProperty(cacheId, "a2");
            Assert.IsTrue(r2.Success);
            Assert.AreEqual(true, Convert.ToBoolean(r2.Data + ""));

            var d2 = _mysqlMemoryCache.RemoveHashProperty(cacheId, "a2");
            Assert.IsTrue(r2.Success);

            r2 = _mysqlMemoryCache.GetHashProperty(cacheId, "a2");
            Assert.IsFalse(r2.Success);

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, 250);
#endif
        }

        /// <summary>MySqlMemoryCache PutHashObject 的測試
        /// </summary>
        [TestMethod()]
        [Description("MySqlMemoryCache PutHashObject 的測試")]
        public void MySqlCache_PutHashObject_Test()
        {
            var cacheId = "zayni.mysql.hash3";

            var model = new UserTestModel()
            {
                Name = "AngulaBaby",
                Age = 28,
                Sex = "bitch",
                Balance = 7782343234.2345,
                Birthday = new DateTime(2000, 2, 15),
                Something = 335623
            };

            var p1 = _mysqlMemoryCache.PutHashObject(cacheId, model);
            Assert.IsTrue(p1.Success);

            var exists = _mysqlMemoryCache.Contains(cacheId);
            Assert.IsTrue(exists);

            var r1 = _mysqlMemoryCache.GetHashProperty(cacheId, "name");
            Assert.IsTrue(r1.Success);
            Assert.AreEqual(model.Name, r1.Data);

            var r2 = _mysqlMemoryCache.GetHashProperty(cacheId, "balance");
            Assert.IsTrue(r2.Success);
            Assert.AreEqual(model.Balance, Convert.ToDouble(r2.Data + ""));

            var r3 = _mysqlMemoryCache.GetHashProperty(cacheId, "pnl");
            Assert.IsTrue(r3.Success);
            Assert.IsTrue((r3.Data + "").IsNullOrEmpty());

            var p2 = _mysqlMemoryCache.PutHashProperty(cacheId, [new HashProperty("pnl", 3324.43), new HashProperty("sex", "欠幹的騷B喔!")]);
            Assert.IsTrue(p2);

            r3 = _mysqlMemoryCache.GetHashProperty(cacheId, "pnl");
            Assert.IsTrue(r3.Success);
            Assert.AreEqual(3324.43M, Convert.ToDecimal(r3.Data + ""));

            var r5 = _mysqlMemoryCache.GetHashProperty(cacheId, "sex");
            Assert.IsTrue(r5.Success);
            Assert.AreEqual("欠幹的騷B喔!", r5.Data + "");

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, 250);
#endif
        }

        /// <summary>MySqlMemoryCache GetHashObject 的測試
        /// </summary>
        [TestMethod()]
        [Description("MySqlMemoryCache GetHashObject 的測試")]
        public void MySqlCache_GetHashObject_Test()
        {
            var cacheId = "zayni.mysql.hash4";

            var model = new UserTestModel()
            {
                Name = "迪麗熱巴",
                Age = 25,
                Sex = "female",
                Balance = 3782231.245,
                Something = 335623
            };

            var p1 = _mysqlMemoryCache.PutHashObject(cacheId, model);
            Assert.IsTrue(p1.Success);

            var exists = _mysqlMemoryCache.Contains(cacheId);
            Assert.IsTrue(exists);

            var r1 = _mysqlMemoryCache.GetHashProperty(cacheId, "name");
            Assert.IsTrue(r1.Success);
            Assert.AreEqual(model.Name, r1.Data);

            var r2 = _mysqlMemoryCache.GetHashProperty(cacheId, "balance");
            Assert.IsTrue(r2.Success);
            Assert.AreEqual(model.Balance, Convert.ToDouble(r2.Data + ""));

            var r3 = _mysqlMemoryCache.GetHashProperty(cacheId, "pnl");
            Assert.IsTrue(r3.Success);
            Assert.IsTrue((r3.Data + "").IsNullOrEmpty());

            // =======================

            var g = _mysqlMemoryCache.GetHashObject<UserTestModel>(cacheId);
            Assert.IsTrue(g.Success);
            Assert.IsNotNull(g.Data);

            var m = g.Data;
            Assert.AreEqual(model.Name, m.Name);
            Assert.AreEqual(model.Age, m.Age);
            Assert.AreEqual(model.Sex, m.Sex);
            Assert.AreEqual(model.Balance, m.Balance);
            Assert.IsNull(model.PnL);

            var valString = "Ansible Playbook";
            var p2 = _mysqlMemoryCache.PutHashProperty(cacheId, [new HashProperty("pnl", 3324.43), new HashProperty("sex", valString)]);
            Assert.IsTrue(p2);

            r3 = _mysqlMemoryCache.GetHashProperty(cacheId, "pnl");
            Assert.IsTrue(r3.Success);
            Assert.AreEqual(3324.43M, Convert.ToDecimal(r3.Data + ""));

            var r5 = _mysqlMemoryCache.GetHashProperty(cacheId, "sex");
            Assert.IsTrue(r5.Success);
            Assert.AreEqual(valString, r5.Data + "");

            g = _mysqlMemoryCache.GetHashObject<UserTestModel>(cacheId);
            Assert.IsTrue(g.Success);
            Assert.IsNotNull(g.Data);

            m = g.Data;
            Assert.AreEqual(model.Name, m.Name);
            Assert.AreEqual(model.Age, m.Age);
            Assert.AreEqual(valString, m.Sex);
            Assert.AreEqual(model.Balance, m.Balance);
            Assert.AreEqual(3324.43M, m.PnL);

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, 250);
#endif
        }

        /// <summary>MySqlMemoryCache GetHashObject 的測試
        /// </summary>
        [TestMethod()]
        [Description("MySqlMemoryCache GetHashObject 的測試")]
        public void MySqlCache_GetHashObject_Test2()
        {
            var cacheId = "zayni.mysql.hash4";

            var model = new UserTestModel()
            {
                Name = "迪麗熱巴",
                Age = 25,
                Sex = "female",
                PnL = 332.41M
            };

            var p1 = _mysqlMemoryCache.PutHashObject(cacheId, model);
            Assert.IsTrue(p1.Success);

            var exists = _mysqlMemoryCache.Contains(cacheId);
            Assert.IsTrue(exists);

            var r1 = _mysqlMemoryCache.GetHashProperty(cacheId, "name");
            Assert.IsTrue(r1.Success);
            Assert.AreEqual(model.Name, r1.Data);

            var r2 = _mysqlMemoryCache.GetHashProperty(cacheId, "balance");
            Assert.IsTrue(r2.Success);
            Assert.AreEqual(0, Convert.ToDouble(r2.Data + ""));

            var r3 = _mysqlMemoryCache.GetHashProperty(cacheId, "pnl");
            Assert.IsTrue(r3.Success);
            Assert.AreEqual(332.41M, Convert.ToDecimal(r3.Data + ""));

            // =======================

            var g = _mysqlMemoryCache.GetHashObject<UserTestModel>(cacheId);
            Assert.IsTrue(g.Success);
            Assert.IsNotNull(g.Data);

            var m = g.Data;
            Assert.AreEqual(model.Name, m.Name);
            Assert.AreEqual(model.Age, m.Age);
            Assert.AreEqual(model.Sex, m.Sex);
            Assert.AreEqual(0, m.Balance);
            Assert.AreEqual(model.PnL, m.PnL);

#if DEBUG
            System.Threading.SpinWait.SpinUntil(() => false, 250);
#endif
        }

        #endregion 宣告測試方法
    }
}
