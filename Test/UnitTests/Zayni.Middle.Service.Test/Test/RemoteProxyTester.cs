﻿// 2024-03-09 發現目前 Linode 上的 K8S pod 有問題，無法正常運作，因此暫時無法進行測試。
// using Microsoft.VisualStudio.TestTools.UnitTesting;
// using ServiceHost.Client.Proxy;
// using ServiceHost.Test.Entity;
// using System;
// using System.Diagnostics;
// using System.IO;
// using System.Reflection;
// using System.Threading.Tasks;
// using ZayniFramework.Common;
// using ZayniFramework.Logging;
// using ZayniFramework.Middle.Service.Client;
// using ZayniFramework.Middle.Service.Grpc.Client;
// using ZayniFramework.Middle.Service.HttpCore.Client;
// using ZayniFramework.Serialization;


// namespace Middle.Service.Tester
// {
//     // dotnet test ./Test/UnitTests/Zayni.Middle.Service.Test/Zayni.Middle.Service.Test.csproj --filter ClassName=Middle.Service.Tester.RemoteProxyTester
//     /// <summary>ZayniFramework 框架 Logger 日誌器的測試類別
//     /// </summary>
//     [TestClass()]
//     public class RemoteProxyTester
//     {
//         #region Private Fields

//         /// <summary>RemoteProxy 在 runtime 下的 serviceClientConfig.json 設定檔的完整路徑。
//         /// </summary>
//         private static string _serviceClientConfigPath;

//         #endregion Private Fields


//         #region 測試組件初始化

//         /// <summary>初始化整個 Test project
//         /// </summary>
//         /// <param name="testcontext"></param>
//         [AssemblyInitialize()]
//         public static void Init(TestContext testcontext)
//         {
//             // 在 .NET Core 版本的 MSTest，一定要自行指定 .config 的完整絕對路徑到 ConfigManagement.ConfigFullPath 中，整個 ZayniFramework 舊有在 XML Config 的機制才會正常運作！
//             var execPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
//             var path = $"{execPath}/zayni.json";
//             ConfigManager.Init(path);
//             _serviceClientConfigPath = $"{execPath}/serviceClientConfig.json";

//             RemoteClientFactory.RegisterRemoteClientType<gRPCRemoteClient>("gRPC");
//             RemoteClientFactory.RegisterRemoteClientType<HttpCoreRemoteClient>("HttpCore");
//         }

//         #endregion 測試組件初始化


//         /// <summary>RemoteProxy 遠端代理人執行 RPC 請求的測試
//         /// </summary>
//         [TestMethod()]
//         [Description("RemoteProxy 遠端代理人執行 RPC 請求的測試")]
//         public async Task RemoteProxy_RPC_Test()
//         {
//             var proxy = MyProxy.GetInstance(_serviceClientConfigPath);
//             var random = new Random(Guid.NewGuid().GetHashCode());
//             Assert.IsNotNull(proxy);

//             for (var i = 0; i < 5; i++)
//             {
//                 var request = new SomethingDTO()
//                 {
//                     SomeMessage = "From RemoteProxyTester unit test.",
//                     LuckyNumber = random.Next(0, 2000)
//                 };

//                 var r = await proxy.ExecuteAsync<SomethingDTO, MagicDTO>("FooActionAsync", request);
//                 var json = NewtonsoftJsonConvert.Serialize(r);
//                 await ConsoleLogger.LogAsync($"RemoteProxy_RPC_Test.FooActionAsync response.{Environment.NewLine}{json}");
//                 Debug.Print($"RemoteProxy_RPC_Test.FooActionAsync response.{Environment.NewLine}{json}");

//                 Assert.IsTrue(r.Success);
//                 Assert.IsTrue(r.Data.IsNotNull());

//                 await Task.Delay(50);
//             }

//             await Task.Delay(200);

//             // var reqCreate1 = new UserDTO()
//             // {
//             //     Name    = $"UnitTest-{RandomTextHelper.Create( 10 )}",
//             //     Sex     = 0,
//             //     Age     = 25,
//             //     DoB     = new DateTime( 1998, 3, 27 ),
//             //     IsGood  = true,
//             //     IsVip   = true
//             // };

//             // var reqCreate2 = new UserDTO()
//             // {
//             //     Name    = $"UnitTest-{RandomTextHelper.Create( 10 )}",
//             //     Sex     = 0,
//             //     Age     = 27,
//             //     DoB     = new DateTime( 1992, 3, 27 ),
//             //     IsGood  = false,
//             //     IsVip   = true
//             // };

//             // var reqCreate3 = new UserDTO()
//             // {
//             //     Name    = $"UnitTest-{RandomTextHelper.Create( 10 )}",
//             //     Sex     = 1,
//             //     Age     = 64,
//             //     DoB     = new DateTime( 1953, 6, 14 ),
//             //     IsGood  = true,
//             //     IsVip   = false
//             // };

//             // for ( int i = 0; i < 1; i++ )
//             // {
//             //     var r = await proxy.ExecuteAsync<UserDTO, UserDTO>( "CreateUserModelActionAsync", reqCreate1 );
//             //     var json = JsonConvertUtil.Serialize( r );
//             //     await ConsoleLogger.LogAsync( $"RemoteProxy_RPC_Test.CreateUserModelActionAsync1 response.{Environment.NewLine}{json}" );
//             //     Debug.Print( $"RemoteProxy_RPC_Test.CreateUserModelActionAsync1 response.{Environment.NewLine}{json}" );
//             //     Assert.IsTrue( r.Success );
//             //     Assert.IsTrue( r.Data.IsNotNull() );

//             //     r = await proxy.ExecuteAsync<UserDTO, UserDTO>( "CreateUserModelActionAsync", reqCreate2 );
//             //     json = JsonConvertUtil.Serialize( r );
//             //     await ConsoleLogger.LogAsync( $"RemoteProxy_RPC_Test.CreateUserModelActionAsync2 response.{Environment.NewLine}{json}" );
//             //     Debug.Print( $"RemoteProxy_RPC_Test.CreateUserModelActionAsync2 response.{Environment.NewLine}{json}" );
//             //     Assert.IsTrue( r.Success );
//             //     Assert.IsTrue( r.Data.IsNotNull() );

//             //     r = await proxy.ExecuteAsync<UserDTO, UserDTO>( "CreateUserModelActionAsync", reqCreate3 );
//             //     json = JsonConvertUtil.Serialize( r );
//             //     await ConsoleLogger.LogAsync( $"RemoteProxy_RPC_Test.CreateUserModelActionAsync3 response.{Environment.NewLine}{json}" );
//             //     Debug.Print( $"RemoteProxy_RPC_Test.CreateUserModelActionAsync3 response.{Environment.NewLine}{json}" );
//             //     Assert.IsTrue( r.Success );
//             //     Assert.IsTrue( r.Data.IsNotNull() );

//             //     await Task.Delay( 100 );
//             // }

//             // for ( int i = 0; i < 3; i++ )
//             // {
//             //     var request = new UserDTO()
//             //     {
//             //         Sex = 0
//             //     };

//             //     var r = await proxy.GetUserModelsAsync( request );
//             //     var json = JsonConvertUtil.Serialize( r );

//             //     await ConsoleLogger.LogAsync( $"RemoteProxy_RPC_Test.GetUserModelsActionAsync response.{Environment.NewLine}{json}" );
//             //     Debug.Print( $"RemoteProxy_RPC_Test.GetUserModelsActionAsync response.{Environment.NewLine}{json}" );

//             //     Assert.IsTrue( r.Success );
//             //     Assert.IsTrue( r.Data.IsNotNull() );

//             //     await Task.Delay( 50 );
//             // }

//             // var q1    = await proxy.ExecuteAsync<UserDTO, UserDTO>( "GetUserModelActionAsync", new UserDTO() { Name = reqCreate1.Name } );
//             // var json1 = JsonConvertUtil.Serialize( q1 );
//             // await ConsoleLogger.LogAsync( $"RemoteProxy_RPC_Test.GetUserModelActionAsync1 response.{Environment.NewLine}{json1}" );
//             // Debug.Print( $"RemoteProxy_RPC_Test.GetUserModelActionAsync1 response.{Environment.NewLine}{json1}" );
//             // Assert.IsTrue( q1.Success );
//             // Assert.IsNotNull( q1.Data );
//             // await Task.Delay( 50 );

//             // var q2    = await proxy.ExecuteAsync<UserDTO, UserDTO>( "GetUserModelActionAsync", new UserDTO() { Name = reqCreate2.Name } );
//             // var json2 = JsonConvertUtil.Serialize( q2 );
//             // await ConsoleLogger.LogAsync( $"RemoteProxy_RPC_Test.GetUserModelActionAsync2 response.{Environment.NewLine}{json2}" );
//             // Debug.Print( $"RemoteProxy_RPC_Test.GetUserModelActionAsync2 response.{Environment.NewLine}{json2}" );
//             // Assert.IsTrue( q2.Success );
//             // Assert.IsNotNull( q2.Data );
//             // await Task.Delay( 50 );

//             // var q3    = await proxy.ExecuteAsync<UserDTO, UserDTO>( "GetUserModelActionAsync", new UserDTO() { Name = reqCreate3.Name } );
//             // var json3 = JsonConvertUtil.Serialize( q3 );
//             // await ConsoleLogger.LogAsync( $"RemoteProxy_RPC_Test.GetUserModelActionAsync3 response.{Environment.NewLine}{json3}" );
//             // Debug.Print( $"RemoteProxy_RPC_Test.GetUserModelActionAsync3 response.{Environment.NewLine}{json3}" );
//             // Assert.IsTrue( q3.Success );
//             // Assert.IsNotNull( q3.Data );
//             // await Task.Delay( 50 );

//             // var d1 = await proxy.ExecuteAsync<UserDTO>( "DeleteUserModelActionAsync", new UserDTO() { Account = q1.Data.Account } );
//             // Assert.IsTrue( d1.Success );

//             // var d2 = await proxy.ExecuteAsync<UserDTO>( "DeleteUserModelActionAsync", new UserDTO() { Account = q2.Data.Account } );
//             // Assert.IsTrue( d2.Success );

//             // var d3 = await proxy.ExecuteAsync<UserDTO>( "DeleteUserModelActionAsync", new UserDTO() { Account = q3.Data.Account } );
//             // Assert.IsTrue( d3.Success );

//             await Task.Delay(100);
//         }
//     }
// }
