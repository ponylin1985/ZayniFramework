// 2024-03-09 發現目前 Linode 上的 K8S pod 有問題，無法正常運作，因此暫時無法進行測試。
// using FluentAssertions;
// using Microsoft.Extensions.DependencyInjection;
// using Microsoft.VisualStudio.TestTools.UnitTesting;
// using NSubstitute;
// using ServiceHost.Service;
// using ServiceHost.Service.DataAccess;
// using ServiceHost.Test.Entity;
// using System;
// using System.Collections.Generic;
// using System.Threading.Tasks;
// using ZayniFramework.Common;
// using ZayniFramework.DataAccess.Lightweight;
// using ZayniFramework.Middle.Service.Entity;


// namespace Middle.Service.Tester
// {
//     // dotnet test ./Test/UnitTests/Zayni.Middle.Service.Test/Zayni.Middle.Service.Test.csproj --filter ClassName=Middle.Service.Tester.UserServiceTester
//     /// <summary>使用者服務測試類別
//     /// </summary>
//     [TestClass()]
//     public class UserServiceTester
//     {
//         #region Private Fields

//         /// <summary>模擬測試用的 UserDTO 請求資料載體
//         /// </summary>
//         private UserDTO _fakeRequestDTO;

//         /// <summary>模擬的使用者資料存取物件
//         /// </summary>
//         private IUserDao _fakeUserDao;

//         /// <summary>模擬測試用的 UserModel 資料模型
//         /// </summary>
//         private UserModel _fakeRequest;

//         /// <summary>模擬測試用的查詢 UserModel 資料集合
//         /// </summary>
//         private IEnumerable<UserModel> _fakeUserModels;

//         /// <summary>待測服務
//         /// </summary>
//         private UserService _service;

//         /// <summary>查詢使用者資料集合的結果
//         /// </summary>
//         private IResult<IEnumerable<UserDTO>> _getUsersResult;

//         /// <summary>查詢使用者資料的結果
//         /// </summary>
//         private IResult<UserDTO> _getUserResult;

//         #endregion Private Fields


//         #region Test Methods

//         /// <summary>查詢使用者資料集合測試。<para/>
//         /// * 當查詢女性使用者的資料集合測試。<para/>
//         /// * 應該正常回傳女性的資料集合
//         /// </summary>
//         [TestMethod()]
//         public void GivenFemaleSex_WhenGetUsersAsync_ThenShouldRetriveFemaleModels()
//         {
//             GivenFemaleSex();
//             WhenGetUsersAsync();
//             ThenShouldRetriveFemaleModels();
//         }

//         /// <summary>查詢使用者資料集合測試。<para/>
//         /// * 給定錯誤值域的 Sex 查詢參數。<para/>
//         /// * 應該回應查詢的 Sex 參數值域錯誤。
//         /// </summary>
//         [TestMethod()]
//         public void GivenInvalidSex_WhenGetUsersAsync_ThenShouldRetriveSexInvalidHints()
//         {
//             GivenInvalidSex();
//             WhenGetUsersAsync();
//             ThenShouldRetriveSexInvalidHints();
//         }

//         /// <summary>查詢使用者明細資料<para/>
//         /// * 當查詢存在的使用者帳號明細資料。<para/>
//         /// * 應該正常回應使用者帳號明細資料。
//         /// </summary>
//         [TestMethod()]
//         public void GivenExistedUserAccount_WhenGetUserAsync_ThenShouldRetriveUserDetail()
//         {
//             GivenExistedUserAccount();
//             WhenGetUserAsync();
//             ThenShouldRetriveUserDetail();
//         }

//         /// <summary>查詢使用者明細資料<para/>
//         /// * 當查詢不存在的使用者帳號明細資料。<para/>
//         /// * 應該正常回應使用者明細查無資料。
//         /// </summary>
//         [TestMethod()]
//         public void GivenNotExistedUserAccount_WhenGetUserAsync_ThenShouldRetriveNoUserDataFound()
//         {
//             GivenNotExistedUserAccount();
//             WhenGetUserAsync();
//             ThenShouldRetriveNoUserDataFound();
//         }

//         #endregion Test Methods


//         #region Private Arrange Methods

//         /// <summary>當給定 Sex = 0 的查詢參數
//         /// </summary>
//         private void GivenFemaleSex()
//         {
//             var fakeSex = 0;

//             _fakeRequestDTO = new UserDTO
//             {
//                 Sex = fakeSex
//             };

//             _fakeRequest = new UserModel
//             {
//                 Sex = fakeSex
//             };

//             _fakeUserModels = new List<UserModel>
//             {
//                 new() {
//                     Account = Guid.NewGuid().ToString( "n" )[..10 ],
//                     Name    = "Judy",
//                     Sex     = 0,
//                     Age     = 18,
//                 },
//                 new() {
//                     Account = Guid.NewGuid().ToString( "n" )[..10 ],
//                     Name    = "Tina",
//                     Sex     = 0,
//                     Age     = 20,
//                 },
//                 new() {
//                     Account = Guid.NewGuid().ToString( "n" )[..10 ],
//                     Name    = "Gina",
//                     Sex     = 0,
//                     Age     = 21,
//                 },
//             };

//             _fakeUserDao = Substitute.For<IUserDao>();
//             _fakeUserDao.GetUsersAsync(Arg.Is<UserModel>(q => q.Sex == fakeSex)).Returns(
//                 Task.FromResult<IResult<IEnumerable<UserModel>>>(Result.Create(true, _fakeUserModels)));

//             ServiceLocator.ClearServices();
//             ServiceLocator.ConfigureServices(s => s.AddScoped<IUserDao>(z => _fakeUserDao));
//         }

//         /// <summary>當給定錯誤值域的 Sex 查詢參數
//         /// </summary>
//         private void GivenInvalidSex()
//         {
//             _fakeRequestDTO = new UserDTO
//             {
//                 Sex = 500   // Invalid value here.
//             };

//             _fakeUserDao = Substitute.For<IUserDao>();
//             ServiceLocator.ClearServices();
//             ServiceLocator.ConfigureServices(s => s.AddScoped<IUserDao>(z => _fakeUserDao));
//         }

//         /// <summary>當給定存在的使用者帳號
//         /// </summary>
//         private void GivenExistedUserAccount()
//         {
//             var account = "Tiffany";

//             _fakeRequestDTO = new UserDTO
//             {
//                 Account = account,
//                 Name = account,
//             };

//             var fakeUserModel = new UserModel
//             {
//                 Account = account,
//                 Name = account,
//                 Sex = 0,
//                 Age = 18,
//             };

//             _fakeUserDao = Substitute.For<IUserDao>();
//             _fakeUserDao.GetAsync(Arg.Is<UserModel>(q => q.Account == _fakeRequestDTO.Account)).Returns(Task.FromResult<IResult<UserModel>>(Result.Create(true, fakeUserModel)));

//             ServiceLocator.ClearServices();
//             ServiceLocator.ConfigureServices(s => s.AddScoped<IUserDao>(z => _fakeUserDao));
//         }

//         /// <summary>當給定不存在的使用者帳號
//         /// </summary>
//         private void GivenNotExistedUserAccount()
//         {
//             var account = "Nancy";

//             _fakeRequestDTO = new UserDTO
//             {
//                 Account = account,
//                 Name = account,
//             };

//             _fakeUserDao = Substitute.For<IUserDao>();
//             _fakeUserDao.GetAsync(Arg.Is<UserModel>(q => q.Account == _fakeRequestDTO.Account)).Returns(Task.FromResult<IResult<UserModel>>(Result.Create<UserModel>(true, code: StatusConstant.NO_DATA_FOUND, message: "No data found.")));

//             ServiceLocator.ClearServices();
//             ServiceLocator.ConfigureServices(s => s.AddScoped<IUserDao>(z => _fakeUserDao));
//         }

//         #endregion Private Arrange Methods


//         #region Private Act Methods

//         /// <summary>執行查詢使用者資料集合
//         /// </summary>
//         private void WhenGetUsersAsync()
//         {
//             _service = new UserService();
//             _getUsersResult = _service.GetUsersAsync(_fakeRequestDTO).ConfigureAwait(false).GetAwaiter().GetResult();
//         }

//         /// <summary>執行查詢使用者資料
//         /// </summary>
//         private void WhenGetUserAsync()
//         {
//             _service = new UserService();
//             _getUserResult = _service.GetAsync(_fakeRequestDTO).ConfigureAwait(false).GetAwaiter().GetResult();
//         }

//         #endregion Private Act Methods


//         #region Private Assert Methods

//         /// <summary>檢查查詢 Sex = 0 條件下查詢使用者資料集合的結果
//         /// </summary>
//         private void ThenShouldRetriveFemaleModels()
//         {
//             _getUsersResult.Success.Should().BeTrue();
//             _getUsersResult.Data.Should().NotBeNullOrEmpty();
//             _getUsersResult.Data.Should().OnlyContain(m => m.Sex == 0);
//             _fakeUserDao.Received(1).GetUsersAsync(Arg.Is<UserModel>(q => q.Sex == _fakeRequest.Sex));
//         }

//         /// <summary>檢查查詢使用者資料集合是否回應正確的錯誤代碼與訊息
//         /// </summary>
//         private void ThenShouldRetriveSexInvalidHints()
//         {
//             _getUsersResult.Success.Should().BeFalse();
//             _getUsersResult.Code.Should().Be(StatusCode.INVALID_REQUEST);
//             _getUsersResult.Message.ToLowerInvariant().Should().Contain("invalid request");
//             _getUsersResult.Message.ToLowerInvariant().Should().Contain("sex");
//             _getUsersResult.Data.Should().BeNullOrEmpty();
//             _fakeUserDao.ReceivedWithAnyArgs(0).GetUsersAsync(Arg.Any<UserModel>());
//         }

//         /// <summary>檢查是否正常回應使用者明細資料
//         /// </summary>
//         private void ThenShouldRetriveUserDetail()
//         {
//             _getUserResult.Success.Should().BeTrue();
//             _getUserResult.Data.Should().NotBeNull();
//             _getUserResult.Data.Account.Should().Be(_fakeRequestDTO.Account);
//             _getUserResult.Data.Name.Should().Be(_fakeRequestDTO.Name);
//             _getUserResult.Data.Sex.Should().Be(0);
//             _getUserResult.Data.Age.Should().Be(18);
//             _fakeUserDao.ReceivedWithAnyArgs(1).GetAsync(Arg.Is<UserModel>(q => q.Account == _fakeRequestDTO.Account));
//         }

//         /// <summary>檢查是否正常回應使用者查無資料
//         /// </summary>
//         private void ThenShouldRetriveNoUserDataFound()
//         {
//             _getUserResult.Success.Should().BeTrue();
//             _getUserResult.Data.Should().BeNull();
//             _getUserResult.Code.Should().Be(StatusConstant.NO_DATA_FOUND);
//             _getUserResult.Message.ToLowerInvariant().Should().Contain("no data found");
//             _fakeUserDao.ReceivedWithAnyArgs(1).GetAsync(Arg.Is<UserModel>(q => q.Account == _fakeRequestDTO.Account));
//         }

//         #endregion Private Assert Methods
//     }
// }