﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZayniFramework.Common;
using ZayniFramework.Serialization;


namespace Serialization.Test
{
    /// <summary>NewtonsoftJsonConvertTester 的測試類別
    /// </summary>
    [TestClass()]
    public class NewtonsoftJsonConvertTester
    {
        /// <summary>CamelCase 並且 UNIX Timestamp 的 JSON 序列化測試
        /// </summary>
        [TestMethod()]
        [TestCategory("JsonConvertUtil - JSON 序列化/反序列化測試")]
        [Description("UNIX Timestamp 的 JSON 序列化測試")]
        public void Serialize_Test()
        {
            var model = new UserModel()
            {
                Name = "Test",
                Birthday = new DateTime(1998, 2, 14, 15, 24, 36).AddMilliseconds(257),
                Sex = 0
            };

            var json = NewtonsoftJsonConvert.Serialize(model);
            Assert.IsTrue(json.IsNotNullOrEmpty());
            Assert.IsTrue(NewtonsoftJsonConvert.IsValidJson(json));

            var obj = NewtonsoftJsonConvert.DeserializeFromCamelCase<UserModel>(json);
            Assert.IsNotNull(obj);
            Assert.AreEqual(model.Name, obj.Name);
            Assert.AreEqual(model.Birthday, obj.Birthday);
            Assert.AreEqual(model.Sex, obj.Sex);

            var obj2 = NewtonsoftJsonConvert.Deserialize<UserModel>(json);
            Assert.IsNotNull(obj2);
            Assert.AreEqual(model.Name, obj2.Name);
            Assert.AreEqual(model.Birthday, obj2.Birthday);
            Assert.AreEqual(model.Sex, obj2.Sex);
        }

        /// <summary>CamelCase 並且 UNIX Timestamp 的 JSON 序列化測試
        /// </summary>
        [TestMethod()]
        [TestCategory("JsonConvertUtil - JSON 序列化/反序列化測試")]
        [Description("CamelCase 並且 UNIX Timestamp 的 JSON 序列化測試")]
        public void SerializeInCamelCase_Test()
        {
            var model = new UserModel()
            {
                Name = "Test",
                Birthday = new DateTime(1998, 2, 14, 15, 24, 36).AddMilliseconds(257),
                Sex = 0
            };

            var json = NewtonsoftJsonConvert.SerializeInCamelCase(model);
            Assert.IsTrue(json.IsNotNullOrEmpty());
            Assert.IsTrue(NewtonsoftJsonConvert.IsValidJson(json));

            var obj = NewtonsoftJsonConvert.DeserializeFromCamelCase<UserModel>(json);
            Assert.IsNotNull(obj);
            Assert.AreEqual(model.Name, obj.Name);
            Assert.AreEqual(model.Birthday, obj.Birthday);
            Assert.AreEqual(model.Sex, obj.Sex);

            var obj2 = NewtonsoftJsonConvert.Deserialize<UserModel>(json);
            Assert.IsNotNull(obj2);
            Assert.AreEqual(model.Name, obj2.Name);
            Assert.AreEqual(model.Birthday, obj2.Birthday);
            Assert.AreEqual(model.Sex, obj2.Sex);
        }

        /// <summary>CamelCase 並且 UNIX Timestamp 的 JSON 序列化測試
        /// </summary>
        [TestMethod()]
        [TestCategory("JsonConvertUtil - JSON 序列化/反序列化測試")]
        [Description("UNIX Timestamp 的 JSON 序列化測試")]
        public void Serialize_InTimestamp_Test()
        {
            var model = new UserModel()
            {
                Name = "Test",
                Birthday = new DateTime(1998, 2, 14, 15, 24, 36).AddMilliseconds(257),
                Sex = 0
            };

            var json = NewtonsoftJsonConvert.Serialize(model, true);
            Assert.IsTrue(json.IsNotNullOrEmpty());
            Assert.IsTrue(NewtonsoftJsonConvert.IsValidJson(json));

            var obj = NewtonsoftJsonConvert.DeserializeFromCamelCase<UserModel>(json, true);
            Assert.IsNotNull(obj);
            Assert.AreEqual(model.Name, obj.Name);
            Assert.AreEqual(model.Birthday, obj.Birthday);
            Assert.AreEqual(model.Sex, obj.Sex);

            var obj2 = NewtonsoftJsonConvert.Deserialize<UserModel>(json, true);
            Assert.IsNotNull(obj2);
            Assert.AreEqual(model.Name, obj2.Name);
            Assert.AreEqual(model.Birthday, obj2.Birthday);
            Assert.AreEqual(model.Sex, obj2.Sex);
        }

        /// <summary>CamelCase 並且 UNIX Timestamp 的 JSON 序列化測試
        /// </summary>
        [TestMethod()]
        [TestCategory("JsonConvertUtil - JSON 序列化/反序列化測試")]
        [Description("CamelCase 並且 UNIX Timestamp 的 JSON 序列化測試")]
        public void SerializeInCamelCase_InTimestamp_Test()
        {
            var model = new UserModel()
            {
                Name = "Test",
                Birthday = new DateTime(1998, 2, 14, 15, 24, 36),
                Sex = 0
            };

            var json = NewtonsoftJsonConvert.SerializeInCamelCase(model, false);
            Assert.IsTrue(json.IsNotNullOrEmpty());
            Assert.IsTrue(NewtonsoftJsonConvert.IsValidJson(json));

            var obj = NewtonsoftJsonConvert.DeserializeFromCamelCase<UserModel>(json, false);
            Assert.IsNotNull(obj);
            Assert.AreEqual(model.Name, obj.Name);
            Assert.AreEqual(model.Birthday, obj.Birthday);
            Assert.AreEqual(model.Sex, obj.Sex);

            var obj2 = NewtonsoftJsonConvert.Deserialize<UserModel>(json, false);
            Assert.IsNotNull(obj2);
            Assert.AreEqual(model.Name, obj2.Name);
            Assert.AreEqual(model.Birthday, obj2.Birthday);
            Assert.AreEqual(model.Sex, obj2.Sex);
        }
    }
}
