﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Serialization.Test
{
    /// <summary>測試用的使用者資料模型
    /// </summary>
    public class UserModel
    {
        /// <summary>使用者姓名
        /// </summary>
        [JsonPropertyName("name")]
        public virtual string Name { get; set; }

        /// <summary>使用者生日
        /// </summary>
        [JsonPropertyName("birthday")]
        public virtual DateTime Birthday { get; set; }

        /// <summary>使用者性別
        /// </summary>
        [JsonPropertyName("sex")]
        public virtual int Sex { get; set; }

        /// <summary>帳戶現金餘額
        /// </summary>
        [JsonPropertyName("cash_balance")]
        public virtual double CashBalance { get; set; }

        /// <summary>總共有多少小孩
        /// </summary>
        [JsonPropertyName("total_kids")]
        public virtual decimal TotalKids { get; set; }

        /// <summary>描述
        /// </summary>
        /// <value></value>
        [JsonPropertyName("description")]
        public virtual string Description { get; set; }

        /// <summary>應該要忽略序列化此欄位
        /// </summary>
        /// <value></value>
        [JsonIgnore()]
        public virtual string IgnoreThis { get; set; }
    }

    /// <summary>測試用的使用者資料模型
    /// </summary>
    public class User
    {
        /// <summary>使用者姓名
        /// </summary>
        public string Name { get; set; }

        /// <summary>使用者生日
        /// </summary>
        public DateTime Birthday { get; set; }

        /// <summary>使用者性別
        /// </summary>
        public int Sex { get; set; }

        /// <summary>擁有的房屋
        /// </summary>
        /// <value></value>
        public IEnumerable<House> Houses { get; set; }
    }

    /// <summary>房屋
    /// </summary>
    public class House
    {
        /// <summary>所在城市名稱
        /// </summary>
        /// <value></value>
        public string City { get; set; }

        /// <summary>電話號碼
        /// </summary>
        /// <value></value>
        public string TelNumber { get; set; }

        /// <summary>地址
        /// </summary>
        /// <value></value>
        public string Address { get; set; }
    }
}
