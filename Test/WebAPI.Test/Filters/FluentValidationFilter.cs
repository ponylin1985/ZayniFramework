using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Linq;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Middle.Service.Entity;


namespace WebAPI.Test
{
    /// <summary>請求資料驗證器
    /// </summary>
    public class FluentValidationFilter : IAsyncActionFilter
    {
        /// <summary>服務供應容器
        /// </summary>
        private readonly IServiceProvider _serviceProvider;

        /// <summary>多載建構子
        /// </summary>
        /// <param name="serviceProvider">服務供應容器</param>
        public FluentValidationFilter(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        /// <summary>執行動作前的請求資料驗證
        /// </summary>
        /// <param name="context"></param>
        /// <param name="next"></param>
        /// <returns></returns>
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var requestParameter = context.ActionArguments.FirstOrDefault(a => a.Value.IsNotNull());

            if (requestParameter.Value.IsNotNull())
            {
                var requestType = requestParameter.Value.GetType();
                var validatorType = typeof(IValidator<>).MakeGenericType(requestType);

                if (_serviceProvider.GetService(validatorType) is IValidator validator)
                {
                    var validationResult = validator.Validate(new ValidationContext<object>(requestParameter.Value));

                    if (!validationResult.IsValid)
                    {
                        var errors =
                            validationResult
                                .Errors
                                .Select(e => new InvalidMetadata { Field = e.PropertyName, Message = e.ErrorMessage })
                                .ToArray();

                        var response = new OkObjectResult(
                            Result.Create(false,
                                data: errors,
                                code: StatusCode.INVALID_REQUEST,
                                message: "Invalid request."
                        ));

                        context.Result = response;
                        return;
                    }
                }
            }

            await next();
        }
    }

    /// <summary>不合法的欄位的中介資料
    /// </summary>
    public class InvalidMetadata
    {
        /// <summary>不合法的欄位名稱
        /// </summary>
        /// <value></value>
        public string Field { get; set; }

        /// <summary>不合法的原因
        /// </summary>
        /// <value></value>
        public string Message { get; set; }
    }
}