using Microsoft.AspNetCore.Mvc;
using ServiceHost.Client.Proxy;
using ServiceHost.Test.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;
using ZayniFramework.Common;


namespace WebAPI.Test.Controllers
{
    /// <summary>測試用的 UserController
    /// </summary>
    [Route("users")]
    [ServiceFilter(typeof(FluentValidationFilter))]
    [ApiController()]
    public class UserController
    {
        #region Private Fields

        /// <summary>服務代理人物件<para/>
        /// 使用服務代理人 Proxy 執行 ServiceAction 的最大其中一個優勢就是:<para/>
        /// 當 InProcess 的 ServiceAction 要採用 Microservice 架構時，拆出去變成一個外部獨立的 microservice 的時候，<para/>
        /// 這邊呼叫端的程式碼是「可以完全不用」任何調整!!! 直接改 serviceClientConfig.json 的 remoteHostType 設定值就搞定了。<para/>
        /// 這才是真正把一些切都抽象化的威力!!!
        /// </summary>
        private static readonly MyProxy _proxy = MyProxy.GetInstance("./conf/serviceClientConfig.json");

        #endregion Private Fields


        #region Public Methods

        /// <summary>取得使用者資料模型列表
        /// * GET /users?sex={sex}
        /// </summary>
        /// <param name="request">查詢參數</param>
        /// <returns>查詢結果</returns>
        [HttpGet()]
        public async Task<IResult<IEnumerable<UserDTO>>> GetUsersAsync([FromQuery(Name = "sex")] int? sex = 0) =>
            await _proxy.ExecuteAsync<UserDTO, IEnumerable<UserDTO>>("GetUserModelsActionAsync", new UserDTO() { Sex = (int)sex });

        /// <summary>取得使用者資料模型列表
        /// </summary>
        /// <param name="account">使用者帳號</param>
        /// <returns>查詢結果</returns>
        [HttpGet("{account}")]
        public async Task<IResult<UserDTO>> GetUserAsync([FromRoute(Name = "account")] string account) =>
            await _proxy.ExecuteAsync<UserDTO, UserDTO>("GetUserModelActionAsync", new UserDTO() { Account = account });

        /// <summary>新增使用者資料模型
        /// </summary>
        /// <param name="request">使用者資料模型請求</param>
        /// <returns>新增結果</returns>
        [HttpPost()]
        public async Task<IResult<UserDTO>> PostAsync([FromBody] UserDTO request) =>
            await _proxy.ExecuteAsync<UserDTO, UserDTO>("CreateUserModelActionAsync", request);

        /// <summary>更新使用者資料模型
        /// </summary>
        /// <param name="request">使用者資料模型請求</param>
        /// <returns>更新結果</returns>
        [HttpPut()]
        public async Task<IResult<UserDTO>> PutAsync([FromBody] UserDTO request) =>
            await _proxy.ExecuteAsync<UserDTO, UserDTO>("UpdateUserModelActionAsync", request);

        /// <summary>刪除使用者資料模型
        /// </summary>
        /// <param name="request">使用者資料模型請求</param>
        /// <returns>刪除結果</returns>
        [HttpDelete("{account}")]
        public async Task<IResult> DeleteAsync([FromRoute(Name = "account")] string account) =>
            await _proxy.ExecuteAsync<UserDTO>("DeleteUserModelActionAsync", new UserDTO() { Account = account });

        #endregion Public Methods
    }
}