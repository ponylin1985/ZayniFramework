using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using ServiceHost.Client.Proxy;
using ServiceHost.Service.Library.Action;
using ServiceHost.Test.Entity;
using System;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Logging;
using ZayniFramework.Serialization;


namespace WebAPI.Test.Controllers
{
    /// <summary>測試用的 FooController
    /// </summary>
    [Route("foo")]
    [ApiController()]
    public class FooController : ControllerBase
    {
        #region Private Fields

        /// <summary>服務代理人物件<para/>
        /// 使用服務代理人 Proxy 執行 ServiceAction 的最大其中一個優勢就是:<para/>
        /// 當 InProcess 的 ServiceAction 要採用 Microservice 架構時，拆出去變成一個外部獨立的 microservice 的時候，<para/>
        /// 這邊呼叫端的程式碼是「可以完全不用」任何調整!!! 直接改 serviceClientConfig.json 的 remoteHostType 設定值就搞定了。<para/>
        /// 這才是真正把一些切都抽象化的威力!!!
        /// </summary>
        private static readonly MyProxy _proxy = MyProxy.GetInstance("./conf/serviceClientConfig.json");

        /// <summary>測試用的服務
        /// </summary>
        private readonly IMyTestService _service;

        #endregion Private Fields


        #region Constructor

        /// <summary>多載建構子
        /// </summary>
        /// <param name="serviceProvider">服務定位器</param>
        public FooController(IServiceProvider serviceProvider)
        {
            _service = serviceProvider.GetService<IMyTestService>();
        }

        #endregion Constructor


        #region Public Methods

        /// <summary>查詢外部網路的 IP 位址。
        /// </summary>
        /// <returns></returns>
        [HttpGet("/get/ip")]
        public async Task<IResult<string>> GetExternalIPAaddressAsync()
        {
            try
            {
                var ipAddress = await IPAddressHelper.GetExternalIPAddressAsync();
                return Result.Create(true, data: ipAddress);
            }
            catch (Exception)
            {
                return Result.Create(false, data: default(string));
            }
        }

        /// <summary>測試用動作
        /// <para>* `POST /foo`</para>
        /// <para>  * 使用 Middle.Service.Client 提供的 Proxy 代理人物件呼叫 ServiceAction。</para>
        /// <para>  * 程式架構較為彈性，但效能比原生 ASP.NET Core Web API 稍差。</para>
        /// </summary>
        /// <param name="request">請求資料載體</param>
        /// <returns>動作結果</returns>
        [HttpPost()]
        public async Task<IResult<MagicDTO>> PostAsync([FromBody] SomethingDTO request) =>
            await _proxy.ExecuteAsync<SomethingDTO, MagicDTO>("FooActionAsync", request);

        /// <summary>測試用動作
        /// <para>* `POST /foo/exec`</para>
        /// <para>  * 執行呼叫 ServiceAction 的公開方法。</para>
        /// <para>  * 程式耦合度較高，但效能非常好，因為這算是 ASP.NET Core 原生的程式寫法。</para>
        /// </summary>
        /// <param name="request">請求資料載體</param>
        /// <returns>動作結果</returns>
        [HttpPost("exec")]
        public async Task<IResult<MagicDTO>> ExecuteActionAsync([FromBody] SomethingDTO request) =>
            await FooActionAsync.ExecuteAsync(request);

        /// <summary>同步的 action method 測試呼叫非同步的 async method 的測試
        /// </summary>
        /// <param name="request">請求資料載體</param>
        /// <returns>動作結果</returns>
        [HttpPost("magic")]
        public IResult<MagicDTO> ExecMagic([FromBody] SomethingDTO request) =>
            _service.Execute(request);

        #endregion Public Methods
    }

    /// <summary>測試用的服務介面
    /// </summary>
    public interface IMyTestService
    {
        /// <summary>執行呼叫
        /// </summary>
        /// <param name="request">請求資料載體</param>
        /// <returns>執行結果</returns>
        IResult<MagicDTO> Execute(SomethingDTO request);
    }

    /// <summary>測試用的服務
    /// </summary>
    public class MyTestService : IMyTestService
    {
        /// <summary>執行呼叫
        /// </summary>
        /// <param name="request">請求資料載體</param>
        /// <returns>執行結果</returns>
        public IResult<MagicDTO> Execute(SomethingDTO request) =>
            ExecMagic(request).ConfigureAwait(false).GetAwaiter().GetResult();

        /// <summary>呼叫遠端 http 服務
        /// </summary>
        /// <param name="request">請求資料載體</param>
        /// <returns>執行結果</returns>
        private async Task<IResult<MagicDTO>> ExecMagic(SomethingDTO request)
        {
            var baseUrl = "http://zayni-lab.ddns.net:31178/zayni/api/";
            var httpClient = HttpClientFactory.Create(baseUrl);

            var req = new HttpRequest()
            {
                Path = "/foo",
                Body = JsonSerialize.SerializeObject(request)
            };

            var r = await httpClient.ExecuteHttp2PostJsonAsync(req, reqHandler: RequestLog, resHandler: ResponseLog, withHttp2: true).ConfigureAwait(false);

            if (!r.Success)
            {
                return Result.Create<MagicDTO>(false, message: "Error");
            }

            var resDTO = JsonSerialize.DeserializeObject<Result<MagicDTO>>(r.Data).Data;
            return Result.Create<MagicDTO>(true, resDTO);
        }

        /// <summary>請求日誌
        /// </summary>
        /// <param name="request">Http 請求封裝物件</param>
        private Task RequestLog(HttpRequest request)
        {
            Logger.Info(this, JsonSerialize.SerializeObject(request), "RequestLog");
            return Task.FromResult(0);
        }

        /// <summary>回應日誌
        /// </summary>
        /// <param name="request">Http 請求封裝物件</param>
        /// <param name="response">Http 回應封裝物件</param>
        private Task ResponseLog(HttpRequest request, HttpResponse response)
        {
            Logger.Info(this, JsonSerialize.SerializeObject(response), "ResponseLog");
            return Task.FromResult(0);
        }
    }
}
