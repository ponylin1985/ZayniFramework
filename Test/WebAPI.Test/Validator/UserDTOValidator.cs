using FluentValidation;
using ServiceHost.Test.Entity;


namespace WebAPI.Test.Validator
{
    /// <summary>UserDTO 資料驗證器
    /// </summary>
    public sealed class UserDTOValidator : AbstractValidator<UserDTO>
    {
        /// <summary>預設建構子
        /// </summary>
        public UserDTOValidator()
        {
            RuleFor(q => q.Age).GreaterThanOrEqualTo(0).WithMessage("Invalid value range. Must greater than 0.");
            RuleFor(q => q.Sex).Must(q => 0 == q || 1 == q).WithMessage("Invalid value range. Must be 0 or 1.");
        }
    }
}