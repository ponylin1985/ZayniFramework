﻿using FluentValidation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using ServiceHost.Service.Library.Action;
using ServiceHost.Test.Entity;
using WebAPI.Test.Controllers;
using WebAPI.Test.Validator;


namespace WebAPI.Test
{
    /// <summary>ASP.NET Core 啟動類別
    /// </summary>
    public class Startup
    {
        /// <summary>ASP.NET Core CORS 允許所有 Origin 的政策名稱
        /// </summary>
        private static readonly string _corsAllowAnyPolicyName = "AnyOriginCorsPolicy";

        /// <summary>應用程式設定組態
        /// </summary>
        /// <param name="configuration"></param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>應用程式設定組態
        /// </summary>
        /// <value></value>
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        /// <summary>注射 Service 服務類別到 ASP.NET Core 的 DI Container 中
        /// </summary>
        /// <param name="services">服務容器</param>
        public void ConfigureServices(IServiceCollection services)
        {
            // services.AddControllers().AddNewtonsoftJson();
            services
                .AddControllers()
                .AddJsonOptions(s => s.JsonSerializerOptions.WriteIndented = true);

            services
                .AddScoped<FluentValidationFilter>()
                .AddScoped<IValidator<UserDTO>, UserDTOValidator>()
                .AddScoped<IMyTestService, MyTestService>()
                .AddScoped<FooActionAsync>();

            services.AddCors(options =>
            {
                // ASP.NET Core Cors (Cross-Origin Request) 處理
                // 參考: https://blog.johnwu.cc/article/ironman-day26-asp-net-core-cross-origin-requests.html
                options.AddPolicy(_corsAllowAnyPolicyName, policy =>
                {
                    policy
                        .AllowAnyOrigin()
                        .AllowAnyHeader()
                        .AllowAnyMethod();

                    // 升級至 ASP.NET Core 3.0 後暫時需要註解掉 AllowCredentials() 否則會有以下的 runtime exception。
                    // 'The CORS protocol does not allow specifying a wildcard (any) origin and credentials at the same time. Configure the CORS policy by listing individual origins if credentials needs to be supported.'
                    // .AllowCredentials();
                });
            });

            services.AddLogging(options =>
            {
                options.ClearProviders();
                options
                    .AddFilter("Default", LogLevel.Error)
                    .AddFilter("Microsoft", LogLevel.Error)
                    .AddFilter("System", LogLevel.Error)
                    .AddConsole();
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// <summary>組態 ASP.NET Core 的 Http Request Pipeline
        /// </summary>
        /// <param name="app">應用程式接收請求 Pipeline 的 Builder 物件</param>
        /// <param name="env">Web 應用程式環境物件</param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            // if ( env.IsDevelopment() )
            // {
            //     app.UseDeveloperExceptionPage();
            // }

            // ASP.NET Core 3.0 啟動順序是有影響的
            // https://docs.microsoft.com/zh-tw/aspnet/core/migration/22-to-30?view=aspnetcore-3.0&tabs=visual-studio
            app.UseRouting();
            app.UseCors(_corsAllowAnyPolicyName);
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints => endpoints.MapControllers());
        }
    }
}
