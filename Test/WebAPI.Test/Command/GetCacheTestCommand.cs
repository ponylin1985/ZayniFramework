using System;
using System.Text.Json;
using System.Threading.Tasks;
using ServiceHost.Service.DataAccess;
using ServiceHost.Test.Entity;
using ZayniFramework.Caching;
using ZayniFramework.Common;
using ZayniFramework.Middle.TelnetService;
using ZayniFramework.Serialization;


namespace WebAPI.Test
{
    /// <summary>取得快取資料的測試命令 (原始命令: get cache)
    /// </summary>
    public class GetCacheTestCommandAsync : RemoteCommand
    {
        /// <summary>執行命令處理
        /// </summary>
        /// <param name="parameters">命令參數集合</param>
        /// <returns>執行結果</returns>
        public override async Task<IResult> ExecuteAsync(ParameterCollection parameters)
        {
            var key = "zayni:webapi:cachemanager.tester";

            var dao = new UserDao();

            var g = await CacheManager.GetAsync(key, async () =>
            {
                var r = await dao.GetAsync(new UserModel() { Account = "WebAPI.Test.CacheManagerTester" });
                return r.Success && r.Data.IsNotNull() ? r.Data : null;
            });

            if (!g.Success)
            {
                await StdoutErrAsync($"Get cache data fail. {g.Message}");
                return Result;
            }

            if (g.CacheData.IsNull())
            {
                await StdoutAsync($"No cache data found.");
                return Result;
            }

            if (g.CacheData is JsonElement payload)
            {
                await StdoutAsync($"{Environment.NewLine}{payload}", ConsoleColor.Yellow);
            }
            else
            {
                await StdoutAsync($"{Environment.NewLine}{NewtonsoftJsonConvert.Serialize(g)}", ConsoleColor.Yellow);
            }

            await StdoutAsync($"Get cache data success.");
            Result.Success = true;
            return Result;
        }
    }
}