﻿using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Logging;
using ZayniFramework.Middle.Service;
using ZayniFramework.Middle.Service.Grpc;
using ZayniFramework.Middle.Service.HttpCore;
using ZayniFramework.Middle.TelnetService;


namespace ServiceHost.ServerSide.App
{
    /// <summary>主程式
    /// </summary>
    public sealed class Program
    {
        #region 宣告程式進入點方法

        /// <summary>程式進入點
        /// </summary>
        /// <param name="args"></param>
        public static async Task Main(string[] args)
        {
            var path = $"{Path.GetDirectoryName(Assembly.GetEntryAssembly().Location)}/zayni.json";
            ConfigManager.Init(path);

            StartServiceHosts();

            var commandContainer = RegisterCommands();

            if (ConfigManager.GetZayniFrameworkSettings().TelnetServerSettings.EnableTelnetServer)
            {
                await TelnetCommandService.StartDefaultTelnetServiceAsync(commandContainer);
            }

            await ConsoleCommandService.StartDefaultConsoleServiceAsync(commandContainer);

            Console.WriteLine($"Press enter key to exit the application...");
            Console.ReadLine();
        }

        #endregion 宣告程式進入點方法


        #region 宣告私有的方法

        /// <summary>開啟 serviceHostConfig.json 自我裝載服務
        /// </summary>
        private static void StartServiceHosts()
        {
            // 註冊 Middle.Service extension module 的 gRPCRemoteHost IRemoteHost 型別
            MiddlewareService.RegisterRemoteHostType<gRPCRemoteHost>("gRPC");

            // 註冊 Middle.Service extension module 的 HttpCoreRemoteHost IRemoteHost 型別
            MiddlewareService.RegisterRemoteHostType<HttpCoreRemoteHost>("HttpCore");

            #region 動態註冊 ServiceAction 的機制

            // Pony Says: 如果採用 MiddlewareService.StartServiceHosts() 的方式啟動服務，允許不用自行註冊 ServiceAction 動作，框架內部會動態的註冊!
            // 註冊 ServiceAction 服務動作到 Middle.Service module 中介服務中
            // MiddlewareService.InitializeServiceActionHandler = () =>
            // {
            //     var testAction = new TestAction();
            //     ServiceActionContainerManager.RegisterServiceAction( testAction.Name, testAction.GetType() );

            //     var magicTestAction = new MagicTestAction();
            //     ServiceActionContainerManager.RegisterServiceAction( magicTestAction.Name, magicTestAction.GetType() );

            //     var largeDataTestAction = new LargeDataTestAction();
            //     ServiceActionContainerManager.RegisterServiceAction( largeDataTestAction.Name, largeDataTestAction.GetType() );

            //     var someEventAction = new SomeEventAction();
            //     ServiceActionContainerManager.RegisterServiceAction( someEventAction.Name, someEventAction.GetType() );

            //     var getUserModelsAction = new GetUserModelsAction();
            //     ServiceActionContainerManager.RegisterServiceAction( getUserModelsAction.Name, getUserModelsAction.GetType() );

            //     return Result.Create( true );
            // };

            #endregion 動態註冊 ServiceAction 的機制

            // Pony Says: 如果採用 MiddlewareService.StartServiceHosts() 的方式啟動服務，允許不用自行註冊 ServiceAction 動作，框架內部會動態的註冊!
            var r = MiddlewareService.StartServiceHosts();

            if (!r.Success)
            {
                ConsoleLogger.Log($"{r.Message}", ConsoleColor.Green);
                return;
            }

            ConsoleLogger.Log("Start all service host successfully.", ConsoleColor.Green);
        }

        // 別以為這種老派的寫法很遜... Sometimes, the old way is the best way...
        /// <summary>註冊命令處理物件
        /// </summary>
        /// <returns>命令容器池</returns>
        private static CommandContainer RegisterCommands()
        {
            var commandContainer = new CommandContainer();
            commandContainer.RegisterCommand<PushCommand>("push", commandText => 0 == string.Compare(commandText.Trim(), "push", true));
            commandContainer.RegisterCommand<ExitConsoleCommand>("exit", commandText => 0 == string.Compare(commandText.Trim(), "exit", true));
            commandContainer.RegisterCommand<ClearConsoleCommand>("cls", commandText => 0 == string.Compare(commandText.Trim(), "cls", true));
            commandContainer.RegisterUnknowCommand<UnknowRemoteCommand>();
            return commandContainer;
        }

        #endregion 宣告私有的方法
    }
}
