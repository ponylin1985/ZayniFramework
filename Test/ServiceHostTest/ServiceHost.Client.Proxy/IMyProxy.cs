using System.Threading.Tasks;
using ZayniFramework.Common;


namespace ServiceHost.Client.Proxy
{
    /// <summary>Middle Service 的服務代理人介面
    /// </summary>
    public interface IMyProxy
    {
        /// <summary>執行遠端服務動作的測試
        /// </summary>
        /// <typeparam name="TReqDTO">請求資料載體的泛型</typeparam>
        /// <typeparam name="TResDTO">回應資料載體的泛型</typeparam>
        /// <param name="actionName">動作名稱</param>
        /// <param name="request">請求資料載體</param>
        /// <returns>執行結果</returns>
        IResult<TResDTO> Execute<TReqDTO, TResDTO>(string actionName, TReqDTO request);

        /// <summary>非同步作業執行遠端服務動作的測試
        /// </summary>
        /// <typeparam name="TReqDTO">請求資料載體的泛型</typeparam>
        /// <typeparam name="TResDTO">回應資料載體的泛型</typeparam>
        /// <param name="actionName">動作名稱</param>
        /// <param name="request">請求資料載體</param>
        /// <returns>執行結果</returns>
        Task<IResult<TResDTO>> ExecuteAsync<TReqDTO, TResDTO>(string actionName, TReqDTO request);

        /// <summary>非同步作業執行遠端服務動作的測試
        /// </summary>
        /// <param name="actionName">動作名稱</param>
        /// <param name="request">請求資料載體</param>
        /// <returns>執行結果</returns>
        Task<IResult> ExecuteAsync<TReqDTO>(string actionName, TReqDTO request);
    }
}