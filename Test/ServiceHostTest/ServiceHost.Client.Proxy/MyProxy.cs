﻿using NeoSmart.AsyncLock;
using ServiceHost.Test.Entity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Middle.Service.Client;


namespace ServiceHost.Client.Proxy
{
    /// <summary>測試用的遠端 Proxy 代理人類別
    /// </summary>
    public class MyProxy : RemoteProxy, IMyProxy
    {
        #region 宣告靜態成員

        /// <summary>非同步作業鎖定物件
        /// </summary>
        private static readonly AsyncLock _asyncLock = new AsyncLock();

        /// <summary>MyProxy 實體
        /// </summary>
        private static MyProxy _instance;

        /// <summary>取得 MyProxy 獨體
        /// </summary>
        /// <param name="configPath"></param>
        /// <returns></returns>
        public static MyProxy GetInstance(string configPath = "./serviceClientConfig.json")
        {
            using (_asyncLock.Lock())
            {
                _instance.IsNull(() => _instance = new MyProxy(configPath));
            }

            return _instance;
        }

        #endregion 宣告靜態成員


        #region 宣告建構子

        /// <summary>多載建構子
        /// </summary>
        /// <param name="path">服務客戶端 serviceClientConfig.json 設定檔的路徑</param>
        private MyProxy(string path = "./serviceClientConfig.json") : base(serviceClientName: "ServiceHost.Test.Client", configPath: path)
        {
        }

        #endregion 宣告建構子


        #region 宣告公開的方法

        /// <summary>註冊 ServiceEvent 服務事件處理器的測試
        /// </summary>
        /// <param name="actionName">事件名稱</param>
        /// <param name="type">事件處理器 (EventProcessor) 的型別</param>
        /// <returns>註冊結果</returns>
        public IResult Add(string actionName, Type type) => base.AddEventProcessor(actionName, type);

        /// <summary>執行遠端服務動作的測試
        /// </summary>
        /// <typeparam name="TReqDTO">請求資料載體的泛型</typeparam>
        /// <typeparam name="TResDTO">回應資料載體的泛型</typeparam>
        /// <param name="actionName">動作名稱</param>
        /// <param name="request">請求資料載體</param>
        /// <returns>執行結果</returns>
        public new IResult<TResDTO> Execute<TReqDTO, TResDTO>(string actionName, TReqDTO request) => base.Execute<TReqDTO, TResDTO>(actionName, request);

        /// <summary>非同步作業執行遠端服務動作的測試
        /// </summary>
        /// <typeparam name="TReqDTO">請求資料載體的泛型</typeparam>
        /// <typeparam name="TResDTO">回應資料載體的泛型</typeparam>
        /// <param name="actionName">動作名稱</param>
        /// <param name="request">請求資料載體</param>
        /// <returns>執行結果</returns>
        public new async Task<IResult<TResDTO>> ExecuteAsync<TReqDTO, TResDTO>(string actionName, TReqDTO request) => await base.ExecuteAsync<TReqDTO, TResDTO>(actionName, request);

        /// <summary>非同步作業執行遠端服務動作的測試
        /// </summary>
        /// <typeparam name="TReqDTO">請求資料載體的泛型</typeparam>
        /// <param name="actionName">動作名稱</param>
        /// <param name="request">請求資料載體</param>
        /// <returns>執行結果</returns>
        public new async Task<IResult> ExecuteAsync<TReqDTO>(string actionName, TReqDTO request) => await base.ExecuteAsync<TReqDTO>(actionName, request);

        /// <summary>發佈 ServiceEvent 服務事件訊息的測試
        /// </summary>
        /// <typeparam name="TMsgDTO">ServiceEvent 訊息資料載體的泛型</typeparam>
        /// <param name="actionName">事件名稱</param>
        /// <param name="dto">訊息資料載體</param>
        /// <returns>發付結果</returns>
        public new IResult Publish<TMsgDTO>(string actionName, TMsgDTO dto) => base.Publish<TMsgDTO>(actionName, dto);

        #endregion 宣告公開的方法


        #region 宣告公開的服務方法

        /// <summary>測試用的服務動作
        /// </summary>
        /// <param name="request">請求資料載體</param>
        /// <returns>執行結果</returns>
        public IResult<MagicDTO> GetMagicDTO(SomethingDTO request) => Execute<SomethingDTO, MagicDTO>("MagicTestAction", request);

        /// <summary>測試用的服務動作
        /// </summary>
        /// <param name="request">請求資料載體</param>
        /// <returns>執行結果</returns>
        public async Task<IResult<MagicDTO>> GetMagicDTOAsync(SomethingDTO request) => await ExecuteAsync<SomethingDTO, MagicDTO>("MagicTestActionAsync", request);

        /// <summary>取得使用者資料模型
        /// </summary>
        /// <param name="request">請求資料載體</param>
        /// <returns>查詢結果</returns>
        public IResult<IEnumerable<UserDTO>> GetUserModels(UserDTO request) => Execute<UserDTO, IEnumerable<UserDTO>>("GetUserModelsAction", request);

        /// <summary>非同步作業取得使用者資料模型
        /// </summary>
        /// <param name="request">請求資料載體</param>
        /// <returns>查詢結果</returns>
        public async Task<IResult<IEnumerable<UserDTO>>> GetUserModelsAsync(UserDTO request) => await base.ExecuteAsync<UserDTO, IEnumerable<UserDTO>>("GetUserModelsActionAsync", request);

        #endregion 宣告公開的服務方法
    }
}
