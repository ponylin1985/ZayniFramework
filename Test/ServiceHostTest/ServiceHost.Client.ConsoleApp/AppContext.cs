using ServiceHost.Client.Proxy;


namespace ServiceHost.Client.App
{
    /// <summary>應用程式參數
    /// </summary>
    public static class AppContext
    {
        /// <summary>Middle.Service.Client module 的 json config 的路徑
        /// </summary>
        /// <value></value>
        public static string ServiceClientConfigPath { get; set; } = "./serviceClientConfig.json";

        /// <summary>測試用的遠端 Proxy 代理人物件
        /// </summary>
        /// <value></value>
        public static MyProxy MyProxy { get; set; }
    }
}