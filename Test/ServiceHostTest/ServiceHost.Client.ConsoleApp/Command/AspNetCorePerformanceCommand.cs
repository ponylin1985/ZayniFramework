using ServiceHost.Test.Entity;
using System;
using System.Linq;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Middle.TelnetService;


namespace ServiceHost.Client.App
{
    /// <summary>執行 ASP.NET Core Web API 效能負載測試的命令 (原始命令: aspnet performance test {count})
    /// </summary>
    public class AspNetCorePerformanceTestCommandAsync : RemoteCommand
    {
        /// <summary>ASP.NET Core Web API 的 URL 位址
        /// </summary>
        private readonly string _apiUrl = ConfigManager.GetAppSettings()["WebAPIUrl"];

        /// <summary>執行命令處理
        /// </summary>
        /// <param name="parameters">命令參數集合</param>
        /// <returns>執行結果</returns>
        public override async Task<IResult> ExecuteAsync(ParameterCollection parameters)
        {
            if (_apiUrl.IsNullOrEmpty())
            {
                await StdoutErrAsync($"Please set the 'WebAPIUrl' in the zayni.json file first.");
                return Result;
            }

            var httpClient = HttpClientFactory.Create(_apiUrl, 5);

            var t = CommandText.Trim()?.Split(' ');
            var count = 5 == t.Length && int.TryParse(t.LastOrDefault(), out var cmdCount) ? cmdCount : 1000;

            var reqDTO = new SomethingDTO()
            {
                SomeMessage = "ASP.NET Core Performance Test!",
                SomeDate = DateTime.UtcNow,
                SomeMagicNumber = 44.33D,
                LuckyNumber = 88,
                IsSuperMan = true
            };

            var reqJson = JsonSerializer.Serialize(reqDTO);
            var begin = DateTime.UtcNow;

            var request = new HttpRequest()
            {
                Path = "test",
                Body = reqJson
            };

            int i;

            for (i = 0; i < count; i++)
            {
                var g = await httpClient.ExecutePostJsonAsync(request, consoleOutputLog: false);

                if (!g.Success || g.StatusCode != HttpStatusCode.OK)
                {
                    await StdoutErrAsync($"Performance fail at {i + 1} due to http response is not OK.");
                    return Result;
                }

                var r = JsonSerializer.Deserialize<Result<MagicDTO>>(g.Data);

                if (!r.Success)
                {
                    await StdoutErrAsync($"Performance fail at {i + 1}.");
                    return Result;
                }
            }

            var end = DateTime.UtcNow;
            var sp = (end - begin).TotalSeconds;
            await StdoutAsync($"All success. ExecuteCount: {count}. Total Seconds: {sp}");

            Result.Success = true;
            return Result;
        }
    }
}