using System;
using System.Threading.Tasks;
using ServiceHost.Client.Proxy;
using ServiceHost.Test.Entity;
using ZayniFramework.Common;
using ZayniFramework.Middle.TelnetService;
using ZayniFramework.Serialization;


namespace ServiceHost.Client.App
{
    // 從 Middle.Service.Client 客戶端的 RemoteProxy 觸發 ServiceEvent 的測試!
    /// <summary>ServiceEvent 訊息發布的測試命令 (原始命令: fire)
    /// </summary>
    public class FireCommand : RemoteCommand
    {
        /// <summary>執行命令處理
        /// </summary>
        /// <param name="parameters">命令參數集合</param>
        /// <returns>執行結果</returns>
        public override async Task<IResult> ExecuteAsync(ParameterCollection parameters)
        {
            var proxy = MyProxy.GetInstance(AppContext.ServiceClientConfigPath);

            var messageDTO = new SomethingDTO()
            {
                SomeMessage = "This is happy bear! From Client App.",
                SomeDate = DateTime.Today,
                SomeMagicNumber = 4562.12,
                LuckyNumber = 4129,
                IsSuperMan = true
            };

            // 觸發 ServiceEvent，傳入事件名稱與訊息資料載體。
            proxy.Publish("SomeEventAction", messageDTO);

            await StdoutAsync($"Publish SomethingingDTO message now...");
            await StdoutAsync(NewtonsoftJsonConvert.Serialize(messageDTO));
            Result.Success = true;
            return Result;
        }
    }

    // 從 Middle.Service.Client 客戶端的 RemoteProxy 觸發 ServiceEvent 的測試!
    /// <summary>ServiceEvent 訊息發布的測試命令 (原始命令: fire async)
    /// </summary>
    public class FireCommandAsync : RemoteCommand
    {
        /// <summary>執行命令處理
        /// </summary>
        /// <param name="parameters">命令參數集合</param>
        /// <returns>執行結果</returns>
        public override async Task<IResult> ExecuteAsync(ParameterCollection parameters)
        {
            var proxy = MyProxy.GetInstance(AppContext.ServiceClientConfigPath);

            var messageDTO = new SomethingDTO()
            {
                SomeMessage = "This is happy bear! From Client App.",
                SomeDate = DateTime.Today,
                SomeMagicNumber = 4562.12,
                LuckyNumber = 4129,
                IsSuperMan = true
            };

            // 觸發 ServiceEvent，傳入事件名稱與訊息資料載體。
            proxy.Publish("SomeEventAction", messageDTO);

            await StdoutAsync($"Publish SomethingingDTO message now...");
            await StdoutAsync(NewtonsoftJsonConvert.Serialize(messageDTO));
            Result.Success = true;
            return Result;
        }
    }
}