using ServiceHost.Client.Proxy;
using ServiceHost.Test.Entity;
using System.Linq;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Middle.TelnetService;
using ZayniFramework.Serialization;


namespace ServiceHost.Client.App
{
    /// <summary>發送 RPC 請求的測試命令 (原始命令: foo async {number})
    /// </summary>
    public class FooActionCommandAsync : RemoteCommand, ICommand
    {
        /// <summary>測試用的服務代理人 Proxy 物件
        /// </summary>
        private readonly MyProxy _proxy = AppContext.MyProxy;

        /// <summary>執行命令處理
        /// </summary>
        /// <param name="parameters">命令參數集合</param>
        /// <returns>執行結果</returns>
        public override async Task<IResult> ExecuteAsync(ParameterCollection parameters)
        {
            if (_proxy.IsNull())
            {
                await StdoutErrAsync($"Please execute the 'init' command first.");
                return Result;
            }

            var number = 100;
            var tokens = CommandText.Split(' ');

            if (3 == tokens.Length)
            {
                number = int.TryParse(tokens.LastOrDefault(), out var n) ? n : number;
            }

            var request = new SomethingDTO()
            {
                SomeMessage = "From console application...",
                LuckyNumber = number,
            };

            var r = await _proxy.ExecuteAsync<SomethingDTO, MagicDTO>("FooActionAsync", request);
            await StdoutAsync(NewtonsoftJsonConvert.SerializeInCamelCase(r));

            Result.Success = r.Success;
            return Result;
        }
    }
}