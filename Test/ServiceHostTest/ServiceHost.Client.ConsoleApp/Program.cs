﻿using System.Globalization;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Logging;
using ZayniFramework.Middle.Service.Client;
using ZayniFramework.Middle.Service.Grpc.Client;
using ZayniFramework.Middle.Service.HttpCore.Client;
using ZayniFramework.Middle.TelnetService;


namespace ServiceHost.Client.App
{
    /// <summary>主程式
    /// </summary>
    public sealed class Program
    {
        #region 宣告進入點方法

        /// <summary>主程式進入點 (Async Main method)
        /// </summary>
        /// <param name="args"></param>
        public static async Task Main(string[] args)
        {
            var path = $"{Path.GetDirectoryName(Assembly.GetEntryAssembly().Location)}/zayni.json";
            await ConfigManager.InitAsync(path);

            ConsoleLogger.WriteLine("Start ServiceHost.Client.App console application now...");
            RemoteClientFactory.RegisterRemoteClientType<gRPCRemoteClient>("gRPC");
            RemoteClientFactory.RegisterRemoteClientType<HttpCoreRemoteClient>("HttpCore");

            #region Start async CommandService

            var asyncCommandContainer = await RegisterCommandsAsync();

            if (ConfigManager.GetZayniFrameworkSettings().TelnetServerSettings.EnableTelnetServer)
            {
                await TelnetCommandService.StartDefaultTelnetServiceAsync(asyncCommandContainer);
            }

            await ConsoleCommandService.StartDefaultConsoleServiceAsync(asyncCommandContainer);

            #endregion Start async CommandService
        }

        #endregion 宣告進入點方法


        #region 宣告私有的方法

        /// <summary>註冊命令處理物件
        /// </summary>
        /// <returns>命令容器池</returns>
        private static async Task<CommandContainer> RegisterCommandsAsync()
        {
            var commandContainer = new CommandContainer();
            await commandContainer.RegisterCommandAsync<InitCommandAsync>("init", commandText => 0 == string.Compare(commandText.Trim(), "init async", true));
            await commandContainer.RegisterCommandAsync<SendMagicActionCommand>("send magic", commandText => 0 == string.Compare(commandText.Trim(), "send magic action", true));
            await commandContainer.RegisterCommandAsync<SendMagicActionCommandAsync>("send magic async", commandText => 0 == string.Compare(commandText.Trim(), "send magic action async", true));
            await commandContainer.RegisterCommandAsync<FooActionCommandAsync>("foo", commandText => commandText.Trim().StartsWith("foo async", true, CultureInfo.InvariantCulture));
            await commandContainer.RegisterCommandAsync<GetUsersCommand>("get users", commandText => 0 == string.Compare(commandText.Trim(), "get users", true));
            await commandContainer.RegisterCommandAsync<GetUsersCommandAsync>("get users async", commandText => 0 == string.Compare(commandText.Trim(), "get users async", true));
            await commandContainer.RegisterCommandAsync<PerformanceTestCommandAsync>("performance", commandText => commandText.Trim().StartsWith("performance test async", true, CultureInfo.InvariantCulture));
            await commandContainer.RegisterCommandAsync<AspNetCorePerformanceTestCommandAsync>("aspnet core performance", commandText => commandText.Trim().StartsWith("aspnet performance test async", true, CultureInfo.InvariantCulture));
            await commandContainer.RegisterCommandAsync<HttpGetTestCommandAsync>("aspnet core http get performance", commandText => commandText.Trim().StartsWith("http get test", true, CultureInfo.InvariantCulture));
            await commandContainer.RegisterCommandAsync<FireCommand>("fire", commandText => commandText.Trim().StartsWith("fire", true, CultureInfo.InvariantCulture));
            await commandContainer.RegisterCommandAsync<FireCommandAsync>("fire", commandText => commandText.Trim().StartsWith("fire async", true, CultureInfo.InvariantCulture));
            await commandContainer.RegisterCommandAsync<ExitConsoleCommand>("exit", commandText => 0 == string.Compare(commandText.Trim(), "exit", true));
            await commandContainer.RegisterCommandAsync<ClearConsoleCommand>("cls", commandText => 0 == string.Compare(commandText.Trim(), "cls", true));
            await commandContainer.RegisterUnknowCommandAsync<UnknowRemoteCommand>();
            return commandContainer;
        }

        #endregion 宣告私有的方法
    }
}
