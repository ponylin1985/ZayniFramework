using ServiceHost.Test.Entity;
using System;
using ZayniFramework.Logging;
using ZayniFramework.Middle.Service.Client;
using ZayniFramework.Serialization;


namespace ServiceHost.Client
{
    /// <summary>用來測試用的 Service Host Service Event 事件處理器
    /// </summary>
    internal class HappyEventHandler : EventProcessor<SomethingDTO>
    {
        /// <summary>預設建構子
        /// </summary>
        /// <returns></returns>
        public HappyEventHandler() : base("HappyPublish")
        {
            // pass
        }

        /// <summary>執行事件處理
        /// </summary>
        public override void Execute()
        {
            ConsoleLogger.WriteLine($"Receive: {Environment.NewLine}{NewtonsoftJsonConvert.Serialize(DataContent)}");
        }
    }
}
