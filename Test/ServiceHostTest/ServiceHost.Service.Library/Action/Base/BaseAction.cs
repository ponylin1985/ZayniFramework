using ZayniFramework.Common;
using ZayniFramework.Middle.Service;


namespace ServiceHost.ServerSide
{
    /// <summary>非同步的服務動作基底
    /// </summary>
    public abstract class BaseAction<TReqDTO, TResDTO> : ServiceAction<TReqDTO, IResult<TResDTO>>
    {
        /// <summary>預設建構子
        /// </summary>
        public BaseAction(string actionName) : base(actionName: actionName)
        {
        }
    }

    /// <summary>非同步的服務動作基底
    /// </summary>
    public abstract class BaseAction<TReqDTO> : ServiceAction<TReqDTO, IResult>
    {
        /// <summary>預設建構子
        /// * 由於 ZayniFramework Middle.Service 框架內部使用 reflection 的機制載入 ServiceAction，導致需要在基底建構子這邊進行初次 ServiceIoCContainer 的相依性容器注入。
        /// </summary>
        public BaseAction(string actionName) : base(actionName: actionName)
        {
        }
    }
}