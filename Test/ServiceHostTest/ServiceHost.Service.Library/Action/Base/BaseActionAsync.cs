using ZayniFramework.Common;
using ZayniFramework.Middle.Service;


namespace ServiceHost.ServerSide
{
    /// <summary>非同步的服務動作基底
    /// </summary>
    public abstract class BaseActionAsync<TReqDTO, TResDTO> : ServiceActionAsync<TReqDTO, IResult<TResDTO>>
    {
        /// <summary>預設建構子
        /// </summary>
        public BaseActionAsync(string actionName) : base(actionName: actionName)
        {
        }
    }

    /// <summary>非同步的服務動作基底
    /// </summary>
    public abstract class BaseActionAsync<TReqDTO> : ServiceActionAsync<TReqDTO, IResult>
    {
        /// <summary>預設建構子
        /// </summary>
        public BaseActionAsync(string actionName) : base(actionName: actionName)
        {
        }
    }
}