using ServiceHost.ServerSide;
using ServiceHost.Test.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;
using ZayniFramework.Common;


namespace ServiceHost.Service.Library.Action
{
    /// <summary>取得使用者資料模型的測試動作
    /// </summary>
    public class GetUserModelsAction : BaseAction<UserDTO, IEnumerable<UserDTO>>
    {
        /// <summary>預設建構子
        /// </summary>
        public GetUserModelsAction() : base(actionName: "GetUserModelsAction")
        {
        }

        /// <summary>執行動作
        /// </summary>
        public override void Execute() => Response = Execute(Request);

        // Pony Says: 這種方法標記可以當作預留，也不是沒有好處，這種呼叫方式理論上會得到效能最佳化。
        /// <summary>執行動作
        /// </summary>
        /// <param name="request">查詢參數</param>
        /// <returns>查詢結果</returns>
        public IResult<IEnumerable<UserDTO>> Execute(UserDTO request)
        {
            using var serviceScope = ServiceLocator.ResolveServiceScope();
            var service = serviceScope.ResolveService<IUserService>();
            return service.GetUsersAsync(request).GetAwaiter().GetResult();
        }
    }

    /// <summary>取得使用者資料模型的測試動作
    /// </summary>
    public class GetUserModelsActionAsync : BaseActionAsync<UserDTO, IEnumerable<UserDTO>>
    {
        /// <summary>預設建構子
        /// </summary>
        public GetUserModelsActionAsync() : base(actionName: "GetUserModelsActionAsync")
        {
        }

        /// <summary>執行動作
        /// </summary>
        public override async Task ExecuteAsync() => Response = await ExecuteAsync(Request);

        // Pony Says: 這種方法標記可以當作預留，也不是沒有好處，這種呼叫方式理論上會得到效能最佳化。
        /// <summary>執行動作
        /// </summary>
        /// <param name="request">查詢參數</param>
        /// <returns>查詢結果</returns>
        public async Task<IResult<IEnumerable<UserDTO>>> ExecuteAsync(UserDTO request)
        {
            using var serviceScope = ServiceLocator.ResolveServiceScope();
            var service = serviceScope.ResolveService<IUserService>();
            return await service.GetUsersAsync(request);
        }
    }
}