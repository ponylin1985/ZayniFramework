using ServiceHost.ServerSide;
using ServiceHost.Test.Entity;
using System.Threading.Tasks;
using ZayniFramework.Common;


namespace ServiceHost.Service.Library.Action
{
    /// <summary>更新使用者資料模型的動作
    /// </summary>
    public class UpdateUserModelAction : BaseAction<UserDTO, UserDTO>
    {
        /// <summary>預設建構子
        /// </summary>
        public UpdateUserModelAction() : base(actionName: "UpdateUserModelAction")
        {
        }

        /// <summary>執行動作
        /// </summary>
        public override void Execute() => Response = Execute(Request);

        // Pony Says: 這種方法標記可以當作預留，也不是沒有好處，這種呼叫方式理論上會得到效能最佳化。
        /// <summary>執行動作
        /// </summary>
        /// <param name="request">查詢參數</param>
        /// <returns>查詢結果</returns>
        public static IResult<UserDTO> Execute(UserDTO request)
        {
            using var serviceScope = ServiceLocator.ResolveServiceScope();
            var service = serviceScope.ResolveService<IUserService>();
            return service.UpdateAsync(request).GetAwaiter().GetResult();
        }
    }

    /// <summary>更新使用者資料模型的動作
    /// </summary>
    public class UpdateUserModelActionAsync : BaseActionAsync<UserDTO, UserDTO>
    {

        /// <summary>預設建構子
        /// </summary>
        public UpdateUserModelActionAsync() : base(actionName: "UpdateUserModelActionAsync")
        {
        }

        /// <summary>執行動作
        /// </summary>
        public override async Task ExecuteAsync() => Response = await ExecuteAsync(Request);

        // Pony Says: 這種方法標記可以當作預留，也不是沒有好處，這種呼叫方式理論上會得到效能最佳化。
        /// <summary>執行動作
        /// </summary>
        /// <param name="request">查詢參數</param>
        /// <returns>查詢結果</returns>
        public static async Task<IResult<UserDTO>> ExecuteAsync(UserDTO request)
        {
            using var serviceScope = ServiceLocator.ResolveServiceScope();
            var service = serviceScope.ResolveService<IUserService>();
            return await service.UpdateAsync(request);
        }
    }
}