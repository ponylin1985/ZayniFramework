using ServiceHost.ServerSide;
using ServiceHost.Test.Entity;
using System.Threading.Tasks;
using ZayniFramework.Common;


namespace ServiceHost.Service.Library.Action
{
    /// <summary>Foo 服務動作
    /// </summary>
    public class FooAction : BaseAction<SomethingDTO, MagicDTO>
    {
        /// <summary>預設建構子
        /// </summary>
        public FooAction() : base(actionName: "FooAction")
        {
        }

        /// <summary>執行動作
        /// </summary>
        public override void Execute() => Response = Execute(Request);

        // 這是一個示範:
        // 如果需要依照每次 request 請求的參數，在 runtimer 決定需要從 DI Container 中 Resolve 不同的實作的情境。
        // 可以支援同一個服務介面，有多個服務實作類別。
        /// <summary>執行測試用的動作。<para/>
        /// * 如果需要依照每次 request 請求的參數，在 runtimer 決定需要從 DI Container 中 Resolve 不同的實作的情境。<para/>
        /// * 當同一個服務介面，有多個不同的服務實作類別時，依照 runtime 執行時期的參數，從 DI Container Resolve 出符合條件的服務實作。<para/>
        /// </summary>
        /// <param name="request">請求資料載體</param>
        /// <returns>執行結果</returns>
        public static IResult<MagicDTO> Execute(SomethingDTO request)
        {
            using var serviceScope = ServiceLocator.ResolveServiceScope();
            var service = serviceScope.ResolveService<IFooServiceFactory>().Create(request);
            return service.GetSomethingAsync(request).GetAwaiter().GetResult();
        }
    }

    /// <summary>非同步作業的 Foo 服務動作
    /// </summary>
    public class FooActionAsync : BaseActionAsync<SomethingDTO, MagicDTO>
    {
        /// <summary>預設建構子
        /// </summary>
        public FooActionAsync() : base(actionName: "FooActionAsync")
        {
        }

        /// <summary>執行動作
        /// </summary>
        public override async Task ExecuteAsync() => Response = await ExecuteAsync(Request);

        // 這是一個示範:
        // 如果需要依照每次 request 請求的參數，在 runtime 決定需要從 DI Container 中 Resolve 不同的實作的情境。
        // 可以支援同一個服務介面，有多個服務實作類別。
        /// <summary>執行測試用的動作。<para/>
        /// * 如果需要依照每次 request 請求的參數，在 runtimer 決定需要從 DI Container 中 Resolve 不同的實作的情境。<para/>
        /// * 當同一個服務介面，有多個不同的服務實作類別時，依照 runtime 執行時期的參數，從 DI Container Resolve 出符合條件的服務實作。<para/>
        /// </summary>
        /// <param name="request">請求資料載體</param>
        /// <returns>執行結果</returns>
        public static Task<IResult<MagicDTO>> ExecuteAsync(SomethingDTO request)
        {
            using var serviceScope = ServiceLocator.ResolveServiceScope();
            var service = serviceScope.ResolveService<IFooServiceFactory>().Create(request);
            return service.GetSomethingAsync(request);
        }
    }
}
