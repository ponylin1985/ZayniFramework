﻿using ServiceHost.ServerSide;
using ServiceHost.Test.Entity;
using System.Threading.Tasks;
using ZayniFramework.Common;


namespace ServiceHost.Service.Library.Action
{
    /// <summary>神奇的測試動作
    /// </summary>
    public class MagicTestAction : BaseAction<SomethingDTO, MagicDTO>
    {
        /// <summary>預設建構子
        /// </summary>
        public MagicTestAction() : base(actionName: "MagicTestAction")
        {
        }

        /// <summary>執行動作
        /// </summary>
        public override void Execute() => Response = Execute(Request);

        // Pony Says: 這種方法標記可以當作預留，也不是沒有好處，這種呼叫方式理論上會得到效能最佳化。
        /// <summary>執行測試用的動作
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Result<MagicDTO> Execute(SomethingDTO request)
        {
            using var serviceScope = ServiceLocator.ResolveServiceScope();
            var service = serviceScope.ResolveService<IFooService>();
            return service.DoMagicAsync(request).GetAwaiter().GetResult();
        }
    }

    /// <summary>非同步作業的服務測試動作
    /// </summary>
    public class MagicTestActionAsync : BaseActionAsync<SomethingDTO, MagicDTO>
    {
        /// <summary>預設建構子
        /// </summary>
        public MagicTestActionAsync() : base(actionName: "MagicTestActionAsync")
        {
        }

        /// <summary>執行動作
        /// </summary>
        public override async Task ExecuteAsync() => Response = await ExecuteAsync(Request);

        // Pony Says: 這種方法標記可以當作預留，也不是沒有好處，這種呼叫方式理論上會得到效能最佳化。
        /// <summary>執行測試用的動作
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Task<Result<MagicDTO>> ExecuteAsync(SomethingDTO request)
        {
            using var serviceScope = ServiceLocator.ResolveServiceScope();
            var service = serviceScope.ResolveService<IFooService>();
            return service.DoMagicAsync(request);
        }
    }
}
