using ServiceHost.Test.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;
using ZayniFramework.Common;


namespace ServiceHost.Service.DataAccess
{
    /// <summary>使用者資料存取介面
    /// </summary>
    public interface IUserDao
    {
        /// <summary>新增資料 ExecuteNoeQuery 測試
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <returns>新增結果</returns>
        Task<IResult> InsertAsync(UserModel model);

        /// <summary>更新資料 ExecuteNoeQuery 測試
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <returns>更新結果</returns>
        Task<IResult> UpdateAsync(UserModel model);

        /// <summary>刪除資料 ExecuteNoeQuery 測試
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <returns>刪除結果</returns>
        Task<IResult> DeleteAsync(UserModel model);

        /// <summary>查詢資料 LoadDataToModel 測試
        /// </summary>
        /// <param name="query">查詢條件</param>
        /// <returns>查詢結果</returns>
        Task<IResult<UserModel>> GetAsync(UserModel query);

        /// <summary>查詢資料 LoadDataToModel 測試
        /// </summary>
        /// <param name="query">查詢條件</param>
        /// <returns>查詢結果</returns>
        Task<IResult<IEnumerable<UserModel>>> GetUsersAsync(UserModel query);
    }
}