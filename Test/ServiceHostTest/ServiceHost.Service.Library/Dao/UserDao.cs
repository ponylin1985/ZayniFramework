using ServiceHost.Test.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.DataAccess;
using ZayniFramework.DataAccess.Lightweight;


namespace ServiceHost.Service.DataAccess
{
    /// <summary>測試用 BaseDataAccess 測試 UserDao 類別
    /// </summary>
    public class UserDao : BaseDataAccess, IUserDao
    {
        #region Constructors

        /// <summary>預設建構子
        /// </summary>
        public UserDao() : base("ZayniUnitTest")
        {
        }

        #endregion Constructors


        #region Public Methods

        /// <summary>新增資料 ExecuteNoeQuery 測試
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <returns>新增結果</returns>
        public async Task<IResult> InsertAsync(UserModel model)
        {
            var result = Result.Create();

            var sql = @"
                INSERT INTO `FS_UNIT_TEST_USER` (
                      `ACCOUNT_ID`
                    , `NAME`
                    , `AGE`
                    , `SEX`
                    , `BIRTHDAY`
                    , `IS_VIP`
                    , `IS_GOOD`
                ) VALUES (
                      @AccountId
                    , @Name
                    , @Age
                    , @Sex
                    , @Birthday
                    , @IsVip
                    , @IsGood
                ); ";

            using (var conn = await base.CreateConnectionAsync())
            using (var trans = base.BeginTransaction(conn))
            {
                var cmd = GetSqlStringCommand(sql, conn, trans);
                base.AddInParameter(cmd, "@AccountId", DbColumnType.String, model.Account);
                base.AddInParameter(cmd, "@Name", DbColumnType.String, model.Name);
                base.AddInParameter(cmd, "@Age", DbColumnType.Int32, model.Age);
                base.AddInParameter(cmd, "@Sex", DbColumnType.Int32, model.Sex);
                base.AddInParameter(cmd, "@Birthday", DbColumnType.DateTime, model.DoB);
                base.AddInParameter(cmd, "@IsVip", DbColumnType.Boolean, model.IsVip);
                base.AddInParameter(cmd, "@IsGood", DbColumnType.Boolean, model.IsGood);

                if (!await base.ExecuteNonQueryAsync(cmd))
                {
                    base.Rollback(trans);
                    await conn.CloseAsync();
                    result.Message = base.Message;
                    return result;
                }

                if (1 != DataCount)
                {
                    base.Rollback(trans);
                    await conn.CloseAsync();
                    result.Message = $"Insert into test data to FS_UNIT_TEST_USER fail.";
                    return result;
                }

                if (!base.Commit(trans))
                {
                    await conn.CloseAsync();
                    result.Message = base.Message;
                    return result;
                }

                await conn.CloseAsync();
            }

            result.Success = true;
            return result;
        }

        /// <summary>更新資料 ExecuteNoeQuery 測試
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <returns>更新結果</returns>
        public async Task<IResult> UpdateAsync(UserModel model)
        {
            var result = Result.Create();

            var sql = @"
                UPDATE `FS_UNIT_TEST_USER`
                   SET `NAME`       = @Name,
                       `SEX`        = @Sex,
                       `AGE`        = @Age,
                       `BIRTHDAY`   = @Birthday
                 WHERE `ACCOUNT_ID` = @AccountId; ";

            using (var conn = await base.CreateConnectionAsync())
            using (var trans = base.BeginTransaction(conn))
            {
                var cmd = GetSqlStringCommand(sql, conn, trans);
                base.AddInParameter(cmd, "@Name", DbColumnType.String, model.Name);
                base.AddInParameter(cmd, "@Sex", DbColumnType.Int32, model.Sex);
                base.AddInParameter(cmd, "@Age", DbColumnType.Int32, model.Age);
                base.AddInParameter(cmd, "@Birthday", DbColumnType.DateTime, model.DoB);
                base.AddInParameter(cmd, "@AccountId", DbColumnType.String, model.Account);

                if (!await base.ExecuteNonQueryAsync(cmd))
                {
                    base.Rollback(trans);
                    await conn.CloseAsync();
                    result.Message = base.Message;
                    return result;
                }

                if (1 != DataCount)
                {
                    base.Rollback(trans);
                    await conn.CloseAsync();
                    result.Message = $"Update test data to FS_UNIT_TEST_USER fail.";
                    return result;
                }

                if (!base.Commit(trans))
                {
                    await conn.CloseAsync();
                    result.Message = base.Message;
                    return result;
                }

                await conn.CloseAsync();
            }

            result.Success = true;
            return result;
        }

        /// <summary>刪除資料 ExecuteNoeQuery 測試
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <returns>刪除結果</returns>
        public async Task<IResult> DeleteAsync(UserModel model)
        {
            var result = Result.Create();

            var sql = @" DELETE FROM `FS_UNIT_TEST_USER`
                             WHERE `ACCOUNT_ID` = @AccountId; ";

            using (var conn = await base.CreateConnectionAsync())
            using (var trans = base.BeginTransaction(conn))
            {
                var cmd = GetSqlStringCommand(sql, conn, trans);
                base.AddInParameter(cmd, "@AccountId", DbColumnType.String, model.Account);

                if (!await base.ExecuteNonQueryAsync(cmd))
                {
                    base.Rollback(trans);
                    await conn.CloseAsync();
                    result.Message = base.Message;
                    return result;
                }

                if (1 != DataCount)
                {
                    base.Rollback(trans);
                    await conn.CloseAsync();
                    result.Message = $"Delete test data from FS_UNIT_TEST_USER fail.";
                    return result;
                }

                if (!base.Commit(trans))
                {
                    await conn.CloseAsync();
                    result.Message = base.Message;
                    return result;
                }

                await conn.CloseAsync();
            }

            result.Success = true;
            return result;
        }

        /// <summary>查詢資料 LoadDataToModel 測試
        /// </summary>
        /// <param name="query">查詢條件</param>
        /// <returns>查詢結果</returns>
        public async Task<IResult<UserModel>> GetAsync(UserModel query)
        {
            var result = Result.Create<UserModel>();

            var sqlWhere = "";
            query.Account.IsNotNullOrEmpty(s => sqlWhere += $"AND `ACCOUNT_ID` = @AccountId");
            query.Name.IsNotNullOrEmpty(s => sqlWhere += $"{Environment.NewLine}AND `NAME` = @Name");

            var sql = $@"
                SELECT
                    `ACCOUNT_ID`
                  , `NAME`
                  , `AGE`
                  , `SEX`
                  , `BIRTHDAY`

                  , `IS_VIP`
                  , `IS_GOOD`
                  , `DATA_FLAG`
                 FROM `FS_UNIT_TEST_USER`
                WHERE 1 = 1
                  {sqlWhere}; ";

            using var conn = await base.CreateConnectionAsync();
            var cmd = base.GetSqlStringCommand(sql, conn);
            query.Account.IsNotNullOrEmpty(n => base.AddInParameter(cmd, "@AccountId", DbColumnType.String, query.Account));
            query.Name.IsNotNullOrEmpty(m => base.AddInParameter(cmd, "@Name", DbColumnType.String, query.Name));

            var r = await base.LoadDataToModelAsync<UserModel>(cmd);

            if (!r.Success)
            {
                await conn.CloseAsync();
                result.Message = base.Message;
                return result;
            }

            await conn.CloseAsync();
            var models = r.Data;

            if (models.IsNullOrEmpty() || models.Count() != 1)
            {
                result.Message = $"No data found.";
                result.Code = StatusConstant.NO_DATA_FOUND;
                result.Success = true;
                return result;
            }

            var model = models.FirstOrDefault();

            result.Data = model;
            result.Success = true;
            return result;
        }

        /// <summary>查詢資料 LoadDataToModel 測試
        /// </summary>
        /// <param name="query">查詢條件</param>
        /// <returns>查詢結果</returns>
        public async Task<IResult<IEnumerable<UserModel>>> GetUsersAsync(UserModel query)
        {
            var result = Result.Create<IEnumerable<UserModel>>();

            var sql = @"
                SELECT
                    `ACCOUNT_ID`
                  , `NAME`
                  , `AGE`
                  , `SEX`
                  , `BIRTHDAY`

                  , `IS_VIP`
                  , `IS_GOOD`
                  , `DATA_FLAG`
                 FROM `FS_UNIT_TEST_USER`
                WHERE `SEX` = @Sex; ";

            using var conn = base.CreateConnection();
            var cmd = base.GetSqlStringCommand(sql, conn);
            base.AddInParameter(cmd, "@Sex", DbColumnType.Int32, query.Sex);

            var g = await base.LoadDataToModelAsync<UserModel>(cmd);

            if (!g.Success)
            {
                await conn.CloseAsync();
                result.Message = base.Message;
                return result;
            }

            await conn.CloseAsync();
            var models = g.Data;

            result.Data = models;
            result.Success = true;
            return result;
        }

        #endregion Public Methods
    }
}