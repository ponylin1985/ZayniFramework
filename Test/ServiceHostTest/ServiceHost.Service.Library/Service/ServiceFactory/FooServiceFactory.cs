using ServiceHost.Test.Entity;


namespace ServiceHost.Service
{
    /// <summary>IFooService 服務工廠
    /// </summary>
    public class FooServiceFactory : IFooServiceFactory
    {
        /// <summary>建立 IFooService 服務實作
        /// </summary>
        /// <param name="request">請求資料載體</param>
        /// <returns>IFooService 服務實作</returns>
        public IFooService Create(SomethingDTO request)
        {
            using var serviceScope = ServiceLocator.ResolveServiceScope();

            if (request.LuckyNumber > 1000)
            {
                return serviceScope.ResolveService<IFooService>();
            }

            return serviceScope.ResolveService<IBarService>();
        }
    }
}