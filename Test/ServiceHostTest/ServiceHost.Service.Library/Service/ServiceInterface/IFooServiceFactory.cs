using ServiceHost.Test.Entity;


namespace ServiceHost.Service
{
    /// <summary>IFooService 服務工廠介面
    /// </summary>
    public interface IFooServiceFactory
    {
        /// <summary>建立 IFooService 服務實作
        /// </summary>
        /// <param name="request">請求資料載體</param>
        /// <returns>IFooService 服務實作</returns>
        IFooService Create(SomethingDTO request);
    }
}