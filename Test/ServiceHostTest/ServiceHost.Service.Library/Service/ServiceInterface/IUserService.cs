using ServiceHost.Test.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;
using ZayniFramework.Common;


namespace ServiceHost.Service
{
    /// <summary>使用者服務介面
    /// </summary>
    public interface IUserService
    {
        /// <summary>查詢使用者清單
        /// </summary>
        /// <param name="request">查詢參數</param>
        /// <returns>查詢結果</returns>
        Task<IResult<IEnumerable<UserDTO>>> GetUsersAsync(UserDTO request);

        /// <summary>查詢使用者資料
        /// </summary>
        /// <param name="request">查詢參數</param>
        /// <returns>查詢結果</returns>
        Task<IResult<UserDTO>> GetAsync(UserDTO request);

        /// <summary>新增使用者
        /// </summary>
        /// <param name="request">請求資料載體</param>
        /// <returns>新增結果</returns>
        Task<IResult<UserDTO>> CreateAsync(UserDTO request);

        /// <summary>更新使用者
        /// </summary>
        /// <param name="request">請求資料載體</param>
        /// <returns>更新結果</returns>
        Task<IResult<UserDTO>> UpdateAsync(UserDTO request);

        /// <summary>刪除使用者
        /// </summary>
        /// <param name="request">請求資料載體</param>
        /// <returns>刪除結果</returns>
        Task<IResult> DeleteAsync(UserDTO request);
    }
}