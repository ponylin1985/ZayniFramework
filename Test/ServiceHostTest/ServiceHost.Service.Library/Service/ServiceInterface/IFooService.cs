using ServiceHost.Test.Entity;
using System.Threading.Tasks;
using ZayniFramework.Common;


namespace ServiceHost.Service
{
    /// <summary>FooService 服務介面
    /// </summary>
    public interface IFooService
    {
        /// <summary>查詢某些 Foo 的資料
        /// </summary>
        /// <param name="request">請求資料載體</param>
        /// <returns>查詢結果</returns>
        Task<IResult<MagicDTO>> GetSomethingAsync(SomethingDTO request);

        /// <summary>執行測試用的 Magic 動作
        /// </summary>
        /// <param name="request">請求資料載體</param>
        /// <returns>執行結果</returns>
        Task<Result<MagicDTO>> DoMagicAsync(SomethingDTO request);
    }

    /// <summary>Bar 服務介面
    /// </summary>
    public interface IBarService : IFooService
    {
    }
}