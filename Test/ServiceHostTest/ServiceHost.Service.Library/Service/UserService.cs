using ServiceHost.Service.DataAccess;
using ServiceHost.Test.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.DataAccess.Lightweight;
using ZayniFramework.Middle.Service.Entity;


namespace ServiceHost.Service
{
    /// <summary>測試用的使用者服務
    /// </summary>
    public class UserService : IUserService
    {
        #region Private Fields

        /// <summary>資料存取物件
        /// </summary>
        private readonly IUserDao _dao;

        #endregion Private Fields


        #region Constructors

        /// <summary>預設建構子
        /// </summary>
        public UserService()
        {
            using var serviceScope = ServiceLocator.ResolveServiceScope();
            _dao = serviceScope.ResolveService<IUserDao>();
        }

        #endregion Constructors


        #region Interface Methods

        /// <summary>查詢使用者清單
        /// </summary>
        /// <param name="request">查詢參數</param>
        /// <returns>查詢結果</returns>
        public async Task<IResult<IEnumerable<UserDTO>>> GetUsersAsync(UserDTO request)
        {
            var result = Result.Create<IEnumerable<UserDTO>>();

            if (request.Sex > 1 || request.Sex < 0)
            {
                result.Message = $"Invalid request. Sex is invalid.";
                result.Code = StatusCode.INVALID_REQUEST;
                return result;
            }

            var args = new UserModel()
            {
                Sex = request.Sex
            };

            var r = await Proxy.InvokeAsync(
                () => GetUsersAsync(),
                retryOption: new RetryOption()
                {
                    EnableRetry = true,
                    RetryFrequencies = new TimeSpan[]
                    {
                        TimeSpan.FromSeconds( 1 ),
                        TimeSpan.FromSeconds( 1 ),
                        TimeSpan.FromSeconds( 2 ),
                        TimeSpan.FromSeconds( 2 ),
                        TimeSpan.FromSeconds( 5 ),
                    },
                },
                retryWhen: r => !r.Success && r.Code != StatusConstant.NO_DATA_FOUND,
                returnWhenRetryFail: s => s
            );

            if (!r.Success)
            {
                result.Message = $"Get user data from database fail. {r.Message}";
                result.Code = StatusCode.INTERNAL_ERROR;
                return result;
            }

            var models = r.Data;

            result.Data = models.Select(m => m.Map());
            result.Code = r.Code;
            result.Message = r.Message;
            result.Success = true;
            return result;

            async Task<IResult<IEnumerable<UserModel>>> GetUsersAsync() =>
                (await _dao
                    .GetUsersAsync(args))
                    .WhenFailure(r => Result.Create<IEnumerable<UserModel>>(false, code: StatusCode.INTERNAL_ERROR, message: $"Get user data from database fail. {r.Message}"))
                    .WhenSuccess(r => r.Data.IsNullOrEmpty(), r => Result.Create(true, data: Enumerable.Empty<UserModel>(), code: StatusConstant.NO_DATA_FOUND, message: $"No data found."))
                    .WhenSuccessWithData(r => Result.Create(true, r.Data, StatusCode.ACTION_SUCCESS));
        }

        /// <summary>查詢使用者資料
        /// </summary>
        /// <param name="request">查詢參數</param>
        /// <returns>查詢結果</returns>
        public async Task<IResult<UserDTO>> GetAsync(UserDTO request)
        {
            var result = Result.Create<UserDTO>();

            if (request.Account.IsNullOrEmpty() && request.Name.IsNullOrEmpty())
            {
                result.Message = $"Invalie request. The 'account_id' and 'name' can not both null or empty string.";
                result.Code = StatusCode.INVALID_REQUEST;
                return result;
            }

            var req = new UserModel()
            {
                Account = request.Account,
                Name = request.Name
            };

            var r = await Proxy.InvokeAsync(
                () => GetAsync(),
                retryOption: new RetryOption()
                {
                    EnableRetry = true,
                    RetryFrequencies = new TimeSpan[]
                    {
                        TimeSpan.FromSeconds( 1 ),
                        TimeSpan.FromSeconds( 1 ),
                        TimeSpan.FromSeconds( 2 ),
                        TimeSpan.FromSeconds( 2 ),
                        TimeSpan.FromSeconds( 5 ),
                    },
                },
                retryWhen: r => !r.Success && r.Code != StatusConstant.NO_DATA_FOUND,
                returnWhenRetryFail: s => s
            );

            if (!r.Success)
            {
                result.Message = $"Get user data from database fail. {r.Message}";
                result.Code = StatusCode.INTERNAL_ERROR;
                return result;
            }

            var model = r.Data;
            result.Data = model.Map();
            result.Code = r.Code;
            result.Message = r.Message;
            result.Success = true;
            return result;

            async Task<IResult<UserModel>> GetAsync() =>
                (await _dao
                    .GetAsync(req))
                    .WhenFailure(r => Result.Create(false, data: default(UserModel), code: StatusCode.INTERNAL_ERROR, message: $"Get user data from database fail. {r.Message}"))
                    .WhenSuccess(r => r.Data.IsNull(), r => Result.Create(true, data: default(UserModel), code: StatusConstant.NO_DATA_FOUND, message: $"No data found."))
                    .WhenSuccess(r => r.Data.IsNotNull(), r => Result.Create(true, data: r.Data, code: StatusCode.ACTION_SUCCESS));
        }

        /// <summary>新增使用者
        /// </summary>
        /// <param name="request">請求資料載體</param>
        /// <returns>新增結果</returns>
        public async Task<IResult<UserDTO>> CreateAsync(UserDTO request)
        {
            var result = Result.Create<UserDTO>();

            var model = request.Map();
            model.Account = RandomTextHelper.Create(10);

            var r = await _dao.InsertAsync(model);

            if (!r.Success)
            {
                result.Message = $"Insert user data to database fail. {r.Message}";
                result.Code = StatusCode.INTERNAL_ERROR;
                return result;
            }

            var g = await _dao.GetAsync(model);

            if (!g.Success || g.Data.IsNull())
            {
                result.Message = $"Insert user data to database fail due to can not get created data after insert. {g.Message}";
                result.Code = StatusCode.INTERNAL_ERROR;
                return result;
            }

            var resModel = g.Data;
            var resDTO = resModel.Map();
            result.Data = resDTO;
            result.Code = StatusCode.ACTION_SUCCESS;
            result.Success = true;
            return result;
        }

        /// <summary>更新使用者
        /// </summary>
        /// <param name="request">請求資料載體</param>
        /// <returns>更新結果</returns>
        public async Task<IResult<UserDTO>> UpdateAsync(UserDTO request)
        {
            var result = Result.Create<UserDTO>();
            var model = request.Map();

            var r = await _dao.UpdateAsync(model);

            if (!r.Success)
            {
                result.Message = $"Update user data to database fail. {r.Message}";
                result.Code = StatusCode.INTERNAL_ERROR;
                return result;
            }

            var g = await _dao.GetAsync(model);

            if (!g.Success || g.Data.IsNull())
            {
                result.Message = $"Update user data to database fail due to can not get created data after insert. {g.Message}";
                result.Code = StatusCode.INTERNAL_ERROR;
                return result;
            }

            var resModel = g.Data;
            var resDTO = resModel.Map();
            result.Data = resDTO;
            result.Code = StatusCode.ACTION_SUCCESS;
            result.Success = true;
            return result;
        }

        /// <summary>刪除使用者
        /// </summary>
        /// <param name="request">請求資料載體</param>
        /// <returns>刪除結果</returns>
        public async Task<IResult> DeleteAsync(UserDTO request)
        {
            var result = Result.Create();

            var model = new UserModel()
            {
                Account = request.Account
            };

            var r = await _dao.DeleteAsync(model);

            if (!r.Success)
            {
                result.Message = $"Delete user data from database fail. {r.Message}";
                result.Code = StatusCode.INTERNAL_ERROR;
                return result;
            }

            result.Code = StatusCode.ACTION_SUCCESS;
            result.Success = true;
            return result;
        }

        #endregion Interface Methods
    }
}