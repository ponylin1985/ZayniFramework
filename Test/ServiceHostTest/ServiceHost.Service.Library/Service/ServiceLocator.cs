using Microsoft.Extensions.DependencyInjection;
using ServiceHost.Service.DataAccess;
using System;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ServiceHost.Service
{
    /// <summary>服務定位器
    /// </summary>
    public static class ServiceLocator
    {
        #region Public Properties

        /// <summary>服務集合
        /// </summary>
        /// <value></value>
        public static IServiceCollection ServiceCollection { get; private set; } = new ServiceCollection();

        /// <summary>服務容器供應者
        /// </summary>
        /// <value></value>
        public static IServiceProvider ServiceProvider { get; private set; }

        #endregion Public Properties


        #region Static Constructors

        /// <summary>靜態建構子
        /// </summary>
        /// <returns></returns>
        static ServiceLocator() => Initial();

        #endregion Static Constructors


        #region Public Methods

        /// <summary>註冊服務相依性
        /// </summary>
        /// <param name="configureServicesHandler">註冊服務處理委派</param>
        public static void ConfigureServices(Action<IServiceCollection> configureServicesHandler)
        {
            configureServicesHandler(ServiceCollection);
            ServiceProvider = ServiceCollection.BuildServiceProvider(true);
        }

        /// <summary>清空所有註冊的服務
        /// </summary>
        public static void ClearServices()
        {
            ServiceCollection.Clear();
            ServiceProvider = null;
        }

        /// <summary>取得服務的生命週期作用域
        /// </summary>
        /// <returns>服務的生命週期作用域</returns>
        public static IServiceScope ResolveServiceScope()
        {
            if (ServiceProvider.IsNull())
            {
                throw new InvalidOperationException($"The service locator is not initialized.");
            }

            var serviceScope = ServiceProvider.CreateScope();
            return serviceScope;
        }

        /// <summary>取得服務實作
        /// </summary>
        /// <typeparam name="TService">服務介面泛型</typeparam>
        /// <returns>服務實作物件</returns>
        public static TService ResolveService<TService>()
        {
            if (ServiceProvider.IsNull())
            {
                throw new InvalidOperationException($"The service locator is not initialized.");
            }

            var service = ServiceProvider.GetService<TService>();
            return service;
        }

        /// <summary>取得服務實作
        /// </summary>
        /// <param name="serviceScope">服務的生命週期作用域</param>
        /// <typeparam name="TService">服務介面泛型</typeparam>
        /// <returns>服務實作物件</returns>
        public static TService ResolveService<TService>(this IServiceScope serviceScope)
        {
            var service = serviceScope.ServiceProvider.GetService<TService>();
            return service;
        }

        #endregion Public Methods


        #region Private Methods

        /// <summary>註冊服務到服務定位器容器中
        /// </summary>
        private static void Initial()
        {
            try
            {
                // 服務實作與 Dao 實作相依性注入
                ServiceCollection
                    .AddSingleton<IFooServiceFactory, FooServiceFactory>()
                    .AddScoped<IFooService, FooService>()
                    .AddScoped<IBarService, BarService>()
                    .AddScoped<IUserService, UserService>()
                    .AddScoped<IUserDao, UserDao>();

                ServiceProvider = ServiceCollection.BuildServiceProvider(true);
            }
            catch (Exception ex)
            {
                Logger.Error(nameof(ServiceLocator), $"{nameof(ServiceLocator)} inital service dependencics occur exception. {ex}", $"{nameof(ServiceLocator)}.{nameof(Initial)}");
                throw;
            }
        }

        #endregion Private Methods
    }
}