using ServiceHost.Test.Entity;
using System;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Logging;
using ZayniFramework.Middle.Service.Entity;
using ZayniFramework.Serialization;


namespace ServiceHost.Service
{
    /// <summary>BarService 服務
    /// </summary>
    public class BarService : IBarService
    {
        #region Interface Methods

        /// <summary>查詢某些 Foo 的資料
        /// </summary>
        /// <param name="request">請求資料載體</param>
        /// <returns>查詢結果</returns>
        public Task<IResult<MagicDTO>> GetSomethingAsync(SomethingDTO request) =>
            Task.FromResult((IResult<MagicDTO>)Result.Create(true, code: StatusCode.ACTION_SUCCESS, message: RandomTextHelper.Create(8), data: new MagicDTO()
            {
                MagicWords = $"This is {nameof(BarService)}!!!",
                TestDouble = request.LuckyNumber,
                MagicTime = DateTime.Today.ToUtcKind()
            }));

        /// <summary>執行測試用的 Magic 動作
        /// </summary>
        /// <param name="request">請求資料載體</param>
        /// <returns>執行結果</returns>
        public Task<Result<MagicDTO>> DoMagicAsync(SomethingDTO request)
        {
            using var trace = LogTracerContainer.CreateLogTracer();
            var result = Result.Create<MagicDTO>();

            Logger.Info(this, $"Receive SomethingDTO is {Environment.NewLine}{JsonSerialize.SerializeObject(request)}", nameof(DoMagicAsync));

            var resDTO = new MagicDTO()
            {
                MagicWords = $"{RandomTextHelper.Create(8)}",
                MagicTime = DateTime.UtcNow
            };

            When.True(7.77 == request.SomeMagicNumber, () => resDTO.TestDouble = 999.999);

            result.Data = resDTO;
            result.Success = true;
            return Task.FromResult(result);
        }

        #endregion Interface Methods
    }
}