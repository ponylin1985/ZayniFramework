using System;
using ServiceHost.Test.Entity;
using ZayniFramework.Common;

namespace ServiceHost.Service
{
    /// <summary>資料對應轉換器
    /// </summary>
    public static class Mapper
    {
        /// <summary>對應成 ORM 資料模型
        /// </summary>
        /// <param name="source">DTO 資料載體</param>
        /// <returns>ORM 資料模型</returns>
        public static UserModel Map(this UserDTO source) => source.Map<UserDTO, UserModel>(d => new UserModel
        {
            Account = d.Account,
            Name = d.Name,
            Age = d.Age,
            Sex = d.Sex,
            DoB = d.DoB,
            IsVip = d.IsVip,
            IsGood = d.IsGood,
            DataFlag = Convert.ToInt64(d.DataFlag)
        });

        /// <summary>對應成 DTO 資料載體
        /// </summary>
        /// <param name="source">ORM 資料模型</param>
        /// <returns>DTO 資料載體</returns>
        public static UserDTO Map(this UserModel source) => source.Map<UserModel, UserDTO>(m => new UserDTO
        {
            Account = m.Account,
            Name = m.Name,
            Age = m.Age,
            Sex = m.Sex,
            DoB = m.DoB,
            IsVip = m.IsVip,
            IsGood = m.IsGood,
            DataFlag = m.DataFlag + ""
        });
    }
}