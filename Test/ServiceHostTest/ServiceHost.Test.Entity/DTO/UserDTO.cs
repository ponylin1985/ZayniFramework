using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;


namespace ServiceHost.Test.Entity
{
    /// <summary>使用者帳戶的資料載體 (測試使用)
    /// </summary>
    public class UserDTO
    {
        /// <summary>帳號
        /// </summary>
        [JsonProperty(PropertyName = "account_id")]
        [JsonPropertyName("account_id")]
        public string Account { get; set; }

        /// <summary>姓名
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        [JsonPropertyName("name")]
        public string Name { get; set; }

        /// <summary>年齡
        /// </summary>
        [JsonProperty(PropertyName = "age")]
        [JsonPropertyName("age")]
        public int Age { get; set; }

        /// <summary>性別
        /// </summary>
        [JsonProperty(PropertyName = "sex")]
        [JsonPropertyName("sex")]
        public int Sex { get; set; }

        /// <summary>生日
        /// </summary>
        [JsonProperty(PropertyName = "birthday")]
        [JsonPropertyName("birthday")]
        public DateTime? DoB { get; set; }

        /// <summary>是否為VIP會員
        /// </summary>
        [JsonProperty(PropertyName = "is_vip")]
        [JsonPropertyName("is_vip")]
        public bool? IsVip { get; set; }

        /// <summary>只有非 Nullable 的型別，才會支援查詢 DefaultValue 預設值機制。
        /// </summary>
        [JsonProperty(PropertyName = "is_good")]
        [JsonPropertyName("is_good")]
        public bool IsGood { get; set; }

        /// <summary>資料異動時間戳記。<para/>
        /// * 為了避免不同請求端程式語言對於 Int64 的精準度，因此建議這種 timestamp 的 date_flag 欄位最好宣告為 string 型別在 API contract 上傳遞。<para/>
        /// * 譬如最常見的 JavaScript 請求端對於 Int64 的精準度就可能會有差異。
        /// </summary>
        [JsonProperty(PropertyName = "data_flag")]
        [JsonPropertyName("data_flag")]
        public string DataFlag { get; set; }

        /// <summary>連絡電話資料集合
        /// </summary>
        [JsonProperty(PropertyName = "phone_details")]
        [JsonPropertyName("phone_details")]
        public List<UserPhoneDTO> PhoneDetails { get; set; }
    }
}