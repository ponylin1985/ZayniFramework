class HttpClientProxy {
  constructor(remoteServiceName, remoteServiceBaseUrl) {
    if (!remoteServiceName) {
      throw new Error("The value of 'remoteServiceName' argument is not allowed null or empty.");
    }
    if (!remoteServiceBaseUrl) {
      throw new Error("The value of 'remoteServiceBaseUrl' argument is not allowed null or empty.");
    }
    this.serviceName = remoteServiceName;
    this.baseUrl = remoteServiceBaseUrl;
  }
  createRandomString(length) {
    let result = '';
    let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }
  async postAsync(actionName, data) {
    let result = {
      success: false,
      data: null,
      code: '',
      message: '',
    };
    if (!actionName) {
      result.code = 'ERROR';
      result.message = "The value of 'actionName' argument is not allowed null or empty.";
      return result;
    }
    if (!data) {
      result.code = 'ERROR';
      result.message = "The value of 'data' argument is not allowed null or empty.";
      return result;
    }
    let requestPackage = {
      packageType: 'RPCRequest',
      serviceName: this.serviceName,
      reqId: this.createRandomString(15),
      action: actionName,
      data: data,
      reqTime: new Date(),
    };
    try {
      let response = await axios.post(this.baseUrl, requestPackage);
      if (response.status !== 200 || !response.data) {
        result.message = 'Remote http service response error.';
        result.code = 'ERROR';
        return result;
      }
      let responsePackage = response.data;
      if (!responsePackage.success) {
        result.code = responsePackage.code;
        result.message = responsePackage.message;
        return result;
      }
      return responsePackage.data;
    } catch (error) {
      result.code = 'EXCEPTION';
      result.message = `An error occur while sending http RPC request to remote http service. ${error.message}`;
      return result;
    }
  }
}