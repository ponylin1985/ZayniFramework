﻿using System;
using System.Threading.Tasks;


namespace AsyncAwait.ConsoleApp.Test
{
    /// <summary>主程式
    /// </summary>
    class TaskTest
    {
        /// <summary>執行結果如下:<para/>
        /// BBB<para/>
        /// AAA<para/>
        /// 基本使用情境:<para/>
        /// * 如果執行的操作屬於 CPU-bound，並且原本呼叫端的程式碼不需等待操作結果時，則可以使用 Task.Factory.StartNew() 方法呼叫，因為會建立一個額外外層的 Task。<para/>
        /// * 但使用 Task.Factory.StartNew() 方法，也會消耗較多的 Task 與記憶體。<para/>
        /// </summary>
        /// <returns></returns>
        static async Task Main_Test1()
        {
            // Task.Factory.StartNew() 方法實際上回傳 Task<Task>，因此這邊只有 await 外層的 Task，故不會 await 內層的 Task。
            await Task.Factory.StartNew(DoSomethingAsync);
            await Console.Out.WriteLineAsync("BBB");
            await Console.In.ReadLineAsync();
        }

        /// <summary>執行結果如下:<para/>
        /// AAA<para/>
        /// BBB<para/>
        /// </summary>
        /// <returns></returns>
        static async Task Main_Test2()
        {
            // Task.Factory.StartNew() 方法實際上回傳 Task<Task>，但隨後又呼叫了 Unwrap() 方法，則會回傳內層的 Task，故這邊是 await 內層的 Task。
            await Task.Factory.StartNew(DoSomethingAsync).Unwrap();
            await Console.Out.WriteLineAsync("BBB");
            await Console.In.ReadLineAsync();
        }

        /// <summary>執行結果如下:<para/>
        /// AAA<para/>
        /// BBB<para/>
        /// </summary>
        /// <returns></returns>
        static async Task Main_Test3()
        {
            // Task.Factory.StartNew() 方法實際上回傳 Task<Task>
            // 第一個右邊的 await 是等待外層的 Task 得到內層的 Task，第二個左邊的 await 是等待內層的 Task。
            // 效果等同 await Task.Factory.StartNew().Unwarp()，但此寫法稍微比較節省一點記憶體資源。
            // https://stackoverflow.com/questions/34816628/await-await-vs-unwrap
            await await Task.Factory.StartNew(DoSomethingAsync);
            await Console.Out.WriteLineAsync("BBB");
            await Console.In.ReadLineAsync();
        }

        /// <summary>執行結果如下:<para/>
        /// AAA<para/>
        /// BBB<para/>
        /// 基本使用情境:<para/>
        /// * 如果要執行的操作屬於 CPU-bound，而且期望以背景的 .NET Thread Pool 工作執行。<para/>
        /// * 並且呼叫端程式碼如果需要 await 等待這個 CPU-bound 的操作，則可以使用 await Task.Run() 的方式呼叫。
        /// </summary>
        /// <returns></returns>
        static async Task Main_Test4()
        {
            // Task.Run() 實際上直接回傳一個 Task，因此這邊直接 await 這個 Task。
            await Task.Run(DoSomethingAsync);
            await Console.Out.WriteLineAsync("BBB");
            await Console.In.ReadLineAsync();
        }

        /// <summary>執行結果如下:<para/>
        /// AAA<para/>
        /// BBB<para/>
        /// 基本使用情境:<para/>
        /// * 如果要執行的操作屬於 IO-bound，譬如 disk IO 或 network IO，並且呼叫端需要等待 IO 的結果，則建議值些使用 async/await 方式呼叫。
        /// </summary>
        /// <returns></returns>
        static async Task Main_Test5()
        {
            await DoSomethingAsync();
            await Console.Out.WriteLineAsync("BBB");
            await Console.In.ReadLineAsync();
        }

        /// <summary>執行結果如下:<para/>
        /// AAA<para/>
        /// BBB<para/>
        /// </summary>
        /// <returns></returns>
        static async Task Main_Test6()
        {
            await Task.Run(async () =>
            {
                await Task.Delay(1000 * 5);
                await Console.Out.WriteLineAsync("AAA");
            });

            await Console.Out.WriteLineAsync("BBB");
            await Console.In.ReadLineAsync();
        }

        /// <summary>執行結果如下:<para/>
        /// BBB<para/>
        /// AAA<para/>
        /// </summary>
        /// <returns></returns>
        static async Task Main_Test7()
        {
            // 只等待 await 外層的 Task，因此內層的 Task 沒有 await 等待。
            await Task.Factory.StartNew(async () =>
            {
                await Task.Delay(1000 * 5);
                await Console.Out.WriteLineAsync("AAA");
            });

            await Console.Out.WriteLineAsync("BBB");
            await Console.In.ReadLineAsync();
        }

        /// <summary>執行結果如下:<para/>
        /// AAA<para/>
        /// BBB<para/>
        /// </summary>
        /// <returns></returns>
        static async Task Main_Test8()
        {
            // await 等待內層的 Task。
            await Task.Factory.StartNew(async () =>
            {
                await Task.Delay(1000 * 5);
                await Console.Out.WriteLineAsync("AAA");
            }).Unwrap();

            await Console.Out.WriteLineAsync("BBB");
            await Console.In.ReadLineAsync();
        }

        /// <summary>執行結果如下:<para/>
        /// AAA<para/>
        /// BBB<para/>
        /// </summary>
        /// <returns></returns>
        static async Task Main_Test9()
        {
            // await 等待外層與內層的 Task。
            await await Task.Factory.StartNew(async () =>
            {
                await Task.Delay(1000 * 5);
                await Console.Out.WriteLineAsync("AAA");
            });

            await Console.Out.WriteLineAsync("BBB");
            await Console.In.ReadLineAsync();
        }

        /// <summary>等待 5 秒後印出 AAA
        /// </summary>
        /// <returns></returns>
        static async Task DoSomethingAsync()
        {
            await Task.Delay(1000 * 5);
            await Console.Out.WriteLineAsync("AAA");
        }
    }
}
