﻿using System;
using System.Threading;
using System.Threading.Tasks;
using ZayniFramework.Common;


namespace AsyncAwait.ConsoleApp.Test
{
    /// <summary>TaskHelper 的測試
    /// </summary>
    class TaskHelperTest
    {
        static async Task Main()
        {
            // 會等
            // await Task.Run( DoSomething );

            // 會等
            // await Task.Factory.StartNew( DoSomething );

            // 不會等: 故意把一個 sync method 包裝成 async method，然後在呼叫端不等待這個被包裝過的 async method。
            // await Task.Factory.StartNew( async () => await TaskHelper.RunAsync( DoSomething ) );

            // 不會等: 因為回傳是 Task<Task>
            // await Task.Factory.StartNew( DoSomethingAsync );

            // 會等: 因為 DoSomethingAsync 本身就是 async method，這邊只是強制它以非同步的方式執行，並且 await 這個非同步 Task。
            await TaskHelper.ForceAsync(DoSomethingAsync);

            // 會等: 因為回傳是 Task
            // await Task.Run( DoSomethingAsync );

            // 會等: 因為雖然故意把一個 sync methodd 包裝成 async method 去執行，但是因為 Task.Run() 的回傳值為包裝過的 Task，外層仍有 await，所以還是會等。
            // await Task.Run( async () => await TaskHelper.RunAsync( DoSomething ) );

            Console.WriteLine("BBB");
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        static void DoSomething()
        {
            SpinWait.SpinUntil(() => false, 5000);
            Console.WriteLine("AAA");
        }

        static async Task DoSomethingAsync()
        {
            await Task.Delay(5000);
            await Console.Out.WriteLineAsync("AAA");
        }

        // private static async Task<T> RunAsync<T>( Func<Task<T>> func )
        // {
        //     await Task.Yield();
        //     return await func();
        // }

        // private static async Task<T> RunAsync<T>( Func<T> func )
        // {
        //     await Task.Yield();
        //     return func();
        // }

        // private static async Task ActionAsync( Action action )
        // {
        //     await Task.Yield();
        //     action();
        // }
    }
}
