﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Elasticsearch.Net;
using Nest;
using Nest.JsonNetSerializer;
using Newtonsoft.Json;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace Logging.ConsoleApp.Test
{
    /// <summary>主程式
    /// </summary>
    class Program
    {
        /// <summary>程式進入點方法
        /// </summary>
        /// <param name="args"></param>
        static void Main2(string[] args)
        {
            var path = $"{Path.GetDirectoryName(Assembly.GetEntryAssembly().Location)}/zayni.json";
            ZayniConfigManagement.Configure(path);

            var testDir = "./log";
            var fullPath = FileHelper.GetFullPath(testDir);
            ConsoleLogger.WriteLine($"FullPath: {fullPath}", ConsoleColor.Green);

            testDir = "/Log";
            fullPath = FileHelper.GetFullPath(testDir, "\\");
            ConsoleLogger.WriteLine($"FullPath: {fullPath}", ConsoleColor.Green);

            testDir = "Log";
            fullPath = FileHelper.GetFullPath(testDir, null);
            ConsoleLogger.WriteLine($"FullPath: {fullPath}", ConsoleColor.Green);

            Console.ReadLine();
        }

        static async Task Main_()
        {
            try
            {
                var esConnString = new Uri("http://localhost:9200");
                var esConnSettings = new ConnectionSettings(new SingleNodeConnectionPool(esConnString), sourceSerializer: JsonNetSerializer.Default).DefaultIndex("dotnet-core-app-log");
                var esClient = new ElasticClient(esConnSettings);

                var searchRequest = new SearchRequest<ElasticsearchLogEntry>
                {
                    Query = new DateRangeQuery
                    {
                        Field = "log_time",
                        LessThan = DateTime.UtcNow.AddDays(-6),
                    },
                    Size = 2,
                };

                while (true)
                {
                    var searchResponse = await esClient.SearchAsync<ElasticsearchLogEntry>(searchRequest);
                    var documentIds = searchResponse.Hits.Select(hit => hit.Id).ToList();

                    if (documentIds.IsNullOrEmpty())
                    {
                        break;
                    }

                    var ids = JsonConvert.SerializeObject(documentIds, Formatting.Indented);
                    System.Console.WriteLine(ids);

                    var deleteRequest = new BulkRequest();

                    foreach (var documentId in documentIds)
                    {
                        System.Console.WriteLine(documentId);
                        var deleteResponse = await esClient.DeleteAsync<ElasticsearchLogEntry>(documentId);

                        if (!deleteResponse.IsValid)
                        {
                            await ConsoleLogger.LogAsync($"An error occured while try to delete the log document. Index: dotnet-app-log, _id: {documentId}.", ConsoleColor.Magenta);
                            continue;
                        }

                        await ConsoleLogger.LogAsync($"Deleted successed. _id: {documentId}.");
                        await Task.Delay(TimeSpan.FromSeconds(1));
                    }
                }
            }
            catch (Exception ex)
            {
                await ConsoleLogger.LogErrorAsync($"An error occur while clearing log messages from elasticsearch service.{Environment.NewLine}{ex}");
                throw;
            }
        }

        /// <summary>Loggger 測試
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            var path = $"{Path.GetDirectoryName(Assembly.GetEntryAssembly().Location)}/zayni.json";
            ZayniConfigManagement.Configure(path);

            Logger.Instance.WriteErrorLogEvent += (sender, message, title, loggerName) =>
            {
                ConsoleLogger.WriteLine($"Ho Ho Ho, this is a error event fired...");
                ConsoleLogger.WriteLine($"LoggerName: {loggerName}.");
                ConsoleLogger.WriteLine($"Error Title: {title}.");
                ConsoleLogger.WriteLine($"Error Message: {message}");
            };

            // ====================

            For.Do(100, () =>
            {
                using (var tracer = LogTracerContainer.CreateLogTracer("TextLogWithEmail"))
                {
                    Logger.Info(nameof(Program), "AAA", Logger.GetTraceLogTitle(nameof(Program), nameof(Main)), "TextLogWithEmail");
                    SpinWait.SpinUntil(() => false, TimeSpan.FromMilliseconds(100));
                    Logger.Info(nameof(Program), "BBB", Logger.GetTraceLogTitle(nameof(Program), nameof(Main)), "TextLogWithEmail");
                    SpinWait.SpinUntil(() => false, TimeSpan.FromMilliseconds(100));
                    Logger.Info(nameof(Program), "BBB", Logger.GetTraceLogTitle(nameof(Program), nameof(Main)), "TextLogWithEmail");
                    SpinWait.SpinUntil(() => false, TimeSpan.FromMilliseconds(100));
                    Logger.Info(nameof(Program), "CCC", Logger.GetTraceLogTitle(nameof(Program), nameof(Main)), "TextLogWithEmail");
                    SpinWait.SpinUntil(() => false, TimeSpan.FromMilliseconds(100));
                    Logger.Info(nameof(Program), "DDD", Logger.GetTraceLogTitle(nameof(Program), nameof(Main)), "TextLogWithEmail");
                    SpinWait.SpinUntil(() => false, TimeSpan.FromMilliseconds(100));
                    Logger.Info(nameof(Program), "EEE", Logger.GetTraceLogTitle(nameof(Program), nameof(Main)), "TextLogWithEmail");
                    SpinWait.SpinUntil(() => false, TimeSpan.FromMilliseconds(100));


                    // Logger.Info( nameof ( Program ), "Start ZayniFramework Logging.Test console.", Logger.GetTraceLogTitle( nameof ( Program ), nameof ( Main ) ), "TextLogWithEmail" );
                    // SpinWait.SpinUntil( () => false, TimeSpan.FromMilliseconds( 20 ) );

                    // Logger.Error( nameof ( Program ), "Something error!!", "MyTest" );
                    // Logger.Error( nameof ( Program ), "Some Error Again!! Testing", "MyTest", loggerName: "TextLogWithEmail" );
                    // SpinWait.SpinUntil( () => false, TimeSpan.FromMilliseconds( 20 ) );

                    // Logger.Info( nameof ( Program ), "End ZayniFramework Logging.Test console.", Logger.GetTraceLogTitle( nameof ( Program ), nameof ( Main ) ), "TextLogWithEmail" );
                    // SpinWait.SpinUntil( () => false, TimeSpan.FromMilliseconds( 20 ) );

                    // ConsoleLogger.WriteLine( "=======" );
                    // ConsoleLogger.Log( "This is a console log message", ConsoleColor.DarkGreen );
                    // ConsoleLogger.LogError( "This is a console log error message" );
                }


            });

            // Logger.Info( nameof ( Program ), "Start ZayniFramework Logging.Test console.", Logger.GetTraceLogTitle( nameof ( Program ), nameof ( Main ) ), "TextLogWithEmail" );
            // SpinWait.SpinUntil( () => false, TimeSpan.FromMilliseconds( 2 ) );

            // Logger.Error( nameof ( Program ), "Something error!!", "MyTest" );
            // Logger.Error( nameof ( Program ), "Some Error Again!! Testing", "MyTest", loggerName: "TextLogWithEmail" );
            // SpinWait.SpinUntil( () => false, TimeSpan.FromMilliseconds( 2 ) );

            // Logger.Info( nameof ( Program ), "End ZayniFramework Logging.Test console.", Logger.GetTraceLogTitle( nameof ( Program ), nameof ( Main ) ), "TextLogWithEmail" );
            // SpinWait.SpinUntil( () => false, TimeSpan.FromMilliseconds( 2 ) );

            // ConsoleLogger.WriteLine( "=======" );
            // ConsoleLogger.Log( "This is a console log message", ConsoleColor.DarkGreen );
            // ConsoleLogger.LogError( "This is a console log error message" );


            // WindowsEventLogger.WriteLog( "ZayniFramework", "Testing", EventLogEntryType.Information, true );
            Console.ReadLine();
        }
    }
}
