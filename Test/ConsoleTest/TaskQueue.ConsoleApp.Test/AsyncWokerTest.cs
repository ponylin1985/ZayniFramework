using System;
using ZayniFramework.Common.Tasks;


namespace QueueTask.Test
{
    /// <summary>AsyncWorker 測試類別
    /// </summary>
    public class AsyncWorkerTest
    {
        /// <summary>非同步工作佇列處理器
        /// </summary>
        /// <returns></returns>
        private readonly AsyncWorker _worker = AsyncWorkerContainer.Resolve("test");

        /// <summary>執行測試
        /// </summary>
        /// <param name="args"></param>
        public void Test()
        {
            _worker.Start();

            for (int i = 0; i < 100; i++)
            {
                int j = i + 1;
                _worker.Enqueue(async () => await Console.Out.WriteLineAsync($"{j}"));
            }
        }
    }
}
