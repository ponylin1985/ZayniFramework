﻿using System;
using System.Threading;
using ZayniFramework.Common;
using ZayniFramework.Common.Tasks;


namespace QueueTask.Test
{
    /// <summary>測試用的資料模型
    /// </summary>
    public class UserModel
    {
        /// <summary>姓名
        /// </summary>
        public string Name { get; set; }

        /// <summary>年齡
        /// </summary>
        public int Age { get; set; }

        /// <summary>性別
        /// </summary>
        public string Sex { get; set; }
    }

    /// <summary>主程式
    /// </summary>
    public class Program
    {
        /// <summary>亂數器
        /// </summary>
        private static readonly Random _random = new Random(Guid.NewGuid().GetHashCode());

        /// <summary>主程式進入點方法
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {
            // 不等待更工作完成的測試
            // ExecuteDirectly();

            // 等待工作完成的測試
            // ExecuteSequentially();

            // AsyncWorker 的測試
            new AsyncWorkerTest().Test();
            Console.WriteLine($"Hello World!");

            Console.ReadLine();
        }

        /// <summary>不等待更工作完成的測試
        /// </summary>
        private static void ExecuteDirectly()
        {
            var queue = new TaskQueue(false);

            for (int i = 0; i < 50; i++)
            {
                int j = i + 1;

                dynamic queueAction = new QueueAction(new Work().DoSomething);
                queueAction.Index = j;
                queueAction.Message = new { Test = "123", NumberNo = 55 };
                queue.EnQueue(queueAction);
            }
        }

        /// <summary>等待工作完成的測試
        /// </summary>
        private static void ExecuteSequentially()
        {
            var queue = new TaskQueue(true);

            for (int i = 0; i < 50; i++)
            {
                dynamic action = new QueueAction(new Work().DoSomething);
                action.Index = i + 1;
                action.Message = new { Test = "123", NumberNo = 55 };

                #region EnQueue 的測試
                //queue.EnQueue( action );
                #endregion EnQueue 的測試

                #region EnQueueForResult 的測試

                dynamic r = queue.EnQueueForResult(action, 1000);

                if (!r.Success)
                {
                    Console.WriteLine(r.Message);
                    continue;
                }

                UserModel model = r.SomeData;
                //Console.WriteLine( $"Result is: {r.Message}" );
                //Console.WriteLine( $"Name: {model.Name}, Age: {model.Age}, Sex: {model.Sex}" );

                #endregion EnQueueForResult 的測試
            }
        }
    }

    /// <summary>工作類別
    /// </summary>
    public class Work
    {
        /// <summary>亂數器
        /// </summary>
        private static readonly Random _random = new Random(Guid.NewGuid().GetHashCode());

        /// <summary>執行工作
        /// </summary>
        /// <param name="args"></param>
        public void DoSomething(dynamic args)
        {
            int sleepTime = _random.Next(0, 70);
            SpinWait.SpinUntil(() => false, sleepTime);   // 模擬每個工作執行的延遲時間都不一樣

            int index = args.Index;

            if (args.IsCancel)
            {
                return;
            }

            dynamic m = args.Message;

            if (null == m)
            {
                return;
            }

            if (0 == sleepTime % 5)
            {
                args.Result = Result.Create(false, "1155", $"Execute fail. No ok {index}. SleepTime is {sleepTime}.");
                Console.WriteLine("Not ok " + index);
                return;
            }

            Console.WriteLine("Ok " + index);

            dynamic result = Result.Create(true, "", $"Execute successfully. {index}");
            result.SomeData = new UserModel() { Name = "Happy Bear", Age = 2, Sex = "male" };
            args.Result = result;
        }
    }
}
