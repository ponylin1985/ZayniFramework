SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FS_UNIT_TEST_PRODUCT](
	[PRODUCT_ID] [uniqueidentifier] NOT NULL,
	[PRODUCT_NAME] [nvarchar](30) NOT NULL,
	[PRODUCT_TYPE] [tinyint] NULL,
	[BUILD_TIME] [datetimeoffset](7) NULL,
	[DATA_FLAG] [timestamp] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FS_UNIT_TEST_PRODUCT] ADD PRIMARY KEY CLUSTERED 
(
	[PRODUCT_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
