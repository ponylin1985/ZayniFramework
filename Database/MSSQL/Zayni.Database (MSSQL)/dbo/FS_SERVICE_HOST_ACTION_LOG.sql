﻿CREATE TABLE [dbo].[FS_SERVICE_HOST_ACTION_LOG] (
    [LOG_SRNO]      BIGINT         IDENTITY (1, 1) NOT NULL,
    [REQUEST_ID]    NCHAR (15)     NULL,
    [SERVICE_NAME]  NVARCHAR (100) NULL,
    [SERVICE_HOST]  NVARCHAR (20)  NULL,
    [DIRECTION]     TINYINT        NOT NULL,
    [ACTION_NAME]   NVARCHAR (100) NOT NULL,
    [DATA_CONTENT]  NTEXT          NULL,
    [REQUEST_TIME]  DATETIME       NULL,
    [RESPONSE_TIME] DATETIME       NULL,
    [IS_SUCCESS]    BIT            NULL,
    [CODE]          NVARCHAR (100) NULL,
    [MESSAGE]       NVARCHAR (500) NULL,
    [LOG_TIME]      DATETIME       NOT NULL,
    CONSTRAINT [PK__FS_SERVI__3345296F9E1870E8] PRIMARY KEY CLUSTERED ([LOG_SRNO] ASC),
    CONSTRAINT [FK_FS_SERVICE_HOST_ACTION_LOG_FS_SERVICE_HOST_ACTION_LOG] FOREIGN KEY ([LOG_SRNO]) REFERENCES [dbo].[FS_SERVICE_HOST_ACTION_LOG] ([LOG_SRNO])
);












GO





GO



GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'服務回應的時間，當 Direction = 2 或 3 時才有資料。', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_SERVICE_HOST_ACTION_LOG', @level2type = N'COLUMN', @level2name = N'RESPONSE_TIME';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'原始請求時間，當 Direction = 1或 4 時才有資料。', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_SERVICE_HOST_ACTION_LOG', @level2type = N'COLUMN', @level2name = N'REQUEST_TIME';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'請求代碼，若 Direction = 0，RequestId 可以允許為 DBNull 或空字串。', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_SERVICE_HOST_ACTION_LOG', @level2type = N'COLUMN', @level2name = N'REQUEST_ID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'服務回應的訊息', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_SERVICE_HOST_ACTION_LOG', @level2type = N'COLUMN', @level2name = N'MESSAGE';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'日誌時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_SERVICE_HOST_ACTION_LOG', @level2type = N'COLUMN', @level2name = N'LOG_TIME';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'日誌流水號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_SERVICE_HOST_ACTION_LOG', @level2type = N'COLUMN', @level2name = N'LOG_SRNO';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'服務執行動作是否成功', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_SERVICE_HOST_ACTION_LOG', @level2type = N'COLUMN', @level2name = N'IS_SUCCESS';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'動作方向，1: Request，2: Response，3: ReceiveMessage，4: PublishMessage。', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_SERVICE_HOST_ACTION_LOG', @level2type = N'COLUMN', @level2name = N'DIRECTION';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'資料內容 (JSON字串格式)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_SERVICE_HOST_ACTION_LOG', @level2type = N'COLUMN', @level2name = N'DATA_CONTENT';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'服務回應的代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_SERVICE_HOST_ACTION_LOG', @level2type = N'COLUMN', @level2name = N'CODE';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'動作方法名稱', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_SERVICE_HOST_ACTION_LOG', @level2type = N'COLUMN', @level2name = N'ACTION_NAME';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'服務設定名稱', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_SERVICE_HOST_ACTION_LOG', @level2type = N'COLUMN', @level2name = N'SERVICE_NAME';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'服務主機位址', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_SERVICE_HOST_ACTION_LOG', @level2type = N'COLUMN', @level2name = N'SERVICE_HOST';


GO
CREATE NONCLUSTERED INDEX [IX_FS_SERVICE_HOST_ACTION_LOG]
    ON [dbo].[FS_SERVICE_HOST_ACTION_LOG]([LOG_SRNO] ASC);

