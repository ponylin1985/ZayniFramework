﻿CREATE TABLE [dbo].[FS_SQL_LOG] (
    [LOG_SRNO]          INT            IDENTITY (1, 1) NOT NULL,
    [DAO_NAME]          NVARCHAR (100) NULL,
    [COMMAND_ID]        NVARCHAR (20)  NULL,
    [COMMAND_DIRECTION] INT            NULL,
    [COMMAND_TYPE]      INT            NULL,
    [COMMAND_TXT]       NVARCHAR (MAX) NULL,
    [COMMAND_RESULT]    NVARCHAR (MAX) NULL,
    [HOST_NAME]         NVARCHAR (100) NOT NULL,
    [LOG_TIME]          DATETIME       NOT NULL,
    CONSTRAINT [PK_FS_SQL_LOG] PRIMARY KEY CLUSTERED ([LOG_SRNO] ASC)
);








GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'紀錄流水號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_SQL_LOG', @level2type = N'COLUMN', @level2name = N'LOG_SRNO';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SQL指令的型態，1: CommandText，2: Stored Procedure', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_SQL_LOG', @level2type = N'COLUMN', @level2name = N'COMMAND_TYPE';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SQL指令的語法', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_SQL_LOG', @level2type = N'COLUMN', @level2name = N'COMMAND_TXT';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'電腦名稱或IP位址', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_SQL_LOG', @level2type = N'COLUMN', @level2name = N'HOST_NAME';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'紀錄時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_SQL_LOG', @level2type = N'COLUMN', @level2name = N'LOG_TIME';


GO
CREATE NONCLUSTERED INDEX [CommandIdIndex]
    ON [dbo].[FS_SQL_LOG]([COMMAND_ID] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'資料存取物件的類別名稱', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_SQL_LOG', @level2type = N'COLUMN', @level2name = N'DAO_NAME';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SQL指令的執行結果', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_SQL_LOG', @level2type = N'COLUMN', @level2name = N'COMMAND_RESULT';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SQL指令的識別代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_SQL_LOG', @level2type = N'COLUMN', @level2name = N'COMMAND_ID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'SQL指令的執行方向，1: 請求指令，2: 執行指令的結果。', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_SQL_LOG', @level2type = N'COLUMN', @level2name = N'COMMAND_DIRECTION';

