﻿CREATE TABLE [dbo].[FS_EVENT_LOG] (
    [LOG_SRNO]      BIGINT          IDENTITY (1, 1) NOT NULL,
    [TEXT_LOGGER]   NVARCHAR (100)  NOT NULL,
    [EVENT_LEVEL]   NVARCHAR (30)   NOT NULL,
    [EVENT_TITLE]   NVARCHAR (500)  NULL,
    [EVENT_MESSAGE] NVARCHAR (4000) NULL,
    [HOST]          NVARCHAR (25)   NULL,
    [LOG_TIME]      DATETIME        NOT NULL,
    CONSTRAINT [PK__FS_EVENT__3345296F1D1F76D0] PRIMARY KEY CLUSTERED ([LOG_SRNO] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = '', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_EVENT_LOG', @level2type = N'COLUMN', @level2name = N'HOST';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'文字日誌器設定名稱', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_EVENT_LOG', @level2type = N'COLUMN', @level2name = N'TEXT_LOGGER';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'日誌時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_EVENT_LOG', @level2type = N'COLUMN', @level2name = N'LOG_TIME';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'日誌紀錄流水號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_EVENT_LOG', @level2type = N'COLUMN', @level2name = N'LOG_SRNO';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'日誌事件標題', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_EVENT_LOG', @level2type = N'COLUMN', @level2name = N'EVENT_TITLE';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'日誌事件訊息', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_EVENT_LOG', @level2type = N'COLUMN', @level2name = N'EVENT_MESSAGE';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'日誌事件層級', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_EVENT_LOG', @level2type = N'COLUMN', @level2name = N'EVENT_LEVEL';

