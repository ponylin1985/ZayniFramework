USE [ZayniFramework]
GO
/****** Object:  Table [dbo].[FS_CACHING_REDIS_ACTION_LOG]    Script Date: 2019/3/29 下午 04:34:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FS_CACHING_REDIS_ACTION_LOG](
    [LOG_SRNO] [bigint] IDENTITY(1,1) NOT NULL,
    [REDIS_HOST] [nvarchar](100) NOT NULL,
    [REDIS_PORT] [int] NOT NULL,
    [REQUEST_ID] [nvarchar](36) NOT NULL,
    [DIRECTION] [int] NOT NULL,
    [ACTION] [nvarchar](100) NOT NULL,
    [DATA_CONTENT] [text] NULL,
    [MESSAGE] [nvarchar](500) NULL,
    [LOG_TIME] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
    [LOG_SRNO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FS_DB_MIGRATION]    Script Date: 2019/3/29 下午 04:34:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FS_DB_MIGRATION](
    [CURRENT_SCRIPT] [nvarchar](255) NOT NULL,
    [CURRENT_SCRIPT_VERSION] [nvarchar](50) NOT NULL,
    [CURRENT_SCRIPT_TIMESTAMP] [datetime] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FS_DB_MIGRATION_LOG]    Script Date: 2019/3/29 下午 04:34:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FS_DB_MIGRATION_LOG](
    [LOG_SRNO] [int] IDENTITY(1,1) NOT NULL,
    [SCRIPT] [nvarchar](255) NOT NULL,
    [SCRIPT_TYPE] [nvarchar](10) NOT NULL,
    [SCRIPT_VERSION] [nvarchar](50) NOT NULL,
    [SCRIPT_TIMESTAMP] [datetime] NOT NULL,
    [SCRIPT_CONTENT] [ntext] NULL,
    [IS_SUCCESS] [bit] NOT NULL,
    [MESSAGE] [nvarchar](1024) NULL,
    [LOG_TIME] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
    [LOG_SRNO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FS_EVENT_LOG]    Script Date: 2019/3/29 下午 04:34:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FS_EVENT_LOG](
    [LOG_SRNO] [bigint] IDENTITY(1,1) NOT NULL,
    [TEXT_LOGGER] [nvarchar](100) NOT NULL,
    [EVENT_LEVEL] [nvarchar](30) NOT NULL,
    [EVENT_TITLE] [nvarchar](500) NULL,
    [EVENT_MESSAGE] [nvarchar](4000) NULL,
    [HOST] [nvarchar](25) NULL,
    [LOG_TIME] [datetime] NOT NULL,
 CONSTRAINT [PK__FS_EVENT__3345296F1D1F76D0] PRIMARY KEY CLUSTERED 
(
    [LOG_SRNO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FS_SERVICE_CLIENT_ACTION_LOG]    Script Date: 2019/3/29 下午 04:34:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FS_SERVICE_CLIENT_ACTION_LOG](
    [LOG_SRNO] [bigint] IDENTITY(1,1) NOT NULL,
    [REQUEST_ID] [nvarchar](15) NULL,
    [CLIENT_NAME] [nvarchar](255) NULL,
    [CLIENT_HOST] [nvarchar](20) NULL,
    [REMOTE_NAME] [nvarchar](255) NULL,
    [REMOTE_HOST] [nvarchar](1024) NULL,
    [DIRECTION] [tinyint] NOT NULL,
    [ACTION_NAME] [nvarchar](100) NOT NULL,
    [DATA_CONTENT] [ntext] NULL,
    [REQUEST_TIME] [datetime] NULL,
    [RESPONSE_TIME] [datetime] NULL,
    [IS_SUCCESS] [bit] NULL,
    [CODE] [nvarchar](100) NULL,
    [MESSAGE] [nvarchar](500) NULL,
    [LOG_TIME] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
    [LOG_SRNO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FS_SERVICE_HOST_ACTION_LOG]    Script Date: 2019/3/29 下午 04:34:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FS_SERVICE_HOST_ACTION_LOG](
    [LOG_SRNO] [bigint] IDENTITY(1,1) NOT NULL,
    [REQUEST_ID] [nchar](15) NULL,
    [SERVICE_NAME] [nvarchar](100) NULL,
    [SERVICE_HOST] [nvarchar](20) NULL,
    [DIRECTION] [tinyint] NOT NULL,
    [ACTION_NAME] [nvarchar](100) NOT NULL,
    [DATA_CONTENT] [ntext] NULL,
    [REQUEST_TIME] [datetime] NULL,
    [RESPONSE_TIME] [datetime] NULL,
    [IS_SUCCESS] [bit] NULL,
    [CODE] [nvarchar](100) NULL,
    [MESSAGE] [nvarchar](500) NULL,
    [LOG_TIME] [datetime] NOT NULL,
 CONSTRAINT [PK__FS_SERVI__3345296F9E1870E8] PRIMARY KEY CLUSTERED 
(
    [LOG_SRNO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FS_SQL_LOG]    Script Date: 2019/3/29 下午 04:34:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FS_SQL_LOG](
    [LOG_SRNO] [int] IDENTITY(1,1) NOT NULL,
    [DAO_NAME] [nvarchar](100) NULL,
    [COMMAND_ID] [nvarchar](20) NULL,
    [COMMAND_DIRECTION] [int] NULL,
    [COMMAND_TYPE] [int] NULL,
    [COMMAND_TXT] [nvarchar](max) NULL,
    [COMMAND_RESULT] [nvarchar](max) NULL,
    [HOST_NAME] [nvarchar](100) NOT NULL,
    [LOG_TIME] [datetime] NOT NULL,
 CONSTRAINT [PK_FS_SQL_LOG] PRIMARY KEY CLUSTERED 
(
    [LOG_SRNO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FS_UNIT_TEST_USER]    Script Date: 2019/3/29 下午 04:34:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FS_UNIT_TEST_USER](
    [ACCOUNT_ID] [nvarchar](50) NOT NULL,
    [NAME] [nvarchar](30) NOT NULL,
    [AGE] [int] NULL,
    [SEX] [int] NULL,
    [BIRTHDAY] [datetime] NULL,
    [IS_VIP] [bit] NULL,
    [IS_GOOD] [bit] NULL,
    [DATA_FLAG] [timestamp] NOT NULL,
PRIMARY KEY CLUSTERED 
(
    [ACCOUNT_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FS_UNIT_TEST_USER_PHONE_DETAIL]    Script Date: 2019/3/29 下午 04:34:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FS_UNIT_TEST_USER_PHONE_DETAIL](
    [ACCOUNT_ID] [nvarchar](50) NOT NULL,
    [PHONE_TYPE] [nvarchar](50) NOT NULL,
    [PHONE_NUMBER] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
    [ACCOUNT_ID] ASC,
    [PHONE_TYPE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FS_UNIT_TEST_USER2]    Script Date: 2019/3/29 下午 04:34:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FS_UNIT_TEST_USER2](
    [ACCOUNT_ID] [nvarchar](50) NOT NULL,
    [USER_ID] [nvarchar](50) NOT NULL,
    [USER_NAME] [nvarchar](50) NOT NULL,
    [USER_PASSWORD] [nvarchar](50) NULL,
    [BIRTHDAY] [datetime] NULL,
    [SEX] [int] NOT NULL,
    [MONEY] [decimal](18, 0) NULL,
    [DESCRIPTION] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
    [ACCOUNT_ID] ASC,
    [USER_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FS_CACHING_REDIS_ACTION_LOG] ADD  DEFAULT ((0)) FOR [REQUEST_ID]
GO
ALTER TABLE [dbo].[FS_UNIT_TEST_USER] ADD  DEFAULT ((0)) FOR [IS_VIP]
GO
ALTER TABLE [dbo].[FS_UNIT_TEST_USER] ADD  DEFAULT ((0)) FOR [IS_GOOD]
GO
ALTER TABLE [dbo].[FS_SERVICE_HOST_ACTION_LOG]  WITH CHECK ADD  CONSTRAINT [FK_FS_SERVICE_HOST_ACTION_LOG_FS_SERVICE_HOST_ACTION_LOG] FOREIGN KEY([LOG_SRNO])
REFERENCES [dbo].[FS_SERVICE_HOST_ACTION_LOG] ([LOG_SRNO])
GO
ALTER TABLE [dbo].[FS_SERVICE_HOST_ACTION_LOG] CHECK CONSTRAINT [FK_FS_SERVICE_HOST_ACTION_LOG_FS_SERVICE_HOST_ACTION_LOG]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'日誌流水號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_CACHING_REDIS_ACTION_LOG', @level2type=N'COLUMN',@level2name=N'LOG_SRNO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Redis Server 服務的位址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_CACHING_REDIS_ACTION_LOG', @level2type=N'COLUMN',@level2name=N'REDIS_HOST'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Redis Server 的連接阜號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_CACHING_REDIS_ACTION_LOG', @level2type=N'COLUMN',@level2name=N'REDIS_PORT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'原始請求識別代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_CACHING_REDIS_ACTION_LOG', @level2type=N'COLUMN',@level2name=N'REQUEST_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'動作方向，1: 對 Redis 發送指令請求，2: Redis 執行指令的回應。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_CACHING_REDIS_ACTION_LOG', @level2type=N'COLUMN',@level2name=N'DIRECTION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'動作名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_CACHING_REDIS_ACTION_LOG', @level2type=N'COLUMN',@level2name=N'ACTION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'資料內容' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_CACHING_REDIS_ACTION_LOG', @level2type=N'COLUMN',@level2name=N'DATA_CONTENT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'框架內部訊息' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_CACHING_REDIS_ACTION_LOG', @level2type=N'COLUMN',@level2name=N'MESSAGE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'日誌時間' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_CACHING_REDIS_ACTION_LOG', @level2type=N'COLUMN',@level2name=N'LOG_TIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'目前資料庫版本的腳本' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_DB_MIGRATION', @level2type=N'COLUMN',@level2name=N'CURRENT_SCRIPT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'目前資料庫 migration 腳本的版本號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_DB_MIGRATION', @level2type=N'COLUMN',@level2name=N'CURRENT_SCRIPT_VERSION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'目前資料庫版本的時間戳記' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_DB_MIGRATION', @level2type=N'COLUMN',@level2name=N'CURRENT_SCRIPT_TIMESTAMP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'日誌記錄流水號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_DB_MIGRATION_LOG', @level2type=N'COLUMN',@level2name=N'LOG_SRNO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'資料庫 migration 腳本檔案名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_DB_MIGRATION_LOG', @level2type=N'COLUMN',@level2name=N'SCRIPT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'資料庫 migration 腳本類型: upgrade、downgrade' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_DB_MIGRATION_LOG', @level2type=N'COLUMN',@level2name=N'SCRIPT_TYPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'資料庫 migration 腳本的版本號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_DB_MIGRATION_LOG', @level2type=N'COLUMN',@level2name=N'SCRIPT_VERSION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'資料庫 migration 腳本的時間戳記' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_DB_MIGRATION_LOG', @level2type=N'COLUMN',@level2name=N'SCRIPT_TIMESTAMP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'資料庫 migration 腳本內容' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_DB_MIGRATION_LOG', @level2type=N'COLUMN',@level2name=N'SCRIPT_CONTENT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'執行 migration 腳本結果是否成功' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_DB_MIGRATION_LOG', @level2type=N'COLUMN',@level2name=N'IS_SUCCESS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'執行 migration 腳本的結果訊息' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_DB_MIGRATION_LOG', @level2type=N'COLUMN',@level2name=N'MESSAGE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'執行日誌時間' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_DB_MIGRATION_LOG', @level2type=N'COLUMN',@level2name=N'LOG_TIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'日誌紀錄流水號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_EVENT_LOG', @level2type=N'COLUMN',@level2name=N'LOG_SRNO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'文字日誌器設定名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_EVENT_LOG', @level2type=N'COLUMN',@level2name=N'TEXT_LOGGER'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'日誌事件層級' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_EVENT_LOG', @level2type=N'COLUMN',@level2name=N'EVENT_LEVEL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'日誌事件標題' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_EVENT_LOG', @level2type=N'COLUMN',@level2name=N'EVENT_TITLE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'日誌事件訊息' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_EVENT_LOG', @level2type=N'COLUMN',@level2name=N'EVENT_MESSAGE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_EVENT_LOG', @level2type=N'COLUMN',@level2name=N'HOST'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'日誌時間' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_EVENT_LOG', @level2type=N'COLUMN',@level2name=N'LOG_TIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'日誌流水號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_SERVICE_CLIENT_ACTION_LOG', @level2type=N'COLUMN',@level2name=N'LOG_SRNO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'請求代碼，若 Direction = 0，RequestId 可以允許為 DBNull 或空字串。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_SERVICE_CLIENT_ACTION_LOG', @level2type=N'COLUMN',@level2name=N'REQUEST_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'請求客戶端的設定名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_SERVICE_CLIENT_ACTION_LOG', @level2type=N'COLUMN',@level2name=N'CLIENT_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'請求客戶端的主機位址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_SERVICE_CLIENT_ACTION_LOG', @level2type=N'COLUMN',@level2name=N'CLIENT_HOST'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'遠端服務的設定名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_SERVICE_CLIENT_ACTION_LOG', @level2type=N'COLUMN',@level2name=N'REMOTE_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'遠端服務的主機位址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_SERVICE_CLIENT_ACTION_LOG', @level2type=N'COLUMN',@level2name=N'REMOTE_HOST'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'動作方向，1: Request，2: Response，3: PublishMessage，4: ReceiveMessage。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_SERVICE_CLIENT_ACTION_LOG', @level2type=N'COLUMN',@level2name=N'DIRECTION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'動作方法名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_SERVICE_CLIENT_ACTION_LOG', @level2type=N'COLUMN',@level2name=N'ACTION_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'資料內容 (JSON字串格式)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_SERVICE_CLIENT_ACTION_LOG', @level2type=N'COLUMN',@level2name=N'DATA_CONTENT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'原始請求時間，當 Direction = 1 或 3 時才有資料。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_SERVICE_CLIENT_ACTION_LOG', @level2type=N'COLUMN',@level2name=N'REQUEST_TIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'服務回應的時間，當 Direction = 2 或 4 時才有資料。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_SERVICE_CLIENT_ACTION_LOG', @level2type=N'COLUMN',@level2name=N'RESPONSE_TIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'服務執行動作是否成功' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_SERVICE_CLIENT_ACTION_LOG', @level2type=N'COLUMN',@level2name=N'IS_SUCCESS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'服務回應的代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_SERVICE_CLIENT_ACTION_LOG', @level2type=N'COLUMN',@level2name=N'CODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'動作的訊息' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_SERVICE_CLIENT_ACTION_LOG', @level2type=N'COLUMN',@level2name=N'MESSAGE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'日誌時間' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_SERVICE_CLIENT_ACTION_LOG', @level2type=N'COLUMN',@level2name=N'LOG_TIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'日誌流水號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_SERVICE_HOST_ACTION_LOG', @level2type=N'COLUMN',@level2name=N'LOG_SRNO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'請求代碼，若 Direction = 0，RequestId 可以允許為 DBNull 或空字串。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_SERVICE_HOST_ACTION_LOG', @level2type=N'COLUMN',@level2name=N'REQUEST_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'服務設定名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_SERVICE_HOST_ACTION_LOG', @level2type=N'COLUMN',@level2name=N'SERVICE_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'服務主機位址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_SERVICE_HOST_ACTION_LOG', @level2type=N'COLUMN',@level2name=N'SERVICE_HOST'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'動作方向，1: Request，2: Response，3: ReceiveMessage，4: PublishMessage。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_SERVICE_HOST_ACTION_LOG', @level2type=N'COLUMN',@level2name=N'DIRECTION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'動作方法名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_SERVICE_HOST_ACTION_LOG', @level2type=N'COLUMN',@level2name=N'ACTION_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'資料內容 (JSON字串格式)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_SERVICE_HOST_ACTION_LOG', @level2type=N'COLUMN',@level2name=N'DATA_CONTENT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'原始請求時間，當 Direction = 1或 4 時才有資料。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_SERVICE_HOST_ACTION_LOG', @level2type=N'COLUMN',@level2name=N'REQUEST_TIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'服務回應的時間，當 Direction = 2 或 3 時才有資料。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_SERVICE_HOST_ACTION_LOG', @level2type=N'COLUMN',@level2name=N'RESPONSE_TIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'服務執行動作是否成功' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_SERVICE_HOST_ACTION_LOG', @level2type=N'COLUMN',@level2name=N'IS_SUCCESS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'服務回應的代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_SERVICE_HOST_ACTION_LOG', @level2type=N'COLUMN',@level2name=N'CODE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'服務回應的訊息' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_SERVICE_HOST_ACTION_LOG', @level2type=N'COLUMN',@level2name=N'MESSAGE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'日誌時間' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_SERVICE_HOST_ACTION_LOG', @level2type=N'COLUMN',@level2name=N'LOG_TIME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'紀錄流水號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_SQL_LOG', @level2type=N'COLUMN',@level2name=N'LOG_SRNO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'資料存取物件的類別名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_SQL_LOG', @level2type=N'COLUMN',@level2name=N'DAO_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SQL指令的識別代碼' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_SQL_LOG', @level2type=N'COLUMN',@level2name=N'COMMAND_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SQL指令的執行方向，1: 請求指令，2: 執行指令的結果。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_SQL_LOG', @level2type=N'COLUMN',@level2name=N'COMMAND_DIRECTION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SQL指令的型態，1: CommandText，2: Stored Procedure' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_SQL_LOG', @level2type=N'COLUMN',@level2name=N'COMMAND_TYPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SQL指令的語法' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_SQL_LOG', @level2type=N'COLUMN',@level2name=N'COMMAND_TXT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SQL指令的執行結果' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_SQL_LOG', @level2type=N'COLUMN',@level2name=N'COMMAND_RESULT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'電腦名稱或IP位址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_SQL_LOG', @level2type=N'COLUMN',@level2name=N'HOST_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'紀錄時間' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FS_SQL_LOG', @level2type=N'COLUMN',@level2name=N'LOG_TIME'
GO

----------------------

INSERT INTO FS_UNIT_TEST_USER (ACCOUNT_ID, NAME, AGE, SEX, BIRTHDAY, IS_VIP, IS_GOOD)
VALUES ('Dexter Gordon', 'Dexter Gordon', 69, 1, '1942-07-30 00:48:26.000', 1, 0);

INSERT INTO FS_UNIT_TEST_USER (ACCOUNT_ID, NAME, AGE, SEX, BIRTHDAY, IS_VIP, IS_GOOD)
VALUES ('Jaco Pastorius', 'Jaco Pastorius', 36, 1, '1970-11-23 00:47:18.000', 0, 0);

INSERT INTO FS_UNIT_TEST_USER (ACCOUNT_ID, NAME, AGE, SEX, BIRTHDAY, IS_VIP, IS_GOOD)
VALUES ('Judy Bitch', 'Judy Bitch', 19, 0, '2000-05-24 00:45:21.000', 1, 0);

INSERT INTO FS_UNIT_TEST_USER (ACCOUNT_ID, NAME, AGE, SEX, BIRTHDAY, IS_VIP, IS_GOOD)
VALUES ('Nancy Gili', 'Nancy Gili', 24, 0, '1998-08-06 00:46:22.000', 1, 1);

INSERT INTO FS_UNIT_TEST_USER (ACCOUNT_ID, NAME, AGE, SEX, BIRTHDAY, IS_VIP, IS_GOOD)
VALUES ('WebAPI.Test.CacheManagerTester', 'WebAPI.Test.CacheManagerTester', 40, 1, '1980-12-12 00:14:14.000', 1, 1);

----------------------

SELECT TOP 100 * FROM FS_CACHING_REDIS_ACTION_LOG
ORDER BY LOG_TIME DESC;

SELECT TOP 100 * FROM FS_EVENT_LOG
ORDER BY LOG_TIME DESC;

SELECT TOP 100 * FROM FS_SQL_LOG
ORDER BY LOG_TIME DESC;

SELECT TOP 100 * FROM FS_SERVICE_CLIENT_ACTION_LOG
ORDER BY LOG_TIME DESC;

SELECT TOP 100 * FROM FS_SERVICE_HOST_ACTION_LOG
ORDER BY LOG_TIME DESC;

SELECT TOP 100 * FROM FS_DB_MIGRATION;

SELECT TOP 100 * FROM FS_DB_MIGRATION_LOG
ORDER BY LOG_TIME DESC;

SELECT TOP 100 * FROM FS_UNIT_TEST_USER
ORDER BY ACCOUNT_ID;

SELECT TOP 100 * FROM FS_UNIT_TEST_USER_PHONE_DETAIL
ORDER BY ACCOUNT_ID;
