-- -------------------------------------------------------------
-- TablePlus 3.1.0(290)
--
-- https://tableplus.com/
--
-- Database: zayni
-- Generation Time: 2020-01-29 00:58:26.5340
-- -------------------------------------------------------------


DROP TABLE IF EXISTS "public"."FS_UNIT_TEST_USER";
-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Table Definition
CREATE TABLE "public"."FS_UNIT_TEST_USER" (
    "ACCOUNT_ID" varchar(50) NOT NULL,
    "NAME" varchar(50) NOT NULL,
    "AGE" int4 NOT NULL,
    "SEX" int4,
    "BIRTHDAY" timestamptz,
    "IS_VIP" bool,
    "IS_GOOD" bool,
    "DATA_FLAG" timestamptz NOT NULL DEFAULT now(),
    PRIMARY KEY ("ACCOUNT_ID")
);

CREATE TRIGGER "TRG_USER_SET_DATAFALG"
BEFORE INSERT OR UPDATE ON "FS_UNIT_TEST_USER"
FOR EACH ROW
EXECUTE PROCEDURE "REFRESH_DATAFLAG"();