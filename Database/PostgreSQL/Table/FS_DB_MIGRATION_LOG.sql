-- -------------------------------------------------------------
-- TablePlus 3.1.0(290)
--
-- https://tableplus.com/
--
-- Database: zayni
-- Generation Time: 2020-01-29 00:56:43.2630
-- -------------------------------------------------------------


DROP TABLE IF EXISTS "public"."FS_DB_MIGRATION_LOG";
-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "FS_DB_MIGRATION_LOG_LOG_SRNO_seq";

-- Table Definition
CREATE TABLE "public"."FS_DB_MIGRATION_LOG" (
    "LOG_SRNO" int8 NOT NULL DEFAULT nextval('"FS_DB_MIGRATION_LOG_LOG_SRNO_seq"'::regclass),
    "SCRIPT" varchar(255) NOT NULL,
    "SCRIPT_TYPE" varchar(10),
    "SCRIPT_VERSION" varchar(50),
    "SCRIPT_CONTENT" text,
    "IS_SUCCESS" bool NOT NULL,
    "MESSAGE" varchar(1024),
    "LOG_TIME" timestamptz NOT NULL DEFAULT now()
);

