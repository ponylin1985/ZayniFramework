-- -------------------------------------------------------------
-- TablePlus 3.1.0(290)
--
-- https://tableplus.com/
--
-- Database: zayni
-- Generation Time: 2020-01-29 00:57:07.6530
-- -------------------------------------------------------------


DROP TABLE IF EXISTS "public"."FS_SERVICE_CLIENT_ACTION_LOG";
-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "FS_SERVICE_CLIENT_ACTION_LOG_LOG_SRNO_seq";

-- Table Definition
CREATE TABLE "public"."FS_SERVICE_CLIENT_ACTION_LOG" (
    "LOG_SRNO" int8 NOT NULL DEFAULT nextval('"FS_SERVICE_CLIENT_ACTION_LOG_LOG_SRNO_seq"'::regclass),
    "REQUEST_ID" bpchar(15) DEFAULT NULL::bpchar,
    "CLIENT_NAME" varchar(255) DEFAULT NULL::character varying,
    "CLIENT_HOST" varchar(20) DEFAULT NULL::character varying,
    "REMOTE_NAME" varchar(255) DEFAULT 'NULL::character varying'::character varying,
    "REMOTE_HOST" varchar(1024) DEFAULT 'NULL::character varying'::character varying,
    "DIRECTION" int2 NOT NULL,
    "ACTION_NAME" varchar(100) NOT NULL,
    "DATA_CONTENT" text,
    "REQUEST_TIME" timestamptz,
    "RESPONSE_TIME" timestamptz,
    "IS_SUCCESS" bool,
    "CODE" varchar(100),
    "MESSAGE" varchar(500),
    "LOG_TIME" timestamptz NOT NULL DEFAULT now(),
    PRIMARY KEY ("LOG_SRNO")
);

-- Column Comment
COMMENT ON COLUMN "public"."FS_SERVICE_CLIENT_ACTION_LOG"."LOG_SRNO" IS '日誌流水號';
COMMENT ON COLUMN "public"."FS_SERVICE_CLIENT_ACTION_LOG"."REQUEST_ID" IS '請求代碼，若 Direction = 0，RequestId 可以允許為 DBNull 或空字串。';
COMMENT ON COLUMN "public"."FS_SERVICE_CLIENT_ACTION_LOG"."CLIENT_NAME" IS '請求客戶端的設定名稱';
COMMENT ON COLUMN "public"."FS_SERVICE_CLIENT_ACTION_LOG"."CLIENT_HOST" IS '請求客戶端的主機位址';
COMMENT ON COLUMN "public"."FS_SERVICE_CLIENT_ACTION_LOG"."REMOTE_NAME" IS '遠端服務的設定名稱';
COMMENT ON COLUMN "public"."FS_SERVICE_CLIENT_ACTION_LOG"."REMOTE_HOST" IS '遠端服務的主機位址';
COMMENT ON COLUMN "public"."FS_SERVICE_CLIENT_ACTION_LOG"."DIRECTION" IS '動作方向，1: Request，2: Response，3: PublishMessage，4: ReceiveMessage。';
COMMENT ON COLUMN "public"."FS_SERVICE_CLIENT_ACTION_LOG"."ACTION_NAME" IS '動作方法名稱';
COMMENT ON COLUMN "public"."FS_SERVICE_CLIENT_ACTION_LOG"."DATA_CONTENT" IS '資料內容 (JSON字串格式)';
COMMENT ON COLUMN "public"."FS_SERVICE_CLIENT_ACTION_LOG"."REQUEST_TIME" IS '原始請求時間，當 Direction = 1 或 3 時才有資料。';
COMMENT ON COLUMN "public"."FS_SERVICE_CLIENT_ACTION_LOG"."RESPONSE_TIME" IS '服務回應的時間，當 Direction = 2 或 4 時才有資料。';
COMMENT ON COLUMN "public"."FS_SERVICE_CLIENT_ACTION_LOG"."IS_SUCCESS" IS '服務執行動作是否成功';
COMMENT ON COLUMN "public"."FS_SERVICE_CLIENT_ACTION_LOG"."CODE" IS '服務回應的代碼';
COMMENT ON COLUMN "public"."FS_SERVICE_CLIENT_ACTION_LOG"."MESSAGE" IS '動作的訊息';
COMMENT ON COLUMN "public"."FS_SERVICE_CLIENT_ACTION_LOG"."LOG_TIME" IS '日誌時間';

