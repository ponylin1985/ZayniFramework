-- -------------------------------------------------------------
-- TablePlus 3.1.0(290)
--
-- https://tableplus.com/
--
-- Database: zayni
-- Generation Time: 2020-01-29 00:57:53.0820
-- -------------------------------------------------------------


CREATE OR REPLACE FUNCTION public."REFRESH_DATAFLAG"()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
BEGIN
  NEW."DATA_FLAG" = NOW();
  RETURN NEW;
END;
$function$;

