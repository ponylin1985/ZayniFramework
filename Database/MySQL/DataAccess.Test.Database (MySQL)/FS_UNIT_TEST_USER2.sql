CREATE TABLE `FS_UNIT_TEST_USER2` (
	`ACCOUNT_ID` VARCHAR(50) NOT NULL,
	`USER_ID` VARCHAR(50) NOT NULL,
	`USER_NAME` VARCHAR(50) NOT NULL,
	`USER_PASSWORD` VARCHAR(50) NULL DEFAULT NULL,
	`BIRTHDAY` DATETIME NULL DEFAULT NULL,
	`SEX` INT(11) NOT NULL,
	`MONEY` DOUBLE NULL DEFAULT NULL,
	`DESCRIPTION` VARCHAR(50) NULL DEFAULT NULL,
	PRIMARY KEY (`ACCOUNT_ID`, `USER_ID`)
)
COMMENT='ZayniFramework 框架 DataAccess unit test 測試用資料表'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
