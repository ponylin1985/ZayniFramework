-- --------------------------------------------------------
-- 主機:                           127.0.0.1
-- 伺服器版本:                        8.0.13 - MySQL Community Server - GPL
-- 伺服器操作系統:                      Linux
-- HeidiSQL 版本:                  10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- 傾印 zayni 的資料庫結構
CREATE DATABASE IF NOT EXISTS `zayni` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `zayni`;

-- 傾印  表格 zayni.FS_CACHING_REDIS_ACTION_LOG 結構
CREATE TABLE IF NOT EXISTS `FS_CACHING_REDIS_ACTION_LOG` (
  `LOG_SRNO` bigint(30) unsigned NOT NULL AUTO_INCREMENT COMMENT '日誌流水號',
  `REDIS_HOST` varchar(100) NOT NULL COMMENT 'Redis Server 服務的位址',
  `REDIS_PORT` int(11) NOT NULL COMMENT 'Redis Server 的連接阜號',
  `REQUEST_ID` varchar(36) NOT NULL DEFAULT '0' COMMENT '原始請求識別代碼',
  `DIRECTION` int(11) NOT NULL COMMENT '動作方向，1: 對 Redis 發送指令請求，2: Redis 執行指令的回應。',
  `ACTION` varchar(100) NOT NULL COMMENT '動作名稱',
  `DATA_CONTENT` mediumtext COMMENT '資料內容',
  `MESSAGE` varchar(500) DEFAULT NULL COMMENT '框架內部訊息',
  `LOG_TIME` datetime(3) NOT NULL,
  PRIMARY KEY (`LOG_SRNO`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Redis 服務的動作日誌紀錄';

-- 取消選取資料匯出。
-- 傾印  表格 zayni.FS_CACHING_STORAGE 結構
CREATE TABLE IF NOT EXISTS `FS_CACHING_STORAGE` (
  `SRNO` bigint(30) unsigned NOT NULL AUTO_INCREMENT COMMENT '資料快取流水號',
  `DATA_TYPE` varchar(15) NOT NULL DEFAULT 'CacheProperty' COMMENT '資料結構類型: CacheProperty 或 HashProperty',
  `CACHE_ID` varchar(255) NOT NULL COMMENT '資料快取 Key 值',
  `CACHE_VALUE` varchar(20000) DEFAULT NULL COMMENT '資料快取內容 (字串最大長度為20000)',
  `DATA_FLAG` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`SRNO`) USING HASH,
  UNIQUE KEY `CacheIdIndex` (`CACHE_ID`) USING HASH
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='MySQL 記憶體快取儲存體';

-- 取消選取資料匯出。
-- 傾印  表格 zayni.FS_CACHING_STORAGE_HASH 結構
CREATE TABLE IF NOT EXISTS `FS_CACHING_STORAGE_HASH` (
  `SRNO` bigint(30) unsigned NOT NULL AUTO_INCREMENT COMMENT '資料快取流水號',
  `CACHE_ID` varchar(255) NOT NULL COMMENT '資料快取 Key 值',
  `SUB_KEY` varchar(255) NOT NULL COMMENT '資料快取 Subkey 值',
  `CACHE_VALUE` varchar(20000) DEFAULT NULL COMMENT '資料快取內容 (字串最大長度為20000)',
  `DATA_FLAG` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`SRNO`) USING HASH,
  UNIQUE KEY `CacheKey` (`CACHE_ID`,`SUB_KEY`) USING HASH
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COMMENT='MySQL 記憶體快取儲存體 (HashProperty 資料結構)';

-- 取消選取資料匯出。
-- 傾印  表格 zayni.FS_DB_MIGRATION 結構
CREATE TABLE IF NOT EXISTS `FS_DB_MIGRATION` (
  `CURRENT_SCRIPT` varchar(255) NOT NULL COMMENT '目前資料庫版本的腳本',
  `CURRENT_SCRIPT_VERSION` varchar(50) NOT NULL COMMENT '目前資料庫 migration 腳本的版本號',
  `CURRENT_SCRIPT_TIMESTAMP` datetime NOT NULL COMMENT '目前資料庫版本的時間戳記'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='資料庫目前 migration 版本設定';

-- 取消選取資料匯出。
-- 傾印  表格 zayni.FS_DB_MIGRATION_LOG 結構
CREATE TABLE IF NOT EXISTS `FS_DB_MIGRATION_LOG` (
  `LOG_SRNO` bigint(30) unsigned NOT NULL AUTO_INCREMENT COMMENT '日誌記錄流水號',
  `SCRIPT` varchar(255) NOT NULL DEFAULT '' COMMENT '資料庫 migration 腳本檔案名稱',
  `SCRIPT_TYPE` varchar(10) NOT NULL DEFAULT '' COMMENT '資料庫 migration 腳本類型: upgrade、downgrade',
  `SCRIPT_VERSION` varchar(50) NOT NULL DEFAULT '' COMMENT '資料庫 migration 腳本的版本號',
  `SCRIPT_TIMESTAMP` datetime NOT NULL COMMENT '資料庫 migration 腳本的時間戳記',
  `SCRIPT_CONTENT` mediumtext COMMENT '資料庫 migration 腳本內容',
  `IS_SUCCESS` bit(1) NOT NULL COMMENT '執行 migration 腳本結果是否成功',
  `MESSAGE` varchar(1024) DEFAULT NULL COMMENT '執行 migration 腳本的結果訊息',
  `LOG_TIME` datetime(3) NOT NULL COMMENT '執行日誌時間',
  PRIMARY KEY (`LOG_SRNO`),
  KEY `LogTimeIndex` (`LOG_TIME`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='資料庫 migration 執行日誌記錄檔';

-- 取消選取資料匯出。
-- 傾印  表格 zayni.FS_EVENT_LOG 結構
CREATE TABLE IF NOT EXISTS `FS_EVENT_LOG` (
  `LOG_SRNO` bigint(30) unsigned NOT NULL AUTO_INCREMENT COMMENT '日誌紀錄流水號',
  `TEXT_LOGGER` varchar(100) NOT NULL COMMENT '文字日誌器設定名稱',
  `EVENT_LEVEL` varchar(30) NOT NULL COMMENT '日誌事件層級',
  `EVENT_TITLE` varchar(500) DEFAULT NULL COMMENT '日誌事件標題',
  `EVENT_MESSAGE` varchar(5120) DEFAULT NULL COMMENT '日誌事件訊息',
  `HOST` varchar(25) DEFAULT NULL COMMENT '寫入日誌的主機位址',
  `LOG_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '日誌時間',
  PRIMARY KEY (`LOG_SRNO`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='日誌訊息紀錄檔';

-- 取消選取資料匯出。
-- 傾印  表格 zayni.FS_SERVICE_CLIENT_ACTION_LOG 結構
CREATE TABLE IF NOT EXISTS `FS_SERVICE_CLIENT_ACTION_LOG` (
  `LOG_SRNO` bigint(30) unsigned NOT NULL AUTO_INCREMENT COMMENT '日誌流水號',
  `REQUEST_ID` char(15) DEFAULT NULL COMMENT '請求代碼，若 Direction = 0，RequestId 可以允許為 DBNull 或空字串。',
  `CLIENT_NAME` varchar(255) DEFAULT NULL COMMENT '請求客戶端的設定名稱',
  `CLIENT_HOST` varchar(20) DEFAULT NULL COMMENT '請求客戶端的主機位址',
  `REMOTE_NAME` varchar(255) DEFAULT NULL COMMENT '遠端服務的設定名稱',
  `REMOTE_HOST` varchar(1024) DEFAULT NULL COMMENT '遠端服務的主機位址',
  `DIRECTION` tinyint(4) NOT NULL COMMENT '動作方向，1: Request，2: Response，3: PublishMessage，4: ReceiveMessage。',
  `ACTION_NAME` varchar(100) NOT NULL COMMENT '動作方法名稱',
  `DATA_CONTENT` longtext COMMENT '資料內容 (JSON字串格式)',
  `REQUEST_TIME` datetime(3) DEFAULT NULL COMMENT '原始請求時間，當 Direction = 1 或 3 時才有資料。',
  `RESPONSE_TIME` datetime(3) DEFAULT NULL COMMENT '服務回應的時間，當 Direction = 2 或 4 時才有資料。',
  `IS_SUCCESS` bit(1) DEFAULT NULL COMMENT '服務執行動作是否成功',
  `CODE` varchar(100) DEFAULT NULL COMMENT '服務回應的代碼',
  `MESSAGE` varchar(500) DEFAULT NULL COMMENT '動作的訊息',
  `LOG_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '日誌時間',
  PRIMARY KEY (`LOG_SRNO`),
  KEY `RequestIdIndex` (`REQUEST_ID`,`LOG_TIME`),
  KEY `ActionIndex` (`ACTION_NAME`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='客戶端動作請求日誌紀錄';

-- 取消選取資料匯出。
-- 傾印  表格 zayni.FS_SERVICE_HOST_ACTION_LOG 結構
CREATE TABLE IF NOT EXISTS `FS_SERVICE_HOST_ACTION_LOG` (
  `LOG_SRNO` bigint(30) unsigned NOT NULL AUTO_INCREMENT COMMENT '日誌流水號',
  `REQUEST_ID` char(15) DEFAULT NULL COMMENT '請求代碼，若 Direction = 0，RequestId 可以允許為 DBNull 或空字串。',
  `SERVICE_NAME` varchar(100) DEFAULT NULL COMMENT '服務設定名稱',
  `SERVICE_HOST` varchar(20) DEFAULT NULL COMMENT '服務主機位址',
  `DIRECTION` tinyint(4) NOT NULL COMMENT '動作方向，1: Request，2: Response，3: ReceiveMessage，4: PublishMessage。',
  `ACTION_NAME` varchar(65) NOT NULL COMMENT '動作方法名稱',
  `DATA_CONTENT` longtext COMMENT '資料內容 (JSON字串格式)',
  `REQUEST_TIME` datetime(3) DEFAULT NULL COMMENT '原始請求時間，當 Direction = 1或 4 時才有資料。',
  `RESPONSE_TIME` datetime(3) DEFAULT NULL COMMENT '服務回應的時間，當 Direction = 2 或 3 時才有資料。',
  `IS_SUCCESS` bit(1) DEFAULT NULL COMMENT '服務執行動作是否成功',
  `CODE` varchar(100) DEFAULT NULL COMMENT '服務回應的代碼',
  `MESSAGE` varchar(500) DEFAULT NULL COMMENT '動作的訊息',
  `LOG_TIME` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '日誌時間',
  PRIMARY KEY (`LOG_SRNO`),
  KEY `RequestIdIndex` (`REQUEST_ID`,`LOG_TIME`),
  KEY `ActionIndex` (`ACTION_NAME`),
  KEY `ServiceInfoIndex` (`SERVICE_NAME`,`SERVICE_HOST`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='服務動作日誌記錄';

-- 取消選取資料匯出。
-- 傾印  表格 zayni.FS_SQL_LOG 結構
CREATE TABLE IF NOT EXISTS `FS_SQL_LOG` (
  `LOG_SRNO` bigint(30) unsigned NOT NULL AUTO_INCREMENT COMMENT 'SQL日誌紀錄流水號',
  `DAO_NAME` varchar(100) DEFAULT NULL COMMENT '資料存取物件的類別名稱',
  `COMMAND_ID` varchar(20) DEFAULT NULL COMMENT 'SQL指令的識別代碼',
  `COMMAND_DIRECTION` int(11) DEFAULT NULL COMMENT 'SQL指令的執行方向，1: 請求指令，2: 執行指令的結果。',
  `COMMAND_TYPE` int(11) DEFAULT NULL COMMENT 'SQL指令的型態，1: CommandText，2: Stored Procedure',
  `COMMAND_TEXT` longtext COMMENT 'SQL指令的語法',
  `COMMAND_RESULT` longtext COMMENT 'SQL指令的執行結果',
  `HOST_NAME` varchar(100) NOT NULL COMMENT '電腦名稱或IP位址',
  `LOG_TIME` datetime(3) NOT NULL COMMENT '紀錄時間',
  PRIMARY KEY (`LOG_SRNO`),
  KEY `CommandIdIndex` (`COMMAND_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=1033 DEFAULT CHARSET=utf8 COMMENT='SQL 執行日誌檔';

-- 取消選取資料匯出。
-- 傾印  表格 zayni.FS_UNIT_TEST_USER 結構
CREATE TABLE IF NOT EXISTS `FS_UNIT_TEST_USER` (
  `ACCOUNT_ID` varchar(50) NOT NULL,
  `NAME` varchar(50) NOT NULL,
  `AGE` int(11) NOT NULL,
  `SEX` int(11) DEFAULT NULL,
  `BIRTHDAY` datetime DEFAULT NULL,
  `IS_VIP` bit(1) DEFAULT NULL COMMENT '資料庫中 Boolean 布林值型別欄位，在MySQL資料庫中: 1. MySQL資料庫可以開成 Bit(1) 或 TinyInt(1)。2. 程式中 MySqlDbType.Bit 可以直接對應。使用 MySqlDbType.Int32 也是可以，但比較不建議。3. 程式中資料值可以直接為 bool 型別，或是將 bool 依照 0 = false，1 = true 的規則，轉換成 Int32 之後都可以。4. 查詢使用者DataAdapter.Fill之後取得DataTable之後，直接使用 Newtonsoft.Json 進行ORM轉換，可以直接轉換成正確的 bool 型別。',
  `IS_GOOD` bit(1) DEFAULT NULL,
  `DATA_FLAG` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`ACCOUNT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ZayniFramework 框架 DataAccess unit test 測試用資料表';

-- 取消選取資料匯出。
-- 傾印  表格 zayni.FS_UNIT_TEST_USER2 結構
CREATE TABLE IF NOT EXISTS `FS_UNIT_TEST_USER2` (
  `ACCOUNT_ID` varchar(50) NOT NULL,
  `USER_ID` varchar(50) NOT NULL,
  `USER_NAME` varchar(50) NOT NULL,
  `USER_PASSWORD` varchar(50) DEFAULT NULL,
  `BIRTHDAY` datetime DEFAULT NULL,
  `SEX` int(11) NOT NULL,
  `MONEY` double DEFAULT NULL,
  `DESCRIPTION` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ACCOUNT_ID`,`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ZayniFramework 框架 DataAccess unit test 測試用資料表';

-- 取消選取資料匯出。
-- 傾印  表格 zayni.FS_UNIT_TEST_USER_PHONE_DETAIL 結構
CREATE TABLE IF NOT EXISTS `FS_UNIT_TEST_USER_PHONE_DETAIL` (
  `ACCOUNT_ID` varchar(50) NOT NULL,
  `PHONE_TYPE` varchar(50) NOT NULL,
  `PHONE_NUMBER` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ACCOUNT_ID`,`PHONE_TYPE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ZayniFramework 框架 DataAccess unit test 測試用資料表';

-- 取消選取資料匯出。
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;


INSERT INTO FS_UNIT_TEST_USER (ACCOUNT_ID, NAME, AGE, SEX, BIRTHDAY, IS_VIP, IS_GOOD)
VALUES ('Dexter Gordon', 'Dexter Gordon', 69, 1, '1942-07-30 00:48:26.000', 1, 0);

INSERT INTO FS_UNIT_TEST_USER (ACCOUNT_ID, NAME, AGE, SEX, BIRTHDAY, IS_VIP, IS_GOOD)
VALUES ('Jaco Pastorius', 'Jaco Pastorius', 36, 1, '1970-11-23 00:47:18.000', 0, 0);

INSERT INTO FS_UNIT_TEST_USER (ACCOUNT_ID, NAME, AGE, SEX, BIRTHDAY, IS_VIP, IS_GOOD)
VALUES ('Judy Bitch', 'Judy Bitch', 19, 0, '2000-05-24 00:45:21.000', 1, 0);

INSERT INTO FS_UNIT_TEST_USER (ACCOUNT_ID, NAME, AGE, SEX, BIRTHDAY, IS_VIP, IS_GOOD)
VALUES ('Nancy Gili', 'Nancy Gili', 24, 0, '1998-08-06 00:46:22.000', 1, 1);

INSERT INTO FS_UNIT_TEST_USER (ACCOUNT_ID, NAME, AGE, SEX, BIRTHDAY, IS_VIP, IS_GOOD)
VALUES ('WebAPI.Test.CacheManagerTester', 'WebAPI.Test.CacheManagerTester', 40, 1, '1980-12-12 00:14:14.000', 1, 1);